export const environment = {
  modulo_gestao_pessoa: 'http://localhost:3000/api',
  modulo_gestao_tarefa: 'http://localhost:3000/api',
  modulo_gestao_projeto: 'http://localhost:3000/api',
  url_api_ged: 'http://localhost:3001/ged-gft-sisbr/v1',
  PRODUCTION: false,
  DEV: true,
  API_GATEWAY: 'http://localhost:3001',
  AUTH_GATEWAY: 'http://localhost:3001/token',
  REVOKE_GATEWAY: 'http://localhost:3001/revoke',
  APPLICATION_TOKEN: 'Basic UWdqX05mcmRmVGxwNFVDZUV3dTVHVDJxOWE0YTp1U1VWbUp1TFY3MGZsTFNIN0NQZXF4aVhuYVFh',
  SSO_GATEWAY: 'http://localhost:3001/cas',
  HOST: 'http://localhost:4200',
  URL_CEP: 'http://localhost:3001/correios/1.0.0/enderecos/',
  ASSETS: 'http://localhost:4200/assets/',
  URL_SCI: 'http://localhost:3001/sistema-cadastro-instituicoes/v1'
};
