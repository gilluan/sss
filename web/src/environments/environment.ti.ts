export const environment = {
  modulo_gestao_pessoa: 'http://apit.homologacao.com.br:8280/sins-gestao-pessoas',
  modulo_gestao_tarefa: 'http://apit.homologacao.com.br:8280/sins-gestao-tarefas',
  modulo_gestao_projeto: 'http://apit.homologacao.com.br:8280/sins-gestao-projetos',
  url_api_ged: 'http://apit.homologacao.com.br:8280/ged-gft-sisbr/v1',
  PRODUCTION: false,
  DEV: false,
  API_GATEWAY: 'http://apit.homologacao.com.br:8280',
  AUTH_GATEWAY: 'http://apit.homologacao.com.br:8280/token',
  REVOKE_GATEWAY: 'http://apit.homologacao.com.br:8280/revoke',
  APPLICATION_TOKEN: 'Basic WlJuVzFabXgya1pLUl9DT1lyTFBNc1ZrSVNFYTpmRDZNVGdNa09nZng5dl81cDhfZjN5cFdiNjhh',
  SSO_GATEWAY: 'http://cas-ti.homologacao.com.br:8080/cas',
  HOST: 'http://sins-app-angular-backoffice-nodejs-ti.homologacao.com.br/',
  URL_CEP: 'http://apit.homologacao.com.br:8280/correios/1.0.0/enderecos/',
  ASSETS: 'http://sins-app-angular-backoffice-nodejs-ti.homologacao.com.br/sins/assets/'
};
