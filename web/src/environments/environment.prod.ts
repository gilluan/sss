export const environment = {
  modulo_gestao_pessoa: 'https://portal.sisbr.coop.br:6443/sins-gestao-pessoas',
  modulo_gestao_projeto: 'https://portal.sisbr.coop.br:6443/sins-gestao-projetos',
  url_api_ged: 'https://portal.sisbr.coop.br:6443/ged-gft-sisbr/v1',
  PRODUCTION: true,
  DEV: false,
  API_GATEWAY: 'https://portal.sisbr.coop.br:6443',
  AUTH_GATEWAY: 'https://portal.sisbr.coop.br:6443/token',
  REVOKE_GATEWAY: 'https://portal.sisbr.coop.br:6443/revoke',
  SSO_GATEWAY: 'https://portal.sisbr.coop.br:6443/cas',
  APPLICATION_TOKEN: 'Basic bzJURnRBVzdzUUcybDRfMnltM3d1Sk1sZktRYTpVMHh0OVBkMkRrSDZwU3RNTHFaTWg3aGp1WmNh',
  HOST: 'https://sins.sisbr.coop.br/sins/',
  URL_CEP: 'https://portal.sisbr.coop.br:6443/correios/1.0.0/enderecos/',
  modulo_gestao_tarefa: 'http://localhost:3001/api/',
  ASSETS: 'https://portal.sisbr.coop.br:6443/sins/assets/'
};
