export const environment = {
  modulo_gestao_pessoa: 'http://apimanager-homol.homologacao.com.br:8280/sins-gestao-pessoas',
  modulo_gestao_tarefa: 'http://apimanager-homol.homologacao.com.br:8280/sins-gestao-tarefas',
  modulo_gestao_projeto: 'http://apimanager-homol.homologacao.com.br:8280/sins-gestao-projetos',
  url_api_ged: 'http://apimanager-homol.homologacao.com.br:8280/ged-gft-sisbr/v1',
  PRODUCTION: false,
  DEV: false,
  API_GATEWAY: 'http://apimanager-homol.homologacao.com.br:8280',
  AUTH_GATEWAY: 'http://apimanager-homol.homologacao.com.br:8280/token',
  REVOKE_GATEWAY: 'http://apimanager-homol.homologacao.com.br:8280/revoke',
  APPLICATION_TOKEN: 'Basic UWdqX05mcmRmVGxwNFVDZUV3dTVHVDJxOWE0YTp1U1VWbUp1TFY3MGZsTFNIN0NQZXF4aVhuYVFh',
  SSO_GATEWAY: 'https://cas-homol.sisbr.coop.br/cas/v1',
  HOST: 'http://sins-app-angular-backoffice-nodejs-homol/',
  URL_CEP: 'http://apimanager-homol.homologacao.com.br:8280/correios/1.0.0/enderecos/',
  ASSETS: 'http://sins-app-angular-backoffice-nodejs-homol/sins/assets/'
};
