import {registerLocaleData} from '@angular/common';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import {LOCALE_ID, NgModule} from '@angular/core';
import {LoaderInterceptor} from '@app/interceptors/interceptors';
import {DialogoConfirmacaoComponent} from '@shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import {DialogoJustificativaAjustesComponent} from '@shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import {DialogoSucessoComponent} from '@shared/components/dialogo-sucesso/dialogo-sucesso.component';
import {SinsLoadingService} from '@shared/components/sins-loading/sins-loading.service';
import {PersistanceService} from '@shared/services/persistence.service';
import {AuthGuard} from '@sicoob/security';
import {SidebarContainerComponent} from '@sicoob/ui';
import {environment} from 'src/environments/environment';
import {AppComponent} from './app.component';
import {CallbackLoginComponent} from './callback-login.component';
import {EXPORTED_MODULES, MODULES, MODULES_DEV} from './declarations';
import {PerfilGuard} from './guards/perfil.guard';
import {ResolveGuard} from './guards/wait-photo.guard';
import {NotFoundComponent} from './not-found.component';
import {NotPermissionComponent} from './not-permission.component';
import {MessageService} from '@shared/services/message.service';
import {RouterService} from '@shared/services/routerext.service';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent,
    DialogoSucessoComponent,
    DialogoConfirmacaoComponent,
    DialogoJustificativaAjustesComponent,
    CallbackLoginComponent,
    NotFoundComponent,
    NotPermissionComponent
  ],
  imports: [ environment.DEV ? MODULES_DEV : MODULES ],
  providers: [
    RouterService,
    MessageService,
    ResolveGuard,
    PerfilGuard,
    AuthGuard,
    PersistanceService,
    SinsLoadingService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  exports: [EXPORTED_MODULES],
  entryComponents: [SidebarContainerComponent]
})
export class AppModule {

}
