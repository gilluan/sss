import { AppActionTypes, AppAction } from './app.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Game } from './shared/models/game.model';

export interface State {
  isToolbarVisible: boolean;
  padding: number;
  isScroll: boolean;
  visibilityHeader: boolean;
  lang: string;
  game: Game[];
}

const initialState: State = {
  isToolbarVisible: false,
  padding: 20,
  visibilityHeader: true,
  isScroll: false,
  lang: 'pt-br',
  game: []
}


export function reducer(state = initialState, action: AppAction): State {
  switch (action.type) {

    case AppActionTypes.AddGameUserSuccess: {
      return { ...state, game: action.game }
    }

    case AppActionTypes.ChangeLang: {
      return { ...state, lang: action.lang }
    }

    case AppActionTypes.DisplayToolbar: {
      return { ...state, isToolbarVisible: action.isVisible }
    }

    case AppActionTypes.ScrollContainer: {
      return { ...state, isScroll: action.isScroll }
    }

    case AppActionTypes.AjustarPadding: {
      return { ...state, padding: action.padding }
    }

    case AppActionTypes.DisplayVisibilityHeader: {
      return { ...state, visibilityHeader: action.visibilityHeader }
    }

    default: {
      return state;
    }
  }
}

export const selectAuthState = createFeatureSelector<State>('app');


export const getGame = createSelector(
  selectAuthState,
  state => state.game
);

export const selectIsToolbarVisible = createSelector(
  selectAuthState,
  state => state.isToolbarVisible
);


export const selectLang = createSelector(
  selectAuthState,
  state => state.lang
);


export const selectPadding = createSelector(
  selectAuthState,
  state => state.padding
);

export const selectVisibilityHeader = createSelector(
  selectAuthState,
  state => state.visibilityHeader
);

export const selectScrollContainer = createSelector(
  selectAuthState,
  state => state.isScroll
);

