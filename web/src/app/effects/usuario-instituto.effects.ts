import { Injectable } from '@angular/core';
import { CarregarUsuariosInstitutoPorFiltro, CarregarUsuariosInstitutoPorFiltroFail, CarregarUsuariosInstitutoPorFiltroSuccess, UsuarioInstitutoActionTypes, CarregarUsuariosInstitutoPorFiltroUnicSuccess, CarregarCooperativasFail, CarregarCooperativasSuccess } from '@app/actions/usuario-instituto.actions';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { UsuarioInstitutoService } from '@app/shared/services/usuario-instituto.service';
import { Actions, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Color } from '@sicoob/ui';


@Injectable()
export class UsuarioInstitutoEffects {

  constructor(
    private actions$: Actions,
    private service: UsuarioInstitutoService,
    private alertService: CustomAlertService,
    ) { }

  @Effect()
  carregarUsuariosInstituto$ =
    this.actions$
      .ofType(UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltro).pipe(
        map((action: CarregarUsuariosInstitutoPorFiltro) => action),
        switchMap((action) => this.service.findAll(action.email, action.cpf, action.nome, null, null, action.identificador).pipe(
          map((usuarioInstituto: UsuarioInstituto[]) => {
            if (action.unicResult)
              return new CarregarUsuariosInstitutoPorFiltroUnicSuccess(usuarioInstituto[0])
            else
              return new CarregarUsuariosInstitutoPorFiltroSuccess(usuarioInstituto)
          }),
          catchError(error => of(new CarregarUsuariosInstitutoPorFiltroFail(error))))));

  @Effect()
  carregarCooperativas$ =
    this.actions$.ofType(UsuarioInstitutoActionTypes.CarregarCooperativas).pipe(
      switchMap((_) => this.service.recuperarCooperativas().pipe(
        map((cooperativas: string[]) => new CarregarCooperativasSuccess(cooperativas)),
        catchError(error => of(new CarregarCooperativasFail(error))))));

  @Effect()
  carregarCooperativasFail$ =
    this.actions$.ofType(UsuarioInstitutoActionTypes.CarregarCooperativasFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar as cooperativas.')));
}
