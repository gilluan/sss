import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EditarVoluntario, SalvarVoluntario } from '@app/actions/voluntarioAction';
import {
  EditarCurso,
  EditarCursoVoluntario,
  SalvarCurso
} from '@app/funcionalidades/perfil-usuario/actions/perfil-usuario.actions';
import { AbrirModal } from '@app/funcionalidades/termo-voluntario/actions/termo-voluntario.actions';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store, Action } from '@ngrx/store';
import { Color } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  CarregarArquivo,
  CarregarArquivoFail,
  CarregarArquivoSuccess,
  CleanIndice,
  CriarArquivo,
  CriarArquivoFail,
  CriarArquivoSuccess,
  EditarArquivoCurso,
  EditarArquivoCursoSuccess,
  EditarArquivoCursoVoluntario,
  EditarArquivoCursoVoluntarioSuccess,
  GedActionTypes,
  GedEvents,
  SalvarArquivoCurso,
  SalvarArquivoCursoSuccess,
  SalvarArquivoCursoVoluntario,
  SalvarArquivoCursoVoluntarioSuccess,
  SalvarArquivosMultiUpload,
  SalvarArquivosMultiUploadFail,
  SalvarArquivosMultiUploadSuccess,
  UploadProgress,
  ExecutarPassoArquivo,
  ExecutarPassoArquivoSuccess
} from '../actions/ged.actions';
import * as fromGed from '../reducers/ged.reducer';
import { GedService } from '@shared/ged/ged.service';
import { DadosDocumento } from '@shared/ged/models/retorno/dadosDocumento.model';
import { DocumentoRetorno } from '@shared/ged/models/retorno/documentoRetorno.model';
import { MultiUploadFile } from '@app/shared/models/multiupload-file.model';
import { ExecutarPassoProjeto } from '@app/funcionalidades/projeto/actions/projeto.actions';
import { TYPE_UPLOAD_FILE } from '@app/funcionalidades/projeto/components/actionbar-execucao-tarefa/actionbar-execucao-tarefa.component';

@Injectable()
export class GedEffects {

  constructor(
    private readonly store$: Store<fromGed.State>,
    private readonly actions$: Actions,
    private readonly service: GedService,
    private readonly alertService: CustomAlertService) { }

  @Effect()
  criar$ = this.actions$.pipe(
    ofType(GedActionTypes.CriarArquivo),
    switchMap((action: CriarArquivo) => this.service.salvarDocumento2(action.documentoEnvio).pipe(
      switchMap((event: HttpEvent<any>) => {
        const retorno = this.getEventMessage(event);
        if (retorno && retorno.resultado && retorno.resultado.length > 0) {
          return [new CriarArquivoSuccess(`${retorno.resultado[0]}`), ...action.acoes];
        } else if (retorno.percent != null) {
          return [new UploadProgress(retorno.percent)];
        } else {
          return [new GedEvents()];
        }
      }
      ))), catchError(erro => { console.log('Error', erro); return of(new CriarArquivoFail(erro)); }));

  @Effect({ dispatch: false })
  criarFail$ = this.actions$.pipe(
    ofType(GedActionTypes.CriarArquivoFail),
    map((action: CriarArquivoFail) => action.payload),
    map(erro => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar arquivo.')));

  @Effect()
  salvarCurso$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivoCurso),
    map((action: SalvarArquivoCurso) =>
      new CriarArquivo(action.documentoEnvio, [new SalvarArquivoCursoSuccess(action.curso, action.actionBar)])));

  @Effect()
  salvarCursoSuccess$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivoCursoSuccess),
    map((action: SalvarArquivoCursoSuccess) => action),
    withLatestFrom(this.store$.pipe(select(fromGed.getArquivoGedState))),
    mergeMap(([action, state]: [SalvarArquivoCursoSuccess, fromGed.State]) => {
      action.curso.identificadorCertificadoGED = state.identificadorGed;
      return of(new SalvarCurso(action.curso, action.actionBar));
    }));


  @Effect()
  ExecutarPassoArquivo$ = this.actions$.pipe(
    ofType(GedActionTypes.ExecutarPassoArquivo),
    map((action: ExecutarPassoArquivo) =>
      new SalvarArquivosMultiUpload(action.documentosEnvio, action.multiUploadFiles, [new ExecutarPassoArquivoSuccess(action.passoFluxo, action.filtro, action.actionBarRef)])
    ));

  @Effect()
  ExecutarPassoArquivoSuccess$ = this.actions$.pipe(
    ofType(GedActionTypes.ExecutarPassoArquivoSuccess),
    map((action: ExecutarPassoArquivoSuccess) => action),
    withLatestFrom(this.store$.pipe(select(fromGed.getArquivoGedState))),
    mergeMap(([action, state]: [ExecutarPassoArquivoSuccess, fromGed.State]) => {
      let respostaForm = action.passoFluxo.execucao.respostaForm;
      const keys = Object.keys(respostaForm);
      const fileUploadMultiple = state.fileUploadMultiple;
      const keysFiles = fileUploadMultiple.map((f: MultiUploadFile) => f.key);
      for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        if (keysFiles.includes(key)) {
          const files = fileUploadMultiple.filter((f: MultiUploadFile) => f.key === key).map((f: MultiUploadFile) => { return { identificadorGed: f.identificadorGed, nome: f.nome, tamanho: f.tamanho, contentType: f.contentType } })
          respostaForm[key] = { value: files, type: TYPE_UPLOAD_FILE };
        }
      }
      return of(new ExecutarPassoProjeto(action.passoFluxo.id, action.passoFluxo.execucao, action.passoFluxo.pontuacao, action.filtro, action.actionBarRef))
    }));


  @Effect()
  EditarCurso$ = this.actions$.pipe(
    ofType(GedActionTypes.EditarArquivoCurso),
    map((action: EditarArquivoCurso) =>
      new CriarArquivo(action.documentoEnvio, [new EditarArquivoCursoSuccess(action.curso, action.actionBar)])));

  @Effect()
  EditarCursoSuccess$ = this.actions$.pipe(
    ofType(GedActionTypes.EditarArquivoCursoSuccess),
    map((action: EditarArquivoCursoSuccess) => action),
    withLatestFrom(this.store$.pipe(select(fromGed.getArquivoGedState))),
    mergeMap(([action, state]: [EditarArquivoCursoSuccess, fromGed.State]) => {
      action.curso.identificadorCertificadoGED = state.identificadorGed;
      return of(new EditarCurso(action.curso, action.actionBar));
    }));

  @Effect()
  salvarCursoVoluntario$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivoCursoVoluntario),
    switchMap((action: SalvarArquivoCursoVoluntario) => [
      new AbrirModal(),
      new CriarArquivo(action.documentoEnvio, [new SalvarArquivoCursoVoluntarioSuccess(action.curso, action.voluntario)])]));

  @Effect()
  salvarCursoVoluntarioSuccess$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivoCursoVoluntarioSuccess),
    map((action: SalvarArquivoCursoVoluntarioSuccess) => action),
    withLatestFrom(this.store$.pipe(select(fromGed.getArquivoGedState))),
    mergeMap(([action, state]: [SalvarArquivoCursoVoluntarioSuccess, fromGed.State]) => {
      action.curso.identificadorCertificadoGED = state.identificadorGed;
      action.voluntario.idDocumentoCertificado = state.identificadorGed;
      return [new SalvarVoluntario(action.voluntario, action.curso)];
    }));

  @Effect()
  editarCursoVoluntario$ = this.actions$.pipe(
    ofType(GedActionTypes.EditarArquivoCursoVoluntario),
    switchMap((action: EditarArquivoCursoVoluntario) => [
      new AbrirModal(),
      new CriarArquivo(action.documentoEnvio, [new EditarArquivoCursoVoluntarioSuccess(action.curso, action.voluntario)])]));

  @Effect()
  editarCursoVoluntarioSuccess$ = this.actions$.pipe(
    ofType(GedActionTypes.EditarArquivoCursoVoluntarioSuccess),
    map((action: EditarArquivoCursoVoluntarioSuccess) => action),
    withLatestFrom(this.store$.pipe(select(fromGed.getArquivoGedState))),
    mergeMap(([action, state]: [EditarArquivoCursoVoluntarioSuccess, fromGed.State]) => {
      action.curso.identificadorCertificadoGED = state.identificadorGed;
      action.voluntario.idDocumentoCertificado = state.identificadorGed;
      return [new EditarCursoVoluntario(action.curso), new EditarVoluntario(action.voluntario)];
    }));

  @Effect()
  carregar$ = this.actions$.pipe(
    ofType(GedActionTypes.CarregarArquivo),
    map((action: CarregarArquivo) => action.identificador),
    switchMap((identificador: string) => this.service.pesquisarDocumento(identificador).pipe(
      map((retorno: DocumentoRetorno) => retorno.resultado),
      map((documento: DadosDocumento) => new CarregarArquivoSuccess(documento)),
      catchError(erro => of(new CarregarArquivoFail(erro))))));

  @Effect({ dispatch: false })
  carregarFail$ = this.actions$.pipe(
    ofType(GedActionTypes.CarregarArquivoFail),
    map((action: CarregarArquivoFail) => action.payload),
    map(erro => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar o arquivo.')));

  @Effect()
  SalvarArquivosMultiUpload$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivosMultiUpload),
    withLatestFrom(this.store$.pipe(select(fromGed.getIndiceAtual))),
    switchMap(([action, indice]: [SalvarArquivosMultiUpload, number]) => {
      action.actions
      let retorno: Action[] = [];
      if (indice < action.documentosEnvio.length) {
        retorno = [new CriarArquivo(action.documentosEnvio[indice], [new SalvarArquivosMultiUploadSuccess(action.documentosEnvio, action.multiUploadFiles, action.actions), ...action.actions])]
      } else {
        retorno = [new CleanIndice()]
      }
      return retorno;
    }

    ));

  @Effect()
  SalvarArquivosMultiUploadSuccess$ = this.actions$.pipe(ofType(GedActionTypes.SalvarArquivosMultiUploadSuccess),
    switchMap((action: SalvarArquivosMultiUploadSuccess) => {
      return [new SalvarArquivosMultiUpload(action.documentosEnvio, action.multiUploadFiles, action.actions)];
    }), catchError(erro => of(new SalvarArquivosMultiUploadFail(erro))));

  @Effect()
  SalvarArquivosMultiUploadFail$ = this.actions$.pipe(
    ofType(GedActionTypes.SalvarArquivosMultiUploadFail),
    map((action: SalvarArquivosMultiUploadFail) => action.payload)
  );

  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        const percent = Math.round(100 * event.loaded / event.total);
        return { percent };
      case HttpEventType.Response:
        return event.body;
      default:
        return 0;
    }
  }
}



