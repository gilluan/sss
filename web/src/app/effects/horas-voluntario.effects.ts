import { Injectable } from '@angular/core';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Color } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { HorasVoluntario } from '@app/shared/models/horas-voluntario.model';
import { HorasVoluntarioTypes, CarregarHorasVoluntario, CarregarHorasVoluntarioSuccess, CarregarHorasVoluntarioFail, SalvarHorasVoluntario, SalvarHorasVoluntarioSuccess, SalvarHorasVoluntarioFail, EditarHorasVoluntario, EditarHorasVoluntarioSuccess, EditarHorasVoluntarioFail, RemoverHorasVoluntario, RemoverHorasVoluntarioSuccess, RemoverHorasVoluntarioFail, CarregarTotalHorasVoluntario, CarregarTotalHorasVoluntarioSuccess, CarregarTotalHorasVoluntarioFail } from '@app/actions/horas-voluntario.actions';
import { HorasVoluntarioService } from '@app/shared/services/horas-voluntario.service';
import { PageModelVm } from '@app/shared/models/page.model';


@Injectable()
export class HorasVoluntarioEffects {

  constructor(private actions$: Actions, private readonly horasVoluntarioService: HorasVoluntarioService, private alertService: CustomAlertService) { }

  @Effect()
  loadHorasVoluntarioById$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.CarregarHorasVoluntario),
    switchMap((action: CarregarHorasVoluntario) => this.horasVoluntarioService.carregaHorasVoluntarios(action.id, action.paginacao).pipe(
      switchMap((HorasVoluntario: PagedDataModelVm) => [new CarregarHorasVoluntarioSuccess(HorasVoluntario), new CarregarTotalHorasVoluntario(action.id)]),
      catchError((error: any) => of(new CarregarHorasVoluntarioFail(error)))
    )));

  @Effect({ dispatch: false })
  loadHorasVoluntarioByIdFail$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.CarregarHorasVoluntarioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível recuperar o Horas de voluntário')));


  @Effect()
  loadTotalHorasVoluntarioById$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.CarregarTotalHorasVoluntario),
    switchMap((action: CarregarTotalHorasVoluntario) => this.horasVoluntarioService.carregaTotalHorasVoluntarios(action.id).pipe(
      map((horas: number) => new CarregarTotalHorasVoluntarioSuccess(horas)),
      catchError((error: any) => of(new CarregarTotalHorasVoluntarioFail(error)))
    )));

  @Effect({ dispatch: false })
  loadTotalHorasVoluntarioByIdFail$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.CarregarTotalHorasVoluntarioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível recuperar o total de horas de voluntário')));

  @Effect()
  addHorasVoluntario$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.SalvarHorasVoluntario),
    switchMap((action: SalvarHorasVoluntario) => this.horasVoluntarioService.salvarHorasVoluntario(action.horasVoluntario).pipe(
      switchMap(_ => [new SalvarHorasVoluntarioSuccess(action.horasVoluntario), new CarregarHorasVoluntario(action.horasVoluntario.voluntario, new PageModelVm(0, 5, 0))]),
      catchError((error: any) => of(new SalvarHorasVoluntarioFail(error)))
    )));

  @Effect({ dispatch: false })
  addHorasVoluntarioFail$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.SalvarHorasVoluntarioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível adicionar as horas ao voluntário')));

  @Effect({ dispatch: false })
  addHorasVoluntarioSuccess$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.SalvarHorasVoluntarioSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Horas cadastradas com sucesso!')));

  @Effect()
  updateHorasVoluntario$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.EditarHorasVoluntario),
    switchMap((action: EditarHorasVoluntario) => this.horasVoluntarioService.alterarHorasVoluntario(action.horasVoluntario).pipe(
      switchMap(_ => [new EditarHorasVoluntarioSuccess(action.horasVoluntario), new CarregarHorasVoluntario(action.horasVoluntario.voluntario, new PageModelVm(0, 5, 0))]),
      catchError((error: any) => of(new EditarHorasVoluntarioFail(error)))
    )));

  @Effect({ dispatch: false })
  updateHorasVoluntarioFail$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.EditarHorasVoluntarioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível alterar as horas')));

  @Effect({ dispatch: false })
  updateHorasVoluntarioSuccess$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.EditarHorasVoluntarioSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Horas alteradas com sucesso!')));

    @Effect()
  deleteHorasVoluntario$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.RemoverHorasVoluntario),
    switchMap((action: RemoverHorasVoluntario) => this.horasVoluntarioService.excluirHorasVoluntario(action.horasVoluntario.id).pipe(
      switchMap(_ => [new RemoverHorasVoluntarioSuccess(action.horasVoluntario.id), new CarregarHorasVoluntario(action.horasVoluntario.voluntario._id, new PageModelVm(0, 5, 0))]),
      catchError((error: any) => of(new RemoverHorasVoluntarioFail(error)))
    )));

  @Effect({ dispatch: false })
  deleteHorasVoluntarioFail$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.RemoverHorasVoluntarioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir as horas do voluntário')));

  @Effect({ dispatch: false })
  deleteHorasVoluntarioSuccess$ = this.actions$.pipe(
    ofType(HorasVoluntarioTypes.RemoverHorasVoluntarioSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Horas excluídas com sucesso!')));

}
