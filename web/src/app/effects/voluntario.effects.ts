import { Injectable, NgZone } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  AssinarContratoVoluntario,
  AssinarContratoVoluntarioFail,
  AssinarContratoVoluntarioSuccess,
  CarregarCooperativasFail,
  CarregarCooperativasSuccess,
  CarregarTotais,
  CarregarTotaisFail,
  CarregarTotaisSuccess,
  CarregarTotalAnaliseFail,
  CarregarTotalAnaliseSuccess,
  CarregarTotalAssinarFail,
  CarregarTotalAssinarSuccess,
  CarregarTotalBancoVoluntarioFail,
  CarregarTotalBancoVoluntarioSuccess,
  CarregarTotalPorFiltro,
  CarregarTotalPorFiltroFail,
  CarregarTotalPorFiltroSuccess,
  CarregarVoluntariosPorCPF,
  CarregarVoluntariosPorCPFFail,
  CarregarVoluntariosPorCPFSuccess,
  CarregarVoluntariosPorFiltro,
  CarregarVoluntariosPorFiltroFail,
  CarregarVoluntariosPorFiltroSuccess,
  DesativarVoluntario,
  DesativarVoluntarioFail,
  DesativarVoluntarioSuccess,
  VoluntarioActionTypes,
  SalvarVoluntario,
  SalvarVoluntarioSuccess,
  SalvarVoluntarioFail,
  ConcordarTermoVoluntario,
  ConcordarTermoVoluntarioSuccess,
  ConcordarTermoVoluntarioFail,
  EditarVoluntario,
  EditarVoluntarioSuccess,
  EditarVoluntarioFail,
  AtivarVoluntarioSuccess,
  AtivarVoluntarioFail,
  AtivarVoluntario,
  CarregarVoluntariosPorId,
  CarregarVoluntariosPorIdSuccess,
  CarregarVoluntariosPorIdFail
} from '../actions/voluntarioAction';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { VoluntariosService } from '../funcionalidades/sins-analise-cadastros/voluntarios.service';
import { PagedDataModelVm } from '@shared/models/paged-data.model';
import { Observable, of } from 'rxjs';
import { ModalService, Color } from '@sicoob/ui';
import { VoluntarioDto } from '@shared/models/voluntario-dto.model';
import { Action, select, Store } from '@ngrx/store';
import { TotalSituacaoVm } from '../funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import * as fromVoluntario from '../reducers/voluntario.reducer';
import { VoluntarioFiltroDto } from '@shared/models/voluntario-filtro.model';
import { AssinarVoluntarioSuccess } from '../funcionalidades/sins-analise-cadastros/actions/analise-voluntario.actions';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Router } from '@angular/router';
import { SituacaoVoluntarioType } from '@app/funcionalidades/sins-analise-cadastros/models/types/situacao-voluntario.type';
import { Voluntario } from '@app/funcionalidades/termo-voluntario/models/voluntario.model';
import { AbrirModal } from '@app/funcionalidades/termo-voluntario/actions/termo-voluntario.actions';
import { SalvarCursoVoluntario } from '@app/funcionalidades/perfil-usuario/actions/perfil-usuario.actions';

@Injectable()
export class VoluntarioEffects {

  constructor(
    private ngZone: NgZone,
    private store$: Store<fromVoluntario.State>,
    private router: Router,
    private actions$: Actions,
    private service: VoluntariosService,
    private modalService: ModalService,
    private alertService: CustomAlertService) { }

  @Effect()
  carregarVoluntariosPorFiltro$ =
    this.actions$
      .ofType(VoluntarioActionTypes.CarregarVoluntariosPorFiltro).pipe(
        map((action: CarregarVoluntariosPorFiltro) => action.filter),
        switchMap((filter) => this.service.pesquisarPorFiltro(filter).pipe(
          map((pagedData: PagedDataModelVm) => new CarregarVoluntariosPorFiltroSuccess(pagedData)),
          catchError(error => of(new CarregarVoluntariosPorFiltroFail(error))))));

  @Effect()
  carregarVoluntariosPorId$ =
    this.actions$
      .ofType(VoluntarioActionTypes.CarregarVoluntariosPorId).pipe(
        map((action: CarregarVoluntariosPorId) => action),
        switchMap((action) => this.service.findVoluntarioById(action.id).pipe(
          map((voluntario: VoluntarioDto) => new CarregarVoluntariosPorIdSuccess(voluntario)),
          catchError(error => of(new CarregarVoluntariosPorIdFail(error))))));

  @Effect()
  carregarVoluntariosPorCPF$ =
    this.actions$
      .ofType(VoluntarioActionTypes.CarregarVoluntariosPorCPF).pipe(
        map((action: CarregarVoluntariosPorCPF) => action),
        switchMap((action) => this.service.findVoluntarioByCpf(action.cpf).pipe(
          map((voluntario: VoluntarioDto) => new CarregarVoluntariosPorCPFSuccess(voluntario)),
          catchError(error => of(new CarregarVoluntariosPorCPFFail(error))))));

  @Effect()
  carregarVoluntariosPorFiltroSuccess$ = this.actions$.pipe(
    ofType(VoluntarioActionTypes.CarregarVoluntariosPorFiltroSuccess),
    withLatestFrom(this.store$.pipe(select(fromVoluntario.selectFiltro))),
    mergeMap(([_, filtro]: [Action, VoluntarioFiltroDto]) => of(new CarregarTotais(filtro))));

  @Effect({ dispatch: false })
  carregarVoluntariosPorFiltroFail$ =
    this.actions$
      .ofType(VoluntarioActionTypes.CarregarVoluntariosPorFiltroFail).pipe(
        map((action: CarregarVoluntariosPorFiltroFail) => action.payload),
        map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os voluntários.')));

    @Effect()
    ativarVoluntario$ =
      this.actions$
        .ofType(VoluntarioActionTypes.AtivarVoluntario).pipe(
          map((action: AtivarVoluntario) => action),
          switchMap((action: AtivarVoluntario) => this.service.ativarVoluntario(action.voluntario.id).pipe(
            map((voluntario: VoluntarioDto) => {
              this.alertService.abrirAlert(Color.SUCCESS, `${voluntario.nome} foi ativado com sucesso!`);
              return new CarregarVoluntariosPorFiltro(action.filter);
            }),
            catchError(error => of(new AtivarVoluntarioFail(error))))));

    @Effect()
    ativarVoluntarioFail$ =
      this.actions$
        .ofType(VoluntarioActionTypes.AtivarVoluntarioFail).pipe(
          map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os voluntários.')));


  @Effect()
  desativarVoluntario$ =
    this.actions$
      .ofType(VoluntarioActionTypes.DesativarVoluntario).pipe(
        map((action: DesativarVoluntario) => action),
        switchMap((action: DesativarVoluntario) => this.service.desativarVoluntario(action.voluntario.id).pipe(
          map((voluntario: VoluntarioDto) => {
            this.alertService.abrirAlert(Color.SUCCESS, `${voluntario.nome} foi inativado com sucesso!`);
            return new CarregarVoluntariosPorFiltro(action.filter);
          }),
          catchError(error => of(new DesativarVoluntarioFail(error))))));

  @Effect()
  desativarVoluntarioFail$ =
    this.actions$
      .ofType(VoluntarioActionTypes.DesativarVoluntarioFail).pipe(
        map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os voluntários.')));

  @Effect()
  carregarCooperativas$ =
    this.actions$.ofType(VoluntarioActionTypes.CarregarCooperativas).pipe(
      switchMap((_) => this.service.recuperarCooperativasDeVoluntarios().pipe(
        map((cooperativas: string[]) => new CarregarCooperativasSuccess(cooperativas)),
        catchError(error => of(new CarregarCooperativasFail(error))))));

  @Effect()
  carregarCooperativasFail$ =
    this.actions$.ofType(VoluntarioActionTypes.CarregarCooperativasFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar as cooperativas.')));

  @Effect()
  carregarTotais$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotais),
      map((action: CarregarTotais) => action.filter),
      switchMap(filtro => this.service.pesquisarSituacaoTotal(filtro).pipe(
        map((totais: TotalSituacaoVm[]) => new CarregarTotaisSuccess(totais)),
        catchError(error => of(new CarregarTotaisFail(error))))));

  @Effect({ dispatch: false })
  carregarTotaisFail$ =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotaisFail),
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os totais.')));

  @Effect()
  assinarContrato$ = this.actions$.pipe(
    ofType(VoluntarioActionTypes.AssinarContratoVoluntario),
    map((action: AssinarContratoVoluntario) => action.id),
    switchMap((id: string) => this.service.assinarVoluntario(id).pipe(
      map((voluntario: VoluntarioDto) => new AssinarContratoVoluntarioSuccess(voluntario)),
      catchError(error => of(new AssinarContratoVoluntarioFail(error))))));

  @Effect({ dispatch: false })
  assinarContratoSuccess$ = this.actions$.pipe(
    ofType(VoluntarioActionTypes.AssinarContratoVoluntarioSuccess),
    map((action: AssinarVoluntarioSuccess) => action.payload),
    map((voluntario: VoluntarioDto) => this.alertService.abrirAlert(Color.SUCCESS, `${voluntario.nome} assinado com sucesso!`, 1000)));

  @Effect({ dispatch: false })
  assinarContratoFail$ = this.actions$.pipe(
    ofType(VoluntarioActionTypes.AssinarContratoVoluntarioFail),
    map((action: AssinarContratoVoluntarioFail) => action.payload),
    map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível assinar o contrato!', 1000)));

  @Effect()
  carregarTotalPorFiltro$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotalPorFiltro),
      map((action: CarregarTotalPorFiltro) => action.filtro),
      switchMap((filtro: VoluntarioFiltroDto) => this.service.pesquisarTotalPorFiltro(filtro).pipe(
        map((total: number) => new CarregarTotalPorFiltroSuccess(total)),
        catchError(error => of(new CarregarTotalPorFiltroFail(error))))));


  @Effect()
  carregarTotalAssinatura$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotalAssinar),
      map((action: CarregarTotalPorFiltro) => action.filtro),
      switchMap((filtro: VoluntarioFiltroDto) => this.service.pesquisarTotalPorFiltro(filtro).pipe(
        map((total: number) => new CarregarTotalAssinarSuccess(total)),
        catchError(error => of(new CarregarTotalAssinarFail(error))))));

  @Effect()
  carregarTotalBancoVoluntario$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotalBancoVoluntario),
      map((action: CarregarTotalPorFiltro) => action.filtro),
      switchMap((filtro: VoluntarioFiltroDto) => this.service.pesquisarTotalPorFiltro(filtro).pipe(
        map((total: number) => new CarregarTotalBancoVoluntarioSuccess(total)),
        catchError(error => of(new CarregarTotalBancoVoluntarioFail(error))))));

  @Effect()
  carregarTotalAnaliseVoluntario$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.CarregarTotalAnalise),
      map((action: CarregarTotalPorFiltro) => action.filtro),
      switchMap((filtro: VoluntarioFiltroDto) => this.service.pesquisarTotalPorFiltro(filtro).pipe(
        map((total: number) => new CarregarTotalAnaliseSuccess(total)),
        catchError(error => of(new CarregarTotalAnaliseFail(error))))));

  @Effect()
  salvarVoluntario$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.SalvarVoluntario),
      map((action: SalvarVoluntario) => action),
      switchMap((action: SalvarVoluntario) => this.service.criarVoluntario(action.voluntario).pipe(
        switchMap((voluntario: Voluntario) => {
          voluntario.situacao = SituacaoVoluntarioType.aguardandoAnalise;
          action.curso.voluntario = voluntario.id;
          return [new SalvarCursoVoluntario(action.curso), new SalvarVoluntarioSuccess(voluntario), new ConcordarTermoVoluntario(voluntario)]
        }),
        catchError(error => of(new SalvarVoluntarioFail(error))))));

  @Effect()
  editarVoluntario$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.EditarVoluntario),
      map((action: EditarVoluntario) => action.voluntario),
      switchMap((voluntario: Voluntario) => this.service.atualizarVoluntario(voluntario).pipe(
        switchMap((voluntario: Voluntario) => {
          voluntario.situacao = SituacaoVoluntarioType.aguardandoAnalise;
          return [new EditarVoluntarioSuccess(voluntario), new ConcordarTermoVoluntario(voluntario)]
        }),
        catchError(error => of(new EditarVoluntarioFail(error))))));


  @Effect()
  concordarTermo$: Observable<Action> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.ConcordarTermoVoluntario),
      map((action: ConcordarTermoVoluntario) => action.voluntario),
      switchMap((voluntario: VoluntarioDto) => this.service.concordarTermo(voluntario).pipe(
        switchMap((voluntario: VoluntarioDto) => [new AbrirModal(), new ConcordarTermoVoluntarioSuccess(voluntario)]),
        catchError(error => of(new ConcordarTermoVoluntarioFail(error))))));

  /* @Effect({ dispatch: false })
  concordarTermoSuccess$: Observable<Promise<boolean>> =
    this.actions$.pipe(
      ofType(VoluntarioActionTypes.ConcordarTermoVoluntarioSuccess),
      map((action: ConcordarTermoVoluntarioSuccess) => {action.voluntario}),
      map(_ => this.ngZone.run(() => this.router.navigate(['/acompanhamento-cadastro'])).then())); */

}
