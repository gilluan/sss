import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CallbackLoginComponent} from './callback-login.component';
import {TermoVoluntarioComponent} from './funcionalidades/termo-voluntario/components/termo-voluntario/termo-voluntario.component';
import {WelcomeVoluntarioComponent} from './funcionalidades/termo-voluntario/components/welcome-voluntario/welcome-voluntario.component';
import {PerfilGuard} from './guards/perfil.guard';
import {InitialRedirectorComponent} from './initial-redirector/initial-redirector.component';
import {NotFoundComponent} from './not-found.component';
import {NotPermissionComponent} from './not-permission.component';

const routes: Routes = [
  {
    path: 'unauthorized',
    component: NotPermissionComponent
  },
  //  {
  //  path: '', component: CallbackLoginComponent
  //},
    {
    //path: 'initial-redirector',
    path: '',
    component: InitialRedirectorComponent,
  }, {
    path: 'inicio',
    loadChildren: './funcionalidades/sins-home/sins-home.module#SinsHomeModule',
    data: {
      breadcrumb: 'Dashboard'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'plano-acao',
    loadChildren: './funcionalidades/plano-acao/plano-acao.module#PlanoAcaoModule',
    data: {
      breadcrumb: 'Plano de Ação'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'analise-cadastros',
    loadChildren: './funcionalidades/sins-analise-cadastros/sins-analise-cadastros.module#SinsAnaliseCadastrosModule',
    data: {
      breadcrumb: 'Voluntários em análise'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'configuracao-termo',
    loadChildren: './funcionalidades/configuracao-termo/configuracao-termo.module#ConfiguracaoTermoModule',
    data: {
      breadcrumb: 'Configuração Termo de Compromisso'
    },
    canActivate: [PerfilGuard]
  },
  {
    path: 'welcome',
    loadChildren: './funcionalidades/termo-voluntario/termo-voluntario.module#TermoVoluntarioModule',
    canActivate: [PerfilGuard]
  },
  {
    path: 'relatorios',
    loadChildren: './funcionalidades/sins-relatorios/sins-relatorios.module#SinsRelatoriosModule',
    data: {
      breadcrumb: 'Relatórios'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'acompanhamento-cadastro',
    loadChildren: './funcionalidades/acompanhamento-cadastro/acompanhamento-cadastro.module#AcompanhamentoCadastroModule',
    data: {
      breadcrumb: 'Acompanhamento Cadastro'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'projetos',
    loadChildren: './funcionalidades/projeto/projeto.module#ProjetoModule',
    data: {
      breadcrumb: 'Projetos'
    },
    canActivate: [ PerfilGuard]
  }, {
    path: 'perfil-usuario/:id',
    loadChildren: './funcionalidades/perfil-usuario/perfil-usuario.module#PerfilUsuarioModule',
    data: {
      breadcrumb: 'Perfil do Usuário'
    },
    canActivate: [ PerfilGuard]
  }, {
    path: 'programas/:id',
    loadChildren: './funcionalidades/programas/programas.module#ProgramasModule',
    data: {
      breadcrumb: 'Programas'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'fluxo-processo',
    loadChildren: './funcionalidades/fluxo-processo/fluxo-processo.module#FluxoProcessoModule',
    data: {
      breadcrumb: 'Fluxos de processo'
    },
    canActivate: [PerfilGuard]
  }, {
    path: 'portfolios',
    loadChildren: './funcionalidades/portfolio/portfolio.module#PortfolioModule',
    data: {
      breadcrumb: 'Portfolio'
    },
    canActivate: [PerfilGuard]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
