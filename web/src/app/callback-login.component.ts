import { Component, OnInit } from '@angular/core';


import * as fromStore from './reducers';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Login } from '@sicoob/security';

@Component({
  selector: 'sc-callback-login',
  template: `<sc-loader></sc-loader>`,
  styles: []
})
export class CallbackLoginComponent implements OnInit {

  constructor(
    private store$: Store<fromStore.State>,
    private route: ActivatedRoute,
  ) {  }

  ngOnInit() {
    this.route
      .queryParamMap.subscribe(
        item => {
          if(item != undefined && item != null ){
            this.store$.dispatch(new Login(item.get('ticket')));
          }
        }
      );
  }


  ngOnDestroy(): void {
  }

}
