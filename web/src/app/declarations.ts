import {OverlayModule} from '@angular/cdk/overlay';
import {PortalModule} from '@angular/cdk/portal';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EffectsModule} from '@ngrx/effects';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AuthModule} from '@sicoob/security';
import {
  CheckboxModule,
  FormModule,
  HeaderActionsContainerModule,
  HeaderModule,
  ModalModule,
  NavbarModule,
  SidebarContainerModule,
  ToolbarModule,
  UiModule
} from '@sicoob/ui';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ClickOutsideModule} from 'src/app/shared/directive/click-outside/click-outside.module';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppEffects} from './app.effects';
import {VoluntarioEffects} from './effects/voluntario.effects';
import {AcompanhamentoCadastroModule} from './funcionalidades/acompanhamento-cadastro/acompanhamento-cadastro.module';
import {ConfiguracaoTermoModule} from './funcionalidades/configuracao-termo/configuracao-termo.module';
import {PlanoAcaoModule} from './funcionalidades/plano-acao/plano-acao.module';
import {SinsAnaliseCadastrosModule} from './funcionalidades/sins-analise-cadastros/sins-analise-cadastros.module';
import {SinsHomeModule} from './funcionalidades/sins-home/sins-home.module';
import {SinsRelatoriosModule} from './funcionalidades/sins-relatorios/sins-relatorios.module';
import {TermoVoluntarioModule} from './funcionalidades/termo-voluntario/termo-voluntario.module';
import {metaReducers, reducers} from './reducers';
import {CodigoVoluntarioModule} from '@shared/pipe/codigoVoluntario.module';
import {GedEffects} from './effects/ged.effects';
import {UsuarioInstitutoEffects} from './effects/usuario-instituto.effects';
import {SharedModule} from '@shared/shared.module';
import {NgxPermissionsModule} from 'ngx-permissions';
import {PerfilUsuarioModule} from './funcionalidades/perfil-usuario/perfil-usuario.module';
import {ProjetoModule} from './funcionalidades/projeto/projeto.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ProgramasModule} from './funcionalidades/programas/programas.module';
import {FluxoProcessoModule} from '@app/funcionalidades/fluxo-processo/fluxo-processo.module';
import {PortfolioModule} from '@app/funcionalidades/portfolio/portfolio.module';
import {BreadcrumbModule} from '@shared/components/breadcrumb/breadcrumb.module';
import {FormioModule} from '@app/funcionalidades/formio/formio.module';

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

// Modulos que serão importados no app.module

export const MODULES_DEV = [
  SharedModule.forRoot(),
  InfiniteScrollModule,
  BrowserModule,
  AppRoutingModule,
  HttpClientModule,
  FormsModule,
  NgxPermissionsModule.forRoot(),
  ReactiveFormsModule,
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [HttpClient]
    }
  }),
  StoreModule.forRoot(
    reducers, { metaReducers }
  ),
  StoreRouterConnectingModule.forRoot({
    stateKey: 'router',
  }),
  NgxDatatableModule,
  !environment.PRODUCTION ? StoreDevtoolsModule.instrument() : [],
  EffectsModule.forRoot([AppEffects, VoluntarioEffects, GedEffects, UsuarioInstitutoEffects ]),
  UiModule,
  NavbarModule,
  ProgramasModule,
  ToolbarModule,
  HeaderModule,
  OverlayModule,
  PortalModule,
  ModalModule,
  TermoVoluntarioModule,
  CheckboxModule,
  SidebarContainerModule,
  SinsAnaliseCadastrosModule,
  ConfiguracaoTermoModule,
  CodigoVoluntarioModule,
  BrowserAnimationsModule,
  SinsHomeModule,
  AcompanhamentoCadastroModule,
  TermoVoluntarioModule,
  PlanoAcaoModule,
  TermoVoluntarioModule,
  ClickOutsideModule,
  SinsRelatoriosModule,
  FormModule,
  PerfilUsuarioModule,
  ProjetoModule,
  FluxoProcessoModule,
  PortfolioModule,
  BreadcrumbModule,
  HeaderActionsContainerModule,
  FormioModule
];

export const MODULES = [
  MODULES_DEV,
  AuthModule.forRoot({
    apiGateway: environment.API_GATEWAY,
    authGateway: environment.AUTH_GATEWAY,
    ssoGateway: environment.SSO_GATEWAY,
    applicationToken: environment.APPLICATION_TOKEN,
    revokeGateway: environment.REVOKE_GATEWAY,
    sessionTokenService: environment.HOST,
    indexRoute: '/initial-redirector',
    unAuthorizedRoute: '/unauthorized'
  }),
];

// Módulos que serão exportados no app.module
export const EXPORTED_MODULES = [
  FormsModule,
  ReactiveFormsModule,
  UiModule
];
