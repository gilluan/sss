// loader.interceptors.ts
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {LoaderService} from '@shared/services/loader.service';
import { environment } from 'src/environments/environment';


@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService) { }

  routasSemLoading = [
    `${environment.url_api_ged}/documentos`,
    `${environment.modulo_gestao_projeto}/rubricas`,
    `${environment.modulo_gestao_projeto}/usuarios-instituto`,
  ]

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if( !this.routasSemLoading.includes(req.url) )
      this.loaderService.show()

    return next.handle(req).pipe(finalize(() => this.loaderService.hide()));
  }
}
