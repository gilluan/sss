import {ActionBarRef} from '@sicoob/ui';

export abstract class ActionBarComponentBase {

  protected constructor(protected readonly actionBarRef: ActionBarRef) { }

  closeActionBar = (data: any = {}): void => this.actionBarRef.close(data);
}
