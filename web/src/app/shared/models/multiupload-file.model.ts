import { SinsFileInProgressStates } from "../components/sins-multiupload/sins-file-system-file-entry.model";

export class MultiUploadFile {
  constructor(
    public nome?: string,
    public tamanho?: number,
    public identificadorGed?: string,
    public progress?: number,
    public estado?: SinsFileInProgressStates,
    public key?: string,
    public contentType?: string,
    public error?: boolean
  ) { }

}
