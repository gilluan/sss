import {PageModelVm} from "./page.model";

export class VoluntarioFiltroDto {
  constructor(public nome?: string,
    public situacao?: string,
    public cooperativas?: string[],
    public ativo?: Boolean,
    public paginacao?: PageModelVm) {}
}
