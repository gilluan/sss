import { Game } from "./game.model";

export class UsuarioInstituto {

  constructor() {

  }

  login: string;
  nome: string;
  cpf: string;
  email: string;
  numeroCooperativa: string;
  idInstituicaoOrigem: number;
  idUnidadeInstOrigem: number;
  foto: string;
  perfil: string;
  isSigned: boolean;
  id: string;
  game: Game[];


  isPaeOuPde(): boolean {
    return ['pae', 'pde'].indexOf((this.perfil || '').toLowerCase()) > -1;
  }
}
