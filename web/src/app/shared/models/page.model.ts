export class PageModelVm {
  pageNumber: number;
  pageSize: number;
  totalPages: number;
  total: number;

  constructor(pageNumber: number, pageSize: number, total: number) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.totalPages = total > 0 ? Math.ceil(total / pageSize) - 1 : 0;
    this.total = total;
  }

  isEOF = (): boolean => (this.pageNumber + 1) < this.totalPages;
}

