import { BaseDto } from "@app/shared/models/base-dto.model";


export class HorasVoluntario extends BaseDto {
  horas: number;
  programa: any;
  responsavel: any;
  voluntario: any;
  outros: boolean;
}
