import { BaseDto } from "./base-dto.model";
import { DadosTermo } from "@app/funcionalidades/termo-voluntario/models/dados-termo.model";
import { VoluntarioTipo } from "@app/funcionalidades/termo-voluntario/models/voluntario-tipo.enum";
import { Game } from "./game.model";

export class VoluntarioDto extends BaseDto {
  voluntarioTipo: VoluntarioTipo;
  idPerfil: string;
  codigoVoluntario: number;
  genero: string;
  loginVoluntario: string;
  nome: string;
  rg: string;
  cpf: string;
  dataNascimento: Date;
  nacionalidade: string;
  naturalidade: string;
  profissao: string;
  telefone: string;
  email: string;
  endCep: string;
  endLogradouro: string;
  endNumero: string;
  endBairro: string;
  endCidade: string;
  endUf: string;
  idDocumentoCertificado: string;
  situacao: string;
  horas: number;
  projetos: number;
  ativo: boolean = true;
  numeroCooperativa: string;
  dadosTermo: DadosTermo;
  game: Game[]

}
