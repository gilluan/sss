export class RotaModel {
  constructor(
    public displayName: string,
    public iconName: string,
    public route: string,
    public disabled?: boolean,
    public children?: RotaModel[]) { };
}
