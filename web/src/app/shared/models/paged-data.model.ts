import {PageModelVm} from './page.model';

export class PagedDataModelVm {
  page: PageModelVm;
  data: any[];

  constructor(page: PageModelVm, data: any[]) {
    this.page = page;
    this.data = data;
  }
}

export class PagedData<T> {

  constructor(public page: PageModelVm, public data: T[]) { }
}

