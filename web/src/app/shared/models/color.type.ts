export enum Colors {
  default = '#C4C4C4',
  primary = '#003641',
  active = '#9B51E0',
  danger = '#EB5757',
  success = '#00D55C',
  warning = "#f39c12",
}
