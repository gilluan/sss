import { PerfilType } from '../types/perfil.type';

export enum SinsPermissions {
  inicio = 'inicio',
  adicionarCurso = 'adicionarCurso',
  atualizarPerfil = 'atualizarPerfil',
  manterPlanoAcao = 'manterPlanoAcao',
  manterAcao = 'manterAcao',
  executarAcao = 'executarAcao',
  manterFluxo = 'manterFluxo',
  manterProjeto = 'manterProjeto',
  manterTarefa = 'manterTarefa',
  listarVoluntario = 'listarVoluntario',
  manterAnalise = 'manterAnalise',
  filtrarPlanoAcao = 'filtrarPlanoAcao',
  assinarVoluntario = 'assinarVoluntario',
  manterTermo = 'manterTermo',
}

export const roles = [
  {
    permissions: [SinsPermissions.adicionarCurso, SinsPermissions.atualizarPerfil],
    role: 'VOLUNTARIO',
    rotas: [
      'inicio',
      'acompanhamento-cadastro',
      'welcome/dados-cadastrais',
      'welcome/termo-voluntario',
      'welcome',
      'perfil-usuario/:id',
      'projetos'
    ]
  }, {
    permissions: [
      SinsPermissions.manterPlanoAcao,
      SinsPermissions.manterAcao,
      SinsPermissions.executarAcao,
      SinsPermissions.manterFluxo,
      SinsPermissions.manterProjeto,
      SinsPermissions.manterTarefa,
      SinsPermissions.listarVoluntario
    ],
    role: 'PAE',
    rotas: [
      'inicio',
      'plano-acao',
      'plano-acao/:id',
      'welcome',
      'welcome/termo-voluntario',
      'analise-cadastros',
      'analise-cadastros/:cpf',
      'relatorios',
      'relatorios/banco-voluntarios',
      'perfil-usuario/:id',
      'projetos'
    ]
  }, {
    permissions: [
      SinsPermissions.manterPlanoAcao,
      SinsPermissions.manterAcao,
      SinsPermissions.executarAcao,
      SinsPermissions.manterFluxo,
      SinsPermissions.manterProjeto,
      SinsPermissions.manterTarefa,
      SinsPermissions.listarVoluntario
    ],
    role: 'PDE',
    rotas: [
      'inicio',
      'plano-acao',
      'plano-acao/:id',
      'welcome',
      'welcome/termo-voluntario',
      'analise-cadastros',
      'analise-cadastros/:cpf',
      'relatorios',
      'relatorios/banco-voluntarios',
      'perfil-usuario/:id',
      'projetos'
    ]
  }, {
    permissions: [
      SinsPermissions.manterPlanoAcao,
      SinsPermissions.manterAcao,
      SinsPermissions.executarAcao,
      SinsPermissions.manterFluxo,
      SinsPermissions.manterProjeto,
      SinsPermissions.manterTarefa,
      SinsPermissions.listarVoluntario,
      SinsPermissions.manterAnalise,
      SinsPermissions.filtrarPlanoAcao
    ],
    role: 'PPE',
    rotas: [
      'inicio',
      'plano-acao',
      'plano-acao/:id',
      'analise-cadastros',
      'analise-cadastros/:cpf',
      'relatorios',
      'relatorios/banco-voluntarios',
      'fluxo-processo',
      'fluxo-processo/:id',
      'fluxo-processo/:id/novaAcao',
      'perfil-usuario/:id',
      'projetos',
      'programas',
      'programas/:id',
      'portfolios']
  }, {
    permissions: [
      SinsPermissions.assinarVoluntario,
      SinsPermissions.manterTermo
    ],
    role: 'GESTOR',
    rotas: [
      'inicio',
      'plano-acao',
      'plano-acao/:id',
      'analise-cadastros',
      'analise-cadastros/assinatura',
      'relatorios',
      'relatorios/banco-voluntarios',
      'configuracao-termo',
      'perfil-usuario/:id',
      'projetos',
      'programas',
      'programas/:id',
      'portfolios'
    ]
  },
  {
    permissions: [],
    role: 'CEO',
    rotas: []
  }
];

export class RoleObject {
  constructor(public role: { [name: string]: string[] }, public permissions: string[], public rotas?: string[]) { }
}

export class RoleFactory {

  static getRole(perfil: PerfilType): RoleObject {
    let retorno: RoleObject;
    PerfilType.values().filter(
      type => {
        if (type == perfil) {
          roles.filter(
            r => {
              if (r.role.toLowerCase() == type) {
                const role = {};
                role[r.role] = r.permissions;
                retorno = new RoleObject(role, r.permissions, r.rotas);
              }
            }
          );
        }
      });
    return retorno;
  }

}
