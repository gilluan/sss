export enum RotasType {
  planoAcao = 'plano-acao',
  inicio = 'inicio',
  analiseCadastroAssinatura = 'analise-cadastros/assinatura',
  analiseCadastro = 'analise-cadastros',
  relatorios = 'relatorios',
  configuracaoTermo = 'configuracao-termo',
  acompanhamentoCadastro = 'acompanhamento-cadastro',
}
