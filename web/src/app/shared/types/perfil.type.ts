export enum PerfilType {
  ceo = 'ceo',
  gestor = 'gestor',
  ppe = 'ppe',
  pde = 'pde',
  pae = 'pae',
  voluntario = 'voluntario',
}

export namespace PerfilType {

  export const valueOf = (perfilStr: string): PerfilType => PerfilType.values().filter(f => f === perfilStr)[0];

  export function getPerfisParaAprovacao(perfilResponsavel: PerfilType): PerfilType[] {
    return valuesResponsavel().splice(0, valuesResponsavel().indexOf(perfilResponsavel));
  }

  export function values(): PerfilType[] {
    return [
      PerfilType.ceo,
      PerfilType.gestor,
      PerfilType.ppe,
      PerfilType.pde,
      PerfilType.pae,
      PerfilType.voluntario,
    ];
  }

  export function valuesResponsavel(): PerfilType[] {
    return [
      PerfilType.ppe,
      PerfilType.pde,
      PerfilType.pae,
      PerfilType.voluntario,
    ];
  }

  export function valuesAprovador(): PerfilType[] {
    return [
      PerfilType.ppe,
      PerfilType.pde,
      PerfilType.pae,
    ];
  }

  export function obterPerfil(perfil: string): string {
    switch (perfil) {
      case PerfilType.ppe:
        return 'Perfil PPE';
      case PerfilType.gestor:
        return 'Gestor';
      case PerfilType.pae:
        return 'Perfil PAE';
      case PerfilType.pde:
        return 'Perfil PDE';
      case PerfilType.voluntario:
        return 'Voluntário';
      case PerfilType.ceo:
        return 'CEO';
    }
  }

  export function obterPerfilEnum(perfil: string): PerfilType {
    switch (perfil) {
      case 'Perfil PPE':
        return PerfilType.ppe;
      case 'Gestor':
        return PerfilType.gestor;
      case 'Perfil PAE':
        return PerfilType.pae;
      case 'Perfil PDE':
        return PerfilType.pde;
      case 'Voluntário':
        return PerfilType.voluntario;
      case 'CEO':
        return PerfilType.ceo;
    }
  }
}



