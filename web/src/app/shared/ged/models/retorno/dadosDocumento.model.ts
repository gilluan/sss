import { ChaveDocumentoRetorno } from './chaveDocumentoRetorno.model';
import { SequenciaisDocumento } from './sequenciaisDocumentoRetorno.model';

export class DadosDocumento {
  idDocumento: number;
  idTipoDocumento: number;
  nomeTipoDocumento: string;
  siglaTipoDocumento: string;
  idUsuarioInclusao: string;
  dataHoraInclusao: string;
  descricaoLocalArmazenamento: string;
  codigoLocalArmazenamento: number;
  listaChaveDocumento: ChaveDocumentoRetorno[];
  listaSequenciaisDocumento: SequenciaisDocumento[];
}
