export class ChaveDocumentoRetorno {
  idDocumento: number;
  nomeChaveDocumento: string;
  siglaChaveDocumento: string;
  descricaoTipoDado: string;
  valorChave: string;
}
