import { Extensao } from "./extensao.model";

export class SequenciaisDocumento {
  idSequencialDocumento: number;
  arquivoCodificadoBase64: string;
  extensao: Extensao;
}
