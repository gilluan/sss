export enum TipoDocumentoGed {
  fluxoProcesso = 'FLUXO.PROCESSO',
  fluxoPassos = 'FLUXO.PASSOS',
}

export namespace TipoDocumentoGed {
  export function values(): TipoDocumentoGed[] {
    return [
      TipoDocumentoGed.fluxoPassos,
      TipoDocumentoGed.fluxoProcesso
    ];
  }
}
