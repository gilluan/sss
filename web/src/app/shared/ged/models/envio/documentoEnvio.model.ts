import { Documento } from "./documento.model";

export class DocumentoEnvio {
  siglaTipoDocumento: string;
  listaDocumento: Documento[];
}
