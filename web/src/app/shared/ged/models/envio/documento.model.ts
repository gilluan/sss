import { ValorChaveEnvio } from "./valorChaveEnvio.model";
import { SequenciaDocumento } from "./sequenciaDocumento.model"

export class Documento {
  idModulo: string;
	idSistema: number;
	idUsuarioInclusao: string;
	idInstituicao: number;
	idUnidadeInstituicao: number;
  listaValorChave: ValorChaveEnvio[];
  listaSequencialDocumento: SequenciaDocumento[];

}
