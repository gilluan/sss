import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DocumentoRetorno } from './models/retorno/documentoRetorno.model';
import { catchError, map, tap, last, switchMap } from 'rxjs/operators';
import { DocumentoEnvio } from './models/envio/documentoEnvio.model';
import { Observable, of, Subscription } from 'rxjs';
import { Service } from '../services/service';
import { IdentificadorDocumentoRetorno } from './models/retorno/identificadorDocumentoRetorno.model';
import { TipoDocumentoGed } from './models/tipo-arquivo-ged.enum';
import { UsuarioInstituto } from '../models/usuario-instituto.model';
import { Documento } from './models/envio/documento.model';
import { SequenciaDocumento } from './models/envio/sequenciaDocumento.model';
import { ValorChaveEnvio } from './models/envio/valorChaveEnvio.model';
import { MessageService } from '../services/message.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class GedService extends Service {

  private RESOURCE = 'documentos';
  private RESOURCE_URL: string = environment.url_api_ged;

  constructor(private http: HttpClient, private messageService: MessageService) { super(); }

  /**
   *
   * @param documento
    <h1>Exemplo de Implementação</h1>
    let valorChave: ValorChaveEnvio = new ValorChaveEnvio();
    valorChave.siglaChaveDocumento = "161";
    valorChave.valorChave = "1234";

    let sequencialDocumentoDTO: SequenciaDocumentoDTO = new SequenciaDocumentoDTO();
    sequencialDocumentoDTO.descricaoExtensao = "JPG";
    sequencialDocumentoDTO.valorBinario = [1, 2];

    let documento: Documento = new Documento();
    documento.idInstituicao = 2;
    documento.idModulo = 1;
    documento.idSistema = 60;
    documento.idUnidadeInstituicao = 0;
    documento.idUsuarioInclusao = "adriano.dantas";
    documento.listaSequencialDocumentoDTO = [sequencialDocumentoDTO];
    documento.listaValorChave = [valorChave];

    this.documentoEnvio = new DocumentoEnvio();
    this.documentoEnvio.siglaTipoDocumento = "DIG.C.END";

    this.documentoEnvio.listaDocumentoDTO = [documento];
   */
  salvarDocumento2(documento: DocumentoEnvio) {
    const url = `${this.RESOURCE_URL}/${this.RESOURCE}`;
    const req = new HttpRequest('POST', url, documento, {
      reportProgress: true,
    });
   return this.http.request(req).pipe(
      catchError(this.handleError));
  }


  salvarDocumento(documento: DocumentoEnvio): Observable<IdentificadorDocumentoRetorno> {
    const url = `${this.RESOURCE_URL}/${this.RESOURCE}`;
    return this.http.post<IdentificadorDocumentoRetorno>(url, documento, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }





  /**
   <h1>Exemplo de retorno</h1>
   let listaChaveDocumento = new ChaveDocumentoRetorno();
   listaChaveDocumento.idDocumento = 0;
   listaChaveDocumento.nomeChaveDocumento = "0";
   listaChaveDocumento.siglaChaveDocumento = "0";
   listaChaveDocumento.descricaoTipoDado = "0";
   listaChaveDocumento.valorChave = "0";

   let extensao = new Extensao();
   extensao.descricaoExtensao = "";
   extensao.idExtensao = 0;

   let listaSequencialDocumento = new SequenciaisDocumento();
   listaSequencialDocumento.idSequencialDocumento = 0;
   listaSequencialDocumento.extensao = extensao;
   listaSequencialDocumento.arquivoCodificadoBase64 = "0";

   let resultado = new DadosDocumento();
   resultado.idDocumento = 0;
   resultado.idTipoDocumento = 0;
   resultado.nomeTipoDocumento = "0";
   resultado.siglaTipoDocumento = "0";
   resultado.idUsuarioInclusao = '0';
   resultado.dataHoraInclusao = "0";
   resultado.descricaoLocalArmazenamento = "0";
   resultado.codigoLocalArmazenamento = 0;
   resultado.listaChaveDocumento = [listaChaveDocumento];
   resultado.listaSequenciaisDocumento = [listaSequencialDocumento];


   this.documentoRetorno = new DocumentoRetorno();
   this.documentoRetorno.resultado = resultado;

   @param idDocumento
   */
  pesquisarDocumento(idDocumento: string) {
    const url = `${this.RESOURCE_URL}/${this.RESOURCE}/${idDocumento}`;
    return this.http.get<DocumentoRetorno>(url, httpOptions).pipe(catchError(this.handleError));
  }

  criarDocumentoEnvio(binario: number[], tipoDocumento: TipoDocumentoGed, usuarioInstituto: any): DocumentoEnvio {
    const documento = this.buildDocumento(binario, usuarioInstituto, tipoDocumento);
    const documentoEnvio: DocumentoEnvio = new DocumentoEnvio();
    documentoEnvio.siglaTipoDocumento = tipoDocumento;
    documentoEnvio.listaDocumento = [documento];
    return documentoEnvio;
  }

  private buildDocumento(binario: number[], usuarioInstituto: any, tipoDocumento: TipoDocumentoGed): Documento {
    const documento: Documento = new Documento();
    documento.idInstituicao = usuarioInstituto.idInstituicaoOrigem;
    documento.idUnidadeInstituicao = usuarioInstituto.idUnidadeInstOrigem;
    documento.idUsuarioInclusao = usuarioInstituto.cpf;
    documento.idModulo = '1';
    documento.idSistema = 60;
    documento.listaSequencialDocumento = [this.buildSequenciaDocumento(binario, tipoDocumento)];
    documento.listaValorChave = [this.buildValorChave()];
    return documento;
  }

  private buildSequenciaDocumento(binario: number[], tipoDocumento: TipoDocumentoGed): SequenciaDocumento {
    const sequecialDocumento: SequenciaDocumento = new SequenciaDocumento();
    sequecialDocumento.descricaoExtensao = tipoDocumento == TipoDocumentoGed.fluxoProcesso ? 'XML' : 'PDF';
    sequecialDocumento.valorBinario = binario;
    return sequecialDocumento;
  }

  private buildValorChave(): ValorChaveEnvio {
    const valorChave: ValorChaveEnvio = new ValorChaveEnvio();
    valorChave.siglaChaveDocumento = '161';
    valorChave.valorChave = '1234';
    return valorChave;
  }

}
