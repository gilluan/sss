import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { Service } from '@app/shared/services/service';
import { Validations } from '@app/shared/utils/validations';
import { HorasVoluntario } from '@shared/models/horas-voluntario.model';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PageModelVm } from '../models/page.model';

const notNull = Validations.notNull;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class HorasVoluntarioService extends Service {


  constructor(private http: HttpClient) {
    super();
  }

  RESOURCE = 'horas-voluntario';
  RESOURCE_URL = `${environment.modulo_gestao_pessoa}/${this.RESOURCE}`;

  alterarHorasVoluntario(horasVoluntario: HorasVoluntario): Observable<HorasVoluntario> {
    return this.http.put<{ resultado: HorasVoluntario }>(this.RESOURCE_URL, horasVoluntario, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  salvarHorasVoluntario(horasVoluntario: HorasVoluntario): Observable<HorasVoluntario> {
    return this.http.post<{ resultado: HorasVoluntario }>(this.RESOURCE_URL, horasVoluntario, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  excluirHorasVoluntario(id: string): Observable<string> {
    return this.http.delete<any>(`${this.RESOURCE_URL}/${id}`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  carregaHorasVoluntarios(id: string, paginacao: PageModelVm): Observable<PagedDataModelVm> {
    let params = this.tratarPaginacao(paginacao);
    return this.http.get<{ resultado: PagedDataModelVm }>(`${this.RESOURCE_URL}/${id}`, { params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  carregaTotalHorasVoluntarios(id: string): Observable<number> {
    return this.http.get<{ resultado: number }>(`${this.RESOURCE_URL}/${id}/total`, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }


  tratarPaginacao = (paginacao: PageModelVm) => {
    let params = new HttpParams();
    notNull(paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('numeroPagina', pn));
      notNull(p.pageSize, ps => params = params.append('tamanhoPagina', ps));
    });
    return params;
  }


}
