import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioInstituto } from '../models/usuario-instituto.model';
import { environment } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Service } from './service';
import { PageModelVm } from '../models/page.model';
import { PerfilType } from '../types/perfil.type';
import { Game } from '../models/game.model';
import { UsuarioService } from './usuario.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class UsuarioInstitutoService extends UsuarioService {

  constructor(public http: HttpClient) {
    super(http, `${environment.modulo_gestao_pessoa}/usuarios-instituto`);
  }

  findByEmail(email: string): Observable<UsuarioInstituto> {
    return this.findAll(email, null, null).pipe(
      map(uss => uss[0])
    )
  }

  findByCpf(cpf: string): Observable<UsuarioInstituto> {
    return this.findAll(null, cpf, null).pipe(
      map(uss => uss[0])
    )
  }

  findAll(email: string, cpf: string, nome?: string, numeroCooperativa?: string, perfil?: PerfilType, identificador?: string): Observable<UsuarioInstituto[]> {
    const url = this.montarUrlLoadPlanos(this.RESOURCE_URL, email, cpf, nome, numeroCooperativa, perfil, identificador);
    return this.http.get<UsuarioInstituto[]>(url, httpOptions).pipe(catchError(this.handleError));
  }

  signByCpf(cpf: string): Observable<UsuarioInstituto> {
    return this.http.post<UsuarioInstituto>(`${this.RESOURCE_URL}/${cpf}/assinar`, {}, httpOptions)
      .pipe(catchError(this.handleError));
  }

  recuperarCooperativas(): Observable<string[]> {
    let paginacao = new PageModelVm(0, 500, 0);
    let params = new HttpParams();
    params = params.append('offset', `${paginacao.pageNumber}`);
    params = params.append('limit', `${paginacao.pageSize}`);
    params = params.append('campo', 'numeroCooperativa');
    return this.http.get<string[]>(`${this.RESOURCE_URL}`, { params: params }).pipe(
      catchError(this.handleError)
    );
  }


  private montarUrlLoadPlanos(url: string, email?: string, cpf?: string, nome?: string, numeroCooperativa?: string, perfil?: PerfilType, identificador?: string) {
    url = url + '?';
    nome = nome ? encodeURIComponent(nome) : nome;
    if (perfil) {
      url = url + `perfil=${perfil}&`;
    }
    if (email) {
      url = url + `email=${email}&`;
    }
    if (cpf) {
      url = url + `cpf=${cpf}&`;
    }
    if (nome) {
      url = url + `nome=${nome}&`;
    }
    if (identificador) {
      url = url + `identificador=${identificador}&`;
    }
    if (numeroCooperativa) {
      url = url + `numeroCooperativa=${numeroCooperativa}`;
    }
    return url;
  }

}
