import { Service } from "./service";
import { Observable } from "rxjs";
import { Game } from "../models/game.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { map, catchError } from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
export class UsuarioService extends Service {

  constructor(public http: HttpClient, public RESOURCE_URL) { super(); }

  findGameByCpf(cpf: string): Observable<Game[]> {
    const url = `${this.RESOURCE_URL}?cpf=${cpf}&campo=game`;
    return this.http.get<Array<Game[]>>(url, httpOptions).pipe(
      map((value: Array<Game[]>) =>
        value.length > 0 ? value[0] : null
      ),
      catchError(this.handleError)
    )
  }

}
