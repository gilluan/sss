import { AlertService, Color } from '@sicoob/ui';
import { Injectable } from '@angular/core'

@Injectable ({
  providedIn: 'root'
})
export class CustomAlertService {

  constructor(private alertService: AlertService) { }

  public abrirAlert(color: Color, message: string, duration?: number, icon?: string) {

    const alertConfig = {
      message: 'My alert message with <b>bold</b>',
      duration: 5000,
      color: Color.DEFAULT,
      icon: color === Color.SUCCESS ? "mdi mdi-checkbox-marked-circle" : "mdi mdi-alert"
    };
    
    alertConfig.icon = icon ? icon : alertConfig.icon;
    alertConfig.color = color ? color : alertConfig.color;
    alertConfig.duration = duration ? duration : alertConfig.duration;
    alertConfig.message = message;
    this.alertService.open(alertConfig);
    
  }
}
