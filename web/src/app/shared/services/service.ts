import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

export class Service {

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param error
   */
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Ocorreu um erro na nossa aplicação:', error.error.message);
    } else {
      console.error(
        `Backend retornou o seguinte erro ${error.status}, ` +
        `Corpo do erro: `, error.error);
    }
    const er = error.error && error.error.mensagens ? error.error.mensagens : 'Algo deu errado, por favor tente mais tarde.';
    return throwError(er[0].mensagem);
  }
}
