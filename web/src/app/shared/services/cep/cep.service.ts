import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Cep } from './cep.model';
import { environment } from 'src/environments/environment';
import { Service } from '../service';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CepService extends Service {

  private URL_CEP = environment.URL_CEP;

  constructor(private http: HttpClient) { super() }

  findCep(cep: string): Observable<Cep> {
    return this.http.get<Cep>(this.URL_CEP + cep, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
