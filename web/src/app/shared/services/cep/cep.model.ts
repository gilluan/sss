export class Cep {
  bairro: string;
  cep: string;
  cidade: string;
  complemento2: string;
  endereco: string;
  uf: string;
}
