import { Injectable } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';

@Injectable()
export class RouterService {

    private CurrentUrl: string;
    private PreviousUrl: string;

    constructor(private router: Router) {
        this.CurrentUrl = this.router.url;
        router.events.subscribe(event => {
            if(event instanceof RoutesRecognized){
                this.PreviousUrl = this.CurrentUrl;
                this.CurrentUrl = event.url;
            }
        });
     }

    public getPreviousUrl(): string{
        return this.PreviousUrl;
    }

}
