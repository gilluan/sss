import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { RubricaFiltro } from '../components/sc-lista-rubricas/rubrica-filtro.model';
import { PageModelVm } from '../models/page.model';
import { PagedDataModelVm } from '../models/paged-data.model';
import { Validations } from '../utils/validations';
import { Service } from './service';

const notNull = Validations.notNull;


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class RubricaService extends Service {

  private RESOURCE = 'rubricas';
  private RESOURCE_URL = `${environment.modulo_gestao_tarefa}/${this.RESOURCE}`;

  constructor(private http: HttpClient) { super(); }

  findAll(filtro: RubricaFiltro): Observable<PagedDataModelVm> {
    const params = this.buildParams(filtro);
    return this.http.get<{ resultado: PagedDataModelVm }>(this.RESOURCE_URL, { headers: httpOptions.headers, params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  private buildParams(filtro: RubricaFiltro): HttpParams {
    let params = new HttpParams();
    notNull(filtro.nome, n => params = params.append('nome', n));
    notNull(filtro.codigo, n => params = params.append('codigo', n));
    notNull(filtro.numeroCooperativa, r => params = params.append('numeroCooperativa', r));
    notNull(filtro.paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('offset', pn));
      notNull(p.pageSize, ps => params = params.append('limit', ps));
    });
    return params;
  }

}
