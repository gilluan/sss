export class Formatters {

  public dataAtualFormatada(date: Date) {
    let data = date,
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return diaF + "/" + mesF + "/" + anoF;
  }

  public static stringToDate: any = (date: string) => {
    let resultString = null;
    if (date) {
      if (date.length === 8)
        resultString = date.substring(4, 8) + '-' + date.substring(2, 4) + '-' + date.substring(0, 2) + 'T00:00:00';
      else if(date.length > 8)
        resultString = date.substring(6, 10) + '-' + date.substring(3, 5) + '-' + date.substring(0, 2) + 'T00:00:00';
    }
    if(resultString)
      return new Date(resultString);
    else
      resultString
  }


}
