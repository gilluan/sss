export class FileUtil {

  static async converterParaString(arquivo: Blob | File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      try {
        const reader: FileReader = new FileReader();
        reader.readAsText(arquivo);
        reader.onload = () => resolve(reader.result as string);
      } catch (e) {
        reject(e);
      }
    });
  }

  static async converterParaByteArray(arquivo: Blob | File): Promise<number[]> {
    return new Promise<number[]>((resolve, reject) => {
      try {
        const reader: FileReader = new FileReader();
        reader.readAsArrayBuffer(arquivo);
        reader.onload = () => resolve(Array.from(new Int8Array(reader.result as ArrayBuffer)));
      } catch (e) {
        reject(e);
      }
    });
  }

  static async converterParaDataURL(arquivo: Blob | File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      try {
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(arquivo);
        reader.onload = () => resolve(reader.result as string);
      } catch (e) {
        reject(e);
      }
    });
  }

  static converterParaBase64(buffer: number[]) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    for (let i = 0; i < bytes.byteLength; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  static base64ToArrayBuffer(base64) {
    const binary_string = window.atob(base64);
    const len = binary_string.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  static decodeBase64(arquivoBase64: string) {
    return decodeURIComponent(atob(arquivoBase64)
      .split('')
      .map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
      .join(''));
  }

  static BASE64_MARKER = ';base64,';

  static convertDataURIToBinary(dataURI) {
    let base64Index = dataURI.indexOf(FileUtil.BASE64_MARKER) + FileUtil.BASE64_MARKER.length;
    let base64 = dataURI.substring(base64Index);
    let raw = window.atob(base64);
    let rawLength = raw.length;
    let array = new Int8Array(new ArrayBuffer(rawLength));
    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

}
