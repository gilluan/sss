export enum DataUnit {
  B,
  KB,
  MB,
  GB,
  TB,
  PT
}

export namespace DataUnit {
  export function generate(value: number, unit: DataUnit = DataUnit.B): string {
    if (value > 1024) {
      return generate(value / 1024, unit++);
    }
    return `${value.toFixed(2)} ${getName(unit)}`;
  }

  export function getName(unit: DataUnit): string {
    switch (unit) {
      case DataUnit.B: return 'B';
      case DataUnit.KB: return 'KB';
      case DataUnit.MB: return 'MB';
      case DataUnit.GB: return 'GB';
      case DataUnit.TB: return 'TB';
      case DataUnit.PT: return 'PT';
    }
  }
}
