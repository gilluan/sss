import { FormControl, FormGroup } from "@angular/forms";

export class Validations {

  public static notNull(value: any, predicate: (value: any) => void) {
    if (value != null) {
      predicate(value);
    }
  }

  public static isNull(value: any, predicate: (value: any) => void) {
    if (value == null) {
      predicate(value);
    }
  }


}

export function nonZero(control: FormGroup) {
  if (Number(control.value) < 1) {
    return { nonZero: true };
  } else {
    return null;
  }
}

export function check_if_is_integer(value) {
  return ((parseFloat(value) == parseInt(value)) && !isNaN(value));
}

export function isInteger(control: FormControl) {
  return check_if_is_integer(control.value) ? null : {
    notNumeric: true
  }
}

export function validURL(str) {
  const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}
