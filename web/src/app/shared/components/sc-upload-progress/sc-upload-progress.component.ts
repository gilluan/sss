import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { MessageService } from '@app/shared/services/message.service';

@Component({
  selector: 'sc-upload-progress',
  template: `
    <sc-progress-bar [progress]="messageService.message" color="#EB5757">
    </sc-progress-bar>
  `
})
export class ScUploadProgressComponent implements OnInit {

  ngOnInit(): void {
    console.log(this.messageService.message);
  }

  constructor(public messageService: MessageService) { }

}
