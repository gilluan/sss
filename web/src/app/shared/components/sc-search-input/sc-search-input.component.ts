import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'sc-search-input',
  templateUrl: './sc-search-input.component.html',
  styleUrls: ['./sc-search-input.component.css']
})
export class ScSearchInputComponent {

  @Input() placeholder: string = 'Buscar';
  @Input() text: string;
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  @Output() clear: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  onSearch = ($event) => this.search.emit(this.text);

  onClear($event) {
    this.clear.emit($event);
    this.text = null;
  }

  isNotNullOrEmpty = () => this.text != null && this.text != '';

}
