import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-relatorio-card',
  template: `
    <div class="ss-card ss-mini-card {{classe}}"  (click)="event()" [routerLink]="rota">
      <div class="ss-mini-card-icon">
        <i class="mdi mdi-36px {{icone}}"></i>
      </div>
      <div class="ss-mini-card-text ajustar-texto">
        <strong>{{texto}}</strong>
        <br>
        <small>{{textoSmall}}</small>
      </div>
    </div>
  `,
  styleUrls: ['./relatorio-card.component.scss']
})
export class RelatorioCardComponent implements OnInit {

  @Input() icone: string;
  @Input() texto: string;
  @Input() rota:  string;
  @Input() textoSmall: string;
  @Output() clicar: EventEmitter<any> = new EventEmitter<any>();
  @Input() classe: string;

  event() {
    if (!this.rota) {
      this.clicar.emit();
    } else {
      this.router.navigate([this.rota]);
    }
  }

  constructor(private router: Router) { }

  ngOnInit() { }

}
