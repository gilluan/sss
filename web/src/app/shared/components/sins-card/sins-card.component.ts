import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemMenu } from '../menu-dropdown/model/item-menu.model';
import { PositionDotsType } from '../menu-dropdown/model/position-dots.type';
import { MenuDropDownComponent } from '../menu-dropdown/menu-dropdown.component';

@Component({
  selector: 'sins-card',
  templateUrl: './sins-card.component.html',
  styleUrls: ['./sins-card.component.scss']
})
export class SinsCardComponent implements OnInit {

  @Input() scTitle: string;
  @Input() scId: string;
  @Input() scEmptyCard: boolean;
  @Input() scItensMenu: Array<ItemMenu>;
  @Input() scMenuOrientation: 'vertical' | 'horizontal' = 'vertical';
  @Input() scType: 'select' | 'interact' = 'interact';
  @Input() selecionado: boolean = false;
  @Output() scClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() scMenuItemSelect: EventEmitter<ItemMenu> = new EventEmitter<ItemMenu>();

  posicao: PositionDotsType;
  menuPosition = '-20px';
  caretPosition = '28px';

  constructor() { }

  ngOnInit(): void {
    this.getMenuOrientation();
  }

  onClick() {
    if(this.isSelect) {
      this.selecionado = !this.selecionado;
      this.scClick.emit(this.scId);
    }
  }

  onMenuItemSelect(menu: MenuDropDownComponent, item: ItemMenu) {
    menu.hide();
    this.scMenuItemSelect.emit(item);
  }

  private getMenuOrientation() {
    switch (this.scMenuOrientation) {
      case 'horizontal':
        this.posicao = PositionDotsType.horizontal;
        break;
      case 'vertical':
        this.posicao = PositionDotsType.vertical;
        break;
    }
  }

  public isSelect() {
    return this.scType === 'select';
  }

  public isInteract() {
    return this.scType === 'interact';
  }

}
