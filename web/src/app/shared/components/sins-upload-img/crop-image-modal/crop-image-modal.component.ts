import { Component, OnInit, Inject } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';

@Component({
  selector: 'sins-crop-image-modal',
  templateUrl: './crop-image-modal.component.html',
  styleUrls: ['./crop-image-modal.component.css']
})
export class CropImageModalComponent implements OnInit {

  base64Img;
  croppedImage: any = '';

  constructor(
    public modalRef: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit(): void {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(this.data['imagem']);
    fileReader.onload = () => {
      this.base64Img = fileReader.result;
    };
  }

  imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.base64;
  }

  cortar(): void {
    this.modalRef.close({
      ...this.data, imagem: this.croppedImage
    });
  }
}
