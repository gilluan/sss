import {Component, DoCheck, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ModalRef, ModalService} from '@sicoob/ui';
import {CropImageModalComponent} from './crop-image-modal/crop-image-modal.component';

@Component({
    selector: 'sins-upload-img',
    styleUrls: ['./sins-upload-image.component.css'],
    templateUrl: './sins-upload-image.component.html',
})
export class SinsUploadImageComponent implements DoCheck {
  @ViewChild('uploadCircle') uploadCircle: any;
  @ViewChild('imgUp') imgUp: any;
  @ViewChild('file') fileElem: any;
  @ViewChild('imageCircle') imageCircle: any;

  @Input() scTexto: String;
  @Input() scPreloadImagem = '';

  @Output() public scFeedback: EventEmitter<any> = new EventEmitter();

  imageFile: any;
  imagem: File;
  uploaded: boolean;

  constructor(private modalService: ModalService) { }

  ngDoCheck(): void {
    if (this.scPreloadImagem && this.scPreloadImagem.length > 0) {
      this.imgUp.nativeElement.setAttribute('src', this.getBase64Img(this.scPreloadImagem));
      this.imageCircle.nativeElement.setAttribute('style', 'display: block');
      this.uploaded = true;
      this.imgUp.nativeElement.setAttribute('style', '');
      this.uploadCircle.nativeElement.setAttribute('style', 'display: none;');
    }
  }

  uploadImage(event: Event) {
    this.imagem = event.target['files'][0];
    const fileReader = new FileReader();
    fileReader.onloadend = (evt: Event) => {
      this.imgUp.nativeElement.setAttribute('src', evt.target['result']);
      this.imageCircle.nativeElement.setAttribute('style', 'display: block');
      this.uploaded = true;
      this.imgUp.nativeElement.setAttribute('style', '');
      this.uploadCircle.nativeElement.setAttribute('style', 'display: none;');
      const modalRef: ModalRef = this.modalService.open(CropImageModalComponent, {
        data: {
          imagem: this.imagem
        }
      });

      modalRef.afterClosed().subscribe((result: any) => {
        const retorno: File = this.dataURLtoFile(result.imagem, Math.random().toString(36).substring(7));
        this.imgUp.nativeElement.setAttribute('src', result.imagem);

        this.scFeedback.emit(retorno);
      });

    };
    fileReader.readAsDataURL(this.imagem);

  }

    private dataURLtoFile(dataurl, filename): File {
        let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

  private getBase64Img(base64Img: string) {
    let src = 'data:image/jpeg;base64,';
    if (base64Img.length > 0) {
      src = `${src}${base64Img}`;
    }
    return src;
  }

  clicado() {
    this.fileElem.nativeElement.click();
  }
}
