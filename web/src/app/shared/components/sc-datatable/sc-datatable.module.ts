import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScDatatableComponent} from "./sc-datatable.component";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

@NgModule({
  declarations: [ ScDatatableComponent ],
  imports: [ CommonModule, NgxDatatableModule ],
  exports: [ ScDatatableComponent ]
})
export class ScDatatableModule { }
