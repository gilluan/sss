import {Component, EventEmitter, Input, Output, ChangeDetectorRef} from '@angular/core';
import {PageModelVm} from "../../models/page.model";

@Component({
  selector: 'sc-datatable',
  template: `
    <ngx-datatable
      class="material"
      [columns]="columns"
      [columnMode]="columnMode"
      [rows]="getItems()"
      [headerHeight]="54"
      [rowHeight]="'auto'"
      [footerHeight]="50"
      [externalPaging]="true"
      [reorderable]="false"
      [count]="page$?.total"
      [offset]="page$?.pageNumber"
      [limit]="page$?.pageSize"
      [messages]="messages"
      [externalSorting]="true"
      (page)='setPage($event)'>
    </ngx-datatable>
  `,
  styleUrls: ['./sc-datatable.component.scss']
})
export class ScDatatableComponent {
  @Input() page$: PageModelVm;
  @Input() items$: any[];
  @Input() columns: any[];
  @Input() columnMode: string = 'force';
  @Input() messages:any = { emptyMessage: 'Não há itens', totalMessage: ' itens' };
  @Output() changePage: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  getItems(){
    return this.items$ = [...this.items$];
  }

   setPage(pageInfo) {
    this.changePage.emit(pageInfo.offset);
  }
}
