import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScDatatableComponent } from './sc-datatable.component';

describe('ScDatatableComponent', () => {
  let component: ScDatatableComponent;
  let fixture: ComponentFixture<ScDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
