import { NgModule } from '@angular/core';
import { SinsBadgeDirective } from './sins-badge.component';
import { style } from '@angular/animations';

@NgModule({
  declarations: [SinsBadgeDirective],
  exports: [SinsBadgeDirective],
})
export class SinsBadgeModule {}