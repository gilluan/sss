import { Directive, ElementRef, Input, HostBinding, AfterViewChecked, ChangeDetectorRef, OnInit } from '@angular/core';
 
@Directive({
    selector: '[scBadge]',
})
export class SinsBadgeDirective implements AfterViewChecked, OnInit {
 
@HostBinding('class')
elementClass = "ss-badge small";

/** default, primary, success, warning, danger and active **/
@Input() color:string = "default";

constructor(
    private _elemento: ElementRef,
    private changeDetector: ChangeDetectorRef) {}
 
ngOnInit(){
    this.changeDetector.detectChanges();
}

public ngAfterViewChecked() {
    this.styleChange();
}
 
private styleChange() {
    let value:number = this._elemento.nativeElement.innerHTML;
    if(value >= 10 && value <= 99){
        this.elementClass = `ss-badge medium ${this.color}`;
    } else if(value > 99){
        this.elementClass = `ss-badge large ${this.color}`;
    }else{
        this.elementClass = `ss-badge small ${this.color}`
    }
}
 
}