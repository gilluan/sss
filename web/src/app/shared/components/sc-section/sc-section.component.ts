import {Component, Input} from '@angular/core';

@Component({
  selector: 'sc-section',
  template: `
    <div class="section-header">{{title}}</div>
    <div class="section-body">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./sc-section.component.scss']
})
export class ScSectionComponent {
  @Input() title: string;

  constructor() { }
}
