import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScSectionComponent} from "./sc-section.component";

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ScSectionComponent ],
  exports: [ ScSectionComponent ]
})
export class ScSectionModule { }
