import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';

@Component({
  selector: 'app-dialogo-justificativa-ajustes',
  templateUrl: './dialogo-justificativa-ajustes.component.html',
  styleUrls: ['./dialogo-justificativa-ajustes.component.scss']
})
export class DialogoJustificativaAjustesComponent implements OnInit {

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  label: string = 'Descreva abaixo o que o voluntário deve ajustar para ser aceito:';
  title: string = "Justificar pedido de ajuste";

  justificativaForm = this.fb.group({
    justificativaAjusteText: ['', Validators.required]
  });

  ngOnInit() {
    if(this.data.label)
      this.label = this.data.label
    if(this.data.title)
      this.title = this.data.title
  }

  closeModal(solicitar: boolean) {
    if(solicitar)
      this.ref.close({sucesso: solicitar, justificativaAjusteText: this.justificativaForm.value.justificativaAjusteText});
    else
      this.ref.close({sucesso: solicitar, justificativaAjusteText: null});
  }


}
