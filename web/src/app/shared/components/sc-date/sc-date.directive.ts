import { Directive, Input, ElementRef, OnInit, Renderer2, ContentChild, HostListener } from '@angular/core';

@Directive({
  selector: '[scDate]',
})
export class ScDateDirective implements OnInit {

  @Input() date: any;

  constructor(
    private el: ElementRef,
    private renderer2: Renderer2
  ) { }

  ngOnInit() {
     console.log(this.date)
  }

}
