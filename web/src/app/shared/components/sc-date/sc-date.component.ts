import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'sc-date',
  template: `
  <div class="ss-form-group">
    <input  style="height: auto;padding: 5px;" type="text"   class="ss-form-control" mask="00/00/0000" pattern="\d{1,2}\d{1,2}\d{4}" >
    <small scDate></small>
  </div>`,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScDateComponent),
      multi: true
    }
  ],
  styleUrls: ['./sc-date.component.scss']
})
export class ScDateComponent implements ControlValueAccessor {

  writeValue(obj: any): void {
    if (obj)
      this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onTouched: any = () => { };
  onChange: any = () => { };

  value: string;

}
