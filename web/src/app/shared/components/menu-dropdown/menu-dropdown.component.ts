import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PositionDotsType } from './model/position-dots.type';
import { ItemMenu } from './model/item-menu.model';

@Component({
  selector: 'sins-menu-dropdown',
  templateUrl: './menu-dropdown.component.html',
  styleUrls: ['./menu-dropdown.component.scss']
})
export class MenuDropDownComponent implements OnInit {

  visible = false;

  @Input() caretPosition = '128px';
  @Input() menuPosition = '-120px';
  @Input() listMenu: Array<ItemMenu> = null;


  @Output() onShow: EventEmitter<boolean>;
  @Output() onHidden: EventEmitter<boolean>;
  @Output() onToggle: EventEmitter<boolean>;
  @Output() onAction: EventEmitter<any> = new EventEmitter<any>();


  private _position: PositionDotsType.vertical | PositionDotsType.horizontal;

  toogle = (_) => this.visible = !this.visible;

  hide = () => this.visible = false;

  show = () => this.visible = true;

  @Input()
  set position(position: PositionDotsType) {
    if (position && position.trim()) {
      const exist: PositionDotsType[] = PositionDotsType.values().filter(p => p == position);
      if (exist && exist.length > 0) {
        this._position = position;
      }
    }
  }

  get position(): PositionDotsType { return this._position; }

  constructor() { }

  ngOnInit() { }

  onClickedOutside(e: Event) {
    this.hide();
  }

  action($entity, $event) {
    this.onAction.emit($entity);
  }

}
