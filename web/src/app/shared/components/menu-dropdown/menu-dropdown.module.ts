import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuDropDownComponent} from "./menu-dropdown.component";
import {FormsModule} from "@angular/forms";
import { RouterModule } from '@angular/router';
import { ClickOutsideModule } from 'src/app/shared/directive/click-outside/click-outside.module';

@NgModule({
  imports: [ RouterModule, CommonModule, FormsModule, ClickOutsideModule ],
  declarations: [ MenuDropDownComponent ],
  exports: [ MenuDropDownComponent ]
})
export class MenuDropDownModule { }
