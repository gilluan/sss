export enum PositionDotsType {
  vertical = "mdi-dots-vertical",
  horizontal = "mdi-dots-horizontal",

}

export namespace PositionDotsType {
  export function values(): PositionDotsType[] {
    return [
      PositionDotsType.vertical,
      PositionDotsType.horizontal
    ]
  }
}
