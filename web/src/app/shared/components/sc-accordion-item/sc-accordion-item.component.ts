import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {CdkAccordionItem} from '@angular/cdk/accordion';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'sc-accordion-item',
  template: `
    <div class="accordion-header" id="toggle" (click)="toggle()">
      <span>{{scTitle}}</span>
      <i class="mdi mdi-chevron-down" [@toggle]="iconState()"></i>
    </div>
    <div class="accordion-content" [@toggle]="expansionState()">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./sc-accordion-item.component.scss'],
  animations: [
    trigger('toggle', [
      state('toggleOn', style({ display: 'block', opacity: 1, height: '*', overflow: 'initial' })),
      state('toggleOff', style({ display: 'none', opacity: 0, height: '0', overflow: 'hidden' })),
      state('turnIconUp', style({ transform: 'rotate(180deg)' })),
      state('turnIconDown', style({ transform: 'rotate(0)' })),
      transition('toggleOn <=> toggleOff', [ animate('0.3s') ]),
      transition('turnIconUp <=> turnIconDown', [ animate('0.3s') ]),
    ])
  ]
})
export class ScAccordionItemComponent extends CdkAccordionItem implements OnInit {

  @Input() scTitle: string;
  @Input() scContentTemplate: TemplateRef<any>;
  @Input() scInitOpen = false;

  ngOnInit() {
    if (this.scInitOpen) {
      this.open();
    }
  }

  expansionState(): string {
    return this.expanded ? 'toggleOn' : 'toggleOff';
  }

  iconState(): string {
    return this.expanded ? 'turnIconUp' : 'turnIconDown';
  }

  contentState(): string {
    return this.expanded ? 'contentOn' : 'contentOff';
  }
}
