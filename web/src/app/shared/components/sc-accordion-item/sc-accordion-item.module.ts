import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScAccordionItemComponent} from "./sc-accordion-item.component";

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ScAccordionItemComponent ],
  exports: [ ScAccordionItemComponent ]
})
export class ScAccordionItemModule { }
