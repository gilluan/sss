import { Component, Input } from "@angular/core";

@Component({
  selector: 'gold-coin',
  template: `
  <div class="coin">
    <div class="front jump">
      <div class="star">
        <span class="currency">$</span>
      </div>
      <div class="shapes">
        <span class="top"></span>
        <span class="bottom">{{value}}</span>
      </div>
    </div>
    <div class="shadow"></div>
  </div>
  `,
  styleUrls: ['./gold-coin.component.scss']
})
export class GoldCoinComponent {

  @Input() value: string = '0';

}
