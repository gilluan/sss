import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sc-banner',
  template: `
  <div class="banner {{tema}}"  [ngClass]="{ 'banner-clickable': this.isScClickUsed }">
    <div style="display: flex;width: 200px;">
        <i style="font-size: 32px;margin-top: 10px;" title="Voltar" (click)="onClick()" class="mdi mdi-chevron-left"></i>
        <div *ngIf="!image64" class="banner-img-blank"></div>
        <img *ngIf="image64" class="banner-img" [src]="convertImage64(this.image64)" alt="">
        <div class="banner-title-container">
            <span [title]="title" class="banner-title">{{title}}</span>
            <span class="banner-subtitle">{{subtitle}}</span>
        </div>
    </div>
    <ng-content #name></ng-content>
  </div>
  `,
  styleUrls: ['./sc-banner.component.scss']
})
export class ScBannerComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() image64: string;
  @Input() tema: BannerTemas = BannerTemas.default;

  @Output() scClick: EventEmitter<any> = new EventEmitter<any>();

  isScClickUsed: boolean;

  constructor() { }

  ngOnInit(): void {
    this.handleClickEventUsage();
  }

  convertImage64 = (base64: string) => `data:image/jpeg;base64,${base64}`;

  onClick = (): void => this.scClick.emit();

  /**
   * Verifica se o evento de click foi implementado verificando se existe algum observer para o evento de click.
   */
  private handleClickEventUsage(): void {
    try {
      this.isScClickUsed = this.scClick.observers.length > 0;
    } catch (e) {
      console.error('Não foi possível determinar se o evento banner foi implementado.', e);
    }
  }
}


export enum BannerTemas  {
    info = 'sc-banner-info',
    success = 'sc-banner-success',
    default = 'sc-banner-default',
}
