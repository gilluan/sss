import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'sc-list-option',
  exportAs: 'scistOption',
  templateUrl: 'sc-list-option.component.html',
  styleUrls: ['./sc-list-option.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScListOption),
      multi: true
    }
  ]
})
export class ScListOption implements ControlValueAccessor {

  @ViewChild('text') _text: ElementRef;

  @Output() select: EventEmitter<boolean> = new EventEmitter();

  checked: boolean;

  constructor(private readonly changeDetector: ChangeDetectorRef) { }

  public onSelect(event) {
    this.checked = event.target.checked;
    this.propagateChange(this.checked);
    this.select.emit(this.checked);
  };

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  writeValue(obj: any): void {
    if ((obj !== undefined) && (obj !== null)) {
      this.checked = obj;
      this.changeDetector.detectChanges();
    }
  }

  propagateChange = (_: any) => {};
  onTouchedCallback: () => {};
}
