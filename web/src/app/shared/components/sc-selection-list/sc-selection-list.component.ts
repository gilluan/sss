
import {
  Component
} from '@angular/core';

import { ViewEncapsulation, ChangeDetectionStrategy } from '@angular/compiler/src/core';

@Component({
  selector: 'sc-selection-list',
  exportAs: 'scSelectionList',
  template: '<ng-content></ng-content>',
  styleUrls: ['sc-selection-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScSelectionListComponent   {

}

