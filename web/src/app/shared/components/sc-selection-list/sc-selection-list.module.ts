import {NgModule} from '@angular/core';
import {CommonModule, NgClass} from '@angular/common';
import { ScSelectionListComponent} from "./sc-selection-list.component";
import {FormsModule} from "@angular/forms";
import { RouterModule } from '@angular/router';
import { ScListOption } from './sc-list-option/sc-list-option.component';

@NgModule({
  imports: [ RouterModule, CommonModule, FormsModule ],
  declarations: [ ScSelectionListComponent, ScListOption ],
  exports: [ ScSelectionListComponent, ScListOption ],
})
export class ScSelectionListModule { }
