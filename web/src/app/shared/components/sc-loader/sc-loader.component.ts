import {Component} from '@angular/core';
import {Subject} from 'rxjs';
import {LoaderService} from '@shared/services/loader.service';

@Component({
  selector: 'sc-loader',
  templateUrl: './sc-loader.component.html',
  styleUrls: ['./sc-loader.component.scss']
})
export class ScLoaderComponent {
  isLoading: Subject<boolean> = this.loaderService.isLoading;

  constructor(private loaderService: LoaderService) { }
}
