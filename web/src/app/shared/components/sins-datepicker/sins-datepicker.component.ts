import { Component, OnDestroy, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'sins-datepicker',
  template: `<div class="input-wrapper">
  <input type="text" style="height: auto;padding: 9px;" class="ss-form-control" #dpMDY="bsDatepicker" [attr.readonly]="isDisabled ? '' : null" [isDisabled]="isDisabled" bsDatepicker [bsConfig]="bsConfig"
      [minDate]="minDate" placeholder="dia/mês/ano" [ngStyle]="{'border-color': error ? '#EB5757' : ''}" [(ngModel)]="datepickerModel">
</div>`,
  styleUrls: ['./sins-datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SinsDatepickerComponent),
      multi: true
    }
  ]
})
export class SinsDatepickerComponent implements OnInit, OnDestroy, ControlValueAccessor {

  writeValue(obj: any): void {
    if (obj)
      this.datepickerModel = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onTouched: any = () => { };
  onChange: any = () => { };

  constructor() { }

  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() bsConfig: any = { dateInputFormat: 'DD/MM/YYYY', showWeekNumbers: false };
  @Input() error: any;
  @Input() isDisabled: boolean;
  datepickerModel: Date;

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

}
