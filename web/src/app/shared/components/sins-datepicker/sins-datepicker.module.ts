import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BsDatepickerModule, defineLocale } from 'ngx-bootstrap';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { SinsDatepickerComponent } from './sins-datepicker.component';
import { FormsModule } from '@angular/forms';

defineLocale('pt-br', ptBrLocale);

@NgModule({
  imports: [CommonModule, BsDatepickerModule.forRoot(), FormsModule],
  declarations: [SinsDatepickerComponent],
  exports: [SinsDatepickerComponent]
})
export class SinsDatepickerModule { }
