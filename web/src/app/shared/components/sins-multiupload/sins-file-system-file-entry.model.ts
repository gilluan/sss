import { FileSystemFileEntry, FileSystemEntry, NgxFileDropEntry } from "ngx-file-drop";

export interface SinsFileSystemFileEntry extends FileSystemFileEntry {
    size: number;
}

export interface SinsFileEntry extends FileSystemEntry {
    size: number;
}

export interface SinsFileDropEntry extends NgxFileDropEntry {
    size: number;
    inProgressState: number;
    progress: number;
    identificadorGed: string;
}

export enum SinsFileInProgressStates {
    NaoIniciado =  0,
    EmProgresso = 1,
    Finalizado = 2
}

export interface SinsMultiuploadGedFeedback {
    nomeArquivo: string;
    idGed: string;
    tamanho: number;
}
