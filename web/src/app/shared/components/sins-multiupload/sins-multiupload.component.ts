import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SalvarArquivosMultiUpload } from '@app/actions/ged.actions';
import { GedService } from '@app/shared/ged/ged.service';
import { DocumentoEnvio } from '@app/shared/ged/models/envio/documentoEnvio.model';
import { TipoDocumentoGed } from '@app/shared/ged/models/tipo-arquivo-ged.enum';
import { MultiUploadFile } from '@app/shared/models/multiupload-file.model';
import { Store } from '@ngrx/store';
import { PersistanceService } from '@shared/services/persistence.service';
import * as fromGed from '../../../reducers/ged.reducer';
import {
  SinsFileDropEntry,
  SinsFileInProgressStates,
  SinsFileSystemFileEntry
} from './sins-file-system-file-entry.model';
import { DataUnit } from "@shared/utils/data-unit";

@Component({
  selector: 'sins-multiupload',
  templateUrl: './sins-multiupload.component.html',
  styleUrls: ['./sins-multiupload.component.css']
})
export class SinsMultiuploadComponent implements OnInit {

  @Input() scMultiple: boolean;
  @Input() scFiles: MultiUploadFile[];

  @Output() scOnError = new EventEmitter();
  @Output() scOnFileOver = new EventEmitter();
  @Output() scOnFileLeave = new EventEmitter();
  @Output() scOnFileRemoved = new EventEmitter<SinsFileExcluded>();

  files: SinsFileDropEntry[] = [];
  totalFiles: number;
  transferring: boolean;

  constructor(
    private gedStore: Store<fromGed.State>,
    private gedService: GedService,
    private persistanceService: PersistanceService,
    private changeDetector: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.transferring = false;
    if (this.scFiles && this.scFiles.length > 0) {
      for (let i = 0; i < this.scFiles.length; i++) {
        const file = this.scFiles[i];
        this.files[i].inProgressState = file.estado;
        this.files[i].progress = file.progress;
        this.files[i].identificadorGed = file.identificadorGed;
        this.changeDetector.detectChanges();
      }
    }
  }

  async dropped(files: SinsFileDropEntry[]) {
    for (let i = 0; i < files.length; i++) {
      const droppedFile = files[i];
      try {
        const fileEntry = droppedFile.fileEntry as SinsFileSystemFileEntry;
        const file = await this.accessFile(fileEntry);
        droppedFile.size = file.size;
        droppedFile.inProgressState = 0;
        droppedFile.progress = 0;
        this.files.push(droppedFile);
      } catch (error) {
        this.scOnError.emit(error);
      }
    }
    this.totalFiles = this.files.length;
  }

  fileOver = ($event): void => this.scOnFileOver.emit($event);

  fileLeave = (event$): void => this.scOnFileLeave.emit(event$);

  remove(event, item): void {
    const index = this.files.indexOf(item);
    this.files.splice(index, 1);
    const output: SinsFileExcluded = { file: item, position: index };
    this.scOnFileRemoved.emit(output);
  }

  calcularTamanhoArquivos(): string {
    const totalSize = this.calculateFilesSize(this.files);
    return DataUnit.generate(totalSize, DataUnit.KB);
  }

  calcularProgressoTotal() {
    return this.calculateProgress(this.files);
  }

  async transfer() {
    const output: Array<MultiUploadFile> = [];
    this.transferring = true;
    const documentosEnvio: DocumentoEnvio[] = [];
    for (const file of this.files) {
      const fileEntry = file.fileEntry as SinsFileSystemFileEntry;
      const arquivo = await this.accessFile(fileEntry);
      const fileReader = new FileReader();
      fileReader.readAsArrayBuffer(arquivo);
      const documentoEnvio = await this.buildDocumento(fileReader);
      output.push({
        nome: file.relativePath,
        tamanho: file.size,
        identificadorGed: null,
        progress: 0,
        key: file.relativePath,
        estado: SinsFileInProgressStates.NaoIniciado,
        error: false,
      });
      documentosEnvio.push(documentoEnvio);
    }
    this.gedStore.dispatch(new SalvarArquivosMultiUpload(documentosEnvio, output, []));
  }

  private calculateFilesSize(files: SinsFileDropEntry[]): number {
    const filesSize = files
      .map(file => file.size)
      .reduce((total, size) => total + size, 0);

    return filesSize > 0 ? filesSize : 0;
  }

  private calculateProgress(files: SinsFileDropEntry[]): number {
    const countCompleted = files.map(file => file.progress).filter(progress => progress === 100).length;
    const calculateCompletedPercentage = (total, current) => (current / total) * 100;
    return countCompleted > 0 ? calculateCompletedPercentage(files.length, countCompleted) : 0;
  }

  private async accessFile(fileEntry: SinsFileSystemFileEntry) {
    return new Promise<File>((resolve, reject) => fileEntry.file((arquivo) =>
      arquivo ? resolve(arquivo) : reject('Vazio')));
  }

  private async buildDocumento(fileReader: FileReader) {
    return new Promise<DocumentoEnvio>((resolve, reject) => {
      fileReader.onload = (_: Event) => {
        const arrayFileData = Array.from(new Int8Array(fileReader.result as ArrayBuffer));
        const documentoEnvio = this.buildDocumentoEnvio(arrayFileData);
        resolve(documentoEnvio);
      };
      fileReader.onerror = (error: any) => reject(error);
    });
  }

  private buildDocumentoEnvio(arrayFileData: number[]): DocumentoEnvio {
    const usuarioInstituto = this.persistanceService.get('usuario_instituto'); // FIXME Houston we've had a problem
    return this.gedService.criarDocumentoEnvio(arrayFileData, TipoDocumentoGed.fluxoPassos, usuarioInstituto);
  }
}

export interface SinsFileExcluded {
  file: SinsFileDropEntry;
  position: number;
}

export interface SinsFileTransferringOutput {
  documents: DocumentoEnvio[];
  files: Array<MultiUploadFile>;
}
