import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinsYearComponent } from './sins-year.component';

describe('SinsYearComponent', () => {
  let component: SinsYearComponent;
  let fixture: ComponentFixture<SinsYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinsYearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinsYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
