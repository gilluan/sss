import { Component, OnInit, DoCheck, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sins-year',
  templateUrl: './sins-year.component.html',
  styleUrls: ['./sins-year.component.css']
})
export class SinsYearComponent implements OnInit {

  public ano: number;
  public anoAtual;
  
  @Output()
  public saidaAno = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.anoAtual = new Date().getFullYear();
    this.ano = this.anoAtual;
    this.emitir();
  }

  reduzAno() {
    this.ano = this.ano - 1;
    this.emitir();
  }

  somaAno() {
    this.ano = this.ano + 1;
    this.emitir();
  }

  verificaAnoEAtualOuMenor() {
    return this.ano <= new Date().getFullYear();
  }

  verificaValor() {
    console.log('Verificando valor...')
    if(this.ano < this.anoAtual) {
      this.ano = this.anoAtual;
    }
    if(this.ano > 9999) {
      this.ano = this.anoAtual;
    }
    this.emitir();
    
  }

  public emitir() {
    this.saidaAno.emit(this.ano.toString());
  }

}
