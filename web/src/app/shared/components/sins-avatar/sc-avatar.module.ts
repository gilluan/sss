import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ScAvatarListComponent } from './sc-avatar-list.component';
import { ScAvatarComponent } from './sc-avatar.component';


@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    ScAvatarComponent, ScAvatarListComponent
  ],
  declarations: [
    ScAvatarComponent, ScAvatarListComponent
  ]
})
export class AvatarModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AvatarModule,
      providers: []
    };
  }
}
