/* tslint:disable component-selector-name */

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ScAvatar } from './sc-avatar.model';


@Component({
  selector: 'sc-avatar',
  template: `
  <div *ngIf="!scAvatar.image && props" class="avatar"
    [style.background-color]="props.background"
    [style.width]="props.size"
    [style.line-height]='props.lineheight'
    [style.height]='props.size'
    [style.font-size]='props.fontSize'
    [style.border-radius]='props.borderradius'>
    <span  [style.color]='fontColor'>{{letter}}</span>
    </div>
  <img (error)="scAvatar.image = null" *ngIf="scAvatar.image" class="ss-user-img" [src]="onBackground(scAvatar.image)">`,
  styles: [`
  .avatar {
    text-align : center;
    overflow   : hidden;

    img {
      vertical-align: top;
    }
  }`]
})
export class ScAvatarComponent implements OnInit, OnChanges {

  @Input('scAvatar') scAvatar: ScAvatar;
  @Input('size') size = 100;
  @Input('background') background;
  @Input('displayType') displayType = 'none';

  gravatarUrl: string;
  fontSize = 49;
  fontColor = '#FFFFFF';
  props: any = null;
  letter: string = '';

  constructor(private sanitization: DomSanitizer) { }


  onBackground(image) {
    if (typeof image === 'object') {
      return image;
    } else {
      const preparar = this.isUrl(image) ? '' : 'data:image/jpeg;base64,';
      return this.sanitization.bypassSecurityTrustUrl(`${preparar}${image}`);
    }
  }

  private isUrl(value: string) {
    var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);
    return value.match(regex);
  }


  getLetter(): void {
    if (this.scAvatar.name && this.scAvatar.name.length) {
      const nameInitials = this.scAvatar.name.match(/\b(\w)/g);
      if (nameInitials) {
        const nameLetters = nameInitials.slice(0, 3).join('');
        this.letter = nameLetters.toUpperCase();
      } else {
        this.letter = this.scAvatar.name[0];
      }
    }
  }

  setCssProps() {
    this.fontSize = (39 * this.size) / 100;
    this.props = {
      size: `${this.size}px`,
      lineheight: `${this.size}px`,
      background: this.background,
      fontSize: `${this.fontSize}px`
    };
    switch (this.displayType) {
      case 'rounded':
        this.props['borderradius'] = '5%';
        break;
      case 'circle':
        this.props['borderradius'] = '50%';
        break;
      default:
        this.props['borderradius'] = '0';
    }
  }

  /**
  * Set background for specific string
  * @see https://medium.com/@pppped/compute-an-arbitrary-color-for-user-avatar-starting-from-his-username-with-javascript-cd0675943b66
  */
  stringToHslColor(str, s, l) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let h = hash % 360;
    return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
  }

  ngOnInit() {
    this.background = this.stringToHslColor(this.scAvatar.name, 30, 60);
    this.setCssProps();
    this.getLetter();
  }

  ngOnChanges() {
    this.getLetter();
  }

}
