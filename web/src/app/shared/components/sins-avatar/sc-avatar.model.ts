export interface ScAvatar {
  image?: any,
  name?: string,
  id?: string,
}
