import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ScAvatar } from './sc-avatar.model';

@Component({
  selector: 'sc-avatar-list',
  templateUrl: './sc-avatar-list.component.html',
  styleUrls: ['./sc-avatar-list.component.scss'],
})
export class ScAvatarListComponent implements OnInit {

  @Input() avatars: ScAvatar[];
  @Input() size: number = 40;
  @Output() onSelected: EventEmitter<ScAvatar> = new EventEmitter<ScAvatar>();
  @Output() onInvited: EventEmitter<void> = new EventEmitter<void>();
  @Input() moreTitle: string;
  @Input() moreActionTitle: string;

  constructor() { }

  ngOnInit(): void {
  }

  selected(scAvatar: ScAvatar) {
    this.onSelected.emit(scAvatar);
  }

  invited() {
    this.onInvited.emit();
  }


}
