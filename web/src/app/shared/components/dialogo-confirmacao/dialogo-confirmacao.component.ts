import {Component, Inject, OnInit} from '@angular/core';
import {MODAL_DATA, ModalRef} from '@sicoob/ui';

@Component({
  selector: 'app-dialogo-confirmacao',
  templateUrl: './dialogo-confirmacao.component.html',
  styleUrls: ['./dialogo-confirmacao.component.scss']
})
export class DialogoConfirmacaoComponent implements OnInit {

  txtSim = 'Confirmar';
  txtNao = 'Cancelar';

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit(): void { }

  decidir(decisao: boolean) {
    this.ref.close(decisao);
  }
}
