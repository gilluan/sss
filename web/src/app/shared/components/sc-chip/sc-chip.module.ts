import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScChipComponent} from "./sc-chip.component";

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ScChipComponent ],
  exports: [ ScChipComponent ]
})
export class ScChipModule { }
