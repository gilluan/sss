import {Component, Input} from '@angular/core';

@Component({
  selector: 'sc-chip',
  template: `
    <span style="cursor: pointer;" class="ss-label ss-label-outline ss-label-{{color}}">
      {{text}} <i style="margin-left: 2px;" class="mdi mdi-close"></i>
    </span>
  `,
  styleUrls: ['./sc-chip.component.scss']
})
export class ScChipComponent {
  @Input() color: string;
  @Input() text: string;

  constructor() { }
}
