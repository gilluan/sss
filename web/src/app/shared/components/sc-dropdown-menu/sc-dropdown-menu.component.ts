import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'sc-dropdown-menu',
  template: `
    <label class="dropdown"
    attachOutsideOnClick="true"
    (clickOutside)="onClickedOutside($event)">
      <div class="dropdown-button"  (click)="toggle()">
        {{scPlaceholder}}
      </div>
      <input [id]="getId" type="checkbox" class="dropdown-input" >
      <ul [ngStyle]="{'display': visible ? 'block' : 'none'}" style="z-index: 2;min-width: 153px;" class="dropdown-menu">
        <li
          [id]="getItemId(i)"
          *ngFor="let item of scItens; let i = index"
          (click)="itemClicked(item)">{{item}}</li>
      </ul>
    </label>
  `,
  styleUrls: ['./sc-dropdown-menu.component.scss']
})
export class ScDropdownMenuComponent implements OnInit {
  @Input() scId: string;
  @Input() scPlaceholder: string;
  @Input() scItens: string[];
  @Output() scItemClicked: EventEmitter<string> = new EventEmitter<string>();

  visible: boolean = false;

  constructor() { }

  onClickedOutside = (e: Event) => this.visible = false;

  toggle = () => this.visible = !this.visible;

  ngOnInit(): void { }

  itemClicked = (item: string) => { this.visible = false; this.scItemClicked.emit(item) };

  getId = (): string => this.scId ? this.scId : "sc-dropdown";

  getItemId = (index: number): string => this.scId ? this.scId + index : 'sc-dropdown-item-' + index;

}
