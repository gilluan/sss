import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SinsLoadingService} from './sins-loading.service';

@Component({
  selector: 'sins-loading',
  templateUrl: './sins-loading.component.html',
  styleUrls: ['./sins-loading.component.scss']
})
export class SinsLoadingComponent implements OnInit, OnDestroy {

  loading: boolean;
  subscription: Subscription;

  constructor(private loaderService: SinsLoadingService) { }

  ngOnInit() {
    this.subscription = this.loaderService.isLoading.subscribe((v) => this.loading = v);
  }

  ngOnDestroy(): void {
    this.loading = false;
    this.subscription.unsubscribe();
  }

}
