export enum RubricaSubcategoria {
  pessoalProprio = '1.1',
  servicosSuprimentoEscritorio = '2.1',
  alimentacao = '2.2',
  deslocamentos = '2.3',
  telecomunicacoes = '2.4',
  hardwareSoftware = '2.5',
  sites = '2.6',
  infraUsoContinuo = '2.7',
  infraUsoEsporadico = '2.10',
  obras = '2.11',
  mobiliario = '2.12',
  desenvolvimentoPessoas = '3.1',
  apoioTecnico = '3.2',
  certificacoesRegistros = '3.3',
  exposicaoInstitucional = '3.4',
  premiosBrindes = '3.5',
  despesasFinanceiras = '4.1',
  depreciacoesAmortizacoes = '4.2',
  seguros = '4.3',
  despesasJudiciais = '4.4',
  contribuicaoAssociados = '5.1'
}

export namespace RubricaSubcategoria {
  export function values(): RubricaSubcategoria[] {
    return [
      RubricaSubcategoria.pessoalProprio,
      RubricaSubcategoria.servicosSuprimentoEscritorio,
      RubricaSubcategoria.alimentacao,
      RubricaSubcategoria.deslocamentos,
      RubricaSubcategoria.telecomunicacoes,
      RubricaSubcategoria.hardwareSoftware,
      RubricaSubcategoria.sites,
      RubricaSubcategoria.infraUsoContinuo,
      RubricaSubcategoria.infraUsoEsporadico,
      RubricaSubcategoria.obras,
      RubricaSubcategoria.mobiliario,
      RubricaSubcategoria.desenvolvimentoPessoas,
      RubricaSubcategoria.apoioTecnico,
      RubricaSubcategoria.certificacoesRegistros,
      RubricaSubcategoria.exposicaoInstitucional,
      RubricaSubcategoria.premiosBrindes,
      RubricaSubcategoria.despesasFinanceiras,
      RubricaSubcategoria.depreciacoesAmortizacoes,
      RubricaSubcategoria.seguros,
      RubricaSubcategoria.despesasJudiciais,
      RubricaSubcategoria.contribuicaoAssociados,
    ];
  }

  export function nome(subcategoriaOrString: RubricaSubcategoria | string, withIndex: boolean = false): string {
    let nomeSubcategoria;
    switch (subcategoriaOrString) {
      case RubricaSubcategoria.pessoalProprio:
        nomeSubcategoria = 'Pessoal próprio';
        break;
      case RubricaSubcategoria.servicosSuprimentoEscritorio:
        nomeSubcategoria = 'Serviços e suprimentos para escritório';
        break;
      case RubricaSubcategoria.alimentacao:
        nomeSubcategoria = 'Alimentação';
        break;
      case RubricaSubcategoria.deslocamentos:
        nomeSubcategoria = 'Deslocamentos';
        break;
      case RubricaSubcategoria.telecomunicacoes:
        nomeSubcategoria = 'Telecomunicações ';
        break;
      case RubricaSubcategoria.hardwareSoftware:
        nomeSubcategoria = 'Hardwares e softwares';
        break;
      case RubricaSubcategoria.sites:
        nomeSubcategoria = 'Sites';
        break;
      case RubricaSubcategoria.infraUsoContinuo:
        nomeSubcategoria = 'Infraestrutura de uso contínuo';
        break;
      case RubricaSubcategoria.infraUsoEsporadico:
        nomeSubcategoria = 'Infraestrutura de uso esporádico';
        break;
      case RubricaSubcategoria.obras:
        nomeSubcategoria = 'Obras e intervenções estruturais';
        break;
      case RubricaSubcategoria.mobiliario:
        nomeSubcategoria = 'Mobiliários, Eletrodomésticos, Utensílios e Itens decorativos';
        break;
      case RubricaSubcategoria.desenvolvimentoPessoas:
        nomeSubcategoria = 'Desenvolvimento e cuidados com pessoas';
        break;
      case RubricaSubcategoria.apoioTecnico:
        nomeSubcategoria = 'Apoio técnico e/ou especializado para desenvolvimento de Portifólio, organizacional e Humano';
        break;
      case RubricaSubcategoria.certificacoesRegistros:
        nomeSubcategoria = 'Certificações, registros e associações';
        break;
      case RubricaSubcategoria.exposicaoInstitucional:
        nomeSubcategoria = 'Exposição institucional';
        break;
      case RubricaSubcategoria.premiosBrindes:
        nomeSubcategoria = 'Prêmios e Brindes';
        break;
      case RubricaSubcategoria.despesasFinanceiras:
        nomeSubcategoria = 'Despesas Financeiras e Tributárias';
        break;
      case RubricaSubcategoria.depreciacoesAmortizacoes:
        nomeSubcategoria = 'Depreciações e Amortizações';
        break;
      case RubricaSubcategoria.seguros:
        nomeSubcategoria = 'Seguros';
        break;
      case RubricaSubcategoria.despesasJudiciais:
        nomeSubcategoria = 'Despesas Judiciais';
        break;
      case RubricaSubcategoria.contribuicaoAssociados:
        nomeSubcategoria = 'Contribuição de Associados';
        break;
    }
    return `${withIndex ? subcategoriaOrString : ''} ${nomeSubcategoria}`;
  }
}
