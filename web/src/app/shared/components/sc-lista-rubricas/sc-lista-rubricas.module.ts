import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ScListaRubricasComponent } from './sc-lista-rubricas.component';


@NgModule({
  imports: [CommonModule, NgSelectModule, FormsModule  ],
  declarations: [ScListaRubricasComponent],
  exports: [ScListaRubricasComponent]
})
export class ScListaRubricaModule { }
