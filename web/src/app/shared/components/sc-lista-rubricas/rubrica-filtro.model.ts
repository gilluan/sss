import {PageModelVm} from '@shared/models/page.model';


export class RubricaFiltro {

  constructor(
    public nome?: string,
    public codigo?: string,
    public numeroCooperativa?: string,
    public paginacao: PageModelVm = new PageModelVm(0, 10, 0)) { }
}
