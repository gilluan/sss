import {BaseDto} from '@app/shared/models/base-dto.model';

export class Rubrica extends BaseDto {
  codigo: string;
  nome: string;
  categoria: string;
  subcategoria: string;
}
