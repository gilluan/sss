import {ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {PageModelVm} from '@app/shared/models/page.model';
import {PagedDataModelVm} from '@app/shared/models/paged-data.model';
import {RubricaService} from '@app/shared/services/rubrica.service';
import {Observable, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {RubricaFiltro} from './rubrica-filtro.model';
import {Rubrica} from './rubrica.model';
import {RubricaSubcategoria} from '@shared/components/sc-lista-rubricas/rubrica-subcategoria.type';
import {Validations} from '@shared/utils/validations';

@Component({
  selector: 'sc-lista-rubricas',
  templateUrl: './sc-lista-rubricas.component.html',
  styleUrls: ['./sc-lista-rubricas.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScListaRubricasComponent),
      multi: true
    }
  ]
})
export class ScListaRubricasComponent implements OnInit, ControlValueAccessor, OnDestroy {

  subscription: Subscription = new Subscription();

  writeValue(obj: any): void {
    if (obj) {
      this.inititalValue = obj;
    }
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  constructor(
    private rubricaService: RubricaService,
    private changeDetector: ChangeDetectorRef) { }

  @Input() numeroCooperativa: string;
  @Input() inititalValue: any;
  @Input() visibleLabel = true;
  @Output() scOnSelect: EventEmitter<any> = new EventEmitter();
  onTouched: any = () => { };
  onChange: any = () => { };

  rubricaTypeahead = new EventEmitter<string>();

  rubricas: Rubrica[] = new Array<Rubrica>();
  paginacao: PageModelVm = new PageModelVm(0, 5, 0);
  loading: boolean;

  carregarRubricas(termo, pageNumber, pageSize, codigo = null) {
    return this.rubricaService.findAll(new RubricaFiltro(termo, codigo, this.numeroCooperativa, new PageModelVm(pageNumber, pageSize, 0)));
  }

  subscribeRubrica = (observable: Observable<any>) => observable.subscribe((pageDataVm: PagedDataModelVm) => {
    this.loading = false;
    if (pageDataVm) {
      this.rubricas = pageDataVm.data;
      this.paginacao = pageDataVm.page;
      if (this.inititalValue && this.inititalValue.codigo && !this.inititalValue.nome) {
        const filteredResult = pageDataVm.data.filter(v => v.codigo === this.inititalValue.codigo);
        if (filteredResult.length > 0) {
          this.inititalValue = filteredResult[0];
        }
      }
      this.changeDetector.detectChanges();
    }
  })

  ngOnInit() {
    let codigo;
    Validations.notNull(this.inititalValue, iv => codigo = iv.codigo);
    this.subscription.add(this.subscribeRubrica(this.carregarRubricas(null, this.paginacao.pageNumber, this.paginacao.pageSize, codigo)));
    this.subscription.add(
      this.subscribeRubrica(
        this.rubricaTypeahead.pipe(
          distinctUntilChanged(),
          debounceTime(300),
          switchMap(term => this.carregarRubricas(term, 0, this.paginacao.pageSize))
        )));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  _change($event) {
    if ($event) {
      this.scOnSelect.emit(this.inititalValue);
      this.inititalValue = $event;
      this.onChange(this.inititalValue);
    } else {
      this.onChange($event);
      this.scOnSelect.emit(null);
    }
  }

  onScrollToEnd() {
    if ( this.rubricas.length < this.paginacao.total ) {
      //this.fetchMore(1);
    }
  }
  numberOfItemsFromEndBeforeFetchingMore = 1;

  onScroll({ end }) {
    if (this.loading || this.paginacao.total <= this.rubricas.length) {
      return;
    }
    if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.rubricas.length) {
      this.fetchMore(1);
    }
  }

  fetchMore(end: number) {
    this.loading = true;
    this.subscription.add(this.rubricaService.findAll(new RubricaFiltro(null, null, this.numeroCooperativa, new PageModelVm(this.paginacao.pageNumber + end, this.paginacao.pageSize, 0))).subscribe(
      (pageDataVm: PagedDataModelVm) => {
        this.loading = false;
        if (pageDataVm && pageDataVm.page) {
          this.rubricas = this.rubricas.concat(pageDataVm.data);
          this.paginacao = pageDataVm.page;
          this.changeDetector.detectChanges();
        }
      }));
  }

  nomeSubcategoria = (subcategoriaOrString: RubricaSubcategoria | string) =>
    RubricaSubcategoria.nome(subcategoriaOrString);
}
