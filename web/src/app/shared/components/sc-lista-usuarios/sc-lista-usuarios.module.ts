import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScListaUsuarios } from './sc-lista-usuarios.component';
import { NgSelectModule } from '@ng-select/ng-select';
import {AvatarModule} from '@app/shared/components/sins-avatar/sc-avatar.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [CommonModule, NgSelectModule, AvatarModule, FormsModule  ],
  declarations: [ScListaUsuarios],
  exports: [ScListaUsuarios]
})
export class ScListaUsuariosModule { }
