import { Component, OnInit, EventEmitter, ChangeDetectorRef, Output, Input, forwardRef, ViewChild, OnDestroy } from '@angular/core';
import { distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { UsuarioInstitutoService } from '../../services/usuario-instituto.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { PerfilType } from '../../types/perfil.type';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subscription } from 'rxjs';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';

@Component({
  selector: 'sc-lista-usuarios',
  templateUrl: './sc-lista-usuarios.component.html',
  styleUrls: ['./sc-lista-usuarios.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScListaUsuarios),
      multi: true
    }
  ]
})
export class ScListaUsuarios implements OnInit, OnDestroy, ControlValueAccessor {


  subscription: Subscription = new Subscription();

  writeValue(obj: any): void {
    if(obj)
      this.inititalValue = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  @ViewChild('listaResponsavel') listaResponsavel: NgSelectComponent;

  selecionarResponsavel = () => {
    this.inititalValue = null;
    this.onChange(this.inititalValue)
    this._open();
  }

  constructor(
    private usuarioInstitutoService: UsuarioInstitutoService,
    private changeDetector: ChangeDetectorRef) { }

  @Input() numeroCooperativa: string;
  @Input() inititalValue: any;
  @Input() visibleLabel: boolean = true;
  @Input() open: boolean = false;
  onTouched: any = () => {};
  onChange: any = () => {};

  peopleTypeahead = new EventEmitter<string>();

  usuarios = [];

  private serverSideSearch() {
    this.peopleTypeahead.pipe(
      distinctUntilChanged(),
      debounceTime(300),
      switchMap(term => this.usuarioInstitutoService.findAll(null, null, term, this.numeroCooperativa))
    ).subscribe(x => {
      this.usuarios = x;
      this.changeDetector.detectChanges();
    }, (err) => {
      console.error(err);
      this.usuarios = [];
    });
  }

  ngOnInit() {
    this.open ? this._open() : console.log();
    this.serverSideSearch();
    this.subscription.add(this.usuarioInstitutoService.findAll(null, null, null, this.numeroCooperativa).subscribe((list: UsuarioInstituto[]) => {
      this.usuarios = list;
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  _change($event) {
    this.inititalValue = $event
    this.onChange(this.inititalValue);
  }

  _open() {
    this.listaResponsavel.open();
  }

  obterPerfil = (perfil: string) => PerfilType.obterPerfil(perfil);


}
