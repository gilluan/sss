import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {ScSelectItem} from './sc-select-item';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'sc-select',
  template: `
    <label class="select" (click)="alternar()" (clickOutside)="fechar()" [exclude]="scIdBotaoAcao">
      <span>{{scItemSelecionado ? scItemSelecionado.label : scPlaceholder}}</span>
      <ul style="z-index: 1;min-width: 153px;" [ngStyle]="{ 'display': visivel ? 'block' : 'none' }">
        <li *ngFor="let item of scItens; let i = index" (click)="selecionarItem(item, i)">
          {{item.label}}
        </li>
      </ul>
    </label>
  `,
  styleUrls: ['./sc-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ScSelectComponent),
      multi: true
    }
  ]
})
export class ScSelectComponent implements ControlValueAccessor, OnInit {
  @Input() scItemSelecionado: any;
  @Input() scItens: ScSelectItem[];
  @Input() scLabel: string;
  @Input() scPlaceholder: string;
  @Input() scIdBotaoAcao: string;
  @Output() scSelecionarItem: EventEmitter<any> = new EventEmitter();

  visivel = false;

  propagateChange = (_: any) => {};

  constructor() { }

  ngOnInit(): void { }

  selecionarItem(item: ScSelectItem, index: number): void {
    this.scItemSelecionado = item;
    this.scSelecionarItem.emit({ item, index});
    this.propagateChange(item);
  }

  public fechar() {
    this.visivel = false;
  }

  public abrir() {
    this.visivel = true;
  }

  public alternar() {
    this.visivel = !this.visivel;
  }

  writeValue(obj: any): void {
    if (obj) {
      this.scItemSelecionado = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void { }
}
