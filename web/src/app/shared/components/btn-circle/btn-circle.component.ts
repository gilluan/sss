import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'btn-circle',
  template: `
    <a class="btn-circle" [title]="title" (click)="event()"
       [ngStyle]="{'font-size': fontSize, 'color': color, 'width': size, 'height': size, 'line-height': size}">
      <i class="mdi {{icone}}"></i>
    </a>
  `,
  styleUrls: ['./btn-circle.component.scss']
})
export class BtnCircleComponent {

  @Input() size = '40px';
  @Input() icone: string;
  @Input() fontSize = '23px';
  @Input() color = '#007164';
  @Input() title: string;
  @Output() clicar: EventEmitter<any> = new EventEmitter<any>();

  event = () => this.clicar.emit();

  constructor() { }
}
