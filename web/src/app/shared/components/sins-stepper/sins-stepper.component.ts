import { Directionality } from '@angular/cdk/bidi';
import { ChangeDetectorRef, Component, EventEmitter, Output } from '@angular/core';
import { CdkStepper, CdkStep } from '@angular/cdk/stepper';

@Component({
  selector: 'sins-stepper',
  templateUrl: './sins-stepper.component.html',
  styleUrls: ['./sins-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: SinsStepperComponent }],
})
export class SinsStepperComponent extends CdkStepper {
  constructor(dir: Directionality, changeDetectorRef: ChangeDetectorRef) {
    super(dir, changeDetectorRef);
    console.log(this.steps);
  }

  onClick(index: number): void {
    this.selectedIndex = index;
  }

}
