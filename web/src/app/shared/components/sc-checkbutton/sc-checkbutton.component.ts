import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sc-checkbutton',
  template: `
    <div class="ss-btn ss-btn-outline-default" (click)="toggleValue()">
      <input [(ngModel)]="value" type="checkbox" />
      <span style="margin-left: 10px">{{label}}</span>
    </div>
  `,
  styleUrls: ['./sc-checkbutton.component.css']
})
export class ScCheckbuttonComponent implements OnInit {
  @Input() label: string;
  @Input() value: boolean;
  @Output() onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  toggleValue() {
    this.value = !this.value;
    this.onChanged.emit(this.value);
  }
}
