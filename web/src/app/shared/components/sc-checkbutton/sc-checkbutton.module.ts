import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScCheckbuttonComponent} from "./sc-checkbutton.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [ CommonModule, FormsModule ],
  declarations: [ ScCheckbuttonComponent ],
  exports: [ ScCheckbuttonComponent ]
})
export class ScCheckbuttonModule { }
