import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import * as fromApp from '@app/app.reduce';
import { select, Store } from '@ngrx/store';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LoaderService } from '@shared/services/loader.service';
import { FormModule, ModalModule, ProgressBarModule, TabsModule } from '@sicoob/ui';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxMaskModule } from 'ngx-mask';
import { BtnCircleComponent } from './components/btn-circle/btn-circle.component';
import { MenuDropDownModule } from './components/menu-dropdown/menu-dropdown.module';
import { RelatorioCardComponent } from './components/relatorio-card/relatorio-card.component';
import { ScBannerComponent } from './components/sc-banner/sc-banner.component';
import { ScDateComponent } from './components/sc-date/sc-date.component';
import { ScDateDirective } from './components/sc-date/sc-date.directive';
import { ScDropdownMenuComponent } from './components/sc-dropdown-menu/sc-dropdown-menu.component';
import { ScLoaderComponent } from './components/sc-loader/sc-loader.component';
import { ScSearchInputComponent } from './components/sc-search-input/sc-search-input.component';
import { ScSelectComponent } from './components/sc-select/sc-select.component';
import { ScUploadProgressComponent } from './components/sc-upload-progress/sc-upload-progress.component';
import { SinsBadgeModule } from './components/sins-badge/sins-badge.module';
import { SinsCardComponent } from './components/sins-card/sins-card.component';
import { SinsLoadingComponent } from './components/sins-loading/sins-loading.component';
import { SinsLoadingService } from './components/sins-loading/sins-loading.service';
import { SinsMultiuploadComponent } from './components/sins-multiupload/sins-multiupload.component';
import { SinsStepperComponent } from './components/sins-stepper/sins-stepper.component';
import { CropImageModalComponent } from './components/sins-upload-img/crop-image-modal/crop-image-modal.component';
import { SinsUploadImageComponent } from './components/sins-upload-img/sins-upload-image.component';
import { SinsYearComponent } from './components/sins-year/sins-year.component';
import { ClickOutsideModule } from './directive/click-outside/click-outside.module';
import { DisableControlDirective } from './directive/disable-control.directive';
import { LogPipe } from './pipe/log.pipe';
import { SafeHtmlPipe } from './pipe/safeHtml.pipe';
import { MessageService } from './services/message.service';
import { AvatarModule } from '@app/shared/components/sins-avatar/sc-avatar.module';
import { GoldCoinComponent } from './components/gold-coin/gold-coin.component';

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};


@NgModule({
  declarations: [
    ScDropdownMenuComponent,
    RelatorioCardComponent,
    ScSearchInputComponent,
    SinsLoadingComponent,
    SafeHtmlPipe,
    BtnCircleComponent,
    ScSelectComponent,
    ScDateComponent,
    ScDateDirective,
    ScUploadProgressComponent,
    ScLoaderComponent,
    SinsStepperComponent,
    SinsUploadImageComponent,
    SinsYearComponent,
    CropImageModalComponent,
    LogPipe,
    SinsMultiuploadComponent,
    ScBannerComponent,
    DisableControlDirective,
    SinsCardComponent,
    GoldCoinComponent,
  ],
  exports: [
    GoldCoinComponent,
    TranslateModule,
    ScDateComponent,
    ScDropdownMenuComponent,
    RelatorioCardComponent,
    ScSearchInputComponent,
    SinsLoadingComponent,
    SafeHtmlPipe,
    BtnCircleComponent,
    ScSelectComponent,
    ScDateDirective,
    ScUploadProgressComponent,
    ScLoaderComponent,
    SinsStepperComponent,
    SinsUploadImageComponent,
    SinsYearComponent,
    SinsMultiuploadComponent,
    LogPipe,
    CdkStepperModule,
    DisableControlDirective,
    ScBannerComponent,
    SinsCardComponent
  ],
  imports: [
    TabsModule,
    AvatarModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: true
    }),
    NgxMaskModule.forRoot(),
    ProgressBarModule,
    CommonModule,
    RouterModule,
    FormsModule,
    FormModule,
    NgxLoadingModule,
    ClickOutsideModule,
    ImageCropperModule,
    ModalModule,
    CdkStepperModule,
    SinsBadgeModule,
    NgxFileDropModule,
    MenuDropDownModule
  ],
  entryComponents: [
    CropImageModalComponent,
  ],
  providers: [
    MessageService
  ]
})
export class SharedModule {

  constructor(public appStore$: Store<fromApp.State>, public translate: TranslateService) {
    this.appStore$.pipe(select(fromApp.selectLang)).subscribe(arg => translate.use(arg));
  }


  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        SinsLoadingService,
        LoaderService
      ]
    };
  }
}
