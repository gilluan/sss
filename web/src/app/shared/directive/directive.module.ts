import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsyncClickDirective } from './asyn_click.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    AsyncClickDirective
  ],
  exports: [
    AsyncClickDirective
  ]
})
export class ClickDirectiveModule { }
