import {NgModule} from "@angular/core";
import {CodigoVoluntarioPipe} from "./codigoVoluntario.pipe";

@NgModule({
  declarations: [ CodigoVoluntarioPipe ],
  exports: [ CodigoVoluntarioPipe ]
})
export class CodigoVoluntarioModule { }
