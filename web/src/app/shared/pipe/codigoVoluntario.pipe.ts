import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'codigoVoluntarioFormatter'})
export class CodigoVoluntarioPipe implements PipeTransform {

  transform(value: any): string {
    let codigo = `${value}`.padStart(6, '0');
    return `#${codigo}`;
  }
}
