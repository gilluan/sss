import { Action } from '@ngrx/store';
import { Game } from './shared/models/game.model';

export enum AppActionTypes {
  DisplayToolbar = 'HIDE_TOOLBAR',
  AjustarPadding = 'AjustarPadding',
  DisplayVisibilityHeader = 'DisplayVisibilityHeader',
  ScrollContainer = 'ScrollContainer',
  ChangeLang = 'ChangeLang',
  AddGameUser = 'AddGamesUser',
  AddGameUserSuccess = 'AddGamesUserSuccess',
  AddGameUserFail = 'AddGamesUserFail',
}
export class ChangeLang implements Action {
  readonly type = AppActionTypes.ChangeLang;
  constructor(public lang: string) { }
}
export class DisplayToolbar implements Action {
  readonly type = AppActionTypes.DisplayToolbar;
  constructor(public isVisible: boolean) { }
}
export class ScrollContainer implements Action {
  readonly type = AppActionTypes.ScrollContainer;
  constructor(public isScroll: boolean) { }
}


export class AjustarPadding implements Action {
  readonly type = AppActionTypes.AjustarPadding;
  constructor(public padding: number) { }
}

export class DisplayVisibilityHeader implements Action {
  readonly type = AppActionTypes.DisplayVisibilityHeader;
  constructor(public visibilityHeader: boolean) { }
}

export class AddGameUser implements Action {
  readonly type = AppActionTypes.AddGameUser;
  constructor(public cpf: string, public voluntario: boolean) { }
}
export class AddGameUserSuccess implements Action {
  readonly type = AppActionTypes.AddGameUserSuccess;
  constructor(public game: Game[]) { }
}
export class AddGameUserFail implements Action {
  readonly type = AppActionTypes.AddGameUserFail;
  constructor(public error: any) { }
}


export type AppAction =
  AddGameUser | AddGameUserSuccess | AddGameUserFail | DisplayToolbar | AjustarPadding | DisplayVisibilityHeader | ScrollContainer | ChangeLang;
