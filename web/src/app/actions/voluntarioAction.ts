import { Action } from '@ngrx/store';
import { VoluntarioFiltroDto } from '../shared/models/voluntario-filtro.model';
import { PagedDataModelVm } from '../shared/models/paged-data.model';
import { VoluntarioDto } from '../shared/models/voluntario-dto.model';
import { TotalSituacaoVm } from '../funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { Voluntario } from '@app/funcionalidades/termo-voluntario/models/voluntario.model';
import { Curso } from '@app/funcionalidades/perfil-usuario/models/curso.model';

export enum VoluntarioActionTypes {
  SalvarVoluntario = '[SalvarVoluntario]',
  SalvarVoluntarioSuccess = '[SalvarVoluntarioSuccess]',
  SalvarVoluntarioFail = '[SalvarVoluntarioFail]',
  EditarVoluntario = '[EditarVoluntario]',
  EditarVoluntarioSuccess = '[EditarVoluntarioSuccess]',
  EditarVoluntarioFail = '[EditarVoluntarioFail]',
  CarregarVoluntariosPorFiltro = '[CarregarVoluntariosPorFiltro]',
  CarregarVoluntariosPorFiltroSuccess = '[CarregarVoluntariosPorFiltroSuccess]',
  CarregarVoluntariosPorFiltroFail = '[CarregarVoluntariosPorFiltroFail]',
  DesativarVoluntario = '[DesativarVoluntario]',
  DesativarVoluntarioSuccess = '[DesativarVoluntarioSuccess]',
  DesativarVoluntarioFail = '[DesativarVoluntarioFail]',
  AtivarVoluntario = '[AtivarVoluntario]',
  AtivarVoluntarioSuccess = '[AtivarVoluntarioSuccess]',
  AtivarVoluntarioFail = '[AtivarVoluntarioFail]',
  CarregarCooperativas = '[CarregarCooperativas]',
  CarregarCooperativasSuccess = '[CarregarCooperativasSuccess]',
  CarregarCooperativasFail = '[CarregarCooperativasFail]',
  CarregarTotais = '[Voluntario CarregarTotais]',
  CarregarTotaisSuccess = '[Voluntario CarregarTotaisSuccess]',
  CarregarTotaisFail = '[Voluntario CarregarTotaisFail]',
  AssinarContratoVoluntario = '[AssinarContratoVoluntario]',
  AssinarContratoVoluntarioSuccess = '[AssinarContratoVoluntarioSuccess]',
  AssinarContratoVoluntarioFail = '[AssinarContratoVoluntarioFail]',
  CarregarTotalPorFiltro = '[CarregarTotaPorFiltro]',
  CarregarTotalPorFiltroSuccess = '[CarregarTotaPorFiltroSuccess]',
  CarregarTotalPorFiltroFail = '[CarregarTotaPorFiltroFail]',
  CarregarTotalAssinar = '[CarregarTotalAssinar]',
  CarregarTotalAssinarSuccess = '[CarregarTotalAssinarSuccess]',
  CarregarTotalAssinarFail = '[CarregarTotalAssinarFail]',
  CarregarTotalBancoVoluntario = '[CarregarTotalBancoVoluntario]',
  CarregarTotalBancoVoluntarioSuccess = '[CarregarTotalBancoVoluntarioSuccess]',
  CarregarTotalBancoVoluntarioFail = '[CarregarTotalBancoVoluntarioFail]',
  CarregarTotalAnalise = '[CarregarTotalAnalise]',
  CarregarTotalAnaliseSuccess = '[CarregarTotalAnaliseSuccess]',
  CarregarTotalAnaliseFail = '[CarregarTotalAnaliseFail]',
  CarregarVoluntariosPorCPF = '[CarregarVoluntariosPorCPF]',
  CarregarVoluntariosPorCPFSuccess = '[CarregarVoluntariosPorCPFSuccess]',
  CarregarVoluntariosPorCPFFail = '[CarregarVoluntariosPorCPFFail]',
  ConcordarTermoVoluntario = '[ConcordarTermoVoluntario]',
  ConcordarTermoVoluntarioSuccess = '[ConcordarTermoVoluntarioSuccess]',
  ConcordarTermoVoluntarioFail = '[ConcordarTermoVoluntarioFail]',
  CarregarVoluntariosPorId = '[CarregarVoluntariosPorId] VoluntariosActions',
  CarregarVoluntariosPorIdSuccess = '[CarregarVoluntariosPorIdSuccess] VoluntariosActions',
  CarregarVoluntariosPorIdFail = '[CarregarVoluntariosPorIdFail] VoluntariosActions',
}

export class CarregarVoluntariosPorId implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorId;
  constructor(public id: string) { }
}
export class CarregarVoluntariosPorIdSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorIdSuccess;
  constructor(public payload: VoluntarioDto) { }
}
export class CarregarVoluntariosPorIdFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorIdFail;
  constructor(public payload: any) { }
}


export class CarregarVoluntariosPorCPF implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorCPF;
  constructor(public cpf: string) { }
}
export class CarregarVoluntariosPorCPFSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorCPFSuccess;
  constructor(public payload: VoluntarioDto) { }
}
export class CarregarVoluntariosPorCPFFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorCPFFail;
  constructor(public payload: any) { }
}

export class CarregarVoluntariosPorFiltro implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorFiltro;
  constructor(public filter: VoluntarioFiltroDto) { }
}

export class CarregarVoluntariosPorFiltroSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorFiltroSuccess;
  constructor(public payload: PagedDataModelVm) { }
}

export class CarregarVoluntariosPorFiltroFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarVoluntariosPorFiltroFail;
  constructor(public payload: any) { }
}

export class DesativarVoluntario implements Action {
  readonly type = VoluntarioActionTypes.DesativarVoluntario;
  constructor(public voluntario: VoluntarioDto, public filter: VoluntarioFiltroDto) { }
}

export class DesativarVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.DesativarVoluntarioSuccess;
  constructor(public payload: VoluntarioDto) { }
}

export class DesativarVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.DesativarVoluntarioFail;
  constructor(public payload: any) { }
}

export class AtivarVoluntario implements Action {
  readonly type = VoluntarioActionTypes.AtivarVoluntario;
  constructor(public voluntario: VoluntarioDto, public filter: VoluntarioFiltroDto) { }
}

export class AtivarVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.AtivarVoluntarioSuccess;
  constructor(public payload: VoluntarioDto) { }
}

export class AtivarVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.AtivarVoluntarioFail;
  constructor(public payload: any) { }
}

export class CarregarCooperativas implements Action {
  readonly type = VoluntarioActionTypes.CarregarCooperativas;
  constructor() { }
}

export class CarregarCooperativasSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarCooperativasSuccess;
  constructor(public payload: string[]) { }
}

export class CarregarCooperativasFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarCooperativasFail;
  constructor(public payload: any) { }
}

export class CarregarTotais implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotais;
  constructor(public filter: VoluntarioFiltroDto) { }
}

export class CarregarTotaisSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotaisSuccess;
  constructor(public payload: TotalSituacaoVm[]) { }
}

export class CarregarTotaisFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotaisFail;
  constructor(public payload: any) { }
}

export class AssinarContratoVoluntario implements Action {
  readonly type = VoluntarioActionTypes.AssinarContratoVoluntario;
  constructor(public id: string) { }
}

export class AssinarContratoVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.AssinarContratoVoluntarioSuccess;
  constructor(public payload: VoluntarioDto) { }
}

export class AssinarContratoVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.AssinarContratoVoluntarioFail;
  constructor(public payload: any) { }
}

export class CarregarTotalPorFiltro implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalPorFiltro;
  constructor(public filtro: VoluntarioFiltroDto) { }
}

export class CarregarTotalPorFiltroSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalPorFiltroSuccess;
  constructor(public total: number) { }
}

export class CarregarTotalPorFiltroFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalPorFiltroFail;
  constructor(public payload: any) { }
}

export class CarregarTotalAssinar implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAssinar;
  constructor(public filtro: VoluntarioFiltroDto) { }
}

export class CarregarTotalAssinarSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAssinarSuccess;
  constructor(public total: number) { }
}

export class CarregarTotalAssinarFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAssinarFail;
  constructor(public payload: any) { }
}

export class CarregarTotalBancoVoluntario implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalBancoVoluntario;
  constructor(public filtro: VoluntarioFiltroDto) { }
}

export class CarregarTotalBancoVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalBancoVoluntarioSuccess;
  constructor(public total: number) { }
}

export class CarregarTotalBancoVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalBancoVoluntarioFail;
  constructor(public payload: any) { }
}

export class CarregarTotalAnalise implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAnalise;
  constructor(public filtro: VoluntarioFiltroDto) { }
}

export class CarregarTotalAnaliseSuccess implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAnaliseSuccess;
  constructor(public total: number) { }
}

export class CarregarTotalAnaliseFail implements Action {
  readonly type = VoluntarioActionTypes.CarregarTotalAnaliseFail;
  constructor(public payload: any) { }
}

export class SalvarVoluntario implements Action {
  readonly type = VoluntarioActionTypes.SalvarVoluntario;
  constructor(public voluntario: Voluntario, public curso: Curso) { }
}

export class SalvarVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.SalvarVoluntarioSuccess;
  constructor(public voluntario: Voluntario) { }
}

export class SalvarVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.SalvarVoluntarioFail;
  constructor(public payload: any) { }
}

export class EditarVoluntario implements Action {
  readonly type = VoluntarioActionTypes.EditarVoluntario;
  constructor(public voluntario: Voluntario) { }
}

export class EditarVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.EditarVoluntarioSuccess;
  constructor(public voluntario: Voluntario) { }
}

export class EditarVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.EditarVoluntarioFail;
  constructor(public payload: any) { }
}



export class ConcordarTermoVoluntario implements Action {
  readonly type = VoluntarioActionTypes.ConcordarTermoVoluntario;
  constructor(public voluntario: VoluntarioDto) { }
}

export class ConcordarTermoVoluntarioSuccess implements Action {
  readonly type = VoluntarioActionTypes.ConcordarTermoVoluntarioSuccess;
  constructor(public voluntario: VoluntarioDto) { }
}

export class ConcordarTermoVoluntarioFail implements Action {
  readonly type = VoluntarioActionTypes.ConcordarTermoVoluntarioFail;
  constructor(public payload: any) { }
}

export type VoluntarioAction =
  | CarregarVoluntariosPorId
  | CarregarVoluntariosPorIdSuccess
  | CarregarVoluntariosPorIdFail
  | CarregarVoluntariosPorFiltro
  | CarregarVoluntariosPorFiltroSuccess
  | CarregarVoluntariosPorFiltroFail
  | DesativarVoluntario
  | DesativarVoluntarioSuccess
  | DesativarVoluntarioFail
  | AtivarVoluntario
  | AtivarVoluntarioSuccess
  | AtivarVoluntarioFail
  | CarregarCooperativas
  | CarregarCooperativasSuccess
  | CarregarCooperativasFail
  | CarregarTotais
  | CarregarTotaisSuccess
  | CarregarTotaisFail
  | AssinarContratoVoluntario
  | AssinarContratoVoluntarioSuccess
  | AssinarContratoVoluntarioFail
  | CarregarTotalPorFiltro
  | CarregarTotalPorFiltroSuccess
  | CarregarTotalPorFiltroFail
  | CarregarTotalAssinar
  | CarregarTotalAssinarSuccess
  | CarregarTotalAssinarFail
  | CarregarTotalBancoVoluntario
  | CarregarTotalBancoVoluntarioSuccess
  | CarregarTotalBancoVoluntarioFail
  | CarregarTotalAnalise
  | CarregarTotalAnaliseSuccess
  | CarregarTotalAnaliseFail
  | CarregarVoluntariosPorCPF
  | CarregarVoluntariosPorCPFSuccess
  | CarregarVoluntariosPorCPFFail
  | SalvarVoluntario
  | SalvarVoluntarioSuccess
  | SalvarVoluntarioFail
  | ConcordarTermoVoluntario
  | ConcordarTermoVoluntarioSuccess
  | ConcordarTermoVoluntarioFail
  | EditarVoluntario
  | EditarVoluntarioSuccess
  | EditarVoluntarioFail;
