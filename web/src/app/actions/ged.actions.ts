import { Curso } from '@app/funcionalidades/perfil-usuario/models/curso.model';
import { Voluntario } from '@app/funcionalidades/termo-voluntario/models/voluntario.model';
import { MultiUploadFile } from '@app/shared/models/multiupload-file.model';
import { Action } from '@ngrx/store';
import { ActionBarRef } from '@sicoob/ui';
import { DocumentoEnvio } from '../shared/ged/models/envio/documentoEnvio.model';
import { DadosDocumento } from '../shared/ged/models/retorno/dadosDocumento.model';
import { PassoFluxoExecucao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.model';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { PassoFluxoExecucaoFiltro } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.filtro';

export enum GedActionTypes {
  CriarArquivo = '[CriarArquivo]',
  CriarArquivoSuccess = '[CriarArquivoSuccess]',
  CriarArquivoFail = '[CriarArquivoFail]',
  CarregarArquivo = '[CarregarArquivo]',
  CarregarArquivoSuccess = '[CarregarArquivoSuccess]',
  CarregarArquivoFail = '[CarregarArquivoFail]',
  LimparStoreGed = '[LimparStoreGed]',
  SalvarArquivoCurso = '[SalvarArquivoCurso]',
  SalvarArquivoCursoSuccess = '[SalvarArquivoCursoSuccess]',
  SalvarArquivoCursoFail = '[SalvarArquivoCursoFail]',
  UploadProgress = '[UploadProgress]',
  GedEvents = '[GedEvents]',
  EditarArquivoCurso = '[EditarArquivoCurso]',
  EditarArquivoCursoSuccess = '[EditarArquivoCursoSuccess]',
  EditarArquivoCursoFail = '[EditarArquivoCursoFail]',
  SalvarArquivoCursoVoluntario = '[SalvarArquivoCursoVoluntario]',
  SalvarArquivoCursoVoluntarioSuccess = '[SalvarArquivoCursoVoluntarioSuccess]',
  SalvarArquivoCursoVoluntarioFail = '[SalvarArquivoCursoVoluntarioFail]',
  EditarArquivoCursoVoluntario = '[EditarArquivoCursoVoluntario]',
  EditarArquivoCursoVoluntarioSuccess = '[EditarArquivoCursoVoluntarioSuccess]',
  EditarArquivoCursoVoluntarioFail = '[EditarArquivoCursoVoluntarioFail]',
  SalvarArquivosMultiUpload = '[SalvarArquivosMultiUpload]',
  SalvarArquivosMultiUploadSuccess = '[SalvarArquivosMultiUploadSuccess]',
  SalvarArquivosMultiUploadFail = '[SalvarArquivosMultiUploadFail]',
  ExecutarPassoArquivo = '[ExecutarPassoArquivo] GED',
  ExecutarPassoArquivoSuccess = '[ExecutarPassoArquivoSuccess] GED',
  ExecutarPassoArquivoFail = '[ExecutarPassoArquivoFail] GED',
  CleanIndice = '[CleanIndice] Ged Store',
}


export class UploadProgress implements Action {
  readonly type = GedActionTypes.UploadProgress;
  constructor(public progress: number) { }
}

export class GedEvents implements Action {
  readonly type = GedActionTypes.GedEvents;
  constructor() { }
}

export class CriarArquivo implements Action {
  readonly type = GedActionTypes.CriarArquivo;
  constructor(public documentoEnvio: DocumentoEnvio, public acoes: Action[]) { }
}

export class CriarArquivoSuccess implements Action {
  readonly type = GedActionTypes.CriarArquivoSuccess;
  constructor(public identificadorGed: string) { }
}

export class CriarArquivoFail implements Action {
  readonly type = GedActionTypes.CriarArquivoFail;
  constructor(public payload: any) { }
}

export class CarregarArquivo implements Action {
  readonly type = GedActionTypes.CarregarArquivo;
  constructor(public identificador: string) { }
}

export class CarregarArquivoSuccess implements Action {
  readonly type = GedActionTypes.CarregarArquivoSuccess;
  constructor(public dadosDocumento: DadosDocumento) { }
}

export class CarregarArquivoFail implements Action {
  readonly type = GedActionTypes.CarregarArquivoFail;
  constructor(public payload: any) { }
}

export class LimparStoreGed implements Action {
  readonly type = GedActionTypes.LimparStoreGed;
}


export class SalvarArquivoCurso implements Action {
  readonly type = GedActionTypes.SalvarArquivoCurso;
  constructor(public documentoEnvio: DocumentoEnvio, public curso: Curso, public actionBar: ActionBarRef) { }
}

export class SalvarArquivoCursoSuccess implements Action {
  readonly type = GedActionTypes.SalvarArquivoCursoSuccess;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}

export class SalvarArquivoCursoFail implements Action {
  readonly type = GedActionTypes.SalvarArquivoCursoFail;
  constructor(public payload: any) { }
}

export class EditarArquivoCurso implements Action {
  readonly type = GedActionTypes.EditarArquivoCurso;
  constructor(public documentoEnvio: DocumentoEnvio, public curso: Curso, public actionBar: ActionBarRef) { }
}

export class EditarArquivoCursoSuccess implements Action {
  readonly type = GedActionTypes.EditarArquivoCursoSuccess;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}

export class EditarArquivoCursoFail implements Action {
  readonly type = GedActionTypes.EditarArquivoCursoFail;
  constructor(public payload: any) { }
}


export class SalvarArquivoCursoVoluntario implements Action {
  readonly type = GedActionTypes.SalvarArquivoCursoVoluntario;
  constructor(public documentoEnvio: DocumentoEnvio, public curso: Curso, public voluntario: Voluntario) { }
}

export class SalvarArquivoCursoVoluntarioSuccess implements Action {
  readonly type = GedActionTypes.SalvarArquivoCursoVoluntarioSuccess;
  constructor(public curso: Curso, public voluntario: Voluntario) { }
}

export class SalvarArquivoCursoVoluntarioFail implements Action {
  readonly type = GedActionTypes.SalvarArquivoCursoVoluntarioFail;
  constructor(public payload: any) { }
}

export class EditarArquivoCursoVoluntario implements Action {
  readonly type = GedActionTypes.EditarArquivoCursoVoluntario;
  constructor(public documentoEnvio: DocumentoEnvio, public curso: Curso, public voluntario: Voluntario) { }
}

export class EditarArquivoCursoVoluntarioSuccess implements Action {
  readonly type = GedActionTypes.EditarArquivoCursoVoluntarioSuccess;
  constructor(public curso: Curso, public voluntario: Voluntario) { }
}

export class EditarArquivoCursoVoluntarioFail implements Action {
  readonly type = GedActionTypes.EditarArquivoCursoVoluntarioFail;
  constructor(public payload: any) { }
}

export class SalvarArquivosMultiUpload implements Action {
  readonly type = GedActionTypes.SalvarArquivosMultiUpload;
  constructor(public documentosEnvio: DocumentoEnvio[], public multiUploadFiles: MultiUploadFile[], public actions: Action[]) { }
}

export class SalvarArquivosMultiUploadSuccess implements Action {
  readonly type = GedActionTypes.SalvarArquivosMultiUploadSuccess;
  constructor(public documentosEnvio: DocumentoEnvio[], public multiUploadFiles: MultiUploadFile[], public actions: Action[]) { }
}

export class SalvarArquivosMultiUploadFail implements Action {
  readonly type = GedActionTypes.SalvarArquivosMultiUploadFail;
  constructor(public payload: any) { }
}

export class ExecutarPassoArquivo implements Action {
  readonly type = GedActionTypes.ExecutarPassoArquivo;
  constructor(public documentosEnvio: DocumentoEnvio[], public multiUploadFiles: MultiUploadFile[], public passoFluxo: PassoFluxo, public filtro: PassoFluxoExecucaoFiltro, public actionBarRef: ActionBarRef) { }
}

export class ExecutarPassoArquivoSuccess implements Action {
  readonly type = GedActionTypes.ExecutarPassoArquivoSuccess;
  constructor(public passoFluxo: PassoFluxo, public filtro: PassoFluxoExecucaoFiltro, public actionBarRef: ActionBarRef) { }
}

export class ExecutarPassoArquivoFail implements Action {
  readonly type = GedActionTypes.ExecutarPassoArquivoFail;
  constructor(public payload: any) { }
}

export class CleanIndice implements Action {
  readonly type = GedActionTypes.CleanIndice;
  constructor() { }
}


export type GedActions =
  | ExecutarPassoArquivo
  | ExecutarPassoArquivoSuccess
  | ExecutarPassoArquivoFail
  | CleanIndice
  | CriarArquivo
  | CriarArquivoSuccess
  | CriarArquivoFail
  | CarregarArquivo
  | CarregarArquivoSuccess
  | CarregarArquivoFail
  | LimparStoreGed
  | SalvarArquivoCurso
  | SalvarArquivoCursoSuccess
  | SalvarArquivoCursoFail
  | UploadProgress
  | GedEvents
  | EditarArquivoCurso
  | EditarArquivoCursoSuccess
  | EditarArquivoCursoFail
  | SalvarArquivoCursoVoluntario
  | SalvarArquivoCursoVoluntarioSuccess
  | SalvarArquivoCursoVoluntarioFail
  | EditarArquivoCursoVoluntario
  | EditarArquivoCursoVoluntarioSuccess
  | EditarArquivoCursoVoluntarioFail
  | SalvarArquivosMultiUpload
  | SalvarArquivosMultiUploadSuccess
  | SalvarArquivosMultiUploadFail;
