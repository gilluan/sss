import { Action } from "@ngrx/store/public_api";
import { HorasVoluntario } from "@shared/models/horas-voluntario.model";
import { PagedDataModelVm } from "@app/shared/models/paged-data.model";
import { PageModelVm } from "@app/shared/models/page.model";

export enum HorasVoluntarioTypes {
  CarregarHorasVoluntario = '[CarregarHorasVoluntario] HorasVoluntarioTypes',
  CarregarHorasVoluntarioSuccess = '[CarregarHorasVoluntarioSuccess] HorasVoluntarioTypes',
  CarregarHorasVoluntarioFail = '[CarregarHorasVoluntarioFail] HorasVoluntarioTypes',
  SalvarHorasVoluntario = '[SalvarHorasVoluntario] HorasVoluntarioTypes',
  SalvarHorasVoluntarioSuccess = '[SalvarHorasVoluntarioSuccess] HorasVoluntarioTypes',
  SalvarHorasVoluntarioFail = '[SalvarHorasVoluntarioFail] HorasVoluntarioTypes',
  EditarHorasVoluntario = '[EditarHorasVoluntario] HorasVoluntarioTypes',
  EditarHorasVoluntarioSuccess = '[EditarHorasVoluntarioSuccess] HorasVoluntarioTypes',
  EditarHorasVoluntarioFail = '[EditarHorasVoluntarioFail] HorasVoluntarioTypes',
  RemoverHorasVoluntario = '[RemoverHorasVoluntario] HorasVoluntarioTypes',
  RemoverHorasVoluntarioSuccess = '[RemoverHorasVoluntarioSuccess] HorasVoluntarioTypes',
  RemoverHorasVoluntarioFail = '[RemoverHorasVoluntarioFail] HorasVoluntarioTypes',
  CleanHorasVoluntario = '[CleanHorasVoluntario] HorasVoluntarioTypes',
  CarregarTotalHorasVoluntario = '[CarregarTotalHorasVoluntario] HorasVoluntarioTypes',
  CarregarTotalHorasVoluntarioSuccess = '[CarregarTotalHorasVoluntarioSuccess] HorasVoluntarioTypes',
  CarregarTotalHorasVoluntarioFail = '[CarregarTotalHorasVoluntarioFail] HorasVoluntarioTypes',
}

export class SalvarHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.SalvarHorasVoluntario;
  constructor(public horasVoluntario: HorasVoluntario) { }
}
export class SalvarHorasVoluntarioSuccess implements Action {
  readonly type = HorasVoluntarioTypes.SalvarHorasVoluntarioSuccess;
  constructor(public horasVoluntario: HorasVoluntario) { }
}
export class SalvarHorasVoluntarioFail implements Action {
  readonly type = HorasVoluntarioTypes.SalvarHorasVoluntarioFail;
  constructor(public erro: any) { }
}

export class EditarHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.EditarHorasVoluntario;
  constructor(public horasVoluntario: HorasVoluntario) { }
}
export class EditarHorasVoluntarioSuccess implements Action {
  readonly type = HorasVoluntarioTypes.EditarHorasVoluntarioSuccess;
  constructor(public horasVoluntario: HorasVoluntario) { }
}
export class EditarHorasVoluntarioFail implements Action {
  readonly type = HorasVoluntarioTypes.EditarHorasVoluntarioFail;
  constructor(public erro: any) { }
}

export class RemoverHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.RemoverHorasVoluntario;
  constructor(public horasVoluntario: HorasVoluntario) { }
}
export class RemoverHorasVoluntarioSuccess implements Action {
  readonly type = HorasVoluntarioTypes.RemoverHorasVoluntarioSuccess;
  constructor(public id: string) { }
}
export class RemoverHorasVoluntarioFail implements Action {
  readonly type = HorasVoluntarioTypes.RemoverHorasVoluntarioFail;
  constructor(public erro: any) { }
}

export class CarregarHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.CarregarHorasVoluntario;
  constructor(public id: string, public paginacao: PageModelVm) { }
}
export class CarregarHorasVoluntarioSuccess implements Action {
  readonly type = HorasVoluntarioTypes.CarregarHorasVoluntarioSuccess;
  constructor(public HorasVoluntario: PagedDataModelVm) { }
}
export class CarregarHorasVoluntarioFail implements Action {
  readonly type = HorasVoluntarioTypes.CarregarHorasVoluntarioFail;
  constructor(public erro: any) { }
}

export class CarregarTotalHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.CarregarTotalHorasVoluntario;
  constructor(public id: string) { }
}
export class CarregarTotalHorasVoluntarioSuccess implements Action {
  readonly type = HorasVoluntarioTypes.CarregarTotalHorasVoluntarioSuccess;
  constructor(public horas: number) { }
}
export class CarregarTotalHorasVoluntarioFail implements Action {
  readonly type = HorasVoluntarioTypes.CarregarTotalHorasVoluntarioFail;
  constructor(public erro: any) { }
}

export class CleanHorasVoluntario implements Action {
  readonly type = HorasVoluntarioTypes.CleanHorasVoluntario;
  constructor(public all: boolean) { }
}

export type HorasVoluntarioActions =
  CarregarHorasVoluntario |
  CarregarHorasVoluntarioSuccess |
  CarregarHorasVoluntarioFail |
  SalvarHorasVoluntario |
  SalvarHorasVoluntarioSuccess |
  SalvarHorasVoluntarioFail |
  EditarHorasVoluntario |
  EditarHorasVoluntarioSuccess |
  EditarHorasVoluntarioFail |
  RemoverHorasVoluntario |
  RemoverHorasVoluntarioSuccess |
  RemoverHorasVoluntarioFail | CleanHorasVoluntario | CarregarTotalHorasVoluntario |
  CarregarTotalHorasVoluntarioSuccess |
  CarregarTotalHorasVoluntarioFail;
