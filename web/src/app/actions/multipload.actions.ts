import { Action } from "@ngrx/store/public_api";
import { MultiUploadFile } from "@app/shared/models/multiupload-file.model";

export enum MultiUploadActionTypes {
    PushArquivos = '[PushArquivo]',
    PushArquivosSuccess = '[PushArquivoSuccess]'
}

export class PushArquivos implements Action {
    readonly type = MultiUploadActionTypes.PushArquivos;
    constructor(public arquivos: MultiUploadFile[]) { }
}

export class PushArquivosSuccess implements Action {
    readonly type = MultiUploadActionTypes.PushArquivosSuccess;
    constructor() {}
}

export type MultiUploadActions = 
PushArquivos |
PushArquivosSuccess;