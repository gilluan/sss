import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { Action } from '@ngrx/store';

export enum UsuarioInstitutoActionTypes {
  CarregarUsuariosInstitutoPorFiltro = '[CarregarUsuariosInstitutoPorFiltro]',
  CarregarUsuariosInstitutoPorFiltroUnicSuccess = '[CarregarUsuariosInstitutoPorFiltroUnicSuccess]',
  CarregarUsuariosInstitutoPorFiltroSuccess = '[CarregarUsuariosInstitutoPorFiltroSuccess]',
  CarregarUsuariosInstitutoPorFiltroFail = '[CarregarUsuariosInstitutoPorFiltroFail]',
  AssinarTermoUsuarioInstituto = '[AssinarTermoUsuarioInstituto]',
  AssinarTermoUsuarioInstitutoSuccess = '[AssinarTermoUsuarioInstitutoSuccess]',
  AssinarTermoUsuarioInstitutoFail = '[AssinarTermoUsuarioInstitutoFail]',
  CarregarCooperativas = '[UsuarioInstituto CarregarCooperativas]',
  CarregarCooperativasSuccess = '[UsuarioInstituto CarregarCooperativasSuccess]',
  CarregarCooperativasFail = '[UsuarioInstituto CarregarCooperativasFail]',
}

export class CarregarCooperativas implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarCooperativas;
  constructor() { }
}
export class CarregarCooperativasSuccess implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarCooperativasSuccess;
  constructor(public payload: string[]) { }
}
export class CarregarCooperativasFail implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarCooperativasFail;
  constructor(public payload: any) { }
}

export class CarregarUsuariosInstitutoPorFiltro implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltro;
  constructor(public email?: string, public cpf?: string, public nome?: string, public unicResult?: boolean, public identificador?: string) { }
}
export class CarregarUsuariosInstitutoPorFiltroSuccess implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltroSuccess;
  constructor(public payload: UsuarioInstituto[]) { }
}

export class CarregarUsuariosInstitutoPorFiltroUnicSuccess implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltroUnicSuccess;
  constructor(public payload: UsuarioInstituto) { }
}

export class CarregarUsuariosInstitutoPorFiltroFail implements Action {
  readonly type = UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltroFail;
  constructor(public payload: any) { }
}
export class AssinarTermoUsuarioInstituto implements Action {
  readonly type = UsuarioInstitutoActionTypes.AssinarTermoUsuarioInstituto;
  constructor(public email?: string, public cpf?: string, public nome?: string) { }
}
export class AssinarTermoUsuarioInstitutoSuccess implements Action {
  readonly type = UsuarioInstitutoActionTypes.AssinarTermoUsuarioInstitutoSuccess;
  constructor(public payload: UsuarioInstituto[]) { }
}
export class AssinarTermoUsuarioInstitutoFail implements Action {
  readonly type = UsuarioInstitutoActionTypes.AssinarTermoUsuarioInstitutoFail;
  constructor(public payload: any) { }
}

export type UsuariosInstitutoAction =
  CarregarUsuariosInstitutoPorFiltro
  | CarregarUsuariosInstitutoPorFiltroSuccess
  | CarregarUsuariosInstitutoPorFiltroUnicSuccess
  | CarregarUsuariosInstitutoPorFiltroFail
  | AssinarTermoUsuarioInstituto
  | AssinarTermoUsuarioInstitutoSuccess
  | AssinarTermoUsuarioInstitutoFail
  | CarregarCooperativas
  | CarregarCooperativasSuccess
  | CarregarCooperativasFail;
