import { Store, select } from "@ngrx/store";
import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router/src/utils/preactivation";
import * as fromAuth from '@sicoob/security';
import { filter, tap, reduce, map, withLatestFrom } from "rxjs/operators";
import { UsuarioSicoob } from "@sicoob/security";
import { Observable, of } from "rxjs";

@Injectable()
export class ResolveGuard implements CanActivate {

  path: any; route: any;

  constructor(
    public authStore$: Store<fromAuth.State>
  ) { }

  canActivate(): Observable<boolean> {
    return this.authStore$.pipe(
      select(fromAuth.selectAuthState),
      withLatestFrom(i => of(i)),
      tap(console.log),
      map(([i, is]) => new Boolean(is && is.user && is.user.avatar).valueOf()),
      tap(console.log)
    );
  }
}
