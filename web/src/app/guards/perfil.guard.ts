import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import * as fromAuth from '@sicoob/security';
import { UsuarioSicoob } from '@sicoob/security';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { RoleFactory } from '../shared/controls/roles.model';
import { UsuarioInstitutoService } from '../shared/services/usuario-instituto.service';
import { PerfilType } from '../shared/types/perfil.type';

@Injectable({
  providedIn: 'root'
})
export class PerfilGuard implements CanActivate {

  constructor(
    public authStore$: Store<fromAuth.State>,
    private usuarioInstitutoService: UsuarioInstitutoService,
    private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (environment.DEV) {
      return true;
    } else {
      return this.authStore$.pipe(select(fromAuth.selectSicoobUser)).pipe(switchMap((user: UsuarioSicoob) =>
        this.findUser(user ? user.cpf : null, next.routeConfig.path)
      ));
    }
  }

  findUser(cpf: string, rota: string): Observable<boolean> {
    if (cpf) {
      return this.usuarioInstitutoService.findByCpf(cpf).pipe(
        switchMap(usuarioInstitutoFound => {
          let perfil = PerfilType.voluntario;
          if (usuarioInstitutoFound) {
            perfil = PerfilType.valueOf(usuarioInstitutoFound.perfil);
          }
          let rotasPermitidas = RoleFactory.getRole(perfil).rotas.filter(p => p == rota )[0];
          if (rotasPermitidas) {
            return of(true);
          } else {
            this.router.navigate(['/unauthorized']);
            return of(false);
          }
        }));
    } else {
      this.router.navigate(['/unauthorized']);
      return of(false);
    }
  }

}
