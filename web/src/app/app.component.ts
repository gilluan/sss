import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import * as fromAuth from '@sicoob/security';
import { RevokeAccessToken, UsuarioSicoob } from '@sicoob/security';
import { NavItem, SidebarContainerComponent, ToolbarItemEnum } from '@sicoob/ui';
import { ToolbarItem } from '@sicoob/ui/lib/toolbar/toolbar-item.model';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';
import { Observable, of, Subscription } from 'rxjs';
import * as fromApp from './app.reduce';
import { RotaModel } from '@shared/models/rota.model';
import { UsuarioInstitutoService } from '@shared/services/usuario-instituto.service';
import { PerfilType } from '@shared/types/perfil.type';
import { RoleFactory } from '@shared/controls/roles.model';
import { ScrollContainer, ChangeLang } from './app.actions';

const DEFAULT_LANG = 'pt-br';

@Component({
  selector: 'sc-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: `./app.component.html`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild(SidebarContainerComponent) sidebarContainer: SidebarContainerComponent;

  titulo = "SINS";
  isToolbarVisible: boolean = false;
  padding: number = 20;
  visibleHeader: String;
  userSubscription: Subscription;
  isScroll: boolean = false;

  itemsToolbar: ToolbarItem[];

  menus: RotaModel[];

  user$: Observable<UsuarioSicoob> = of({
    login: 'login',
    nome: 'Nome Temporário',
    cpf: '123.456.789-00',
    email: 'login@sicoob.com.br',
    numeroCooperativa: 300,
    idInstituicaoOrigem: 2,
    idUnidadeInstOrigem: 0,
    dataHoraUltimoLogin: 0,
    avatar: {
      foto: 'assets/images/ico-person.svg'
    }
  });

  /**
   * Estrutura para adicionar um item para o navbar
   */
  itemsNavbar: NavItem[] = [
    { displayName: 'Notificações', iconName: 'mail', disabled: true },
    { displayName: 'Pessoa', iconName: 'face', disabled: false },
  ];

  langs = ['en', 'es', 'pt-br'];
  currentLang: Observable<string>;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public authStore$: Store<fromAuth.State>,
    public appStore$: Store<fromApp.State>,
    private store: Store<any>,
    private changeDetector: ChangeDetectorRef,
    private usuarioInstitutoService: UsuarioInstitutoService,
    private permissionsService: NgxPermissionsService,
    private rolesService: NgxRolesService) {
    this.menus = new Array<RotaModel>();
    this.menus.push(new RotaModel('Início', 'mdi-home menuitem-inicio', 'inicio'));
    this.menus.push(new RotaModel('Plano de Ação', 'mdi-ballot-outline menuitem-plano-acao', 'plano-acao'));
    //this.menus.push(new RotaModel('Projetos', 'mdi-clipboard-text menuitem-projeto', 'projetos'));
    this.menus.push(new RotaModel('Voluntários em Análise', 'mdi-account-edit menuitem-gerenciar-voluntarios', 'analise-cadastros'));
    this.menus.push(new RotaModel('Termo', 'mdi-file-document-box menuitem-termo', 'configuracao-termo'));
    this.menus.push(new RotaModel('Banco Voluntários', 'mdi-clipboard-account menuitem-relatorio', 'relatorios/banco-voluntarios'));
    this.menus.push(new RotaModel('Assinatura', 'mdi-pen menuitem-assinatura', 'analise-cadastros/assinatura'));
    this.configureTranslate();
  }

  changeLang(lang: string) {
    this.appStore$.dispatch(new ChangeLang(lang));
  }

  ngOnInit() {
    this.currentLang = this.appStore$.pipe(select(fromApp.selectLang));
    this.user$ = of({
      'login': 'yourep0300_00',
      'nome': 'YOURE PENA FERNANDEZ',
      'cpf': '01234567890',
      'email': 'youre.fernandez@sicoob.com.br',
      'numeroCooperativa': 300,
      'idInstituicaoOrigem': 681,
      'idUnidadeInstOrigem': 0,
      'dataHoraUltimoLogin': 1578592848844
    });
    //this.authStore$.pipe(select(fromAuth.selectSicoobUser));
    this.userSubscription = this.user$.subscribe(user => this.userSubscriber(user));
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  isHeaderVisible$ = (): Observable<boolean> => this.appStore$.pipe(select(fromApp.selectVisibilityHeader));

  isToolbarVisible$ = (): Observable<boolean> => this.appStore$.pipe(select(fromApp.selectIsToolbarVisible));

  isClosed$ = (): Observable<boolean> => this.sidebarContainer ? this.sidebarContainer.isClosed$ : of(true);

  padding$ = (): Observable<number> => this.appStore$.pipe(select(fromApp.selectPadding));

  scrollToTop = () => (<HTMLInputElement>document.getElementById('scrollTo')).scrollIntoView({ behavior: 'smooth', block: 'center' });

  onWindowScroll = ($event): void => { this.isScroll = $event.path[0].scrollTop > 40; this.appStore$.dispatch(new ScrollContainer(this.isScroll)) };

  logout = () => this.authStore$.dispatch(new RevokeAccessToken());

  private userSubscriber(user: UsuarioSicoob) {
    if (user != null) {
      this.usuarioInstitutoService.findByCpf(user.cpf).subscribe(usuarioInstitutoFound => {
        let perfil = null;
        if (usuarioInstitutoFound != null) {
          perfil = usuarioInstitutoFound.perfil;
          this.itemsToolbar = this.createMenuItemsByPerfil(usuarioInstitutoFound.perfil);
        } else {
          perfil = 'voluntario';
          this.itemsToolbar = this.createMenuItemsByPerfil(perfil);
        }
        const roleObj = RoleFactory.getRole(PerfilType.valueOf(perfil));
        this.rolesService.addRoles(roleObj.role);
        this.permissionsService.loadPermissions(roleObj.permissions);
        this.changeDetector.detectChanges();
      });
    }
  }

  private configureTranslate() {
    this.translate.setDefaultLang(DEFAULT_LANG);
    this.translate.use(DEFAULT_LANG);
  }

  private createMenuItemsByPerfil(perfil: string): ToolbarItem[] {
    const arrayItems = [];
    const perfilControl = RoleFactory.getRole(PerfilType.valueOf(perfil));
    for (let i = 0; i < perfilControl.rotas.length; i++) {
      const rotaPermitida = perfilControl.rotas[i];
      for (let j = 0; j < this.menus.length; j++) {
        const menu = this.menus[j];
        if (rotaPermitida === menu.route) {
          arrayItems.push({
            displayName: menu.displayName,
            iconName: menu.iconName,
            route: `/${menu.route}`,
            type: ToolbarItemEnum.LINK
          });
        }
      }
    }
    return arrayItems;
  }

}
