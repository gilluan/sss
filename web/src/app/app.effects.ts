import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { AppActionTypes, AddGameUserFail, AddGameUser, AddGameUserSuccess } from './app.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { CustomAlertService } from './shared/services/alert-service';
import { Color } from '@sicoob/ui';
import { UsuarioInstituto } from './shared/models/usuario-instituto.model';
import { UsuarioInstitutoService } from './shared/services/usuario-instituto.service';
import { VoluntariosService } from './funcionalidades/sins-analise-cadastros/voluntarios.service';
import { of, Observable } from 'rxjs';
import { Game } from './shared/models/game.model';


@Injectable()
export class AppEffects {

  constructor(private actions$: Actions,
    private alertService: CustomAlertService,
    private _usuarioInstitutoService: UsuarioInstitutoService,
    private _voluntarioService: VoluntariosService) { }

  @Effect()
  addGameUser$ =
    this.actions$
      .ofType(AppActionTypes.AddGameUser).pipe(
        map((action: AddGameUser) => action),
        switchMap((action: AddGameUser) => {
          const observable: Observable<Game[]> = action.voluntario ? this._voluntarioService.findGameByCpf(action.cpf) : this._usuarioInstitutoService.findGameByCpf(action.cpf);
          return observable.pipe(
            map((game: Game[]) => new AddGameUserSuccess(game)),
            catchError(error => of(new AddGameUserFail(error))))
        }));

  @Effect({ dispatch: false })
  addGameUserFail$ =
    this.actions$
      .ofType(AppActionTypes.AddGameUserFail).pipe(
        map((action: AddGameUserFail) => action),
        map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar a pontuação do usuário.')));


}
