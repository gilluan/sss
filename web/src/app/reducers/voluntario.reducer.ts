import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { VoluntarioAction, VoluntarioActionTypes } from '../actions/voluntarioAction';
import { TotalSituacaoVm } from '../funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { PageModelVm } from '../shared/models/page.model';
import { PagedDataModelVm } from '../shared/models/paged-data.model';
import { VoluntarioDto } from '../shared/models/voluntario-dto.model';
import { VoluntarioFiltroDto } from '../shared/models/voluntario-filtro.model';

export interface State extends EntityState<VoluntarioDto> {
  pagination: PageModelVm;
  cooperativas: string[];
  totais: TotalSituacaoVm[];
  filtro: VoluntarioFiltroDto;
  total: number;
  totalAssinatura: number;
  totalBancoVoluntario: number;
  totalAnaliseVoluntario: number;
  voluntarioAtual: VoluntarioDto;
}

export const adapter: EntityAdapter<VoluntarioDto> = createEntityAdapter<VoluntarioDto>();

export const initialState: State = adapter.getInitialState({
  pagination: new PageModelVm(0, 6, 0),
  cooperativas: [],
  totais: [],
  filtro: new VoluntarioFiltroDto(),
  total: 0,
  totalAssinatura: 0,
  totalBancoVoluntario: 0,
  totalAnaliseVoluntario: 0,
  voluntarioAtual: null,
});

export function reducer(state = initialState, action: VoluntarioAction): State {
  switch (action.type) {

    case VoluntarioActionTypes.CarregarVoluntariosPorFiltro: {
      return { ...state, filtro: action.filter };
    }

    case VoluntarioActionTypes.CarregarVoluntariosPorFiltroSuccess: {
      return adapter.addAll(action.payload.data, { ...state, pagination: action.payload.page });
    }

    case VoluntarioActionTypes.CarregarVoluntariosPorFiltroFail: {
      return { ...state, ids: [], entities: {}, pagination: initialState.pagination };
    }

    case VoluntarioActionTypes.DesativarVoluntarioFail: {
      return { ...state };
    }

    case VoluntarioActionTypes.CarregarCooperativasSuccess: {
      state.cooperativas = action.payload;
      return { ...state };
    }

    case VoluntarioActionTypes.CarregarCooperativasFail: {
      state.cooperativas = initialState.cooperativas;
      return { ...state };
    }

    case VoluntarioActionTypes.CarregarTotaisSuccess: {
      state.totais = action.payload;
      return { ...state };
    }

    case VoluntarioActionTypes.CarregarTotaisFail: {
      state.totais = initialState.totais;
      return { ...state };
    }

    case VoluntarioActionTypes.AssinarContratoVoluntarioSuccess: {
      return adapter.removeOne(action.payload.id, state);
    }

    case VoluntarioActionTypes.CarregarTotalPorFiltroSuccess: {
      state.total = action.total;
      return { ...state };
    }

    case VoluntarioActionTypes.CarregarTotalPorFiltroFail: {
      return { ...state, total: initialState.total };
    }

    case VoluntarioActionTypes.CarregarTotalAssinarSuccess: {
      return { ...state, totalAssinatura: action.total };
    }

    case VoluntarioActionTypes.CarregarTotalBancoVoluntarioSuccess: {
      return { ...state, totalBancoVoluntario: action.total };
    }

    case VoluntarioActionTypes.CarregarTotalAnaliseSuccess: {
      return { ...state, totalAnaliseVoluntario: action.total };
    }

    case VoluntarioActionTypes.CarregarVoluntariosPorCPFSuccess: {
      return { ...state, voluntarioAtual: action.payload };
    }

    case VoluntarioActionTypes.CarregarVoluntariosPorIdSuccess: {
      return { ...state, voluntarioAtual: action.payload };
    }




    default: {
      return state;
    }
  }
}

export const selectVoluntario = createFeatureSelector<State>('voluntarioSelector');

export const selectVoluntarioPaginado = createSelector(
  selectVoluntario, (state: State) => new PagedDataModelVm(state.pagination, Object.values(state.entities)));

export const selectVoluntariosCooperativas = createSelector(selectVoluntario, (state: State) => state.cooperativas);

export const selectFiltro = createSelector(selectVoluntario, (state: State) => state.filtro);

export const selectVolunatarioPaginadoComTotais = createSelector(selectVoluntario, (state: State) => {
  return { totais: state.totais, dadosPaginados: new PagedDataModelVm(state.pagination, Object.values(state.entities)) };
});

export const selectTotalPorFiltro = createSelector(selectVoluntario, (state: State) => state.total);

export const getTotalAssinatura = createSelector(
  selectVoluntario,
  state => {
    return state.totalAssinatura;
  });

export const getTotalBancoVoluntario = createSelector(
  selectVoluntario,
  state => {
    return state.totalBancoVoluntario;
  });

export const getTotalAnaliseVoluntario = createSelector(
  selectVoluntario,
  state => {
    return state.totalAnaliseVoluntario;
  });

export const getVoluntarioAtual = createSelector(
  selectVoluntario,
  state => {
    return state.voluntarioAtual;
  });
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(selectVoluntario);
