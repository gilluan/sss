import { SinsFileInProgressStates } from '@app/shared/components/sins-multiupload/sins-file-system-file-entry.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GedActions, GedActionTypes } from '../actions/ged.actions';
import { DadosDocumento } from '@shared/ged/models/retorno/dadosDocumento.model';
import { MultiUploadFile } from '@app/shared/models/multiupload-file.model';


export interface State extends EntityState<DadosDocumento> {
  identificadorGed: string;
  thumbnail: any;
  fileUploadProgress: number;
  indiceAtual: number;
  fileUploadMultipleProgresso: Array<MultiUploadFile>;
  fileUploadMultiple: Array<MultiUploadFile>;
}

export const adapter: EntityAdapter<DadosDocumento> = createEntityAdapter<DadosDocumento>({
  selectId: dadosDocumento => dadosDocumento.idDocumento
});

export const initialState: State = adapter.getInitialState({
  identificadorGed: null,
  thumbnail: null,
  fileUploadProgress: 0,
  fileUploadMultipleProgresso: [],
  fileUploadMultiple: [],
  indiceAtual: 0,
});

export function reducer(state = initialState, action: GedActions): State {
  switch (action.type) {

    case GedActionTypes.SalvarArquivosMultiUpload: {
      if (state.indiceAtual < action.multiUploadFiles.length) {
        state.fileUploadMultipleProgresso[state.indiceAtual] = action.multiUploadFiles[state.indiceAtual];
      }
      return { ...state };
    }

    case GedActionTypes.CriarArquivoFail: {

      return { ...state, indiceAtual: 0, fileUploadMultiple: [], fileUploadMultipleProgresso: [] }
    }

    case GedActionTypes.CriarArquivoSuccess: {
      let array = [];
      const file = state.fileUploadMultipleProgresso[state.indiceAtual];
      if (file) {
        state.fileUploadMultipleProgresso[state.indiceAtual] = {
          nome: file.nome,
          tamanho: file.tamanho,
          identificadorGed: action.identificadorGed,
          progress: 100,
          contentType: file.contentType,
          key: file.key,
          estado: SinsFileInProgressStates.Finalizado,
          error: false,
        }
        array = [...state.fileUploadMultipleProgresso];
      }
      return { ...state, identificadorGed: action.identificadorGed, fileUploadMultipleProgresso: array };
    }

    case GedActionTypes.SalvarArquivosMultiUploadSuccess: {
      state.indiceAtual++;
      return { ...state };
    }

    case GedActionTypes.CleanIndice: {
      state.fileUploadMultiple = state.fileUploadMultipleProgresso;
      state.fileUploadMultipleProgresso = [];
      state.indiceAtual = 0;
      return { ...state };
    }

    case GedActionTypes.CarregarArquivoSuccess: {
      return adapter.addOne(action.dadosDocumento, state);
    }
    case GedActionTypes.LimparStoreGed: {
      return adapter.removeAll({
        ...state,
        identificadorGed: null,
        fileUploadProgress: 0,
        fileUploadMultipleProgresso: [],
        fileUploadMultiple: []
      });
    }
    case GedActionTypes.UploadProgress: {
      let array = [];
      const file = state.fileUploadMultipleProgresso[state.indiceAtual];
      if (file) {
        state.fileUploadMultipleProgresso[state.indiceAtual] = {
          nome: file.nome,
          tamanho: file.tamanho,
          identificadorGed: null,
          progress: action.progress,
          key: file.key,
          contentType: file.contentType,
          estado: SinsFileInProgressStates.EmProgresso,
          error: false,
        }
        array = [...state.fileUploadMultipleProgresso];
      }
      return { ...state, fileUploadProgress: action.progress, fileUploadMultipleProgresso: array };
    }
    default: {
      return state;
    }
  }
}

export const getArquivoGedState = createFeatureSelector<State>('arquivoGedSelector');

export const selectArquivoGed =
  createSelector(getArquivoGedState,
    (state: State) => state && state.entities ? Object.values(state.entities)[0] : {} as any);

export const selectIdentificadorGed =
  createSelector(getArquivoGedState,
    (state: State) => state.identificadorGed);

export const getFileUploadProgress =
  createSelector(getArquivoGedState,
    (state: State) => state.fileUploadProgress);

export const getFileUploadMultipleProgresso =
  createSelector(getArquivoGedState,
    (state: State) => state.fileUploadMultipleProgresso);

export const getFileUploadMultiple =
  createSelector(getArquivoGedState,
    (state: State) => state.fileUploadMultiple);

export const getIndiceAtual =
  createSelector(getArquivoGedState,
    (state: State) => state.indiceAtual);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getArquivoGedState);

