import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PageModelVm } from '@app/shared/models/page.model';
import { HorasVoluntario } from '@app/shared/models/horas-voluntario.model';
import { HorasVoluntarioTypes, HorasVoluntarioActions } from '@app/actions/horas-voluntario.actions';

export interface State extends EntityState<HorasVoluntario> {
  // additional entities state properties
  horasVoluntario: HorasVoluntario[],
  paginacaoAtual: PageModelVm;
  totalHoras: number;
  loading: boolean;
}

export const adapter: EntityAdapter<HorasVoluntario> = createEntityAdapter<HorasVoluntario>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  horasVoluntario: [],
  paginacaoAtual: new PageModelVm(0, 5, 0),
  totalHoras: 0,
  loading: true,
});

export function reducer(state = initialState, action: HorasVoluntarioActions): State {
  switch (action.type) {
    case HorasVoluntarioTypes.CarregarHorasVoluntario: {
      return {
        ...state, loading: true
      }
    }
    case HorasVoluntarioTypes.CarregarHorasVoluntarioSuccess: {
      state.horasVoluntario = action.HorasVoluntario && action.HorasVoluntario.data.length > 0 ? state.horasVoluntario.concat(action.HorasVoluntario.data) : [];
      let paginacaoAtual = new PageModelVm(action.HorasVoluntario.page.pageNumber, action.HorasVoluntario.page.pageSize, action.HorasVoluntario.page.total);
      return {
        ...state, paginacaoAtual, loading: false
      }
    }
    case HorasVoluntarioTypes.CarregarHorasVoluntarioFail: {
      return {
        ...state, loading: false
      }
    }
     case HorasVoluntarioTypes.CarregarTotalHorasVoluntarioSuccess: {
      return {
        ...state, totalHoras: action.horas
      }
    }
    case HorasVoluntarioTypes.CleanHorasVoluntario: {
      return action.all ? {...state, paginacaoAtual: initialState.paginacaoAtual, horasVoluntario: [], totalHoras: 0, loading: true} : {...state, horasVoluntario: [], totalHoras: 0, loading: true}
    }

    default: {
      return state;
    }
  }
}

export const getHorasVoluntariosSate = createFeatureSelector<State>('horasVoluntarioState');

export const getHorasVoluntarios = createSelector(
  getHorasVoluntariosSate,
  state => {
    return state.horasVoluntario;
  });


export const getPaginacaoAtual = createSelector(
  getHorasVoluntariosSate,
  state => {
    return state.paginacaoAtual
  }
)

export const isLoading = createSelector(
  getHorasVoluntariosSate,
  state => {
    return state.loading
  }
)

export const getTotalHorasVoluntario = createSelector(
  getHorasVoluntariosSate,
  state => {
    return state.totalHoras
  }
)


