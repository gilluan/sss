import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromRouter from '@ngrx/router-store';
import * as fromTermoVoluntario from '../funcionalidades/termo-voluntario/reducers/termo-voluntario.reducer';
import * as fromSinsHome from '../funcionalidades/sins-home/reducers/sins-home.reducer';
import * as fromConfiguracaoTermo from '../funcionalidades/configuracao-termo/reducers/configuracao-termo.reducer';
import * as fromApp from '../app.reduce';
import * as fromVoluntario from '../reducers/voluntario.reducer';
import * as fromAuth from '@sicoob/security';
import * as fromAcompanhamentoCadastro from '../funcionalidades/acompanhamento-cadastro/reducers/acompanhamento-cadastro.reducer';
import * as fromPlanoAcao from '../funcionalidades/plano-acao/reducers/plano-acao.reducer';
import * as fromSinsRelatorios from '../funcionalidades/sins-relatorios/reducers/sins-relatorios.reducer';
import * as fromGed from '../reducers/ged.reducer';
import * as fromhorasVoluntarioState from '../reducers/horas-voluntario.reducers';
import * as fromPerfilUsuario from '../funcionalidades/perfil-usuario/reducers/perfil-usuario.reducer';
import * as fromProgramas from '../funcionalidades/programas/reducers/programas.reducer';
import * as fromFluxoProcesso from '../funcionalidades/fluxo-processo/reducers/fluxo-processo.reducer';
import * as fromMarco from '../funcionalidades/fluxo-processo/reducers/marco.reducer';
import * as fromPortfolio from '../funcionalidades/portfolio/reducers/portfolio.reducer';

export interface State {
  router: fromRouter.RouterReducerState;
  sinsHome: fromSinsHome.State;
  configuracaoTermo: fromConfiguracaoTermo.State;
  termoVolutario: fromTermoVoluntario.State;
  auth: fromAuth.State;
  acompanhamentoCadastro: fromAcompanhamentoCadastro.State;
  app: fromApp.State;
  planoAcao: fromPlanoAcao.State;
  sinsRelatorios: fromSinsRelatorios.State;
  voluntario: fromVoluntario.State;
  ged: fromGed.State;
  perfilUsuario: fromPerfilUsuario.State;
  programaSelector: fromProgramas.State;
  fluxoProcesso: fromFluxoProcesso.State;
  marcoState: fromMarco.State;
  portfolio: fromPortfolio.State;
  horasVoluntarioState: fromhorasVoluntarioState.State;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
  sinsHome: fromSinsHome.reducer,
  configuracaoTermo: fromConfiguracaoTermo.reducer,
  termoVolutario: fromTermoVoluntario.reducer,
  auth: fromAuth.reducer,
  acompanhamentoCadastro: fromAcompanhamentoCadastro.reducer,
  app: fromApp.reducer,
  planoAcao: fromPlanoAcao.reducer,
  sinsRelatorios: fromSinsRelatorios.reducer,
  voluntario: fromVoluntario.reducer,
  ged: fromGed.reducer,
  perfilUsuario: fromPerfilUsuario.reducer,
  programaSelector: fromProgramas.reducer,
  fluxoProcesso: fromFluxoProcesso.reducer,
  marcoState: fromMarco.reducer,
  portfolio: fromPortfolio.reducer,
  horasVoluntarioState: fromhorasVoluntarioState.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.PRODUCTION ? [] : [];
