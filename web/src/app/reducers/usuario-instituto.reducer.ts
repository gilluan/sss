import { UsuarioInstitutoActionTypes, UsuariosInstitutoAction } from '@app/actions/usuario-instituto.actions';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface State extends EntityState<UsuarioInstituto> {
  usuarioInstitutoAtual: UsuarioInstituto;
  usuariosInsitutos: UsuarioInstituto[];
  cooperativas: string[]
}

export const adapter: EntityAdapter<UsuarioInstituto> = createEntityAdapter<UsuarioInstituto>();

export const initialState: State = adapter.getInitialState({
  usuarioInstitutoAtual: null,
  usuariosInsitutos: [],
  cooperativas: []
});

export function reducer(state = initialState, action: UsuariosInstitutoAction): State {
  switch (action.type) {
    case UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltroSuccess: {
      return { ...state, usuariosInsitutos: action.payload };
    }
    case UsuarioInstitutoActionTypes.CarregarUsuariosInstitutoPorFiltroUnicSuccess: {
      return { ...state, usuarioInstitutoAtual: action.payload };
    }
    case UsuarioInstitutoActionTypes.CarregarCooperativasSuccess: {
      return { ...state, cooperativas: action.payload };
    }
    default: {
      return state;
    }
  }
}

export const usuarioInstitutoState = createFeatureSelector<State>('usuarioInsitutoState');

export const selectCooperativas = createSelector(usuarioInstitutoState, (state: State) => state.cooperativas);


export const getUsuarioInstitutoAtual = createSelector(
  usuarioInstitutoState,
  state => {
    return state.usuarioInstitutoAtual;
  });

export const getUsuariosInstituto = createSelector(
  usuarioInstitutoState,
  state => {
    return state.usuariosInsitutos;
  });

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(usuarioInstitutoState);

