import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitialRedirectorRoutingModule } from './initial-redirector-routing.module';
import { InitialRedirectorComponent } from './initial-redirector.component';
import { PersistanceService } from '../shared/services/persistence.service';
import { SinsLoadingService } from '../shared/components/sins-loading/sins-loading.service';
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [InitialRedirectorComponent],
  imports: [
    CommonModule,
    InitialRedirectorRoutingModule,
    SharedModule
  ],
  providers: [PersistanceService, SinsLoadingService]
})
export class InitialRedirectorModule { }
