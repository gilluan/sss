import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VoluntariosService } from '@app/funcionalidades/sins-analise-cadastros/voluntarios.service';
import { VoluntarioDto } from '@app/shared/models/voluntario-dto.model';
import { select, Store } from '@ngrx/store';
import { UsuarioInstituto } from '@shared/models/usuario-instituto.model';
import { LoaderService } from '@shared/services/loader.service';
import { PersistanceService } from '@shared/services/persistence.service';
import { UsuarioInstitutoService } from '@shared/services/usuario-instituto.service';
import { PerfilType } from '@shared/types/perfil.type';
import { SituacaoVoluntarioType } from '@shared/types/situacao-voluntario.type';
import * as fromAuth from '@sicoob/security';
import { UsuarioSicoob } from '@sicoob/security';
import { Subscription, of } from 'rxjs';
import { AddGameUser, DisplayToolbar } from 'src/app/app.actions';
import * as fromApp from '../app.reduce';

@Component({
  selector: 'sc-initial-redirector',
  template: `<sc-loader></sc-loader>`,
  styles: []
})
export class InitialRedirectorComponent implements OnInit, OnDestroy {
  private user: UsuarioSicoob;
  private voluntario: VoluntarioDto;
  private usuarioInstituto: UsuarioInstituto;
  private subscription: Subscription = new Subscription();

  constructor(
    private voluntarioService: VoluntariosService,
    private usuarioInstitutoService: UsuarioInstitutoService,
    private router: Router,
    public authStore$: Store<fromAuth.State>,
    public appStore$: Store<fromApp.State>,
    private persistenceService: PersistanceService,
    private loaderService: LoaderService) { }

  ngOnInit(): void {
    this.loaderService.show();
    this.appStore$.dispatch(new DisplayToolbar(false));
    let user$ = of({
      'login': 'yourep0300_00',
      'nome': 'YOURE PENA FERNANDEZ',
      'cpf': '01234567890',
      'email': 'youre.fernandez@sicoob.com.br',
      'numeroCooperativa': 300,
      'idInstituicaoOrigem': 681,
      'idUnidadeInstOrigem': 0,
      'dataHoraUltimoLogin': 1578592848844
    });
    //user$ = this.authStore$.pipe(select(fromAuth.selectSicoobUser));
    this.subscription.add(user$.subscribe((user: UsuarioSicoob) => {
      if (user) {
        this.user = user;
        this.subscription.add(this.usuarioInstitutoService.findByCpf(this.user.cpf).subscribe(found => {
          this.usuarioInstituto = found ? Object.assign(new UsuarioInstituto(), found) : null;
          if (this.usuarioInstituto != null) {
            this.navigateSins();
          } else {
            this.voluntarioService.findVoluntarioByCpf(this.user.cpf).subscribe(voluntarioFound => {
              this.voluntario = voluntarioFound;
              this.navigateSins();
            },
              (error: any) => {
                console.log(error);
                this.loaderService.hide();
              });
          }
        },
          (error: any) => {
            console.log(error);
            this.loaderService.hide();
          }));
      }
    },
      (error: any) => {
        console.log(error);
        this.loaderService.hide();
      }
    ));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.loaderService.hide();
  }

  private setDadosUsuario(isUsuarioCadastrado: boolean, isUsuarioVoluntario: boolean) {
    const perfil = (isUsuarioVoluntario ? 'voluntario' : this.usuarioInstituto.perfil);
    const isVoluntario = perfil === 'voluntario';
    const isSigned = isVoluntario ? null : this.usuarioInstituto.isSigned;
    let situacao = null;
    if (this.voluntario) {
      situacao = this.voluntario.situacao;
    }
    const dadosUsuario = {
      perfil: perfil,
      perfilName: this.obterPerfil(perfil),
      isCadastrado: isUsuarioCadastrado,
      isVoluntario: isUsuarioVoluntario,
      numeroCooperativa: this.user.numeroCooperativa,
      cpf: this.user.cpf ? this.user.cpf.trim() : null,
      nome: this.user.nome ? this.user.nome.trim() : null,
      email: this.user.email ? this.user.email.trim().toLowerCase() : null,
      idInstituicaoOrigem: this.user.idInstituicaoOrigem,
      idUnidadeInstOrigem: this.user.idUnidadeInstOrigem,
      situacao: situacao,
      termoAssinado: isSigned,
      id: this.populateIdUsuario()
    };
    this.appStore$.dispatch(new AddGameUser(dadosUsuario.cpf, isVoluntario))
    this.persistenceService.set('usuario_instituto', dadosUsuario);
  }

  private navigateToWelcome(isUsuarioCadastrado: boolean, isUsuarioVoluntario: boolean) {
    this.setDadosUsuario(isUsuarioCadastrado, isUsuarioVoluntario);
    this.router.navigate(['/welcome']);
  }

  private navigateToTermoVoluntario(isUsuarioCadastrado: boolean, isUsuarioVoluntario: boolean) {
    this.setDadosUsuario(isUsuarioCadastrado, isUsuarioVoluntario);
    this.router.navigate(['/welcome/termo-voluntario']);
  }

  private navigateToTermoPaePde() {
    this.setDadosUsuario(true, false);
    this.router.navigate(['/welcome/termo-voluntario']);
  }

  private navigateToAcompanhamentoCadastro() {
    this.setDadosUsuario(true, true);
    this.router.navigate(['/acompanhamento-cadastro']);
  }

  private navigateToInicio(isVoluntario: boolean) {
    this.setDadosUsuario(true, isVoluntario);
    this.router.navigate(['/inicio']);
  }

  private navigateSins() {
    if (this.usuarioInstituto) {
      if ([PerfilType.gestor, PerfilType.ppe].indexOf(PerfilType.valueOf(this.usuarioInstituto.perfil)) > -1 ||
        this.usuarioInstituto.isSigned) {
        this.navigateToInicio(false);
      } else {
        this.navigateToTermoPaePde();
      }
    } else {
      if (this.voluntario && this.voluntario.situacao) {
        switch (this.voluntario.situacao) {
          case SituacaoVoluntarioType.atualizacaoTermo: {
            this.navigateToWelcome(true, true);
          } break;
          case SituacaoVoluntarioType.aguardandoAssinaturaTermo: {
            this.navigateToTermoVoluntario(true, true);
          } break;
          case SituacaoVoluntarioType.assinado: {
            this.navigateToInicio(true);
          } break;
          default: {
            this.navigateToAcompanhamentoCadastro();
          }
        }

      } else {
        this.navigateToWelcome(false, true);
      }
    }
  }

  private populateIdUsuario() {
    return this.voluntario && this.voluntario.id ? this.voluntario.id :
      this.usuarioInstituto && this.usuarioInstituto.id ? this.usuarioInstituto.id : null;
  }

  obterPerfil = (perfil: string) => PerfilType.obterPerfil(perfil);
}
