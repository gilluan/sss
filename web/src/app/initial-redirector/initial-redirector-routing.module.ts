import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitialRedirectorComponent } from './initial-redirector.component';

const routes: Routes = [
  {
    path: 'initial-redirector',
    component: InitialRedirectorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitialRedirectorRoutingModule { }
