import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Validations } from '@shared/utils/validations';
import { ActionBarRef, ActionBarService, Color, ModalService } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { EditarCurso, EditarCursoFail, EditarCursoSuccess, EditarCursoVoluntario, EditarCursoVoluntarioFail, EditarCursoVoluntarioSuccess, ExcluirCurso, ExcluirCursoFail, ExcluirCursoSuccess, ListarCategoriasCurso, ListarCategoriasCursoFail, ListarCategoriasCursoSuccess, ListarCurso, ListarCursoFail, ListarCursos, ListarCursosFail, ListarCursosSuccess, ListarCursoSuccess, PerfilUsuarioActionTypes, SalvarCurso, SalvarCursoFail, SalvarCursoSuccess, SalvarCursoVoluntario, SalvarCursoVoluntarioFail, SalvarCursoVoluntarioSuccess, LoadConvitesSuccess, LoadConvitesFail, LoadConvites, TotalConvites, TotalConvitesSuccess, TotalConvitesFail, ParticiparProjeto, ParticiparProjetoFail, ParticiparProjetoSuccess, LoadProjetos, LoadProjetosSuccess, LoadProjetosFail, TotalProjetos, TotalProjetosSuccess, TotalProjetosFail } from '../actions/perfil-usuario.actions';
import { ActionBarCadastroCursoComponent } from '../components/action-bar-cadastro-curso/action-bar-cadastro-curso.component';
import { CategoriaCurso } from '../models/categoria-curso.model';
import { CursoFiltro } from '../models/curso.filtro';
import { Curso } from '../models/curso.model';
import { PerfilUsuarioService } from '../perfil-usuario.service';
import * as fromPerfilUsuario from '../reducers/perfil-usuario.reducer';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { DialogoSucessoComponent } from '@app/shared/components/dialogo-sucesso/dialogo-sucesso.component';

@Injectable()
export class PerfilUsuarioEffects {

  constructor(
    private ngZone: NgZone,
    private router: Router,
    private actions$: Actions,
    private persistenceService: PersistanceService,
    private perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private _perfilUsuarioService: PerfilUsuarioService,
    private actionBarService: ActionBarService,
    private _alertService: CustomAlertService,
    private _modalService: ModalService) { }


  @Effect()
  editarCursoVoluntario$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.EditarCursoVoluntario),
    map((action: EditarCursoVoluntario) => action),
    switchMap((action: EditarCursoVoluntario) => this._perfilUsuarioService.listarCursos(new CursoFiltro(action.curso.voluntario, null, action.curso.nome)).pipe(
      switchMap((c: Curso[]) => {
        let cursoOld = c[0];
        action.curso.id = cursoOld.id;
        action.curso.categoriaCurso = cursoOld.categoriaCurso;
        action.curso.descricao = cursoOld.descricao;
        action.curso.dataConclusao = cursoOld.dataConclusao;
        return this._perfilUsuarioService.editarCurso(action.curso).pipe(
          map((curso: Curso) => new EditarCursoVoluntarioSuccess(curso)),
          catchError(error => of(new EditarCursoVoluntarioFail(error))));
      }))));

  @Effect({ dispatch: false })
  editarCursoVoluntarioSucess$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.EditarCursoVoluntarioSuccess),
    map(_ => this.ngZone.run(() => this.router.navigate(['/acompanhamento-cadastro'])).then()));

  @Effect()
  salvarCursoVoluntario$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.SalvarCursoVoluntario),
    map((action: SalvarCursoVoluntario) => action),
    switchMap((action: SalvarCursoVoluntario) => this._perfilUsuarioService.listarCategoriasCurso('Consciência Social').pipe(
      switchMap((c: CategoriaCurso[]) => {
        action.curso.categoriaCurso = c[0].id;
        return this._perfilUsuarioService.salvarCurso(action.curso).pipe(
          map((curso: Curso) => new SalvarCursoVoluntarioSuccess(curso)),
          catchError(error => of(new SalvarCursoVoluntarioFail(error))));
      }))));

  @Effect({ dispatch: false })
  salvarCursoVoluntarioSucess$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.SalvarCursoVoluntarioSuccess),
    map(_ => this.ngZone.run(() => this.router.navigate(['/acompanhamento-cadastro'])).then()));

  @Effect()
  salvarCurso$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.SalvarCurso),
    map((action: SalvarCurso) => action),
    switchMap((action: SalvarCurso) => this._perfilUsuarioService.salvarCurso(action.curso).pipe(
      map((curso: Curso) => new SalvarCursoSuccess(curso, action.actionBar)),
      catchError(error => of(new SalvarCursoFail(error, action.actionBar))))));

  @Effect()
  salvarCursoSucess$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.SalvarCursoSuccess),
      map((action: SalvarCursoSuccess) => {
        Validations.notNull(action.actionBar, (ref: ActionBarRef) => ref.close());
        this._alertService.abrirAlert(Color.SUCCESS, 'Capacitação adicionada com sucesso.');
        return new ListarCursos(new CursoFiltro(action.curso.voluntario, action.curso.usuarioInstituto));
      }));

  @Effect({ dispatch: false })
  salvarCursoFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.SalvarCursoFail),
    map((action: SalvarCursoFail) => {
      action.actionBar.close();
      this._alertService.abrirAlert(Color.DANGER, 'Erro ao adicionar Capacitação.');
    }));


  @Effect()
  participarProjeto$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.ParticiparProjeto),
      map((action: ParticiparProjeto) => action),
      switchMap((action: ParticiparProjeto) => this._perfilUsuarioService.participarProjeto(action.idProjeto, action.idVoluntario).pipe(
        switchMap((mensagem: string) => [new ParticiparProjetoSuccess(mensagem), new LoadConvites(action.idVoluntario, new PageModelVm(0, 10, 0)), new TotalConvites(action.idVoluntario)]),
        catchError(error => of(new ParticiparProjetoFail(error))))));

  @Effect({ dispatch: false })
  participarProjetoSuccess$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ParticiparProjetoSuccess),
    map((action: ParticiparProjetoSuccess) => {
      const data: any = {
        header: 'Parabéns',
        message: action.mensagem,
        buttonClass: 'mdi-check-circle-outline',
        rota: this.getUrl(),
        textButton: 'OK'
      };
      this._modalService.open(DialogoSucessoComponent, { data: data, panelClass: 'sins-modal-success' });
    }));

  private getUrl() {
    let pieces = this.router.url.split('/');
    pieces[3] = 'projetos';
    return pieces.join('/');
  }

  @Effect({ dispatch: false })
  participarFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ParticiparProjetoFail),
    map((action: ParticiparProjetoFail) => this._alertService.abrirAlert(Color.DANGER, action.error)));

  @Effect()
  loadConvites$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.LoadConvites),
      map((action: LoadConvites) => action),
      switchMap((action: LoadConvites) => this._perfilUsuarioService.listarConvites(action.id, action.paginacao).pipe(
        map((convites: PagedDataModelVm) => new LoadConvitesSuccess(convites)),
        catchError(error => of(new LoadConvitesFail(error))))));

  @Effect({ dispatch: false })
  loadConvitesFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.LoadConvitesFail),
    map(_ => this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel encontrar os convites.')));

  @Effect()
  totalConvites$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.TotalConvites),
      map((action: TotalConvites) => action),
      switchMap((action: TotalConvites) => this._perfilUsuarioService.totalConvites(action.id).pipe(
        map((total: number) => new TotalConvitesSuccess(total)),
        catchError(error => of(new TotalConvitesFail(error))))));

  @Effect({ dispatch: false })
  totalConvitesFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.TotalConvitesFail),
    map(_ => this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel contabilizar os convites.')));

  @Effect()
  loadProjetos$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.LoadProjetos),
      map((action: LoadProjetos) => action),
      switchMap((action: LoadProjetos) => this._perfilUsuarioService.listarProjetos(action.id, action.paginacao).pipe(
        map((convites: PagedDataModelVm) => new LoadProjetosSuccess(convites)),
        catchError(error => of(new LoadProjetosFail(error))))));

  @Effect({ dispatch: false })
  loadProjetosFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.LoadProjetosFail),
    map(_ => this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel encontrar os projetos.')));

  @Effect()
  totalProjetos$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.TotalProjetos),
      map((action: TotalProjetos) => action),
      switchMap((action: TotalProjetos) => this._perfilUsuarioService.totalProjetos(action.id).pipe(
        map((total: number) => new TotalProjetosSuccess(total)),
        catchError(error => of(new TotalProjetosFail(error))))));

  @Effect({ dispatch: false })
  totalProjetosFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.TotalProjetosFail),
    map(_ => this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel contabilizar os projetos.')));

  @Effect()
  listarCurso$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.ListarCurso),
      map((action: ListarCurso) => action),
      switchMap((action: ListarCurso) => this._perfilUsuarioService.listarCursoById(action.id).pipe(
        map((curso: Curso) => new ListarCursoSuccess(curso)),
        catchError(error => of(new ListarCursoFail(error))))));

  @Effect({ dispatch: false })
  listarCursoSucess$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCursoSuccess),
    map((action: ListarCursoSuccess) => {
      this.actionBarService.open(ActionBarCadastroCursoComponent, {});
      return new ListarCursos(new CursoFiltro(action.curso.voluntario, action.curso.usuarioInstituto));
    }));

  @Effect({ dispatch: false })
  listarCursoFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCursoFail),
    map((action: ListarCursoFail) => {
      this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel encontrar o curso.');
      console.log(action.error);
    }));

  @Effect()
  listarCursos$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCursos),
    map((action: ListarCursos) => action),
    switchMap((action: ListarCursos) => this._perfilUsuarioService.listarCursos(action.filtro).pipe(
      map((cursos: Curso[]) => new ListarCursosSuccess(cursos)),
      catchError(error => of(new ListarCursosFail(error))))));

  @Effect({ dispatch: false })
  listarCursosFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCursosFail),
    map((action: ListarCursosFail) => {
      this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel carrega os cursos.');
      console.log(action.error);
    }));

  @Effect()
  editarCurso$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.EditarCurso),
    map((action: EditarCurso) => action),
    switchMap((action: EditarCurso) => this._perfilUsuarioService.editarCurso(action.curso).pipe(
      map((curso: Curso) => new EditarCursoSuccess(curso, action.actionBar)),
      catchError(error => of(new EditarCursoFail(error, action.actionBar))))));

  @Effect()
  editarCursoSucess$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.EditarCursoSuccess),
      map((action: EditarCursoSuccess) => {
        action.actionBar.close();
        this._alertService.abrirAlert(Color.SUCCESS, 'Capacitação editada com sucesso.');
        let voluntarioB = this.persistenceService.get("usuario_instituto").perfil == 'voluntario' ? true : false;
        let voluntario = voluntarioB ? this.persistenceService.get("usuario_instituto").id : null;
        let usuarioInstituto = voluntarioB ? null : this.persistenceService.get("usuario_instituto").id;
        return new ListarCursos(new CursoFiltro(voluntario, usuarioInstituto));
      }));

  @Effect({ dispatch: false })
  editarCursoFail$ =
    this.actions$.pipe(
      ofType(PerfilUsuarioActionTypes.EditarCursoFail),
      map((action: EditarCursoFail) => {
        action.actionBar.close();
        this._alertService.abrirAlert(Color.DANGER, 'Erro ao editar Capacitação.');
        console.log(action.error);
      }));

  @Effect()
  listarCategorias$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCategoriasCurso),
    map((action: ListarCategoriasCurso) => action),
    switchMap((action: ListarCategoriasCurso) => this._perfilUsuarioService.listarCategoriasCurso().pipe(
      map((categorias: CategoriaCurso[]) => new ListarCategoriasCursoSuccess(categorias)),
      catchError(error => of(new ListarCategoriasCursoFail(error))))));

  @Effect({ dispatch: false })
  listarCategoriasFail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ListarCategoriasCursoFail),
    map((action: ListarCategoriasCursoFail) => {
      this._alertService.abrirAlert(Color.DANGER, 'Não foi possivel encontrar as categorias.');
      console.log(action.error);
    }));


  @Effect()
  excluirCurso$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ExcluirCurso),
    switchMap((excluirCurso: ExcluirCurso) => this._perfilUsuarioService.excluirCurso(excluirCurso.curso.id).pipe(
      map((acaoPai: Curso) => new ExcluirCursoSuccess(excluirCurso.curso, excluirCurso.filtro)),
      catchError(error => of(new ExcluirCursoFail(error))))));

  @Effect({ dispatch: false })
  excluirCursofail$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ExcluirCursoFail),
    map((_) => {
      this._alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir a capacitação.');
    }
    ));

  @Effect({ dispatch: false })
  excluirCursoSuccess$ = this.actions$.pipe(
    ofType(PerfilUsuarioActionTypes.ExcluirCursoSuccess),
    map((action: ExcluirCursoSuccess) => action),
    map((action) => {
      this._alertService.abrirAlert(Color.SUCCESS, 'Capacitação excluída com sucesso!');
      this.perfilUsuarioStore$.dispatch(new ListarCursos(action.filtro));
    }));
}
