import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { PerfilUsuarioEffects } from './perfil-usuario.effects';

describe('PerfilUsuarioEffects', () => {
  let actions$: Observable<any>;
  let effects: PerfilUsuarioEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PerfilUsuarioEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(PerfilUsuarioEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
