import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsuarioInstitutoEffects } from '@app/effects/usuario-instituto.effects';
import * as fromUsuarioInstituto from '@app/reducers/usuario-instituto.reducer';
import { MenuDropDownModule } from '@app/shared/components/menu-dropdown/menu-dropdown.module';
import { ScAccordionItemModule } from '@app/shared/components/sc-accordion-item/sc-accordion-item.module';
import { SharedModule } from '@app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AvatarModule } from '@app/shared/components/sins-avatar/sc-avatar.module';
import { ActionbarModule, CardModule, FormModule, ProgressBarModule, TabsModule } from '@sicoob/ui';
import { NgxFileDropModule } from 'ngx-file-drop';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPermissionsModule } from 'ngx-permissions';
import * as fromGed from '../../reducers/ged.reducer';
import { ActionBarCadastroCursoComponent } from './components/action-bar-cadastro-curso/action-bar-cadastro-curso.component';
import { CursoDetalhesAccordionComponent } from './components/curso-detalhes-accordion/curso-detalhes-accordion.component';
import { PerfilUsuarioComponent } from './components/perfil-usuario/perfil-usuario.component';
import { TabInformacoesUsuariosComponent } from './components/tab-informacoes-usuario/tab-informacoes-usuario.component';
import { ContainerPerfilUsuarioComponent } from './containers/container-perfil-usuario/container-perfil-usuario.component';
import { PerfilUsuarioEffects } from './effects/perfil-usuario.effects';
import { PerfilUsuarioRoutingModule } from './perfil-usuario-routing.module';
import * as fromPerfilUsuario from './reducers/perfil-usuario.reducer';
import { SinsBadgeModule } from '@app/shared/components/sins-badge/sins-badge.module';
import { ContainerPerfilUsuarioCursosComponent } from './containers/container-perfil-usuario-cursos/container-perfil-usuario-cursos.component';
import { ContainerPerfilUsuarioConvitesComponent } from './containers/container-perfil-usuario-convites/container-perfil-usuario-convites.component';
import { ContainerPerfilUsuarioProjetosComponent } from './containers/container-perfil-usuario-projetos/container-perfil-usuario-projetos.component';
import { ConviteDetalhesAccordionComponent } from './components/convite-detalhes-accordion/convite-detalhes-accordion.component';





@NgModule({
  declarations: [ConviteDetalhesAccordionComponent, ContainerPerfilUsuarioCursosComponent, ContainerPerfilUsuarioConvitesComponent, ContainerPerfilUsuarioProjetosComponent, ContainerPerfilUsuarioComponent, PerfilUsuarioComponent, TabInformacoesUsuariosComponent, CursoDetalhesAccordionComponent, ActionBarCadastroCursoComponent],
  imports: [
    SinsBadgeModule,
    ProgressBarModule,
    FormsModule, ReactiveFormsModule,
    FormModule,
    MenuDropDownModule,
    AvatarModule,
    NgxFileDropModule,
    NgxPermissionsModule.forChild(),
    CommonModule,
    ScAccordionItemModule,
    NgxMaskModule.forRoot(),
    PerfilUsuarioRoutingModule,
    StoreModule.forFeature('perfilUsuario', fromPerfilUsuario.reducer),
    StoreModule.forFeature('usuarioInsitutoState', fromUsuarioInstituto.reducer),
    StoreModule.forFeature('arquivoGedSelector', fromGed.reducer),
    EffectsModule.forFeature([PerfilUsuarioEffects, UsuarioInstitutoEffects]),
    CardModule,
    SharedModule,
    TabsModule,
    ActionbarModule,
  ],
  entryComponents: [ActionBarCadastroCursoComponent],
})
export class PerfilUsuarioModule { }
