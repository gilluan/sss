import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { VoluntarioDto } from '@app/shared/models/voluntario-dto.model';
import { Observable } from 'rxjs';
import { PerfilType } from '@app/shared/types/perfil.type';

@Component({
  selector: 'sc-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.css']
})
export class PerfilUsuarioComponent  {

  @Input() usuarioInstituto: VoluntarioDto | UsuarioInstituto;
  @Input() perfil: string;
  @Input() photo: Observable<string>;
  @Input() totalHoras: Observable<number>;
  @Output() onDownload: EventEmitter<any> = new EventEmitter();
  constructor() { }

  havePerfil(object: any): object is UsuarioInstituto {
    return 'perfil' in object;
  }

  public async baixarTermo() {
    this.onDownload.emit();
  }

  obterPerfil = (perfil: string) => PerfilType.obterPerfil(perfil);
}
