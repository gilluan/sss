import { ChangeDetectorRef, Component, DoCheck, OnDestroy, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LimparStoreGed, SalvarArquivoCurso, EditarArquivoCurso } from '@app/actions/ged.actions';
import { GedService } from '@app/shared/ged/ged.service';
import { TipoDocumentoGed } from '@app/shared/ged/models/tipo-arquivo-ged.enum';
import { PerfilType } from '@app/shared/types/perfil.type';
import { select, Store } from '@ngrx/store';
import { DocumentoEnvio } from '@shared/ged/models/envio/documentoEnvio.model';
import { PersistanceService } from '@shared/services/persistence.service';
import { ActionBarRef, ACTIONBAR_DATA } from '@sicoob/ui';
//import { FileSystemDirectoryEntry, FileSystemFileEntry, UploadEvent } from 'ngx-file-drop';
import { Observable, Subscription } from 'rxjs';
import * as fromGed from '../../../../reducers/ged.reducer';
import { ListarCategoriasCurso, EditarCurso } from '../../actions/perfil-usuario.actions';
import { CategoriaCurso } from '../../models/categoria-curso.model';
import * as fromPerfilUsuario from '../../reducers/perfil-usuario.reducer';
import { Curso } from '../../models/curso.model';
import { Formatters } from '@app/shared/utils/formatters';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

@Component({
  selector: 'sc-action-bar-cadastro-curso',
  templateUrl: './action-bar-cadastro-curso.component.html',
  styleUrls: ['./action-bar-cadastro-curso.component.scss']
})
export class ActionBarCadastroCursoComponent implements OnInit, OnDestroy, DoCheck {
  public files: NgxFileDropEntry[];

  constructor(
    private fb: FormBuilder,
    public actionBarRef: ActionBarRef,
    private changeDetector: ChangeDetectorRef,
    public perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private gedStore$: Store<fromGed.State>,
    private gedService: GedService,
    private persistanceService: PersistanceService,
    @Inject(ACTIONBAR_DATA) public data: any
  ) { }

  ngOnDestroy(): void {
    if (this.categoriasSubjec)
      this.categoriasSubjec.unsubscribe()
    this.gedStore$.dispatch(new LimparStoreGed());
  }

  usuarioInstituto: any;
  categoriasSubjec: Subscription;
  cursoForm: FormGroup;
  novo: boolean = true;

  ngOnInit(): void {
    this.carregarForm();

    this.usuarioInstituto = this.persistanceService.get('usuario_instituto');
    this.perfilUsuarioStore$.dispatch(new ListarCategoriasCurso());
    this.fileProgress = this.gedStore$.pipe(select(fromGed.getFileUploadProgress));
    this.categoriasSubjec = this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getCategorias)).subscribe(
      categorias => {
        this.categoriasCurso = categorias;
        if (this.data.curso) {
          this.novo = false;
          this.selecionarCategoriaCurso(this.getCategoriaCurso(this.data.curso.categoriaCurso));
          this.preencherForm(this.data.curso);
        }
        this.changeDetector.detectChanges()
      }
    );

  }

  getCategoriaCurso(categoriaCurso: any) {
    let categoria: CategoriaCurso = new CategoriaCurso();
    categoria.dataCriacao = categoriaCurso.dataCriacao;
    categoria.dataModificacao = categoriaCurso.dataModificacao;
    categoria.descricao = categoriaCurso.descricao;
    categoria.icone = categoriaCurso.icone;
    categoria.id = categoriaCurso._id;
    categoria.nome = categoriaCurso.nome;
    categoria.selecionado = false;
    return categoria;
  }

  preencherForm(curso: Curso) {
    this.cursoForm.patchValue({
      nome: curso.nome,
      quantidadeHoras: curso.quantidadeHoras,
      descricao: curso.descricao,
      file: curso.identificadorCertificadoGED,
      dataConclusao: this.formatDate(new Date(curso.dataConclusao)),
      voluntario: curso.voluntario,
      usuarioInstituto: curso.usuarioInstituto,
    });
  }


  carregarForm() {
    this.cursoForm = this.fb.group({
      nome: ['', Validators.required],
      quantidadeHoras: ['', Validators.required],
      descricao: [''],
      categoriaCurso: ['', Validators.required],
      file: ['', Validators.required],
      dataConclusao: ['', Validators.required],
      voluntario: [],
      usuarioInstituto: [],
    });
  }

  ngDoCheck(): void {
    let isEmptyDescricao: string = this.cursoForm.controls.descricao.value;
    if (isEmptyDescricao.length >= 1
      || (this.cursoForm.controls.nome.valid
        && this.cursoForm.controls.quantidadeHoras.valid
        && this.cursoForm.controls.categoriaCurso.valid
        && this.cursoForm.controls.file.valid
        && this.cursoForm.controls.dataConclusao.valid
        && this.cursoForm.controls.voluntario.valid)) {
      this.cursoForm.controls['descricao'].setErrors(null)
    } else {
      this.cursoForm.controls['descricao'].setErrors({ "invalid": true, message: '' });
      this.cursoForm.controls['descricao'].markAsUntouched();
      this.cursoForm.controls['descricao'].markAsPristine();
    }
  }



  cancelar() {
    this.actionBarRef.close();
  }

  dataMsg: string = 'Campo requerido';
  errorData: boolean = false;
  keyUpData($event) {
    if (this.cursoForm.controls['dataConclusao'].errors) {
      this.errorData = true;
      this.dataMsg = "Data Inválida";
    } else {
      this.dataMsg = null;
      this.errorData = false;
    }
  }
  blurData(event) {
    if (this.cursoForm.controls['dataConclusao'].errors) {
      this.errorData = true;
      if (this.cursoForm.controls['dataConclusao'].errors.required) {
        this.dataMsg = "Campo requerido";
      } else if (this.cursoForm.controls['dataConclusao'].errors.pattern) {
        this.dataMsg = "Data Inválida";
      }
    } else { this.dataMsg = null; this.errorData = false; }
  }

  categoriasCurso: CategoriaCurso[];
  fileProgress: Observable<number>;
  categoriaCursoSelecionado: CategoriaCurso;

  selecionarCategoriaCurso(categoriaCurso: CategoriaCurso): void {
    categoriaCurso.selecionado = !categoriaCurso.selecionado;
    if (categoriaCurso.selecionado) {
      this.categoriasCurso.forEach(m => m.selecionado = m.id === categoriaCurso.id);
      this.categoriaCursoSelecionado = categoriaCurso;
    } else {
      this.categoriaCursoSelecionado = null;
    }
    this.cursoForm.controls['categoriaCurso'].setValue(this.categoriaCursoSelecionado ? this.categoriaCursoSelecionado.id : null)
    this.changeDetector.detectChanges();
  }

  fileCertificado: File;
  isFileLoaded: boolean;
  selectedFileCert: string;
  arrayFileData: number[];
  messageErrorFileSize: string = 'O tamanho máximo do arquivo deve ser 500 KB';
  messageErrorFileType: string = 'Somente o tipo PDF é permitido';
  messageErrorSendFile: string = 'Problema ao enviar o certificado ou certificado não encontrado'
  mensagemError: string;

  //FIXME arrumar funcionalidade de action upload de arquivos e file drop
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.mensagemError = "";
    let that = this;
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          that.fileCertificado = file;
          if (this.isFileTypeAllowed(this.fileCertificado)) {
            if (this.isFileSizeAllowed(this.fileCertificado)) {
              this.loadArrayFileData(this.fileCertificado);
              this.isFileLoaded = true;
              this.selectedFileCert = this.fileCertificado.name;
            } else {
              this.mensagemError = this.messageErrorFileSize;
            }
          } else {
            this.mensagemError = this.messageErrorFileType;
          }
          this.changeDetector.detectChanges();
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  public fileChangeEvent(fileInput: any) {
    this.mensagemError = "";
    this.fileCertificado = fileInput.target.files[0];
    if (this.fileCertificado != null && this.fileCertificado != undefined) {
      if (this.isFileTypeAllowed(this.fileCertificado)) {
        if (this.isFileSizeAllowed(this.fileCertificado)) {
          this.loadArrayFileData(this.fileCertificado);
          this.isFileLoaded = true;
          this.selectedFileCert = this.fileCertificado.name;
        } else {
          this.mensagemError = this.messageErrorFileSize;
        }
      } else {
        this.mensagemError = this.messageErrorFileType;
      }
    }
  }

  public loadArrayFileData(file: File) {
    let fileReader: FileReader = new FileReader();
    fileReader.readAsArrayBuffer(new Blob([file]));

    fileReader.onload = (e) => {
      this.arrayFileData = Array.from(new Int8Array(fileReader.result as ArrayBuffer));
      this.cursoForm.controls['file'].setValue(this.arrayFileData)
    }

    this.changeDetector.markForCheck();

  }

  private isFileTypeAllowed(fileCert: File): boolean {
    return fileCert.type === 'application/pdf';
  }

  private isFileSizeAllowed(fileCert: File): boolean {
    return (fileCert.size / 1024 < 500);
  }

  showProgress: boolean = false;
  async salvarCurso() {
    this.showProgress = true;
    const documentoEnvio: DocumentoEnvio = await this.gerarDocumentoEnvio();
    let perfil = this.usuarioInstituto.perfil == PerfilType.voluntario ? 'voluntario' : 'usuarioInstituto';
    this.cursoForm.controls[perfil].setValue(this.usuarioInstituto.id)
    this.cursoForm.controls['dataConclusao'].setValue(Formatters.stringToDate(this.cursoForm.value.dataConclusao));
    this.cursoForm.controls['file'].setValue("")
    this.gedStore$.dispatch(new SalvarArquivoCurso(documentoEnvio, this.cursoForm.value, this.actionBarRef));
  }

  async editarCurso() {
    let cursoEditar = this.getCurso(this.cursoForm.value);
    if (typeof this.cursoForm.value.file !== 'string') {
      this.showProgress = true;
      const documentoEnvio: DocumentoEnvio = await this.gerarDocumentoEnvio();
      this.gedStore$.dispatch(new EditarArquivoCurso(documentoEnvio, cursoEditar, this.actionBarRef));
    } else {
      cursoEditar.identificadorCertificadoGED = this.cursoForm.value.file;
      this.perfilUsuarioStore$.dispatch(new EditarCurso(cursoEditar, this.actionBarRef));
    }
  }

  private getCurso(cursoForm: any) {
    let curso = new Curso();
    curso.categoriaCurso = cursoForm.categoriaCurso
    curso.dataConclusao = Formatters.stringToDate(cursoForm.dataConclusao)
    curso.descricao = cursoForm.descricao
    curso.id = this.data.curso._id
    curso.nome = cursoForm.nome
    curso.quantidadeHoras = cursoForm.quantidadeHoras
    curso.usuarioInstituto = cursoForm.usuarioInstituto
    curso.voluntario = cursoForm.voluntario
    return curso;
  }

  private async gerarDocumentoEnvio(): Promise<DocumentoEnvio> {
    try {
      const fileBinario: number[] = await this.cursoForm.value.file;
      return this.gedService.criarDocumentoEnvio(fileBinario, TipoDocumentoGed.fluxoPassos, this.usuarioInstituto);
    } catch (e) {
      console.error('erro', e);
    }
  }



  private formatDate(date: Date) {
    let data = date,
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return diaF + "/" + mesF + "/" + anoF;
  }

}
