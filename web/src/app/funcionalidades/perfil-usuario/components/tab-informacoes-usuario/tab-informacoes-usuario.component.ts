import { Component, EventEmitter, OnInit, Output, ViewChild, Input, OnDestroy } from '@angular/core';
import { TabsInformacoesUsuario } from '../../models/tab-informacoes-usuario.type';
import { TabPanelComponent } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'sc-tab-informacoes-usuario',
  templateUrl: './tab-informacoes-usuario.component.html',
  styleUrls: ['./tab-informacoes-usuario.component.scss']
})
export class TabInformacoesUsuariosComponent implements OnInit, OnDestroy {


  @Input() totalConvites: Observable<number>;
  @Input() totalProjetos: Observable<number>;
  @Input() mostrarProjetos: boolean;
  @Output() changeTab: EventEmitter<TabsInformacoesUsuario> = new EventEmitter<TabsInformacoesUsuario>();
  @ViewChild('tabPanelFiltro') tabPanelDashboard: TabPanelComponent;
  subscriptions: Subscription = new Subscription();


  constructor(private router: Router) { }

  ngOnInit(): void {
    this.subscriptions.add(this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      const tab = event.url.split('/')[3] ? TabsInformacoesUsuario.getTab(event.url.split('/')[3].split('?')[0]) : TabsInformacoesUsuario.cursos;
      this.mudarAba(tab);
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onTabSelected(tabIndex) {
    this.changeTab.emit(tabIndex);
  }

  mudarAba(index: TabsInformacoesUsuario) {
    this.tabPanelDashboard.selectByIndex(index);
  }

}
