import { animate, state, style, transition, trigger } from "@angular/animations";
import { CdkAccordionItem } from "@angular/cdk/accordion";
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ItemMenu } from "@app/shared/components/menu-dropdown/model/item-menu.model";
import { PositionDotsType } from "@app/shared/components/menu-dropdown/model/position-dots.type";
import { CategoriaCurso } from '../../models/categoria-curso.model';
import { Curso } from '../../models/curso.model';
import { MenuDropDownComponent } from "@app/shared/components/menu-dropdown/menu-dropdown.component";

@Component({
  selector: 'curso-detalhes-accordion',
  templateUrl: './curso-detalhes-accordion.component.html',
  styleUrls: ['./curso-detalhes-accordion.component.scss'],
  animations: [
    trigger('toggle', [
      state('toggleOn', style({ display: 'block', opacity: 1, height: '*', overflow: 'initial' })),
      state('toggleOff', style({ display: 'none', opacity: 0, height: '0', overflow: 'hidden' })),
      state('turnIconUp', style({ transform: 'rotate(180deg)', margin: '5px 0px 8px 0px' })),
      state('turnIconDown', style({ transform: 'rotate(0)', margin: '10px 0px 0px 0px' })),
      transition('toggleOn <=> toggleOff', [animate('0.3s')]),
      transition('turnIconUp <=> turnIconDown', [animate('0.3s')]),
    ])
  ]
})
export class CursoDetalhesAccordionComponent extends CdkAccordionItem implements OnInit, AfterViewInit {

  @ViewChild('dropDown') sinsMenuDropDown: MenuDropDownComponent;

  ngOnInit(): void {
    this.montarMenu();
  }

  ngAfterViewInit(): void {
    if (this.curso)
      this.curso.categoriaCurso
  }

  @Input() curso: Curso;
  @Input() isVisitante: boolean;
  @Output() baixarCertificado: EventEmitter<Curso> = new EventEmitter<Curso>();
  @Output() excluirCertificado: EventEmitter<Curso> = new EventEmitter<Curso>();
  @Output() onEditarCurso: EventEmitter<Curso> = new EventEmitter<Curso>();
  position: PositionDotsType = PositionDotsType.vertical;
  itensMenu: Array<ItemMenu>;

  montarMenu = () => {
    this.itensMenu = new Array<ItemMenu>();
    this.itensMenu.push(new ItemMenu(1, 'Baixar Certificado'));
    this.isVisitante ? null : this.itensMenu.push(new ItemMenu(2, 'Excluir Capacitação'));
  }


  _baixarCertificado() {
    this.baixarCertificado.emit(this.curso);
  }

  onAction($event: ItemMenu) {
    if ($event.id == 1)
      this._baixarCertificado();
    else if ($event.id == 2){
      this.sinsMenuDropDown.hide();
      this._excluirCertificado();
    }
  }

  editarCurso(curso: Curso) {
    this.onEditarCurso.emit(curso);
  }

  isCategoriaCurso(param: string | CategoriaCurso): param is CategoriaCurso {
    return (<CategoriaCurso>param).icone !== undefined;
  }

  _excluirCertificado() {
    this.excluirCertificado.emit(this.curso);
  }

  expansionState(): string {
    return this.expanded ? 'toggleOn' : 'toggleOff'
  }

  iconState(): string {
    return this.expanded ? 'turnIconUp' : 'turnIconDown'
  }

  contentState(): string {
    return this.expanded ? 'contentOn' : 'contentOff'
  }
}
