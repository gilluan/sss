import { animate, state, style, transition, trigger } from "@angular/animations";
import { CdkAccordionItem } from "@angular/cdk/accordion";
import { DatePipe } from "@angular/common";
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'convite-detalhes-accordion',
  templateUrl: './convite-detalhes-accordion.component.html',
  styleUrls: ['./convite-detalhes-accordion.component.scss'],
  animations: [
    trigger('toggle', [
      state('toggleOn', style({ display: 'block', opacity: 1, height: '*', overflow: 'initial' })),
      state('toggleOff', style({ display: 'none', opacity: 0, height: '0', overflow: 'hidden' })),
      state('turnIconUp', style({ transform: 'rotate(180deg)', margin: '5px 0px 8px 0px' })),
      state('turnIconDown', style({ transform: 'rotate(0)', margin: '10px 0px 0px 0px' })),
      transition('toggleOn <=> toggleOff', [animate('0.3s')]),
      transition('turnIconUp <=> turnIconDown', [animate('0.3s')]),
    ])
  ]
})
export class ConviteDetalhesAccordionComponent extends CdkAccordionItem {

  @Input() convite: any;
  @Output() participar: EventEmitter<any> = new EventEmitter<any>();


  getDataIniticio() {
    const datePipe = new DatePipe('pt-br');
    const dataInicio = datePipe.transform(this.convite.projeto.dataInicio, "dd 'de' MMMM");
    return `Início em ${dataInicio}`;
  }

  onParticipar() {
    this.participar.emit(this.convite);
  }

  expansionState(): string {
    return this.expanded ? 'toggleOn' : 'toggleOff'
  }

  iconState(): string {
    return this.expanded ? 'turnIconUp' : 'turnIconDown'
  }

  contentState(): string {
    return this.expanded ? 'contentOn' : 'contentOff'
  }
}
