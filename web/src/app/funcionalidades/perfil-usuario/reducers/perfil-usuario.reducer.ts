import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { PerfilUsuario } from '../models/perfil-usuario.model';
import { Curso } from '../models/curso.model';
import { CategoriaCurso } from '../models/categoria-curso.model';

import { PerfilUsuarioActions, PerfilUsuarioActionTypes } from '../actions/perfil-usuario.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { PerfilType } from '@app/shared/types/perfil.type';

export interface State extends EntityState<PerfilUsuario> {
  cursos: Curso[];
  cursoEditar: Curso;
  categorias: CategoriaCurso[],
  salvarCursoVoluntarioSuccess: boolean;
  convites: PagedDataModelVm;
  totalProjetos: number;
  projetos: PagedDataModelVm;
  totalConvites: number;
  infosVisitante: {
    id: string,
    isVisitante: boolean,
    perfil: string
  };
}

export const adapter: EntityAdapter<PerfilUsuario> = createEntityAdapter<PerfilUsuario>();

export const initialState: State = adapter.getInitialState({
  cursos: [],
  cursoEditar: null,
  categorias: [],
  salvarCursoVoluntarioSuccess: false,
  convites: new PagedDataModelVm(new PageModelVm(0, 10, 0), []),
  totalConvites: 0,
  totalProjetos: 0,
  projetos: new PagedDataModelVm(new PageModelVm(0, 10, 0), []),
  infosVisitante: {
    id: '',
    isVisitante: false,
    perfil: PerfilType.voluntario
  },
});

export function reducer(
  state = initialState,
  action: PerfilUsuarioActions
): State {
  switch (action.type) {

    case PerfilUsuarioActionTypes.TotalConvitesSuccess: {
      return { ...state, totalConvites: action.total }
    }

    case PerfilUsuarioActionTypes.LoadConvitesSuccess: {
      let convites = new PagedDataModelVm(new PageModelVm(0, 10, 0), []);
      convites.data = action.convites && action.convites.data.length > 0 ? state.convites.data.concat(action.convites.data) : [];
      convites.page = action.convites.page;
      return { ...state, convites }
    }

    case PerfilUsuarioActionTypes.TotalProjetosSuccess: {
      return { ...state, totalProjetos: action.total }
    }

    case PerfilUsuarioActionTypes.LoadProjetosSuccess: {
      let projetos = new PagedDataModelVm(new PageModelVm(0, 10, 0), []);
      projetos.data = action.projetos && action.projetos.data.length > 0 ? state.projetos.data.concat(action.projetos.data) : [];
      projetos.page = action.projetos.page;
      return { ...state, projetos }
    }

    case PerfilUsuarioActionTypes.LimparProjetos: {
      return { ...state, projetos: initialState.projetos }
    }

    case PerfilUsuarioActionTypes.LimparConvites: {
      return { ...state, convites: initialState.convites }
    }

    case PerfilUsuarioActionTypes.LimparTotais: {
      return { ...state, totalConvites: 0, totalProjetos: 0 }
    }

    case PerfilUsuarioActionTypes.ListarCursosSuccess: {
      return {
        ...state, cursos: action.cursos
      };
    }
    case PerfilUsuarioActionTypes.ListarCursoSuccess: {
      return {
        ...state, cursoEditar: action.curso
      };
    }
    case PerfilUsuarioActionTypes.LimparCursos: {
      return {
        ...state, cursos: [], categorias: []
      };
    }
    case PerfilUsuarioActionTypes.ListarCategoriasCursoSuccess: {
      return {
        ...state, categorias: action.categoriasCurso
      };
    }
    case PerfilUsuarioActionTypes.SalvarCursoVoluntarioSuccess: {
      return {
        ...state, salvarCursoVoluntarioSuccess: true
      };
    }
    case PerfilUsuarioActionTypes.InformarAcesso: {
      return {
        ...state, infosVisitante: {id: action.id, isVisitante: action.isVisitante, perfil: action.perfil}
      };
    }
    default: {
      return state;
    }
  }
}


export const perfilUsuarioSelector = createFeatureSelector<State>('perfilUsuario');



export const getInfosVisitante = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.infosVisitante;
  });

export const getTotalProjetos = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.totalProjetos;
  });

export const getProjetos = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.projetos;
  });


export const getTotalConvites = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.totalConvites;
  });

export const getConvites = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.convites;
  });

export const getCursos = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.cursos;
  });

export const getCursoEditar = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.cursoEditar;
  });

export const getSalvarCursoVoluntarioSuccess = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.salvarCursoVoluntarioSuccess;
  });

export const getCategorias = createSelector(
  perfilUsuarioSelector,
  state => {
    return state.categorias;
  });

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
