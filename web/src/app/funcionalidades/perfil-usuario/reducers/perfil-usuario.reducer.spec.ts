import { reducer, initialState } from '../reducers/perfil-usuario.reducer';

describe('PerfilUsuario Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
