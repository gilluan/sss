import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PageModelVm } from '@app/shared/models/page.model';
import { Validations } from '@app/shared/utils/validations';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Service } from 'src/app/shared/services/service';
import { environment } from 'src/environments/environment';
import { CategoriaCurso } from './models/categoria-curso.model';
import { CursoFiltro } from './models/curso.filtro'
import { Curso } from './models/curso.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
const notNull = Validations.notNull;

@Injectable({
  providedIn: 'root'
})
export class PerfilUsuarioService extends Service {

  private RESOURCE_CURSOS = 'cursos';
  private RESOURCE_CATEGORIA = 'categorias-curso'
  private RESOURCE_CONVITES = 'convites'
  private RESOURCE_PROJETOS = 'projetos'
  private RESOURCE_URL_CURSOS = `${environment.modulo_gestao_pessoa}/${this.RESOURCE_CURSOS}`;
  private RESOURCE_URL_CATEGORIA = `${environment.modulo_gestao_pessoa}/${this.RESOURCE_CATEGORIA}`;
  private RESOURCE_URL_CONVITES = `${environment.modulo_gestao_projeto}/${this.RESOURCE_CONVITES}`;
  private RESOURCE_URL_PROJETOS = `${environment.modulo_gestao_projeto}/${this.RESOURCE_PROJETOS}`;


  constructor(private http: HttpClient) { super(); }

  listarCategoriasCurso(nome?: string): Observable<CategoriaCurso[]> {
    let url = this.RESOURCE_URL_CATEGORIA;
    if (nome) {
      nome = encodeURIComponent(nome)
      url = `${url}?nome=${nome}`
    }
    return this.http.get<CategoriaCurso[]>(url, httpOptions)
      .pipe(
        map((r: any) => r.resultado),
        catchError(this.handleError)
      );
  }

  salvarCurso(curso: Curso): Observable<Curso> {
    return this.http.post<Curso>(this.RESOURCE_URL_CURSOS, curso, httpOptions)
      .pipe(
        map((r: any) => r.resultado),
        catchError(this.handleError)
      );
  }

  editarCurso(curso: Curso): Observable<Curso> {
    return this.http.put<Curso>(this.RESOURCE_URL_CURSOS, curso, httpOptions)
      .pipe(
        map((r: any) => r.resultado),
        catchError(this.handleError)
      );
  }

  listarCursoById(id: string): Observable<Curso> {
    let url = `${this.RESOURCE_URL_CURSOS}/${id}`;
    return this.http.get<Curso>(url, httpOptions)
      .pipe(
        map((r: any) => r.resultado),
        catchError(this.handleError)
      );
  }

  listarCursos(filtro: CursoFiltro): Observable<Curso[]> {
    const params = this.buildParams(filtro);
    return this.http.get<Curso[]>(this.RESOURCE_URL_CURSOS, { params }).pipe(
      map((r: any) => r.resultado.data),
      catchError(this.handleError)
    );
  }

  listarConvites(id: string, paginacao: PageModelVm): Observable<PagedDataModelVm> {
    const params = this.buildPaginacao(new HttpParams(), paginacao);
    return this.http.get<PagedDataModelVm>(`${this.RESOURCE_URL_CONVITES}/${id}`, { params }).pipe(
      map((r: any) => r.resultado),
      catchError(this.handleError)
    );
  }

  totalConvites(id: string): Observable<number> {
    return this.http.get<{ resultado: number }>(`${this.RESOURCE_URL_CONVITES}/${id}/total`, httpOptions).pipe(
      map((r: any) => r.resultado),
      catchError(this.handleError)
    );
  }

  listarProjetos(id: string, paginacao: PageModelVm): Observable<PagedDataModelVm> {
    const params = this.buildPaginacao(new HttpParams(), paginacao);
    return this.http.get<PagedDataModelVm>(`${this.RESOURCE_URL_PROJETOS}/${id}/voluntario`, { params }).pipe(
      map((r: any) => r.resultado),
      catchError(this.handleError)
    );
  }

  totalProjetos(id: string): Observable<number> {
    return this.http.get<{ resultado: number }>(`${this.RESOURCE_URL_PROJETOS}/${id}/voluntario/total`, httpOptions).pipe(
      map((r: any) => r.resultado),
      catchError(this.handleError)
    );
  }

  participarProjeto(idProjeto: string, idVoluntario: string): Observable<string> {
    const infos = { projeto: idProjeto, voluntario: idVoluntario }
    return this.http.patch<{ resultado: string }>(`${environment.modulo_gestao_projeto}/projetos/participar`, infos, httpOptions).pipe(
      map(data => data.resultado),
      catchError(this.handleError)
    );
  }

  excluirCurso(id: string) {
    return this.http.delete<any>(`${this.RESOURCE_URL_CURSOS}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  private buildPaginacao(params: HttpParams, paginacao: PageModelVm) {
    notNull(paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('offset', pn));
      notNull(p.pageSize, ps => params = params.append('limit', ps));
    });
    return params;
  }

  private buildParams(filtro: CursoFiltro): HttpParams {
    let params = new HttpParams();
    notNull(filtro.nome, n => params = params.append('nome', n));
    notNull(filtro.categoriaCurso, c => params = params.append('categoriaCurso', c));
    notNull(filtro.identificadorVoluntario, c => params = params.append('identificadorVoluntario', c));
    notNull(filtro.identificadorUsuarioInstituto, c => params = params.append('identificadorUsuarioInstituto', c));
    params = this.buildPaginacao(params, filtro.paginacao)
    return params;
  }

}
