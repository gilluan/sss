import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerPerfilUsuarioComponent } from './containers/container-perfil-usuario/container-perfil-usuario.component';
import { ContainerPerfilUsuarioCursosComponent } from './containers/container-perfil-usuario-cursos/container-perfil-usuario-cursos.component';
import { ContainerPerfilUsuarioConvitesComponent } from './containers/container-perfil-usuario-convites/container-perfil-usuario-convites.component';
import { ContainerPerfilUsuarioProjetosComponent } from './containers/container-perfil-usuario-projetos/container-perfil-usuario-projetos.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerPerfilUsuarioComponent,
    children: [
      {
        path: '',
        component: ContainerPerfilUsuarioCursosComponent,
        data: {
          breadcrumb: "Cursos"
        }
      },
      {
        path: 'convites',
        component: ContainerPerfilUsuarioConvitesComponent,
        data: {
          breadcrumb: "Convites"
        }
      },
      {
        path: 'projetos',
        component: ContainerPerfilUsuarioProjetosComponent,
        data: {
          breadcrumb: "Projetos"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilUsuarioRoutingModule { }
