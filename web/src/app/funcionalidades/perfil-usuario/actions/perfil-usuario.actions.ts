import { Action } from '@ngrx/store';
import { ActionBarRef } from '@sicoob/ui';
import { CategoriaCurso } from '../models/categoria-curso.model';
import { CursoFiltro } from '../models/curso.filtro';
import { Curso } from '../models/curso.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';

export enum PerfilUsuarioActionTypes {
  SalvarCurso = '[SalvarCurso]',
  SalvarCursoSuccess = '[SalvarCursoSuccess]',
  SalvarCursoFail = '[SalvarCursoFail]',
  EditarCurso = '[EditarCurso]',
  EditarCursoSuccess = '[EditarCursoSuccess]',
  EditarCursoFail = '[EditarCursoFail]',
  ListarCursos = '[ListarCursos]',
  ListarCursosSuccess = '[ListarCursosSuccess]',
  ListarCursosFail = '[ListarCursosFail]',
  ListarCurso = '[ListarCurso]',
  ListarCursoSuccess = '[ListarCursoSuccess]',
  ListarCursoFail = '[ListarCursoFail]',
  LimparCursos = '[LimparCursos]',
  ListarCategoriasCurso = '[ListarCategoriasCurso]',
  ListarCategoriasCursoSuccess = '[ListarCategoriasCursoSuccess]',
  ListarCategoriasCursoFail = '[ListarCategoriasCursoFail]',
  SalvarCursoVoluntario = '[SalvarCursoVoluntario]',
  SalvarCursoVoluntarioSuccess = '[SalvarCursoVoluntarioSuccess]',
  SalvarCursoVoluntarioFail = '[SalvarCursoVoluntarioFail]',
  EditarCursoVoluntario = '[EditarCursoVoluntario]',
  EditarCursoVoluntarioSuccess = '[EditarCursoVoluntarioSuccess]',
  EditarCursoVoluntarioFail = '[EditarCursoVoluntarioFail]',
  ExcluirCurso = '[ExcluirCurso]',
  ExcluirCursoSuccess = '[ExcluirCursoSuccess]',
  ExcluirCursoFail = '[ExcluirCursoFail]',
  LoadConvites = '[LoadConvites] PerfilUsuario',
  LoadConvitesSuccess = '[LoadConvitesSuccess] PerfilUsuario',
  LoadConvitesFail = '[LoadConvitesFail] PerfilUsuario',
  LimparConvites = '[LimparConvites] PerfilUsuario',
  TotalConvites = '[TotalConvites] PerfilUsuario',
  TotalConvitesSuccess = '[TotalConvitesSuccess] PerfilUsuario',
  TotalConvitesFail = '[TotalConvitesFail] PerfilUsuario',
  LimparTotais = '[LimparTotais]  PerfilUsuario',
  ParticiparProjeto = '[ParticiparProjeto] PerfilUsuario',
  ParticiparProjetoSuccess = '[ParticiparProjetoSuccess] PerfilUsuario',
  ParticiparProjetoFail = '[ParticiparProjetoFail] PerfilUsuario',
  LimparProjetos = '[LimparProjetos] PerfilUsuario',
  LoadProjetos = '[LoadProjetos] PerfilUsuario',
  LoadProjetosSuccess = '[LoadProjetosSuccess] PerfilUsuario',
  LoadProjetosFail = '[LoadProjetosFail] PerfilUsuario',
  TotalProjetos = '[TotalProjetos] PerfilUsuario',
  TotalProjetosSuccess = '[TotalProjetosSuccess] PerfilUsuario',
  TotalProjetosFail = '[TotalProjetosFail] PerfilUsuario',
  InformarAcesso = '[InformarAcesso] PerfilUsuario',
}

export class InformarAcesso implements Action {
  readonly type = PerfilUsuarioActionTypes.InformarAcesso;
  constructor(public id: string, public isVisitante: boolean, public perfil: string) { }
}
export class LimparTotais implements Action {
  readonly type = PerfilUsuarioActionTypes.LimparTotais;
  constructor() { }
}
export class LimparProjetos implements Action {
  readonly type = PerfilUsuarioActionTypes.LimparProjetos;
  constructor() { }
}
export class LoadProjetos implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadProjetos;
  constructor(public id: string, public paginacao: PageModelVm) { }
}
export class LoadProjetosSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadProjetosSuccess;
  constructor(public projetos: PagedDataModelVm) { }
}
export class LoadProjetosFail implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadProjetosFail;
  constructor(public error: any) { }
}
export class TotalProjetos implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalProjetos;
  constructor(public id: string) { }
}
export class TotalProjetosSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalProjetosSuccess;
  constructor(public total: number) { }
}
export class TotalProjetosFail implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalProjetosFail;
  constructor(public error: any) { }
}


export class LimparConvites implements Action {
  readonly type = PerfilUsuarioActionTypes.LimparConvites;
  constructor() { }
}
export class LoadConvites implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadConvites;
  constructor(public id: string, public paginacao: PageModelVm) { }
}
export class LoadConvitesSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadConvitesSuccess;
  constructor(public convites: PagedDataModelVm) { }
}
export class LoadConvitesFail implements Action {
  readonly type = PerfilUsuarioActionTypes.LoadConvitesFail;
  constructor(public error: any) { }
}
export class TotalConvites implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalConvites;
  constructor(public id: string) { }
}
export class TotalConvitesSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalConvitesSuccess;
  constructor(public total: number) { }
}
export class TotalConvitesFail implements Action {
  readonly type = PerfilUsuarioActionTypes.TotalConvitesFail;
  constructor(public error: any) { }
}
export class ParticiparProjeto implements Action {
  readonly type = PerfilUsuarioActionTypes.ParticiparProjeto;
  constructor(public idProjeto: string, public idVoluntario: string) { }
} export class ParticiparProjetoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.ParticiparProjetoSuccess;
  constructor(public mensagem: string) { }
} export class ParticiparProjetoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.ParticiparProjetoFail;
  constructor(public error: any) { }
}


export class EditarCursoVoluntario implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCursoVoluntario;
  constructor(public curso: Curso) { }
}
export class EditarCursoVoluntarioSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCursoVoluntarioSuccess;
  constructor(public curso: Curso) { }
}
export class EditarCursoVoluntarioFail implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCursoVoluntarioFail;
  constructor(public error: any) { }
}

export class SalvarCursoVoluntario implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCursoVoluntario;
  constructor(public curso: Curso) { }
}
export class SalvarCursoVoluntarioSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCursoVoluntarioSuccess;
  constructor(public curso: Curso) { }
}
export class SalvarCursoVoluntarioFail implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCursoVoluntarioFail;
  constructor(public error: any) { }
}

export class SalvarCurso implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCurso;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}
export class SalvarCursoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCursoSuccess;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}
export class SalvarCursoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.SalvarCursoFail;
  constructor(public error: any, public actionBar: ActionBarRef) { }
}

export class EditarCurso implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCurso;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}
export class EditarCursoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCursoSuccess;
  constructor(public curso: Curso, public actionBar: ActionBarRef) { }
}
export class EditarCursoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.EditarCursoFail;
  constructor(public error: any, public actionBar: ActionBarRef) { }
}

export class ListarCurso implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCurso;
  constructor(public id: string) { }
}
export class ListarCursoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCursoSuccess;
  constructor(public curso: Curso) { }
}
export class ListarCursoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCursoFail;
  constructor(public error: any) { }
}

export class ListarCursos implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCursos;
  constructor(public filtro: CursoFiltro) { }
}
export class ListarCursosSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCursosSuccess;
  constructor(public cursos: Curso[]) { }
}
export class ListarCursosFail implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCursosFail;
  constructor(public error: any) { }
}
export class LimparCursos implements Action {
  readonly type = PerfilUsuarioActionTypes.LimparCursos;
  constructor() { }
}

export class ListarCategoriasCurso implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCategoriasCurso;
  constructor() { }
}
export class ListarCategoriasCursoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCategoriasCursoSuccess;
  constructor(public categoriasCurso: CategoriaCurso[]) { }
}
export class ListarCategoriasCursoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.ListarCategoriasCursoFail;
  constructor(public error: any) { }
}

export class ExcluirCurso implements Action {
  readonly type = PerfilUsuarioActionTypes.ExcluirCurso;
  constructor(public curso: Curso, public filtro: CursoFiltro) { }
} export class ExcluirCursoSuccess implements Action {
  readonly type = PerfilUsuarioActionTypes.ExcluirCursoSuccess;
  constructor(public curso: Curso, public filtro: CursoFiltro) { }
} export class ExcluirCursoFail implements Action {
  readonly type = PerfilUsuarioActionTypes.ExcluirCursoFail;
  constructor(public error: any) { }
}

export type PerfilUsuarioActions =
  LimparProjetos | InformarAcesso |
  LoadProjetos |
  LoadProjetosSuccess |
  LoadProjetosFail |
  TotalProjetos |
  TotalProjetosSuccess |
  TotalProjetosFail |
  ParticiparProjeto |
  ParticiparProjetoSuccess |
  ParticiparProjetoFail |
  TotalConvites | TotalConvitesSuccess | TotalConvitesFail |
  LoadConvites | LoadConvitesSuccess | LoadConvitesFail | LimparConvites | LimparTotais |
  SalvarCurso | SalvarCursoSuccess | SalvarCursoFail |
  EditarCurso | EditarCursoSuccess | EditarCursoFail |
  ListarCursos | ListarCursosSuccess | ListarCursosFail |
  ListarCurso | ListarCursoSuccess | ListarCursoFail | LimparCursos |
  ListarCategoriasCurso | ListarCategoriasCursoSuccess | ListarCategoriasCursoFail | SalvarCursoVoluntario |
  SalvarCursoVoluntarioSuccess | SalvarCursoVoluntarioFail | EditarCursoVoluntario | EditarCursoVoluntarioSuccess | EditarCursoVoluntarioFail | ExcluirCurso | ExcluirCursoSuccess | ExcluirCursoFail;
