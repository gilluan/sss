import { BaseDto } from "@app/shared/models/base-dto.model";

export class CategoriaCurso extends BaseDto {
  nome: string;
  descricao: string;
  icone: string;
  selecionado: boolean;
}
