import { CategoriaCurso } from "./categoria-curso.model";
import { Voluntario } from "@app/funcionalidades/termo-voluntario/models/voluntario.model";
import { BaseDto } from "@app/shared/models/base-dto.model";

export class Curso extends BaseDto {
  nome: string;
  dataConclusao: Date;
  descricao?: string;
  identificadorCertificadoGED: string;
  categoriaCurso: CategoriaCurso | string;
  voluntario: string;
  usuarioInstituto: string;
  quantidadeHoras: number;
}
