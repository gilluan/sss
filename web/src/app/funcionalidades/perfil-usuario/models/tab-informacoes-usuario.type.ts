export enum  TabsInformacoesUsuario {
  cursos,
  projetos,
  convites,
}

export namespace TabsInformacoesUsuario {
  export function getTab(rota: string) {
    switch (rota) {
      case 'projetos':
        return TabsInformacoesUsuario.projetos;
      case 'convites':
        return TabsInformacoesUsuario.convites;
      default:
        return TabsInformacoesUsuario.cursos;
    }
  }
}

