import {PageModelVm} from '../../../shared/models/page.model';

export class CursoFiltro {

  constructor(
    public identificadorVoluntario?: string,
    public identificadorUsuarioInstituto?: string,
    public nome?: string,
    public categoriaCurso?: string,
    public paginacao: PageModelVm = new PageModelVm(0, 1000, 0)) { }


}
