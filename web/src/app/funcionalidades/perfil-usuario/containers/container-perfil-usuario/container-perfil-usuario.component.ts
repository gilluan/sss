import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarregarTotalHorasVoluntario } from '@app/actions/horas-voluntario.actions';
import { CarregarUsuariosInstitutoPorFiltro } from '@app/actions/usuario-instituto.actions';
import { CarregarVoluntariosPorCPF, CarregarVoluntariosPorId } from '@app/actions/voluntarioAction';
import { BaixarTermoCompromisso } from '@app/funcionalidades/configuracao-termo/actions/configuracao-termo.actions';
import * as fromConfiguracaoTermo from '@app/funcionalidades/configuracao-termo/reducers/configuracao-termo.reducer';
import * as fromHorasVoluntario from '@app/reducers/horas-voluntario.reducers';
import * as fromUsuarioInstituto from '@app/reducers/usuario-instituto.reducer';
import * as fromVoluntario from '@app/reducers/voluntario.reducer';
import { PageModelVm } from '@app/shared/models/page.model';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { VoluntarioDto } from '@app/shared/models/voluntario-dto.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { PerfilType } from '@app/shared/types/perfil.type';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import * as fromAuth from '@sicoob/security';
import * as jsPDF from 'jspdf';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DisplayToolbar } from 'src/app/app.actions';
import * as fromApp from '../../../../app.reduce';
import { LimparConvites, LimparTotais, LoadConvites, TotalConvites, TotalProjetos, LimparProjetos, LoadProjetos, InformarAcesso } from '../../actions/perfil-usuario.actions';
import { TabInformacoesUsuariosComponent } from '../../components/tab-informacoes-usuario/tab-informacoes-usuario.component';
import { TabsInformacoesUsuario } from '../../models/tab-informacoes-usuario.type';
import * as fromPerfilUsuario from '../../reducers/perfil-usuario.reducer';

@Component({
  selector: 'sc-container-perfil-usuario',
  templateUrl: './container-perfil-usuario.component.html',
  styleUrls: ['./container-perfil-usuario.component.scss']
})
export class ContainerPerfilUsuarioComponent implements OnInit, OnDestroy {

  @ViewChild("tabsInfo") tabInfo: TabInformacoesUsuariosComponent;
  subscriptions: Subscription = new Subscription();
  userlocalStorage: any;
  defaultImageUser: string = 'assets/images/ico-person.svg';
  isVisit: boolean;
  totalHoras$: Observable<number>;
  usuarioInstituto: VoluntarioDto | UsuarioInstituto;
  totalProjetos$: Observable<number>;
  totalConvites$: Observable<number>;
  id: string;
  perfil: string;
  mostrarProjetos: boolean;

  constructor(
    private persistenceService: PersistanceService,
    private storeTermos: Store<fromConfiguracaoTermo.State>,
    private storeUsuarioInstituto: Store<fromUsuarioInstituto.State>,
    private storeVoluntario: Store<fromVoluntario.State>,
    public appStore$: Store<fromApp.State>,
    public authStore$: Store<fromAuth.State>,
    public perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private route: ActivatedRoute,
    private router: Router,
    private horasVoluntarioStore$: Store<fromHorasVoluntario.State>,
  ) { }


  irParaCursos = () => this.tabInfo ? this.tabInfo.mudarAba(TabsInformacoesUsuario.cursos) : null;

  ngOnInit() {
    this.mostrarProjetos = false;
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.userlocalStorage = this.persistenceService.get("usuario_instituto");
    this.subscriptions.add(this.route.parent.paramMap.subscribe(params => {
      this.id = params.get("id");
      this.isVisit = this.id != this.userlocalStorage.id;
    })).add(this.route.parent.queryParamMap.subscribe((params: any) => {
      this.perfil = params.params.perfil;
      const redux = this.tratarPerfil();
      this.subscriptions.add(redux.store.pipe(select(redux.selector)).subscribe(u => {
        this.mostrarUsuario(u);
      }));
    }));
    this.totalHoras$ = this.horasVoluntarioStore$.pipe(select(fromHorasVoluntario.getTotalHorasVoluntario));
    this.totalConvites$ = this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getTotalConvites));
    this.totalProjetos$ = this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getTotalProjetos));
  }

  private havePerfil(object: any): object is UsuarioInstituto {
    return 'perfil' in object;
  }

  private mostrarUsuario(u: any) {
    this.usuarioInstituto = u;
    if (this.usuarioInstituto) {
      this.perfil = this.usuarioInstituto && this.havePerfil(this.usuarioInstituto) ? this.usuarioInstituto.perfil : PerfilType.voluntario;
      if (this.perfil === PerfilType.voluntario) {
        this.mostrarProjetos = true;
      }
      this.perfilUsuarioStore$.dispatch(new InformarAcesso(this.usuarioInstituto.id, this.isVisit, this.perfil));
    }
  }


  private tratarPerfil() {
    let store: Store<any>;
    let selector: MemoizedSelector<any, any>;
    if (this.userlocalStorage.perfil == PerfilType.voluntario || this.perfil == PerfilType.voluntario) {
      this.dispatchVoluntario(this.id);
      store = this.storeVoluntario;
      selector = fromVoluntario.getVoluntarioAtual;
    } else {
      this.dispatchUsuarioInstituto(this.id);
      store = this.storeUsuarioInstituto;
      selector = fromUsuarioInstituto.getUsuarioInstitutoAtual;
    }
    return { store, selector };
  }

  private dispatchVoluntario(id: string) {
    this.perfilUsuarioStore$.dispatch(new TotalConvites(id));
    this.perfilUsuarioStore$.dispatch(new TotalProjetos(id));
    this.storeVoluntario.dispatch(new CarregarVoluntariosPorId(id))
    this.horasVoluntarioStore$.dispatch(new CarregarTotalHorasVoluntario(id));
  }

  private dispatchUsuarioInstituto(id: string) {
    this.storeUsuarioInstituto.dispatch(new CarregarUsuariosInstitutoPorFiltro(null, null, null, true, id))
  }

  public get photo(): Observable<string> {
    if (!this.isVisit) {
      return this.authStore$.pipe(select(fromAuth.selectSicoobUser)).pipe(
        map((user) => user && user.avatar && user.avatar.foto ? user.avatar.foto : null))
    }

  }

  baixarTermo() {
    this.storeTermos.dispatch(new BaixarTermoCompromisso(this.perfil, this.id));
    this.subscriptions.add(this.storeTermos.pipe(select(fromConfiguracaoTermo.getTermoCompromissoBaixar)).subscribe(
      (termo: string) => {
        if (termo) {
          let doc = new jsPDF("p", "mm", "a4");
          doc.fromHTML(termo, 10, 10, {
            width: 180
          });
          doc.save('Termo de Compromisso.pdf');
        }
      }
    ))
  }

  ngOnDestroy(): void {
    this.perfilUsuarioStore$.dispatch(new LimparTotais());
    this.subscriptions.unsubscribe();
  }

  onChangeTab($event) {
    if ($event === TabsInformacoesUsuario.cursos) {
      this.router.navigate(['./'], { queryParams: { perfil: this.perfil }, relativeTo: this.route })
    } else if ($event === TabsInformacoesUsuario.projetos) {
      this.router.navigate(['./projetos'], { queryParams: { perfil: this.perfil }, relativeTo: this.route })
      this.perfilUsuarioStore$.dispatch(new LimparProjetos());
      this.perfilUsuarioStore$.dispatch(new TotalProjetos(this.id));
      this.perfilUsuarioStore$.dispatch(new LoadProjetos(this.id, new PageModelVm(0, 10, 0)));
    } else if ($event === TabsInformacoesUsuario.convites) {
      this.perfilUsuarioStore$.dispatch(new LimparConvites());
      this.perfilUsuarioStore$.dispatch(new TotalConvites(this.id));
      this.perfilUsuarioStore$.dispatch(new LoadConvites(this.id, new PageModelVm(0, 10, 0)));
      this.router.navigate(['./convites'], { relativeTo: this.route })
    }
  }
}
