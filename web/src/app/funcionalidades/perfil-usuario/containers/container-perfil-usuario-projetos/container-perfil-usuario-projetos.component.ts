import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { LimparProjetos, LoadProjetos } from '../../actions/perfil-usuario.actions';
import * as fromPerfilUsuario from '../../reducers/perfil-usuario.reducer';
import * as fromApp from '@app/app.reduce';
import { DisplayVisibilityHeader } from '@app/app.actions';

@Component({
  selector: 'sc-container-perfil-usuario-projetos',
  templateUrl: './container-perfil-usuario-projetos.component.html',
  styleUrls: ['./container-perfil-usuario-projetos.component.scss']
})
export class ContainerPerfilUsuarioProjetosComponent implements OnInit, OnDestroy {

  userlocalStorage: any;
  paginacao = new PageModelVm(0, 10, 0);
  subscription: Subscription = new Subscription();
  projetos: any[] = [];
  isVisitante: boolean

  constructor(
    private perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private appStore$: Store<fromApp.State>,
    private persistenceService: PersistanceService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.userlocalStorage = this.persistenceService.get("usuario_instituto");
    this.subscription
      .add(this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getInfosVisitante)).subscribe((infosVisitante: { id: string, isVisitante: boolean, perfil: string }) => {
        this.isVisitante = infosVisitante.isVisitante;
        this.perfilUsuarioStore$.dispatch(new LoadProjetos(infosVisitante.id, this.paginacao))
      }))
      .add(this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getProjetos)).subscribe((pagedModel: PagedDataModelVm) => {
        this.projetos = pagedModel.data;
        this.paginacao = pagedModel.page;
      }));
  }

  countPassos(projeto): number{
    return projeto && projeto.passosFluxo ? projeto.passosFluxo.length : 0
  }

  ngOnDestroy(): void {
    this.perfilUsuarioStore$.dispatch(new LimparProjetos());
    this.subscription.unsubscribe();
  }

  getDataInicio(data: Date) {
    const datePipe = new DatePipe('pt-br');
    const dataInicio = datePipe.transform(data, "dd 'de' MMMM");
    return `Início em ${dataInicio}`;
  }

  irParaAtividades(id: string) {
    this.appStore$.dispatch(new DisplayVisibilityHeader(false))
    this.router.navigate(['/projetos', id, 'tarefas'])
  }

}
