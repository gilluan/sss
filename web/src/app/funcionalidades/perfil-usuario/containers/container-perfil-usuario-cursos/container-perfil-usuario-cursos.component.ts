import { Component, OnDestroy, OnInit, ViewChild, TemplateRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarregarArquivo, LimparStoreGed } from '@app/actions/ged.actions';
import { DialogoConfirmacaoComponent } from '@app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { DadosDocumento } from '@app/shared/ged/models/retorno/dadosDocumento.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { PerfilType } from '@app/shared/types/perfil.type';
import { select, Store } from '@ngrx/store';
import { ActionBarRef, ActionBarService, ModalService, HeaderActionsContainerService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import * as fromGed from '../../../../reducers/ged.reducer';
import { ExcluirCurso, ListarCursos, LimparCursos } from '../../actions/perfil-usuario.actions';
import { ActionBarCadastroCursoComponent } from '../../components/action-bar-cadastro-curso/action-bar-cadastro-curso.component';
import { CursoFiltro } from '../../models/curso.filtro';
import { Curso } from '../../models/curso.model';
import * as fromPerfilUsuario from '../../reducers/perfil-usuario.reducer';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'sc-container-perfil-usuario-cursos',
  templateUrl: './container-perfil-usuario-cursos.component.html',
  styleUrls: ['./container-perfil-usuario-cursos.component.scss']
})
export class ContainerPerfilUsuarioCursosComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;
  cursos$: Observable<Curso[]>;
  actionsBarRef: ActionBarRef[] = [];
  subscription: Subscription = new Subscription();
  userlocalStorage: any;
  perfil: string;
  isVisitante: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private actionBarService: ActionBarService,
    private persistenceService: PersistanceService,
    private modalService: ModalService,
    private gedStore$: Store<fromGed.State>,
    private headerActionsService: HeaderActionsContainerService,
  ) { }

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  ngOnInit() {
    this.userlocalStorage = this.persistenceService.get("usuario_instituto");
    this.cursos$ = this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getCursos));
    this.subscription.add(this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getInfosVisitante)).subscribe((infosVisitante: { id: string, isVisitante: boolean, perfil: string }) => {
      this.perfil = infosVisitante.perfil;
      this.isVisitante = infosVisitante.isVisitante;
      if (infosVisitante.perfil === PerfilType.voluntario) {
        this.perfilUsuarioStore$.dispatch(new ListarCursos(new CursoFiltro(infosVisitante.id)));
      } else
        this.perfilUsuarioStore$.dispatch(new ListarCursos(new CursoFiltro(null, infosVisitante.id)));
    }));

  }

  adicionarCapacitacao() {
    const actionBarRef = this.actionBarService.open(ActionBarCadastroCursoComponent, {});
    this.actionsBarRef.push(actionBarRef);
    this.subscription.add(actionBarRef.afterClosed().subscribe(() => {
      this.router.navigate(['./'], { queryParams: {perfil: this.perfil}, relativeTo: this.route })
    }));
  }

  editarCurso(curso: Curso) {
    const actionBarRef = this.actionBarService.open(ActionBarCadastroCursoComponent, { data: { curso } });
    this.actionsBarRef.push(actionBarRef);
  }

  excluirCertificado(curso: Curso) {
    let header = "Atenção";
    let pergunta = "Todas as informações dessa capacitação serão excluídas. Deseja continuar?";
    let adicional = "Essa ação não pode ser desfeita";
    let txtSim = "Continuar";
    let txtNao = "Cancelar";
    const modalConfig = { data: { header, pergunta, adicional, txtSim, txtNao }, panelClass: 'sins-modal-confirm' };
    const modalRef = this.modalService.open(DialogoConfirmacaoComponent, modalConfig);
    this.subscription.add(modalRef.afterClosed().subscribe(
      confirmacao => {
        if (confirmacao) {
          if (this.userlocalStorage.perfil == PerfilType.voluntario)
            this.perfilUsuarioStore$.dispatch(new ExcluirCurso(curso, new CursoFiltro(this.userlocalStorage.id)))
          else
            this.perfilUsuarioStore$.dispatch(new ExcluirCurso(curso, new CursoFiltro(null, this.userlocalStorage.id)))
        }
      }
    ))
  }

  baixarCertificado(curso: Curso) {
    this.gedStore$.dispatch(new CarregarArquivo(curso.identificadorCertificadoGED));
    this.subscription.add(this.gedStore$.pipe(select(fromGed.selectAll)).subscribe((arquivos: DadosDocumento[]) => {
      if (arquivos && arquivos.length > 0 && curso) {
        this.downLoadFile(arquivos[0].listaSequenciaisDocumento[0].arquivoCodificadoBase64, `${curso.nome}.pdf`)
        curso = null;
      }
    }))
  }

  private downLoadFile(dataBase64: string, filename: string) {
    let downloadLink = document.createElement('a');
    downloadLink.href = `data:application/pdf;base64,${dataBase64}` //window.URL.createObjectURL(new Blob([dataBase64], {type: 'application/pdf;base64'}));;
    downloadLink.setAttribute('download', filename)
    document.body.appendChild(downloadLink);
    downloadLink.click();
    downloadLink.remove();
    this.gedStore$.dispatch(new LimparStoreGed())
  }

  ngOnDestroy(): void {
    this.perfilUsuarioStore$.dispatch(new LimparCursos());
    this.headerActionsService.remove();
    this.subscription.unsubscribe();
    this.actionsBarRef.forEach(s => s.close());
  }

}
