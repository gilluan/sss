import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { LimparConvites, LoadConvites, ParticiparProjeto } from '../../actions/perfil-usuario.actions';
import * as fromPerfilUsuario from '../../reducers/perfil-usuario.reducer';

@Component({
  selector: 'sc-container-perfil-usuario-convites',
  templateUrl: './container-perfil-usuario-convites.component.html',
  styleUrls: ['./container-perfil-usuario-convites.component.scss']
})
export class ContainerPerfilUsuarioConvitesComponent implements OnInit, OnDestroy {

  userlocalStorage: any;
  paginacao = new PageModelVm(0, 10, 0);
  subscription: Subscription = new Subscription();
  convites: any[] = [];

  constructor(
    private perfilUsuarioStore$: Store<fromPerfilUsuario.State>,
    private persistenceService: PersistanceService,
  ) { }

  ngOnInit(): void {
    this.userlocalStorage = this.persistenceService.get("usuario_instituto");
    this.perfilUsuarioStore$.dispatch(new LoadConvites(this.userlocalStorage.id, this.paginacao))
    this.subscription.add(this.perfilUsuarioStore$.pipe(select(fromPerfilUsuario.getConvites)).subscribe((pagedModel: PagedDataModelVm) => {
      this.convites = pagedModel.data;
      this.paginacao = pagedModel.page;
    }));
  }

  ngOnDestroy(): void {
    this.perfilUsuarioStore$.dispatch(new LimparConvites());
    this.subscription.unsubscribe();
  }

  participar(convite: any) {
    this.perfilUsuarioStore$.dispatch(new ParticiparProjeto(convite.projeto._id, this.userlocalStorage.id));
  }

}
