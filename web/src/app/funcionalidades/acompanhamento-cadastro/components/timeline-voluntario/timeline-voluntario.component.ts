import { Component, Input } from '@angular/core';


export type TimeLineVoluntario = {
  label: string,
  icon: string,
  complete: boolean,
  current: boolean,
  error: boolean,
}

@Component({
  selector: 'sc-timeline-voluntario',
  templateUrl: './timeline-voluntario.component.html',
  styleUrls: ['./timeline-voluntario.component.scss']
})
export class TimelineVoluntarioComponent  {


  @Input() listaSituacao: TimeLineVoluntario[];



}
