import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompanhamentoCadastroComponent } from './acompanhamento-cadastro.component';

describe('AcompanhamentoCadastroComponent', () => {
  let component: AcompanhamentoCadastroComponent;
  let fixture: ComponentFixture<AcompanhamentoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcompanhamentoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompanhamentoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
