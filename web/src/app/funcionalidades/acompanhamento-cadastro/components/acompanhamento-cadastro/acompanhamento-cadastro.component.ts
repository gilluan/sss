import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SituacaoVoluntarioType} from 'src/app/shared/types/situacao-voluntario.type';
import {ScreenObjectsInfo} from '../../models/screen-objects-info';
import * as fromApp from '../../../../app.reduce';
import {Store} from '@ngrx/store';
import {DisplayToolbar} from 'src/app/app.actions';
import {PersistanceService} from 'src/app/shared/services/persistence.service';
import { VoluntariosService } from '@app/funcionalidades/sins-analise-cadastros/voluntarios.service';

@Component({
  selector: 'sc-acompanhamento-cadastro',
  templateUrl: './acompanhamento-cadastro.component.html',
  styleUrls: ['./acompanhamento-cadastro.component.css']
})
export class AcompanhamentoCadastroComponent implements OnInit {

  constructor(private voluntarioService: VoluntariosService,
    private router: Router,
    private changeDetector: ChangeDetectorRef,
    public appStore$: Store<fromApp.State>,
    private persistenceService: PersistanceService) { }

  cpf: string;
  situacao: SituacaoVoluntarioType;
  screenObjectsInfo: ScreenObjectsInfo = new ScreenObjectsInfo();
  listaSituacao = [
    {
      label: "Envio de dados",
      icon: "telegram",
      complete: true,
      current: false,
      error: false,
    },
    {
      label: "Análise de dados",
      icon: "clipboard-text-outline",
      complete: false,
      current: true,
      error: false,
    },
    {
      label: "Em autorização",
      icon: "pencil",
      complete: false,
      current: false,
      error: false,
    },
    {
      label: "Autorizado",
      icon: "check-bold",
      complete: false,
      current: false,
      error: false,
    }
  ]

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(false));
    const userInstituto = this.persistenceService.get('usuario_instituto');
    this.cpf = userInstituto.cpf;
    this.voluntarioService.findVoluntarioByCpf(this.cpf).subscribe(voluntarioFound => {
      this.situacao = SituacaoVoluntarioType[voluntarioFound.situacao];
      this.screenObjectsInfo.setPasso(this.situacao);
      for (let i = 0; i < this.listaSituacao.length; i++) {
        const element = this.listaSituacao[i];
        element.complete = this.screenObjectsInfo.listaSituacao[i].complete;
        element.current  = this.screenObjectsInfo.listaSituacao[i].current;
        element.error = this.screenObjectsInfo.listaSituacao[i].error;
        element.icon = element.error ? 'close' : element.icon;
      }
      this.changeDetector.detectChanges();
    });
  }

  clickButtonRight() {
    const win = window.open('https://www.youtube.com/watch?v=8E2LagHOPb0');
  }

  clickButtonLeft() {
    if (this.situacao === SituacaoVoluntarioType.ajusteVoluntario) {
      this.router.navigate(['welcome/dados-cadastrais']);
    } else if (this.situacao === SituacaoVoluntarioType.aguardandoAnalise ||
               this.situacao === SituacaoVoluntarioType.devolvidoAjuste ||
               this.situacao === SituacaoVoluntarioType.aguardandoAssinatura) {
      const win = window.open('http://www.institutosicoob.org.br/');
    } else if (this.situacao === SituacaoVoluntarioType.assinado) {
      const win = window.open('http://www.institutosicoob.org.br/SINS');
    }
  }
}
