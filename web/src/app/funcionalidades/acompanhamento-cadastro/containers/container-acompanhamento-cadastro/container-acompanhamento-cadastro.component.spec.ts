import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerAcompanhamentoCadastroComponent } from './container-acompanhamento-cadastro.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerAcompanhamentoCadastroComponent', () => {
  let component: ContainerAcompanhamentoCadastroComponent;
  let fixture: ComponentFixture<ContainerAcompanhamentoCadastroComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerAcompanhamentoCadastroComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerAcompanhamentoCadastroComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
