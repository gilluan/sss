import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '@app/app.reduce';
import { DisplayVisibilityHeader, DisplayToolbar } from '@app/app.actions';

@Component({
  selector: 'sc-container-acompanhamento-cadastro',
  templateUrl: './container-acompanhamento-cadastro.component.html',
  styleUrls: ['./container-acompanhamento-cadastro.component.css']
})
export class ContainerAcompanhamentoCadastroComponent implements OnInit, OnDestroy {

  constructor(public appStore$: Store<fromApp.State>,) { }

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(false));
    this.appStore$.dispatch(new DisplayVisibilityHeader(false));
  }

  ngOnDestroy(): void {
    this.appStore$.dispatch(new DisplayVisibilityHeader(true))
  }

}
