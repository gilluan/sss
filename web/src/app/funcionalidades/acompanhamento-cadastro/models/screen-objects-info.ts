import { SituacaoVoluntarioType } from 'src/app/shared/types/situacao-voluntario.type';
import { TimeLineVoluntario } from '../components/timeline-voluntario/timeline-voluntario.component';
export class ScreenObjectsInfo {
  imagePathTimeline: string;
  imagePathInfoDados: string;
  isImagePathInfoDadosVisible: boolean;
  textoInfoDados: string;
  isTextoInfoDadosVisible: boolean;
  textoPasso: string;
  isTextoPassoVisible: boolean;
  textoTituloPasso: string;
  isTextoTituloPassoVisible: boolean;
  textoDetalhesPasso: string;
  isTextoDetalhesPassoVisible: boolean;
  textoDetalhes2Passo: string;
  isTextoDetalhes2PassoVisible: boolean;
  textoButtonLeft: string;
  isButtonLeftVisible: boolean;
  textoButtonRight: string;
  isButtonRightVisible: boolean;
  mainImagePath: string;
  backgroundColor: string;
  listaSituacao: any;

  private setPasso2(){
    this.imagePathTimeline = "./assets/images/2-timeline-analise-dados.png";
    this.imagePathInfoDados = './assets/images/dados-enviados.png';
    this.isImagePathInfoDadosVisible = true;
    this.textoInfoDados = "Seus dados foram recebidos!";
    this.isTextoInfoDadosVisible = true;
    this.textoPasso = "Passo 2";
    this.isTextoPassoVisible = true;
    this.textoTituloPasso = "Análise dos dados e Certificado de Voluntário.";
    this.isTextoTituloPassoVisible = true;
    this.textoDetalhesPasso = "Seus dados foram enviados com sucesso, pedimos que aguarde a " +
      "análise e acompanhe sua adesão como voluntário por aqui.";
    this.isTextoDetalhesPassoVisible = true;
    this.textoDetalhes2Passo = "";
    this.isTextoDetalhes2PassoVisible = false;
    this.textoButtonLeft = "Site do Instituto";
    this.isButtonLeftVisible = true;
    this.textoButtonRight = "Mais sobre o Instituto";
    this.isButtonRightVisible =  true;
    this.mainImagePath = './assets/images/2-analise-dados.png';
    this.backgroundColor = "ECFDFD";
    this.listaSituacao = [
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: false,
        current: true,
        error: false
      },
      {
        complete: false,
        current: false,
        error: false
      },
      {
        complete: false,
        current: false,
        error: false
      }
    ];
  }

  private setPasso3(){
    this.imagePathTimeline = "./assets/images/3-timeline-enviado-autorizacao.png";
    this.imagePathInfoDados = './assets/images/dados-enviados.png';
    this.isImagePathInfoDadosVisible = false;
    this.textoInfoDados = "";
    this.isTextoInfoDadosVisible = false;
    this.textoPasso = "Passo 3";
    this.isTextoPassoVisible = true;
    this.textoTituloPasso = "Enviado para autorização!";
    this.isTextoTituloPassoVisible = true;
    this.textoDetalhesPasso = "Oba! Suas informações foram recebidas e seu cadastro foi enviado " +
      "para assinatura.";
    this.isTextoDetalhesPassoVisible = true;
    this.textoDetalhes2Passo = "";
    this.isTextoDetalhes2PassoVisible = false;
    this.textoButtonLeft = "Site do Instituto";
    this.isButtonLeftVisible = true;
    this.textoButtonRight = "Mais sobre o Instituto";
    this.isButtonRightVisible =  true;
    this.mainImagePath = './assets/images/3-enviado-autorizacao.png';
    this.backgroundColor = "ECFDFD";
    this.listaSituacao = [
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: false,
        current: true,
        error: false
      },
      {
        complete: false,
        current: false,
        error: false
      }
    ];
  }

  private setPasso4(){
    this.imagePathTimeline = "./assets/images/4-timeline-autorizado.png";
    this.imagePathInfoDados = './assets/images/dados-enviados.png';
    this.isImagePathInfoDadosVisible = false;
    this.textoInfoDados = "";
    this.isTextoInfoDadosVisible = false;
    this.textoPasso = "Passo 4";
    this.isTextoPassoVisible = true;
    this.textoTituloPasso = "Parabéns! Seja bem-vindo ao time de voluntários do Instituto Sicoob.";
    this.isTextoTituloPassoVisible = true;
    this.textoDetalhesPasso = "Após análise e aprovação das informações enviadas, seu cadastro " +
      "como voluntário foi confirmado. Agora você poderá gerenciar as atividades relacionadas " +
      "ao Instituto em uma única ferramenta.";
    this.isTextoDetalhesPassoVisible = true;
    this.textoDetalhes2Passo = "Procure o PAE ou PDE para liberação das devidas permissões do " +
      "SINS e início das atividades.";
    this.isTextoDetalhes2PassoVisible = true;
    this.textoButtonLeft = "Sobre o Sistema";
    this.isButtonLeftVisible = true;
    this.textoButtonRight = "Mais sobre o Instituto";
    this.isButtonRightVisible =  true;
    this.mainImagePath = './assets/images/4-autorizado.png';
    this.backgroundColor = "ECFDFD";
    this.listaSituacao = [
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: true,
        current: false,
        error: false
      }
    ];
  }

  private setPasso5(){
    this.imagePathTimeline = "./assets/images/5-timeline-ajuste-voluntario.png";
    this.imagePathInfoDados = './assets/images/dados-incorretos.png';
    this.isImagePathInfoDadosVisible = true;
    this.textoInfoDados = "Dados incorretos!";
    this.isTextoInfoDadosVisible = true;
    this.textoPasso = "Passo 2";
    this.isTextoPassoVisible = true;
    this.textoTituloPasso = "Análise dos dados e Certificado de Voluntário.";
    this.isTextoTituloPassoVisible = true;
    this.textoDetalhesPasso = "Ops! Encontramos algumas incoerências no seu cadastro. Solicitamos " +
      "que realize as correções para que o processo possa ser continuado.";
    this.isTextoDetalhesPassoVisible = true;
    this.textoDetalhes2Passo = "";
    this.isTextoDetalhes2PassoVisible = false;
    this.textoButtonLeft = "Corrigir dados";
    this.isButtonLeftVisible = true;
    this.textoButtonRight = "";
    this.isButtonRightVisible = false;
    this.mainImagePath = './assets/images/5-ajuste-voluntario.png';
    this.backgroundColor = "F5F7F8";
    this.listaSituacao = [
      {
        complete: true,
        current: false,
        error: false
      },
      {
        complete: false,
        current: false,
        error: true
      },
      {
        complete: false,
        current: false,
        error: false
      },
      {
        complete: false,
        current: false,
        error: false
      }
    ];
  }

  public setPasso(situacao: SituacaoVoluntarioType){
    if (situacao === SituacaoVoluntarioType.aguardandoAnalise || situacao === SituacaoVoluntarioType.devolvidoAjuste){
        this.setPasso2();
    } else if (situacao === SituacaoVoluntarioType.aguardandoAssinatura) {
        this.setPasso3();
    } else if (situacao === SituacaoVoluntarioType.assinado) {
      this.setPasso4();
    } else if (situacao === SituacaoVoluntarioType.ajusteVoluntario) {
      this.setPasso5();
    }
  }
}
