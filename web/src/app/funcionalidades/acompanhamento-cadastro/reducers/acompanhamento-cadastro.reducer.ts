import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { AcompanhamentoCadastro } from '../models/acompanhamento-cadastro.model';
import { AcompanhamentoCadastroActions, AcompanhamentoCadastroActionTypes } from '../actions/acompanhamento-cadastro.actions';

export interface State extends EntityState<AcompanhamentoCadastro> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AcompanhamentoCadastro> = createEntityAdapter<AcompanhamentoCadastro>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: AcompanhamentoCadastroActions
): State {
  switch (action.type) {
    case AcompanhamentoCadastroActionTypes.AddAcompanhamentoCadastro: {
      return adapter.addOne(action.payload.acompanhamentoCadastro, state);
    }

    case AcompanhamentoCadastroActionTypes.UpsertAcompanhamentoCadastro: {
      return adapter.upsertOne(action.payload.acompanhamentoCadastro, state);
    }

    case AcompanhamentoCadastroActionTypes.AddAcompanhamentoCadastros: {
      return adapter.addMany(action.payload.acompanhamentoCadastros, state);
    }

    case AcompanhamentoCadastroActionTypes.UpsertAcompanhamentoCadastros: {
      return adapter.upsertMany(action.payload.acompanhamentoCadastros, state);
    }

    case AcompanhamentoCadastroActionTypes.UpdateAcompanhamentoCadastro: {
      return adapter.updateOne(action.payload.acompanhamentoCadastro, state);
    }

    case AcompanhamentoCadastroActionTypes.UpdateAcompanhamentoCadastros: {
      return adapter.updateMany(action.payload.acompanhamentoCadastros, state);
    }

    case AcompanhamentoCadastroActionTypes.DeleteAcompanhamentoCadastro: {
      return adapter.removeOne(action.payload.id, state);
    }

    case AcompanhamentoCadastroActionTypes.DeleteAcompanhamentoCadastros: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case AcompanhamentoCadastroActionTypes.LoadAcompanhamentoCadastros: {
      return adapter.addAll(action.payload.acompanhamentoCadastros, state);
    }

    case AcompanhamentoCadastroActionTypes.ClearAcompanhamentoCadastros: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
