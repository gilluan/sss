import { TestBed } from '@angular/core/testing';

import { AcompanhamentoCadastroService } from './acompanhamento-cadastro.service';

describe('AcompanhamentoCadastroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AcompanhamentoCadastroService = TestBed.get(AcompanhamentoCadastroService);
    expect(service).toBeTruthy();
  });
});
