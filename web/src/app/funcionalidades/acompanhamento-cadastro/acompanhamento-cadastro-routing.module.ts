import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerAcompanhamentoCadastroComponent } from './containers/container-acompanhamento-cadastro/container-acompanhamento-cadastro.component';
import { AcompanhamentoCadastroComponent } from './components/acompanhamento-cadastro/acompanhamento-cadastro.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerAcompanhamentoCadastroComponent,
    children: [
      {
        path: '',
        component: AcompanhamentoCadastroComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcompanhamentoCadastroRoutingModule { }
