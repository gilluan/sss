import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AcompanhamentoCadastroEffects } from './acompanhamento-cadastro.effects';

describe('AcompanhamentoCadastroEffects', () => {
  let actions$: Observable<any>;
  let effects: AcompanhamentoCadastroEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AcompanhamentoCadastroEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(AcompanhamentoCadastroEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
