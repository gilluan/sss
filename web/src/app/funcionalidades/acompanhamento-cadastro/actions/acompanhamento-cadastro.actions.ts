import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AcompanhamentoCadastro } from '../models/acompanhamento-cadastro.model';

export enum AcompanhamentoCadastroActionTypes {
  LoadAcompanhamentoCadastros = '[AcompanhamentoCadastro] Load AcompanhamentoCadastros',
  AddAcompanhamentoCadastro = '[AcompanhamentoCadastro] Add AcompanhamentoCadastro',
  UpsertAcompanhamentoCadastro = '[AcompanhamentoCadastro] Upsert AcompanhamentoCadastro',
  AddAcompanhamentoCadastros = '[AcompanhamentoCadastro] Add AcompanhamentoCadastros',
  UpsertAcompanhamentoCadastros = '[AcompanhamentoCadastro] Upsert AcompanhamentoCadastros',
  UpdateAcompanhamentoCadastro = '[AcompanhamentoCadastro] Update AcompanhamentoCadastro',
  UpdateAcompanhamentoCadastros = '[AcompanhamentoCadastro] Update AcompanhamentoCadastros',
  DeleteAcompanhamentoCadastro = '[AcompanhamentoCadastro] Delete AcompanhamentoCadastro',
  DeleteAcompanhamentoCadastros = '[AcompanhamentoCadastro] Delete AcompanhamentoCadastros',
  ClearAcompanhamentoCadastros = '[AcompanhamentoCadastro] Clear AcompanhamentoCadastros'
}

export class LoadAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.LoadAcompanhamentoCadastros;

  constructor(public payload: { acompanhamentoCadastros: AcompanhamentoCadastro[] }) {}
}

export class AddAcompanhamentoCadastro implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.AddAcompanhamentoCadastro;

  constructor(public payload: { acompanhamentoCadastro: AcompanhamentoCadastro }) {}
}

export class UpsertAcompanhamentoCadastro implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.UpsertAcompanhamentoCadastro;

  constructor(public payload: { acompanhamentoCadastro: AcompanhamentoCadastro }) {}
}

export class AddAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.AddAcompanhamentoCadastros;

  constructor(public payload: { acompanhamentoCadastros: AcompanhamentoCadastro[] }) {}
}

export class UpsertAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.UpsertAcompanhamentoCadastros;

  constructor(public payload: { acompanhamentoCadastros: AcompanhamentoCadastro[] }) {}
}

export class UpdateAcompanhamentoCadastro implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.UpdateAcompanhamentoCadastro;

  constructor(public payload: { acompanhamentoCadastro: Update<AcompanhamentoCadastro> }) {}
}

export class UpdateAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.UpdateAcompanhamentoCadastros;

  constructor(public payload: { acompanhamentoCadastros: Update<AcompanhamentoCadastro>[] }) {}
}

export class DeleteAcompanhamentoCadastro implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.DeleteAcompanhamentoCadastro;

  constructor(public payload: { id: string }) {}
}

export class DeleteAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.DeleteAcompanhamentoCadastros;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearAcompanhamentoCadastros implements Action {
  readonly type = AcompanhamentoCadastroActionTypes.ClearAcompanhamentoCadastros;
}

export type AcompanhamentoCadastroActions =
 LoadAcompanhamentoCadastros
 | AddAcompanhamentoCadastro
 | UpsertAcompanhamentoCadastro
 | AddAcompanhamentoCadastros
 | UpsertAcompanhamentoCadastros
 | UpdateAcompanhamentoCadastro
 | UpdateAcompanhamentoCadastros
 | DeleteAcompanhamentoCadastro
 | DeleteAcompanhamentoCadastros
 | ClearAcompanhamentoCadastros;
