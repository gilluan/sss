import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcompanhamentoCadastroRoutingModule } from './acompanhamento-cadastro-routing.module';
import { StoreModule } from '@ngrx/store';
import * as fromAcompanhamentoCadastro from './reducers/acompanhamento-cadastro.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AcompanhamentoCadastroEffects } from './effects/acompanhamento-cadastro.effects';
import { ContainerAcompanhamentoCadastroComponent } from './containers/container-acompanhamento-cadastro/container-acompanhamento-cadastro.component';
import { AcompanhamentoCadastroComponent } from './components/acompanhamento-cadastro/acompanhamento-cadastro.component';
import {TimelineVoluntarioComponent} from './components/timeline-voluntario/timeline-voluntario.component';

@NgModule({
  declarations: [ContainerAcompanhamentoCadastroComponent, AcompanhamentoCadastroComponent, TimelineVoluntarioComponent],
  imports: [
    CommonModule,
    AcompanhamentoCadastroRoutingModule,
    StoreModule.forFeature('acompanhamentoCadastro', fromAcompanhamentoCadastro.reducer),
    EffectsModule.forFeature([AcompanhamentoCadastroEffects])
  ]
})
export class AcompanhamentoCadastroModule { }
