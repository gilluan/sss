import { BaseDto } from "@app/shared/models/base-dto.model";
import { Programa } from "@app/funcionalidades/programas/models/programas.model";

export class Portfolio extends BaseDto {
    _id?: string;
    descricao?: string;
    nome?: string;
    inicioVigencia?: Date;
    fimVigencia?: Date;
    programas?: Programa[]
 }
