import { Injectable } from '@angular/core';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { ProgramasService } from '@app/funcionalidades/programas/programas.service';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Color, ModalService } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { CriarPortfolio, CriarPortfolioFail, CriarPortfolioSuccess, ListarPortfolios, ListarPortfoliosFail, ListarPortfoliosSuccess, PortfolioActionTypes, PreCriarPrograma, PreCriarProgramaSuccess, PreCriarProgramaFail, EditarPortfolio, EditarPortfolioSuccess, EditarPortfolioFail, DeletarPortfolio, DeletarPortfolioSuccess, DeletarPortfolioFail } from '../actions/portfolio.actions';
import { ModalSuccessProgramaCriadoComponent } from '../components/modal-success-programa-criado/modal-success-programa-criado.component';
import { PortfolioService } from '../portfolio.service';
import { Router } from '@angular/router';

@Injectable()
export class PortfolioEffects {

  constructor(
    private modalService: ModalService,
    private actions$: Actions,
    private router: Router,
    private portfolioService: PortfolioService,
    private programaService: ProgramasService,
    private alertService: CustomAlertService) { }

  @Effect()
  listarPortfolios$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.ListaPortfolios),
    map((action: ListarPortfolios) => action),
    switchMap(() => this.portfolioService.listarPortfolios().pipe(
      map((portfolios) => new ListarPortfoliosSuccess(portfolios)),
      catchError((error: any) => of(new ListarPortfoliosFail(error)))
    )));

  @Effect({ dispatch: false })
  listarPortfoliosFail$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.ListarPortfoliosFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os eixos de atuação')));

  @Effect()
  criarPortfolio$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.CriarPortfolio),
    map((action: CriarPortfolio) => action),
    switchMap((action: CriarPortfolio) => this.portfolioService.criarPortfolio(action.portfolio).pipe(
      switchMap(_ => [new ListarPortfolios(), new CriarPortfolioSuccess()]))),
    catchError(erro => of(new CriarPortfolioFail(erro))));

  @Effect({ dispatch: false })
  criarPortfolioSuccess$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.CriarPortfolioSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Eixo de atuação criado com sucesso.')));

  @Effect({ dispatch: false })
  criarPortfolioFail$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.CriarPortfolioFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar um novo eixo de atuação')));

    @Effect()
    editarPortfolio$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.EditarPortfolio),
      map((action: EditarPortfolio) => action),
      switchMap((action: EditarPortfolio) => this.portfolioService.editarPortfolio(action.portfolio).pipe(
        switchMap(_ => [new ListarPortfolios(), new EditarPortfolioSuccess()]))),
      catchError(erro => of(new EditarPortfolioFail(erro))));

    @Effect({ dispatch: false })
    editarPortfolioSuccess$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.EditarPortfolioSuccess),
      map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Eixo de atuação editado com sucesso.')));

    @Effect({ dispatch: false })
    editarPortfolioFail$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.EditarPortfolioFail),
      map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível editar o eixo de atuação')));


    @Effect()
    deletarPortfolio$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.DeletarPortfolio),
      map((action: DeletarPortfolio) => action),
      switchMap((action: DeletarPortfolio) => this.portfolioService.deletarPortfolio(action.id).pipe(
        switchMap(_ => [new ListarPortfolios(), new DeletarPortfolioSuccess()]))),
      catchError(erro => of(new DeletarPortfolioFail(erro))));

    @Effect({ dispatch: false })
    deletarPortfolioSuccess$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.DeletarPortfolioSuccess),
      map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Eixo de atuação deletado com sucesso.')));

    @Effect({ dispatch: false })
    deletarPortfolioFail$ = this.actions$.pipe(
      ofType(PortfolioActionTypes.DeletarPortfolioFail),
      map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível deletar o eixo de atuação')));

  @Effect()
  criarPrograma$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.PreCriarPrograma),
    map((action: PreCriarPrograma) => action),
    switchMap((action: PreCriarPrograma) => this.programaService.criarPrograma(action.programa).pipe(
      switchMap((programa: Programa) => [new ListarPortfolios(), new PreCriarProgramaSuccess(programa)]))),
    catchError(erro => of(new PreCriarProgramaFail(erro))));

  @Effect({ dispatch: false })
  criarProgramaSuccess$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.PreCriarProgramaSuccess),
    map((action: PreCriarProgramaSuccess) => this.modalService.open(ModalSuccessProgramaCriadoComponent, { data: action.programa })));

  @Effect({ dispatch: false })
  criarProgramaFail$ = this.actions$.pipe(
    ofType(PortfolioActionTypes.PreCriarProgramaFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar um novo programa')));







}
