import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioRoutingModule } from './portfolio-routing.module';
import { StoreModule } from '@ngrx/store';
import * as fromPortfolio from './reducers/portfolio.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PortfolioEffects } from './effects/portfolio.effects';
import { ContainerPortfolioComponent } from './containers/container-portfolio/container-portfolio.component';
import { ModalModule, FormModule, CardModule } from '@sicoob/ui';
import { ModalNovoEixoComponent } from './components/modal-novo-eixo/modal-novo-eixo.component';
import { SharedModule } from '@app/shared/shared.module';
import { UiModule } from '@sicoob/ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuDropDownModule } from '@app/shared/components/menu-dropdown/menu-dropdown.module';
import { ModalNovoProgramaComponent } from './components/modal-novo-programa/modal-novo-programa.component';
import { ModalSuccessProgramaCriadoComponent } from './components/modal-success-programa-criado/modal-success-programa-criado.component';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { NgSelectModule } from '@ng-select/ng-select';

defineLocale('pt-br', ptBrLocale);

@NgModule({
  declarations: [ContainerPortfolioComponent, ModalNovoEixoComponent, ModalNovoProgramaComponent, ModalSuccessProgramaCriadoComponent],
  imports: [
    NgSelectModule,
    BsDatepickerModule.forRoot(),
    CommonModule,
    PortfolioRoutingModule,
    StoreModule.forFeature('portfolio', fromPortfolio.reducer),
    EffectsModule.forFeature([PortfolioEffects]),
    ModalModule,
    SharedModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule,
    FormModule,
    CardModule,
    SharedModule,
    MenuDropDownModule
  ],
  entryComponents: [
    ModalNovoEixoComponent,
    ModalNovoProgramaComponent,
    ModalSuccessProgramaCriadoComponent
  ],
  providers: [BsLocaleService]
})
export class PortfolioModule { }
