import { Action } from '@ngrx/store';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { Portfolio } from '../models/portfolio.model';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';

export enum PortfolioActionTypes {
  ListaPortfolios = '[Portfolio] Lista Portfolios',
  ListarPortfoliosSuccess = '[Portfolio] Lista Portfolios Success',
  ListarPortfoliosFail = '[Portfolio] Listar Portfolios Fail',
  ListarProgramasPortfolio = '[Portfolio] Lista Programas Portfolio',
  ListarProgramasPortfolioSuccess = '[Portfolio] Lista Programas Portfolio Success',
  ListarProgramasPortfolioFail = '[Portfolio] Lista Programas Portfolio Fail',
  CriarPortfolio = '[Portfolio] Cria Portfolio',
  CriarPortfolioSuccess = '[Portfolio] Cria Portfolio Success',
  CriarPortfolioFail = '[Portfolio] Cria Portfolio Fail',
  EditarPortfolio = '[Portfolio] EditarPortfolio',
  EditarPortfolioSuccess = '[Portfolio] EditarPortfolioSuccess',
  EditarPortfolioFail = '[Portfolio] EditarPortfolioFail',
  PreCriarPrograma = '[Portfolio] Pre Criar Programa',
  PreCriarProgramaSuccess = '[Portfolio] Pre Criar Programa Success',
  PreCriarProgramaFail = '[Portfolio] Pre Criar Programa Fail',
  DeletarPortfolio = '[Portfolio] DeletarPortfolio',
  DeletarPortfolioSuccess = '[Portfolio] DeletarPortfolioSuccess',
  DeletarPortfolioFail = '[Portfolio] DeletarPortfolioFail',
}

export class ListarPortfolios implements Action {
  readonly type = PortfolioActionTypes.ListaPortfolios;
  constructor() { }
}
export class ListarPortfoliosSuccess implements Action {
  readonly type = PortfolioActionTypes.ListarPortfoliosSuccess;
  constructor(public portfolios: Portfolio[]) { }
}
export class ListarPortfoliosFail implements Action {
  readonly type = PortfolioActionTypes.ListarPortfoliosFail;
  constructor(public portfolios: Portfolio[]) { }
}

export class ListarProgramasPortfolio implements Action {
  readonly type = PortfolioActionTypes.ListarProgramasPortfolio;
  constructor(public portfolioId: string) { }
}
export class ListarProgramasPortfolioSuccess implements Action {
  readonly type = PortfolioActionTypes.ListarProgramasPortfolioSuccess;
  constructor() { }
}
export class ListarProgramasPortfolioFail implements Action {
  readonly type = PortfolioActionTypes.ListarProgramasPortfolioFail;
  constructor() { }
}

export class CriarPortfolio implements Action {
  readonly type = PortfolioActionTypes.CriarPortfolio;
  constructor(public portfolio: Portfolio) { }
}
export class CriarPortfolioSuccess implements Action {
  readonly type = PortfolioActionTypes.CriarPortfolioSuccess;
  constructor() { }
}
export class CriarPortfolioFail implements Action {
  readonly type = PortfolioActionTypes.CriarPortfolioFail;
  constructor(public error: any) { }
}

export class EditarPortfolio implements Action {
  readonly type = PortfolioActionTypes.EditarPortfolio;
  constructor(public portfolio: Portfolio) { }
}
export class EditarPortfolioSuccess implements Action {
  readonly type = PortfolioActionTypes.EditarPortfolioSuccess;
  constructor() { }
}
export class EditarPortfolioFail implements Action {
  readonly type = PortfolioActionTypes.EditarPortfolioFail;
  constructor(public error: any) { }
}


export class DeletarPortfolio implements Action {
  readonly type = PortfolioActionTypes.DeletarPortfolio;
  constructor(public id: string) { }
}
export class DeletarPortfolioSuccess implements Action {
  readonly type = PortfolioActionTypes.DeletarPortfolioSuccess;
  constructor() { }
}
export class DeletarPortfolioFail implements Action {
  readonly type = PortfolioActionTypes.DeletarPortfolioFail;
  constructor(public error: any) { }
}

export class PreCriarPrograma implements Action {
  readonly type = PortfolioActionTypes.PreCriarPrograma;
  constructor(public programa: Programa) { }
}
export class PreCriarProgramaSuccess implements Action {
  readonly type = PortfolioActionTypes.PreCriarProgramaSuccess;
  constructor(public programa: Programa) { }
}
export class PreCriarProgramaFail implements Action {
  readonly type = PortfolioActionTypes.PreCriarProgramaFail;
  constructor(public error: any) { }
}

export type PortfolioActions =
  DeletarPortfolio |
  DeletarPortfolioSuccess |
  DeletarPortfolioFail |
  EditarPortfolio |
  EditarPortfolioSuccess |
  EditarPortfolioFail |
  ListarPortfolios |
  ListarPortfoliosSuccess |
  ListarPortfoliosFail |
  ListarProgramasPortfolio |
  ListarProgramasPortfolioSuccess |
  ListarProgramasPortfolioFail |
  CriarPortfolio |
  CriarPortfolioSuccess |
  CriarPortfolioFail |
  PreCriarPrograma |
  PreCriarProgramaSuccess |
  PreCriarProgramaFail;
