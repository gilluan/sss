import { TemplatePortal } from '@angular/cdk/portal';
import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as fromPortfolio from '@app/funcionalidades/portfolio/reducers/portfolio.reducer';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { DialogoConfirmacaoComponent } from '@app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { select, Store } from '@ngrx/store';
import { HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable } from 'rxjs';
import { DeletarPortfolio, ListarPortfolios } from '../../actions/portfolio.actions';
import { ModalNovoEixoComponent } from '../../components/modal-novo-eixo/modal-novo-eixo.component';
import { ModalNovoProgramaComponent } from '../../components/modal-novo-programa/modal-novo-programa.component';
import { Portfolio } from '../../models/portfolio.model';


type Label = {
  class: string,
  title: string,
}

@Component({
  selector: 'sc-container-portfolio',
  templateUrl: './container-portfolio.component.html',
  styleUrls: ['./container-portfolio.component.scss']
})
export class ContainerPortfolioComponent implements OnInit, AfterViewInit {

  @ViewChild("botoesPortfolio") headerActionsTmpl: TemplateRef<any>;
  portfolios$: Observable<Portfolio[]>;
  modalsRef: ModalRef[] = []

  constructor(
    private router: Router,
    private modalService: ModalService,
    private portfolioStore$: Store<fromPortfolio.State>,
    private headerActionsService: HeaderActionsContainerService,
  ) { }

  ngOnInit() {
    this.portfolioStore$.dispatch(new ListarPortfolios());
    this.portfolios$ = this.portfolioStore$.pipe(select(fromPortfolio.selectPortfolios));
  }

  ngAfterViewInit(): void {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  abrirModalPortfolio(portfolio: Portfolio) {
    const modalRef: ModalRef = this.modalService.open(ModalNovoEixoComponent, { data: { portfolio }, panelClass: 'sins-modal-novo-eixo' });
    this.modalsRef.push(modalRef);
  }

  excluirPortfolio(portfolio: Portfolio) {
    const modalRef: ModalRef = this.modalService.open(DialogoConfirmacaoComponent, { data: { header: 'Atenção', pergunta: 'Deseja realmente deletar o Eixo?' }, panelClass: 'sins-modal-novo-eixo' });
    modalRef.afterClosed().subscribe(s => s ? this.portfolioStore$.dispatch(new DeletarPortfolio(portfolio._id)) : null);
    this.modalsRef.push(modalRef);
  }

  abrirModalPrograma(portfolio: Portfolio) {
    const modalRef: ModalRef = this.modalService.open(ModalNovoProgramaComponent, { data: { portfolio: portfolio } });
    this.modalsRef.push(modalRef);
  }

  editarPrograma(programa) {
    this.router.navigate(['/programas/' + programa._id]);
  }

  tratarTotalProjetos(totalProjetos: number) {
    if (totalProjetos === 0)
      return "Nenhum projeto associado";
    else if (totalProjetos === 1)
      return "Um projeto associado";
    else
      return `${totalProjetos} projetos associados`;
  }

  ngOnDestroy(): void {
    this.headerActionsService.remove();
    this.modalsRef.forEach(s => s.close())
  }

  getLabel(programa: Programa): Label {
    if (programa) {
      if (this.isProgramaAtivo(programa)) {
        return { class: 'ss-label-success', title: 'Ativo' };
      } else if (this.isProgramaRascunho(programa)) {
        return { class: 'ss-label-default', title: 'Rascunho' };
      } else {
        return { class: 'ss-label-danger', title: 'Inativo' };
      }
    }
  }

  private isProgramaAtivo(programa) {
    return programa.ativo === true && !this.isProgramaRascunho(programa);
  }

  private isProgramaRascunho(programa) {
    return programa.ativo === undefined || programa.ativo === null;
  }



}
