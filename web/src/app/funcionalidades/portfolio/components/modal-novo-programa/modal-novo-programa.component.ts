import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Portfolio } from '@app/funcionalidades/portfolio/models/portfolio.model';
import { SalvarPrograma } from '@app/funcionalidades/programas/actions/programas.actions';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { select, Store } from '@ngrx/store';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import { range } from 'rxjs';
import { toArray } from 'rxjs/operators';
import * as fromPortfolio from '../../reducers/portfolio.reducer';
import { PreCriarPrograma } from '@app/funcionalidades/portfolio/actions/portfolio.actions';

@Component({
  selector: 'app-modal-novo-programa',
  templateUrl: './modal-novo-programa.component.html',
  styleUrls: ['./modal-novo-programa.component.scss']
})
export class ModalNovoProgramaComponent implements OnInit {

  vigencias = [];
  portfolio: Portfolio;
  headerTitle =  'Criando novo Programa';
  submitLabel = 'Criar novo programa';

  constructor(
    public modalRef: ModalRef, // Passar parametros ao fechar no modalRef.close({nome: 'test'})
    @Inject(MODAL_DATA) public data: any,
    private fb: FormBuilder,
    private portfolioStore$: Store<fromPortfolio.State>
  ) { }

  ngOnInit() {
   this.populateVigencia();
  }

  public criarProgramaForm = this.fb.group({
    vigencia: [null, Validators.required],
    nome: ['', Validators.required]
  });

  criarPrograma() {
    let programa = new Programa();
    programa.vigencia = this.criarProgramaForm.value.vigencia;
    programa.nome = this.criarProgramaForm.value.nome;
    programa.portfolio = this.data.portfolio._id;
    this.portfolioStore$.dispatch(new PreCriarPrograma(programa));
    this.modalRef.close(programa);
  }

  fechar() {
    this.modalRef.close();
  }

  private populateVigencia() {
    if (this.data.portfolio) {
      this.portfolio = this.data.portfolio;
      if (this.portfolio.inicioVigencia) {
        const inicioVigencia = new Date(this.portfolio.inicioVigencia);
        range(inicioVigencia.getFullYear(), 3)
        .pipe(toArray())
        .subscribe((anos: number[]) => this.vigencias = anos);
      }
    }
  }
}
