import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { CriarPortfolio, EditarPortfolio } from '@app/funcionalidades/portfolio/actions/portfolio.actions';
import { Portfolio } from '@app/funcionalidades/portfolio/models/portfolio.model';
import { Store } from '@ngrx/store';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import * as fromPortfolio from '../../reducers/portfolio.reducer';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'ss-portfolio-modal-novo-eixo',
  templateUrl: './modal-novo-eixo.component.html',
  styleUrls: ['./modal-novo-eixo.component.scss']
})
export class ModalNovoEixoComponent implements OnInit {

  headerTitle: string = 'Criando novo Eixo de Atuação';
  submitLabel: string = 'Criar Eixo';
  editar: boolean = false;
  portfolio: Portfolio;
  minDate: Date = new Date('2018-01-01:00:00:00');

  constructor(
    public modalRef: ModalRef, // Passar parametros ao fechar no modalRef.close({nome: 'test'})
    @Inject(MODAL_DATA) public data: any,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private portfolioStore$: Store<fromPortfolio.State>,
  ) { }

  ngOnInit() {
    this.localeService.use("pt-br");
    if(this.data.portfolio) {
      this.portfolio = this.data.portfolio;
      this.headerTitle = 'Editando Eixo de Atuação';
      this.submitLabel = 'Editar Eixo';
      this.editar = true;
      this.patchForm(this.portfolio.programas && this.portfolio.programas.length > 0 && !!this.portfolio.programas[0].nome);
    }
    this.setFimVigencia();
  }

  private patchForm(disable: boolean = false) {
    this.criarPortfolioForm.patchValue({
      nome: this.portfolio.nome,
      descricao: this.portfolio.descricao,
      inicioVigencia: this.portfolio.inicioVigencia,
      fimVigencia: new Date(this.portfolio.fimVigencia).getFullYear(),
    });
    if(disable) {
      this.inicioVigencia.disable();
      this.fimVigencia.disable();
    }
  }

  public criarPortfolioForm = this.fb.group({
    nome: ['', Validators.required],
    descricao: ['', Validators.required],
    inicioVigencia: ['', Validators.required],
    fimVigencia: ['', Validators.required]
  });

  private setFimVigencia() {
    if(this.inicioVigencia) {
      this.inicioVigencia.valueChanges.subscribe((date: Date) => {
        if(date && date.getFullYear() && date.getFullYear() >= 2018) {
          this.fimVigencia.setValue(`${date.getFullYear() + 2}`);
        }
      });
    }
  }

  get fimVigencia() {
    return this.criarPortfolioForm.get('fimVigencia') as FormControl;
  }

  get inicioVigencia() {
    return this.criarPortfolioForm.get('inicioVigencia') as FormControl;
  }

  criarPortfolio() {
    const portfolio: Portfolio = {
      nome: this.criarPortfolioForm.value.nome,
      descricao: this.criarPortfolioForm.value.descricao,
      inicioVigencia: this.inicioVigencia.value,
      fimVigencia: new Date(`${this.fimVigencia.value}-12-31T23:59:59`)
    }
    if(this.editar) {
      const id = this.portfolio.id ? this.portfolio.id : this.portfolio._id;
      const portfolioEditar: Portfolio = {...portfolio, id, dataCriacao: this.portfolio.dataCriacao, dataModificacao: this.portfolio.dataModificacao};
      this.portfolioStore$.dispatch(new EditarPortfolio(portfolioEditar));
    } else {
      this.portfolioStore$.dispatch(new CriarPortfolio(portfolio));
    }
    this.fechar();
  }

  fechar() {
    this.modalRef.close();
  }



}
