import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';

@Component({
  selector: 'app-modal-success-programa-criado',
  templateUrl: './modal-success-programa-criado.component.html',
  styleUrls: ['./modal-success-programa-criado.component.scss']
})
export class ModalSuccessProgramaCriadoComponent implements OnInit {

  constructor(
    private router: Router,
    public modalRef: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit() {
  }

  continuar() {
    console.log(this.data._id)
    this.router.navigate(['/programas/'+this.data._id]);
    this.modalRef.close(this.data);
  }

  preencherDepois() {
    this.modalRef.close();
  }

}
