import { PageModelVm } from '@app/shared/models/page.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PortfolioActions, PortfolioActionTypes } from '../actions/portfolio.actions';
import { Portfolio } from '../models/portfolio.model';

export interface State extends EntityState<Portfolio> {
  paginacao: PageModelVm;
  portfolios: Portfolio[]
  portfolio: Portfolio
}

export const adapter: EntityAdapter<Portfolio> = createEntityAdapter<Portfolio>();

export const initialState: State = adapter.getInitialState({
  paginacao: new PageModelVm(0, 8, 0),
  portfolios: new Array<Portfolio>(),
  portfolio: new Portfolio()
});

export function reducer(state = initialState, action: PortfolioActions): State {
  switch (action.type) {
    case PortfolioActionTypes.ListarPortfoliosSuccess: {
      return { ...state, portfolios: action.portfolios };
    }
    default: {
      return state;
    }
  }
}

export const getPortfolioState = createFeatureSelector<State>('portfolio');

export const selectPortfolios = createSelector(
  getPortfolioState,
  (state: State) => {
    return state.portfolios;
  });

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getPortfolioState);
