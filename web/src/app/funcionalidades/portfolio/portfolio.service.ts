import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Portfolio } from './models/portfolio.model';
import { map, catchError } from 'rxjs/operators';
import { Service } from '@app/shared/services/service';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PortfolioService extends Service {

  private RESOURCE_PORTFOLIOS = 'portfolios'
  private RESOURCE_URL_PORTFOLIOS = `${environment.modulo_gestao_projeto}/${this.RESOURCE_PORTFOLIOS}`;

  constructor(private http: HttpClient) { super() }

  public listarPortfolios() {
    const url = this.RESOURCE_URL_PORTFOLIOS;
    return this.http.get<{ resultado: Portfolio[] }>(url, httpOptions).pipe(map(r => r.resultado));
  }

  editarPortfolio(portfolio: Portfolio): Observable<Portfolio> {
    return this.http.put<{ resultado: Portfolio }>(this.RESOURCE_URL_PORTFOLIOS, portfolio, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  deletarPortfolio(id: string): Observable<string> {
    return this.http.delete<any>(`${this.RESOURCE_URL_PORTFOLIOS}/${id}`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  public criarPortfolio(portfolio: Portfolio) {
    const url = this.RESOURCE_URL_PORTFOLIOS;
    return this.http.post(url, portfolio, httpOptions);
  }
}
