import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerPortfolioComponent } from './containers/container-portfolio/container-portfolio.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerPortfolioComponent ,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule { }
