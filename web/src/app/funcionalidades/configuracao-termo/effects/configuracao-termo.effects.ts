import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { TermoCompromissoActionTypes, LoadTermoCompromisso, LoadTermoCompromissoFail, LoadTermoCompromissoSuccess, UpdateTermoCompromisso, UpdateTermoCompromissoSucess, UpdateTermoCompromissoFail, OpenDialogo, LoadAllTermoCompromisso, LoadAllTermoCompromissoFail, LoadAllTermoCompromissoSuccess, BaixarTermoCompromisso, BaixarTermoCompromissoSuccess, BaixarTermoCompromissoFail } from '../actions/configuracao-termo.actions';
import { ConfiguracaoTermoService } from '../configuracao-termo.service';
import { TermoCompromisso } from '../models/termo-compromisso.model';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Color } from '@sicoob/ui';


@Injectable()
export class ConfiguracaoTermoEffects {

  constructor(private actions$: Actions, private termoCompromissoService: ConfiguracaoTermoService, private alertService: CustomAlertService,
  ) { }

  @Effect()
  load$ =
    this.actions$
      .ofType(TermoCompromissoActionTypes.LoadTermoCompromisso).pipe(
        map((action: LoadTermoCompromisso) => action),
        switchMap((action) => this.termoCompromissoService.findTermoCompromisso(action.tipo).pipe(
          map((termoCompromisso: TermoCompromisso) => new LoadTermoCompromissoSuccess(termoCompromisso)),
          catchError(error => of(new LoadTermoCompromissoFail(error))))));

  @Effect()
  baixarTermoCompromisso$ =
    this.actions$
      .ofType(TermoCompromissoActionTypes.BaixarTermoCompromisso).pipe(
        map((action: BaixarTermoCompromisso) => action),
        switchMap((action) => this.termoCompromissoService.baixarTermoCompromisso(action.tipo, action.id).pipe(
          map((termoCompromisso: string) => new BaixarTermoCompromissoSuccess(termoCompromisso)),
          catchError(error => of(new BaixarTermoCompromissoFail(error))))));

  @Effect()
  loadAll$ =
    this.actions$
      .ofType(TermoCompromissoActionTypes.LoadAllTermoCompromisso).pipe(
        map((action: LoadAllTermoCompromisso) => action),
        switchMap((action) => this.termoCompromissoService.findAllTermoCompromisso().pipe(
          map((termoCompromisso: TermoCompromisso[]) => new LoadAllTermoCompromissoSuccess(termoCompromisso)),
          catchError(error => of(new LoadAllTermoCompromissoFail(error))))));

  @Effect()
  loadAllError$ = this.actions$.ofType(TermoCompromissoActionTypes.LoadAllTermoCompromissoFail).pipe(
    map((action: LoadAllTermoCompromissoFail) => action.payload),
    switchMap((action) => {
      console.error(action.payload);

      this.alertService.abrirAlert(Color.DANGER, `Deu algum problema listando os termo de compromisso`);
      return of(new OpenDialogo());
    })
  );

  @Effect()
  update$ =
    this.actions$
      .ofType(TermoCompromissoActionTypes.UpdateTermoCompromisso).pipe(
        map((action: UpdateTermoCompromisso) => action.payload),
        switchMap((termoCompromisso: TermoCompromisso) => this.termoCompromissoService.updateTermoCompromisso(termoCompromisso).pipe(
          map((termoCompromisso: TermoCompromisso) => new UpdateTermoCompromissoSucess(termoCompromisso.id, { textoTermo: termoCompromisso.textoTermo })),
          catchError(error => of(new UpdateTermoCompromissoFail(error))))));



  /*  @Effect()
   updateSuccess$ = this.actions$.ofType(TermoCompromissoActionTypes.UpdateTermoCompromissoSucess).pipe(
     map((action: UpdateTermoCompromissoSucess) => action.changes),
     switchMap(() => {
       this.alertService.open({
         message: `Termo de compromisso atualizado com sucesso`,
         duration: 1000000,
         color: 'success'
       });
       return of(new OpenDialogo());
     })
   ); */



}
