import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ConfiguracaoTermoEffects } from './configuracao-termo.effects';

describe('ConfiguracaoTermoEffects', () => {
  let actions$: Observable<any>;
  let effects: ConfiguracaoTermoEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ConfiguracaoTermoEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(ConfiguracaoTermoEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
