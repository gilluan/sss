import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { TermoCompromissoActionTypes, TermoCompromissoAction, UpdateTermoCompromisso, } from '../actions/configuracao-termo.actions';
import { TermoCompromisso } from '../models/termo-compromisso.model';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { take } from 'rxjs/operators';

export interface State extends EntityState<TermoCompromisso> {

  headerButton: { title: string, color: string };
  listaCompromisso: TermoCompromisso[];
  termoCompromissoBaixar: string;
}

export const adapter: EntityAdapter<TermoCompromisso> = createEntityAdapter<TermoCompromisso>();

export const initialState: State = adapter.getInitialState({
  headerButton: {
    title: 'SALVAR ALTERAÇÕES',
    color: 'success'
  },
  listaCompromisso: [],
  termoCompromissoBaixar: ''
});

export function reducer(
  state = initialState,
  action: TermoCompromissoAction
): State {
  switch (action.type) {

    case TermoCompromissoActionTypes.LoadTermoCompromissoSuccess: {
      state.ids = [];
      state.entities = {};
      return adapter.addOne(action.payload, state);
    }

    /* case TermoCompromissoActionTypes.UpdateTermoCompromisso: {
      //state.headerButton.color = "success";
      //state.headerButton.title = "Termo de compromisso atualizado com sucesso";
      return { ...state };
    } */

    case TermoCompromissoActionTypes.UpdateTermoCompromissoSucess: {
      return adapter.updateOne({ id: action.id, changes: action.changes }, {
        ...state, headerButton: {
          title: 'Termo de compromisso atualizado com sucesso',
          color: 'success'
        }
      });

    }

    case TermoCompromissoActionTypes.UpdateTermoCompromissoFail: {
      return {
        ...state, headerButton: {
          title: 'Deu algum problema atualizando o termo de compromisso',
          color: 'danger'
        }
      };
    }

    case TermoCompromissoActionTypes.GenerateThumbnail: {
      return {
        ...state, headerButton: {
          title: 'Estamos validando as informações, por favor aguarde....',
          color: 'success'
        }
      };
    }

    case TermoCompromissoActionTypes.CleanButton: {
      return {
        ...state, headerButton: {
          title: 'SALVAR ALTERAÇÕES',
          color: 'success'
        }
      };
    }

    case TermoCompromissoActionTypes.LoadAllTermoCompromissoSuccess: {
      return {
        ...state, listaCompromisso: action.payload
      };
    }

    case TermoCompromissoActionTypes.BaixarTermoCompromissoSuccess: {
      return {
        ...state, termoCompromissoBaixar: action.termoCompromisso
      };
    }

    default: {
      return state;
    }
  }
}

export const getTermoCompromissoState = createFeatureSelector<State>('termoCompromisso');

export const getButtonState = createSelector(
  getTermoCompromissoState,
  selectAll => selectAll.headerButton
);

export const getTermoCompromissoBaixar = createSelector(
  getTermoCompromissoState,
  selectAll => selectAll.termoCompromissoBaixar
);

export const getListaTermos = createSelector(
  getTermoCompromissoState,
  selectAll => selectAll.listaCompromisso
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getTermoCompromissoState);
