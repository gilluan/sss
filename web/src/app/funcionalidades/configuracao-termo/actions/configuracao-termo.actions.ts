import { Action } from '@ngrx/store';
import { TermoCompromisso } from '../models/termo-compromisso.model';
import { Update } from '@ngrx/entity';


export enum TermoCompromissoActionTypes {
  LoadTermoCompromisso = '[LoadTermoCompromisso] Load LoadTermoCompromissos',
  LoadTermoCompromissoSuccess = '[LoadTermoCompromissoSuccess] Carregar TermoCompromisso Sucesso',
  LoadTermoCompromissoFail = '[LoadTermoCompromissoFail] Carregar TermoCompromisso Erro',
  LoadAllTermoCompromisso = '[LoadTermoComproLoadAllTermoCompromissomisso]',
  LoadAllTermoCompromissoSuccess = '[LoadAllTermoCompromissoSuccess]',
  LoadAllTermoCompromissoFail = '[LoadAllTermoCompromissoFail]',
  UpdateTermoCompromisso = '[UpdateTermoCompromisso] Load UpdateTermoCompromisso',
  UpdateTermoCompromissoSucess = '[UpdateTermoCompromissoSucess] Load UpdateTermoCompromissoSucess',
  UpdateTermoCompromissoFail = '[UpdateTermoCompromissoFail] Load UpdateTermoCompromissoFail',
  OpenDialogo = "[OpenDialogo]",
  CleanButton = "[CleanButton]",
  BaixarTermoCompromisso = '[BaixarTermoCompromisso]',
  BaixarTermoCompromissoSuccess = '[BaixarTermoCompromissoSuccess]',
  BaixarTermoCompromissoFail = '[BaixarTermoCompromissoFail]',
  GenerateThumbnail = '[GenerateThumbnail] Termo'
}

export class BaixarTermoCompromisso implements Action {
  readonly type = TermoCompromissoActionTypes.BaixarTermoCompromisso;
  constructor(public tipo: string, public id: string) { }
}
export class BaixarTermoCompromissoSuccess implements Action {
  readonly type = TermoCompromissoActionTypes.BaixarTermoCompromissoSuccess;
  constructor(public termoCompromisso: string) { }
}

export class BaixarTermoCompromissoFail implements Action {
  readonly type = TermoCompromissoActionTypes.BaixarTermoCompromissoFail;
  constructor(public erro: any) { }
}

export class LoadTermoCompromisso implements Action {
  readonly type = TermoCompromissoActionTypes.LoadTermoCompromisso;
  constructor(public tipo: string) { }

}

export class LoadTermoCompromissoSuccess implements Action {
  readonly type = TermoCompromissoActionTypes.LoadTermoCompromissoSuccess;
  constructor(public payload: TermoCompromisso) { }
}

export class LoadTermoCompromissoFail implements Action {
  readonly type = TermoCompromissoActionTypes.LoadTermoCompromissoFail;
  constructor(public payload: TermoCompromisso) { }
}

/* COMEÇO DO LOAD ALL */
export class LoadAllTermoCompromisso implements Action {
  readonly type = TermoCompromissoActionTypes.LoadAllTermoCompromisso;
}

export class LoadAllTermoCompromissoSuccess implements Action {
  readonly type = TermoCompromissoActionTypes.LoadAllTermoCompromissoSuccess;
  constructor(public payload: TermoCompromisso[]) { }
}

export class LoadAllTermoCompromissoFail implements Action {
  readonly type = TermoCompromissoActionTypes.LoadAllTermoCompromissoFail;
  constructor(public payload: any) { }
}

export class UpdateTermoCompromisso implements Action {
  readonly type = TermoCompromissoActionTypes.UpdateTermoCompromisso;
  constructor(public payload: TermoCompromisso) { }
}

export class UpdateTermoCompromissoSucess implements Action {
  readonly type = TermoCompromissoActionTypes.UpdateTermoCompromissoSucess;
  constructor(public id: string, public changes: Partial<TermoCompromisso>) { }
}

export class UpdateTermoCompromissoFail implements Action {
  readonly type = TermoCompromissoActionTypes.UpdateTermoCompromissoFail;
  constructor(public payload: TermoCompromisso) { }
}

export class OpenDialogo implements Action {
  readonly type = TermoCompromissoActionTypes.OpenDialogo;
}

export class GenerateThumbnail implements Action {
  readonly type = TermoCompromissoActionTypes.GenerateThumbnail;
}


export class CleanButton implements Action {
  readonly type = TermoCompromissoActionTypes.CleanButton;
}


export type TermoCompromissoAction =
    GenerateThumbnail
  | LoadTermoCompromisso
  | LoadTermoCompromissoFail
  | LoadTermoCompromissoSuccess
  | LoadAllTermoCompromisso
  | LoadAllTermoCompromissoSuccess
  | LoadAllTermoCompromissoFail
  | UpdateTermoCompromisso
  | UpdateTermoCompromissoSucess
  | UpdateTermoCompromissoFail
  | OpenDialogo
  | CleanButton
  | BaixarTermoCompromisso
  | BaixarTermoCompromissoSuccess
  | BaixarTermoCompromissoFail;
