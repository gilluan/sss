import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracaoTermoRoutingModule } from './configuracao-termo-routing.module';
import { StoreModule } from '@ngrx/store';
import * as fromConfiguracaoTermo from './reducers/configuracao-termo.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ConfiguracaoTermoEffects } from './effects/configuracao-termo.effects';
import { ContainerConfiguracaoTermoComponent } from './containers/container-configuracao-termo/container-configuracao-termo.component';
import { ConfiguracaoTermoComponent } from './components/configuracao-termo/configuracao-termo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillEditorModule } from 'ngx-quill-editor';
import { ButtonModule, SidebarContainerService } from '@sicoob/ui';
import { SidebarContainerModule } from '@sicoob/ui';
import { ListaTermosComponent } from './components/lista-termos/lista-termos.component';
import { ContainerListarTermosComponent } from './containers/container-listar-termos/container-listar-termos.component';
import { CardTermoComponent } from './components/card-termo/card-termo.component';

@NgModule({
  declarations: [ContainerConfiguracaoTermoComponent, ContainerListarTermosComponent,  ConfiguracaoTermoComponent, ListaTermosComponent, CardTermoComponent],
  imports: [
    CommonModule,
    ButtonModule,
    SidebarContainerModule,
    ConfiguracaoTermoRoutingModule,
    QuillEditorModule,  FormsModule, ReactiveFormsModule,
    StoreModule.forFeature('termoCompromisso', fromConfiguracaoTermo.reducer),
    EffectsModule.forFeature([ConfiguracaoTermoEffects])
  ],
  providers: [SidebarContainerService],
  entryComponents: []
})
export class ConfiguracaoTermoModule { }
