import {
  Component,
  ViewEncapsulation,
  Input,
} from '@angular/core';
import { TermoCompromisso } from '../../models/termo-compromisso.model';

@Component({
  selector: 'sc-card-termo',
  templateUrl: './card-termo.component.html',
  styleUrls: ['./card-termo.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CardTermoComponent {

  @Input() termo: TermoCompromisso;


  getSrc(thumbnail: any) {
    return thumbnail ? `data:image/png;base64,${thumbnail.buffer}` : '';
  }

}
