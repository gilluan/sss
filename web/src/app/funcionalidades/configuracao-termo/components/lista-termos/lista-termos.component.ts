import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DisplayToolbar } from 'src/app/app.actions';
import * as fromApp from '../../../../app.reduce';
import { LoadAllTermoCompromisso } from '../../actions/configuracao-termo.actions';
import { TermoCompromisso } from '../../models/termo-compromisso.model';
import * as fromConfiguracaoTermo from '../../reducers/configuracao-termo.reducer';



@Component({
  selector: 'sc-lista-termos',
  templateUrl: './lista-termos.component.html',
  styleUrls: ['./lista-termos.component.css'],
})
export class ListaTermosComponent {

  constructor(
    public appStore$: Store<fromApp.State>,
    private store: Store<fromConfiguracaoTermo.State>) {
  }

  listaTermos: Observable<TermoCompromisso[]>;

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.store.dispatch(new LoadAllTermoCompromisso());
    this.listaTermos = this.store.pipe(select(fromConfiguracaoTermo.getListaTermos))
  }


}
