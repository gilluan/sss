import { TemplatePortal } from '@angular/cdk/portal';
import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HeaderActionsContainerService } from '@sicoob/ui';
import domtoimage from 'dom-to-image';
import { QuillEditorComponent } from 'ngx-quill-editor/quillEditor.component';
import { TermoCompromisso } from '../../models/termo-compromisso.model';
import * as fromConfiguracaoTermo from '../../reducers/configuracao-termo.reducer';
import { GenerateThumbnail } from '../../actions/configuracao-termo.actions';
import { Store } from '@ngrx/store';



@Component({
  selector: 'sc-configuracao-termo',
  templateUrl: './configuracao-termo.component.html',
  styleUrls: ['./configuracao-termo.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfiguracaoTermoComponent implements AfterViewInit, OnDestroy {

  @Output() atualizarTextoTermo = new EventEmitter<TermoCompromisso>();

  @Input() id: string;
  @Input() editorContent: string;
  @Input() tipo: string;
  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;
  @ViewChild('quillEditor') container: QuillEditorComponent;


  @Input() buttonText: string;
  @Input() buttonClass: string;

  constructor(
    private headerActionsService: HeaderActionsContainerService, private store: Store<fromConfiguracaoTermo.State>,) {
  }

  ngOnDestroy() {
    this.headerActionsService.remove();
  }

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  async updateTexto() {
    let thumbnail: string = null;
    try {
      this.store.dispatch(new GenerateThumbnail());
      thumbnail = await domtoimage.toBlob(this.container.quillEditor.container); //await domtoimage.toPng(this.container.quillEditor.container);
    } catch (error) {
      console.error('oops, something went wrong!', error);
    }
    let te = new TermoCompromisso();
    te.id = this.id;
    te.textoTermo = this.editorContent;
    te.tipo = this.tipo;
    te.thumbnail =  thumbnail;
    this.atualizarTextoTermo.emit(te);
  }


  public editor;
  public editorConfig = {
    placeholder: "inserir um texto...",
    modules: {
      toolbar: '#toolbar'
    }
  };

  onEditorFocused(quill) {
    //console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
  }

  onContentChanged({ quill, html, text }) {
    //console.log('quill content is changed!', quill, html, text);
  }

}
