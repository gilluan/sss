import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracaoTermoComponent } from './configuracao-termo.component';

describe('ConfiguracaoTermoComponent', () => {
  let component: ConfiguracaoTermoComponent;
  let fixture: ComponentFixture<ConfiguracaoTermoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracaoTermoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracaoTermoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
