import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TermoCompromisso } from './models/termo-compromisso.model';
import { Service } from 'src/app/shared/services/service';
import { Validations } from '@app/shared/utils/validations';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const notNull = Validations.notNull;

@Injectable({
  providedIn: 'root'
})
export class ConfiguracaoTermoService extends Service {

  private RESOURCE_TERMO = 'termos-compromisso';
  private RESOURCE_URL_TERMO = `${environment.modulo_gestao_pessoa}/${this.RESOURCE_TERMO}`;

  constructor(private http: HttpClient) { super() }

  updateTermoCompromisso(termoCompromisso: TermoCompromisso): Observable<TermoCompromisso> {
    const formData = new FormData();
    notNull(termoCompromisso.id, n => formData.append('id', n));
    notNull(termoCompromisso.dataCriacao, n => formData.append('dataCriacao', n.toDateString()));
    notNull(termoCompromisso.dataModificacao, n => formData.append('dataModificacao', n.toDateString()));
    notNull(termoCompromisso.textoTermo, n => formData.append('textoTermo', n));
    notNull(termoCompromisso.tipo, n => formData.append('tipo', n));
    notNull(termoCompromisso.thumbnail, n => formData.append('thumbnail', n, `${termoCompromisso.tipo}.png`));
    return this.http.put<TermoCompromisso>(this.RESOURCE_URL_TERMO, formData).pipe(
      catchError(this.handleError)
    );
  }

  findTermoCompromisso(tipo: string = "voluntario"): Observable<TermoCompromisso> {
    return this.http.get<TermoCompromisso>(`${this.RESOURCE_URL_TERMO}?tipo=${tipo}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  baixarTermoCompromisso(tipo: string, id: string) {
    let url = `${this.RESOURCE_URL_TERMO}/baixar-termo?tipo=${tipo}&id=${id}`;
    return this.http.get<{ resultado: string }>(url, httpOptions)
      .pipe(
        map(r => r.resultado),
        catchError(this.handleError)
      );
  }

  findAllTermoCompromisso(tipo?: string): Observable<TermoCompromisso[]> {
    let url: string = this.RESOURCE_URL_TERMO;
    if (tipo)
      url = `${this.RESOURCE_URL_TERMO}?tipo=${tipo}`;
    return this.http.get<TermoCompromisso[]>(url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
