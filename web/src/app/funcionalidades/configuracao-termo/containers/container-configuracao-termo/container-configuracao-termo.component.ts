import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { LoadTermoCompromisso, UpdateTermoCompromisso, CleanButton } from '../../actions/configuracao-termo.actions';
import { Observable } from 'rxjs';
import { TermoCompromisso } from '../../models/termo-compromisso.model';
import * as fromConfiguracaoTermo from '../../reducers/configuracao-termo.reducer';
import * as fromApp from '../../../../app.reduce';
import { DisplayToolbar } from 'src/app/app.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sc-container-configuracao-termo',
  templateUrl: './container-configuracao-termo.component.html',
  styleUrls: ['./container-configuracao-termo.component.css']
})
export class ContainerConfiguracaoTermoComponent implements OnInit, OnDestroy {

  termoCompromisso$: Observable<TermoCompromisso[]>;


  editorContent: string;
  id: string;
  tipo: string;
  buttonText: string;
  buttonClass: string;

  constructor(
    private store: Store<fromConfiguracaoTermo.State>,
    private changeDetector: ChangeDetectorRef,
    public appStore$: Store<fromApp.State>,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.carregarTextoTermo();
    this.handleButton();
  }

  atualizarTextoTermo(te: TermoCompromisso) {
    this.store.dispatch(new UpdateTermoCompromisso(te));
    setTimeout(() => {
      this.store.dispatch(new CleanButton());
      this.handleButton();
    }, 6000);
  }


  ngOnDestroy() {
    this.store.dispatch(new CleanButton());
  }

  handleButton() {
    this.store.pipe(select(fromConfiguracaoTermo.getButtonState)).subscribe(
      (val) => {
        this.buttonClass = val.color;
        this.buttonText = val.title;
        this.changeDetector.detectChanges();
      }
    );
  }

  carregarTextoTermo() {
    this.termoCompromisso$ = this.store.pipe(select(fromConfiguracaoTermo.selectAll));
    this.activatedRoute.params.subscribe(params => {
      this.store.dispatch(new LoadTermoCompromisso(params['tipo']));
    });
    this.termoCompromisso$.subscribe(data => {
      if (data !== undefined && data != null && data.length > 0) {
        this.editorContent = data[0].textoTermo;
        this.id = data[0].id;
        this.tipo = data[0].tipo;
        this.changeDetector.detectChanges();
      }
    });
  }

}
