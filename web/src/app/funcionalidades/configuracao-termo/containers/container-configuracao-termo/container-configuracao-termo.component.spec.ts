import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerConfiguracaoTermoComponent } from './container-configuracao-termo.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerConfiguracaoTermoComponent', () => {
  let component: ContainerConfiguracaoTermoComponent;
  let fixture: ComponentFixture<ContainerConfiguracaoTermoComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerConfiguracaoTermoComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerConfiguracaoTermoComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
