import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaTermosComponent } from './components/lista-termos/lista-termos.component';
import { ContainerConfiguracaoTermoComponent } from './containers/container-configuracao-termo/container-configuracao-termo.component';
import { ContainerListarTermosComponent } from './containers/container-listar-termos/container-listar-termos.component';

const routes: Routes = [
  {
    path: '',
    component: ListaTermosComponent
  },
  {
    path: '',
    component: ContainerListarTermosComponent,
    children: [
      {
        path: ':tipo',
        component: ContainerConfiguracaoTermoComponent,
        data: {
          breadcrumb: "Edição"
        }
      }
    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracaoTermoRoutingModule { }
