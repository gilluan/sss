import { BaseDto } from "@app/shared/models/base-dto.model";

export class TermoCompromisso extends BaseDto{
  textoTermo: string;
  tipo: string;
  thumbnail: any;
}
