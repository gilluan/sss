import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'sc-container-formio',
  templateUrl: './container-formio.component.html',
  styleUrls: ['./container-formio.component.css']
})
export class ContainerFormioComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

}
