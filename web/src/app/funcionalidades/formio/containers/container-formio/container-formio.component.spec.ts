import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerFormioComponent } from './container-formio.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerFormioComponent', () => {
  let component: ContainerFormioComponent;
  let fixture: ComponentFixture<ContainerFormioComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerFormioComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerFormioComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
