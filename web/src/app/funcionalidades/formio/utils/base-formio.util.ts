import {EditFormTab} from '@app/funcionalidades/formio/models/abas/edit-form-tab.model';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

export class BaseFormioUtil {

  private static DEFAULT_IGNORED_TABS: string[] = [ new ConditionalTab(), new LogicTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());

  static generateTabsIgnore(...tabs: string[]) {
    const tabsToIgnore: string[] = [ ...this.DEFAULT_IGNORED_TABS, ...tabs ];
    return this.generateIgnore(...tabsToIgnore);
  }

  static generateOptionsIgnore(tabsWithOptions: Map<string, string[]>) {
    return Array.from(tabsWithOptions, ([key, options]) => ({key: key, components: this.generateIgnore(...options)}));
  }

  private static generateIgnore(...keys: string[]) {
    return keys.map(key => ({key: key, ignore: true}));
  }
}
