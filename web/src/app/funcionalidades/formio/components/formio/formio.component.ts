import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {formBuilderConfig} from '../../form-builder.config';
import {Validations} from '@shared/utils/validations';
import {Formio} from '@fbelluco/sins-formio';

const isNull = Validations.isNull;

@Component({
  selector: 'sc-formio',
  templateUrl: './formio.component.html',
  styleUrls: ['./formio.component.scss']
})
export class FormioComponent implements OnInit {
  @Input() scFormulario: object;

  @Output() scFormularioAlterado: EventEmitter<any> = new EventEmitter();
  @Output() scComponenteAlterado: EventEmitter<any> = new EventEmitter();
  @Output() scComponenteAdicionado: EventEmitter<any> = new EventEmitter();

  @ViewChild('builder') builder?: ElementRef;

  constructor() { }

  ngOnInit(): void {
    isNull(this.scFormulario, () => this.scFormulario = { components: [] });
    this.inicializarBuilder(this.builder.nativeElement, this.scFormulario, formBuilderConfig);
  }

  /**
   * Inicializa e registra os eventos do Builder.
   *
   * @param elemento elemento HTML onde o builder será renderizado
   * @param formulario objeto que representa o formulário
   * @param builderConfig configuração do builder
   */
  private inicializarBuilder(elemento: any, formulario: object, builderConfig: any): void {
    Formio.builder(elemento, formulario, builderConfig).then(builder => {
      builder.on('change', (formOrChangedData) => this.formularioAlterado(formOrChangedData));
      builder.on('addComponent', (component) => this.componenteAdicionado(component));
      builder.on('editComponent', (component) => this.componenteAlterado(component));
    });
  }

  /**
   * Dispara evento quando o formulário é alterado. Ele é disparado suas vezes: uma retornando um objeto que representa
   * os componentes do formulário, e otra retornando um objeto que encapsula os dados alterados.
   *
   * @param formularioOuAlteracoes formulário ou alterações
   */
  private formularioAlterado = (formularioOuAlteracoes: any): void => this.scFormularioAlterado.emit(formularioOuAlteracoes);

  /**
   * Dispara evento quando um componente é adicionado ao builder.
   *
   * @param componente componente adicionado
   */
  private componenteAdicionado = (componente: any): void => this.scComponenteAdicionado.emit(componente);

  /**
   * Dispara evento quando um componente é alterado.
   *
   * @param componente componente alterado
   */
  private componenteAlterado = (componente: any): void => this.scComponenteAlterado.emit(componente);
}
