import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Validations} from '@shared/utils/validations';
import {Formio} from '@fbelluco/sins-formio';
import {formBuilderConfig} from '../../form-builder.config';

const isNull = Validations.isNull;

@Component({
  selector: 'sc-formio-render',
  templateUrl: './formio-render.component.html',
  styleUrls: ['./formio-render.component.scss']
})
export class FormioRenderComponent implements OnInit {
  @Input() scFormulario: object;

  @Output() scFormularioCarregado: EventEmitter<any> = new EventEmitter<any>();
  @Output() scErroAoCarregar: EventEmitter<any> = new EventEmitter<any>();
  @Output() scFormularioEnviado: EventEmitter<any> = new EventEmitter<any>();
  @Output() scFormularioEnvioFinalizado: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('render') render?: ElementRef;

  constructor() { }

  ngOnInit(): void {
    isNull(this.scFormulario, () => this.scFormulario = {components: []});
    this.inicializarRender(this.render.nativeElement, this.scFormulario);
  }

  private inicializarRender(elemento: any, formulario: object): void {
    Formio.createForm(elemento, formulario, { language: formBuilderConfig.language, i18n: formBuilderConfig.i18n }).then(render => {
      render.on('render', (form) => this.formularioCarregado(form));
      render.on('error', (errors) => this.erroAoEnviar(errors));
      render.on('submit', (submission) => this.formularioEnviado(submission));
      render.on('submitDone', (submission) => this.formularioEnvioFinalizado(submission));
    });
  }

  private formularioCarregado = (form: any): void => this.scFormularioCarregado.emit(form);

  private erroAoEnviar = (errors: any): void => this.scErroAoCarregar.emit(errors);

  private formularioEnviado = (submission: any): void => this.scFormularioEnviado.emit(submission);

  private formularioEnvioFinalizado = (submission: any): void => this.scFormularioEnvioFinalizado.emit(submission);

}
