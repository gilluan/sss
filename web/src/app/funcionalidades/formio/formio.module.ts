import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContainerFormioComponent} from './containers/container-formio/container-formio.component';
import {FormioComponent} from './components/formio/formio.component';

import './models/componentes/campo-texto.component';
import './models/componentes/area-texto.component';
import './models/componentes/lista-suspensa.component';
// import './models/componentes/data.component';
import './models/componentes/horario.component';
import './models/componentes/caixa-selecao.component';
import './models/componentes/grade-selecao.component';
import './models/componentes/multipla-escolha.component';
import './models/componentes/escala-linear.component';
import './models/componentes/upload-arquivo.component';
import './models/componentes/indicador.component';
import './models/componentes/dia.component';
import { FormioRenderComponent } from './components/formio-render/formio-render.component';

const Formio = require('@fbelluco/sins-formio').Formio;
Formio.icons = 'fontawesome';

@NgModule({
  declarations: [ContainerFormioComponent, FormioComponent, FormioRenderComponent],
  exports: [
    FormioComponent,
    FormioRenderComponent
  ],
  imports: [CommonModule]
})
export class FormioModule { }
