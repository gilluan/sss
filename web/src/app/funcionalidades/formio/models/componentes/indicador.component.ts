import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';

const NumberComponent = require('@fbelluco/sins-formio/components/number/Number').default;
const Components = require('@fbelluco/sins-formio/components/Components').default;

const displayTab = new DisplayTab();
const { label, placeholder, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, placeholder, tooltip });

const dataTab = new DataTab();
const { thousandsSeparator, decimalLimit, requireDecimal } = dataTab.getOptions();
const dataOpts = Object.keys({ thousandsSeparator, decimalLimit, requireDecimal });

export class IndicadorComponent extends NumberComponent implements ComponenteFormio {

  static schema() {
    return NumberComponent.schema({
      type: 'indicador',
      label: 'Indicador',
      placeholder: 'Informe o valor do indicador...'
    });
  }

  static get builderInfo() {
    return {
      title: 'Indicador',
      group: 'sinsbasic',
      icon: 'fa fa-star',
      weight: 50,
      documentation: 'http://help.form.io/userguide/#number',
      schema: IndicadorComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), { label, placeholder, tooltip } ],
    [ dataTab.getName(), { thousandsSeparator, decimalLimit, requireDecimal } ],
  ])
}

IndicadorComponent.editForm = function (...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(dataTab.getName(), dataTab.getOptionsToIgnore(...dataOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new ConditionalTab(), new LogicTab(), new LayoutTab(), new ValidationTab(), new ApiTab() ].map(tab => tab.getName());
  const tabsIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);
  return NumberComponent.editForm([ ...tabsIgnore, ...optionsIgnore ], ...extend);
};

Components.addComponent('indicador', IndicadorComponent);
