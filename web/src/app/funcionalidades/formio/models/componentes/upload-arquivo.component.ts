import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {FileTab} from '@app/funcionalidades/formio/models/abas/file-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const FileComponent = require('@fbelluco/sins-formio/components/file/File').default;

const displayTab = new DisplayTab();
const { label, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, tooltip });

const fileTab = new FileTab();
const { webcam, filePattern, fileMaxSize, fileMinSize } = fileTab.getOptions();
const fileOpts = Object.keys({ webcam, filePattern, fileMaxSize, fileMinSize });

const validationTab = new ValidationTab();
const { required } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

export class UploadArquivoComponent extends FileComponent implements ComponenteFormio {

  static schema() {
    return FileComponent.schema({
      type: 'uploadarquivo',
      label: 'Upload de arquivo',
      storage: 'base64'
    });
  }

  static get builderInfo() {
    return  {
      title: 'Upload de arquivo',
      group: 'sinsbasic',
      icon: 'fa fa-file',
      documentation: 'http://help.form.io/userguide/#file',
      weight: 600,
      schema: UploadArquivoComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), { label, tooltip } ],
    [ fileTab.getName(), { webcam, filePattern, fileMaxSize, fileMinSize } ],
    [ validationTab.getName(), { required } ],
  ])
}

UploadArquivoComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(fileTab.getName(), fileTab.getOptionsToIgnore(...fileOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsToIgnoreMap = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new ConditionalTab(), new LogicTab(), new DataTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsToIgnoreList = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return FileComponent.editForm([ ...tabsToIgnoreList, ...optionsToIgnoreMap ], ...extend);
};

Components.addComponent('uploadarquivo', UploadArquivoComponent);
