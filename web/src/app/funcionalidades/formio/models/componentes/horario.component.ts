import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {TimeTab} from '@app/funcionalidades/formio/models/abas/time-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const TimeComponent = require('@fbelluco/sins-formio/components/time/Time').default;

const displayTab = new DisplayTab();
const { label, placeholder, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, placeholder, tooltip });

const validationTab = new ValidationTab();
const { required } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

export class HorarioComponent extends TimeComponent implements ComponenteFormio {

  static schema() {
    return TimeComponent.schema({
      type: 'horario',
      label: 'Horário',
      placeholder: 'Ex.: 12:34',
      format: 'HH:mm'
    });
  }

  static get builderInfo() {
    return {
      title: 'Horário',
      group: 'sinsbasic',
      icon: 'fa fa-clock-o',
      documentation: 'http://help.form.io/userguide/#time',
      weight: 350,
      schema: HorarioComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), { label, placeholder, tooltip } ],
    [ validationTab.getName(), { required } ]
  ])
}

HorarioComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] = [  new TimeTab(), new DataTab() ].map(tab => tab.getName());
  const tabsIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return TimeComponent.editForm([...tabsIgnore, ...optionsIgnore ], ...extend);
};

Components.addComponent('horario', HorarioComponent);
