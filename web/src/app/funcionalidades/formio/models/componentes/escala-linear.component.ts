import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const SurveyComponent = require('@fbelluco/sins-formio/components/survey/Survey').default;

const displayTab = new DisplayTab();
const { label, questions, values, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, questions, values, tooltip });

const validationTab = new ValidationTab();
const { required  } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

export class EscalaLinearComponent extends SurveyComponent implements ComponenteFormio {

  static schema() {
    return SurveyComponent.schema({
      type: 'escalalinear',
      label: 'Escala linear',
    });
  }

  static get builderInfo() {
    return {
      title: 'Escala linear',
      group: 'sinsbasic',
      icon: 'fa fa-list',
      weight: 550,
      documentation: 'http://help.form.io/userguide/#survey',
      schema: EscalaLinearComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), { label, questions, values, tooltip } ],
    [ validationTab.getName(), { required } ]
  ])
}

EscalaLinearComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsToIgnoreMap = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new DataTab(), new ConditionalTab(), new LogicTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsToIgnoreList = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return SurveyComponent.editForm([ ...tabsToIgnoreList, ...optionsToIgnoreMap ], ...extend);
};

Components.addComponent('escalalinear', EscalaLinearComponent);
