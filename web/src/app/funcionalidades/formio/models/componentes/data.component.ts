import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {DateTab} from '@app/funcionalidades/formio/models/abas/date-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {TimeTab} from '@app/funcionalidades/formio/models/abas/time-tab.model';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const DateTimeComponent = require('@fbelluco/sins-formio/components/datetime/DateTime').default;

const displayTab = new DisplayTab();
const { label, placeholder, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, placeholder, tooltip });

const dateTab = new DateTab();
const { maxDate, minDate } = dateTab.getOptions();
const dateOpts = Object.keys({ maxDate, minDate });

const validationTab = new ValidationTab();
const { required } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

export class DataComponent extends DateTimeComponent implements ComponenteFormio {

  static schema() {
    return DateTimeComponent.schema({
      type: 'data',
      label: 'Calendário',
      placeholder: 'Ex.: 02/01/1989',
      format: 'dd/MM/yyyy',
      enableTime: false,
      language: 'pt-BR',
      appendTo: 'sc-formio'
    });
  }

  static get builderInfo() {
    return {
      title: 'Calendário',
      group: 'sinsbasic',
      icon: 'fa fa-calendar-plus-o',
      documentation: 'http://help.form.io/userguide/#datetime',
      weight: 300,
      schema: DataComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map([
    [ displayTab.getName(), {label, placeholder, tooltip} ],
    [ dateTab.getName(), { maxDate, minDate } ],
    [ validationTab.getName(), { required } ]
  ])
}

DataComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(dateTab.getName(), dateTab.getOptionsToIgnore(...dateOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new TimeTab(), new ConditionalTab(), new LogicTab(), new DataTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return DateTimeComponent.editForm([...tabsIgnore, ...optionsIgnore ], ...extend);
};

Components.addComponent('data', DataComponent);
