import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const SelectBoxesComponent = require('@fbelluco/sins-formio/components/selectboxes/SelectBoxes').default;

const displayTab = new DisplayTab();
const { label, values } = displayTab.getOptions();
const displayOpts = Object.keys({ label, values });

const validationTab = new ValidationTab();
const { required, validateMinSelectedCount, validateMaxSelectedCount } = validationTab.getOptions();
const validationOpts = Object.keys({ required, validateMinSelectedCount, validateMaxSelectedCount });

export class GradeSelecaoComponent extends SelectBoxesComponent implements ComponenteFormio {

  static schema() {
    return SelectBoxesComponent.schema({
      type: 'gradeselecao',
      label: 'Grade de Seleção'
    });
  }

  static get builderInfo() {
    return {
      title: 'Grade de seleção',
      group: 'sinsbasic',
      icon: 'fa fa-plus-square',
      weight: 450,
      documentation: 'http://help.form.io/userguide/#selectboxes',
      schema: GradeSelecaoComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), { label, values } ],
    [ validationTab.getName(), { required, validateMinSelectedCount, validateMaxSelectedCount } ]
  ])
}

GradeSelecaoComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsToIgnoreMap = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new DataTab(), new ConditionalTab(), new LogicTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsToIgnoreList = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return SelectBoxesComponent.editForm([ ...tabsToIgnoreList, ...optionsToIgnoreMap ], ...extend);
};

Components.addComponent('gradeselecao', GradeSelecaoComponent);
