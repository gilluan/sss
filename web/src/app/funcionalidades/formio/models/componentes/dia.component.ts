import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const DayComponent = require('@fbelluco/sins-formio/components/day/Day').default;

const displayTab = new DisplayTab();
const { label, tooltip, dayPlaceholder , monthPlaceholder, yearPlaceholder } = displayTab.getOptions();
const displayOpts = Object.keys({ label, tooltip, dayPlaceholder , monthPlaceholder, yearPlaceholder });

const validationTab = new ValidationTab();
const { required, requireDay, requireMonth, requireYear, minDate, maxDate } = validationTab.getOptions();
const validationOpts = Object.keys({ required, requireDay, requireMonth, requireYear, minDate, maxDate });

export class DiaComponent extends DayComponent implements ComponenteFormio {

  static schema() {
    return DayComponent.schema({
      type: 'dia',
      label: 'Dia',
      dayFirst: true
    });
  }

  static get builderInfo() {
    return {
      title: 'Dia',
      group: 'sinsbasic',
      icon: 'fa fa-calendar',
      documentation: 'http://help.form.io/userguide/#day',
      weigth: 250,
      schema: DiaComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [displayTab.getName(), { label, tooltip, dayPlaceholder, monthPlaceholder, yearPlaceholder }],
    [validationTab.getName(), { required, requireDay, requireMonth, requireYear, minDate, maxDate }]
  ])
}

DiaComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsIgnore = BaseFormioUtil.generateTabsIgnore();

  return DayComponent.editForm([ ...tabsIgnore, ...optionsIgnore ], ...extend);
};

Components.addComponent('dia', DiaComponent);
