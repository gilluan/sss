import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const CheckboxComponent = require('@fbelluco/sins-formio/components/checkbox/Checkbox').default;

const displayTab = new DisplayTab();
const { label, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, tooltip });

const validationTab = new ValidationTab();
const { required } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

export class CaixaSelecaoComponent extends CheckboxComponent implements ComponenteFormio {

  static schema() {
    return CheckboxComponent.schema({
      type: 'caixaselecao',
      label: 'Caixa de seleção'
    });
  }

  static get builderInfo() {
    return {
      title: 'Caixa de seleção',
      group: 'sinsbasic',
      icon: 'fa fa-check-square',
      documentation: 'http://help.form.io/userguide/#checkbox',
      weight: 400,
      schema: CaixaSelecaoComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
    [ displayTab.getName(), {label, tooltip} ],
    [ validationTab.getName(), {required} ]
  ])
}

CaixaSelecaoComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsToIgnoreMap = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new ConditionalTab(), new LogicTab(), new DataTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsToIgnoreList = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return CheckboxComponent.editForm([ ...tabsToIgnoreList, ...optionsToIgnoreMap ], ...extend);
};

Components.addComponent('caixaselecao', CaixaSelecaoComponent);
