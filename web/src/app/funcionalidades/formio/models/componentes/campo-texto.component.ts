import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {LayoutTab} from "@app/funcionalidades/formio/models/abas/layout-tab.model";

const BaseComponent = require('@fbelluco/sins-formio/components/base/Base').default;
const Components = require('@fbelluco/sins-formio/components/Components').default;
const TextFieldComponent = require('@fbelluco/sins-formio/components/textfield/TextField').default;

const displayTab = new DisplayTab();
const { showCharCount, suffix, prefix, placeholder, label, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ showCharCount, suffix, prefix, placeholder, label, tooltip });

const validationTab = new ValidationTab();
const { maxLength, minLength, required } = validationTab.getOptions();
const validationOpts = Object.keys({ maxLength, minLength, required });

export class CampoTextoComponent extends BaseComponent implements ComponenteFormio {
  static schema() {
    return BaseComponent.schema({
      type: 'campotexto',
      label: 'Campo de texto',
      placeholder: 'Insira o texto aqui...',
    });
  }

  static get builderInfo() {
    return {
      title: 'Campo de texto',
      group: 'sinsbasic',
      icon: 'fa fa-terminal',
      weight: 100,
      documentation: 'http://help.form.io/userguide/#textfield',
      schema: CampoTextoComponent.schema()
    };
  }

  getOptions(): Map<string, object> {
    const options = new Map<string, object>();
    options.set(displayTab.getName(), { showCharCount, suffix, prefix, placeholder, label, tooltip });
    options.set(validationTab.getName(), { maxLength, minLength, required });
    return options;
  }
}

CampoTextoComponent.editForm = function (...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new ConditionalTab(), new LogicTab(), new DataTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return TextFieldComponent.editForm([...tabsIgnore, ...optionsIgnore], ...extend);
};

Components.addComponent('campotexto', CampoTextoComponent);
