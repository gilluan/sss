import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const TextAreaComponent = require('@fbelluco/sins-formio/components/textarea/TextArea').default;

const displayTab = new DisplayTab();
const { label, placeholder, tooltip, prefix, suffix, showCharCount } = displayTab.getOptions();
const displayOpts = Object.keys({ label, placeholder, tooltip, prefix, suffix, showCharCount });

const validationTab = new ValidationTab();
const { required, maxLength, minLength } = validationTab.getOptions();
const validationOpts = Object.keys({ required, maxLength, minLength });

export class AreaTextoComponent extends TextAreaComponent implements ComponenteFormio {

  static schema() {
    return TextAreaComponent.schema({
      type: 'areatexto',
      label: 'Campo de texto longo',
      placeholder: 'Insira seu texto aqui...'
    });
  }

  static get builderInfo() {
    return {
      title: 'Campo de texto longo',
      group: 'sinsbasic',
      icon: 'fa fa-font',
      weight: 200,
      documentation: 'http://help.form.io/userguide/#textarea',
      schema: AreaTextoComponent.schema()
    };
  }

  getOptions(): Map<string, object> {
    const options = new Map<string, object>();
    options.set(displayTab.getName(), { label, placeholder, tooltip, prefix, suffix, showCharCount });
    options.set(validationTab.getName(), { required, maxLength, minLength });
    return options;
  }
}

AreaTextoComponent.editForm = function(...extend) {
  const optionsToIgnore = new Map<string, string[]>();
  optionsToIgnore.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optionsToIgnore.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));

  const optionsIgnore = BaseFormioUtil.generateOptionsIgnore(optionsToIgnore);

  const tabsToIgnore: string[] =
    [ new ConditionalTab(), new LogicTab(), new DataTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnore);

  return TextAreaComponent.editForm([...tabsIgnore, ...optionsIgnore ], ...extend);
};

Components.addComponent('areatexto', AreaTextoComponent);
