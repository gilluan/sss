import {DisplayTab} from '@app/funcionalidades/formio/models/abas/display-tab.model';
import {ValidationTab} from '@app/funcionalidades/formio/models/abas/validation-tab.model';
import {DataTab} from '@app/funcionalidades/formio/models/abas/data-tab.model';
import {ApiTab} from '@app/funcionalidades/formio/models/abas/api-tab.model';
import {ComponenteFormio} from '@app/funcionalidades/formio/models/componentes/componente-formio.component';
import {BaseFormioUtil} from '@app/funcionalidades/formio/utils/base-formio.util';
import {ConditionalTab} from '@app/funcionalidades/formio/models/abas/conditional-tab.model';
import {LogicTab} from '@app/funcionalidades/formio/models/abas/logic-tab.model';
import {LayoutTab} from '@app/funcionalidades/formio/models/abas/layout-tab.model';

const Components = require('@fbelluco/sins-formio/components/Components').default;
const SelectComponent = require('@fbelluco/sins-formio/components/select/Select').default;

const displayTab = new DisplayTab();
const { label, placeholder, tooltip } = displayTab.getOptions();
const displayOpts = Object.keys({ label, placeholder, tooltip });

const validationTab = new ValidationTab();
const { required } = validationTab.getOptions();
const validationOpts = Object.keys({ required });

const dataTab = new DataTab();
const { dataValues } = dataTab.getOptions();
const dataOpts = Object.keys({ dataValues });

export class ListaSuspensaComponent extends SelectComponent implements ComponenteFormio {

  static schema() {
    return SelectComponent.schema({
      type: 'listasuspensa',
      label: 'Lista suspensa',
      placeholder: 'Selecione um item...',
      dataSrc: 'values'
    });
  }

  static get builderInfo() {
    return {
      title: 'Lista suspensa',
      group: 'sinsbasic',
      icon: 'fa fa-th-list',
      weight: 250,
      documentation: 'http://help.form.io/userguide/#select',
      schema: ListaSuspensaComponent.schema()
    };
  }

  getOptions = (): Map<string, object> => new Map<string, object>([
      [ displayTab.getName(), { label, placeholder, tooltip } ],
      [ validationTab.getName(), { required } ],
      [ dataTab.getName(), { dataValues } ]
    ])
}

ListaSuspensaComponent.editForm = function(...extend) {
  const optsToIgnoreMap = new Map<string, string[]>();
  optsToIgnoreMap.set(displayTab.getName(), displayTab.getOptionsToIgnore(...displayOpts));
  optsToIgnoreMap.set(validationTab.getName(), validationTab.getOptionsToIgnore(...validationOpts));
  optsToIgnoreMap.set(dataTab.getName(), dataTab.getOptionsToIgnore(...dataOpts));

  const optsToIgnore = BaseFormioUtil.generateOptionsIgnore(optsToIgnoreMap);

  const tabsToIgnoreNames: string[] = [ new ConditionalTab(), new LogicTab(), new ApiTab(), new LayoutTab() ].map(tab => tab.getName());
  const tabsToIgnore = BaseFormioUtil.generateTabsIgnore(...tabsToIgnoreNames);

  return SelectComponent.editForm([...tabsToIgnore, ...optsToIgnore ], ...extend);
};

Components.addComponent('listasuspensa', ListaSuspensaComponent);
