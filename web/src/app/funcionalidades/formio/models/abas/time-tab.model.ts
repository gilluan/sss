import {EditFormTab} from './edit-form-tab.model';

export class TimeTab extends EditFormTab {
  getName = (): string => 'time';

  getOptions() {
    return {
      enableTime: 'enableTime',
      hourStep: 'timePicker.hourStep',
      minuteStep: 'timePicker.minuteStep',
      showMeridian: 'timePicker.showMeridian'
    };
  }
}
