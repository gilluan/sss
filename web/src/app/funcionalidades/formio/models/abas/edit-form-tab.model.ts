export abstract class EditFormTab {
  abstract getName(): string;
  abstract getOptions(): Object;

  getOptionsName(): string[] {
    return Object.keys(this.getOptions());
  }

  getOptionsValue(): string[] {
    return Object.values(this.getOptions());
  }

  getOptionsToIgnore(...except: string[]): string[] {
    return this.getOptionsName()
      .filter(option => except.indexOf(option) === -1)
      .map(key => this.getOptions()[key]);
  }
}
