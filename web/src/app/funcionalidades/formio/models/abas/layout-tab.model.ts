import {EditFormTab} from './edit-form-tab.model';

export class LayoutTab extends EditFormTab {
  getName = (): string => 'layout';

  getOptions = (): Object => ({
    attributes: 'attributes'
  })
}
