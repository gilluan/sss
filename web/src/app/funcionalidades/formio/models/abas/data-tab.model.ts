import {EditFormTab} from './edit-form-tab.model';

export class DataTab extends EditFormTab {
  getName = (): string => 'data';

  getOptions() {
    return {
      inputFormat: 'inputFormat',
      refreshOn: 'refreshOn',
      clearOnRefresh: 'clearOnRefresh',
      customDefaultValuePanel: 'customDefaultValuePanel',
      calculateValuePanel: 'calculateValuePanel',
      allowCalculateOverride: 'allowCalculateOverride',
      encrypted: 'encrypted',
      databaseIndex: 'dbIndex',
      defaultValue: 'defaultValue',
      customDefaultValueJs: 'customDefaultValue-js',
      customDefaultValue: 'customDefaultValue',
      customDefaultValueJson: 'customDefaultValue-json',
      calculateValueJs: 'calculateValue-js',
      calculateValue: 'calculateValue',
      calculateValueJson: 'calculateValue-json',
      dbIndex: 'dbIndex',
      dataSrc: 'dataSrc',
      dataValues: 'data.values',
      dataJson: 'data.json',
      dataUrl: 'data.url',
      dataResource: 'data.resource',
      lazyLoad: 'lazyLoad',
      dataHeaders: 'data.headers',
      valueProperty: 'valueProperty',
      selectValues: 'selectValues',
      selectFields: 'selectFields',
      dataCustom: 'data.custom',
      disableLimit: 'disableLimit',
      searchField: 'searchField',
      minSearch: 'minSearch',
      filter: 'filter',
      sort: 'sort',
      limit: 'limit',
      template: 'template',
      searchEnabled: 'searchEnabled',
      reference: 'reference',
      authenticate: 'authenticate',
      readOnlyValue: 'readOnlyValue',
      customOptions: 'customOptions',
      selectThreshold: 'selectThreshold',
      thousandsSeparator: 'thousandsSeparator',
      requireDecimal: 'requireDecimal',
      decimalLimit: 'decimalLimit'
    };
  }
}
