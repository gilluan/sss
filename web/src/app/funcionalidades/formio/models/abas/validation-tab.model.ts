import {EditFormTab} from './edit-form-tab.model';

export class ValidationTab extends EditFormTab {
  getName = (): string => 'validation';

  getOptions = () => ({
    required: 'required',
    unique: 'unique',
    minWords: 'validate.minWords',
    maxWords: 'validate.maxWords',
    regex: 'validate.pattern',
    validateOn: 'validateOn',
    validationJs: 'custom-validation-js',
    validationJson: 'json-validation-json',
    customMessage: 'validate.customMessage',
    customValidation: 'customValidation',
    jsonLogicValidatio: 'jsonLogicValidation',
    minLength: 'validate.minLength',
    maxLength: 'validate.maxLength',
    pattern: 'validate.pattern',
    customValidationJs: 'custom-validation-js',
    jsonValidationJson: 'json-validation-json',
    validateRequired: 'validateRequired',
    validateSelect: 'validate.select',
    validateCustom: 'validate.custom',
    validateCustomPrivate: 'validate.customPrivate',
    validateJson: 'validate.json',
    validateMinSelectedCount: 'validate.minSelectedCount',
    validateMaxSelectedCount: 'validate.maxSelectedCount',
    minSelectedCountMessage: 'minSelectedCountMessage',
    maxSelectedCountMessage: 'maxSelectedCountMessage',
    requireDay: 'fields.day.required',
    requireMonth: 'fields.month.required',
    requireYear: 'fields.year.required',
    minDate: 'minDate',
    maxDate: 'maxDate'
  })
}
