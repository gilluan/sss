import {EditFormTab} from './edit-form-tab.model';

export class ConditionalTab extends EditFormTab {
  getName = (): string => 'conditional';

  getOptions = (): Object => ({
    simpleConditional: 'simple-conditional',
    customConditionalPanel: 'customConditionalPanel',
    customConditionalJs: 'customConditional-js',
    customConditional: 'customConditional',
    customConditionalJson: 'customConditional-json',
    conditionalJson: 'conditional.json'
  })
}
