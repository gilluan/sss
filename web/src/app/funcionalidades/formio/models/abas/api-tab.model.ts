import {EditFormTab} from './edit-form-tab.model';

export class ApiTab extends EditFormTab {
  getName = (): string => 'api';

  getOptions = () => ({
    key: 'key',
    tags: 'tags',
    properties: 'properties'
  })
}
