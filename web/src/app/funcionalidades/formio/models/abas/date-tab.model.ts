import {EditFormTab} from './edit-form-tab.model';

export class DateTab extends EditFormTab {
  getName = (): string => 'date';

  getOptions = () => ({
    enableDate: 'enableDate',
    minDate: 'datePicker.minDate',
    maxDate: 'datePicker.maxDate',
  })
}
