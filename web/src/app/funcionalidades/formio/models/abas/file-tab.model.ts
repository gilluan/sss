import {EditFormTab} from './edit-form-tab.model';

export class FileTab extends EditFormTab {

  getName = (): string => 'file';

  getOptions() {
    return {
      storage: 'storage',
      url: 'url',
      options: 'options',
      directory: 'dir',
      fileNameTemplate: 'fileNameTemplate',
      displayAsImage: 'image',
      privateDownload: 'privateDownload',
      imageSize: 'imageSize',
      webcam: 'webcam',
      webcamSize: 'webcamSize',
      fileTypes: 'fileTypes',
      filePattern: 'filePattern',
      fileMinSize: 'fileMinSize',
      fileMaxSize: 'fileMaxSize'
    };
  }
}
