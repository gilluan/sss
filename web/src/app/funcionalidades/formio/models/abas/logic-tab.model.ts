import {EditFormTab} from './edit-form-tab.model';

export class LogicTab extends EditFormTab {
  getName = (): string => 'logic';

  getOptions = (): Object => ({
    logic: 'logic'
  })
}
