import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { TermoVoluntario } from '../models/termo-voluntario.model';
import { TermoCompromisso } from '@app/funcionalidades/configuracao-termo/models/termo-compromisso.model';
import { ModalRef } from '@sicoob/ui';

export enum TermoVoluntarioActionTypes {
  LoadTermoVoluntarios = '[TermoVoluntario] Load TermoVoluntarios',
  AddTermoVoluntario = '[TermoVoluntario] Add TermoVoluntario',
  UpsertTermoVoluntario = '[TermoVoluntario] Upsert TermoVoluntario',
  AddTermoVoluntarios = '[TermoVoluntario] Add TermoVoluntarios',
  UpsertTermoVoluntarios = '[TermoVoluntario] Upsert TermoVoluntarios',
  UpdateTermoVoluntario = '[TermoVoluntario] Update TermoVoluntario',
  UpdateTermoVoluntarios = '[TermoVoluntario] Update TermoVoluntarios',
  DeleteTermoVoluntario = '[TermoVoluntario] Delete TermoVoluntario',
  DeleteTermoVoluntarios = '[TermoVoluntario] Delete TermoVoluntarios',
  ClearTermoVoluntarios = '[TermoVoluntario] Clear TermoVoluntarios',
  LoadTermoVoluntariosSuccess = '[LoadTermoVoluntariosSuccess] Carregar TermoCompromisso Sucesso',
  LoadTermoVoluntariosFail = '[LoadTermoVoluntariosFail] Carregar TermoCompromisso Erro',
  AbrirModal = '[AbrirModal] TermoCompromisso',
  AddModal = '[AddModal] TermoCompromisso',
  FecharModal = '[FecharModal] TermoCompromisso',
}

export class AbrirModal implements Action {
  readonly type = TermoVoluntarioActionTypes.AbrirModal;
  constructor() { }
}

export class AddModal implements Action {
  readonly type = TermoVoluntarioActionTypes.AddModal;
  constructor(public modalRef: ModalRef) { }
}

export class FecharModal implements Action {
  readonly type = TermoVoluntarioActionTypes.FecharModal;
  constructor() { }
}


export class LoadTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.LoadTermoVoluntarios;
  constructor(public tipo: string) { }

}

export class LoadTermoVoluntariosSuccess implements Action {
  readonly type = TermoVoluntarioActionTypes.LoadTermoVoluntariosSuccess;
  constructor(public payload: TermoCompromisso) { }
}

export class LoadTermoVoluntariosFail implements Action {
  readonly type = TermoVoluntarioActionTypes.LoadTermoVoluntariosFail;
  constructor(public payload: TermoCompromisso) { }
}


export class AddTermoVoluntario implements Action {
  readonly type = TermoVoluntarioActionTypes.AddTermoVoluntario;

  constructor(public payload: { termoVoluntario: TermoVoluntario }) { }
}

export class UpsertTermoVoluntario implements Action {
  readonly type = TermoVoluntarioActionTypes.UpsertTermoVoluntario;

  constructor(public payload: { termoVoluntario: TermoVoluntario }) { }
}

export class AddTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.AddTermoVoluntarios;

  constructor(public payload: { termoVoluntarios: TermoVoluntario[] }) { }
}

export class UpsertTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.UpsertTermoVoluntarios;

  constructor(public payload: { termoVoluntarios: TermoVoluntario[] }) { }
}

export class UpdateTermoVoluntario implements Action {
  readonly type = TermoVoluntarioActionTypes.UpdateTermoVoluntario;

  constructor(public payload: { termoVoluntario: Update<TermoVoluntario> }) { }
}

export class UpdateTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.UpdateTermoVoluntarios;

  constructor(public payload: { termoVoluntarios: Update<TermoVoluntario>[] }) { }
}

export class DeleteTermoVoluntario implements Action {
  readonly type = TermoVoluntarioActionTypes.DeleteTermoVoluntario;

  constructor(public payload: { id: string }) { }
}

export class DeleteTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.DeleteTermoVoluntarios;

  constructor(public payload: { ids: string[] }) { }
}

export class ClearTermoVoluntarios implements Action {
  readonly type = TermoVoluntarioActionTypes.ClearTermoVoluntarios;
}

export type TermoVoluntarioActions =
  AddModal
  | FecharModal
  | AbrirModal
  | LoadTermoVoluntarios
  | AddTermoVoluntario
  | UpsertTermoVoluntario
  | AddTermoVoluntarios
  | UpsertTermoVoluntarios
  | UpdateTermoVoluntario
  | UpdateTermoVoluntarios
  | DeleteTermoVoluntario
  | DeleteTermoVoluntarios
  | ClearTermoVoluntarios
  | LoadTermoVoluntariosSuccess
  | LoadTermoVoluntariosFail;
