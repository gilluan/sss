import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxBrModule } from '@nbfontana/ngx-br';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AlertComponent, AlertModule, CardModule, CheckboxModule, FormModule, ModalModule } from '@sicoob/ui';
import { CpfCnpjModule } from 'ng2-cpf-cnpj';
import { BsDatepickerModule, defineLocale, ptBrLocale, PopoverModule } from 'ngx-bootstrap';
import { CountdownModule } from 'ngx-countdown';
import { NgxFileDropModule } from 'ngx-file-drop';
import { NgxMaskModule } from 'ngx-mask';
import { QuillEditorModule } from 'ngx-quill-editor';
import { DialogoSucessoComponent } from 'src/app/shared/components/dialogo-sucesso/dialogo-sucesso.component';
import { SinsLoadingService } from 'src/app/shared/components/sins-loading/sins-loading.service';
import { ClickOutsideModule } from 'src/app/shared/directive/click-outside/click-outside.module';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { RadioModule } from '../../shared/components/sc-radio/radio.module';
import { SharedModule } from '../../shared/shared.module';
import { DadosCadastraisComponent } from './components/dados-cadastrais/dados-cadastrais.component';
import { DialogoCorrecoesComponent } from './components/dialogo-correcoes/dialogo-correcoes.component';
import { DialogoTokenComponent } from './components/dialogo-token/dialogo-token.component';
import { TermoVoluntarioComponent } from './components/termo-voluntario/termo-voluntario.component';
import { WelcomeVoluntarioComponent } from './components/welcome-voluntario/welcome-voluntario.component';
import { ContainerTermoVoluntarioComponent } from './containers/container-termo-voluntario/container-termo-voluntario.component';
import { TermoVoluntarioEffects } from './effects/termo-voluntario.effects';
import * as fromTermoVoluntario from './reducers/termo-voluntario.reducer';
import { TermoVoluntarioRoutingModule } from './termo-voluntario-routing.module';
defineLocale('pt-br', ptBrLocale);

@NgModule({
  declarations: [
    ContainerTermoVoluntarioComponent,
    DialogoTokenComponent,
    TermoVoluntarioComponent,
    WelcomeVoluntarioComponent,
    DadosCadastraisComponent,
    DialogoCorrecoesComponent,
  ],
  entryComponents: [AlertComponent, DialogoTokenComponent, DialogoSucessoComponent, DialogoCorrecoesComponent],
  imports: [
    PopoverModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CountdownModule,
    QuillEditorModule,
    CommonModule,
    TermoVoluntarioRoutingModule,
    StoreModule.forFeature('termoVoluntario', fromTermoVoluntario.reducer),
    AlertModule,
    CheckboxModule,
    FormModule,
    ModalModule,
    FormsModule,
    NgxBrModule.forRoot(),
    NgxMaskModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    NgxFileDropModule,
    CardModule,
    CpfCnpjModule,
    SharedModule,
    EffectsModule.forFeature([TermoVoluntarioEffects]),
    RadioModule,
    ClickOutsideModule
  ],
  providers: [PersistanceService, SinsLoadingService, DatePipe]
})
export class TermoVoluntarioModule { }
