import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeVoluntarioComponent } from './components/welcome-voluntario/welcome-voluntario.component';
import { TermoVoluntarioComponent } from './components/termo-voluntario/termo-voluntario.component';
import { DadosCadastraisComponent } from './components/dados-cadastrais/dados-cadastrais.component';
import { ContainerTermoVoluntarioComponent } from './containers/container-termo-voluntario/container-termo-voluntario.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerTermoVoluntarioComponent,
    children: [
      {
        path: '',
        component: WelcomeVoluntarioComponent
      },
      {
        path: 'termo-voluntario',
        component: TermoVoluntarioComponent,
        data: {
          breadcrumb: "Termo de Compromisso"
        }
      },
      {
        path: 'dados-cadastrais',
        component: DadosCadastraisComponent,
        data: {
          breadcrumb: "Termo de Compromisso"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermoVoluntarioRoutingModule { }
