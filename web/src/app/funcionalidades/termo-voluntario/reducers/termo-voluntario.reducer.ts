import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TermoVoluntarioActions, TermoVoluntarioActionTypes } from '../actions/termo-voluntario.actions';
import { TermoVoluntario } from '../models/termo-voluntario.model';
import { TermoCompromisso } from '@app/funcionalidades/configuracao-termo/models/termo-compromisso.model';
import { GedActionTypes, GedActions } from '@app/actions/ged.actions';
import { VoluntarioActionTypes, VoluntarioAction } from '@app/actions/voluntarioAction';
import { ModalRef } from '@sicoob/ui';

export type ModalSuccessProp = {
  header: string,
  buttonClass: string,
  textButton: string,
  rota: string,
  message: string,
};

export interface State extends EntityState<TermoVoluntario> {
  // additional entities state properties
  compromisso: TermoCompromisso;
  modal: ModalSuccessProp,
  modalRef: ModalRef;

}

export const adapter: EntityAdapter<TermoVoluntario> = createEntityAdapter<TermoVoluntario>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  compromisso: null,
  modal: {
    header: 'Agurdando resposta...',
    buttonClass: 'mdi-loading mdi-spin',
    message: 'Estamos analisando a solicitação, por favor aguarde',
    rota: '/acompanhamento-cadastro',
    textButton: null
  },
  modalRef: null
});

export function reducer(
  state = initialState,
  action: TermoVoluntarioActions | VoluntarioAction
): State {
  switch (action.type) {
    case TermoVoluntarioActionTypes.AddTermoVoluntario: {
      return adapter.addOne(action.payload.termoVoluntario, state);
    }

    case TermoVoluntarioActionTypes.UpsertTermoVoluntario: {
      return adapter.upsertOne(action.payload.termoVoluntario, state);
    }

    case TermoVoluntarioActionTypes.AddTermoVoluntarios: {
      return adapter.addMany(action.payload.termoVoluntarios, state);
    }

    case TermoVoluntarioActionTypes.UpsertTermoVoluntarios: {
      return adapter.upsertMany(action.payload.termoVoluntarios, state);
    }

    case TermoVoluntarioActionTypes.UpdateTermoVoluntario: {
      return adapter.updateOne(action.payload.termoVoluntario, state);
    }

    case TermoVoluntarioActionTypes.UpdateTermoVoluntarios: {
      return adapter.updateMany(action.payload.termoVoluntarios, state);
    }

    case TermoVoluntarioActionTypes.DeleteTermoVoluntario: {
      return adapter.removeOne(action.payload.id, state);
    }

    case TermoVoluntarioActionTypes.DeleteTermoVoluntarios: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case TermoVoluntarioActionTypes.LoadTermoVoluntariosSuccess: {
      return {...state, compromisso: action.payload };
    }

    case TermoVoluntarioActionTypes.ClearTermoVoluntarios: {
      return adapter.removeAll(state);
    }

    case VoluntarioActionTypes.ConcordarTermoVoluntarioSuccess: {
      let modal = state.modal;
      modal.message = 'Cadastro realizado com sucesso';
      modal.buttonClass = 'mdi-check-circle-outline';
      modal.header = 'Pedido enviado';
      modal.textButton = 'Acompanhar cadastro';
      return {...state, modal }
    }

    case TermoVoluntarioActionTypes.AddModal: {
      return {...state, modalRef: action.modalRef}
    }

    default: {
      return state;
    }
  }
}

export const getTermoVoluntarioState = createFeatureSelector<State>('termoVoluntario');


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getTermoVoluntarioState);


export const getFirst = createSelector(
  getTermoVoluntarioState,
  termo =>  termo.compromisso
);


export const getModal = createSelector(
  getTermoVoluntarioState,
  termo =>  termo.modal
);


export const getModalRef = createSelector(
  getTermoVoluntarioState,
  termo =>  termo.modalRef
);

