import { Component, OnInit, ViewChild, TemplateRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { HeaderActionsContainerService } from '@sicoob/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { Observable } from 'rxjs';
import { JustificativaAjusteVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';
import * as fromJustificativaAjustes from '@app/funcionalidades/sins-analise-cadastros/reducers/justificativa-ajustes.reducer';
import { LoadJustificativasAjustes } from '@app/funcionalidades/sins-analise-cadastros/actions/analise-voluntario.actions';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { SituacaoVoluntarioType } from '@app/shared/types/situacao-voluntario.type';
import { VoluntariosService } from '@app/funcionalidades/sins-analise-cadastros/voluntarios.service';
import * as fromApp from '../../../../app.reduce';
import { DisplayVisibilityHeader, DisplayToolbar } from '@app/app.actions';

@Component({
  selector: 'sc-container-termo-voluntario',
  templateUrl: './container-termo-voluntario.component.html',
  styleUrls: ['./container-termo-voluntario.component.css']
})
export class ContainerTermoVoluntarioComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('correcoesDados') headerTpl: TemplateRef<any>;

  constructor(
    private readonly headerActionService: HeaderActionsContainerService,
    /*private justificativaStore: Store<fromJustificativaAjustes.State>,
    private persistenceService: PersistanceService,
    private voluntarioService: VoluntariosService,*/
    public appStore$: Store<fromApp.State>,) {
    }

  public popoverAparente : boolean = false;
  public justificativasAjustes: Observable<JustificativaAjusteVm[]>;
  public existeJustificativas: boolean = false;

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(false));
    this.appStore$.dispatch(new DisplayVisibilityHeader(false));
    /*let userInstituto = this.persistenceService.get("usuario_instituto");
    this.justificativaStore.dispatch(new LoadJustificativasAjustes(userInstituto.id));
    this.voluntarioService.findVoluntarioByCpf(userInstituto.cpf).subscribe(voluntarioExistente => {
      if(voluntarioExistente) {
        if(voluntarioExistente.situacao == SituacaoVoluntarioType.ajusteVoluntario || voluntarioExistente.situacao == SituacaoVoluntarioType.devolvidoAjuste ){
          this.justificativasAjustes = this.justificativaStore.pipe(select(fromJustificativaAjustes.selectAll));
          this.existeJustificativas  = true;
        }
      }
    })*/
  }

  ngAfterViewInit(): void {
    this.headerActionService.open(new TemplatePortal(this.headerTpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.appStore$.dispatch(new DisplayVisibilityHeader(true))
    this.headerActionService.remove();
  }

  /*apresentarCorrecoes() {
    this.popoverAparente = !this.popoverAparente;
  }

  fecharPopover() {
    this.popoverAparente = false;
  }*/

}
