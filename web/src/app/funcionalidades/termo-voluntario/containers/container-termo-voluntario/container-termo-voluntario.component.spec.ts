import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerTermoVoluntarioComponent } from './container-termo-voluntario.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerTermoVoluntarioComponent', () => {
  let component: ContainerTermoVoluntarioComponent;
  let fixture: ComponentFixture<ContainerTermoVoluntarioComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerTermoVoluntarioComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerTermoVoluntarioComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
