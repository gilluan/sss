export class TokenResult {
  isValid: boolean;
  reason: string;
}
