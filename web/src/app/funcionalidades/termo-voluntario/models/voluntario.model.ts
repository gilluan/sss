import { VoluntarioTipo } from "./voluntario-tipo.enum";
import { DadosTermo } from "./dados-termo.model";
import { BaseDto } from "@app/shared/models/base-dto.model";
import { Game } from "@app/shared/models/game.model";

export class Voluntario extends BaseDto {
  voluntarioTipo: VoluntarioTipo;
  idPerfil: string;
  codigoVoluntario: number;
  genero: string;
  loginVoluntario: string;
  nome: string;
  rg: string;
  cpf: string;
  dataNascimento: any;
  nacionalidade: string;
  naturalidade: string;
  profissao: string;
  telefone: string;
  email: string;
  endCep: string;
  endLogradouro: string;
  endNumero: string;
  endBairro: string;
  endCidade: string;
  endUf: string;
  idDocumentoCertificado: string;
  situacao: string;
  horas: number;
  projetos: number;
  ativo: boolean = true;
  numeroCooperativa: string;
  dadosTermo: DadosTermo;
  game: Game[]
}
