import {Component, Inject, OnInit, Renderer} from '@angular/core';
import {MODAL_DATA, ModalRef} from '@sicoob/ui';
import {TokenService} from '../token-service/token.service';
import {Token} from '../../models/token.model';
import {DirectionFocus} from '../../models/direction-focus.enum';
import {LoaderService} from '@shared/services/loader.service';

@Component({
  selector: 'app-dialogo-token',
  templateUrl: './dialogo-token.component.html',
  styleUrls: ['./dialogo-token.component.scss']
})
export class DialogoTokenComponent implements OnInit {

  isPossivelReenviar: boolean = false;

  token: string;
  cpf: string;
  tokenChars: string[] = new Array(4);
  $eventCountdown: any;

  erro = false;
  isReenviado = false;

  lastEvent: number;
  isPaste: boolean;
  title = 'Insira seu código de segurança';

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any,
    private tokenService: TokenService,
    private renderer: Renderer,
    private loaderService: LoaderService) { }

  ngOnInit() {
    this.renderer.selectRootElement('#tokenChar0').focus();
  }

  public closeModal() {
    this.ref.close();
  }

  onKeyDownInputToken(keyboardEvent: KeyboardEvent) {
    const keyCode: number = keyboardEvent.keyCode;
    this.lastEvent = keyCode;

    if (!(keyCode >= 48 && keyCode <= 57) &&
      !(keyCode >= 65 && keyCode <= 90) &&
      !(keyCode >= 96 && keyCode <= 105) &&
      keyCode != 8 && keyCode != 37 && keyCode != 39 && keyCode != 46) {
      keyboardEvent.preventDefault() ;
    }
  }

  onKeyUpInputToken(keyboardEvent: KeyboardEvent) {
    const keyCode: number = keyboardEvent.keyCode;
    const currentElementId: string = keyboardEvent.srcElement.id;
    const index = currentElementId.slice(currentElementId.length - 1, currentElementId.length);
    if ((this.lastEvent === 17 && keyboardEvent.keyCode === 86) || this.isPaste) {
    } else if (this.lastEvent === 86 && keyboardEvent.keyCode === 17) {
    } else if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 96 && keyCode <= 105)) {
      this.nextFocus(currentElementId, DirectionFocus.Forward);
      this.tokenChars[index] = keyboardEvent.key;
    } else if (keyCode === 8) {
      this.tokenChars[index] = null; // (value, index);
      this.nextFocus(currentElementId, DirectionFocus.Backward);
    } else if (keyCode === 37) {
      this.nextFocus(currentElementId, DirectionFocus.Backward);
    } else if (keyCode === 39) {
      this.nextFocus(currentElementId, DirectionFocus.Forward);
    }
    this.lastEvent = keyCode;
    this.isPaste = false;

  }

  nextFocus(currentElementId: string, directionFocus: DirectionFocus) {
    const nextElementId = this.getNextElementToFocus(currentElementId, directionFocus);
    const element = this.renderer.selectRootElement('#' + nextElementId);
    element.focus();
    return nextElementId;
  }

  pasteEvent(event: ClipboardEvent) {
    this.isPaste = true;
    const clipboardData = event.clipboardData;
    const pastedText: string = clipboardData.getData('text').trim();
    this.tokenChars =  Object.assign([], pastedText.slice(0, 5));
  }

  public validateAcesso() {
    this.token = this.tokenChars.toString().split(',').join('');
    this.validateToken(this.token);
  }

  public regenerateToken() {
    const tokenObj: Token = new Token();
    tokenObj.cpf = this.data.cpf;
    tokenObj.email = this.data.email;
    tokenObj.nome = this.data.nome;
    this.tokenService.registerToken(tokenObj).subscribe(tokenResult => {
      this.isReenviado = tokenResult.isValid;
    });
    this.$eventCountdown.restart();
    this.isPossivelReenviar = false;
  }

  public onFinished($eventCountdown: any){
    this.$eventCountdown = $eventCountdown;
    this.isPossivelReenviar = true;
  }

  private validateToken(token: string) {
    const tokenObj: Token = new Token();
    tokenObj.cpf = this.data.cpf;
    tokenObj.token = token;
    this.tokenService.validateToken(tokenObj).subscribe(tokenResult => {
      if (tokenResult.isValid) {
        this.loaderService.show();
        this.ref.close(tokenResult.isValid);
      } else {
        this.erro = true;
      }
    });
  }

  private getNextElementToFocus(currentElement: string, directionFocus: DirectionFocus): string {
    let elementNumber: number;
    let nextElement: string;
    try {
      nextElement = currentElement.substring(0, currentElement.length - 1);
      elementNumber = Number.parseInt(currentElement.substring(currentElement.length - 1, currentElement.length));

      if (0 < elementNumber && elementNumber < 4) {
        if (directionFocus === DirectionFocus.Forward) {
          elementNumber++;
        } else if (directionFocus === DirectionFocus.Backward) {
          elementNumber--;
        }
      } else if (elementNumber <= 0) {
        if (directionFocus === DirectionFocus.Forward) {
          elementNumber++;
        } else if (directionFocus === DirectionFocus.Backward) {
          elementNumber = 0;
        }
      } else if (elementNumber >= 4) {
        if (directionFocus === DirectionFocus.Forward) {
          elementNumber = 4;
        } else if (directionFocus === DirectionFocus.Backward) {
          elementNumber--;
        }
      }
    } catch {
      elementNumber = 0;
    }
    return nextElement + elementNumber;
  }
}
