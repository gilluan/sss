import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeVoluntarioComponent } from './welcome-voluntario.component';

describe('WelcomeVoluntarioComponent', () => {
  let component: WelcomeVoluntarioComponent;
  let fixture: ComponentFixture<WelcomeVoluntarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeVoluntarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeVoluntarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
