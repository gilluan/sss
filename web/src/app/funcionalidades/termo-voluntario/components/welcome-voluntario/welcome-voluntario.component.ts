import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersistanceService } from 'src/app/shared/services/persistence.service';

@Component({
  selector: 'sc-welcome-voluntario',
  templateUrl: './welcome-voluntario.component.html',
  styleUrls: ['./welcome-voluntario.component.css']
})
export class WelcomeVoluntarioComponent implements OnInit {

  constructor(private router: Router, private persistenceService: PersistanceService) { }

  cpf: string;
  userName: string;

  navigatePaginaTermoVoluntario() {
    this.router.navigate(['/welcome/termo-voluntario']);
  }

  aboutSins() {
    const win = window.open('http://www.institutosicoob.org.br/');
  }

  ngOnInit() {
    const userInstituto = this.persistenceService.get('usuario_instituto');
    this.cpf = userInstituto.cpf;
    this.userName = userInstituto.nome;
  }


}
