import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EditarArquivoCursoVoluntario, SalvarArquivoCursoVoluntario } from '@app/actions/ged.actions';
import { CarregarVoluntariosPorCPF, ConcordarTermoVoluntario } from '@app/actions/voluntarioAction';
import { Curso } from '@app/funcionalidades/perfil-usuario/models/curso.model';
import { LoadJustificativasAjustes } from '@app/funcionalidades/sins-analise-cadastros/actions/analise-voluntario.actions';
import { JustificativaAjusteVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';
import * as fromJustificativaAjustes from '@app/funcionalidades/sins-analise-cadastros/reducers/justificativa-ajustes.reducer';
import * as fromVoluntario from '@app/reducers/voluntario.reducer';
import { RadioOption } from '@app/shared/components/sc-radio/radio-option.model';
import { TipoDocumentoGed } from '@app/shared/ged/models/tipo-arquivo-ged.enum';
import { select, Store } from '@ngrx/store';
import { GedService } from '@shared/ged/ged.service';
import { DocumentoEnvio } from '@shared/ged/models/envio/documentoEnvio.model';
import { UsuarioInstituto } from '@shared/models/usuario-instituto.model';
import { Cep } from '@shared/services/cep/cep.model';
import { CepService } from '@shared/services/cep/cep.service';
import { Color, ModalRef, ModalService } from '@sicoob/ui';
import { Subscription, Observable } from 'rxjs';
import { DisplayToolbar } from 'src/app/app.actions';
import { DialogoSucessoComponent } from 'src/app/shared/components/dialogo-sucesso/dialogo-sucesso.component';
import { CustomAlertService } from 'src/app/shared/services/alert-service';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { SituacaoVoluntarioType } from 'src/app/shared/types/situacao-voluntario.type';
import * as fromApp from '../../../../app.reduce';
import * as fromGed from '../../../../reducers/ged.reducer';
import { Token } from '../../models/token.model';
import { VoluntarioTipo } from '../../models/voluntario-tipo.enum';
import { DialogoCorrecoesComponent } from '../dialogo-correcoes/dialogo-correcoes.component';
import { DialogoTokenComponent } from '../dialogo-token/dialogo-token.component';
import { TokenService } from '../token-service/token.service';
import { Voluntario } from '../../models/voluntario.model';
import { BsLocaleService } from 'ngx-bootstrap';
import * as fromTermoVoluntario from '../../reducers/termo-voluntario.reducer';
import { FecharModal } from '../../actions/termo-voluntario.actions';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

@Component({
  selector: 'sc-dados-cadastrais',
  templateUrl: './dados-cadastrais.component.html',
  styleUrls: ['./dados-cadastrais.component.scss']
})
export class DadosCadastraisComponent implements OnInit, OnDestroy {


  subscription: Subscription = new Subscription();
  modalRefs: ModalRef[] = new Array<ModalRef>();
  isFileLoaded = false;
  voluntario: Voluntario = new Voluntario();
  fileCertificado: File;
  arrayFileData: number[];
  selectedFileCert: string;
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  messageErrorFileSize = 'O tamanho máximo do arquivo deve ser 500 KB';
  messageErrorFileType = 'Somente o tipo PDF é permitido';
  messageErrorSendFile = 'Problema ao enviar o certificado ou certificado não encontrado';
  generos: RadioOption[] = [
    new RadioOption('Masculino', 'masculino'),
    new RadioOption('Feminino', 'feminino'),
    new RadioOption('Outros', 'outros'),
  ];
  userInstituto: any;
  public files: NgxFileDropEntry[] = [];
  public popoverAparente: boolean = false;
  public existeJustificativas: boolean = false;
  public justificativasAjustes: Observable<JustificativaAjusteVm[]>;

  constructor(
    private router: Router,
    private changeDetector: ChangeDetectorRef,
    private cepService: CepService,
    private gedService: GedService,
    private tokenService: TokenService,
    private modalService: ModalService,
    public appStore$: Store<fromApp.State>,
    private persistenceService: PersistanceService,
    private justificativaStore: Store<fromJustificativaAjustes.State>,
    private alertService: CustomAlertService,
    private gedStore$: Store<fromGed.State>,
    private voluntarioStore$: Store<fromVoluntario.State>,
    private localeService: BsLocaleService,
    private store$: Store<fromTermoVoluntario.State>,

  ) { }


  ngOnInit() {
    this.localeService.use("pt-br");
    this.appStore$.dispatch(new DisplayToolbar(false));
    this.userInstituto = this.persistenceService.get('usuario_instituto');
    if (this.userInstituto && this.userInstituto.cpf) {
      this.atualizarVoluntarioComUsuarioInstituto(this.userInstituto);
      this.voluntarioStore$.dispatch(new CarregarVoluntariosPorCPF(this.voluntario.cpf));
      this.subscription
        .add(this.carregarVoluntario());
    }
  }
  private carregarVoluntario = () => {
    return this.voluntarioStore$.pipe(select(fromVoluntario.getVoluntarioAtual)).subscribe(voluntarioExistente => {
      if (voluntarioExistente) {
        this.voluntario = Object.assign(new Voluntario(), voluntarioExistente);
        this.voluntario.dataNascimento = this.getDateAsString(this.voluntario.dataNascimento);
        this.isFileLoaded = this.voluntario.idDocumentoCertificado != null;
        this.selectedFileCert = this.voluntario.idDocumentoCertificado != null ? 'Certificado do Curso de Voluntariado' : this.selectedFileCert;
        this.subscription.add(this.abrirModalJustificativa());
      }
    })
  }

  private abrirModalJustificativa = () => {
    if (this.voluntario && this.voluntario.id && (this.voluntario.situacao === SituacaoVoluntarioType.ajusteVoluntario || this.voluntario.situacao === SituacaoVoluntarioType.devolvidoAjuste)) {
      this.justificativaStore.dispatch(new LoadJustificativasAjustes(this.voluntario.id));
      this.justificativasAjustes = this.justificativaStore.pipe(select(fromJustificativaAjustes.selectAll));
      this.existeJustificativas = true;
      let sub = this.justificativasAjustes.subscribe(
        (justificativas: JustificativaAjusteVm[]) => {
          if (this.modalRefs.length > 0) { this.modalRefs.forEach(s => s.close()) }
          if (justificativas && justificativas.length > 0) {
            this.modalRefs.push(this.modalService.open(DialogoCorrecoesComponent, { data: { justificativa: justificativas[0] }, panelClass: 'sins-modal-token' }))
          }
        })
      return sub;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
    if (this.modalRefs.length > 0) this.modalRefs.forEach(s => s.close())
    this.store$.dispatch(new FecharModal())
  }

  private gerarDocumentoEnvio() {
    try {
      const fileBinario: number[] = this.arrayFileData;
      if (this.userInstituto)
        return this.gedService.criarDocumentoEnvio(fileBinario, TipoDocumentoGed.fluxoPassos, this.userInstituto);
    } catch (e) {
      console.error('erro', e);
    }
  }

  enviarCertificadoGed(isNewVoluntario: boolean) {
    let documentoEnvio: DocumentoEnvio = this.gerarDocumentoEnvio();
    if (documentoEnvio) {
      if (isNewVoluntario) {
        this.gedStore$.dispatch(new SalvarArquivoCursoVoluntario(documentoEnvio, this.populateCurso(), this.voluntario));
      } else {
        this.gedStore$.dispatch(new EditarArquivoCursoVoluntario(documentoEnvio, this.populateCurso(), this.voluntario));
      }
    }
  }

  private populateCurso() {
    const curso: Curso = new Curso();
    curso.categoriaCurso = null;
    curso.dataConclusao = null;
    curso.descricao = '';
    curso.identificadorCertificadoGED = null;
    curso.nome = 'Voluntário Transformador';
    curso.quantidadeHoras = 20;
    curso.voluntario = this.voluntario.id;
    return curso;
  }

  private atualizarVoluntario(): void {
    if (this.voluntario.idDocumentoCertificado) {
      this.voluntario.situacao = SituacaoVoluntarioType.aguardandoAnalise;
      this.voluntarioStore$.dispatch(new ConcordarTermoVoluntario(this.voluntario));
    } else {
      this.showFileError(this.messageErrorSendFile);
      this.changeDetector.detectChanges();
    }
  }

  onLostFocusFieldCep() {
    if (this.voluntario.endCep) {
      this.cepService.findCep(this.voluntario.endCep)
        .subscribe(cepFound => {
          if (cepFound != null) {
            this.populateFieldsEndereco(cepFound);
          } else {
            this.populateFieldsEndereco(new Cep());
          }
          this.changeDetector.detectChanges();
        });
    } else {
      this.populateFieldsEndereco(new Cep());
    }
  }

  public loadArrayFileData(file: File) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsArrayBuffer(new Blob([file]));

    fileReader.onload = (e) => {
      this.arrayFileData = Array.from(new Int8Array(fileReader.result as ArrayBuffer));
    };
  }

  public fileChangeEvent(fileInput: any) {
    this.fileCertificado = fileInput.target.files[0];
    if (this.fileCertificado != null && this.fileCertificado != undefined) {
      if (this.isFileTypeAllowed(this.fileCertificado)) {
        if (this.isFileSizeAllowed(this.fileCertificado)) {
          this.loadArrayFileData(this.fileCertificado);
          this.isFileLoaded = true;
          this.selectedFileCert = this.fileCertificado.name;
        } else {
          this.showFileError(this.messageErrorFileSize);
        }
      } else {
        this.showFileError(this.messageErrorFileType);
      }
    }
  }

  //FIXME Arrumar as funcionalidades do NgxFileUpload para certificado
  public dropped(files: NgxFileDropEntry[]) {
    const that = this;
    this.files = files;
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;

        fileEntry.file((file: File) => {
          that.fileCertificado = file;
          if (this.isFileTypeAllowed(this.fileCertificado)) {
            if (this.isFileSizeAllowed(this.fileCertificado)) {
              this.loadArrayFileData(this.fileCertificado);
              this.isFileLoaded = true;
              this.selectedFileCert = this.fileCertificado.name;
            } else {
              this.showFileError(this.messageErrorFileSize);
            }
          } else {
            this.showFileError(this.messageErrorFileType);
          }
          this.changeDetector.detectChanges();
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  solicitarAcesso() {
    //this.registerToken();
    this.criarAtualizarVoluntario();
  }

  public cancelarClick() {
    if (this.voluntario != null && this.voluntario.situacao === SituacaoVoluntarioType.ajusteVoluntario) {
      this.router.navigate(['/acompanhamento-cadastro']);
    } else {
      this.router.navigate(['/welcome/termo-voluntario']);
    }
  }

  registerToken() {
    const token: Token = new Token();
    token.cpf = this.voluntario.cpf;
    token.email = this.voluntario.email;
    token.nome = this.voluntario.nome;
    this.tokenService.registerToken(token).subscribe(tokenResult => {
      if (tokenResult.isValid) {
        let modal = this.modalService.open(DialogoTokenComponent, {
          data: {
            email: this.voluntario.email,
            cpf: this.voluntario.cpf,
            nome: this.voluntario.nome,
            numeroCooperativa: this.voluntario.numeroCooperativa
          },
          panelClass: 'sins-modal-token'
        })
        this.modalRefs.push(modal);
        modal.afterClosed().subscribe(isValid => {
          if (isValid) {
            this.changeDetector.detectChanges();
            this.criarAtualizarVoluntario();
          }
        });
      } else {
        this.openModalErro(tokenResult.reason);
      }
    }, erro => this.openModalErro(erro));
  }

  openModalErro(erro: any) {
    const data: any = {
      header: 'Erro no envio do token',
      message: erro,
      buttonClass: 'mdi-close-circle-outline',
      rota: 'welcome/dados-cadastrais',
      textButton: 'Voltar para seus dados'
    };

    this.modalService.open(DialogoSucessoComponent, { data: data, panelClass: 'sins-modal-success' });
  }


  public fileOver(event) {
  }

  public fileLeave(event) {
  }

  private isFileTypeAllowed(fileCert: File): boolean {
    return fileCert.type === 'application/pdf';
  }

  private isFileSizeAllowed(fileCert: File): boolean {
    return (fileCert.size / 1024 < 500);
  }

  private showFileError(message: string) {
    this.alertService.abrirAlert(Color.DANGER, message);
  }

  private populateFieldsEndereco(cep: Cep) {
    this.voluntario.endCep = cep.cep;
    this.voluntario.endBairro = cep.bairro;
    this.voluntario.endCidade = cep.cidade;
    this.voluntario.endLogradouro = cep.endereco;
    this.voluntario.endUf = cep.uf;
  }

  private atualizarVoluntarioComUsuarioInstituto(usuarioInstituto: UsuarioInstituto) {
    this.voluntario.cpf = usuarioInstituto.cpf;
    this.voluntario.nome = usuarioInstituto.nome;
    this.voluntario.email = usuarioInstituto.email;
    this.voluntario.voluntarioTipo = VoluntarioTipo.Sicoob;
    this.voluntario.numeroCooperativa = usuarioInstituto.numeroCooperativa;
  }

  /**
   * Se o usuário for novo ou, um usuário já cadastrado que está atualizando os dados, e alterou o arquivo pertinente ao
   * certificado do curso de voluntariado, deve passar pelo processo de envio do certificado para a GED; caso seja um
   * usuário já cadastrado, que já possui o arquivo no GED e não o alterou, segue apenas com a atualização dos dados.
   */
  private criarAtualizarVoluntario() {
    if (this.arrayFileData) {
      this.voluntario.situacao = SituacaoVoluntarioType.aguardandoAnalise;
      this.voluntario.dataNascimento = this.getDataNascimento(this.voluntario.dataNascimento);
      this.enviarCertificadoGed(!this.voluntario.id);
    } else {
      this.atualizarVoluntario();
    }
  }

  private getDateAsString(dataNascimento: Date): string {
    return new Date(dataNascimento).toLocaleDateString('pt-BR');
  }

  private getDataNascimento(dataNascimento: any): any {
    const dataString = dataNascimento;
    if (dataString.length === 8) {
      const resultString: string =
        dataString.substring(4, 8) + '-' +
        dataString.substring(2, 4) + '-' +
        dataString.substring(0, 2) + 'T00:00:00';
      return new Date(resultString);
    } else {
      const resultString: string =
        dataString.substring(6, 10) + '-' +
        dataString.substring(3, 5) + '-' +
        dataString.substring(0, 2) + 'T00:00:00';
      return new Date(resultString);
    }
  }


  apresentarCorrecoes() {
    this.popoverAparente = !this.popoverAparente;
  }

  fecharPopover() {
    this.popoverAparente = false;
  }

}
