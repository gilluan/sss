import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { Token } from '../../models/token.model';
import { TokenResult } from '../../models/token-result.model';
import { Service } from 'src/app/shared/services/service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TokenService extends Service {
  private RESOURCE_TOKEN = 'tokens';
  private RESOURCE_URL_TOKEN =  `${environment.modulo_gestao_pessoa}/${this.RESOURCE_TOKEN}`;

  constructor(private http: HttpClient) { super()}

  registerToken(token: Token): Observable<TokenResult> {
    return this.http.post<TokenResult>(this.RESOURCE_URL_TOKEN, token, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  validateToken(token: Token): Observable<TokenResult> {
    let VALIDATE_URL = `${this.RESOURCE_URL_TOKEN}/${token.token}/validar/${token.cpf}`;
    return this.http.patch<TokenResult>(VALIDATE_URL, token, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }




}
