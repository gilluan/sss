import { Component, OnInit, Inject } from '@angular/core';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';

@Component({
  selector: 'app-dialogo-correcoes',
  templateUrl: './dialogo-correcoes.component.html',
  styleUrls: ['./dialogo-correcoes.component.css']
})
export class DialogoCorrecoesComponent implements OnInit {
  title = 'Atenção';

  constructor(public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit() {
  }

  public closeModal() {
    this.ref.close();
  }

}
