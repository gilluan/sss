import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogoCorrecoesComponent } from './dialogo-correcoes.component';

describe('DialogoCorrecoesComponent', () => {
  let component: DialogoCorrecoesComponent;
  let fixture: ComponentFixture<DialogoCorrecoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogoCorrecoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogoCorrecoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
