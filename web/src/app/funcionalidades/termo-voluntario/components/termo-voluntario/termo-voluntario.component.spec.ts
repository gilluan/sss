import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermoVoluntarioComponent } from './termo-voluntario.component';

describe('TermoVoluntarioComponent', () => {
  let component: TermoVoluntarioComponent;
  let fixture: ComponentFixture<TermoVoluntarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermoVoluntarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermoVoluntarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
