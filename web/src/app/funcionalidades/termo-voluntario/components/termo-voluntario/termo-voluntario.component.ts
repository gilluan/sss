import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SituacaoVoluntarioType } from '@app/funcionalidades/sins-analise-cadastros/models/types/situacao-voluntario.type';
import { VoluntariosService } from '@app/funcionalidades/sins-analise-cadastros/voluntarios.service';
import { VoluntarioDto } from '@app/shared/models/voluntario-dto.model';
import { select, Store } from '@ngrx/store';
import { Color } from '@sicoob/ui';
import { Subscription } from 'rxjs';
import { DisplayToolbar } from 'src/app/app.actions';
import { CustomAlertService } from 'src/app/shared/services/alert-service';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { UsuarioInstitutoService } from 'src/app/shared/services/usuario-instituto.service';
import * as fromApp from '../../../../app.reduce';
import { LoadTermoVoluntarios } from '../../actions/termo-voluntario.actions';
import * as fromTermoVoluntario from '../../reducers/termo-voluntario.reducer';




@Component({
  selector: 'sc-termo-voluntario',
  templateUrl: './termo-voluntario.component.html',
  styleUrls: ['./termo-voluntario.component.css']
})
export class TermoVoluntarioComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private alertService: CustomAlertService,
    private store: Store<fromTermoVoluntario.State>,
    public appStore$: Store<fromApp.State>,
    private persistenceService: PersistanceService,
    private usuarioInstitutoService: UsuarioInstitutoService,
    private voluntarioService: VoluntariosService,
    private changeDetector: ChangeDetectorRef
  ) { }

  checkTermoValue: boolean;
  cpf: string;
  nome: string;
  email: string;
  perfil: string;
  situacao: string;
  public editor: any;
  public editorContent: string;
  public editorConfig = {
    readOnly: true,
    theme: 'bubble',
    placeholder: "inserir um texto...",
    modules: {
      toolbar: [
        ['bold']
      ]
    }
  };

  onEditorCreated(quill) {
    this.editor = quill;
  }

  private verificarTermoAceito(): boolean {
    return this.checkTermoValue;
  }

  sub: Subscription;

  navigatePaginaDadosCadastrais() {
    if (!this.verificarTermoAceito()) {
      this.alertService.abrirAlert(Color.DANGER, "Você precisa aceitar os termos antes de avançar.", 5000);
    } else {
      if (this.perfil === "voluntario") {
        if (this.situacao != SituacaoVoluntarioType.aguardandoAssinaturaTermo) {
          this.router.navigate(['/welcome/dados-cadastrais']);
        } else {
          this.sub = this.voluntarioService.concordarTermo(this.voluntario).subscribe(
            _ => this.router.navigate(['/inicio'])
          )
        }
      } else {
        this.usuarioInstitutoService.signByCpf(this.cpf).subscribe(found => {
          this.router.navigate(['/inicio']);
        });
      }
    }
  }

  cancelTermo() {
    if (this.perfil === "voluntario") {
      this.router.navigate(['/welcome']);
    } else {
      var win = window.open("http://www.institutosicoob.org.br/", "_self");
      win.close();
    }
  }

  voluntario: VoluntarioDto;

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(false));
    let userInstituto = this.persistenceService.get("usuario_instituto");
    this.cpf = userInstituto.cpf;
    this.nome = userInstituto.nome;
    this.email = userInstituto.email;
    this.perfil = userInstituto.perfil;
    this.situacao = userInstituto.situacao;
    this.store.dispatch(new LoadTermoVoluntarios(userInstituto.perfil));
    this.store.pipe(select(fromTermoVoluntario.getFirst)).subscribe(
      t => { this.editorContent = t ? t.textoTermo : ''; this.changeDetector.detectChanges() }
    );
    this.voluntarioService.findVoluntarioByCpf(this.cpf).subscribe(voluntarioExistente => {
      if (voluntarioExistente) {
        this.voluntario = voluntarioExistente;
      }
    });
  }

  isPaeOuPde(): boolean {
    if (this.perfil == 'pae' || this.perfil == 'pde') return true;
    return false;
  }

}
