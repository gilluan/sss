import { Injectable } from '@angular/core';
import { ConfiguracaoTermoService } from '@app/funcionalidades/configuracao-termo/configuracao-termo.service';
import { TermoCompromisso } from '@app/funcionalidades/configuracao-termo/models/termo-compromisso.model';
import { DialogoSucessoComponent } from '@app/shared/components/dialogo-sucesso/dialogo-sucesso.component';
import { Actions, Effect } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { ModalService, ModalRef } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, mergeMap, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AbrirModal, LoadTermoVoluntarios, LoadTermoVoluntariosFail, LoadTermoVoluntariosSuccess, TermoVoluntarioActionTypes, AddModal, FecharModal } from '../actions/termo-voluntario.actions';
import * as fromTermoVoluntario from '../reducers/termo-voluntario.reducer';


@Injectable()
export class TermoVoluntarioEffects {

  constructor(private actions$: Actions,
    private termoService: ConfiguracaoTermoService,
    private modalService: ModalService,
    private store$: Store<fromTermoVoluntario.State>,
) { }


  @Effect()
  create$ =
    this.actions$
      .ofType(TermoVoluntarioActionTypes.LoadTermoVoluntarios).pipe(
        map((action: LoadTermoVoluntarios) => action.tipo),
        switchMap((tipo: string) =>  this.termoService.findTermoCompromisso(tipo).pipe(
          map((termoCompromisso: TermoCompromisso) => new LoadTermoVoluntariosSuccess(termoCompromisso)),
          catchError(error => of(new LoadTermoVoluntariosFail(error)))
        )
        ));


  @Effect()
  abrirModal$ =
    this.actions$
      .ofType(TermoVoluntarioActionTypes.AbrirModal).pipe(
        withLatestFrom(this.store$.pipe(select(fromTermoVoluntario.getModal))),
        withLatestFrom(this.store$.pipe(select(fromTermoVoluntario.getModalRef))),
        mergeMap(([[_, data], modalRef]: [[AbrirModal,  fromTermoVoluntario.ModalSuccessProp], ModalRef]) => {
          if(modalRef) modalRef.close();
          modalRef = this.modalService.open(DialogoSucessoComponent, { data: data, panelClass: 'sins-modal-success' });
          return of(new AddModal(modalRef))
        })
      );

      @Effect()
      fecharModal$ =
        this.actions$.ofType(TermoVoluntarioActionTypes.FecharModal).pipe(
          withLatestFrom(this.store$.pipe(select(fromTermoVoluntario.getModalRef))),
          mergeMap(([_, modalRef]: [FecharModal, ModalRef]) => {
            if (modalRef)
              modalRef.close();
            return of({type: '[FecharModalSuccess] Termo Voluntario'});
          })
        );

}
