import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { TermoVoluntarioEffects } from './termo-voluntario.effects';

describe('TermoVoluntarioEffects', () => {
  let actions$: Observable<any>;
  let effects: TermoVoluntarioEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TermoVoluntarioEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(TermoVoluntarioEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
