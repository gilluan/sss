import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContainerSinsHomeComponent } from './containers/container-sins-home/container-sins-home.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerSinsHomeComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinsHomeRoutingModule { }
