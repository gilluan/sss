import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { SinsHome } from '../models/sins-home.model';

export enum SinsHomeActionTypes {
  LoadSinsHomes = '[SinsHome] Load SinsHomes',
  AddSinsHome = '[SinsHome] Add SinsHome',
  UpsertSinsHome = '[SinsHome] Upsert SinsHome',
  AddSinsHomes = '[SinsHome] Add SinsHomes',
  UpsertSinsHomes = '[SinsHome] Upsert SinsHomes',
  UpdateSinsHome = '[SinsHome] Update SinsHome',
  UpdateSinsHomes = '[SinsHome] Update SinsHomes',
  DeleteSinsHome = '[SinsHome] Delete SinsHome',
  DeleteSinsHomes = '[SinsHome] Delete SinsHomes',
  ClearSinsHomes = '[SinsHome] Clear SinsHomes'
}

export class LoadSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.LoadSinsHomes;

  constructor(public payload: { sinsHomes: SinsHome[] }) {}
}

export class AddSinsHome implements Action {
  readonly type = SinsHomeActionTypes.AddSinsHome;

  constructor(public payload: { sinsHome: SinsHome }) {}
}

export class UpsertSinsHome implements Action {
  readonly type = SinsHomeActionTypes.UpsertSinsHome;

  constructor(public payload: { sinsHome: SinsHome }) {}
}

export class AddSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.AddSinsHomes;

  constructor(public payload: { sinsHomes: SinsHome[] }) {}
}

export class UpsertSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.UpsertSinsHomes;

  constructor(public payload: { sinsHomes: SinsHome[] }) {}
}

export class UpdateSinsHome implements Action {
  readonly type = SinsHomeActionTypes.UpdateSinsHome;

  constructor(public payload: { sinsHome: Update<SinsHome> }) {}
}

export class UpdateSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.UpdateSinsHomes;

  constructor(public payload: { sinsHomes: Update<SinsHome>[] }) {}
}

export class DeleteSinsHome implements Action {
  readonly type = SinsHomeActionTypes.DeleteSinsHome;

  constructor(public payload: { id: string }) {}
}

export class DeleteSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.DeleteSinsHomes;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearSinsHomes implements Action {
  readonly type = SinsHomeActionTypes.ClearSinsHomes;
}

export type SinsHomeActions =
 LoadSinsHomes
 | AddSinsHome
 | UpsertSinsHome
 | AddSinsHomes
 | UpsertSinsHomes
 | UpdateSinsHome
 | UpdateSinsHomes
 | DeleteSinsHome
 | DeleteSinsHomes
 | ClearSinsHomes;
