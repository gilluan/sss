import { TestBed } from '@angular/core/testing';

import { SinsHomeService } from './sins-home.service';

describe('SinsHomeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SinsHomeService = TestBed.get(SinsHomeService);
    expect(service).toBeTruthy();
  });
});
