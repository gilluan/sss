import { Store, select } from '@ngrx/store';
import { CarregarTotalPorFiltro, CarregarTotalBancoVoluntario } from 'src/app/actions/voluntarioAction';
import { VoluntarioFiltroDto } from 'src/app/shared/models/voluntario-filtro.model';
import { SituacaoVoluntarioType } from 'src/app/shared/types/situacao-voluntario.type';
import * as fromVoluntario from '../../../../reducers/voluntario.reducer';
import { CardHome } from './card-home.model';
import { UsuarioInstituto } from 'src/app/shared/models/usuario-instituto.model';
import { ChangeDetectorRef } from '@angular/core';

export class CardBancoVoluntarios extends CardHome  {


    constructor(public changeDetector: ChangeDetectorRef, public store: Store<fromVoluntario.State>, user: any) {
        super(changeDetector, store, fromVoluntario.getTotalBancoVoluntario);
        this.id = 'card-voluntarios';
        this.icone = 'mdi-account-group';
        this.texto = 'Banco de voluntários';
        this.rota = 'relatorios/banco-voluntarios';
        this.rotaUrl = this.rota;
        this.store.dispatch(new CarregarTotalBancoVoluntario(this.filtro(user)));
        this.getTextoSmall('voluntário');
    }

    private filtro(user: any) {
        const voluntarioFiltro = new VoluntarioFiltroDto();
        voluntarioFiltro.situacao = SituacaoVoluntarioType.assinado;
        voluntarioFiltro.ativo = true;
        const usuarioInstituto = Object.assign(new UsuarioInstituto(), user);
        /* if (usuarioInstituto.isPaeOuPde()) {
            voluntarioFiltro.cooperativas = [];
            voluntarioFiltro.cooperativas.push(usuarioInstituto.numeroCooperativa);
        } */
        return voluntarioFiltro;
    }


}
