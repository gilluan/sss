import { CardHome } from './card-home.model';

export class CardMarcos extends CardHome {

  constructor() {
    super(null, null, null);
    this.id = 'card-marcos';
    this.icone = 'mdi-cards-variant';
    this.texto = 'Meus Marcos';
    this.rota = 'marcos';
    this.rotaUrl = 'marcos';
  }
}
