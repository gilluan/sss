import { CardHome } from './card-home.model';

export class CardPerfilUsuario extends CardHome {

  constructor(public id: string) {
    super(null, null, null);
    this.id = 'card-perfil-usuario';
    this.icone = 'mdi-account-tie';
    this.texto = 'Meu Perfil';
    this.rota = 'perfil-usuario/' + id;
    this.rotaUrl = 'perfil-usuario/:id';
  }
}
