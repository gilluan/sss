import { CardHome } from './card-home.model';
import { ChangeDetectorRef } from '@angular/core';
import * as fromProgramas from '@app/funcionalidades/programas/reducers/programas.reducer';
import { Store } from '@ngrx/store';

export class CardProgramas extends CardHome {

  constructor(public changeDetector: ChangeDetectorRef, public programaStore$: Store<fromProgramas.State>) {
    super(null, null, null);
    this.id = 'card-programas';
    this.icone = 'mdi-account-tie';
    this.texto = 'Programas';
    this.rota = 'programas';
    this.rotaUrl = 'programas';
  }
}