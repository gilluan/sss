import { ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { CarregarTotalPorFiltro } from 'src/app/actions/voluntarioAction';
import { VoluntarioFiltroDto } from 'src/app/shared/models/voluntario-filtro.model';
import { SituacaoVoluntarioType } from 'src/app/shared/types/situacao-voluntario.type';
import * as fromVoluntario from '../../../../reducers/voluntario.reducer';
import { CardHome } from './card-home.model';

export class CardAnaliseVoluntarios extends CardHome {

    constructor(public changeDetector: ChangeDetectorRef, public store: Store<fromVoluntario.State>) {
        super(changeDetector, store, fromVoluntario.selectTotalPorFiltro);
        this.id = 'card-assinar-voluntario';
        this.icone = 'mdi-pen';
        this.texto = 'Assinar Voluntários';
        this.rota = 'analise-cadastros/assinatura';
        this.textoSmall = 'candidato(s)';
        this.rotaUrl = this.rota;
        this.store.dispatch(new CarregarTotalPorFiltro(this.filtro()));
        this.getTextoSmall('candidato');
    }

    private filtro() {
        const voluntarioFiltro: VoluntarioFiltroDto = new VoluntarioFiltroDto();
        voluntarioFiltro.situacao = SituacaoVoluntarioType.aguardandoAssinatura;
        voluntarioFiltro.ativo = true;
        return voluntarioFiltro;
    }
}
