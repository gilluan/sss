import { CardHome } from "./card-home.model";
import { ChangeDetectorRef } from "@angular/core";
import * as fromPortfolio from '@app/funcionalidades/portfolio/reducers/portfolio.reducer';
import { Store } from "@ngrx/store";

export class CardPortfolio extends CardHome {

    constructor(public changeDetector: ChangeDetectorRef, public portfolioStore$: Store<fromPortfolio.State>) {
      super(null, null, null);
      this.id = 'card-portfolio';
      this.icone = 'mdi-book';
      this.texto = 'Eixos de atuação';
      this.rota = 'portfolios';
      this.rotaUrl = 'portfolios';
    }
  }
