import {CardHome} from './card-home.model';
import {ChangeDetectorRef} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromFluxoProcesso from 'src/app/funcionalidades/fluxo-processo/reducers/fluxo-processo.reducer';
import {LoadTotalsFluxoProcessoList} from '@app/funcionalidades/fluxo-processo/actions/fluxo-processo.actions';
import {FluxoProcessoFilter} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo-filter.model';

export class CardFluxoProcesso extends CardHome {

  constructor(public changeDetector: ChangeDetectorRef, public store: Store<fromFluxoProcesso.State>) {
    super(changeDetector, store, fromFluxoProcesso.selectTotalsAll);
    this.id = 'card-plano-acao';
    this.icone = 'mdi-source-fork';
    this.texto = 'Criar novo fluxo de processo';
    this.rota = 'fluxo-processo';
    this.rotaUrl = this.rota;
    this.store.dispatch(new LoadTotalsFluxoProcessoList(new FluxoProcessoFilter()));
    this.getTextoSmall('fluxo de processo', 'fluxos de processo');
  }
}
