import {CardHome} from './card-home.model';
import {Store} from '@ngrx/store';
import * as fromProjetos from 'src/app/funcionalidades/projeto/reducers/projeto.reducer';
import {ChangeDetectorRef} from '@angular/core';
import { TotalProjetos } from '@app/funcionalidades/projeto/actions/projeto.actions';
import { ProjetoFiltro } from '@app/funcionalidades/projeto/models/projeto.filtro';

export class CardProjetos extends CardHome {

    constructor(public changeDetector: ChangeDetectorRef, public store: Store<fromProjetos.State>, numeroCooperativa: string) {
        super(changeDetector, store, fromProjetos.getTotalProjetos);
        this.id = 'card-projetos';
        this.icone = 'mdi-clipboard-text';
        this.texto = 'Projetos';
        this.rota = 'projetos';
        this.rotaUrl = this.rota;
        this.store.dispatch(new TotalProjetos(new ProjetoFiltro(null, 'concluido')));
        this.getTextoSmall('ativo');
    }
}
