import { Observable } from 'rxjs';
import { Store, MemoizedSelector, select, State } from '@ngrx/store';
import { ChangeDetectorRef } from '@angular/core';

export class CardHome {

  constructor(
    public changeDetector: ChangeDetectorRef,
    public store: Store<any>,
    public selector: MemoizedSelector<any, any>,
    public id?: string,
    public icone?: string,
    public texto?: string,
    public rota?: string,
    public rotaUrl?: string,
    public textoSmall?: string,
    public observable?: Observable<any>
  ) { }

  public getTextoSmall(labelSingular: string, labelPlural: string = `${labelSingular}s`) {
    this.store.pipe(select(this.selector)).subscribe(
      (total: number) => {
        this.textoSmall = `${total} ${total > 1 ? labelPlural : labelSingular}`;
        this.changeDetector.detectChanges();
      }
    );
  }
}
