import {CardHome} from './card-home.model';
import {Store} from '@ngrx/store';
import * as fromPlanoAcao from 'src/app/funcionalidades/plano-acao/reducers/plano-acao.reducer';
import {TotalPlanosAcao} from 'src/app/funcionalidades/plano-acao/actions/plano-acao.actions';
import {ChangeDetectorRef} from '@angular/core';
import {PerfilType} from '@shared/types/perfil.type';

export class CardPlanosAcao extends CardHome {

    constructor(public changeDetector: ChangeDetectorRef,
        public store: Store<fromPlanoAcao.State>,
        usuarioInstituto: string) {
        super(changeDetector, store, fromPlanoAcao.getTotalPlanosAcao);
        this.id = 'card-plano-acao';
        this.icone = 'mdi-ballot-outline';
        this.texto = 'Plano de ação';
        this.rota = 'plano-acao';
        this.rotaUrl = this.rota;
        this.store.dispatch(new TotalPlanosAcao(null, null, this.filtroPerfil(usuarioInstituto)));
        this.getTextoSmall('ativo');
    }

    public filtroPerfil(usuarioInstituto: any){
        if(usuarioInstituto.perfil === PerfilType.gestor || usuarioInstituto.perfil === PerfilType.ppe){
            return null;
        }else{
            return usuarioInstituto.numeroCooperativa
        }
    }
}
