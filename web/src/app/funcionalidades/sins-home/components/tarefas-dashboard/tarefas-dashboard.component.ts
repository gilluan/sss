import {Component, Input, OnInit} from '@angular/core';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';

@Component({
  selector: 'sc-tarefas-dashboard',
  templateUrl: './tarefas-dashboard.component.html',
  styleUrls: ['./tarefas-dashboard.component.scss']
})
export class TarefasDashboardComponent implements OnInit {

  @Input() set scTotaisPassosSituacao(value) {
    if (value) {
      this.totais = value;
    }
  }

  totais: TotaisPassoSituacao = TotaisPassoSituacao.initialState();

  constructor() { }

  ngOnInit(): void { }

}
