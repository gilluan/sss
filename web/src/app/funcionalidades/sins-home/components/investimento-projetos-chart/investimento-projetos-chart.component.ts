import {Component, Input, OnInit} from '@angular/core';
import {ApexChart, ApexNonAxisChartSeries, ApexPlotOptions} from 'ng-apexcharts';
import {ApexTitleSubtitle} from 'ng-apexcharts/lib/model/apex-types';
import {TotaisOrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';

@Component({
  selector: 'sc-investimento-projetos-chart[scTamanho][scTotaisInvestimentoProjeto]',
  templateUrl: 'investimento-projetos-chart.component.html',
  styleUrls: ['./investimento-projetos-chart.component.scss']
})
export class InvestimentoProjetosChartComponent implements OnInit {

  @Input() set scTotaisInvestimentoProjeto(value) {
    if (value) {
      this.totaisInvestimentoProjeto = value;
      this.atualizarValoresChart(value);
    }
  }

  totaisInvestimentoProjeto = new TotaisOrcamentoInvestimentoProjeto();
  
  /* Aceita '100px' ou 100 */
  @Input() scTamanho: string | number;

  chart: ApexChart;
  title: ApexTitleSubtitle;
  series: ApexNonAxisChartSeries;
  plotOptions: ApexPlotOptions;

  constructor() { }

  ngOnInit(): void {
    this.chart = {
      height: this.scTamanho,
      type: 'radialBar',
      toolbar: {
        show: false
      }
    };

    this.series = [ 0 ];

    this.plotOptions = {
      radialBar: {
        dataLabels: {
          name: {
            fontSize: '16px'
          },
          value: {
            offsetY: -10,
            fontSize: '22px'
          }
        },
        hollow: {
          size: '60%'
        }
      }
    };

    this.atualizarValoresChart(this.scTotaisInvestimentoProjeto);
  }

  private atualizarValoresChart(totais?: TotaisOrcamentoInvestimentoProjeto): void {
    if (totais) {
      this.series = [ this.porcentagemInvestido(totais.investimento, totais.orcamento) ];
    }
  }

  private porcentagemInvestido(investido: number, estimado: number) {
    const procentagemInvestido = Math.ceil((investido * 100) / estimado);
    return Number.isNaN(procentagemInvestido) ? 0 : procentagemInvestido;
  }
}
