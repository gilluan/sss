import {Component, Input, OnInit} from '@angular/core';
import {ApexChart, ApexLegend, ApexNonAxisChartSeries, ApexPlotOptions} from 'ng-apexcharts';
import {ApexTitleSubtitle} from 'ng-apexcharts/lib/model/apex-types';
import {TotalProjetosSituacao} from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';

const VALORES_DEFAULT = [0, 0, 0];

@Component({
  selector: 'sc-contador-projetos-chart',
  template: `
    <apx-chart
      [chart]="chart"
      [series]="series"
      [title]="title"
      [colors]="color"
      [legend]="legend"
      [labels]="labels" [dataLabels]="{ enabled: false }"
      [plotOptions]="plotOptions"></apx-chart>
  `,
  styleUrls: ['./contador-projetos-chart.component.scss']
})
export class ContadorProjetosChartComponent implements OnInit {

  @Input() scTitulo: string;
  @Input() scLargura = '100%';
  @Input() scAltura = '100%';

  totalProjetosSituacao: TotalProjetosSituacao;

  @Input() set scTotalProjetosSituacao(value) {
    if (value) {
      this.totalProjetosSituacao = value;
      this.atualizarSeries(value);
    }
  }

  chart: ApexChart;
  title: ApexTitleSubtitle;
  series: ApexNonAxisChartSeries;
  legend: ApexLegend;
  plotOptions: ApexPlotOptions;
  labels: string[];
  color: string[];

  constructor() { }

  ngOnInit() {
    this.chart = {
      width: this.scLargura,
      height: this.scAltura,
      type: 'donut',
    };

    this.title = {
      text: this.scTitulo,
      align: 'left',
      offsetX: 30,
      style: { fontSize: '16px' },
    };

    this.labels = ['Não iniciados', 'Iniciados', 'Concluídos'];

    this.color = [ '#003641', '#596ADA', '#00A190' ];

    this.legend = {
      position: 'right',
      horizontalAlign: 'right',
      fontSize: '14px',
      offsetY: 75,
      labels: {
        useSeriesColors: true
      }
    };

    this.plotOptions = {
      pie: {
        donut: {
          labels: {
            show: true,
            total: {
              show: true,
              color: '#000'
            }
          }
        }
      }
    };

    this.atualizarSeries(this.totalProjetosSituacao);
  }

  private atualizarSeries(total: TotalProjetosSituacao) {
    if (total) {
      this.series = [ total.naoIniciados, total.iniciados, total.concluidos ];
    } else {
      this.series = VALORES_DEFAULT;
    }
  }
}
