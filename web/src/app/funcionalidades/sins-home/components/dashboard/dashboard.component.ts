import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';
import {OrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import {TotaisOrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import {TotalProjetosSituacao} from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';

@Component({
  selector: 'sc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  @Input() scTotaisTarefas: TotaisPassoSituacao;
  @Input() scInvestimentosProjeto: OrcamentoInvestimentoProjeto[];
  @Input() scTotaisInvestimentoProjetos: TotaisOrcamentoInvestimentoProjeto;
  @Input() scTotalProjetosSituacao: TotalProjetosSituacao;

  anosVigencia: string[] = [];

  @Input() set scAnosVigencia(value) {
    if (value) {
      this.anosVigencia = value;
      this.patchForm(value);
    }
  }

  @Output() scAtualizar: EventEmitter<string> = new EventEmitter();
  @Output() scDetalharProjetos: EventEmitter<any> = new EventEmitter();

  dadosProjetosForm = this.fb.group({
    anoVigencia: ['', Validators.required]
  });

  get anoVigencia(): FormControl {
    return this.dadosProjetosForm.get('anoVigencia') as FormControl;
  }

  private subscription: Subscription = new Subscription();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.subscription
      .add(this.anoVigencia.valueChanges.subscribe(vigencia => this.scAtualizar.emit(vigencia)));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  detalharProjetos(): void {
    this.scDetalharProjetos.emit();
  }

  private patchForm(vigencias?: string[]): void {
    if (vigencias && vigencias.length > 0) {
      this.dadosProjetosForm.patchValue({ anoVigencia: this.getAnoVigente(vigencias) });
    }
  }

  // TODO melhorar a lógica
  private getAnoVigente(vigencias?: string[]): string {
    if (vigencias && vigencias.length > 0) {
      const anoCorrente = new Date().getFullYear().toString();
      const anoVigente = vigencias.find(ano => ano === anoCorrente);
      return anoVigente ? anoVigente : vigencias[0];
    } else {
      return null;
    }
  }
}
