import { Component, Input, OnInit } from '@angular/core';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { CardHome } from '../../models/cards/card-home.model';

@Component({
  selector: 'sc-sins-home',
  templateUrl: './sins-home.component.html',
  styleUrls: ['./sins-home.component.css']
})
export class SinsHomeComponent implements OnInit {

  nomeUsuario: string;

  constructor(
    private persistenceService: PersistanceService,
  ) { }

  @Input() cards: Array<CardHome>;

  ngOnInit() {
    const userInstituto = this.persistenceService.get('usuario_instituto');
    this.nomeUsuario = userInstituto.nome;
  }
}
