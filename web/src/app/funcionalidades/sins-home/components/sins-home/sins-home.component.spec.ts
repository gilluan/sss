import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinsHomeComponent } from './sins-home.component';

describe('SinsHomeComponent', () => {
  let component: SinsHomeComponent;
  let fixture: ComponentFixture<SinsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
