import {Component, Input, OnInit} from '@angular/core';
import {ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexPlotOptions, ApexTooltip, ApexXAxis} from 'ng-apexcharts';
import {ApexTitleSubtitle} from 'ng-apexcharts/lib/model/apex-types';
import {OrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';

const VALORES_DEFAULT = [0, 0, 0, 0, 0];
const CATEGORIAS_DEFAULT = ['', '', '', '', ''];

@Component({
  selector: 'sc-orcamento-projetos-chart[scOrcamentoInvestimentoProjeto]',
  template: `
    <apx-chart
      [chart]="chart"
      [series]="series"
      [title]="title"
      [xaxis]="xaxis"
      [dataLabels]="dataLabels"
      [tooltip]="tooltip"
      [colors]="colors"
      [plotOptions]="plotOptions"></apx-chart>
  `,
  styleUrls: ['./orcamento-projetos-chart.component.scss']
})
export class OrcamentoProjetosChartComponent implements OnInit {

  @Input() scTitulo: string;
  @Input() scLargura = '100%';
  @Input() scAltura = '100%';

  orcamentoInvestimentoProjeto: OrcamentoInvestimentoProjeto[];

  @Input() set scOrcamentoInvestimentoProjeto(value) {
    if (value) {
      this.orcamentoInvestimentoProjeto = value;
      this.atualizarInvestimentosChart(value);
    }
  }

  chart: ApexChart;
  title: ApexTitleSubtitle;
  series: ApexAxisChartSeries;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
  colors: ['#596ADA', '#00A190'];

  constructor() { }

  ngOnInit(): void {
    this.chart = {
      height: this.scAltura,
      width: this.scLargura,
      type: 'bar',
      offsetY: -30,
      toolbar: {
        show: false
      }
    };

    this.title = {
      text: this.scTitulo,
      align: 'center',
      offsetY: 30,
      style: { fontSize: '16px' },
    };

    this.dataLabels = {
      enabled: true,
      offsetX: -20,
      formatter(val: number): string {
        return `R$ ${val}`;
      },
      style: {
        colors: [ '#fff' ]
      }
    };

    this.plotOptions = {
      bar: {
        horizontal: true,
        endingShape: 'rounded',
        dataLabels: {
          position: 'top'
        }
      }
    };

    this.tooltip = {
      theme: 'dark',
      x: { show: false },
      y: {
        formatter(val: number, opts?: any): string {
          return `R$ ${val}`;
        },
        title: {
          formatter(seriesName: string): string {
            return '';
          }
        },
      }
    };

    this.atualizarInvestimentosChart(this.orcamentoInvestimentoProjeto);
  }

  private atualizarInvestimentosChart(investimentos: OrcamentoInvestimentoProjeto[]) {
    if (investimentos && investimentos.length > 0) {
      this.atualizarSeries(
        investimentos.map(investimento => investimento.investimento),
        investimentos.map(investimento => investimento.orcamento));
      this.atualizarCategoria(investimentos.map( investimento => investimento.nome));
    } else {
      this.atualizarSeries(VALORES_DEFAULT, VALORES_DEFAULT);
      this.atualizarCategoria(CATEGORIAS_DEFAULT);
    }
  }

  private atualizarSeries(investido: number[], orcado: number[]): void {
    this.series = [
      { name: 'Investido', data: investido },
      { name: 'Orçado', data: orcado }
    ];
  }

  private atualizarCategoria(categorias: string[]): void {
    this.xaxis = {
      categories: categorias,
      labels: {
        formatter(value: string): string {
          return `R$ ${value}`;
        }
      }
    };
  }
}
