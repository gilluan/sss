import { Injectable } from '@angular/core';
import { Service } from 'src/app/shared/services/service';

@Injectable({
  providedIn: 'root'
})
export class SinsHomeService extends Service{

  constructor() { super() }
}
