import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { SinsHome } from '../models/sins-home.model';
import { SinsHomeActions, SinsHomeActionTypes } from '../actions/sins-home.actions';

export interface State extends EntityState<SinsHome> {
  // additional entities state properties
}

export const adapter: EntityAdapter<SinsHome> = createEntityAdapter<SinsHome>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: SinsHomeActions
): State {
  switch (action.type) {
    case SinsHomeActionTypes.AddSinsHome: {
      return adapter.addOne(action.payload.sinsHome, state);
    }

    case SinsHomeActionTypes.UpsertSinsHome: {
      return adapter.upsertOne(action.payload.sinsHome, state);
    }

    case SinsHomeActionTypes.AddSinsHomes: {
      return adapter.addMany(action.payload.sinsHomes, state);
    }

    case SinsHomeActionTypes.UpsertSinsHomes: {
      return adapter.upsertMany(action.payload.sinsHomes, state);
    }

    case SinsHomeActionTypes.UpdateSinsHome: {
      return adapter.updateOne(action.payload.sinsHome, state);
    }

    case SinsHomeActionTypes.UpdateSinsHomes: {
      return adapter.updateMany(action.payload.sinsHomes, state);
    }

    case SinsHomeActionTypes.DeleteSinsHome: {
      return adapter.removeOne(action.payload.id, state);
    }

    case SinsHomeActionTypes.DeleteSinsHomes: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case SinsHomeActionTypes.LoadSinsHomes: {
      return adapter.addAll(action.payload.sinsHomes, state);
    }

    case SinsHomeActionTypes.ClearSinsHomes: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
