import {CommonModule} from '@angular/common';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AlertComponent, AlertModule, CardModule, FormModule} from '@sicoob/ui';
import {PersistanceService} from 'src/app/shared/services/persistence.service';
import {SharedModule} from '@shared/shared.module';
import * as fromFluxoProcesso from '../fluxo-processo/reducers/fluxo-processo.reducer';
import {PlanoAcaoService} from '../plano-acao/plano-acao.service';
import {SinsHomeComponent} from './components/sins-home/sins-home.component';
import {ContainerSinsHomeComponent} from './containers/container-sins-home/container-sins-home.component';
import {SinsHomeEffects} from './effects/sins-home.effects';
import * as fromSinsHome from './reducers/sins-home.reducer';
import {SinsHomeRoutingModule} from './sins-home-routing.module';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {NgxPermissionsModule} from 'ngx-permissions';
import {ContadorProjetosChartComponent} from './components/contador-projetos-chart/contador-projetos-chart.component';
import {NgApexchartsModule} from 'ng-apexcharts';
import {OrcamentoProjetosChartComponent} from './components/orcamento-projetos-chart/orcamento-projetos-chart.component';
import {ScSelectionListModule} from '@shared/components/sc-selection-list/sc-selection-list.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {TarefasDashboardComponent} from './components/tarefas-dashboard/tarefas-dashboard.component';
import {InvestimentoProjetosChartComponent} from './components/investimento-projetos-chart/investimento-projetos-chart.component';

@NgModule({
  declarations: [
    ContainerSinsHomeComponent,
    SinsHomeComponent,
    DashboardComponent,
    ContadorProjetosChartComponent,
    OrcamentoProjetosChartComponent,
    TarefasDashboardComponent,
    InvestimentoProjetosChartComponent
  ],
  imports: [
    NgxPermissionsModule.forChild(),
    SharedModule,
    AlertModule,
    CommonModule,
    FormModule,
    SinsHomeRoutingModule,
    StoreModule.forFeature('sinsHome', fromSinsHome.reducer),
    StoreModule.forFeature('fluxograma', fromFluxoProcesso.reducer),
    EffectsModule.forFeature([SinsHomeEffects]),
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    NgApexchartsModule,
    ScSelectionListModule,
    NgSelectModule
  ],
  providers: [PlanoAcaoService, PersistanceService],
  entryComponents: [AlertComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SinsHomeModule { }
