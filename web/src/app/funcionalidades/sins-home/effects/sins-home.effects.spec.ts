import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SinsHomeEffects } from './sins-home.effects';

describe('SinsHomeEffects', () => {
  let actions$: Observable<any>;
  let effects: SinsHomeEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SinsHomeEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(SinsHomeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
