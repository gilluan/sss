import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {DisplayToolbar} from '@app/app.actions';
import * as fromFluxoProcesso from '@app/funcionalidades/fluxo-processo/reducers/fluxo-processo.reducer';
import * as fromPlanoAcao from '@app/funcionalidades/plano-acao/reducers/plano-acao.reducer';
import * as fromPortfolio from '@app/funcionalidades/portfolio/reducers/portfolio.reducer';
import * as fromProgramas from '@app/funcionalidades/programas/reducers/programas.reducer';
import * as fromProjetos from '@app/funcionalidades/projeto/reducers/projeto.reducer';
import * as fromPassoFluxo from '@app/funcionalidades/fluxo-processo/reducers/passo-fluxo.reducer';
import {RoleFactory} from '@app/shared/controls/roles.model';
import {PersistanceService} from '@app/shared/services/persistence.service';
import {select, Store} from '@ngrx/store';
import * as fromApp from '../../../../app.reduce';
import * as fromVoluntario from '../../../../reducers/voluntario.reducer';
import {CardAssinaturaVoluntarios} from '../../models/cards/card-assinatura-voluntarios.model';
import {CardBancoVoluntarios} from '../../models/cards/card-banco-voluntarios.model';
import {CardHome} from '../../models/cards/card-home.model';
import {CardPerfilUsuario} from '../../models/cards/card-perfil-usuario.model';
import {CardPlanosAcao} from '../../models/cards/card-planos-acao.model';
import {Observable} from 'rxjs';
import {LoadTotaisPassosSituacao} from '@app/funcionalidades/fluxo-processo/actions/passo-fluxo.actions';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';
import {TotaisOrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import {CardPortfolio} from '../../models/cards/card-portfolio.model';
import {
  LoadInvestimentosProjeto,
  LoadTotaisInvestimento,
  LoadTotalProjetosSituacao,
  LoadVigenciasDisponiveis
} from '@app/funcionalidades/projeto/actions/projeto.actions';
import {OrcamentoInvestimentoProjeto} from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import {TotalProjetosSituacao} from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';
import {Router} from '@angular/router';
import {CardProjetos} from '../../models/cards/card-projetos.model';
import {PerfilType} from '@app/shared/types/perfil.type';

@Component({
  selector: 'sc-container-sins-home',
  templateUrl: './container-sins-home.component.html',
  styleUrls: ['./container-sins-home.component.scss']
})
export class ContainerSinsHomeComponent implements OnInit {

  usuarioInstituto: any;
  cards: Array<CardHome> = new Array<CardHome>();

  totaisInvestimentoProjeto$: Observable<TotaisOrcamentoInvestimentoProjeto>;
  orcamentoInvestimentosProjeto$: Observable<OrcamentoInvestimentoProjeto[]>;
  vigencias$: Observable<string[]>;
  totaisPassoSituacao$: Observable<TotaisPassoSituacao>;
  totalProjetosSituacao$: Observable<TotalProjetosSituacao>;

  popularCards = () => {
    const cards = new Array<CardHome>();
    cards.push(new CardAssinaturaVoluntarios(this.changeDetector, this.voluntarioStore$));
    cards.push(new CardBancoVoluntarios(this.changeDetector, this.voluntarioStore$, this.usuarioInstituto));
    cards.push(new CardPlanosAcao(this.changeDetector, this.planoAcaoStore$, this.usuarioInstituto));
    cards.push(new CardPerfilUsuario(this.usuarioInstituto.id));
    cards.push(new CardPortfolio(this.changeDetector, this.portfolioStore$));
    // cards.push(new CardFluxoProcesso(this.changeDetector, this.fluxoProcessoStore$));
    if (this.usuarioInstituto.perfil === PerfilType.ppe) {
      cards.push(new CardProjetos(this.changeDetector, this.projetosStore$, this.usuarioInstituto.numeroCooperativa));
    }
    const perfilControl = RoleFactory.getRole(this.usuarioInstituto.perfil);
    for (let i = 0; i < perfilControl.rotas.length; i++) {
      const rotaPermitida = perfilControl.rotas[i];
      for (let j = 0; j < cards.length; j++) {
        const card = cards[j];
        if (rotaPermitida == card.rotaUrl) {
          this.cards.push(card);
        }
      }
    }
  }

  constructor(
    public appStore$: Store<fromApp.State>,
    private readonly voluntarioStore$: Store<fromVoluntario.State>,
    private readonly planoAcaoStore$: Store<fromPlanoAcao.State>,
    private readonly projetosStore$: Store<fromProjetos.State>,
    private readonly fluxoProcessoStore$: Store<fromFluxoProcesso.State>,
    private readonly portfolioStore$: Store<fromPortfolio.State>,
    private readonly programaStore$: Store<fromProgramas.State>,
    private readonly passoFluxoStore$: Store<fromPassoFluxo.State>,
    private changeDetector: ChangeDetectorRef,
    private persistenceService: PersistanceService,
    private router: Router) { }

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.usuarioInstituto = this.persistenceService.get('usuario_instituto');
    this.popularCards();
    this.popularDashboard();
  }

  atualizarDashboard(vigencia: string): void {
    if (vigencia) {
      this.projetosStore$.dispatch(new LoadTotaisInvestimento(vigencia));
      this.projetosStore$.dispatch(new LoadInvestimentosProjeto(vigencia));
      this.projetosStore$.dispatch(new LoadTotalProjetosSituacao(vigencia));
    }
  }

  detalharProjetos(): void {
    this.router.navigate(['/projetos']);
  }

  private popularDashboard(): void {
    if (this.usuarioInstituto && this.usuarioInstituto.perfil.toLowerCase() === 'ppe') {
      this.totaisPassoSituacao$ = this.passoFluxoStore$.pipe(select(fromPassoFluxo.selectTotais));
      this.vigencias$ = this.projetosStore$.pipe(select(fromProjetos.selectVigencias));
      this.totaisInvestimentoProjeto$ = this.projetosStore$.pipe(select(fromProjetos.selectTotaisInvestimento));
      this.orcamentoInvestimentosProjeto$ = this.projetosStore$.pipe(select(fromProjetos.selectInvestimentosProjeto));
      this.totalProjetosSituacao$ = this.projetosStore$.pipe(select(fromProjetos.selectTotalProjetosSituacao));

      this.passoFluxoStore$.dispatch(new LoadTotaisPassosSituacao(this.usuarioInstituto.id));
      this.projetosStore$.dispatch(new LoadVigenciasDisponiveis());
    }
  }
}
