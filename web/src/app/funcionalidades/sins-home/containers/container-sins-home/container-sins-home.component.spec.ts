import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerSinsHomeComponent } from './container-sins-home.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerSinsHomeComponent', () => {
  let component: ContainerSinsHomeComponent;
  let fixture: ComponentFixture<ContainerSinsHomeComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerSinsHomeComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerSinsHomeComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
