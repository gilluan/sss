import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PageModelVm } from '@app/shared/models/page.model';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarService, Color } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { TotalSituacaoVm } from '../../sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { ContarAcoes, ContarAcoesFail, ContarAcoesSuccess, ContarExecutarAcoes, ContarExecutarAcoesFail, ContarExecutarAcoesSuccess, ContarPlanosAcao, ContarPlanosAcaoFail, ContarPlanosAcaoSuccess, CriarAcao, CriarAcaoESubTarefa, CriarAcaoESubTarefaSuccess, CriarAcaoFail, CriarAcaoSuccess, CriarAcaoSuccessClose, CriarPlanoAcao, CriarPlanoAcaoFail, CriarPlanoAcaoSuccess, ExcluirAcao, ExcluirAcaoFail, ExcluirAcaoSuccess, ExcluirPlanoAcao, ExcluirPlanoAcaoFail, ExcluirPlanoAcaoSuccess, ExecutarAcao, ExecutarAcaoFail, ExecutarAcaoSuccess, IniciarPlanoAcao, IniciarPlanoAcaoFail, LoadAcao, LoadAcaoFail, LoadAcaoSuccess, LoadAcoesExecutar, LoadAcoesExecutarSuccess, LoadAcoesPlano, LoadAcoesPlanoSuccess, LoadPlanoAcao, LoadPlanoAcaoFail, LoadPlanoAcaoSuccess, LoadPlanosAcao, LoadPlanosAcaoFail, LoadPlanosAcaoSuccess, PlanoAcaoActionTypes, TotalPlanosAcao, TotalPlanosAcaoFail, TotalPlanosAcaoSuccess, UpdateAcao, UpdateAcaoFail, UpdateAcaoSuccess, UpdatePlanoAcao, UpdatePlanoAcaoFail } from '../actions/plano-acao.actions';
import { ActionBarCadastroTarefaComponent } from '../components/action-bar-cadastro-tarefa/action-bar-cadastro-tarefa.component';
import { AcaoPaginada } from '../models/acao-paginada.model';
import { Acao } from '../models/acao.model';
import { PlanoAcao } from '../models/plano-acao.model';
import { PlanosPaginados } from '../models/planos-paginados.model';
import { PlanoAcaoService } from '../plano-acao.service';
import * as fromPlanoAcao from '../reducers/plano-acao.reducer';

@Injectable()
export class PlanoAcaoEffects {

  constructor(
    private persistenceService: PersistanceService,
    private store: Store<fromPlanoAcao.State>,
    private router: Router,
    private alertService: CustomAlertService,
    private actions$: Actions,
    private _planoAcaoService: PlanoAcaoService,
    private actionBarService: ActionBarService) { }

  @Effect()
  totalPlanosAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.TotalPlanosAcao).pipe(
        map((action: TotalPlanosAcao) => action),
        switchMap((action: TotalPlanosAcao) => this._planoAcaoService.contarPlanoAcao(null, null, action.numeroCooperativa).pipe(
          map((total: number) => new TotalPlanosAcaoSuccess(total)),
          catchError(error => of(new TotalPlanosAcaoFail(error))))));

  @Effect()
  createPlanoAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarPlanoAcao).pipe(
        map((action: CriarPlanoAcao) => action.planoAcao),
        switchMap((planoAcao: PlanoAcao) => this._planoAcaoService.criarPlanoAcao(planoAcao).pipe(
          map((planoAcao: PlanoAcao) => new CriarPlanoAcaoSuccess(planoAcao)),
          catchError(error => of(new CriarPlanoAcaoFail(error))))));

  @Effect()
  createPlanoAcaoFail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarPlanoAcaoFail).pipe(
        map((action: CriarPlanoAcaoFail) => action.erro),
        map((erro: any) => {
          this.alertService.abrirAlert(Color.DANGER, erro ? erro : 'Não foi possível cadastrar o plano de ação.');
          return new CriarAcaoSuccessClose();
        }
        ));

  @Effect()
  createAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarAcao).pipe(
        switchMap((criarAcao: CriarAcao) => this._planoAcaoService.criarAcao(criarAcao.acao).pipe(
          map((acao: Acao) => new CriarAcaoSuccess(acao, criarAcao.actionBarRef)),
          catchError(error => of(new CriarAcaoFail(error, criarAcao.actionBarRef))))));

  @Effect()
  updateAcao$ =
    this.actions$.pipe(
      ofType(PlanoAcaoActionTypes.UpdateAcao),
      withLatestFrom(this.store.pipe(select(fromPlanoAcao.getPaginacaoAtualAcoes))),
      mergeMap(([updateAcao, pageModelVm]: [UpdateAcao, PageModelVm]) =>
        this._planoAcaoService.updateAcao(updateAcao.action).pipe(
          map(acaoEditada => new UpdateAcaoSuccess(acaoEditada, updateAcao.actionBarRef)),
          catchError(error => of(new UpdateAcaoFail(error, updateAcao.actionBarRef))))));

  @Effect()
  updatePlanoAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.UpdatePlanoAcao).pipe(
        switchMap((updatePlanoAcao: UpdatePlanoAcao) => this._planoAcaoService.updatePlanoAcao(updatePlanoAcao.planoAcao).pipe(
          switchMap(planoAcao => this._planoAcaoService.loadPlanoAcao(planoAcao.id).pipe(
            map(planoAcao => new LoadPlanoAcaoSuccess(planoAcao)),
            catchError(error => of(new LoadPlanoAcaoFail(error)))
          )), catchError(error => of(new UpdatePlanoAcaoFail(error)))
        )));

  @Effect()
  updatePlanoAcaoFail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.UpdatePlanoAcaoFail).pipe(
        map((action: UpdatePlanoAcaoFail) => action.error),
        map((error: any) => {
          this.alertService.abrirAlert(Color.DANGER, error ? error : 'Não foi possível editar o plano de ação.');
          return new CriarAcaoSuccessClose();
        }
        ));

  @Effect()
  iniciarPlanoAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.IniciarPlanoAcao).pipe(
        switchMap((iniciarPlanoAcao: IniciarPlanoAcao) => this._planoAcaoService.iniciarPlanoAcao(iniciarPlanoAcao.id).pipe(
          switchMap(planoAcao => [new LoadPlanoAcaoSuccess(planoAcao)]),
          catchError(error => of(new IniciarPlanoAcaoFail(error)))
        )));

  @Effect()
  executarAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.ExecutarAcao).pipe(
        switchMap((executarAcao: ExecutarAcao) => this._planoAcaoService.executarAcao(executarAcao.acao, executarAcao.situacaoAlvo, executarAcao.responsavel).pipe(
          switchMap(acao => {
            let retorno: (LoadAcao | any)[] = [];
            if (executarAcao.origem === 'executar')
              retorno.push(new ContarExecutarAcoes(executarAcao.acao.planoAcaoPai, executarAcao.nome, executarAcao.responsavel));
            else
              retorno.push(new ContarAcoes(executarAcao.acao.planoAcaoPai, executarAcao.nome));

            retorno.push(new ExecutarAcaoSuccess(acao, executarAcao.origem, executarAcao.situacaoAlvo), new LoadPlanoAcao(executarAcao.acao.planoAcaoPai))
            return retorno;
          }),
          catchError(error => of(new ExecutarAcaoFail(error)))
        )));

  @Effect()
  loadAcoesExecutarSuccess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadAcoesExecutarSuccess).pipe(
        switchMap((loadAcoesExecutar: LoadAcoesExecutarSuccess) => {
          return [new LoadPlanoAcao(loadAcoesExecutar.idPlanoAcao), new ContarExecutarAcoes(loadAcoesExecutar.idPlanoAcao, loadAcoesExecutar.nome, loadAcoesExecutar.responsavel)]
        }
        ),
        catchError(error => of(new LoadPlanoAcaoFail(error))));

  @Effect()
  loadAcoesPlanoSuccess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadAcoesPlanoSuccess).pipe(
        switchMap((loadAcoesPlanoSuccess: LoadAcoesPlanoSuccess) => {
          return [new LoadPlanoAcao(loadAcoesPlanoSuccess.idPlanoAcao), new ContarAcoes(loadAcoesPlanoSuccess.idPlanoAcao, loadAcoesPlanoSuccess.nome)]
        }
        ),
        catchError(error => of(new LoadPlanoAcaoFail(error))));

  @Effect()
  iniciarPlanoAcaoFail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.IniciarPlanoAcaoFail).pipe(
        map((action: IniciarPlanoAcaoFail) => action.error),
        map((error) => {
          this.alertService.abrirAlert(Color.DANGER, error);
          return new CriarAcaoSuccessClose();
        }
        ));

  @Effect()
  createAcaofail$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.CriarAcaoFail),
    map((action: CriarAcaoFail) => {
      if (action.actionBarRef != null && action.actionBarRef != undefined) {
        action.actionBarRef.close();
      }
      this.alertService.abrirAlert(Color.DANGER, action.erro ? action.erro : 'Não foi possível cadastrar a ação.');
      return new CriarAcaoSuccessClose();
    }));

  @Effect()
  updateAcaofail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.UpdateAcaoFail).pipe(
        map((action: UpdateAcaoFail) => {
          if (action.actionBarRef != null && action.actionBarRef != undefined) {
            action.actionBarRef.close();
          }
          this.alertService.abrirAlert(Color.DANGER, action.erro ? action.erro : 'Não foi possível atualizar esta ação.');
          return new CriarAcaoSuccessClose();
        }));

  @Effect()
  createAcaoESubTarefa$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarAcaoESubTarefa).pipe(
        switchMap((criarAcao: CriarAcaoESubTarefa) => this._planoAcaoService.criarAcao(criarAcao.acao).pipe(
          switchMap((acao: Acao) => this._planoAcaoService.loadAcoes(criarAcao.planoAcaoPai, new PageModelVm(0, 5, 0)).pipe(
            map((acoes: AcaoPaginada) => new CriarAcaoESubTarefaSuccess(acoes, criarAcao.actionBarRef, acao.id)),
            catchError(error => of(new CriarAcaoFail(error, criarAcao.actionBarRef)))
          )), catchError(error => of(new CriarAcaoFail(error, criarAcao.actionBarRef))))));

  @Effect()
  loadPlanoAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadPlanoAcao).pipe(
        map((action: LoadPlanoAcao) => action),
        switchMap((action: LoadPlanoAcao) => this._planoAcaoService.loadPlanoAcao(action.id, action.situacao, action.filtroNome, action.reponsavel).pipe(
          map((planoAcao: PlanoAcao) => new LoadPlanoAcaoSuccess(planoAcao, action.situacao, action.filtroNome)),
          catchError(error => of(new LoadPlanoAcaoFail(error))))));

  @Effect()
  loadPlanosAcao$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.LoadPlanosAcao),
    map((action: LoadPlanosAcao) => action),
    switchMap((action: LoadPlanosAcao) => this._planoAcaoService.loadPlanosAcoes(action.page, action.responsavel, action.situacao, action.numeroCooperativa, action.filtroNome, action.vigencia).pipe(
      map((planosAcao: PlanosPaginados) => new LoadPlanosAcaoSuccess(planosAcao, action.numeroCooperativa, action.filtroNome, action.vigencia)),
      catchError(error => of(new LoadPlanosAcaoFail(error))))));

  @Effect()
  loadPlanoAcaoSuccess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadPlanosAcaoSuccess).pipe(
        map((action: LoadPlanosAcaoSuccess) => new ContarPlanosAcao(null, null, action.numeroCooperativa, action.filtroNome, action.vigencia)),
        catchError(error => of(new LoadPlanoAcaoFail(error))));

  @Effect()
  loadAcao$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadAcao).pipe(
        map((action: LoadAcao) => action),
        switchMap((action: LoadAcao) => this._planoAcaoService.loadAcaoById(action.idAcao).pipe(
          map((acao: Acao) => new LoadAcaoSuccess(acao)),
          catchError(error => of(new LoadAcaoFail(error))))));

  @Effect()
  loadAcoesExecutar$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadAcoesExecutar).pipe(
        map((action: LoadAcoesExecutar) => action),
        switchMap((action: LoadAcoesExecutar) => this._planoAcaoService.loadAcoesExecutar(action.responsavel, action.idPlanoAcao, action.pageModelVm, action.situacao, action.nome).pipe(
          map((acaoPaginada: AcaoPaginada) => new LoadAcoesExecutarSuccess(action.idPlanoAcao, acaoPaginada, action.responsavel, action.situacao, action.nome)),
          catchError(error => of(new LoadAcaoFail(error))))));

  @Effect()
  loadAcoesPlano$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadAcoesPlano).pipe(
        map((action: LoadAcoesPlano) => action),
        switchMap((action: LoadAcoesPlano) => {
          return this._planoAcaoService.loadAcoes(action.idPlanoAcao, action.pageModelVm, action.situacao, action.nome, action.responsavel).pipe(
            map((acoes: AcaoPaginada) => { return new LoadAcoesPlanoSuccess(action.idPlanoAcao, acoes, action.responsavel, action.situacao, action.nome) }),
            catchError(error => of(new LoadAcaoFail(error))))
        }));
  @Effect()
  loadPlanoAcaofail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadPlanoAcaoFail).pipe(
        map((action: LoadPlanoAcaoFail) => action.erro),
        map((_) => {
          this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os dados do plano de ação.');
          return new CriarAcaoSuccessClose();
        }
        ));
  @Effect()
  loadPlanosAcaofail$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.LoadPlanosAcaoFail).pipe(
        map((action: LoadPlanoAcaoFail) => action.erro),
        map((_) => {
          this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os planos de ação.');
          return new CriarAcaoSuccessClose();
        }
        ));

  @Effect()
  criarAcaoSucess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarAcaoSuccess).pipe(
        map((action: CriarAcaoSuccess) => action),
        switchMap((action: CriarAcaoSuccess) => {
          if (action.actionBarRef != null && action.actionBarRef != undefined) {
            action.actionBarRef.close();
          }
          return of(new ContarAcoes(action.acao.planoAcaoPai));
        }));

  @Effect()
  updateAcaoSucess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.UpdateAcaoSuccess).pipe(
        map((action: UpdateAcaoSuccess) => action),
        switchMap((action: UpdateAcaoSuccess) => {
          if (action.actionBarRef != null && action.actionBarRef != undefined) {
            action.actionBarRef.close();
          }
          this.alertService.abrirAlert(Color.SUCCESS, 'Ação atualizada com sucesso!');
          return of(new ContarAcoes(action.acaoEditada.planoAcaoPai));
        }));

  @Effect()
  criarAcaoESubTarefaSucess$ =
    this.actions$
      .ofType(PlanoAcaoActionTypes.CriarAcaoESubTarefaSuccess).pipe(
        map((action: CriarAcaoESubTarefaSuccess) => action),
        switchMap((criarAcaoESubTarefaSuccess: CriarAcaoESubTarefaSuccess) => {
          if (criarAcaoESubTarefaSuccess.actionBarRef != null && criarAcaoESubTarefaSuccess.actionBarRef != undefined) {
            criarAcaoESubTarefaSuccess.actionBarRef.close();
          }
          const config: ActionBarConfig = {};
          config.data = { tipo: 2, planoAcao: criarAcaoESubTarefaSuccess.acaoPaginada.data[0].planoAcaoPai, acaoPai: criarAcaoESubTarefaSuccess.acaoPai };
          config.panelClass = 'action-bar-cadastro-tarefa'
          this.actionBarService.open(ActionBarCadastroTarefaComponent, config);
          return of(new ContarAcoes(criarAcaoESubTarefaSuccess.acaoPaginada.data[0].planoAcaoPai));
        }));

  @Effect()
  contarAcoes$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ContarAcoes),
    switchMap((contarAcoes: ContarAcoes) =>
      this._planoAcaoService.countAcoes(contarAcoes.planoAcaoPai, contarAcoes.filtroNome).pipe(
        map((situacaoTotalVm: TotalSituacaoVm[]) => new ContarAcoesSuccess(situacaoTotalVm)),
        catchError(error => of(new ContarAcoesFail(error))))));

  @Effect()
  contarAcoesExecutar$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ContarExecutarAcoes),
    switchMap((contarAcoes: ContarExecutarAcoes) =>
      this._planoAcaoService.countAcoesExecutar(contarAcoes.responsavel, contarAcoes.planoAcaoPai, contarAcoes.filtroNome).pipe(
        map((situacaoTotalVm: TotalSituacaoVm[]) => new ContarExecutarAcoesSuccess(situacaoTotalVm)),
        catchError(error => of(new ContarExecutarAcoesFail(error))))));

  @Effect()
  contarPlanosAcao$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ContarPlanosAcao),
    switchMap((contarAcoes: ContarPlanosAcao) =>
      this._planoAcaoService.contarSituacaoPlanoAcao(
        contarAcoes.responsavel,
        contarAcoes.situacao,
        contarAcoes.numeroCooperativa,
        contarAcoes.filtroNome,
        contarAcoes.vigencia).pipe(
          map(situacaoTotalVm => new ContarPlanosAcaoSuccess(situacaoTotalVm)),
          catchError(error => of(new ContarPlanosAcaoFail(error))))));

  @Effect()
  excluirAcao$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirAcao),
    switchMap((excluirAcao: ExcluirAcao) => this._planoAcaoService.excluirAcao(excluirAcao.acao.id).pipe(
      map((acaoPai: Acao) => new ExcluirAcaoSuccess(excluirAcao.acao, excluirAcao.idPlanoAcaoPai, acaoPai)),
      catchError(error => of(new ExcluirAcaoFail(error))))));

  @Effect()
  excluirPlanoAcao$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirPlanoAcao),
    switchMap((excluirPlanoAcao: ExcluirPlanoAcao) => this._planoAcaoService.excluirPlanoAcao(excluirPlanoAcao.idPlanoAcao).pipe(
      map(_ => new ExcluirPlanoAcaoSuccess(excluirPlanoAcao.idPlanoAcao, excluirPlanoAcao.numeroCooperativa, excluirPlanoAcao.situacao, excluirPlanoAcao.filtroNome)),
      catchError(error => of(new ExcluirPlanoAcaoFail(error))))));

  @Effect({ dispatch: false })
  excluirPlanoAcaoSuccess$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirPlanoAcaoSuccess),
    map((action: ExcluirPlanoAcaoSuccess) => action),
    map((excluirPlanoAcao) => {
      this.alertService.abrirAlert(Color.SUCCESS, 'Plano de ação excluído com sucesso!');
      if (this.router.url == '/plano-acao') {
        this.store.dispatch(new ContarPlanosAcao(null, null, excluirPlanoAcao.numeroCooperativa, excluirPlanoAcao.filtroNome))
      } else {
        this.router.navigate([`/plano-acao/`]);
      }
    }
    ));

  @Effect({ dispatch: false })
  excluirPlanoAcaoFail$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirPlanoAcaoFail),
    map((action: ExcluirPlanoAcaoFail) => action.error),
    map((error) => {
      this.alertService.abrirAlert(Color.DANGER, error);
    }
    ));

  @Effect({ dispatch: false })
  excluirAcaofail$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirAcaoFail),
    map((_) => {
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir a ação.');
    }
    ));

  @Effect()
  excluirAcaoSuccess$ = this.actions$.pipe(
    ofType(PlanoAcaoActionTypes.ExcluirAcaoSuccess),
    map((action: ExcluirAcaoSuccess) => action),
    map((action) => {
      this.alertService.abrirAlert(Color.SUCCESS, 'Ação excluída com sucesso!');
      return new ContarAcoes(action.planoAcaoId);
    }));
}
