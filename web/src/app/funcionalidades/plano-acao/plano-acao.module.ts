import {NgModule} from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';

import {PlanoAcaoRoutingModule} from './plano-acao-routing.module';
import {StoreModule} from '@ngrx/store';
import * as fromPlanoAcao from './reducers/plano-acao.reducer';
import {EffectsModule} from '@ngrx/effects';
import {PlanoAcaoEffects} from './effects/plano-acao.effects';
import {ContainerPlanoAcaoComponent} from './containers/container-plano-acao/container-plano-acao.component';
import {PlanoAcaoComponent} from './components/plano-acao/plano-acao.component';
import {DialogoNovoPlanoAcaoComponent} from './components/dialogo-novo-plano-acao/dialogo-novo-plano-acao.component';
import {
  ActionbarModule,
  CardModule,
  FormModule,
  ModalModule,
  ModalService,
  ProgressBarModule,
  TabsModule
} from '@sicoob/ui';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {ptBrLocale} from 'ngx-bootstrap/locale';
import {PersistanceService} from 'src/app/shared/services/persistence.service';
import {ContainerAcoesComponent} from './containers/container-acoes/container-acoes.component';
import {AvatarModule} from '@app/shared/components/sins-avatar/sc-avatar.module';
import {ActionBarCadastroTarefaComponent} from './components/action-bar-cadastro-tarefa/action-bar-cadastro-tarefa.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgxCurrencyModule} from 'ngx-currency';
import {ScAccordionItemModule} from 'src/app/shared/components/sc-accordion-item/sc-accordion-item.module';
import {ScSelectionListModule} from 'src/app/shared/components/sc-selection-list/sc-selection-list.module';
import {ExecutarAcaoComponent} from './components/executar-acao/executar-acao.component';
import {DetailActionComponent} from './components/detail-action/detail-action.component';
import {FiltroAcoesComponent} from './components/filtro-acoes/filtro-acoes.component';
import {ToolBarPlanoAcaoComponent} from './components/tool-bar-plano-acao/tool-bar-plano-acao.component';
import {ScCheckbuttonModule} from 'src/app/shared/components/sc-checkbutton/sc-checkbutton.module';
import {DialogoConfirmacaoComponent} from 'src/app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import {ContainerExecutarPlanoComponent} from './containers/container-executar-plano/container-executar-plano.component';
import {MenuDropDownModule} from 'src/app/shared/components/menu-dropdown/menu-dropdown.module';
import {DialogoJustificativaAjustesComponent} from 'src/app/shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import {DialogoConcluirAcao} from './components/dialogo-concluir-acao/dialogo-concluir-acao.component';
import {ContainerRouterAcaoComponent} from './containers/container-router-acao/container-router-acao.component';
import {SinsLoadingService} from 'src/app/shared/components/sins-loading/sins-loading.service';
import {ScListaUsuariosModule} from 'src/app/shared/components/sc-lista-usuarios/sc-lista-usuarios.module';
import {ActionBarDetalheAcaoComponent} from './components/action-bar-detalhe-acao/action-bar-detalhe-acao.component';
import {ExecutarAcaoButtonsComponent} from './components/executar-acao-buttons/executar-acao-buttons.component';
import {SharedModule} from '@shared/shared.module';
import {NgxPermissionsModule} from 'ngx-permissions';
import { SinsBadgeModule } from '@app/shared/components/sins-badge/sins-badge.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {ActionbarFiltroPlanosComponent} from './components/actionbar-filtro-planos/actionbar-filtro-planos.component'
import {ScSectionModule} from '@shared/components/sc-section/sc-section.module';
import {ScChipModule} from '@shared/components/sc-chip/sc-chip.module';
import * as fromUsuarioInstituto from '@app/reducers/usuario-instituto.reducer';
import {RadioModule} from '@shared/components/sc-radio/radio.module';
import { PopoverModule } from 'ngx-bootstrap';
import { ContainerRouterPlanoAcao } from './containers/container-router-acao/container-router-plano-acao.component';

defineLocale('pt-br', ptBrLocale);

@NgModule({
  declarations: [
    ExecutarAcaoButtonsComponent,
    ContainerRouterAcaoComponent,
    DialogoConcluirAcao,
    ContainerExecutarPlanoComponent,
    ContainerPlanoAcaoComponent,
    PlanoAcaoComponent,
    DialogoNovoPlanoAcaoComponent,
    ContainerAcoesComponent,
    ActionBarCadastroTarefaComponent,
    DetailActionComponent,
    FiltroAcoesComponent,
    ToolBarPlanoAcaoComponent,
    ExecutarAcaoComponent,
    ActionBarDetalheAcaoComponent,
    ActionbarFiltroPlanosComponent,
    ContainerRouterPlanoAcao
  ],
  entryComponents: [
    DialogoConcluirAcao,
    DialogoJustificativaAjustesComponent,
    DialogoNovoPlanoAcaoComponent,
    ActionBarCadastroTarefaComponent,
    DialogoConfirmacaoComponent,
    ActionBarDetalheAcaoComponent,
    ActionbarFiltroPlanosComponent

  ],
  imports: [
    PopoverModule.forRoot(),
    RadioModule,
    ScChipModule,
    ScSectionModule,
    InfiniteScrollModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    FormModule,
    NgSelectModule,
    ScSelectionListModule,
    ActionbarModule,
    AvatarModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
    NgxCurrencyModule,
    ModalModule,
    CommonModule,
    ScCheckbuttonModule,
    TabsModule,
    ScAccordionItemModule,
    PlanoAcaoRoutingModule,
    CardModule,
    MenuDropDownModule,
    ProgressBarModule,
    ScListaUsuariosModule,
    NgxPermissionsModule.forChild(),
    StoreModule.forFeature('planoAcaoState', fromPlanoAcao.reducer),
    StoreModule.forFeature('usuarioInsitutoState', fromUsuarioInstituto.reducer),
    EffectsModule.forFeature([PlanoAcaoEffects]),
    SinsBadgeModule
  ],
  providers: [ModalService, BsLocaleService, PersistanceService, SinsLoadingService, CurrencyPipe ],

})
export class PlanoAcaoModule { }
