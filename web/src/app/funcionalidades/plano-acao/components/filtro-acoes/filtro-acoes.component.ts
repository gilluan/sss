import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { TabComponent, TabPanelComponent } from "@sicoob/ui";
import { Observable } from "rxjs";
import { TotalSituacaoVm } from "src/app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model";
import { SituacaoAcaoType } from "../../models/situacao-acao.type";

@Component({
  selector: 'filtro-acoes',
  templateUrl: './filtro-acoes.component.html',
  styleUrls: ['./filtro-acoes.component.scss']
})
export class FiltroAcoesComponent implements AfterViewInit {

  @Input() counters$: Observable<TotalSituacaoVm[]>;
  @Output() changeTab: EventEmitter<SituacaoAcaoType> = new EventEmitter<SituacaoAcaoType>();

  @ViewChild('tabPanelDashboard') tabPanelDashboard: TabPanelComponent;

  abaSelecionada: TabComponent;

  onTabSelected(tabIndex) {
    this.changeTab.emit(this.getSituacao(tabIndex));
  }

  ngAfterViewInit() {
    this.abaSelecionada = this.getTab(SituacaoAcaoType.todas);
  }

  active(situacao): boolean {
    return this.abaSelecionada && this.abaSelecionada.title == situacao
  }

  getTab = (situacao: SituacaoAcaoType) => this.tabPanelDashboard.tabs.find(s => s.title == situacao);
  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);
  getSituacaoClasse = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classesBadge(situacao);
  getIndexSituacao = (situacao: SituacaoAcaoType) => SituacaoAcaoType.values().indexOf(situacao) + 1

  mudarAba(situacao: SituacaoAcaoType) {
    this.abaSelecionada = this.getTab(situacao);
    this.tabPanelDashboard.selectByIndex(this.getIndexSituacao(situacao));
  }

  private getSituacao(tabIndex: number): SituacaoAcaoType {
    if (tabIndex != 0) {
      let i = (tabIndex - 1);
      return SituacaoAcaoType.values()[i];
    }
  }


}
