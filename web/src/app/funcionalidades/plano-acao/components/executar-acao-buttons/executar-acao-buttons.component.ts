import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Acao } from 'src/app/funcionalidades/plano-acao/models/acao.model';

@Component({
  selector: 'executar-acao-buttons',
  templateUrl: './executar-acao-buttons.component.html',
  styleUrls: ['./executar-acao-buttons.component.scss']
})
export class ExecutarAcaoButtonsComponent {

  @Input() acao: Acao;

  @Output() iniciar = new EventEmitter<any>();
  @Output() retomar = new EventEmitter<any>();
  @Output() parar = new EventEmitter<any>();
  @Output() concluir = new EventEmitter<any>();


  onIniciar() { this.iniciar.emit() }

  onRetomar() { this.retomar.emit() }

  onParar() { this.parar.emit() }

  onConcluir() { this.concluir.emit(0) }
}
