import { Component, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ModalRef, ModalService, ACTIONBAR_DATA } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { JustificativaAjustesService } from 'src/app/funcionalidades/sins-analise-cadastros/justificativa-ajustes.service';
import { JustificativaAjusteVm } from 'src/app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';
import { DialogoJustificativaAjustesComponent } from 'src/app/shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import { ScAccordionItemComponent } from 'src/app/shared/components/sc-accordion-item/sc-accordion-item.component';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { ClearAcaoEditar, ExecutarAcao } from '../../actions/plano-acao.actions';
import { DialogoConcluirAcao } from '../../components/dialogo-concluir-acao/dialogo-concluir-acao.component';
import { Acao } from '../../models/acao.model';
import { DateControlLabelFactory } from '../../models/date-control-label/date-control-label.factory';
import { DateControlLabel } from '../../models/date-control-label/date-control-label.service';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { PlanoAcao } from '../../models/plano-acao.model';
import { JustificativaType } from '@app/shared/models/justificativa.type';

@Component({
  selector: 'sc-action-bar-detalhe-acao',
  templateUrl: './action-bar-detalhe-acao.component.html',
  styleUrls: ['./action-bar-detalhe-acao.component.scss', '../executar-acao/executar-acao.component.scss'],
})
export class ActionBarDetalheAcaoComponent implements OnInit, OnDestroy {


  ngOnDestroy(): void {
    this.store$.dispatch(new ClearAcaoEditar())
  }

  modalConcluirAcao: ModalRef;
  modalJustificativa: ModalRef;
  usuarioInstituto: any;
  origem: string;
  situacaoAtual: SituacaoAcaoType;
  nomeFiltro: string;

  constructor(
    private store$: Store<fromPlanoAcao.State>,
    private persistenceService: PersistanceService,
    private justificativaService: JustificativaAjustesService,
    private modalService: ModalService,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  acao$: Observable<Acao>;
  planoAcao: Observable<PlanoAcao>;

  ngOnInit() {
    this.usuarioInstituto = this.persistenceService.get("usuario_instituto");
    this.planoAcao = this.store$.pipe(select(fromPlanoAcao.getPlanoAtual));
    this.acao$ = this.store$.pipe(select(fromPlanoAcao.getActionEditar));
    this.origem = this.data.origem;
    this.situacaoAtual = this.data.situacaoAtual;
    this.nomeFiltro = this.data.nomeFiltro
  }

  calculateDays(acao: Acao) {
    let dateControlLabel: DateControlLabel = DateControlLabelFactory.createSituacaoMenu(acao.situacao, acao.dataFim, acao.dataInicio, acao.dataRealFim, acao.dataRealInicio)
    return dateControlLabel.getLabel();
  }


  onParar(acao: Acao) {
    this.modalJustificativa = this.modalService.open(DialogoJustificativaAjustesComponent,
      {
        data: {
          title: 'Pausar tarefa por impedimento',
          label: 'Descreva abaixo o impedimento que encontrou ao executar a tarefa:'
        },
        panelClass: 'sins-modal-justificativa'
      }
    );
    this.modalJustificativa.afterClosed().subscribe(
      (retorno: any) => {
        if (retorno.sucesso) {
          let justificativaAjuste = new JustificativaAjusteVm();
          justificativaAjuste.justificativa = retorno.justificativaAjusteText;
          justificativaAjuste.idAcao = acao.id;
          justificativaAjuste.tipo = JustificativaType.impedimentoAcao;
          justificativaAjuste.idVoluntarioCriador = this.persistenceService.get("usuario_instituto").id;
          this.justificativaService.salvarJustificativa(justificativaAjuste).subscribe(
            _ => this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.impedida ,acao.planoAcaoPai, acao, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro))
          );
        }
      }
    )
  }

  onIniciar(acao: Acao) {
    acao.dataRealInicio = new Date(Date.now());
    this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.iniciada ,acao.planoAcaoPai, acao, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro))
  }
  onRetomar(acao: Acao) {
    this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.iniciada ,acao.planoAcaoPai, acao, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro));
  }
  onConcluir(acao: Acao) {
    this.modalConcluirAcao = this.modalService.open(DialogoConcluirAcao,
      {
        data: {acao},
        panelClass: 'sins-modal-success'
      }
    );
    this.modalConcluirAcao.afterClosed().subscribe(
      (investimento: number) => {
        if (investimento >= 0.0) {
          acao.investimentoReal = investimento;
          acao.dataRealFim = new Date(Date.now());
          this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.concluida ,acao.planoAcaoPai, acao, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro));
        }
      }
    )

  }

  getSituacaoClass = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);

  podeExecutar = (acao, planoAcao) => {
    let responsavel = acao.responsavel._id ? acao.responsavel._id : acao.responsavel;
    return !this.temSub(acao) && planoAcao.situacao != 'planejando' && responsavel == this.usuarioInstituto.id && planoAcao.vigencia === new Date().getFullYear().toString() ;
  }

  temSub = (acao) => {
    return acao.subTarefas && acao.subTarefas.length > 0 && acao.subTarefas[0] != null
  }

}
