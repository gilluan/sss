import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Acao } from '../../models/acao.model';
import { DateControlLabelFactory } from '../../models/date-control-label/date-control-label.factory';
import { DateControlLabel } from '../../models/date-control-label/date-control-label.service';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import { DatePipe, CurrencyPipe } from '@angular/common';

@Component({
  selector: 'sc-executar-acao',
  templateUrl: './executar-acao.component.html',
  styleUrls: ['./executar-acao.component.scss']
})
export class ExecutarAcaoComponent implements OnInit, OnDestroy {

  constructor(private datePipe: DatePipe, private currencyPipe: CurrencyPipe) { }

  @Output() iniciar: EventEmitter<any> = new EventEmitter<any>();
  @Output() parar: EventEmitter<any> = new EventEmitter<any>();
  @Output() retomar: EventEmitter<any> = new EventEmitter<any>();
  @Output() concluir: EventEmitter<any> = new EventEmitter<any>();
  @Output() openDetalhar: EventEmitter<any> = new EventEmitter<any>();
  @Input() acao: Acao;

  formatarDatas = (data1: Date, data2: Date) => data1 && data2 ? `${this.datePipe.transform(data1, 'dd/MM/yyyy')} | ${this.datePipe.transform(data2, 'dd/MM/yyyy')}` : '';
  formatarMoeda = (valor: any) => valor ? this.currencyPipe.transform(valor, 'BRL') : '';

  onIniciar() {
    this.iniciar.emit(this.acao);
  }
  onParar() {
    this.parar.emit(this.acao);
  }
  onRetomar() {
    this.retomar.emit(this.acao);
  }
  onConcluir() {
    this.concluir.emit(this.acao);
  }

  ngOnInit() { }

  ngOnDestroy() { }

  calculateDays() {
    let dateControlLabel: DateControlLabel = DateControlLabelFactory.createSituacaoMenu(this.acao.situacao, this.acao.dataFim, this.acao.dataInicio, this.acao.dataRealFim, this.acao.dataRealInicio)
    return dateControlLabel.getLabel();
  }

  openActionBarDetalhar(id: string){
    this.openDetalhar.emit(id);
  }

  getSituacaoClass = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);


}
