import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { Observable, Subscription } from "rxjs";

@Component({
  selector: 'app-actionbar-filtro-planos',
  templateUrl: './actionbar-filtro-planos.component.html',
  styleUrls: ['./actionbar-filtro-planos.component.scss']
})
export class ActionbarFiltroPlanosComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();

  cooperativasObservable: Observable<string[]>;
  responsavelObservable:  Observable<UsuarioInstituto[]>;
  minDate: Date;
  maxDate: Date;

  filtroForm = this.fb.group({
    nomeFiltro: [''],
    cooperativas: [''],
    vigencia: [''],
  });

  constructor(
    public actionBarRef: ActionBarRef,
    private fb: FormBuilder,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  ngOnInit(): void {
    this.tratarRange();
    this.filtroForm.setErrors({ required: true });
    this.cooperativasObservable = this.data['cooperativas'];
    this.responsavelObservable = this.data['responsaveis'];
    if (this.data.filtros) {
      this.filtroForm.controls['nomeFiltro'].setValue(this.data.filtros.nome)
      this.filtroForm.controls['cooperativas'].setValue(this.data.filtros.numeroCooperativa)
      const vigencia = this.data.filtros.vigencia ? new Date(`${this.data.filtros.vigencia}-01-01T00:00:00`) : null;
      this.filtroForm.controls['vigencia'].setValue(vigencia);
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  limparFiltros = () => this.filtroForm.reset();


  closeSidebar = () => this.actionBarRef.close({ action: 'fechar' });

  applyFilter = () => this.actionBarRef.close({
    action: 'filtrar',
    data: this.filtroForm.value.cooperativas,
    filtroNome: this.filtroForm.value.nomeFiltro,
    programa: this.filtroForm.value.programa,
    responsavel: this.filtroForm.value.responsavel,
    vigencia: this.filtroForm.value.vigencia ? (this.filtroForm.value.vigencia as Date).getFullYear().toString() : null,
  });


  private tratarRange() {
    this.minDate = new Date(`${new Date().getFullYear()}-01-01T00:00:00`);
    this.maxDate = new Date(`${new Date().getFullYear() + 1}-12-31T23:59:59`);
  }
}
