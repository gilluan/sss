import { Component, Inject, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import { Acao } from '../../models/acao.model';
import { JustificativaAjusteVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { JustificativaAjustesService } from '@app/funcionalidades/sins-analise-cadastros/justificativa-ajustes.service';
import { Subscription } from 'rxjs';
import { JustificativaType } from '@app/shared/models/justificativa.type';


@Component({
  selector: 'app-dialogo-concluir-acao',
  templateUrl: './dialogo-concluir-acao.component.html',
  styleUrls: ['./dialogo-concluir-acao.component.scss']
})
export class DialogoConcluirAcao implements OnInit, DoCheck, OnDestroy {


  ngOnDestroy(): void {
    if (this.sub) this.sub.unsubscribe();
  }

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any,
    private fb: FormBuilder,
    private persistenceService: PersistanceService,
    private justificativaService: JustificativaAjustesService
  ) { }

  justificativaForm = this.fb.group({
    justificativaAjusteText: ['', Validators.required]
  });

  acao: Acao;
  investimentoSuperior: boolean = false;
  justificarInvestimento: boolean = false;

  labelAction: string = 'Salvar';
  label: string = 'Atenção';
  sub: Subscription
  investimentoForm = this.fb.group({
    investimento: [0, Validators.required]
  });

  ngDoCheck() {
    if (this.investimentoForm.valid) {
      this.investimentoForm.controls['investimento'].markAsTouched();
    } else {
      this.investimentoForm.controls['investimento'].markAsPristine();
    }
  }

  ngOnInit() {
    this.acao = this.data.acao;
  }

  closeModal(submit: boolean = true) {
    if( submit )
      this.ref.close(this.investimentoForm.value.investimento);
    else
      this.ref.close();
  }

  irParaJustificar() {
    this.label = "Alteração de investimento"
    this.investimentoSuperior = false;
    this.justificarInvestimento = true;
  }

  onJustificar() {
    let justificativaAjuste = new JustificativaAjusteVm();
    justificativaAjuste.justificativa = this.justificativaForm.value.justificativaAjusteText;
    justificativaAjuste.idAcao = this.acao.id;
    justificativaAjuste.tipo = JustificativaType.investimentoMaior;
    justificativaAjuste.idVoluntarioCriador = this.persistenceService.get("usuario_instituto").id;
    this.sub = this.justificativaService.salvarJustificativa(justificativaAjuste).subscribe(
      _ => this.closeModal())
  }

  onSubmit() {
    if (this.investimentoForm.value.investimento > this.acao.investimento) {
      this.investimentoSuperior = true;
    } else
      this.closeModal()
  }

}
