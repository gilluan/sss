import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Acao } from '../../models/acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import { PlanoAcao } from '../../models/plano-acao.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'sc-tool-bar-plano-acao',
  templateUrl: './tool-bar-plano-acao.component.html',
  styleUrls: ['./tool-bar-plano-acao.component.scss']
})
export class ToolBarPlanoAcaoComponent {


  @Input() isScroll: boolean;
  @Input() planoAcao: Observable<PlanoAcao>;


  getSituacaoClass = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);



}
