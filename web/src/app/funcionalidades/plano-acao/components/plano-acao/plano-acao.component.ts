import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ItemMenu } from 'src/app/shared/components/menu-dropdown/model/item-menu.model';
import { PositionDotsType } from 'src/app/shared/components/menu-dropdown/model/position-dots.type';
import { SituacaoMenuFactory } from '../../models/menu-situacao/situacao-menu.factory';
import { PlanoAcao } from '../../models/plano-acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';

@Component({
  selector: 'sc-plano-acao',
  templateUrl: './plano-acao.component.html',
  styleUrls: ['./plano-acao.component.scss']
})
export class PlanoAcaoComponent implements OnInit {


  @Output() criarPlanoAcao: EventEmitter<any> = new EventEmitter<any>();
  @Output() atualizarPlanoAcao: EventEmitter<any> = new EventEmitter<any>();
  @Output() excluirPlano: EventEmitter<any> = new EventEmitter<any>();

  @Input() planosAcao: Observable<PlanoAcao[]>;

  position: PositionDotsType = PositionDotsType.vertical;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  montarMenu(planoAcao: PlanoAcao): Array<ItemMenu> {
    let situacaoMenu = SituacaoMenuFactory.createSituacaoMenu(planoAcao.situacao, planoAcao);
    return situacaoMenu.menu;
  }

  abrirCriar() {
    this.criarPlanoAcao.emit();
  }

  onAction($event: ItemMenu) {
    if ($event.id == 1 || $event.id == 3 || $event.id == 5)
      this.router.navigate([`/plano-acao/${$event.entity.id}/planejar`])
    else if ($event.id == 2)
      this.onAtualizarPlanoAcao($event.entity);
    else if ($event.id == 4)
      this.router.navigate([`/plano-acao/${$event.entity.id}/executar`])
    else if ($event.id == 6)
      this.onExcluirPlano($event.entity);
  }

  onAtualizarPlanoAcao(planoAcao: PlanoAcao) {
    this.atualizarPlanoAcao.emit(planoAcao);
  }

  onExcluirPlano(planoAcao: PlanoAcao) {
    this.excluirPlano.emit(planoAcao);
  }

  getSituacaoClass = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);

}
