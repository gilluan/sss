import { AfterViewInit, Component, Inject, OnDestroy, OnInit, DoCheck } from '@angular/core';
import { FormBuilder, FormControl, Validators, ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { ACTIONBAR_DATA, ActionBarRef } from '@sicoob/ui';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Subscription, Observable, of } from 'rxjs';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { ClearAcaoEditar, CriarAcao, CriarAcaoESubTarefa, UpdateAcao } from '../../actions/plano-acao.actions';
import { Acao } from '../../models/acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { PlanoAcaoService } from '../../plano-acao.service';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlanoAcao } from '../../models/plano-acao.model';


@Component({
  selector: 'sc-action-bar-cadastro-tarefa',
  templateUrl: './action-bar-cadastro-tarefa.component.html',
  styleUrls: ['./action-bar-cadastro-tarefa.component.scss']
})
export class ActionBarCadastroTarefaComponent implements OnInit, OnDestroy, AfterViewInit, DoCheck {

  minDate: Date;
  maxDate: Date;
  subscription: Subscription = new Subscription();
  usuarioInstituto: any;
  temSubTarefa = false;
  acaoPai$: Observable<Acao>;
  labelAcao: string;
  labelSubstarefa: string;

  ngOnInit() {
    this.localeService.use("pt-br");
    if (this.data.acaoPai) {
      this.acaoPai$ = this.planoAcaoService.loadAcaoById(this.data.acaoPai)
    }
    this.usuarioInstituto = this.persistenceService.get("usuario_instituto");
    this.carregarForm();
    this.subscription.add(this.planoAcaoService.loadPlanoAcao(this.data.planoAcao).subscribe((planoAcao: PlanoAcao) => {
      this.minDate = new Date(`${planoAcao.vigencia}-01-01T00:00:01`);
      this.maxDate = new Date(`${planoAcao.vigencia}-12-31T23:59:59`);
    }))
    forkJoin({ acao: this.translateService.get("planoacao.actionbar.tipo.acao"), subtarefa: this.translateService.get("planoacao.actionbar.tipo.subTarefa") })
      .subscribe((labels: { acao: string, subtarefa: string }) => {
        this.labelAcao = labels.acao;
        this.labelSubstarefa = labels.subtarefa;
        this.tipo = this.data.tipo === 1 ? labels.acao : labels.subtarefa;
        this.salvarCriarTipo = this.tipo;
      });
  }

  acaoForm: any;
  isDisabled: boolean;
  tipo: string;
  salvarCriarTipo: string;
  acaoEditar: Acao;
  changeObservable: Subscription = new Subscription();

  constructor(
    public actionBarRef: ActionBarRef,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private persistenceService: PersistanceService,
    private store$: Store<fromPlanoAcao.State>,
    private planoAcaoService: PlanoAcaoService,
    private translateService: TranslateService,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  ngAfterViewInit(): void {
    this.subscription.add(this.store$.pipe(select(fromPlanoAcao.getActionEditar)).subscribe((action: Acao) => {
      if (action) {
        this.acaoEditar = action;
        this.acaoPai$ = of(action.acaoPai);
        this.acaoForm.patchValue({
          responsavel: action.responsavel,
          dataInicio: new Date(action.dataInicio),
          dataFim: new Date(action.dataFim),
          investimento: action.investimento,
          nome: action.nome,
          lugar: action.lugar,
          descricao: action.descricao,
          motivo: action.motivo,
        });
        if (action.subTarefas && action.subTarefas.length > 0 && action.subTarefas[0] != null) {
          this.acaoForm.controls['investimento']['disable']();
          this.isDisabled = true;
        }
      }
    }));

  }

  carregarForm() {
    this.acaoForm = this.fb.group({
      responsavel: new FormControl({ value: this.persistenceService.get('usuario_instituto'), disabled: this.temSubTarefa }, Validators.required),
      dataInicio: ['', Validators.required],
      dataFim: ['', Validators.required],
      investimento: [0, Validators.required],
      nome: ['', Validators.required],
      lugar: ['', Validators.required],
      descricao: ['', Validators.required],
      motivo: ['', Validators.required],
    });
    this.acaoForm.validator = this.compareteDateValidator;
  }

  compareteDateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const dataFimControl = control.get('dataFim');
    const dataInicioControl = control.get('dataInicio');
    if (dataInicioControl.value && dataFimControl.value) {
      if (dataFimControl.value < dataInicioControl.value) {
        this.dataFim.setErrors({ "dataFimMenorControl": true })
      }else {
        this.dataFim.setErrors(null)
      }
    }
    return null;
  };

  get dataFim() {
    return this.acaoForm.get('dataFim');
  }


  get dataInicio() {
    return this.acaoForm.get('dataInicio');
  }

  editarAcao() {
    if (this.editarAcao) {
      const acaoEditar = new Acao();
      acaoEditar.nome = this.acaoForm.value.nome;
      acaoEditar.acaoPai = this.acaoEditar.acaoPai;
      acaoEditar.dataInicio = this.acaoForm.value.dataInicio;
      acaoEditar.dataFim = this.acaoForm.value.dataFim;
      acaoEditar.descricao = this.acaoForm.value.descricao;
      acaoEditar.id = this.acaoEditar.id;
      acaoEditar.investimento = this.acaoForm.value.investimento >= 0 ? this.acaoForm.value.investimento : 0;
      acaoEditar.lugar = this.acaoForm.value.lugar;
      acaoEditar.motivo = this.acaoForm.value.motivo;
      acaoEditar.planoAcaoPai = this.acaoEditar.planoAcaoPai;
      acaoEditar.responsavel = this.acaoForm.value.responsavel.id;
      this.store$.dispatch(new UpdateAcao(acaoEditar, this.actionBarRef));
    }
  }

  ngDoCheck() {
    if (this.acaoForm.valid && !this.acaoEditar) {
      this.acaoForm.controls['investimento'].markAsTouched();
    } else {
      this.acaoForm.controls['investimento'].markAsPristine();
    }
  }

  onChanges(): void {
    this.changeObservable
      .add(this.acaoForm.valueChanges.subscribe(val => {
        console.log(val);
      }));
  }

  ngOnDestroy() {
    this.store$.dispatch(new ClearAcaoEditar());
    if (this.changeObservable) {
      this.changeObservable.unsubscribe();
    }
    this.subscription.unsubscribe();
  }

  criar(fechar: boolean) {
    let actionBarRef = this.actionBarRef;
    const responsavel = this.acaoForm.value.responsavel;
    const acao = new Acao();
    acao.nome = this.acaoForm.value.nome;
    acao.dataInicio = this.acaoForm.value.dataInicio ? this.acaoForm.value.dataInicio : null;
    acao.dataFim = this.acaoForm.value.dataFim ? this.acaoForm.value.dataFim : null;
    acao.descricao = this.acaoForm.value.descricao;
    acao.investimento = this.acaoForm.value.investimento >= 0 ? this.acaoForm.value.investimento : 0;
    acao.lugar = this.acaoForm.value.lugar;
    acao.motivo = this.acaoForm.value.motivo;
    acao.situacao = SituacaoAcaoType.naoIniciada;
    acao.responsavel = responsavel.id;
    if (!fechar && !this.temSubTarefa) {
      actionBarRef = null;
    }
    if (this.tipo == this.labelAcao && this.salvarCriarTipo == this.labelSubstarefa) {
      acao.acaoPai = null;
      acao.planoAcaoPai = this.data.planoAcao;
      this.store$.dispatch(new CriarAcaoESubTarefa(acao, actionBarRef, this.data.planoAcao));
    } else {
      if (this.tipo == this.labelAcao && this.salvarCriarTipo == this.labelAcao) {  // Criar Nova Acao
        acao.acaoPai = null;
        acao.planoAcaoPai = this.data.planoAcao;
      } else if (this.tipo == this.labelSubstarefa && this.salvarCriarTipo == this.labelSubstarefa) { // Criar Nova Acao Subtarefa
        acao.acaoPai = this.data.acaoPai;
        acao.planoAcaoPai = this.data.planoAcao;
      }
      this.store$.dispatch(new CriarAcao(acao, actionBarRef, this.data.planoAcao));
    }
    this.carregarForm();

  }

  changePossuiSub($event) {
    this.temSubTarefa = !this.temSubTarefa;
    const state = this.temSubTarefa ? 'disable' : 'enable';
    this.salvarCriarTipo = this.temSubTarefa ? this.labelSubstarefa : this.labelAcao;
    this.acaoForm.controls['investimento'][state]();
    this.acaoForm.controls['dataInicio'][state]();
    this.acaoForm.controls['dataFim'][state]();
  }

  get title(): Observable<string> {
    return forkJoin({ editando: this.translateService.get("planoacao.actionbar.titulo.editando"), criandoNova: this.translateService.get("planoacao.actionbar.titulo.criandoNova") }).pipe(
      map((labels: { editando: string, criandoNova: string }) => {
        return this.acaoEditar ? `${labels.editando} ${this.tipo}` : `${labels.criandoNova} ${this.tipo}`;
      })
    )
  }
}
