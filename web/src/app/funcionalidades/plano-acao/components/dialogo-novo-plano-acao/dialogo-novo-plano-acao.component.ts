import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { PerfilType } from '@app/shared/types/perfil.type';
import { Store, select } from '@ngrx/store';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import { CriarPlanoAcao, UpdatePlanoAcao } from '../../actions/plano-acao.actions';
import { PlanoAcao } from '../../models/plano-acao.model';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { Subscription } from 'rxjs';
import { PageModelVm } from '@app/shared/models/page.model';


@Component({
  selector: 'app-dialogo-novo-plano-acao',
  templateUrl: './dialogo-novo-plano-acao.component.html',
  styleUrls: ['./dialogo-novo-plano-acao.component.scss']
})
export class DialogoNovoPlanoAcaoComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscription: Subscription = new Subscription();

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private store$: Store<fromPlanoAcao.State>,
    private persistenceService: PersistanceService

  ) { }

  labelAction: string = 'Criar Plano';
  label: string = 'Novo plano de ação';
  planoAcao: PlanoAcao = null;
  minDate: Date;
  maxDate: Date;
  anos = [
    new Date().getFullYear().toString()/*,
    new Date().getFullYear() + 1,
    Foi retirado por descisao do Instituto Sicoob*/
  ]
  usuarioInstituto: any;

  get vigencia(): FormControl {
    return this.planoAcaoForm.get('vigencia') as FormControl;
  }

  planoAcaoForm = this.fb.group({
    nome: ['', Validators.required],
    vigencia: [null, Validators.required],
    dataProximaRevisao: ["", Validators.required],
    responsavel: [this.persistenceService.get("usuario_instituto"), Validators.required]
  });

  ngOnInit() {
    this.localeService.use("pt-br");
    this.subscription
    .add(this.vigencia.valueChanges.subscribe((vigencia: string) => this.tratarVigencia(vigencia, this.planoAcao ? this.planoAcao.dataProximaRevisao : null)))
    .add(this.store$.pipe(select(fromPlanoAcao.getPaginacaoAtualAcoes)).subscribe((paginacao: PageModelVm) => this.checkDisableVigencia(paginacao)));
    this.planoAcao = this.data.planoAcao;
    if (this.planoAcao) {
      this.labelAction = 'Editar Plano';
      this.label = 'Editar Plano de Ação';
      this.planoAcaoForm.patchValue({
        dataProximaRevisao: this.planoAcao.dataProximaRevisao,
        nome: this.planoAcao.nome,
        responsavel: this.planoAcao.responsavel,
        vigencia: this.planoAcao.vigencia
      });
    }
    this.usuarioInstituto = this.persistenceService.get("usuario_instituto");
  }

  closeModal(sucesso) {
    this.ref.close({ sucesso });
  }

  onSubmit() {
    if (!this.planoAcao) {
      this.store$.dispatch(new CriarPlanoAcao(this.montarPlano()));
    } else {
      this.store$.dispatch(new UpdatePlanoAcao(this.montarPlano(), this.data.situacao));
    }
    this.closeModal(true);
  }

  private checkDisableVigencia(paginacao: PageModelVm) {
    if(paginacao) {
      if(paginacao.total > 0) {
        this.vigencia.disable();
      }
    }
  }

  private montarPlano() {
    let planoAcaoX = new PlanoAcao();
    if (this.planoAcao) {
      planoAcaoX.id = this.planoAcao.id;
    }
    let responsavel = this.planoAcaoForm.value.responsavel;
    planoAcaoX.numeroCooperativa = this.planoAcao ? this.planoAcao.numeroCooperativa : responsavel.numeroCooperativa;
    planoAcaoX.responsavel = responsavel;
    planoAcaoX.dataProximaRevisao = this.planoAcaoForm.value.dataProximaRevisao;
    planoAcaoX.nome = this.planoAcaoForm.value.nome;
    planoAcaoX.vigencia = this.planoAcaoForm.value.vigencia;
    return planoAcaoX;
  }

  public tratarVigencia(vigencia: string, dataProximaRevisao: Date) {
    this.minDate = new Date(`${vigencia}-01-01T00:00:00`);
    this.maxDate = new Date(`${vigencia}-12-31T23:59:59`);
    this.planoAcaoForm.get("dataProximaRevisao").patchValue(dataProximaRevisao);
  }

}
