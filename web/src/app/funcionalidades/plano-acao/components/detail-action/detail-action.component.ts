import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { LoadAcao } from '../../actions/plano-acao.actions';
import { Acao } from '../../models/acao.model';
import { DateControlLabelFactory } from '../../models/date-control-label/date-control-label.factory';
import { DateControlLabel } from '../../models/date-control-label/date-control-label.service';
import { PlanoAcao } from '../../models/plano-acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';

@Component({
  selector: 'sc-detail-action',
  templateUrl: './detail-action.component.html',
  styleUrls: ['./detail-action.component.scss']
})
export class DetailActionComponent implements OnInit {

  constructor(private store$: Store<fromPlanoAcao.State>, private datePipe: DatePipe, private currencyPipe: CurrencyPipe ) {}

  @Output() open: EventEmitter<any> = new EventEmitter<any>();
  @Output() openDetalhar: EventEmitter<any> = new EventEmitter<any>();
  @Input() acoes: Observable<Acao[]>;
  @Output() onExcluir: EventEmitter<any> = new EventEmitter<any>();
  @Input() planoAcao: Observable<PlanoAcao>;


  formatarDatas = (data1: Date, data2: Date) => data1 && data2 ? `${this.datePipe.transform(data1, 'dd/MM/yyyy')} | ${this.datePipe.transform(data2, 'dd/MM/yyyy')}` : '';
  formatarMoeda = (valor: any) => valor ? this.currencyPipe.transform(valor, 'BRL') : '';


  openActionBarDetalhar(id: string){
    this.openDetalhar.emit(id);
  }

  excluir(acao: Acao) {
    this.onExcluir.emit(acao);
  }

  openActionBar(tipo: number, acao: Acao, acaoPai: string) {
    if(acao && acao.id)
      this.store$.dispatch(new LoadAcao(acao.id));
    if(tipo == 2) {
      this.open.emit({tipo, acaoPai});
    }else {
      this.open.emit({tipo, acaoPai: null});
    }
  }

  ngOnInit() {
  }

  calculateDays(acao: Acao) {
    let dateControlLabel: DateControlLabel = DateControlLabelFactory.createSituacaoMenu(acao.situacao, acao.dataFim, acao.dataInicio, acao.dataRealFim, acao.dataRealInicio)
    return dateControlLabel.getLabel();
  }


  getSituacaoClass = (situacao: SituacaoAcaoType) => SituacaoAcaoType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoAcaoType) => SituacaoAcaoType.label(situacao);



}
