import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContainerAcoesComponent } from './containers/container-acoes/container-acoes.component';
import { ContainerExecutarPlanoComponent } from './containers/container-executar-plano/container-executar-plano.component';
import { ContainerPlanoAcaoComponent } from './containers/container-plano-acao/container-plano-acao.component';
import { ContainerRouterAcaoComponent } from './containers/container-router-acao/container-router-acao.component';
import { ContainerRouterPlanoAcao } from './containers/container-router-acao/container-router-plano-acao.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerPlanoAcaoComponent,
  },
  {
    path: '',
    component: ContainerRouterPlanoAcao,
    children: [
      {
        path: ':id',
        component: ContainerRouterAcaoComponent,
        children: [
          {
            path: 'executar',
            component: ContainerExecutarPlanoComponent,
            data: {
              breadcrumb: "Executar"
            }
          },
          {
            path: 'planejar',
            component: ContainerAcoesComponent,
            data: {
              breadcrumb: "Planejar"
            }
          }
        ]
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanoAcaoRoutingModule { }
