import { Action } from '@ngrx/store';
import { PlanoAcao } from '../models/plano-acao.model';
import { Acao } from '../models/acao.model';
import { ActionBarRef } from '@sicoob/ui';
import { SituacaoAcaoType } from '../models/situacao-acao.type';
import { TotalSituacaoVm } from '../../sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { AcaoPaginada } from '../models/acao-paginada.model';
import { PlanosPaginados } from '../models/planos-paginados.model';

export enum PlanoAcaoActionTypes {
  CriarPlanoAcao = '[CriarPlanoAcao]',
  CriarPlanoAcaoSuccess = '[CriarPlanoAcaoSuccess]',
  CriarPlanoAcaoFail = '[CriarPlanoAcaoFail]',
  LoadPlanoAcao = '[LoadPlanoAcao]',
  LoadPlanoAcaoSuccess = '[LoadPlanoAcaoSuccess]',
  LoadPlanoAcaoFail = '[LoadPlanoAcaoFail]',
  CriarAcao = '[CriarAcao]',
  CriarAcaoSuccess = '[CriarAcaoSuccess]',
  CriarAcaoFail = '[CriarAcaoFail]',
  CriarAcaoSuccessClose = '[CriarAcaoSuccessClose]',
  CriarAcaoESubTarefa = '[CriarAcaoESubTarefa]',
  CriarAcaoESubTarefaFail = '[CriarAcaoESubTarefaFail]',
  CriarAcaoESubTarefaSuccess = '[CriarAcaoESubTarefaSuccess]',
  ContarAcoes = '[ContarAcoes]',
  ContarAcoesSuccess = '[ContarAcoesSuccess]',
  ContarAcoesFail = '[ContarAcoesFail]',
  ContarExecutarAcoes = '[ContarExecutarAcoes]',
  ContarExecutarAcoesSuccess = '[ContarExecutarAcoesSuccess]',
  ContarExecutarAcoesFail = '[ContarExecutarAcoesFail]',
  ContarPlanosAcao = '[ContarPlanosAcao]',
  ContarPlanosAcaoSuccess = '[ContarPlanosAcaoSuccess]',
  ContarPlanosAcaoFail = '[ContarPlanosAcaoFail]',
  LoadAcao = '[LoadAcao]',
  LoadAcaoSuccess = '[LoadAcaoSuccess]',
  LoadAcaoFail = '[LoadAcaoFail]',
  ClearAcaoEditar = '[ClearAcaoEditar]',
  UpdateAcao = '[UpdateAcao]',
  UpdateAcaoSuccess = '[UpdateAcaoSuccess]',
  UpdateAcaoFail = '[UpdateAcaoFail]',
  LoadPlanosAcao = '[LoadPlanosAcao]',
  LoadPlanosAcaoSuccess = '[LoadPlanosAcaoSuccess]',
  LoadPlanosAcaoFail = '[LoadPlanosAcaoFail]',
  ExcluirAcao = '[ExcluirAcao]',
  ExcluirAcaoSuccess = '[ExcluirAcaoSuccess]',
  ExcluirAcaoFail = '[ExcluirAcaoFail]',
  ExcluirPlanoAcao = '[ExcluirPlanoAcao]',
  ExcluirPlanoAcaoSuccess = '[ExcluirPlanoAcaoSuccess]',
  ExcluirPlanoAcaoFail = '[ExcluirPlanoAcaoFail]',
  UpdatePlanoAcao = '[UpdatePlanoAcao]',
  UpdatePlanoAcaoSuccess = '[UpdatePlanoAcaoSuccess]',
  UpdatePlanoAcaoFail = '[UpdatePlanoAcaoFail]',
  IniciarPlanoAcao = '[IniciarPlanoAcao]',
  IniciarPlanoAcaoSuccess = '[IniciarPlanoAcaoSuccess]',
  IniciarPlanoAcaoFail = '[IniciarPlanoAcaoFail]',
  PararAcao = '[PararAcao]',
  PararAcaoFail = '[PararAcaoFail]',
  ExecutarAcao = '[ExecutarAcao]',
  ExecutarAcaoFail = '[ExecutarAcaoFail]',
  RetomarAcao = '[RetomarAcao]',
  RetomarAcaoFail = '[RetomarAcaoFail]',
  ConcluirAcao = '[ConcluirAcao]',
  ConcluirAcaoFail = '[ConcluirAcaoFail]',
  PararAcaoPlanejar = '[PararAcaoPlanejar]',
  PararAcaoPlanejarSuccess = '[PararAcaoPlanejarSuccess]',
  PararAcaoPlanejarFail = '[PararAcaoPlanejarFail]',
  ExecutarAcaoPlanejar = '[ExecutarAcaoPlanejar]',
  ExecutarAcaoPlanejarSuccess = '[ExecutarAcaoPlanejarSuccess]',
  ExecutarAcaoPlanejarFail = '[ExecutarAcaoPlanejarFail]',
  RetomarAcaoPlanejar = '[RetomarAcaoPlanejar]',
  RetomarAcaoPlanejarSuccess = '[RetomarAcaoPlanejarSuccess]',
  RetomarAcaoPlanejarFail = '[RetomarAcaoPlanejarFail]',
  ConcluirAcaoPlanejar = '[ConcluirAcaoPlanejar]',
  ConcluirAcaoPlanejarFail = '[ConcluirAcaoPlanejarFail]',
  ConcluirAcaoPlanejarSuccess = '[ConcluirAcaoPlanejarSuccess]',
  LoadAcoesExecutar = '[LoadAcoesExecutar]',
  LoadAcoesExecutarSuccess = '[LoadAcoesExecutarSuccess]',
  LoadAcoesExecutarFail = '[LoadAcoesExecutarFail]',
  LoadAcoesPlano = '[LoadAcoesPlano]',
  LoadAcoesPlanoSuccess = '[LoadAcoesPlanoSuccess]',
  LoadAcoesPlanoFail = '[LoadAcoesPlanoFail]',
  TotalPlanosAcao = '[TotalPlanosAcao]',
  TotalPlanosAcaoSuccess = '[TotalPlanosAcaoSuccess]',
  TotalPlanosAcaoFail = '[TotalPlanosAcaoFail]',
  ClearPlanoAcaoAtual = '[ClearPlanoAcaoAtual]',
  ClearTarefas = '[ClearTarefas]',
  ClearTarefasExecutar = '[ClearTarefasExecutar]',
  ClearPlanosAcao = '[ClearPlanosAcao]',
  ExecutarAcaoSuccess = '[ExecutarAcaoSuccess]'
}


export class TotalPlanosAcao implements Action {
  readonly type = PlanoAcaoActionTypes.TotalPlanosAcao;
  constructor(public responsavel?: string, public situacao?: SituacaoAcaoType, public numeroCooperativa?: string, public filtroNome?: string) { }
}
export class TotalPlanosAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.TotalPlanosAcaoSuccess;
  constructor(public total: number) { }
}
export class TotalPlanosAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.TotalPlanosAcaoFail;
  constructor(public erro: any) { }
}

export class CriarPlanoAcao implements Action {
  readonly type = PlanoAcaoActionTypes.CriarPlanoAcao;
  constructor(public planoAcao: PlanoAcao) { }
}
export class CriarPlanoAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.CriarPlanoAcaoSuccess;
  constructor(public planoAcao: PlanoAcao) { }
}
export class CriarPlanoAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.CriarPlanoAcaoFail;
  constructor(public erro: any) { }
}
export class LoadPlanoAcao implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanoAcao;
  constructor(public id: string, public situacao?: SituacaoAcaoType, public filtroNome?: string, public reponsavel?: string) { }
}
export class LoadPlanoAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanoAcaoSuccess;
  constructor(public planoAcao: PlanoAcao, public situacao?: SituacaoAcaoType, public filtroNome?: string) { }
}
export class LoadPlanoAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanoAcaoFail;
  constructor(public erro: any) { }
}
export class CriarAcao implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcao;
  constructor(public acao: Acao, public actionBarRef: ActionBarRef, public planoAcaoPai?: string) { }
}
export class CriarAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoSuccess;
  constructor(public acao: Acao, public actionBarRef: ActionBarRef) { }
}
export class CriarAcaoSuccessClose implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoSuccessClose;
  constructor() { }
}
export class CriarAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoFail;
  constructor(public erro: any, public actionBarRef: ActionBarRef) { }
}

export class CriarAcaoESubTarefa implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoESubTarefa;
  constructor(public acao: Acao, public actionBarRef: ActionBarRef, public planoAcaoPai?: string) { }
}
export class CriarAcaoESubTarefaSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoESubTarefaSuccess;
  constructor(public acaoPaginada: AcaoPaginada, public actionBarRef: ActionBarRef, public acaoPai: string) { }
}
export class CriarAcaoESubTarefaFail implements Action {
  readonly type = PlanoAcaoActionTypes.CriarAcaoESubTarefaFail;
  constructor(public erro: any) { }
}

export class ContarAcoes implements Action {
  readonly type = PlanoAcaoActionTypes.ContarAcoes;
  constructor(public planoAcaoPai?: string, public filtroNome?: string) { }
}
export class ContarAcoesSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ContarAcoesSuccess;
  constructor(public situacaoVm: TotalSituacaoVm[]) { }
}
export class ContarAcoesFail implements Action {
  readonly type = PlanoAcaoActionTypes.ContarAcoesFail;
  constructor(public erro: any) { }
}
export class ContarExecutarAcoes implements Action {
  readonly type = PlanoAcaoActionTypes.ContarExecutarAcoes;
  constructor(public planoAcaoPai?: string, public filtroNome?: string, public responsavel?: string) { }
}
export class ContarExecutarAcoesSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ContarExecutarAcoesSuccess;
  constructor(public situacaoVm: TotalSituacaoVm[]) { }
}
export class ContarExecutarAcoesFail implements Action {
  readonly type = PlanoAcaoActionTypes.ContarExecutarAcoesFail;
  constructor(public erro: any) { }
}

export class ContarPlanosAcao implements Action {
  readonly type = PlanoAcaoActionTypes.ContarPlanosAcao;
  constructor(public responsavel?: string, public situacao?: SituacaoAcaoType, public numeroCooperativa?: string, public filtroNome?: string, public vigencia?: string) { }
}
export class ContarPlanosAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ContarPlanosAcaoSuccess;
  constructor(public situacaoVm: TotalSituacaoVm[]) { }
}
export class ContarPlanosAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.ContarPlanosAcaoFail;
  constructor(public erro: any) { }
}
export class LoadAcao implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcao;
  constructor(public idAcao: string) { }
}
export class LoadAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcaoSuccess;
  constructor(public acao: Acao) { }
}
export class LoadAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcaoFail;
  constructor(public error: any) { }
}
export class ClearAcaoEditar implements Action {
  readonly type = PlanoAcaoActionTypes.ClearAcaoEditar;
  constructor() { }
}
export class UpdateAcao implements Action {
  readonly type = PlanoAcaoActionTypes.UpdateAcao;
  constructor(public action: Acao, public actionBarRef: ActionBarRef) { }
}
export class UpdateAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.UpdateAcaoSuccess;
  constructor(public acaoEditada: Acao, public actionBarRef: ActionBarRef) { }
}
export class UpdateAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.UpdateAcaoFail;
  constructor(public erro: any, public actionBarRef: ActionBarRef) { }
}
export class LoadPlanosAcao implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanosAcao;
  constructor(
    public page: PageModelVm,
    public responsavel?: string,
    public situacao?: SituacaoAcaoType,
    public numeroCooperativa?: string,
    public filtroNome?: string,
    public vigencia?: string) { }
}
export class LoadPlanosAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanosAcaoSuccess;
  constructor(public planosPaginados: PlanosPaginados, public numeroCooperativa: string, public filtroNome: string, public vigencia: string) { }
}
export class LoadPlanosAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.LoadPlanosAcaoFail;
  constructor(public error: any) { }
}
export class ExcluirAcao implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirAcao;
  constructor(public acao: Acao, public idPlanoAcaoPai: string) { }
} export class ExcluirAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirAcaoSuccess;
  constructor(public acao: Acao, public planoAcaoId: string, public acaoPai: Acao) { }
} export class ExcluirAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirAcaoFail;
  constructor(public error: any) { }
}
export class ExcluirPlanoAcao implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirPlanoAcao;
  constructor(public idPlanoAcao: string, public numeroCooperativa: string, public situacao: SituacaoAcaoType, public filtroNome: string) { }
} export class ExcluirPlanoAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirPlanoAcaoSuccess;
  constructor(public idPlanoAcao: string, public numeroCooperativa: string, public situacao: SituacaoAcaoType, public filtroNome: string) { }
} export class ExcluirPlanoAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.ExcluirPlanoAcaoFail;
  constructor(public error: any) { }
}
export class UpdatePlanoAcao implements Action {
  readonly type = PlanoAcaoActionTypes.UpdatePlanoAcao;
  constructor(public planoAcao: PlanoAcao, public situacao?: SituacaoAcaoType) { }
} export class UpdatePlanoAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.UpdatePlanoAcaoSuccess;
  constructor(public planoAcao: PlanoAcao) { }
} export class UpdatePlanoAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.UpdatePlanoAcaoFail;
  constructor(public error: any) { }
}
export class IniciarPlanoAcao implements Action {
  readonly type = PlanoAcaoActionTypes.IniciarPlanoAcao;
  constructor(public id: string) { }
} export class IniciarPlanoAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.IniciarPlanoAcaoSuccess;
  constructor(public planoAcao: PlanoAcao) { }
} export class IniciarPlanoAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.IniciarPlanoAcaoFail;
  constructor(public error: any) { }
}

export class ExecutarAcao implements Action {
  readonly type = PlanoAcaoActionTypes.ExecutarAcao;
  constructor(public situacaoAlvo: SituacaoAcaoType, public planoAcaoId: string, public acao: Acao, public responsavel: string, public origem: string, public situacao: SituacaoAcaoType, public nome: string) { }
} export class ExecutarAcaoFail implements Action {
  readonly type = PlanoAcaoActionTypes.ExecutarAcaoFail;
  constructor(public error: any) { }
} export class ExecutarAcaoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.ExecutarAcaoSuccess;
  constructor(public acao: Acao, public origem: string, public situacaoAlvo: SituacaoAcaoType ) { }
}


export class LoadAcoesExecutar implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesExecutar;
  constructor(public idPlanoAcao, public pageModelVm: PageModelVm, public responsavel?: string, public situacao?: SituacaoAcaoType, public nome?: string) { }
}
export class LoadAcoesExecutarSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesExecutarSuccess;
  constructor(public idPlanoAcao: string, public acoesPaginadas: AcaoPaginada, public responsavel?: string, public situacao?: SituacaoAcaoType, public nome?: string) { }
}
export class LoadAcoesExecutarFail implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesExecutarFail;
  constructor(public error: any) { }
}

export class LoadAcoesPlano implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesPlano;
  constructor(public idPlanoAcao, public pageModelVm: PageModelVm, public responsavel?: string, public situacao?: SituacaoAcaoType, public nome?: string) { }
}
export class LoadAcoesPlanoSuccess implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesPlanoSuccess;
  constructor(public idPlanoAcao: string, public acoesPaginadas: AcaoPaginada, public responsavel?: string, public situacao?: SituacaoAcaoType, public nome?: string) { }
}
export class LoadAcoesPlanoFail implements Action {
  readonly type = PlanoAcaoActionTypes.LoadAcoesPlanoFail;
  constructor(public error: any) { }
}


export class ClearPlanoAcaoAtual implements Action {
  readonly type = PlanoAcaoActionTypes.ClearPlanoAcaoAtual;
  constructor() { }
}
export class ClearTarefas implements Action {
  readonly type = PlanoAcaoActionTypes.ClearTarefas;
  constructor() { }
}
export class ClearTarefasExecutar implements Action {
  readonly type = PlanoAcaoActionTypes.ClearTarefasExecutar;
  constructor() { }
}
export class ClearPlanosAcao implements Action {
  readonly type = PlanoAcaoActionTypes.ClearPlanosAcao;
  constructor() { }
}




export type PlanoAcaoActions =
  CriarPlanoAcao
  | CriarPlanoAcaoSuccess
  | CriarPlanoAcaoFail
  | LoadPlanoAcao
  | LoadPlanoAcaoSuccess
  | LoadPlanoAcaoFail
  | CriarAcao
  | CriarAcaoSuccess
  | CriarAcaoFail
  | CriarAcaoSuccessClose
  | CriarAcaoESubTarefa
  | CriarAcaoESubTarefaFail
  | CriarAcaoESubTarefaSuccess
  | ContarAcoesFail
  | ContarAcoesSuccess
  | ContarAcoes
  | ContarPlanosAcaoFail
  | ContarPlanosAcaoSuccess
  | ContarPlanosAcao
  | LoadAcao
  | LoadAcaoSuccess
  | LoadAcaoFail
  | ClearAcaoEditar
  | UpdateAcao
  | UpdateAcaoSuccess
  | UpdateAcaoFail
  | LoadPlanosAcao
  | LoadPlanosAcaoSuccess
  | LoadPlanosAcaoFail
  | ExcluirAcao
  | ExcluirAcaoSuccess
  | ExcluirAcaoFail
  | ExcluirPlanoAcao
  | ExcluirPlanoAcaoSuccess
  | ExcluirPlanoAcaoFail
  | UpdatePlanoAcao
  | UpdatePlanoAcaoSuccess
  | UpdatePlanoAcaoFail
  | IniciarPlanoAcao
  | IniciarPlanoAcaoSuccess
  | IniciarPlanoAcaoFail
  | ExecutarAcao
  | ExecutarAcaoFail
  | LoadAcoesExecutar
  | LoadAcoesExecutarSuccess
  | LoadAcoesExecutarFail
  | ContarExecutarAcoes
  | ContarExecutarAcoesSuccess
  | ContarExecutarAcoesFail
  | LoadAcoesPlano
  | LoadAcoesPlanoSuccess
  | LoadAcoesPlanoFail
  | TotalPlanosAcao
  | TotalPlanosAcaoSuccess
  | TotalPlanosAcaoFail
  | ClearPlanoAcaoAtual
  | ClearTarefas
  | ClearTarefasExecutar
  | ClearPlanosAcao
  | ExecutarAcaoSuccess;
