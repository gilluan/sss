import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPlanoAcaoComponent } from './container-plano-acao.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerPlanoAcaoComponent', () => {
  let component: ContainerPlanoAcaoComponent;
  let fixture: ComponentFixture<ContainerPlanoAcaoComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerPlanoAcaoComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPlanoAcaoComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
