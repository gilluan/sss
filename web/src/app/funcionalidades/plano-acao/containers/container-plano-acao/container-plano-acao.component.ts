import { TemplatePortal } from '@angular/cdk/portal';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CooperativaFiltro } from '@app/funcionalidades/sins-relatorios/models/cooperativa-filtro.model';
import * as fromUsuarioInstituto from '@app/reducers/usuario-instituto.reducer';
import { PageModelVm } from '@app/shared/models/page.model';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService, HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AjustarPadding, DisplayToolbar } from 'src/app/app.actions';
import { TotalSituacaoVm } from 'src/app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { DialogoConfirmacaoComponent } from 'src/app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import * as fromApp from '../../../../app.reduce';
import { ClearPlanosAcao, ExcluirPlanoAcao, LoadPlanosAcao } from '../../actions/plano-acao.actions';
import { ActionbarFiltroPlanosComponent } from '../../components/actionbar-filtro-planos/actionbar-filtro-planos.component';
import { DialogoNovoPlanoAcaoComponent } from '../../components/dialogo-novo-plano-acao/dialogo-novo-plano-acao.component';
import { FiltroAcoesComponent } from '../../components/filtro-acoes/filtro-acoes.component';
import { PlanoAcao } from '../../models/plano-acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { CarregarCooperativas } from '@app/actions/usuario-instituto.actions';
import { PerfilType } from '@app/shared/types/perfil.type';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { UsuarioInstitutoService } from '@app/shared/services/usuario-instituto.service';

@Component({
  selector: 'sc-container-plano-acao',
  templateUrl: './container-plano-acao.component.html',
  styleUrls: ['./container-plano-acao.component.css']
})


export class ContainerPlanoAcaoComponent implements OnInit, OnDestroy {

  @ViewChild("filtroAcoes") filtroAcoesComponent: FiltroAcoesComponent;

  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;

  modalRef: ModalRef;
  atualizarPlanoModal: ModalRef;
  modalRefExcluirPlanoAcao: ModalRef;
  planosAcao$: Observable<PlanoAcao[]>;
  idResponsavel: string;
  situacaoFiltro: SituacaoAcaoType;
  counters$: Observable<TotalSituacaoVm[]>;
  cooperativasObservable: Observable<CooperativaFiltro[]>;
  nomeFiltro: string;
  vigencia: string;
  numeroCooperativa: string;
  navegation: Subscription;
  paginacaoAtualSub: Subscription;
  paginacaoAtual: PageModelVm;
  filtroActionBarRef: ActionBarRef;

  constructor(
    private store$: Store<fromPlanoAcao.State>,
    public appStore$: Store<fromApp.State>,
    private modalService: ModalService,
    private router: Router,
    private persistenceService: PersistanceService,
    private headerActionsService: HeaderActionsContainerService,
    private readonly storeUsuarioInstituto: Store<fromUsuarioInstituto.State>,
    private readonly actionBarService: ActionBarService,
    private readonly usuarioInstitutoService: UsuarioInstitutoService,
  ) { }


  usuarioinstituto: any;

  ngOnInit(): void {
    this.paginacaoAtualSub = this.store$.pipe(select(fromPlanoAcao.getPaginacaoAtualPlanos)).subscribe(pg => this.paginacaoAtual = pg);
    this.usuarioinstituto = this.persistenceService.get("usuario_instituto");
    this.idResponsavel = this.usuarioinstituto.id;
    this.numeroCooperativaFixo(this.usuarioinstituto)
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.appStore$.dispatch(new AjustarPadding(20));
    this.store$.dispatch(new LoadPlanosAcao(this.paginacaoAtual, null, null, this.numeroCooperativa, null, this.vigencia));
    this.planosAcao$ = this.store$.pipe(select(fromPlanoAcao.getTodos));
    this.counters$ = this.store$.pipe(select(fromPlanoAcao.getTotalSituacaoPlanoAcao));

  }

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  onScroll() {
    if (this.paginacaoAtual.pageNumber < this.paginacaoAtual.totalPages) {
      this.paginacaoAtual.pageNumber++;
      let situacao = this.situacaoFiltro == SituacaoAcaoType.todas ? null : this.situacaoFiltro;
      this.store$.dispatch(new LoadPlanosAcao(this.paginacaoAtual, null, situacao, this.numeroCooperativa, this.nomeFiltro, this.vigencia));
    }
  }


  onChangeTab($event) {
    this.store$.dispatch(new ClearPlanosAcao());
    this.situacaoFiltro = $event;
    this.paginacaoAtual = new PageModelVm(0, 8, 0);
    this.store$.dispatch(new LoadPlanosAcao(this.paginacaoAtual, null, $event, this.numeroCooperativa, this.nomeFiltro, this.vigencia));
  }

  onSearch($event: string) {
    this.store$.dispatch(new ClearPlanosAcao());
    this.irParaTodas();
    this.nomeFiltro = $event;
    this.paginacaoAtual = new PageModelVm(0, 8, 0);
    this.store$.dispatch(new LoadPlanosAcao(this.paginacaoAtual, null, this.situacaoFiltro, this.numeroCooperativa, $event, this.vigencia));
  }

  onClear() {
    this.store$.dispatch(new ClearPlanosAcao());
    this.irParaTodas();
    this.situacaoFiltro = SituacaoAcaoType.todas;
    this.paginacaoAtual = new PageModelVm(0, 8, 0);
    this.store$.dispatch(new LoadPlanosAcao(this.paginacaoAtual, null, null, this.numeroCooperativa, this.nomeFiltro, this.vigencia));
  }

  onClearCooperativas() {
    this.numeroCooperativaFixo(this.usuarioinstituto);
    this.onClear();
  }

  onClearVigencia() {
    this.vigencia = null;
    this.onClear();
  }

  onClearNomeFiltro() {
    this.nomeFiltro = null;
    this.onClear();
  }

  ngOnDestroy(): void {
    if (this.modalRef)
      this.modalRef.close();
    if (this.atualizarPlanoModal)
      this.atualizarPlanoModal.close();
    if (this.modalRefExcluirPlanoAcao)
      this.modalRefExcluirPlanoAcao.close();
    if (this.navegation)
      this.navegation.unsubscribe();
    if (this.paginacaoAtualSub)
      this.paginacaoAtualSub.unsubscribe();
    if (this.headerActionsService)
      this.headerActionsService.remove();
    if (this.filtroActionBarRef)
      this.filtroActionBarRef.close();

    this.store$.dispatch(new ClearPlanosAcao());
  }

  abrirCriar() {
    this.modalRef = this.modalService.open(DialogoNovoPlanoAcaoComponent, {
      data: {},
      panelClass: 'sins-modal-plano-acao'
    });
    this.modalRef.afterClosed().subscribe((data: any) => {
      if (data.sucesso) {
        this.navegation = this.store$.pipe(select(fromPlanoAcao.getPlanoAtual)).subscribe((a) => {
          if (a) {
            if (a.id) {
              this.router.navigate([`/plano-acao/${a.id}/planejar`]);
            }
          }
        });
      }
    });
  }

  onExcluirPlano(planoAcao: PlanoAcao) {
    this.modalRefExcluirPlanoAcao = this.modalService.open(DialogoConfirmacaoComponent, {
      data: {
        header: "Excluir Plano de Ação"
      },
      panelClass: 'sins-modal-confirm'
    });

    this.modalRefExcluirPlanoAcao.afterClosed().subscribe(
      (confirmacao) => {
        if (confirmacao) {
          //this.irParaTodas();
          this.store$.dispatch(new ExcluirPlanoAcao(planoAcao.id, this.numeroCooperativa, this.situacaoFiltro, this.nomeFiltro));
        }
      }
    )
  }

  atualizarPlanoAcao($event) {
    if ($event) {
      this.atualizarPlanoModal = this.modalService.open(DialogoNovoPlanoAcaoComponent, {
        data: {
          planoAcao: $event,
          situacao: this.situacaoFiltro
        },
        panelClass: 'sins-modal-plano-acao'
      });
    }
  }

  abrirFiltroActionBar(): void {
    this.carregarCooperativas();
    let cooperativasObservable = this.storeUsuarioInstituto.pipe(
      select(fromUsuarioInstituto.selectCooperativas),
      map((numerosCooperativas: string[]) => numerosCooperativas)
    );
    let responsaveis$ = this.usuarioInstitutoService.findAll(null, null, null);
    responsaveis$ = this.tratarResponsaveis(responsaveis$);
    const config: ActionBarConfig = {
      data: {
        cooperativas: cooperativasObservable,
        responsaveis: responsaveis$,
        filtros: {
          numeroCooperativa: this.numeroCooperativa,
          nome: this.nomeFiltro,
          usuarioInstituto: this.usuarioinstituto,
          vigencia: this.vigencia
        }
      },
      panelClass: 'action-bar-filtro-planos'
    };
    this.filtroActionBarRef = this.actionBarService.open(ActionbarFiltroPlanosComponent, config);
    const subscription: Subscription = this.filtroActionBarRef.afterClosed()
      .subscribe(data => this.filtrarActionBarAfterClose(data, subscription));
  }

  private tratarResponsaveis = (responsaveis$) => {
    return responsaveis$.pipe(map((users: any) => {
      let usuarios: UsuarioInstituto[] = []
      users.forEach(u => {
        let us = new UsuarioInstituto();
        us.id = u._id
        us.nome = u.nome
        usuarios.push(us);
      });
      return usuarios;
    }))
  }

  private filtrarActionBarAfterClose(data: any, subscription: Subscription): void {
    if (data) {
      if (data.action === 'filtrar') {
        this.numeroCooperativa = data.data;
        this.nomeFiltro = data.filtroNome;
        this.vigencia = data.vigencia;
        this.onSearch(this.nomeFiltro);
        subscription.unsubscribe()
      }
    }
  }

  carregarCooperativas(): void {
    this.storeUsuarioInstituto.dispatch(new CarregarCooperativas());
  }

  irParaTodas = () => this.filtroAcoesComponent ? this.filtroAcoesComponent.mudarAba(SituacaoAcaoType.todas) : null;

  numeroCooperativaFixo = (usuarioinstituto) => {
    if (usuarioinstituto.perfil != PerfilType.ppe && usuarioinstituto.perfil != PerfilType.gestor)
      this.numeroCooperativa = usuarioinstituto.numeroCooperativa;
    else
      this.numeroCooperativa = null;
  }


}
