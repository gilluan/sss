import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageModelVm } from '@app/shared/models/page.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { AjustarPadding, DisplayToolbar } from 'src/app/app.actions';
import { TotalSituacaoVm } from 'src/app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { DialogoConfirmacaoComponent } from 'src/app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import * as fromApp from '../../../../app.reduce';
import { ClearPlanoAcaoAtual, ClearTarefas, ContarExecutarAcoes, ExcluirAcao, LoadAcao, LoadAcoesPlano } from '../../actions/plano-acao.actions';
import { ActionBarCadastroTarefaComponent } from '../../components/action-bar-cadastro-tarefa/action-bar-cadastro-tarefa.component';
import { ActionBarDetalheAcaoComponent } from '../../components/action-bar-detalhe-acao/action-bar-detalhe-acao.component';
import { DialogoNovoPlanoAcaoComponent } from '../../components/dialogo-novo-plano-acao/dialogo-novo-plano-acao.component';
import { FiltroAcoesComponent } from '../../components/filtro-acoes/filtro-acoes.component';
import { Acao } from '../../models/acao.model';
import { PlanoAcao } from '../../models/plano-acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';

@Component({
  selector: 'sc-container-acoes',
  templateUrl: './container-acoes.component.html',
  styleUrls: ['./container-acoes.component.scss']
})
export class ContainerAcoesComponent implements OnInit, OnDestroy {

  @ViewChild("filtroAcoes") filtroAcoesComponent: FiltroAcoesComponent;


  anoAtual = new Date().getFullYear().toString();
  counters$: Observable<TotalSituacaoVm[]>;
  nomeFiltro: string;
  planoAcao: Observable<PlanoAcao>;
  acoes: Observable<Acao[]>;
  acoesExecutar: TotalSituacaoVm;
  idPlano: string;
  actionBarRef: ActionBarRef;
  modalConfirmRef: ModalRef;
  modalRefIniciar: ModalRef;
  modalRefExcluirPlanoAcao: ModalRef;
  modalRefExcluirAcao: ModalRef;
  situacaoAtual: SituacaoAcaoType;
  usuarioinstituto: any;
  acoesExecutarSub: Subscription;
  paginacaoAtualAcoesSub: Subscription;
  paginacaoAtualAcoes: PageModelVm;

  constructor(
    private store$: Store<fromPlanoAcao.State>,
    public appStore$: Store<fromApp.State>,
    private actionBarService: ActionBarService,
    private route: ActivatedRoute,
    private modalService: ModalService,
    private persistenceService: PersistanceService,
  ) { }

  ngOnInit() {
    this.paginacaoAtualAcoesSub = this.store$.pipe(select(fromPlanoAcao.getPaginacaoAtualAcoes)).subscribe(pg => this.paginacaoAtualAcoes = pg);
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.appStore$.dispatch(new AjustarPadding(0));
    this.usuarioinstituto = this.persistenceService.get("usuario_instituto");
    this.route.parent.paramMap.subscribe(params => {
      this.idPlano = params.get("id");
      this.store$.dispatch(new ContarExecutarAcoes(this.idPlano, null, this.usuarioinstituto.id));
      this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes));
    })
    this.counters$ = this.store$.pipe(select(fromPlanoAcao.getTotalSituacao));
    this.acoes = this.store$.pipe(select(fromPlanoAcao.getAcoesPlano));
    this.acoesExecutarSub = this.store$.pipe(select(fromPlanoAcao.getTotalSituacaoExecutar)).subscribe(
      (totalVm: TotalSituacaoVm[]) => this.acoesExecutar = totalVm.filter(s => s.situacao == SituacaoAcaoType.todas)[0]
    )
    this.planoAcao = this.store$.pipe(select(fromPlanoAcao.getPlanoAtual));
  }

  onScroll() {
    if( this.paginacaoAtualAcoes.pageNumber < this.paginacaoAtualAcoes.totalPages) {
      this.paginacaoAtualAcoes.pageNumber++;
      this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes, null, this.situacaoAtual, this.nomeFiltro));
    }
  }
  onScrollUp() {
    if( this.paginacaoAtualAcoes.pageNumber >= 1) {
      this.paginacaoAtualAcoes.pageNumber--;
      this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes));
    }
  }
  ngOnDestroy(): void {
    if (this.actionBarRef) {
      this.actionBarRef.close();
    }
    if (this.modalRefIniciar) {
      this.modalRefIniciar.close();
    }
    if (this.modalRefExcluirPlanoAcao) {
      this.modalRefExcluirPlanoAcao.close();
    }
    if (this.modalRefExcluirAcao) {
      this.modalRefExcluirAcao.close();
    }
    if (this.modalConfirmRef) {
      this.modalConfirmRef.close();
    }
    if (this.acoesExecutarSub) {
      this.acoesExecutarSub.unsubscribe()
    }
    if(this.paginacaoAtualAcoesSub) {
      this.paginacaoAtualAcoesSub.unsubscribe();
    }
    this.appStore$.dispatch(new AjustarPadding(20));
    this.store$.dispatch(new ClearPlanoAcaoAtual());
    this.store$.dispatch(new ClearTarefas());
  }

  onSearch($event: string) {
    this.nomeFiltro = $event;
    this.irParaTodas();
    this.paginacaoAtualAcoes = new PageModelVm(0, 5, 0);
    this.store$.dispatch(new ClearTarefas());
    this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes, null, this.situacaoAtual, $event));
  }

  onClear() {
    this.situacaoAtual = SituacaoAcaoType.todas;
    this.nomeFiltro = null;
    this.irParaTodas();
    this.paginacaoAtualAcoes = new PageModelVm(0, 5, 0);
    this.store$.dispatch(new ClearTarefas());
    this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes));
  }

  onChangeTab($event) {
    this.situacaoAtual = $event;
    this.paginacaoAtualAcoes = new PageModelVm(0, 5, 0);
    this.store$.dispatch(new ClearTarefas());
    this.store$.dispatch(new LoadAcoesPlano(this.idPlano, this.paginacaoAtualAcoes, null, $event, this.nomeFiltro));
  }

  abrirActionBar(event: any) {

    const config: ActionBarConfig = {};
    config.data = { tipo: event.tipo, planoAcao: this.idPlano, acaoPai: event.acaoPai };
    config.panelClass = 'action-bar-cadastro-tarefa'
    if(event.tipo == 1)
      this.irParaTodas();
    if (this.actionBarRef == null || this.actionBarRef === undefined) {
      this.actionBarRef = this.actionBarService.open(ActionBarCadastroTarefaComponent, config);
      this.actionBarRef.afterClosed().subscribe(() => {
        this.actionBarRef = undefined;
      });
    }
  }

  atualizarPlanoAcao() {
    let sub = this.store$.pipe(select(fromPlanoAcao.getPlanoAtual)).subscribe(
      (planoAcao: PlanoAcao) => {
        if (planoAcao) {
          this.modalConfirmRef = this.modalService.open(DialogoNovoPlanoAcaoComponent, {
            data: {
              planoAcao
            },
            panelClass: 'sins-modal-plano-acao'
          });
        }
      });
    sub.unsubscribe();
  }

  excluir(acao: Acao) {
    this.modalRefExcluirAcao = this.modalService.open(DialogoConfirmacaoComponent, {
      data: {
        header: "Excluir ação"
      },
      panelClass: 'sins-modal-confirm'
    });

    this.modalRefExcluirAcao.afterClosed().subscribe(
      (confirmacao) => {
        if (confirmacao) this.store$.dispatch(new ExcluirAcao(acao, acao.planoAcaoPai ? acao.planoAcaoPai : this.idPlano))
      }
    )
  }

  irParaTodas = () => this.filtroAcoesComponent ? this.filtroAcoesComponent.mudarAba(SituacaoAcaoType.todas) : null;

  abrirActionBarDetalhes(event: any) {
    this.store$.dispatch(new LoadAcao(event));
    const config: ActionBarConfig = {};
    config.data = { origem: 'planejar', situacaoAtual: this.situacaoAtual, nomeFiltro: this.nomeFiltro };
    config.panelClass = 'action-bar-detalhe-acao'
    if (!this.actionBarRef) {
      this.actionBarRef = this.actionBarService.open(ActionBarDetalheAcaoComponent, config);
      this.actionBarRef.afterClosed().subscribe(() => {
        this.actionBarRef = undefined;
      });
    }
  }



}
