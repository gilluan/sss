import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { AjustarPadding, DisplayToolbar } from 'src/app/app.actions';
import { JustificativaAjustesService } from 'src/app/funcionalidades/sins-analise-cadastros/justificativa-ajustes.service';
import { JustificativaAjusteVm } from 'src/app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';
import { TotalSituacaoVm } from 'src/app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { DialogoJustificativaAjustesComponent } from 'src/app/shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import * as fromApp from '../../../../app.reduce';
import {   LoadAcao, LoadAcoesExecutar, ClearTarefasExecutar, ExecutarAcao } from '../../actions/plano-acao.actions';
import { ActionBarDetalheAcaoComponent } from '../../components/action-bar-detalhe-acao/action-bar-detalhe-acao.component';
import { DialogoConcluirAcao } from '../../components/dialogo-concluir-acao/dialogo-concluir-acao.component';
import { FiltroAcoesComponent } from '../../components/filtro-acoes/filtro-acoes.component';
import { Acao } from '../../models/acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { PageModelVm } from '@app/shared/models/page.model';
import {JustificativaType} from '@shared/models/justificativa.type'
import { PlanoAcao } from '../../models/plano-acao.model';
@Component({
  selector: 'sc-container-executar-plano',
  templateUrl: './container-executar-plano.component.html',
  styleUrls: ['./container-executar-plano.component.scss']
})
export class ContainerExecutarPlanoComponent implements OnInit, OnDestroy {

  @ViewChild("filtroAcoes") filtroAcoesComponent: FiltroAcoesComponent;

  onScroll() {
    if( this.paginacaoAtualAcoes.pageNumber < this.paginacaoAtualAcoes.totalPages) {
      this.paginacaoAtualAcoes.pageNumber++;
      this.store$.dispatch(new LoadAcoesExecutar(this.idPlano, this.paginacaoAtualAcoes, this.usuarioInstituto.id, this.situacaoAtual, this.nomeFiltro));
    }
  }

  constructor(
    private store$: Store<fromPlanoAcao.State>,
    public appStore$: Store<fromApp.State>,
    private route: ActivatedRoute,
    private router: Router,
    private persistenceService: PersistanceService,
    private modalService: ModalService,
    private justificativaService: JustificativaAjustesService,
    private actionBarService: ActionBarService,
  ) { }

  actionBarRef: ActionBarRef
  acoes: Observable<Acao[]>;
  idPlano: string;
  nomeFiltro: string;
  counters$: Observable<TotalSituacaoVm[]>;
  usuarioInstituto: any;
  modalJustificativa: ModalRef;
  modalConcluirAcao: ModalRef;
  situacaoAtual: SituacaoAcaoType;
  origem: string;
  paginacaoAtualAcoesSub: Subscription;
  paginacaoAtualAcoes: PageModelVm;

  ngOnInit(): void {
    this.store$.pipe(select(fromPlanoAcao.getPlanoAtual)).subscribe((planoAcaoAtual: PlanoAcao) => {
      if(planoAcaoAtual && planoAcaoAtual.vigencia !== new Date().getFullYear().toString() ) {
        let paths = this.router.url.split('/');
        paths[3] = 'planejar';
        this.router.navigate([paths.join('/')]);
      }
    });
    this.paginacaoAtualAcoesSub = this.store$.pipe(select(fromPlanoAcao.getPaginacaoAtualAcoesExecutar)).subscribe(pg => this.paginacaoAtualAcoes = pg);
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.appStore$.dispatch(new AjustarPadding(0));
    this.usuarioInstituto = this.persistenceService.get("usuario_instituto");
    this.route.parent.paramMap.subscribe(params => {
      this.idPlano = params.get("id");
      this.store$.dispatch(new LoadAcoesExecutar(this.idPlano, this.paginacaoAtualAcoes, this.usuarioInstituto.id));
    })
    this.origem = 'executar'
    this.acoes = this.store$.pipe(select(fromPlanoAcao.getAcoesExecutar));
    this.counters$ = this.store$.pipe(select(fromPlanoAcao.getTotalSituacaoExecutar));
  }

  parar($event) {
    this.modalJustificativa = this.modalService.open(DialogoJustificativaAjustesComponent,
      {
        data: {
          title: 'Pausar tarefa por impedimento',
          label: 'Descreva abaixo o impedimento que encontrou ao executar a tarefa:'
        },
        panelClass: 'sins-modal-justificativa'
      }
    );
    this.modalJustificativa.afterClosed().subscribe(
      (retorno: any) => {
        if (retorno.sucesso) {
          let justificativaAjuste = new JustificativaAjusteVm();
          justificativaAjuste.justificativa = retorno.justificativaAjusteText;
          justificativaAjuste.idAcao = $event.id;
          justificativaAjuste.idVoluntarioCriador = this.persistenceService.get("usuario_instituto").id;
          justificativaAjuste.tipo = JustificativaType.impedimentoAcao;
          this.justificativaService.salvarJustificativa(justificativaAjuste).subscribe(
            _ => this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.impedida ,this.idPlano, $event, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro))
          );
        }
      }
    )
  }

  iniciar($event) {
    $event.dataRealInicio = new Date(Date.now());
    this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.iniciada, this.idPlano, $event, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro));
  }
  retomar($event) {
    this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.iniciada, this.idPlano, $event, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro));
  }

  concluir($event) {
    this.modalConcluirAcao = this.modalService.open(DialogoConcluirAcao,
      {
        data: {acao: $event},
        panelClass: 'sins-modal-success'
      }
    );
    this.modalConcluirAcao.afterClosed().subscribe(
      (investimento: number) => {
        if (investimento >= 0.0) {
          $event.investimentoReal = investimento;
          $event.dataRealFim = new Date(Date.now());
          this.store$.dispatch(new ExecutarAcao(SituacaoAcaoType.concluida, this.idPlano, $event, this.usuarioInstituto.id, this.origem, this.situacaoAtual, this.nomeFiltro));
        }
      }
    )
  }

  ngOnDestroy(): void {
    if (this.modalJustificativa)
      this.modalJustificativa.close();
    if (this.modalConcluirAcao)
      this.modalConcluirAcao.close();
    if (this.actionBarRef)
      this.actionBarRef.close();
    if(this.paginacaoAtualAcoesSub)
      this.paginacaoAtualAcoesSub.unsubscribe()
    this.store$.dispatch(new ClearTarefasExecutar());
    this.appStore$.dispatch(new AjustarPadding(20));
  }

  onSearch($event: string) {
    this.irParaTodas();
    this.nomeFiltro = $event;
    this.store$.dispatch(new ClearTarefasExecutar());
    this.store$.dispatch(new LoadAcoesExecutar(this.idPlano, new PageModelVm(0, 5, 0), this.usuarioInstituto.id, this.situacaoAtual, $event));
  }

  onClear() {
    this.situacaoAtual = SituacaoAcaoType.todas;
    this.nomeFiltro = null;
    this.irParaTodas();
    this.store$.dispatch(new ClearTarefasExecutar());
    this.store$.dispatch(new LoadAcoesExecutar(this.idPlano, new PageModelVm(0, 5, 0), this.usuarioInstituto.id));
  }

  onChangeTab($event) {
    this.situacaoAtual = $event;
    this.store$.dispatch(new ClearTarefasExecutar());
    this.store$.dispatch(new LoadAcoesExecutar(this.idPlano, new PageModelVm(0, 5, 0), this.usuarioInstituto.id, $event, this.nomeFiltro));
  }

  irParaTodas = () => this.filtroAcoesComponent ? this.filtroAcoesComponent.mudarAba(SituacaoAcaoType.todas) : null;

  abrirActionBarDetalhes(event: any) {
    this.store$.dispatch(new LoadAcao(event));
    const config: ActionBarConfig = {};
    config.data = { origem: 'executar', situacaoAtual: this.situacaoAtual, nomeFiltro: this.nomeFiltro };
    config.panelClass = 'action-bar-detalhe-acao'
    if (this.actionBarRef == null || this.actionBarRef === undefined) {
      this.actionBarRef = this.actionBarService.open(ActionBarDetalheAcaoComponent, config);
      this.actionBarRef.afterClosed().subscribe(() => {
        this.actionBarRef = undefined;
      });
    }
  }


}
