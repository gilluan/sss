import { Component } from '@angular/core';

@Component({
  selector: 'sc-container-router-plano-acao',
  template: `<router-outlet></router-outlet>`
})
export class ContainerRouterPlanoAcao {

  constructor() { }


}

