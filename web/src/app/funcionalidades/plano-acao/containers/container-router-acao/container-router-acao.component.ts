import { TemplatePortal } from '@angular/cdk/portal';
import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TotalSituacaoVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { AjustarPadding, DisplayToolbar } from 'src/app/app.actions';
import { DialogoConfirmacaoComponent } from 'src/app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import * as fromApp from '../../../../app.reduce';
import { ExcluirPlanoAcao, IniciarPlanoAcao, LoadPlanoAcao, ContarExecutarAcoes, ClearPlanoAcaoAtual, ClearAcaoEditar, ClearTarefas, ClearTarefasExecutar, UpdatePlanoAcao } from '../../actions/plano-acao.actions';
import { DialogoNovoPlanoAcaoComponent } from '../../components/dialogo-novo-plano-acao/dialogo-novo-plano-acao.component';
import { PlanoAcao } from '../../models/plano-acao.model';
import { SituacaoAcaoType } from '../../models/situacao-acao.type';
import * as fromPlanoAcao from '../../reducers/plano-acao.reducer';
import { AcoesPlano } from './type/acoes-plano.enum';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'sc-container-router-acao',
  templateUrl: './container-router-acao.component.html',
  styleUrls: ['./container-router-acao.component.scss']
})

export class ContainerRouterAcaoComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;
  planoAcao: Observable<PlanoAcao>;
  idPlano: string;
  modalRefIniciar: ModalRef;
  modalConfirmRef: ModalRef;
  modalRefExcluirPlanoAcao: ModalRef;
  acoesExecutar: TotalSituacaoVm;
  acoesExecutarSub: Subscription;
  usuarioinstituto: any;
  isScroll$: Observable<boolean>;
  acoesPlano: AcoesPlano[] = AcoesPlano.values();

  subscription: Subscription = new Subscription();

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  constructor(
    private store$: Store<fromPlanoAcao.State>,
    private route: ActivatedRoute,
    public appStore$: Store<fromApp.State>,
    private modalService: ModalService,
    private headerActionsService: HeaderActionsContainerService,
    private persistenceService: PersistanceService,
    private changeDetector: ChangeDetectorRef,
    public router: Router
  ) { }

  onEditarPlano(planoAcao) {
    planoAcao.situacao = SituacaoAcaoType.planejando;
    //planoAcao.iniciado = false;
    this.store$.dispatch(new UpdatePlanoAcao(planoAcao, SituacaoAcaoType.planejando));
  }

  ngOnInit(): void {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.appStore$.dispatch(new AjustarPadding(0));
    this.isScroll$ = this.appStore$.pipe(select(fromApp.selectScrollContainer));
   /*  this.subscription.add(this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
      if (this.router.url.indexOf('planejar') < 0 && this.router.url.indexOf('executar') < 0) {
        this.router.navigate([this.router.url.concat('/planejar')]);
      }
    })); */

    this.usuarioinstituto = this.persistenceService.get("usuario_instituto");
    this.route.paramMap.subscribe(params => {
      this.idPlano = params.get("id");
      this.store$.dispatch(new LoadPlanoAcao(this.idPlano));
      this.store$.dispatch(new ContarExecutarAcoes(this.idPlano, null, this.usuarioinstituto.id));
    })
    this.acoesExecutarSub = this.store$.pipe(select(fromPlanoAcao.getTotalSituacaoExecutar)).subscribe(
      (totalVm: TotalSituacaoVm[]) => {
        this.acoesExecutar = totalVm.filter(s => s.situacao == SituacaoAcaoType.todas)[0]
        this.changeDetector.detectChanges()
      }
    )
    this.planoAcao = this.store$.pipe(select(fromPlanoAcao.getPlanoAtual));

  }

  async iniciarPlanoAcao() {
    this.modalRefIniciar = this.modalService.open(DialogoConfirmacaoComponent, {
      data: {
        header: "Atenção",
        pergunta: "Já verificou se todas as ações foram realmente inseridas no plano de ação? Verifique também com a sua equipe! Para replanejar o plano após finalizar o planejamento, selecione: Outras Opções > Alterar dados",
        txtNao: "Cancelar",
        txtSim: 'Continuar'
      },
      panelClass: 'sins-modal-confirm'
    });
    this.modalRefIniciar.afterClosed().subscribe(
      (confirmacao) => {
        if (confirmacao) {
          this.store$.dispatch(new ContarExecutarAcoes(this.idPlano, null, this.usuarioinstituto.id));
          this.store$.dispatch(new IniciarPlanoAcao(this.idPlano));
        }
      }
    )
  }

  atualizarPlanoAcao(planoAcao) {
    if (planoAcao) {
      this.modalConfirmRef = this.modalService.open(DialogoNovoPlanoAcaoComponent, {
        data: { planoAcao }, panelClass: 'sins-modal-plano-acao'
      })
    }
  }

  ngOnDestroy(): void {
    this.appStore$.dispatch(new AjustarPadding(20));
    this.store$.dispatch(new ClearPlanoAcaoAtual())
    this.store$.dispatch(new ClearAcaoEditar())
    this.store$.dispatch(new ClearTarefas)
    this.store$.dispatch(new ClearTarefasExecutar())
    this.subscription.unsubscribe();
    if (this.modalRefIniciar) {
      this.modalRefIniciar.close();
    }
    if (this.modalRefExcluirPlanoAcao) {
      this.modalRefExcluirPlanoAcao.close();
    }
    if (this.modalConfirmRef) {
      this.modalConfirmRef.close();
    }
    if (this.headerActionsService) {
      this.headerActionsService.remove();
    }
    if (this.acoesExecutarSub) {
      this.acoesExecutarSub.unsubscribe()
    }
  }

  excluirPlano() {
    this.modalRefExcluirPlanoAcao = this.modalService.open(DialogoConfirmacaoComponent, {
      data: { header: "Excluir Plano de Ação" }, panelClass: 'sins-modal-confirm'
    });
    this.modalRefExcluirPlanoAcao.afterClosed().subscribe((confirmacao) => { if (confirmacao) this.store$.dispatch(new ExcluirPlanoAcao(this.idPlano, this.usuarioinstituto.numeroCooperativa, null, null)) })
  }

  acoesPlanoAcao(item: string, planoAcao): void {
    switch (item) {
      case AcoesPlano.alterar:
        this.atualizarPlanoAcao(planoAcao);
        break;
      case AcoesPlano.excluir:
        this.excluirPlano();
        break;
      default: { }
    }
  }


}
