export enum AcoesPlano {
  alterar = 'Alterar',
  excluir = 'Excluir',
}

export namespace AcoesPlano {
  export function values(): AcoesPlano[] {
    return [
      AcoesPlano.alterar,
      AcoesPlano.excluir
    ]
  }
}



