import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { PlanoAcao } from '../models/plano-acao.model';
import { PlanoAcaoActions, PlanoAcaoActionTypes } from '../actions/plano-acao.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TotalSituacaoVm } from '../../sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { SituacaoAcaoType } from '../models/situacao-acao.type';
import { Acao } from '../models/acao.model';
import { PageModelVm } from '@app/shared/models/page.model';

export interface State extends EntityState<Acao> {
  // additional entities state properties
  totalSituacao: TotalSituacaoVm[];
  totalSituacaoExecutar: TotalSituacaoVm[];
  totalSituacaoPlanoAcao: TotalSituacaoVm[];
  acoesPlano: Acao[];
  paginacaoAtualAcoes: PageModelVm;
  situacaoAtualAcoes: SituacaoAcaoType;
  acoesExecutar: Acao[];
  paginacaoAtualAcoesExecutar: PageModelVm;
  situacaoAtualAcoesExecutar: SituacaoAcaoType;
  acaoEditar: Acao;
  todos: PlanoAcao[];
  paginacaoAtualPlanos: PageModelVm;
  planoAtual: PlanoAcao;
  totalPlanosAcao: number;
}

export const adapter: EntityAdapter<Acao> = createEntityAdapter<Acao>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  acaoEditar: null,
  totalSituacao:
    [
      new TotalSituacaoVm(SituacaoAcaoType.todas, 0),
      new TotalSituacaoVm(SituacaoAcaoType.naoIniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.iniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.concluida, 0),
      new TotalSituacaoVm(SituacaoAcaoType.impedida, 0),
    ],
  totalSituacaoExecutar:
    [
      new TotalSituacaoVm(SituacaoAcaoType.todas, 0),
      new TotalSituacaoVm(SituacaoAcaoType.naoIniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.iniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.concluida, 0),
      new TotalSituacaoVm(SituacaoAcaoType.impedida, 0),
    ]
  ,
  totalSituacaoPlanoAcao:
    [
      new TotalSituacaoVm(SituacaoAcaoType.todas, 0),
      new TotalSituacaoVm(SituacaoAcaoType.naoIniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.iniciada, 0),
      new TotalSituacaoVm(SituacaoAcaoType.concluida, 0),
      new TotalSituacaoVm(SituacaoAcaoType.impedida, 0),
    ]
  ,
  planoAtual: null,
  todos: [],
  acoesPlano: [],
  paginacaoAtualAcoes: new PageModelVm(0, 5, 0),
  situacaoAtualAcoes: SituacaoAcaoType.todas,
  paginacaoAtualAcoesExecutar: new PageModelVm(0, 5, 0),
  situacaoAtualAcoesExecutar: SituacaoAcaoType.todas,
  paginacaoAtualPlanos: new PageModelVm(0, 8, 0),
  acoesExecutar: [],
  totalPlanosAcao: 0,
});

export function reducer(
  state = initialState,
  action: PlanoAcaoActions
): State {
  switch (action.type) {

    case PlanoAcaoActionTypes.CriarPlanoAcaoSuccess: {
      return {
        ...state, planoAtual: action.planoAcao
      };
    }
    case PlanoAcaoActionTypes.CriarAcaoSuccess: {
      let acao: any;
      let paginacaoAtualAcoes = state.paginacaoAtualAcoes;
      if (action.acao.acaoPai) {
        acao = state.acoesPlano.filter(a => a.id === action.acao.acaoPai._id)[0];
        acao.investimento = action.acao.acaoPai.investimento;
        acao.dataInicio = action.acao.acaoPai.dataInicio;
        acao.dataFim = action.acao.acaoPai.dataFim;
        if (!acao.subTarefas || !acao.subTarefas[0]) acao.subTarefas = [];
        acao.subTarefas = acao.subTarefas.filter(a => a.id !== action.acao.id);
        acao.subTarefas.push(action.acao)
        for (let i = 0; i < state.acoesPlano.length; i++) {
          const acoes = state.acoesPlano[i];
          if (acoes.id === acao._id)
            state.acoesPlano[i] = acao
        }
      } else {
        acao = action.acao;
        state.acoesPlano = [acao].concat(state.acoesPlano)
        paginacaoAtualAcoes = new PageModelVm(paginacaoAtualAcoes.pageNumber, paginacaoAtualAcoes.pageSize, paginacaoAtualAcoes.total + 1);
      }
      return {
        ...state, paginacaoAtualAcoes
      };
    }
    case PlanoAcaoActionTypes.CriarAcaoESubTarefaSuccess: {
      let paginacaoAtualAcoes = action.acaoPaginada.page;
      let acoesPlano = action.acaoPaginada.data;
      return {
        ...state, acoesPlano, paginacaoAtualAcoes
      };
    }
    case PlanoAcaoActionTypes.ExcluirAcaoSuccess: {
      let paginacaoAtualAcoes;
      if (action.acao.acaoPai) {
        let acaoPai = action.acaoPai;
        paginacaoAtualAcoes = state.paginacaoAtualAcoes;
        for (let i = 0; i < state.acoesPlano.length; i++) {
          const acoes = state.acoesPlano[i];
          if (acoes.id === acaoPai.id)
            state.acoesPlano[i] = acaoPai
        }
      } else {
        state.acoesPlano = state.acoesPlano.filter(a => a.id !== action.acao.id);
        paginacaoAtualAcoes = new PageModelVm(state.paginacaoAtualAcoes.pageNumber, state.paginacaoAtualAcoes.pageSize, (state.paginacaoAtualAcoes.total - 1))
      }
      return {
        ...state, paginacaoAtualAcoes
      };
    }
    case PlanoAcaoActionTypes.UpdateAcaoSuccess: {
      let acaoEditada: Acao;
      if (action.acaoEditada.acaoPai) {
        acaoEditada = atualizarSubTarefaAcoesPlano(action.acaoEditada, state.acoesPlano)
      } else {
        acaoEditada = action.acaoEditada;
      }
      for (let i = 0; i < state.acoesPlano.length; i++) {
        const acoes = state.acoesPlano[i];
        if (acoes.id === acaoEditada.id)
          state.acoesPlano[i] = acaoEditada
      }
      return {
        ...state
      };
    }
    case PlanoAcaoActionTypes.LoadAcoesPlanoSuccess: {
      let acoesPlano = action.acoesPaginadas && action.acoesPaginadas.data.length > 0 ? state.acoesPlano.concat(action.acoesPaginadas.data) : []; //action.acoesPaginadas.data;
      let paginacaoAtualAcoes = action.acoesPaginadas.page;
      let situacaoAtualAcoes = action.situacao ? action.situacao : SituacaoAcaoType.todas
      return {
        ...state, acoesPlano, paginacaoAtualAcoes, situacaoAtualAcoes
      };
    }
    case PlanoAcaoActionTypes.LoadAcoesExecutarSuccess: {
      let acoesExecutar = action.acoesPaginadas && action.acoesPaginadas.data.length > 0 ? state.acoesExecutar.concat(action.acoesPaginadas.data) : [];
      let paginacaoAtualAcoesExecutar = action.acoesPaginadas.page;
      let situacaoAtualAcoesExecutar = action.situacao ? action.situacao : SituacaoAcaoType.todas
      return {
        ...state, acoesExecutar, paginacaoAtualAcoesExecutar, situacaoAtualAcoesExecutar
      };
    }
    case PlanoAcaoActionTypes.ExecutarAcaoSuccess: {
      if (action.origem === 'executar') {
        state.acoesExecutar =  executarTarefaExecutar(action, state.acoesExecutar, state.situacaoAtualAcoesExecutar, action.situacaoAlvo )
        state.paginacaoAtualAcoesExecutar =  state.situacaoAtualAcoesExecutar != SituacaoAcaoType.todas && state.situacaoAtualAcoesExecutar != action.situacaoAlvo ? new PageModelVm(state.paginacaoAtualAcoesExecutar.pageNumber, state.paginacaoAtualAcoesExecutar.pageSize, state.paginacaoAtualAcoesExecutar.total-1) : state.paginacaoAtualAcoesExecutar;
      } else {
        state.acoesPlano  = executarTarefaPlano(action, state.acoesPlano, state.situacaoAtualAcoes, action.situacaoAlvo )
        state.paginacaoAtualAcoes =  state.situacaoAtualAcoes != SituacaoAcaoType.todas && state.situacaoAtualAcoes != action.situacaoAlvo ? new PageModelVm(state.paginacaoAtualAcoes.pageNumber, state.paginacaoAtualAcoes.pageSize, state.paginacaoAtualAcoes.total-1) : state.paginacaoAtualAcoes;
      }
      state.acaoEditar = action.acao
      return {
        ...state
      };
    }
    case PlanoAcaoActionTypes.LoadPlanoAcaoSuccess: {
      return {
        ...state, planoAtual: action.planoAcao
      };
    }
    case PlanoAcaoActionTypes.ContarAcoesSuccess: {
      return {
        ...state, totalSituacao: action.situacaoVm
      };
    }
    case PlanoAcaoActionTypes.ContarExecutarAcoesSuccess: {
      return {
        ...state, totalSituacaoExecutar: action.situacaoVm
      };
    }
    case PlanoAcaoActionTypes.ContarPlanosAcaoSuccess: {
      return {
        ...state, totalSituacaoPlanoAcao: action.situacaoVm
      };
    }
    case PlanoAcaoActionTypes.LoadAcaoSuccess: {
      return {
        ...state, acaoEditar: action.acao
      };
    }
    case PlanoAcaoActionTypes.ClearAcaoEditar: {
      return {
        ...state, acaoEditar: null
      };
    }
    case PlanoAcaoActionTypes.ExcluirPlanoAcaoSuccess: {
      let paginacaoAtualPlanos = new PageModelVm(state.paginacaoAtualPlanos.pageNumber, state.paginacaoAtualPlanos.pageSize, state.paginacaoAtualPlanos.total -1);
      let todos = state.todos.filter( s => s.id !== action.idPlanoAcao);
      return {
        ...state, todos, paginacaoAtualPlanos
      };
    }
    case PlanoAcaoActionTypes.LoadPlanosAcaoSuccess: {
      let paginacaoAtualPlanos = action.planosPaginados.page;
      let todos = action.planosPaginados && action.planosPaginados.data.length > 0 ? state.todos.concat(action.planosPaginados.data) : [];

      return {
        ...state, todos, paginacaoAtualPlanos
      };
    }
    case PlanoAcaoActionTypes.TotalPlanosAcaoSuccess: {
      return {
        ...state, totalPlanosAcao: action.total
      };
    }
    case PlanoAcaoActionTypes.ClearPlanoAcaoAtual: {
      return {
        ...state, planoAtual: null
      };
    }
    case PlanoAcaoActionTypes.ClearTarefas: {
      return {
        ...state, acoesPlano: [], paginacaoAtualAcoes: new PageModelVm(0, 5, 0)
      };
    }
    case PlanoAcaoActionTypes.ClearTarefasExecutar: {
      return {
        ...state, acoesExecutar: [], paginacaoAtualAcoesExecutar: new PageModelVm(0, 5, 0)
      };
    }
    case PlanoAcaoActionTypes.ClearPlanosAcao: {
      return {
        ...state, todos: [], paginacaoAtualPlanos: new PageModelVm(0, 8, 0)
      };
    }

    default: {
      return state;
    }
  }
}


export function executarTarefaPlano(action: any, acoesPlano: Acao[], situacaoAtual: SituacaoAcaoType, situacao: SituacaoAcaoType) {
  let acaoEditada: Acao;
  if (action.acao.acaoPai) {
    if(situacaoAtual != SituacaoAcaoType.todas && situacaoAtual != situacao) {
      acoesPlano = acoesPlano.filter(s => s.id !== action.acao.acaoPai._id);
    } else {
      acaoEditada = atualizarSubTarefaAcoesPlano(action.acao, acoesPlano);
    }
  } else {
    acaoEditada = action.acao
  }
  if( acaoEditada ) {
    for (let i = 0; i < acoesPlano.length; i++) {
      const acoes = acoesPlano[i];
      if (acoes.id === action.acao.id) {
        if(situacaoAtual == SituacaoAcaoType.todas || situacaoAtual == situacao) {
          acoesPlano[i] = acaoEditada
        }else {
          acoesPlano = acoesPlano.filter(s => s.id !== action.acao.id);
        }
      }
    }
  }

  return acoesPlano
}

export function atualizarSubTarefaAcoesPlano(acaoEditada: Acao, acoesState: Acao[]) {
  let id = acaoEditada.acaoPai._id;
  let acaoPaiEditada = acoesState.filter(a => a.id === id)[0];
  acaoPaiEditada.investimento = acaoEditada.acaoPai.investimento;
  acaoPaiEditada.dataInicio = acaoEditada.acaoPai.dataInicio;
  acaoPaiEditada.dataFim = acaoEditada.acaoPai.dataFim;
  acaoPaiEditada.situacao = acaoEditada.acaoPai.situacao;
  for (let i = 0; i < acaoPaiEditada.subTarefas.length; i++) {
    const acoes = acaoPaiEditada.subTarefas[i];
    if (acoes.id === acaoEditada.id)
      acaoPaiEditada.subTarefas[i] = acaoEditada
  }
  return acaoPaiEditada
}

export function executarTarefaExecutar(action: any, acoesExecutar: Acao[], situacaoAtual: SituacaoAcaoType, situacao: SituacaoAcaoType) {
    for (let i = 0; i < acoesExecutar.length; i++) {
      const acoes = acoesExecutar[i];
      if (acoes.id === action.acao.id) {
        if(situacaoAtual == SituacaoAcaoType.todas || situacaoAtual == situacao) {
          acoesExecutar[i] = action.acao
        }else {
          acoesExecutar = acoesExecutar.filter(s => s.id !== action.acao.id);
        }
      }
  }
  return acoesExecutar;
}

export const getPlanoAcaoState = createFeatureSelector<State>('planoAcaoState');

export const getPlanoAtual = createSelector(
  getPlanoAcaoState,
  state => {
    return state.planoAtual;
  });

export const getTotalSituacao = createSelector(
  getPlanoAcaoState,
  state => {
    return state.totalSituacao;
  });

export const getTotalSituacaoExecutar = createSelector(getPlanoAcaoState, state => state.totalSituacaoExecutar);

export const getTotalSituacaoPlanoAcao = createSelector(
  getPlanoAcaoState,
  state => {
    return state.totalSituacaoPlanoAcao;
  });

export const getActionEditar = createSelector(
  getPlanoAcaoState,
  state => {
    return state.acaoEditar;
  });

export const getTodos = createSelector(
  getPlanoAcaoState,
  state => {
    return state.todos;
  });

export const getAcoesPlano = createSelector(
  getPlanoAcaoState,
  state => {
    return state.acoesPlano;
  });

export const getAcoesExecutar = createSelector(
  getPlanoAcaoState,
  state => {
    return state.acoesExecutar;
  });


export const getTotalPlanosAcao = createSelector(
  getPlanoAcaoState,
  state => {
    return state.totalPlanosAcao;
  });

export const getPaginacaoAtualAcoes = createSelector(
  getPlanoAcaoState,
  state => {
    return state.paginacaoAtualAcoes;
  });

export const getPaginacaoAtualAcoesExecutar = createSelector(
  getPlanoAcaoState,
  state => {
    return state.paginacaoAtualAcoesExecutar;
  });



export const getPaginacaoAtualPlanos = createSelector(
  getPlanoAcaoState,
  state => {
    return state.paginacaoAtualPlanos;
  });


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getPlanoAcaoState);
