import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Service } from 'src/app/shared/services/service';
import { environment } from 'src/environments/environment';
import { Acao } from './models/acao.model';
import { PlanoAcao } from './models/plano-acao.model';
import { SituacaoAcaoType } from './models/situacao-acao.type';
import { TotalSituacaoVm } from '../sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { AcaoPaginada } from './models/acao-paginada.model';
import { PlanosPaginados } from './models/planos-paginados.model';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PlanoAcaoService extends Service {

  private RESOURCE_PLANO_ACAO = 'planos-acao';
  private RESOURCE_URL_PLANO_ACAO = `${environment.modulo_gestao_pessoa}/${this.RESOURCE_PLANO_ACAO}`;

  constructor(private http: HttpClient) { super(); }

  criarPlanoAcao(planoAcao: PlanoAcao): Observable<PlanoAcao> {
    return this.http.post<PlanoAcao>(this.RESOURCE_URL_PLANO_ACAO, planoAcao, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  criarAcao(acao: Acao): Observable<Acao> {
    return this.http.post<Acao>(`${this.RESOURCE_URL_PLANO_ACAO}/acao/`, acao, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  excluirAcao(id: string) {
    return this.http.delete<any>(`${this.RESOURCE_URL_PLANO_ACAO}/acao/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  excluirPlanoAcao(id: string) {
    return this.http.delete(`${this.RESOURCE_URL_PLANO_ACAO}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateAcao(acao: Acao): Observable<Acao> {
    return this.http.put<Acao>(`${this.RESOURCE_URL_PLANO_ACAO}/acao/`, acao, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updatePlanoAcao(planoAcao: PlanoAcao): Observable<PlanoAcao> {
    return this.http.put<PlanoAcao>(`${this.RESOURCE_URL_PLANO_ACAO}/`, planoAcao, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  iniciarPlanoAcao(id: string): Observable<PlanoAcao> {
    return this.http.patch<{ resultado: PlanosPaginados }>(`${this.RESOURCE_URL_PLANO_ACAO}/${id}/iniciar`, httpOptions)
      .pipe(
        map(p => p.resultado.data[0]),
        catchError(this.handleError)
      );
  }

  loadPlanosAcoes(pageModelVm: PageModelVm, responsavel?: string, situacao?: SituacaoAcaoType, numeroCooperativa?: string, filtroNome?: string, vigencia?: string) {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}?offset=${pageModelVm.pageNumber}&limit=${pageModelVm.pageSize}&`;
    url = this.montarUrlLoadPlanos(url, responsavel, situacao, numeroCooperativa, filtroNome, vigencia);
    return this.http.get<{ resultado: PlanosPaginados }>(url, httpOptions)
      .pipe(
        map(p => p.resultado),
        catchError(this.handleError)
      );
  }

  contarPlanoAcao(responsavel?: string, situacao?: SituacaoAcaoType, numeroCooperativa?: string, filtroNome?: string): Observable<number> {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/total-registros?`;
    url = this.montarUrlLoadPlanos(url, responsavel, situacao, numeroCooperativa, filtroNome);
    return this.http.get<number>(url, httpOptions)
      .pipe(
        map((total: any) => total.resultado.total),
        catchError(this.handleError)
      );
  }

  contarSituacaoPlanoAcao(responsavel?: string, situacao?: SituacaoAcaoType, numeroCooperativa?: string, nome?: string, vigencia?: string) {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/situacao/total-registros-agrupados?`;
    url = this.montarUrlLoadPlanos(url, responsavel, situacao, numeroCooperativa, nome, vigencia);
    return this.http.get<TotalSituacaoVm[]>(url, httpOptions)
      .pipe(
        map((total: any) => total.resultado.totalSituacao),
        catchError(this.handleError)
      );
  }

  loadPlanoAcao(id: string, situacao?: SituacaoAcaoType, filtroNome?: string, responsavel?: string): Observable<PlanoAcao> {
    if (id) {
      let url = `${this.RESOURCE_URL_PLANO_ACAO}/${id}?`;
      url = this.montarUrlLoadPlanos(url, responsavel, situacao, null, filtroNome);
      return this.http.get<{ resultado: PlanosPaginados }>(url, httpOptions)
        .pipe(
          map(r => r.resultado.data[0]),
          catchError(this.handleError)
        );
    }

  }

  loadAcoesExecutar(responsavel: string, planoAcaoId: string, pageModelVm: PageModelVm, situacao?: SituacaoAcaoType, nome?: string): Observable<AcaoPaginada> {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/${responsavel}/acoes-executar?offset=${pageModelVm.pageNumber}&limit=${pageModelVm.pageSize}&planoAcao=${planoAcaoId}&`;
    url = this.montarUrlLoadPlanos(url, null, situacao, null, nome);
    return this.http.get<{ resultado: any }>(url, httpOptions)
      .pipe(
        map(data =>  data.resultado),
        catchError(this.handleError)
      );
  }

  countAcoesExecutar(responsavel: string, planoAcaoId: string, nome?: string, ) {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/${responsavel}/acoes-executar/situacao/total-registros-agrupados?planoAcao=${planoAcaoId}&`;
    url = this.montarUrlLoadPlanos(url, null, null, null, nome);
    return this.http.get<TotalSituacaoVm[]>(url, httpOptions)
      .pipe(
        map((data: any) => data.resultado.totalSituacao),
        catchError(this.handleError)
      );
  }

  loadAcoes(planoAcaoId: string, pageModelVm: PageModelVm, situacao?: SituacaoAcaoType, nome?: string, responsavel?: string): Observable<AcaoPaginada> {
    pageModelVm = pageModelVm && pageModelVm.pageSize ? pageModelVm : new PageModelVm(0, 5, 0)
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/${planoAcaoId}/acoes?offset=${pageModelVm.pageNumber}&limit=${pageModelVm.pageSize}&`;
    url = this.montarUrlLoadPlanos(url, responsavel, situacao, null, nome);
    return this.http.get<{ resultado: any }>(url, httpOptions)
      .pipe(
        map(data => data.resultado),
        catchError(this.handleError)
      );
  }

  countAcoes(planoAcaoPai: string, filtroNome: string, responsavel?: string) {
    let url = `${this.RESOURCE_URL_PLANO_ACAO}/${planoAcaoPai}/acoes/situacao/total-registros-agrupados?`;
    url = this.montarUrlLoadPlanos(url, responsavel, null, null, filtroNome);
    return this.http.get<TotalSituacaoVm[]>(url, httpOptions)
      .pipe(
        map((data: any) => data.resultado.totalSituacao),
        catchError(this.handleError)
      );
  }

  loadAcaoById(id: string) {
    return this.http.get<Acao>(`${this.RESOURCE_URL_PLANO_ACAO}/acao/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  executarAcao(acao: Acao, situacao: SituacaoAcaoType, responsavel: string) {
    acao.situacao = situacao;
    acao.responsavel = responsavel;
    return this.updateAcao(acao);
  }

  private montarUrlLoadPlanos(url: string, responsavel?: string, situacao?: SituacaoAcaoType,
    numeroCooperativa?: string, filtroNome?: string, vigencia?: string) {
    filtroNome = filtroNome ? encodeURIComponent(filtroNome) : filtroNome;
    if(vigencia){
      url = url + `vigencia=${vigencia}&`;
    }
    if (responsavel) {
      url = url + `responsavel=${responsavel}&`;
    }
    if (situacao) {
      url = url + `situacao=${situacao}&`;
    }
    if (numeroCooperativa) {
      url = url + `numeroCooperativa=${numeroCooperativa}&`;
    }
    if (filtroNome) {
      url = url + `nome=${filtroNome}&`;
    }
    return url;
  }
}
