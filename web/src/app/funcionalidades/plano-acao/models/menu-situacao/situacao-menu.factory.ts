import { SituacaoAcaoType } from "../situacao-acao.type";
import { SituacaoMenu } from "./situacao-menu.model";
import { PlanejandoMenu } from "./planejando-menu.model";
import { NaoIniciadaMenu } from "./naoIniciada-menu.model";
import { ConcluidaMenu } from "./concluida-menu.model";
import { ImpedidaMenu } from "./impedida-menu.model";
import { IniciadaMenu } from "./iniciada-menu.model";
import { ItemMenu } from "src/app/shared/components/menu-dropdown/model/item-menu.model";

export class SituacaoMenuFactory {

  public static createSituacaoMenu(situacao: SituacaoAcaoType, entity: any): SituacaoMenu {
    if (situacao == SituacaoAcaoType.planejando) {
      return new PlanejandoMenu(entity);
    } else if (situacao == SituacaoAcaoType.concluida) {
      return new ConcluidaMenu(entity);
    } else if (situacao == SituacaoAcaoType.naoIniciada) {
      return new NaoIniciadaMenu(entity);
    } else if (situacao == SituacaoAcaoType.iniciada) {
      return new IniciadaMenu(entity);
    } else if (situacao == SituacaoAcaoType.impedida) {
      return new ImpedidaMenu(entity);
    } else {
      throw new Error(`Situacao ainda não foi implementado: [${situacao}]`);
    }
  }

}
