import { ItemMenu } from "src/app/shared/components/menu-dropdown/model/item-menu.model";

export class SituacaoMenu {
  public menu: Array<ItemMenu>;
  constructor(public entity: any) {
    this.menu = new Array<ItemMenu>();
    /* this.menu.push(new ItemMenu(2, "Alterar Informações", entity)) */
    this.menu.push(new ItemMenu(3, "Acompanhar plano", entity))
    this.menu.push(new ItemMenu(4, "Suas tarefas", entity))
  }
}
