import { SituacaoMenu } from "./situacao-menu.model";
import { ItemMenu } from "src/app/shared/components/menu-dropdown/model/item-menu.model";

export class ConcluidaMenu extends SituacaoMenu {

  constructor(public entity: any) {
    super(entity);
    this.menu = new Array<ItemMenu>();
    this.menu.push(new ItemMenu(1, "Visualizar Informações", entity))
  }

}
