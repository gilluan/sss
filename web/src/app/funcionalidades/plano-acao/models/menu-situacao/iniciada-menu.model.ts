import { SituacaoMenu } from "./situacao-menu.model";
import { ItemMenu } from "src/app/shared/components/menu-dropdown/model/item-menu.model";

export class IniciadaMenu extends SituacaoMenu {

  constructor(public entity: any) {
    super(entity);
  }

}
