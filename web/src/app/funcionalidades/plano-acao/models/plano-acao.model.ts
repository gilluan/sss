import { Acao } from './acao.model';
import { SituacaoAcaoType } from './situacao-acao.type';
import { BaseDto } from 'src/app/shared/models/base-dto.model';

export class PlanoAcao extends BaseDto {
  nome: string;
  situacao: SituacaoAcaoType;
  dataProximaRevisao: Date;
  responsavel: any;
  acoes: Acao[];
  iniciado: boolean;
  numeroCooperativa: string;
  situacaoAcoes: any;
  vigencia: string;
}
