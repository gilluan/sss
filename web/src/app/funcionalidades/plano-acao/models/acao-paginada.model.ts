import { Acao } from "./acao.model";
import { PageModelVm } from "@app/shared/models/page.model";

export class AcaoPaginada {
  constructor(public page: PageModelVm, public data: Acao[]) {}
}
