import { PageModelVm } from "@app/shared/models/page.model";
import { PlanoAcao } from './plano-acao.model';

export class PlanosPaginados {
  constructor(public page: PageModelVm, public data: PlanoAcao[]) {}
}
