import {SituacaoAcaoType} from './situacao-acao.type';
import {BaseDto} from 'src/app/shared/models/base-dto.model';
import { JustificativaAjusteVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/justificativa-ajuste-vm.model';

export class Acao extends BaseDto {

  public responsavel: any;
  public dataInicio: Date;
  public dataFim: Date;
  public nome: string;
  public lugar: string;
  public investimento: number;
  public descricao: string;
  public motivo: string;
  public acaoPai: any;
  public acaoPaiObj: Acao;
  public planoAcaoPai: string;
  public situacao: SituacaoAcaoType;
  public subTarefas: Acao[];

  public dataRealInicio: Date;
  public dataRealFim: Date;
  public investimentoReal: number;
  public controlTempoExecucao: string;
  public justificativaImpedimento: JustificativaAjusteVm;
  public justificativaInvestimento: JustificativaAjusteVm;
  public nomeAcaoPai: string;


}
