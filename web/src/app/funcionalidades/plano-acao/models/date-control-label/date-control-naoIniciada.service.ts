import { Colors } from 'src/app/shared/models/color.type';
import { SituacaoAcaoType } from '../situacao-acao.type';
import { DateControlLabel } from './date-control-label.service';

export class DateControlNaoIniciada extends DateControlLabel {

  private situacao: SituacaoAcaoType = SituacaoAcaoType.naoIniciada;

  constructor(
    public tempoPlanejadoFimDate: Date,
    public tempoPlanejadoInicioDate: Date,
    public tempoRealFim: Date,
    public tempoRealInicio: Date) {
    super(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
  }

  public getLabel(): string {
    let condition = this.getCondicao();
    let label = 'Deveria ser iniciada hoje';
    let daysLabel = condition.days > 1 ? 'dias' : 'dia';
    if (condition.color == null)
      label = `Deve ser iniciada em ${condition.days} ${daysLabel}`;
    if (condition.color == Colors.danger)
      label = `Deveria ter iniciado há ${condition.days} ${daysLabel}`;
    return this.toSpan(label, condition.color);
  }

  public getCondicao(): { color: Colors, days: number } {
    var diffDays = this.diffDaysBetweenDates(this.tempoPlanejadoInicio, new Date(Date.now()).getTime());
    return diffDays > 0 ? { color: null, days: diffDays } : diffDays < 0 ? { color: Colors.danger, days: (diffDays * -1) } : { color: Colors.warning, days: 0 };
  }

  public getSituacao(): SituacaoAcaoType {
    return this.situacao;
  }
}
