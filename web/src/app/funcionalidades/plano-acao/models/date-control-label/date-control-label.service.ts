import { Colors } from 'src/app/shared/models/color.type';
import { Formatters } from 'src/app/shared/utils/formatters';

export abstract class DateControlLabel extends Formatters {

  tempoPlanejadoFim: number;
  tempoPlanejadoInicio: number;
  dataRealInicioStr: string;
  dataRealFimStr: string;

  constructor(public tempoPlanejadoFimDate: Date, public tempoPlanejadoInicioDate: Date, public tempoRealFim: Date, public tempoRealInicio: Date) {
    super();
    this.tempoPlanejadoFim = new Date(tempoPlanejadoFimDate).getTime();
    this.tempoPlanejadoInicio = new Date(tempoPlanejadoInicioDate).getTime();
  }

  abstract getLabel(): string;

  abstract getCondicao(): { color: Colors, days: number };

  protected toSpan(texto: string, color?: Colors): string {
    let spanStart = '<span';
    let spanEnd: string = '</span>';
    if (texto) {
      if (color) spanStart = spanStart + ` style="color: ${color}; font-weight: bold;"> ${texto}`;
      else spanStart = spanStart + `> ${texto}`;
    } else spanStart = spanStart + '>'
    return spanStart + spanEnd;
  }

  protected diffDaysBetweenDates(dataEnd: number, dateStart: number) {
    var diff = dataEnd - dateStart;
    return Math.ceil(diff / (1000 * 3600 * 24));
  }

}
