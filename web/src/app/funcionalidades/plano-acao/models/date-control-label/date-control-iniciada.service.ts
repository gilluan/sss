import { Injectable } from '@angular/core';
import { Colors } from 'src/app/shared/models/color.type';
import { SituacaoAcaoType } from '../situacao-acao.type';
import { DateControlLabel } from './date-control-label.service';

export class DateControlIniciada extends DateControlLabel {

  private situacao: SituacaoAcaoType = SituacaoAcaoType.iniciada;

  constructor(
    public tempoPlanejadoFimDate: Date,
    public tempoPlanejadoInicioDate: Date,
    public tempoRealFim: Date,
    public tempoRealInicio: Date) {
    super(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
    this.tempoRealInicio = new Date(tempoRealInicio);
    this.dataRealInicioStr = this.dataAtualFormatada(new Date(tempoRealInicio));
  }

  public getLabel(): string {
    let condition = this.getCondicao();

    let daysLabel = condition.days > 1 ? 'dias' : 'dia';
    let faltamLabel = condition.days > 1 ? 'Faltam' : 'Falta';
    let label1: string = `${this.dataRealInicioStr} |`;
    let label2: string = `${faltamLabel} ${condition.days} ${daysLabel}`;
    if (condition.color == Colors.danger) {
      label2 = `Atrasada há ${condition.days} ${daysLabel}`;
    } else if (condition.days == 0) {
      label1 = `${this.dataRealInicioStr} |`;
      label2 = `Deveria ser concluída hoje`;
    }
    return this.toSpan(label1) + this.toSpan(label2, condition.color);
  }

  public getCondicao(): { color: Colors, days: number } {
    var diffDays = this.diffDaysBetweenDates(this.tempoPlanejadoFim, new Date(Date.now()).getTime());
    return diffDays > 0 ? { color: Colors.success, days: diffDays } : diffDays < 0 ? { color: Colors.danger, days: (diffDays * -1) } : { color: Colors.warning, days: 0 };
  }

  public getSituacao(): SituacaoAcaoType {
    return this.situacao;
  }
}
