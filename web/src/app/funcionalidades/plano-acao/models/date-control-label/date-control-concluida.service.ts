import { Injectable } from '@angular/core';
import { Colors } from 'src/app/shared/models/color.type';
import { SituacaoAcaoType } from '../situacao-acao.type';
import { DateControlLabel } from './date-control-label.service';

export class DateControlConcluida extends DateControlLabel {

  private situacao: SituacaoAcaoType = SituacaoAcaoType.concluida;

  constructor(
    public tempoPlanejadoFimDate: Date,
    public tempoPlanejadoInicioDate: Date,
    public tempoRealFim: Date,
    public tempoRealInicio: Date) {
    super(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
    this.tempoRealFim = new Date(tempoRealFim);
    this.tempoRealInicio = new Date(tempoRealInicio);
    this.dataRealInicioStr = this.dataAtualFormatada(new Date(tempoRealInicio));
    this.dataRealFimStr = this.dataAtualFormatada(new Date(tempoRealFim));
  }

  public getLabel(): string {
    let condition = this.getCondicao();
    return this.toSpan(`${this.dataRealInicioStr} |`) + this.toSpan(this.dataRealFimStr, condition.color);
  }

  public getCondicao(): { color: Colors, days: number } {
    var diffDays = this.diffDaysBetweenDates(this.tempoPlanejadoFim, this.tempoRealFim.getTime());
    return diffDays < 0 ? { color: Colors.danger, days: (diffDays * -1) } : { color: Colors.success, days: null };
  }

  public getSituacao(): SituacaoAcaoType {
    return this.situacao;
  }
}
