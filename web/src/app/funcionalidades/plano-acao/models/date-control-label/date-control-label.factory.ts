import { SituacaoAcaoType } from "../situacao-acao.type";
import { DateControlConcluida } from "./date-control-concluida.service";
import { DateControlIniciada } from "./date-control-iniciada.service";
import { DateControlLabel } from "./date-control-label.service";
import { DateControlNaoIniciada } from "./date-control-naoIniciada.service";


export class DateControlLabelFactory {

  public static createSituacaoMenu(situacao: SituacaoAcaoType, tempoPlanejadoFimDate: Date, tempoPlanejadoInicioDate: Date, tempoRealFim: Date, tempoRealInicio: Date): DateControlLabel {
    if (situacao == SituacaoAcaoType.concluida) {
      return new DateControlConcluida(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
    } else if (situacao == SituacaoAcaoType.naoIniciada) {
      return new DateControlNaoIniciada(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
    } else if (situacao == SituacaoAcaoType.iniciada || situacao == SituacaoAcaoType.impedida) {
      return new DateControlIniciada(tempoPlanejadoFimDate, tempoPlanejadoInicioDate, tempoRealFim, tempoRealInicio);
    } else {
      throw new Error(`Situacao ainda não foi implementado: [${situacao}]`);
    }
  }

}
