export enum SituacaoAcaoType {
  naoIniciada = 'naoIniciada',
  iniciada = 'iniciada',
  concluida = 'concluida',
  impedida = 'impedida',
  todas = 'todas',
  planejando = 'planejando',

}

export namespace SituacaoAcaoType {
  export function values(): SituacaoAcaoType[] {
    return [
      SituacaoAcaoType.naoIniciada,
      SituacaoAcaoType.iniciada,
      SituacaoAcaoType.concluida,
      SituacaoAcaoType.impedida,
      SituacaoAcaoType.planejando,
    ];
  }

  export function label(situacao: SituacaoAcaoType): string {
    switch (situacao) {
      case SituacaoAcaoType.naoIniciada:
        return 'Não iniciada';
      case SituacaoAcaoType.iniciada:
        return 'Iniciada';
      case SituacaoAcaoType.concluida:
        return 'Concluída';
      case SituacaoAcaoType.impedida:
        return 'Impedida';
      case SituacaoAcaoType.todas:
        return 'Todas';
      case SituacaoAcaoType.planejando:
        return 'Planejando';
    }
  }

  export function classes(situacao: SituacaoAcaoType): string {
    switch (situacao) {
      case SituacaoAcaoType.iniciada:
        return 'ss-label-active';
      case SituacaoAcaoType.naoIniciada:
        return 'ss-label-primary';
      case SituacaoAcaoType.concluida:
        return 'ss-label-success';
      case SituacaoAcaoType.impedida:
        return 'ss-label-danger';
      case SituacaoAcaoType.planejando:
        return 'ss-label-default';
      default:
        return 'ss-label-default';
    }
  }

  export function classesBadge(situacao: SituacaoAcaoType): string {
    switch (situacao) {
      case SituacaoAcaoType.iniciada:
        return 'active';
      case SituacaoAcaoType.naoIniciada:
        return 'primary';
      case SituacaoAcaoType.concluida:
        return 'success';
      case SituacaoAcaoType.impedida:
        return 'danger';
      case SituacaoAcaoType.planejando:
        return 'default';
      default:
        return 'default';
    }
  }
}
