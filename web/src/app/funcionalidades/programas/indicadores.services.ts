import {Injectable} from '@angular/core';
import {Service} from '@app/shared/services/service';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {Indicador} from '../projeto/models/indicador.model';
import {PagedDataModelVm} from '@app/shared/models/paged-data.model';
import {catchError, map} from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class IndicadoresService extends Service {

  constructor(private http: HttpClient) {
    super();
  }

  RESOURCE = 'indicadores';
  RESOURCE_URL = `${environment.modulo_gestao_projeto}/${this.RESOURCE}`;

  carregarIndicadores(idPrograma: string): Observable<Indicador[]> {
    const params = this.buildParams();
    return this.http.get<{ resultado: PagedDataModelVm }>(`${this.RESOURCE_URL}/${idPrograma}`, {params}).pipe(
      map(r => r.resultado.data as Indicador[]),
      catchError(this.handleError)
    );
  }

  private buildParams(): HttpParams {
    let params = new HttpParams();
    params = params.append('numeroPagina', '0');
    params = params.append('tamanhoPagina', '10000');
    return params;
  }


}
