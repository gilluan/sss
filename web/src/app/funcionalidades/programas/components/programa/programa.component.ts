import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {range, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import * as fromProgramas from '@app/funcionalidades/programas/reducers/programas.reducer';

const ANOS_VIGENCIA_PORTFOLIO = 3;

@Component({
  selector: 'sc-programa',
  templateUrl: './programa.component.html',
  styleUrls: ['./programa.component.scss']
})
export class ProgramaComponent implements OnInit, OnDestroy {

  @Input() scPrograma: Programa;

  @Output() scFinalizar: EventEmitter<Programa> = new EventEmitter();
  @Output() scAtivar: EventEmitter<Programa> = new EventEmitter();
  @Output() scFormChange: EventEmitter<any> = new EventEmitter();
  @Output() scNavegarFluxoProcesso: EventEmitter<any> = new EventEmitter();

  anos = [];
  programaForm = this.fb.group({
    infosBasicasForm: this.fb.group({
      imagem: ['', Validators.required],
      nome: ['', Validators.required],
      vigencia: ['', Validators.required]
    }),
    infosGeraisForm: this.fb.group({
      descricao: ['', Validators.required],
      justificativa: ['', Validators.required],
      partesInteressadas: ['', Validators.required]
    }),
    produtoForm: this.fb.group({
      produto: ['', Validators.required],
      beneficios: ['', Validators.required],
      premissas: this.fb.array([], Validators.required),
      riscos: this.fb.array([], Validators.required),
      restricoes: this.fb.array([], Validators.required),
      requisitos: this.fb.array([], Validators.required)
    }),
    objetivosForm: this.fb.group({
      objetivoGeral: ['', Validators.required],
      objetivoEspecifico: ['', Validators.required],
      objetivoSmart: ['', Validators.required],
    }),
    processoForm: this.fb.group({
      processo: ['']
    })
  });

  programaEditavel = true;

  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<fromProgramas.State>,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.subscription
      .add(this.programaForm.valueChanges.subscribe(_ => this.formChange()))
      .add(this.store.pipe(select(fromProgramas.selectEditavel)).subscribe(editavel => this.programaEditavel = editavel));

    this.patchPrograma(this.scPrograma);
    this.initAnos();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  atualizarImagemPrograma($event): void {
    this.infosBasicasForm.get('imagem').setValue($event);
    this.scPrograma.imagem = $event;
  }

  finalizar(): void {
    this.scFinalizar.emit(this.parsePrograma());
  }

  ativar(): void {
    this.scAtivar.emit(this.parsePrograma());
  }

  navegarFluxoProcesso(): void {
    this.scNavegarFluxoProcesso.emit();
  }

  get infosBasicasForm() {
    return this.programaForm.get('infosBasicasForm');
  }

  private formChange(): void {
    const output = { programa: this.parsePrograma(), valid: this.programaForm.valid };
    this.scFormChange.emit(output);
  }

  private initAnos(): void {
    const anoAtual = new Date().getFullYear();
    range(anoAtual, ANOS_VIGENCIA_PORTFOLIO).subscribe(ano => this.anos.push(ano));
  }

  private patchPrograma(programa: Programa): void {
    if (programa && programa.id) {
      this.patchArray(programa.premissas, 'produtoForm', 'premissas');
      this.patchArray(programa.riscos, 'produtoForm', 'riscos');
      this.patchArray(programa.restricoes, 'produtoForm', 'restricoes');
      this.patchArray(programa.requisitos, 'produtoForm', 'requisitos');

      this.programaForm.patchValue({
        infosBasicasForm: {
          imagem: programa.imagem,
          nome: programa.nome,
          vigencia: programa.vigencia
        },
        infosGeraisForm: {
          descricao: programa.descricao,
          justificativa: programa.justificativa,
          partesInteressadas: programa.partesInteressadas
        },
        produtoForm: {
          produto: programa.produto,
          beneficios: programa.beneficios
        },
        objetivosForm: {
          objetivoGeral: programa.objetivoGeral,
          objetivoEspecifico: programa.objetivoEspecifico,
          objetivoSmart: programa.objetivoSmart
        },
        processoForm: {
          processo: programa.fluxoprocesso
        }
      });
    }
  }

  private patchArray(values: string[], groupName: string, arrayName: string) {
    (values || []).forEach(v => {
      const group = <FormGroup>this.programaForm.controls[groupName];
      const array = <FormArray>group.controls[arrayName];
      array.push(new FormControl(v, [ Validators.required ]));
    });
  }

  private parsePrograma(): Programa {
    const infosBasicasForm = <FormGroup>this.programaForm.get('infosBasicasForm');
    const infosGeraisForm = <FormGroup>this.programaForm.get('infosGeraisForm');
    const produtoForm = <FormGroup>this.programaForm.get('produtoForm');
    const objetivosForm = <FormGroup>this.programaForm.get('objetivosForm');
    const processoForm = <FormGroup>this.programaForm.get('processoForm');

    return {
      nome: infosBasicasForm.value['nome'],
      vigencia: infosBasicasForm.value['vigencia'],
      imagem: infosBasicasForm.value['imagem'],
      descricao: infosGeraisForm.value['descricao'],
      justificativa: infosGeraisForm.value['justificativa'],
      partesInteressadas: infosGeraisForm.value['partesInteressadas'],
      produto: produtoForm.value['produto'],
      beneficios: produtoForm.value['beneficios'],
      premissas: produtoForm.value['premissas'],
      riscos: produtoForm.value['riscos'],
      restricoes: produtoForm.value['restricoes'],
      requisitos: produtoForm.value['requisitos'],
      objetivoGeral: objetivosForm.value['objetivoGeral'],
      objetivoEspecifico: objetivosForm.value['objetivoEspecifico'],
      objetivoSmart: objetivosForm.value['objetivoSmart'],
      fluxoprocesso: processoForm.value['processo'],
    };
  }

}
