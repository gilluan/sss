import {Component, Input, OnInit} from '@angular/core';
import {ControlContainer, FormGroup, FormGroupDirective} from '@angular/forms';

@Component({
  selector: 'sins-programas-infos-gerais',
  templateUrl: './infos-gerais.component.html',
  styleUrls: ['./infos-gerais.component.scss'],
  viewProviders: [{provide: ControlContainer, useExisting: FormGroupDirective}]
})
export class InfosGeraisComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() scEditavel = true;

  constructor() { }

  ngOnInit(): void { }

}
