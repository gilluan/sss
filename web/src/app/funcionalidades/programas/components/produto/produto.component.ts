import {Component, Input, OnInit} from '@angular/core';
import {ControlContainer, FormArray, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';

const DEFAULT_VALIDATORS = [ Validators.required ];

@Component({
  selector: 'sins-programas-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.scss'],
  viewProviders: [{provide: ControlContainer, useExisting: FormGroupDirective}]
})
export class ProdutoComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() scEditavel = true;


  get premissas(): FormArray {
    return this.parentForm.get('produtoForm').get('premissas') as FormArray;
  }

  get riscos(): FormArray {
    return this.parentForm.get('produtoForm').get('riscos') as FormArray;
  }

  get restricoes(): FormArray {
    return this.parentForm.get('produtoForm').get('restricoes') as FormArray;
  }

  get requisitos(): FormArray {
    return this.parentForm.get('produtoForm').get('requisitos') as FormArray;
  }

  constructor() { }

  ngOnInit(): void { }

  adicionarPremissa = (): void => this.premissas.push(new FormControl('', DEFAULT_VALIDATORS));

  adicionarRisco = (): void => this.riscos.push(new FormControl('', DEFAULT_VALIDATORS));

  adicionarRestricao = (): void => this.restricoes.push(new FormControl('', DEFAULT_VALIDATORS));

  adicionarRequisito = (): void => this.requisitos.push(new FormControl('', DEFAULT_VALIDATORS));

  removerPremissa = (index: number): void => this.premissas.removeAt(index);

  removerRisco = (index: number): void => this.riscos.removeAt(index);

  removerRestricao = (index: number): void => this.restricoes.removeAt(index);

  removerRequisito = (index: number): void => this.requisitos.removeAt(index);

}
