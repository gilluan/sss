import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'sc-novo-programa',
    templateUrl: './novo-programa.component.html',
    styleUrls: ['./novo-programa.component.css']
  })
export class NovoProgramaComponent implements OnInit{
    
    programas$ = [];
    constructor(private router: Router) {

    }

    ngOnInit() {
        
    }

    public adicionarPrograma() {
        this.router.navigate(['/programas/criar-programa']);
    }
}