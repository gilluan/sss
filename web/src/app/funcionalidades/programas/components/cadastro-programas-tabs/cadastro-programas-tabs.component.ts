import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import * as fromProgramas from '@app/funcionalidades/programas/reducers/programas.reducer';
import {Subscription} from 'rxjs';
import {TabComponent, TabPanelComponent} from '@sicoob/ui';

@Component({
  selector: 'sins-cadastro-programas-tabs',
  templateUrl: './cadastro-programas-tabs.component.html',
  styleUrls: ['./cadastro-programas-tabs.component.scss']
})
export class CadastroProgramasTabsComponent implements OnInit, OnDestroy {

  @ViewChild('tabPanelFiltro') tabPanelFiltro: TabPanelComponent;
  @ViewChild('tabProcesso') tabProcesso: TabComponent;

  @Input() parentForm: FormGroup;

  @Output() changeTab = new EventEmitter();
  @Output() scFinalizar = new EventEmitter();
  @Output() scAtivar = new EventEmitter();
  @Output() scNavegarFluxoProcesso = new EventEmitter();

  programaAtivo = false;
  programaEditavel = true;

  private subscriprion: Subscription = new Subscription();

  get infoGeralForm() {
    return this.parentForm.controls.infosGeraisForm;
  }

  get produtoForm() {
    return this.parentForm.controls.produtoForm;
  }

  get objetivosForm() {
    return this.parentForm.controls.objetivosForm;
  }

  get processoForm() {
    return this.parentForm.controls.processoForm;
  }

  constructor(private store: Store<fromProgramas.State>) { }

  ngOnInit(): void {
    this.subscriprion
      .add(this.store.pipe(select(fromProgramas.selectAtivo)).subscribe(ativo => this.programaAtivo = ativo))
      .add(this.store.pipe(select(fromProgramas.selectEditavel)).subscribe(editavel => this.programaEditavel = editavel));
  }

  ngOnDestroy(): void {
    this.subscriprion.unsubscribe();
  }

  onTabSelected(tabIndex) {
    if (this.checkSelectionValid(tabIndex)) {
      this.changeTab.emit(tabIndex);
    }
  }

  finalizar(): void {
    this.scFinalizar.emit();
  }

  ativar(): void {
    this.scAtivar.emit();
  }

  navegarFluxoProcesso(): void {
    this.scNavegarFluxoProcesso.emit();
  }

  hasProcesso(): boolean {
    const processoControl = this.processoForm.get('processo');

    if (!processoControl) {
      return false;
    }

    return !!processoControl.value;
  }

  navegarTabProcesso(): void {
    this.tabPanelFiltro.selectTab(this.tabProcesso);
  }

  private checkSelectionValid(tabIndex): boolean {
    switch (tabIndex) {
      case 1: {
        return true;
      }
      case 2: {
        return this.infoGeralForm.valid;
      }
      case 3: {
        return this.infoGeralForm.valid && this.objetivosForm.valid;
      }
      case 4: {
        return this.infoGeralForm.valid && this.objetivosForm.valid && this.produtoForm.valid;
      }
    }
  }
}
