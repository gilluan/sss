import {Component, Input, OnInit} from '@angular/core';
import {ControlContainer, FormGroup, FormGroupDirective} from '@angular/forms';

@Component({
  selector: 'sins-programas-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.scss'],
  viewProviders: [{provide: ControlContainer, useExisting: FormGroupDirective}]
})
export class ObjetivosComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() scEditavel = true;

  constructor() { }

  ngOnInit(): void { }

}
