import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as fromFluxoProcesso from '../../../../fluxo-processo/reducers/fluxo-processo.reducer';
import * as fromProgramas from '../../../reducers/programas.reducer';
import {select, Store} from '@ngrx/store';
import {LoadFluxoProcessoList} from '@app/funcionalidades/fluxo-processo/actions/fluxo-processo.actions';
import {FluxoProcessoFilter} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo-filter.model';
import {PageModelVm} from '@app/shared/models/page.model';
import {FluxoProcesso} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo.model';
import {PagedData} from '@app/shared/models/paged-data.model';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';
import {ListarIndicadoresFluxo, SalvarPrograma} from '@app/funcionalidades/programas/actions/programas.actions';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'sins-step-tarefas',
  templateUrl: './step-tarefas.component.html',
  styleUrls: ['./step-tarefas.component.css']
})
export class StepTarefasComponent implements OnInit {

  @Input() public scPrograma: Programa;

  @Output() public feedback: EventEmitter<any> = new EventEmitter();

  public filter: FluxoProcessoFilter =  this.initFilter();
  public fluxosProcesso: Array<FluxoProcesso>;
  public idFluxoSelecionado: string;

  private subscription: Subscription = new Subscription();

  constructor(
    private fluxoProcessoStore: Store<fromFluxoProcesso.State>,
    private programaStore: Store<fromProgramas.State>,
    private router: Router) { }

  ngOnInit() {
    this.fluxoProcessoStore.dispatch(new LoadFluxoProcessoList(this.filter));

    const fluxoListObservable = this.fluxoProcessoStore.pipe(select(fromFluxoProcesso.selectFluxoProcessoList));

    // TODO implementar paginação
    this.subscription.add(fluxoListObservable.subscribe((data: PagedData<FluxoProcesso>) => this.fluxosProcesso = data.data));
  }

  receiverOutputClick(event) {
    if (this.idFluxoSelecionado === event) {
      this.router.navigate([`fluxo-processo/${this.idFluxoSelecionado}`]);
    } else {
      this.idFluxoSelecionado = event;
      this.scPrograma.fluxoprocesso = this.idFluxoSelecionado;
    }
  }

  isSelecionado = (idCard): boolean => idCard === this.idFluxoSelecionado;

  isStepCompleted = (): boolean => this.scPrograma.fluxoprocesso !== null && this.scPrograma.fluxoprocesso !== undefined;

  finalizar(): void {
    if (this.isStepCompleted()) {
      const output: OutputFeedbackTarefas = { stepTarefasCompleto: true, programa: this.scPrograma };
      this.feedback.emit(output);
      this.programaStore.dispatch(new SalvarPrograma(this.scPrograma.imagem, this.scPrograma));
      this.programaStore.dispatch(new ListarIndicadoresFluxo(this.scPrograma.fluxoprocesso));
    }
  }

  private initFilter() {
    const filter = new FluxoProcessoFilter();
    filter.paginacao = new PageModelVm(0, 8, 0);
    return filter;
  }

  abrirModalNovoFluxo() {

  }
}

export interface OutputFeedbackTarefas {
  stepTarefasCompleto: boolean;
  programa: Programa;
}
