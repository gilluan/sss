import {Component, DoCheck, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import * as fromPortfolio from '../../../portfolio/reducers/portfolio.reducer';
import * as fromProgramas from '../../reducers/programas.reducer';
import {select, Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {ListarPortfolios} from '../../../portfolio/actions/portfolio.actions';
import * as fromApp from '../../../../app.reduce';
import {AjustarPadding} from '@app/app.actions';
import {Programa} from '../../models/programas.model';
import {OutputStepArquivos} from './step-arquivos/step-arquivos.component';
import {OutputFeedbackTarefas} from './step-tarefas/step-tarefas.component';
import {PortfolioService} from '@app/funcionalidades/portfolio/portfolio.service';

@Component({
  selector: 'sc-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit, DoCheck {
  @Output() public feedback = new EventEmitter();

  portfolios;
  programa: Programa;
  anos = [];

  emptyImg = '';
  isDisabled: boolean;
  informacoesGeraisActive = false;

  minDate;
  maxDate;

  stepInfoCompleto = false;
  stepArquivosCompleto = false;
  stepTarefasCompleto = false;
  stepAtivarProgramaCompleto = false;

  programaForm = this.fb.group({
    imagem: ['', Validators.required],
    nome: ['', Validators.required],
    portfolio: ['', Validators.required],
    vigencia: ['', Validators.required]
  });

  private subscriptions: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private portfoliosStore: Store<fromPortfolio.State>,
    private portfolioService: PortfolioService,
    private appStore$: Store<fromApp.State>,
    private programaStore$: Store<fromProgramas.State>) { }

  ngDoCheck(): void {
    this.programa.nome = this.programaForm.value.nome;
    this.programa.vigencia = this.programaForm.value.vigencia;
    this.programa.portfolio = this.programaForm.value.portfolio;

    this.sendFeedback();
  }

  ngOnInit() {
    this.programaStore$.pipe(select(fromProgramas.selectPrograma)).subscribe((programa: Programa) => {
      this.programa = programa;
      if (this.programa && this.programa.id) {
        this.programaForm.controls.imagem.setValue(programa.imagem);
        this.programaForm.controls.nome.setValue(programa.nome);
        this.programaForm.controls.portfolio.setValue(programa.portfolio._id);
        this.programaForm.controls.vigencia.setValue(programa.vigencia);
      }
    });

    const anoVigente = new Date().getFullYear();

    this.anos.push(anoVigente);
    this.anos.push(anoVigente + 1);
    this.anos.push(anoVigente + 2);
    this.anos.push(anoVigente + 3);
    this.portfoliosStore.dispatch(new ListarPortfolios());
    const portfoliosObservable = this.portfoliosStore.pipe(select(fromPortfolio.selectPortfolios)).subscribe(portfolios => {
      this.portfolios = portfolios;
    });

    this.appStore$.dispatch(new AjustarPadding(0));
  }

  cadastrarInformacoesGerais() {
    this.informacoesGeraisActive = true;
  }

  receiveFeedbackUploadImagem(event: Event) {

    this.programaForm.controls['imagem'].setValue(event);
    this.programa.imagem = this.programaForm.value.imagem;
  }

  receiveFeedbackArquivos(feedback: OutputStepArquivos) {
    // this.stepArquivosCompleto = feedback.completado;
  }

  receiveFeedbackTarefas(feedback: OutputFeedbackTarefas) {
    this.stepTarefasCompleto = feedback.stepTarefasCompleto;
    this.programa = feedback.programa;
    this.sendFeedback();
  }

  lerAnoVigencia(event) {
    this.programaForm.controls['vigencia'].setValue(event);
  }

  concluir($event) {
    const stepIndex = $event.step;
    if (stepIndex) {
      switch (stepIndex) {
        case 1:
          //this.store$.dispatch(new AddInformacoes(this.informacoesProjeto));
          console.debug('Concluindo o primeiro passo!');
          break;
        case 2:
          //this.store$.dispatch(new AddEscopos(this.escoposProjeto));
          break;
        default:
          break;
      }
      if ($event.isLast) {
        //this.store$.dispatch(new AddMetas(this.metasProjeto));
        //this.salvar(false)
      }
    }
  }

  sendFeedback() {
    this.feedback.emit(this.programa);
  }

  receiverFeedbackAbas(feedback: Programa) {
    this.programa.descricao = feedback.descricao;
    this.programa.justificativa = feedback.justificativa;
    this.programa.partesInteressadas = feedback.partesInteressadas;
    this.programa.produto = feedback.produto;
    this.programa.beneficios = feedback.beneficios;
    this.programa.premissas = feedback.premissas;
    this.programa.riscos = feedback.riscos;
    this.programa.restricoes = feedback.restricoes;
    this.programa.requisitos = feedback.requisitos;
    this.programa.objetivoGeral = feedback.objetivoGeral;
    this.programa.objetivoEspecifico = feedback.objetivoEspecifico;
    this.programa.objetivoSmart = feedback.objetivoSmart;

    this.stepInfoCompleto = this.programaForm.valid && feedback['valids'].infosGerais && feedback['valids'].produto && feedback['valids'].objetivos;

  }

  stepInfosCompleto($event) {
    this.stepInfoCompleto = $event.formValid;
  }
}
