import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SalvarArquivosPrograma} from '@app/funcionalidades/programas/actions/programas.actions';
import {ArquivoPrograma} from '@app/funcionalidades/programas/models/arquivo-programa.model';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';
import * as fromGed from '@app/reducers/ged.reducer';
import {MultiUploadFile} from '@app/shared/models/multiupload-file.model';
import {select, Store} from '@ngrx/store';
import * as fromPrograma from '../../../reducers/programas.reducer';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sins-step-arquivos',
  templateUrl: './step-arquivos.component.html',
  styleUrls: ['./step-arquivos.component.css']
})
export class StepArquivosComponent implements OnInit, OnDestroy {

  @Input() scPrograma: Programa;

  @Output() scCompleted: EventEmitter<any> = new EventEmitter<any>();

  subscription: Subscription = new Subscription();

  arquivosPrograma: MultiUploadFile[];

  constructor(private gedStore: Store<fromGed.State>, private programaStore$: Store<fromPrograma.State>) { }

  ngOnInit(): void {
    const arquivosProgramaObservable = this.gedStore.pipe(select(fromGed.getFileUploadMultiple));

    this.subscription
        .add(arquivosProgramaObservable.subscribe((arquivos: MultiUploadFile[]) => this.arquivosProgramaObserver(arquivos)));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  checkArquivosEnviados(): boolean {
    return (this.arquivosPrograma || []).length > 0;
  }

  finalizar(): void {
    if (this.arquivosPrograma) {
      const arquivosProgramaParaEnvio: ArquivoPrograma[] = this.arquivosPrograma.map(arquivo => this.parseArquivo(arquivo));
      this.programaStore$.dispatch(new SalvarArquivosPrograma(arquivosProgramaParaEnvio));
    }
  }

  private arquivosProgramaObserver(arquivosPrograma: MultiUploadFile[]): void {
    this.arquivosPrograma = arquivosPrograma;
    const outputStepArquivos: OutputStepArquivos = { files: this.arquivosPrograma, completed: (arquivosPrograma || []).length > 0 };
    this.scCompleted.emit(outputStepArquivos);
  }

  private parseArquivo(arquivo: MultiUploadFile): ArquivoPrograma {
    if (!arquivo) {
      return null;
    }
    const arquivoPrograma = new ArquivoPrograma();
    arquivoPrograma.idGed = arquivo.identificadorGed;
    arquivoPrograma.nome = arquivo.nome;
    arquivoPrograma.tamanho = arquivo.tamanho;
    arquivoPrograma.programaId = this.scPrograma.id;
    return arquivoPrograma;
  }
}

export interface OutputStepArquivos {
  completed: boolean;
  files: MultiUploadFile[];
}
