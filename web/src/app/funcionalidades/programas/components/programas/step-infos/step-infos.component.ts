import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';
import {Portfolio} from '@app/funcionalidades/portfolio/models/portfolio.model';
import {select, Store} from '@ngrx/store';
import * as fromPortfolio from '@app/funcionalidades/portfolio/reducers/portfolio.reducer';
import * as fromProgramas from '@app/funcionalidades/programas/reducers/programas.reducer';
import {range, Subscription} from 'rxjs';
import {ListarPortfolios} from '@app/funcionalidades/portfolio/actions/portfolio.actions';

@Component({
  selector: 'sins-step-infos',
  templateUrl: './step-infos.component.html',
  styleUrls: ['./step-infos.component.css']
})
export class StepInfosComponent implements OnInit {
  @Input() scPrograma: Programa;

  @Output() scCompleted = new EventEmitter<any>();

  portfolios: Portfolio[];
  anos = [];

  stepInfoForm = this.fb.group({
    programaForm: this.fb.group({
      imagem: ['', Validators.required],
      nome: ['', Validators.required],
      portfolio: ['', Validators.required],
      vigencia: ['', Validators.required]
    }),
    infosGeraisForm: this.fb.group({
      descricao: ['', Validators.required],
      justificativa: ['', Validators.required],
      partesInteressadas: ['', Validators.required]
    }),
    produtoForm: this.fb.group({
      produto: ['', Validators.required],
      beneficios: ['', Validators.required],
      premissas: this.fb.array([], Validators.required),
      riscos: this.fb.array([], Validators.required),
      restricoes: this.fb.array([], Validators.required),
      requisitos: this.fb.array([], Validators.required)
    }),
    objetivosForm: this.fb.group({
      objetivoGeral: ['', Validators.required],
      objetivoEspecifico: ['', Validators.required],
      objetivoSmart: ['', Validators.required],
    })
  });

  get programaForm() {
    return this.stepInfoForm.get('programaForm');
  }

  private subscription: Subscription = new Subscription();

  constructor(
    private programaStore$: Store<fromProgramas.State>,
    private portfolioStore$: Store<fromPortfolio.State>,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.portfolioStore$.dispatch(new ListarPortfolios());

    const portfolioObservable = this.portfolioStore$.pipe(select(fromPortfolio.selectPortfolios));

    this.subscription
      .add(this.stepInfoForm.valueChanges.subscribe(_ => this.scCompleted.emit({
        programa: this.parsePrograma(),
        formValid: this.stepInfoForm.valid
      })))
      .add(portfolioObservable.subscribe((portfolio: Portfolio[]) => this.portfolioObserver(portfolio)));

    range(new Date().getFullYear(), 3).subscribe(ano => this.anos.push(ano));

    this.programaObserver(this.scPrograma);
  }

  receiveFeedbackUploadImagem(event: Event) {
    const programaForm = this.stepInfoForm.get('programaForm');
    programaForm.get('imagem').setValue(event);
    this.scPrograma.imagem = programaForm.value.imagem;
  }

  finalizar(finalizar: boolean = false) {
    this.scCompleted.emit({ programa: this.parsePrograma(), formValid: this.stepInfoForm.valid, finalizar });
  }

  private programaObserver(programa: Programa) {
    if (programa) {
      this.scPrograma = programa;

      if (this.scPrograma.id || this.scPrograma.id) {
        this.patchArray(programa.premissas, 'produtoForm', 'premissas');
        this.patchArray(programa.riscos, 'produtoForm', 'riscos');
        this.patchArray(programa.restricoes, 'produtoForm', 'restricoes');
        this.patchArray(programa.requisitos, 'produtoForm', 'requisitos');

        this.stepInfoForm.patchValue({
          programaForm: {
            imagem: programa.imagem,
            nome: programa.nome,
            portfolio: programa.portfolio._id,
            vigencia: programa.vigencia
          },
          infosGeraisForm: {
            descricao: programa.descricao,
            justificativa: programa.justificativa,
            partesInteressadas: programa.partesInteressadas
          },
          produtoForm: {
            produto: programa.produto,
            beneficios: programa.beneficios
          },
          objetivosForm: {
            objetivoGeral: programa.objetivoGeral,
            objetivoEspecifico: programa.objetivoEspecifico,
            objetivoSmart: programa.objetivoSmart
          }
        });
      }
    }
  }

  private patchArray(values: string[], groupName: string, arrayName: string) {
    (values || []).forEach(v => {
      const produtoForm = <FormGroup>this.stepInfoForm.controls[groupName];
      const premissas = <FormArray>produtoForm.controls[arrayName];
      premissas.push(new FormControl(v));
    });
  }

  private portfolioObserver(portfolios: Portfolio[]) {
    if (portfolios) {
      this.portfolios = portfolios;
    }
  }

  private parsePrograma() {
    const programaForm = <FormGroup>this.stepInfoForm.get('programaForm');
    const infosGeraisForm = <FormGroup>this.stepInfoForm.get('infosGeraisForm');
    const produtoForm = <FormGroup>this.stepInfoForm.get('produtoForm');
    const objetivosForm = <FormGroup>this.stepInfoForm.get('objetivosForm');

    const programa: Programa = {
      nome: programaForm.value['nome'],
      portfolio: programaForm.value['portfolio'],
      vigencia: programaForm.value['vigencia'],
      imagem: programaForm.value['imagem'],
      descricao: infosGeraisForm.value['descricao'],
      justificativa: infosGeraisForm.value['justificativa'],
      partesInteressadas: infosGeraisForm.value['partesInteressadas'],
      produto: produtoForm.value['produto'],
      beneficios: produtoForm.value['beneficios'],
      premissas: produtoForm.value['premissas'],
      riscos: produtoForm.value['riscos'],
      restricoes: produtoForm.value['restricoes'],
      requisitos: produtoForm.value['requisitos'],
      objetivoGeral: objetivosForm.value['objetivoGeral'],
      objetivoEspecifico: objetivosForm.value['objetivoEspecifico'],
      objetivoSmart: objetivosForm.value['objetivoSmart']
    };

    return programa;
  }
}
