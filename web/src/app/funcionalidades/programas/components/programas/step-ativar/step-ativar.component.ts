import {Component, Input, OnInit} from '@angular/core';
import {SalvarIndicadorPrograma, SalvarPrograma} from '@app/funcionalidades/programas/actions/programas.actions';
import * as fromProgramas from '../../../reducers/programas.reducer';
import {select, Store} from '@ngrx/store';
import {Indicador} from '@app/funcionalidades/projeto/models/indicador.model';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';

@Component({
  selector: 'sins-step-ativar',
  templateUrl: './step-ativar.component.html',
  styleUrls: ['./step-ativar.component.css']
})
export class StepAtivarComponent implements OnInit {

  @Input() public scPrograma: Programa;

  constructor(private programaStore$: Store<fromProgramas.State>) { }

  public indicadoresDisponiveis: any[] = [];

  ngOnInit() {
    this.programaStore$.pipe(select(fromProgramas.selectIndicadores)).subscribe(response => {
      this.indicadoresDisponiveis = response;
    });
  }

  isIndicadoresVazio() {
    return this.indicadoresDisponiveis.length === 0;
  }

  ativarPrograma() {
    const indicadores: Indicador[] = this.criarIndicadores();
    console.log('ativar programa!');
    this.programaStore$.dispatch(new SalvarIndicadorPrograma(indicadores));
    this.scPrograma.ativo = true;
    this.programaStore$.dispatch(new SalvarPrograma(this.scPrograma.imagem, this.scPrograma));
  }

  private criarIndicadores() {
    const indicadores: Indicador[] = [];

    for (const i of this.indicadoresDisponiveis) {
      const indicador = new Indicador();
      indicador.nome = i.nomeIndicador;
      indicador.programaId = this.scPrograma.id;
      indicadores.push(indicador);
    }

    return indicadores;
  }

}
