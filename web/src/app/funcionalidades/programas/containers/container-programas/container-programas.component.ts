import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {TemplatePortal} from '@angular/cdk/portal';
import {HeaderActionsContainerService} from '@sicoob/ui';
import {Programa} from '../../models/programas.model';
import * as fromProgramas from '../../reducers/programas.reducer';
import {
  ClearPrograma,
  DetalharPrograma,
  SalvarEAtivarPrograma,
  SalvarPrograma,
  SaveAndNavigateFluxoProcesso
} from '../../actions/programas.actions';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sc-container-programas',
  templateUrl: './container-programas.component.html',
  styleUrls: ['./container-programas.component.css']
})
export class ContainerProgramasComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('botoesProgramas') headerActionsTmpl: TemplateRef<any>;

  idPrograma: string;
  programa: Programa;
  programaForm: Programa;
  ativo = false;

  isProgramaFormValid = false;

  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<fromProgramas.State>,
    private headerActionsService: HeaderActionsContainerService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const programaObservable = this.store.pipe(select(fromProgramas.selectPrograma));

    this.subscription
      .add(this.route.paramMap.subscribe(params => this.paramsObserver(params)))
      .add(programaObservable.subscribe(programa => this.programaObserver(programa)));
  }

  ngAfterViewInit(): void {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.headerActionsService.remove();
  }

  salvarRascunho(): void {
    this.store.dispatch(new SalvarPrograma(this.programaForm.imagem, this.programaForm));
  }

  atualizarPrograma($event): void {
    this.isProgramaFormValid = $event.valid;
    this.programaForm = $event.programa;
    if ($event.valid && $event.finalizar) {
      this.salvarRascunho();
    }
  }

  navegarParaFluxoProcesso(): void {
    this.programaForm.id = this.idPrograma;
    this.store.dispatch(new SaveAndNavigateFluxoProcesso(this.programaForm));
  }

  ativar(): void {
    this.programaForm.id = this.idPrograma;
    this.store.dispatch(new SalvarEAtivarPrograma(this.programaForm));
  }

  private paramsObserver(params: ParamMap): void {
    if (params) {
      this.idPrograma = params.get('id');
      this.store.dispatch(new DetalharPrograma(params.get('id')));
    }
  }

  private programaObserver(programa: Programa): void {
    if (programa && programa.id) {
      this.ativo = programa.ativo;
      this.programa = Object.assign(new Programa(), programa);
      this.programaForm = Object.assign(new Programa(), programa);
    }
  }
}
