import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Programa} from '../models/programas.model';
import {ProgramasActions, ProgramasActionTypes} from '../actions/programas.actions';
import {PageModelVm} from '@app/shared/models/page.model';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ArquivoPrograma} from '../models/arquivo-programa.model';

export interface State extends EntityState<Programa> {
  paginacao: PageModelVm;
  programas: Programa[];
  indicadores: Array<String>;
  programa: Programa;
  arquivoPrograma: ArquivoPrograma;
  arquivosPrograma: Array<ArquivoPrograma>;
}

export const adapter: EntityAdapter<Programa> = createEntityAdapter<Programa>();

export const initialState: State = adapter.getInitialState({
  paginacao: new PageModelVm(0, 8, 0),
  indicadores: [],
  programas: [],
  programa: new Programa(),
  arquivoPrograma: new ArquivoPrograma(),
  arquivosPrograma: [],
});

export function reducer(
  state = initialState,
  action: ProgramasActions
): State {
  switch (action.type) {
    case ProgramasActionTypes.LoadProgramaSuccess: {
      return { ...state, programa: action.programa };
    }

    case ProgramasActionTypes.SalvarProgramaSuccess: {
      return {...state, programa: action.programa };
    }

    case ProgramasActionTypes.ListaProgramasSuccess: {
      return { ...state, programas: action.programas};
    }

    case ProgramasActionTypes.ListarIndicadoresFluxoSuccess: {
      return {...state, indicadores: action.indicadores};
    }

    case ProgramasActionTypes.SalvarArquivoPrograma: {
      return {...state, arquivoPrograma: action.arquivoPrograma};
    }

    case ProgramasActionTypes.DetalharProgramaSuccess: {
      return {...state, programa: action.programa};
    }

    case ProgramasActionTypes.DetalharProgramaFail: {
      return {...state};
    }

    case ProgramasActionTypes.ClearPrograma: {
      return adapter.removeAll({
        ...state,
        paginacao: null,
        programas: null,
        indicadores: null,
        programa: null,
        arquivoPrograma: null,
        arquivosPrograma: null,
        imagemPrograma: null,
      });
    }

    default: {
      return state;
    }
  }
}

export const getProgramasState = createFeatureSelector<State>('programaSelector');

export const selectProgramas = createSelector(
  getProgramasState,
  (state: State) => state.programas
);

export const selectPrograma = createSelector(
  getProgramasState,
  (state: State) => state.programa
);

export const selectAtivo = createSelector(
  getProgramasState,
  (state: State) => state.programa.ativo
);

export const selectEditavel = createSelector(
  getProgramasState,
  (state: State) => !state.programa.possuiProjetoVinculado
);

export const selectIndicadores = createSelector(
  getProgramasState,
  (state: State) => state.indicadores
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
