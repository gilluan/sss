import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerProgramasComponent } from './containers/container-programas/container-programas.component';
import { ProgramasComponent } from './components/programas/programas.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerProgramasComponent ,
  },
  {
    path: 'criar-programa',
    component: ProgramasComponent ,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramasRoutingModule { }
