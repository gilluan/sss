import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Programa } from './models/programas.model';
import { ArquivoPrograma } from './models/arquivo-programa.model';
import { Indicador } from '../projeto/models/indicador.model';
import { catchError, switchMap, tap, map } from 'rxjs/operators';
import { Service } from '@app/shared/services/service';
import { Observable } from 'rxjs';
import { Validations } from '@shared/utils/validations';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const formDataOptions = {
  headers: new HttpHeaders({
    'enctype': 'multipart/form-data'
  })
};

const notNull = Validations.notNull;

@Injectable({
  providedIn: 'root'
})
export class ProgramasService extends Service {

  private RESOURCE_PROGRAMAS = 'programas';
  private RESOURCE_URL_PROGRAMAS = `${environment.modulo_gestao_projeto}/${this.RESOURCE_PROGRAMAS}`;
  private RESOURCE_ARQUIVOS_PROGRAMA = `arquivos-programas`;
  private RESOURCE_INDICADORES_PROGRAMA = '/indicadores';
  private RESOURCE_URL_TAREFA_INDICADORES = '/tarefas/indicadores';

  constructor(private http: HttpClient) { super(); }

  listarProgramas(tamanhoPagina: number, numeroPagina: number, portfolio?: string) {
    let url = `${this.RESOURCE_URL_PROGRAMAS}?tamanhoPagina=${tamanhoPagina}&numeroPagina=${numeroPagina}`;
    notNull(portfolio, p => url += `&portfolio=${p}`);
    return this.http.get(url, httpOptions);
  }

  criarPrograma(programa: Programa) {
    return this.http.post(`${this.RESOURCE_URL_PROGRAMAS}`, programa, httpOptions);
  }

  alterarPrograma(programa: Programa): Observable<Programa> {
    return this.http.put<Programa>(`${this.RESOURCE_URL_PROGRAMAS}/${programa.id}`, programa, httpOptions)
      .pipe(catchError(this.handleError));
  }

  alterarImagem(imagem: File, idPrograma: string): Observable<string> {
    const formData = new FormData();
    formData.append('imagem', imagem, imagem.name);

    return this.http.patch<string>(`${this.RESOURCE_URL_PROGRAMAS}/${idPrograma}`, formData, formDataOptions).pipe(
      catchError(this.handleError));
  }

  getImagem(idPrograma: string) {
    let url = `${this.RESOURCE_URL_PROGRAMAS}/${idPrograma}/imagem`;
    return this.http.get<{ resultado: string }>(url).pipe(map((resultado) => resultado.resultado), catchError(this.handleError));
  }

  vincularFluxoProcesso(idPrograma: string, idFluxo: string): Observable<Programa> {
    return this.findById(idPrograma).pipe(
      switchMap((programa: Programa) => {
        programa.fluxoprocesso = idFluxo;
        return this.alterarPrograma(programa);
      }),
      catchError(this.handleError));
  }

  ativar(idPrograma: string): Observable<Programa> {
    const url = `${this.RESOURCE_URL_PROGRAMAS}/${idPrograma}/ativar`;
    return this.http.put<Programa>(url, httpOptions).pipe(catchError(this.handleError));
  }

  /**
   * @deprecated
   * @param idPrograma
   */
  ativarPrograma(idPrograma: string) {
    return this.findById(idPrograma).pipe(
      switchMap((programa: Programa) => {
        programa.ativo = true;
        return this.alterarPrograma(programa);
      }),
      catchError(this.handleError));
  }

  listarIndicadores(idFluxoProcesso: string) {
    const url = `${environment.modulo_gestao_tarefa}${this.RESOURCE_URL_TAREFA_INDICADORES}?identificadorFluxo=${idFluxoProcesso}`;
    return this.http.get(url, httpOptions);
  }

  criarIndicadoresPrograma(indicadores: Indicador[]) {
    console.log('from Service>', indicadores);
    const url = `${environment.modulo_gestao_projeto}${this.RESOURCE_INDICADORES_PROGRAMA}`;
    return this.http.post(url, indicadores, httpOptions);
  }

  salvarArquivosPrograma(arquivoPrograma: ArquivoPrograma[]) {
    const url = `${environment.modulo_gestao_projeto}/${this.RESOURCE_ARQUIVOS_PROGRAMA}`;
    return this.http.post(url, arquivoPrograma, httpOptions);
  }

  detalharPrograma(idPrograma: string): Observable<Programa> {
    const url = `${this.RESOURCE_URL_PROGRAMAS}/${idPrograma}/detalhado`;
    return this.http.get<Programa>(url, httpOptions).pipe(catchError(this.handleError));
  }

  findById(idPrograma: string, detalhado: boolean = false): Observable<Programa> {
    let url = `${this.RESOURCE_URL_PROGRAMAS}/${idPrograma}`;
    if (detalhado) {
      url += '/detalhado';
    }
    return this.http.get<Programa>(url, httpOptions).pipe(catchError(this.handleError));
  }
}
