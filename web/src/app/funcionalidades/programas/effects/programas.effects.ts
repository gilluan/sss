import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  AtivarPrograma,
  AtivarProgramaFail,
  AtivarProgramaSuccess,
  DetalharPrograma,
  DetalharProgramaFail,
  DetalharProgramaSuccess,
  ListaProgramas,
  ListaProgramasSuccess,
  ListarIndicadoresFluxo,
  ListarIndicadoresFluxoSuccess,
  LoadPrograma,
  LoadProgramaFail,
  LoadProgramaSuccess,
  OpenDialogAtivarPrograma,
  OpenDialogAtivarProgramaSuccess,
  ProgramasActionTypes,
  SalvarArquivoPrograma,
  SalvarArquivoProgramaSuccess,
  SalvarArquivosPrograma,
  SalvarArquivosProgramaSuccess, SalvarEAtivarPrograma, SalvarEAtivarProgramaFail,
  SalvarImagemPrograma,
  SalvarImagemProgramaFail,
  SalvarImagemProgramaSuccess,
  SalvarIndicadorPrograma,
  SalvarIndicadorProgramaFail,
  SalvarIndicadorProgramaSuccess,
  SalvarPrograma,
  SalvarProgramaFail,
  SalvarProgramaSuccess,
  SaveAndNavigateFluxoProcesso,
  SaveAndNavigateFluxoProcessoFail
} from '../actions/programas.actions';
import {catchError, map, mergeMap, switchMap, withLatestFrom} from 'rxjs/operators';
import {ProgramasService} from '../programas.service';
import {CustomAlertService} from '@app/shared/services/alert-service';
import {Color, ModalService} from '@sicoob/ui';
import {of} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {Router} from '@angular/router';
import * as fromProgramas from '../reducers/programas.reducer';
import {Programa} from '../models/programas.model';
import {DialogoConfirmacaoComponent} from '@shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import {NoActionPassoFluxo} from '@app/funcionalidades/fluxo-processo/actions/passo-fluxo.actions';
import {NavigateFluxoProcesso} from '@app/funcionalidades/fluxo-processo/actions/fluxo-processo.actions';

const noDispatch = { dispatch: false };

@Injectable()
export class ProgramasEffects {

  constructor(
    private actions$: Actions,
    private programaService: ProgramasService,
    private alertService: CustomAlertService,
    private programaStore$: Store<fromProgramas.State>,
    private router: Router,
    private modalService: ModalService) { }

  @Effect()
  loadPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.LoadPrograma),
    switchMap((action: LoadPrograma) => this.programaService.findById(action.idPrograma, action.detalhado).pipe(
      map((programa: Programa) => new LoadProgramaSuccess(programa)),
      catchError(error => of(new LoadProgramaFail(error))))));

  @Effect({dispatch: false})
  loadProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.LoadProgramaFail),
    map((action: LoadProgramaFail) => action.payload),
    map((error: string) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar o programa.')));

  @Effect()
  listarIndicadores$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.ListarIndicadoresFluxo),
    map((action: ListarIndicadoresFluxo) => action),
    switchMap((action) => this.programaService.listarIndicadores(action.idFluxo).pipe(
      map((indicadores) => new ListarIndicadoresFluxoSuccess(indicadores['resultado'])))));

  @Effect()
  selectProgramas$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.ListaProgramas),
    map((action: ListaProgramas) => action),
    switchMap((action) => this.programaService.listarProgramas(action.tamanhoPagina, action.numeroPagina).pipe(
      map((listaProgramaPaginada) => new ListaProgramasSuccess(listaProgramaPaginada['resultado']['data'])))));

  @Effect()
  salvarPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarPrograma),
    withLatestFrom(this.programaStore$.pipe(select(fromProgramas.selectPrograma))),
    mergeMap(([action, programa]: [SalvarPrograma, Programa]) => {
      const programaPatched = Object.assign(programa, action.programa);
      return this.programaService.alterarPrograma(programaPatched).pipe(
        switchMap((programaAlterado: Programa) => action.imagem ?
          [ new SalvarProgramaSuccess(programaAlterado), new SalvarImagemPrograma(action.imagem, programaAlterado.id) ] :
          [ new SalvarProgramaSuccess(programaAlterado) ]));
    }));

  @Effect(noDispatch)
  salvarProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarProgramaSuccess),
    map((action: SalvarProgramaSuccess) => this.alertService.abrirAlert(Color.SUCCESS, 'Programa salvo com sucesso.')));

  @Effect(noDispatch)
  salvarProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarProgramaFail),
    map((action: SalvarProgramaFail) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar o programa.')));

  @Effect()
  salvarImagemPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarImagemPrograma),
    switchMap((action: SalvarImagemPrograma) => this.programaService.alterarImagem(action.imagem, action.idPrograma).pipe(
      map(_ => new SalvarImagemProgramaSuccess()),
      catchError(error => of(new SalvarImagemProgramaFail(error))))));

  @Effect(noDispatch)
  salvarImagemProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarImagemProgramaFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar a imagem do programa')));

  @Effect()
  salvarArquivoPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarArquivoPrograma),
    map((action: SalvarArquivoPrograma) => action),
    switchMap(action => this.programaService.salvarArquivosPrograma([action.arquivoPrograma]).pipe(
      map(resultado => new SalvarArquivoProgramaSuccess()))));

  @Effect({dispatch: false})
  salvarArquivoProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarArquivoProgramaSuccess),
    map((action: SalvarArquivoProgramaSuccess) =>
      this.alertService.abrirAlert(Color.SUCCESS, 'Arquivo salvo com sucesso!')));

  @Effect()
  salvarArquivosPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarArquivosPrograma),
    map((action: SalvarArquivosPrograma) => action),
    switchMap(action => this.programaService.salvarArquivosPrograma(action.arquivosPrograma).pipe(
      map(resultado => new SalvarArquivoProgramaSuccess()))));

  @Effect({dispatch: false})
  salvarArquivosProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarArquivosProgramaSuccess),
    map((action: SalvarArquivosProgramaSuccess) =>
      this.alertService.abrirAlert(Color.SUCCESS, 'Arquivo salvo com sucesso!')));

  @Effect()
  salvarIndicadorPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarIndicadorPrograma),
    map((action: SalvarIndicadorPrograma) => action),
    switchMap((action: SalvarIndicadorPrograma) =>
      this.programaService.criarIndicadoresPrograma(action.indicadores).pipe(map(resultado => {
        return new SalvarIndicadorProgramaSuccess();
      }))),
    catchError(erro => of(new SalvarIndicadorProgramaFail())));

  @Effect({dispatch: false})
  salvarIndicadorProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarIndicadorProgramaSuccess),
    map((action: SalvarIndicadorProgramaSuccess) =>
      this.alertService.abrirAlert(Color.SUCCESS, 'Indicadores salvos com sucesso!')));

  @Effect({dispatch: false})
  salvarIndicadorProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarIndicadorProgramaFail),
    map((action: SalvarIndicadorProgramaFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar os indicadores.')));

  @Effect()
  detalharPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.DetalharPrograma),
    map((action: DetalharPrograma) => action),
    switchMap((action: DetalharPrograma) => this.programaService.detalharPrograma(action.id).pipe(
      map((programa: Programa) => new DetalharProgramaSuccess(programa)),
      catchError(_ => of(new DetalharProgramaFail())))));

  @Effect({ dispatch: false })
  detalharProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.DetalharProgramaFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Programa não encontrado.')));

  @Effect()
  openDialogAtivarPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.OpenDialogAtivarPrograma),
    map((action: OpenDialogAtivarPrograma) => action.idPrograma),
    map((idPrograma: string) => {
      const modalConfig = {
        data: {
          header: 'Ativar Programa',
          pergunta: `
            Após a ativação não será mais possível editar o programa.
            Portanto, tenha certeza de que cadastrou todos os marcos, passos e formulários.
          `,
          adicional: 'Deseja ativar o programa mesmo assim?',
          txtNao: 'Cancelar',
          txtSim: 'Ativar'
        },
        panelClass: 'sins-modal-confirm'
      };
      return new OpenDialogAtivarProgramaSuccess(this.modalService.open(DialogoConfirmacaoComponent, modalConfig), idPrograma);
    }));

  @Effect()
  openDialogAtivarProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.OpenDialogAtivarProgramaSuccess),
    switchMap((action: OpenDialogAtivarProgramaSuccess) => action.modalRef.afterClosed().pipe(
      map((confirmacao) => confirmacao ? new AtivarPrograma(action.idPrograma) : new NoActionPassoFluxo()))));

  @Effect()
  ativarPrograma$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.AtivarPrograma),
    map((action: AtivarPrograma) => action.idPrograma),
    switchMap((idPrograma: string) => this.programaService.ativar(idPrograma).pipe(
      map((programaAtivo: Programa) => new AtivarProgramaSuccess(programaAtivo)),
      catchError(error => of(new AtivarProgramaFail(error))))));

  @Effect(noDispatch)
  ativarProgramaSuccess$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.AtivarProgramaSuccess),
    map(_ => this.router.navigate(['/portfolios'])));

  @Effect(noDispatch)
  ativarProgramaFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.AtivarProgramaFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível ativar o programa.')));

  @Effect()
  saveAndNavigateFluxoProcesso$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SaveAndNavigateFluxoProcesso),
    switchMap((action: SaveAndNavigateFluxoProcesso) => this.programaService.alterarPrograma(action.programa).pipe(
      map((programa: Programa) => new NavigateFluxoProcesso(programa, programa.fluxoprocesso)),
      catchError(error => of(new SaveAndNavigateFluxoProcessoFail(error))))));

  @Effect(noDispatch)
  saveAndNavigateFluxoProcessoFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SaveAndNavigateFluxoProcessoFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar o programa antes de ir para fluxo de processo.')));

  @Effect()
  salvarEAtivar$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarEAtivarPrograma),
    switchMap((action: SalvarEAtivarPrograma) => this.programaService.alterarPrograma(action.programa).pipe(
      map((programa: Programa) => new OpenDialogAtivarPrograma(programa.id)),
      catchError(error => of(new SalvarEAtivarProgramaFail(error))))));

  @Effect(noDispatch)
  salvarEAtivarFail$ = this.actions$.pipe(
    ofType(ProgramasActionTypes.SalvarEAtivarProgramaFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar o programa antes de ativá-lo')));
}
