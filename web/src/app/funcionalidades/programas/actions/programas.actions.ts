import {Action} from '@ngrx/store';
import {Programa} from '../models/programas.model';
import {ArquivoPrograma} from '../models/arquivo-programa.model';
import {Indicador} from '@app/funcionalidades/projeto/models/indicador.model';
import {ModalRef} from '@sicoob/ui';

export enum ProgramasActionTypes {
  LoadPrograma = '[LoadPrograma]',
  LoadProgramaSuccess = '[LoadProgramaSuccess]',
  LoadProgramaFail = '[LoadProgramaFail]',
  ListaProgramas = '[Programas] Lista Programas',
  ListaProgramasSuccess = 'Success: [Programas] Lista Programas',
  ListaProgramasFail = 'Fail: [Programas] Lista Programas',
  SalvarPrograma = '[Programas] Salva Programa',
  SalvarProgramaSuccess = '[Programas] Salva Programa Success',
  SalvarProgramaFail = '[Programas] Salva Programa Fail',
  SalvarArquivoPrograma = '[Programas] Salva ArquivoPrograma',
  SalvarArquivoProgramaSuccess = '[Programas] Salva ArquivoPrograma Success',
  SalvarArquivosPrograma = '[Programas] SalvarArquivosPrograma',
  SalvarArquivosProgramaSuccess = '[Programas] SalvarArquivosProgramaSuccess',
  ListarIndicadoresFluxo = '[Programas] Lista Indicadores por Fluxo',
  ListarIndicadoresFluxoSuccess = '[Programas] Sucesso: Lista Indicadores por Fluxo',
  ListarIndicadoresFluxoFail = '[Programas] Falha: Lista Indicadores por Fluxo',
  SalvarIndicadorPrograma = '[Programas] Salva Indicador de um Programa',
  SalvarIndicadorProgramaSuccess = '[Programas] Sucesso: Salva Indicador de um Programa',
  SalvarIndicadorProgramaFail = '[Programas] Falha: Salva Indicador de um Programa Fail',
  DetalharPrograma = '[Programas] Detalha Programa',
  DetalharProgramaSuccess = '[Programas] Sucesso: Detalha Programa',
  DetalharProgramaFail = '[Programas] Falha: Detalha Programa',
  OpenDialogAtivarPrograma = '[OpenDialogAtivarPrograma]',
  OpenDialogAtivarProgramaSuccess = '[OpenDialogAtivarProgramaSuccess]',
  OpenDialogAtivarProgramaFail = '[OpenDialogAtivarProgramaFail]',
  AtivarPrograma = '[AtivarPrograma]',
  AtivarProgramaSuccess = '[AtivarProgramaSuccess]',
  AtivarProgramaFail = '[AtivarProgramaFail]',
  NoActionPrograma = '[NoActionPrograma]',
  ClearPrograma = '[ClearPrograma]',
  SalvarImagemPrograma = '[SalvarImagemPrograma]',
  SalvarImagemProgramaSuccess = '[SalvarImagemProgramaSuccess]',
  SalvarImagemProgramaFail = '[SalvarImagemProgramaFail]',
  SaveAndNavigateFluxoProcesso = '[SaveAndNavigateFluxoProcesso]',
  SaveAndNavigateFluxoProcessoSuccess = '[SaveAndNavigateFluxoProcessoSuccess]',
  SaveAndNavigateFluxoProcessoFail = '[SaveAndNavigateFluxoProcessoFail]',
  SalvarEAtivarPrograma = '[SalvarEAtivarPrograma]',
  SalvarEAtivarProgramaSuccess = '[SalvarEAtivarProgramaSuccess]',
  SalvarEAtivarProgramaFail = '[SalvarEAtivarProgramaFail]',
}

export class LoadPrograma implements Action {
  readonly type = ProgramasActionTypes.LoadPrograma;
  constructor(public idPrograma: string, public detalhado: boolean = false) { }
}

export class LoadProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.LoadProgramaSuccess;
  constructor(public programa: Programa) { }
}

export class LoadProgramaFail implements Action {
  readonly type = ProgramasActionTypes.LoadProgramaFail;
  constructor(public payload: any) { }
}

export class ListaProgramas implements Action {
  readonly type = ProgramasActionTypes.ListaProgramas;
  constructor(public tamanhoPagina: number, public numeroPagina: number) { }
}

export class ListaProgramasSuccess implements Action {
  readonly type = ProgramasActionTypes.ListaProgramasSuccess;
  constructor(public programas: Programa[]) { }
}

export class ListaProgramasFail implements Action {
  readonly type = ProgramasActionTypes.ListaProgramasFail;
  constructor() { }
}

export class SalvarPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarPrograma;
  constructor(public imagem: File, public programa: Programa) { }
}

export class SalvarProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarProgramaSuccess;
  constructor(public programa: Programa) { }
}

export class SalvarProgramaFail implements Action {
  readonly type = ProgramasActionTypes.SalvarProgramaFail;
  constructor() { }
}

export class SalvarArquivoPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarArquivoPrograma;
  constructor(public arquivoPrograma: ArquivoPrograma) { }
}

export class SalvarArquivoProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarArquivoProgramaSuccess;
  constructor() { }
}

export class SalvarArquivosPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarArquivosPrograma;
  constructor(public arquivosPrograma: ArquivoPrograma[]) { }
}
export class SalvarArquivosProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarArquivosProgramaSuccess;
  constructor() { }
}

export class ListarIndicadoresFluxo implements Action {
  readonly type = ProgramasActionTypes.ListarIndicadoresFluxo;
  constructor(public idFluxo: string) { }
}

export class ListarIndicadoresFluxoSuccess implements Action {
  readonly type = ProgramasActionTypes.ListarIndicadoresFluxoSuccess;
  constructor(public indicadores: any) { }
}

export class ListarIndicadoresFluxoFail implements Action {
  readonly type = ProgramasActionTypes.ListarIndicadoresFluxoFail;
  constructor() { }
}

export class SalvarIndicadorPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarIndicadorPrograma;
  constructor(public indicadores: Indicador[]) { }
}

export class SalvarIndicadorProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarIndicadorProgramaSuccess;
  constructor() { }
}

export class SalvarIndicadorProgramaFail implements Action {
  readonly type = ProgramasActionTypes.SalvarIndicadorProgramaFail;
  constructor() { }
}

export class DetalharPrograma implements Action {
  readonly type = ProgramasActionTypes.DetalharPrograma;
  constructor(public id: string) { }
}

export class DetalharProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.DetalharProgramaSuccess;
  constructor(public programa: any) { }
}

export class DetalharProgramaFail implements Action {
  readonly type = ProgramasActionTypes.DetalharProgramaFail;
  constructor() { }
}

export class OpenDialogAtivarPrograma implements Action {
  readonly type = ProgramasActionTypes.OpenDialogAtivarPrograma;
  constructor(public idPrograma: string) { }
}

export class OpenDialogAtivarProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.OpenDialogAtivarProgramaSuccess;
  constructor(public modalRef: ModalRef, public idPrograma: string) { }
}

export class OpenDialogAtivarProgramaFail implements Action {
  readonly type = ProgramasActionTypes.OpenDialogAtivarProgramaFail;
  constructor(public erro: any) { }
}

export class AtivarPrograma implements Action {
  readonly type = ProgramasActionTypes.AtivarPrograma;
  constructor(public idPrograma: string) { }
}
export class AtivarProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.AtivarProgramaSuccess;
  constructor(public programa: Programa) { }
}
export class AtivarProgramaFail implements Action {
  readonly type = ProgramasActionTypes.AtivarProgramaFail;
  constructor(public error: any) { }
}

export class NoActionPrograma implements Action {
  readonly type = ProgramasActionTypes.NoActionPrograma;
  constructor() { }
}
export class ClearPrograma implements Action {
  readonly type = ProgramasActionTypes.ClearPrograma;
  constructor() { }
}

export class SalvarImagemPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarImagemPrograma;
  constructor(public imagem: File, public idPrograma: string) { }
}

export class SalvarImagemProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarImagemProgramaSuccess;
  constructor() { }
}

export class SalvarImagemProgramaFail implements Action {
  readonly type = ProgramasActionTypes.SalvarImagemProgramaFail;
  constructor(public error: any) { }
}

export class SaveAndNavigateFluxoProcesso implements Action {
  readonly type = ProgramasActionTypes.SaveAndNavigateFluxoProcesso;
  constructor(public programa: Programa) { }
}

export class SaveAndNavigateFluxoProcessoSuccess implements Action {
  readonly type = ProgramasActionTypes.SaveAndNavigateFluxoProcessoSuccess;
  constructor(public programa: Programa) { }
}

export class SaveAndNavigateFluxoProcessoFail implements Action {
  readonly type = ProgramasActionTypes.SaveAndNavigateFluxoProcessoFail;
  constructor(public error: any) { }
}

export class SalvarEAtivarPrograma implements Action {
  readonly type = ProgramasActionTypes.SalvarEAtivarPrograma;
  constructor(public programa: Programa) { }
}

export class SalvarEAtivarProgramaSuccess implements Action {
  readonly type = ProgramasActionTypes.SalvarEAtivarProgramaSuccess;
  constructor() { }
}

export class SalvarEAtivarProgramaFail implements Action {
  readonly type = ProgramasActionTypes.SalvarEAtivarProgramaFail;
  constructor(public error: any) { }
}

export type ProgramasActions =
  ListaProgramas
  | ListaProgramasSuccess
  | ListaProgramasFail
  | SalvarPrograma
  | SalvarProgramaSuccess
  | SalvarArquivoPrograma
  | SalvarProgramaFail
  | SalvarArquivoProgramaSuccess
  | ListarIndicadoresFluxo
  | ListarIndicadoresFluxoSuccess
  | ListarIndicadoresFluxoFail
  | SalvarArquivosPrograma
  | SalvarArquivosProgramaSuccess
  | LoadPrograma
  | LoadProgramaSuccess
  | LoadProgramaFail
  | SalvarIndicadorPrograma
  | SalvarIndicadorProgramaSuccess
  | SalvarIndicadorProgramaFail
  | DetalharPrograma
  | DetalharProgramaSuccess
  | DetalharProgramaFail
  | OpenDialogAtivarPrograma
  | OpenDialogAtivarProgramaSuccess
  | OpenDialogAtivarProgramaFail
  | AtivarPrograma
  | AtivarProgramaSuccess
  | AtivarProgramaFail
  | NoActionPrograma
  | ClearPrograma
  | SalvarImagemPrograma
  | SalvarImagemProgramaSuccess
  | SalvarImagemProgramaFail
  | SaveAndNavigateFluxoProcesso
  | SaveAndNavigateFluxoProcessoSuccess
  | SaveAndNavigateFluxoProcessoFail
  | SalvarEAtivarPrograma
  | SalvarEAtivarProgramaSuccess
  | SalvarEAtivarProgramaFail;
