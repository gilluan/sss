import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProgramasRoutingModule} from './programas-routing.module';
import {StoreModule} from '@ngrx/store';
import * as fromProgramas from './reducers/programas.reducer';
import * as fromPortfolio from '../portfolio/reducers/portfolio.reducer';
import * as fromFluxoProcesso from '@app/funcionalidades/fluxo-processo/reducers/fluxo-processo.reducer';
import {EffectsModule} from '@ngrx/effects';
import {ProgramasEffects} from './effects/programas.effects';
import {ContainerProgramasComponent} from './containers/container-programas/container-programas.component';
import {NovoProgramaComponent} from './components/novo-programa/novo-programa.component';
import {ProgramasComponent} from './components/programas/programas.component';
import {CardModule, FormModule, TabsModule, UiModule} from '@sicoob/ui';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ScSelectionListModule} from 'src/app/shared/components/sc-selection-list/sc-selection-list.module';
import {SharedModule} from '@app/shared/shared.module';
import {BsDatepickerModule, PopoverModule} from 'ngx-bootstrap';
import {CadastroProgramasTabsComponent} from './components/cadastro-programas-tabs/cadastro-programas-tabs.component';
import {InfosGeraisComponent} from './components/infos-gerais/infos-gerais.component';
import {ProdutoComponent} from './components/produto/produto.component';
import {ObjetivosComponent} from './components/objetivos/objetivos.component';
import {PortfolioEffects} from '../portfolio/effects/portfolio.effects';
import {StepArquivosComponent} from './components/programas/step-arquivos/step-arquivos.component';
import {StepInfosComponent} from './components/programas/step-infos/step-infos.component';
import {StepTarefasComponent} from './components/programas/step-tarefas/step-tarefas.component';
import {StepAtivarComponent} from './components/programas/step-ativar/step-ativar.component';
import {ScAccordionItemModule} from '@shared/components/sc-accordion-item/sc-accordion-item.module';
import {ProgramaComponent} from './components/programa/programa.component';
import {AvatarModule} from '@app/shared/components/sins-avatar/sc-avatar.module';
import {NgxPermissionsModule} from 'ngx-permissions';

@NgModule({
  declarations: [
    ContainerProgramasComponent,
    ProgramasComponent,
    NovoProgramaComponent,
    CadastroProgramasTabsComponent,
    InfosGeraisComponent,
    ProdutoComponent,
    ObjetivosComponent,
    StepArquivosComponent,
    StepInfosComponent,
    StepTarefasComponent,
    StepAtivarComponent,
    ProgramaComponent
  ],
  imports: [
    NgxPermissionsModule.forChild(),
    FormsModule, ReactiveFormsModule,
    FormModule,
    CommonModule,
    CardModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    ScSelectionListModule,
    UiModule,
    TabsModule,
    ProgramasRoutingModule,
    StoreModule.forFeature('programaSelector', fromProgramas.reducer),
    StoreModule.forFeature('portfolioSelector', fromPortfolio.reducer),
    StoreModule.forFeature('fluxoProcessoState', fromFluxoProcesso.reducer),
    EffectsModule.forFeature([ProgramasEffects, PortfolioEffects]),
    AvatarModule, ScAccordionItemModule, PopoverModule
  ]
})
export class ProgramasModule { }
