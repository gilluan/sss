export class Programa {
  id?: string;
  totalProjetos?: number;
  nome?: string;
  portfolio?: string | any;
  vigencia?: number;
  descricao?: string;
  justificativa?: string;
  fluxoprocesso?: string;
  partesInteressadas?: string;
  produto?: string;
  beneficios?: string;
  premissas?: string[];
  riscos?: string[];
  restricoes?: string[];
  requisitos?: string[];
  objetivoGeral?: string;
  objetivoEspecifico?: string;
  objetivoSmart?: string;
  selecionado?: boolean;
  imagem?: any;
  ativo?: boolean;
  possuiProjetoVinculado?: boolean;
}
