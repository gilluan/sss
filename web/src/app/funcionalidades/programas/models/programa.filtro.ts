import {PageModelVm} from '@shared/models/page.model';

export class ProgramaFiltro {
  constructor(
    public paginacao: PageModelVm = new PageModelVm(0, 10, 0),
    public ativo: boolean = true,
    public vigencia: string = new Date().getFullYear().toString(),
    public nome?: string,
    public portifolioId?: string,
  ) {}
}
