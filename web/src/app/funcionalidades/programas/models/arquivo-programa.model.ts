import { BaseDto } from "@app/shared/models/base-dto.model";

export class ArquivoPrograma extends BaseDto {
    idGed: string;
    nome: string;
    tamanho: number;
    nomeDiretorio: string;
    programaId: string;
}
