
export declare type FinanceiroTarefa = {
    id: string,
    rubrica: string,
    valor: number
};
