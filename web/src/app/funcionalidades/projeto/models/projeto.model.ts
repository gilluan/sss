import { BaseDto } from "@app/shared/models/base-dto.model";
import { CronogramaFisico } from "./cronogramas/cronograma-fisico.model";
import { Meta } from "./meta.model";

export class Projeto extends BaseDto {
  programa: any;
  nome: string;
  dataInicio: Date;
  dataFim: Date;
  responsavel: string;
  criador: string;
  justificativa: string;
  escopo: string;
  naoEscopo: string;
  concluirCadastro: boolean;
  cronogramaFisico: CronogramaFisico;
  metas: Meta[];
  numeroCooperativa: string;
  orcamento: number;
  codigoProjeto: number;
  situacao: string;
  //cronogramaFinanceiro: CronogramaFinanceiro;
}
