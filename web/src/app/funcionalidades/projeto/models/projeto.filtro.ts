import { PageModelVm } from '@shared/models/page.model';

declare type ProjetoEstado = 'todos' | 'concluido' | 'rascunho';

export class ProjetoFiltro {

  constructor(
    public paginacao: PageModelVm = new PageModelVm(0, 8, 0),
    public estado: ProjetoEstado = 'todos',
    public numeroCooperativa?: number[],
    public nome?: string,
    public programa?: any,
    public usuarioInstituto?: any,
    ) { }
}
