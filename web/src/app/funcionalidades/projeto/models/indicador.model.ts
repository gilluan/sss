import { BaseDto } from '@app/shared/models/base-dto.model';

export class Indicador extends BaseDto {
  nome: string;
  programaId: string;
  descricao: string;
  idPasso: string;
}
