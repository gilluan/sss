import { BaseDto } from "@app/shared/models/base-dto.model";

export class Meta extends BaseDto {
    indicador: string;
    valor: number;
}
