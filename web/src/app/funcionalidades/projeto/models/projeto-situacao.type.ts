export enum ProjetoSituacaoType {
  todos = 'todos',
  concluido = 'concluido',
  rascunho = 'rascunho',
}

export namespace ProjetoSituacaoType {
  export function values(): ProjetoSituacaoType[] {
    return [
      ProjetoSituacaoType.todos,
      ProjetoSituacaoType.concluido,
      ProjetoSituacaoType.rascunho,
    ]
  }

  export function label(situacao: ProjetoSituacaoType): string {
    switch (situacao) {
      case ProjetoSituacaoType.todos:
        return 'Todos';
      case ProjetoSituacaoType.concluido:
        return 'Concluídos';
      case ProjetoSituacaoType.rascunho:
        return 'Rascunhos';
    }
  }

  export function classes(situacao: ProjetoSituacaoType): string {
    switch (situacao) {
      case ProjetoSituacaoType.todos:
        return 'ss-label-default';
      case ProjetoSituacaoType.concluido:
        return 'ss-label-success';
      case ProjetoSituacaoType.rascunho:
        return 'ss-label-primary';
      default:
        return 'ss-label-default'
    }
  }

  export function classesBadge(situacao: ProjetoSituacaoType): string {
    switch (situacao) {
      case ProjetoSituacaoType.todos:
        return 'default';
      case ProjetoSituacaoType.concluido:
        return 'success';
      case ProjetoSituacaoType.rascunho:
        return 'primary';
      default:
        return 'default'
    }
  }




}
