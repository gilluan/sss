export class OrcamentoInvestimentoProjeto {
  constructor(
    public id: string = null,
    public nome: string = null,
    public orcamento: number = 0.0,
    public investimento: number = 0.0) { }
}
