import { BaseDto } from "@app/shared/models/base-dto.model";

export class InformacoesProjeto extends BaseDto {
  programa: string;
  nome: string;
  dataInicio: Date;
  dataFim: Date;
  responsavel: string;
  justificativa: string;
  orcamento: number;
  codigoProjeto: number;
  criador: string;
}
