export class Investimento {
  _id: string;
  orcamento: number;
  investido: number;
  percentual: number;
  restam: number;
}
