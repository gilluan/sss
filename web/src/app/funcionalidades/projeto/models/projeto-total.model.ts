export class ProjetoTotal {
  todos: number;
  rascunho: number;
  finalizado: number;

  constructor(todos: number = 0, rascunho: number = 0, finalizado: number = 0) {
    this.todos = todos;
    this.rascunho = rascunho;
    this.finalizado = finalizado;
  }
}
