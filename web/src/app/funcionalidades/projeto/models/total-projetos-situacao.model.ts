export class TotalProjetosSituacao {
  constructor(public naoIniciados: number = 0, public iniciados: number = 0, public concluidos: number = 0) { }
}
