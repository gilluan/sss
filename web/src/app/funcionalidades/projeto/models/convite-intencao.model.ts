import { BaseDto } from "@app/shared/models/base-dto.model";

export class ConviteIntencao extends BaseDto {
  texto: string;
  projeto: any;
  convidador: any;
  enviado: boolean;
}
