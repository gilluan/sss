import { Marco } from '@app/funcionalidades/fluxo-processo/models/marco.model';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { OrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import { TotaisOrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import { TotalProjetosSituacao } from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';
import { TotalSituacaoVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProjetoActions, ProjetoActionTypes, UpdateProjeto } from '../actions/projeto.actions';
import { ConviteIntencao } from '../models/convite-intencao.model';
import { CronogramaFinanceiro } from '../models/cronogramas/cronograma-financeiro.model';
import { CronogramaFisico } from '../models/cronogramas/cronograma-fisico.model';
import { Indicador } from '../models/indicador.model';
import { Investimento } from '../models/investimento.model';
import { Meta } from '../models/meta.model';
import { ProjetoSituacaoType } from '../models/projeto-situacao.type';
import { ProjetoTotal } from '../models/projeto-total.model';
import { ProjetoFiltro } from '../models/projeto.filtro';
import { Projeto } from '../models/projeto.model';
import { UsuariosProjeto } from '../models/usuarios-projetos.model';
import { EscoposProjeto } from '../models/wizard/escopos-projeto.model';
import { InformacoesProjeto } from '../models/wizard/informacoes-projeto.model';

export interface State extends EntityState<Projeto> {
  projetos: Projeto[];
  totalProjetos: ProjetoTotal;
  totalSituacao: TotalSituacaoVm[];
  projetoCadastro: Projeto;
  projetoEditar: Projeto;
  projetoAtual: Projeto;
  filtroAtual: ProjetoFiltro;
  informacoes: InformacoesProjeto;
  escopos: EscoposProjeto;
  metas: Meta[];
  cronogramaFinanceiro: CronogramaFinanceiro;
  cronogramaFisico: CronogramaFisico;
  programaAtual: string;
  cadastroSucesso: boolean;
  editarSucesso: boolean;
  indicadores: Indicador[];
  usuarios: UsuariosProjeto;
  conviteIntencao: ConviteIntencao;
  totaisInvestimento: TotaisOrcamentoInvestimentoProjeto;
  investimentosProjeto: OrcamentoInvestimentoProjeto[];
  vigencias: string[];
  totalProjetosSituacao: TotalProjetosSituacao;
  marcoAtual: Marco;
  passos: PassoFluxo[];
  passosConcluidos: PassoFluxo[];
  passosAguardandoAprovacao: PassoFluxo[];
  isResponsavel: boolean;
  isLoadingProjetos: boolean;
  investimentoAtual: Investimento
}

export const adapter: EntityAdapter<Projeto> = createEntityAdapter<Projeto>();

export const initialState: State = adapter.getInitialState({
  isLoadingProjetos: false,
  isResponsavel: false,
  marcoAtual: null,
  passos: [],
  passosConcluidos: [],
  passosAguardandoAprovacao: [],
  indicadores: [],
  projetos: [],
  projetoAtual: null,
  investimentoAtual: null,
  totalSituacao:
    [
      new TotalSituacaoVm(ProjetoSituacaoType.todos, 0),
      new TotalSituacaoVm(ProjetoSituacaoType.concluido, 0),
      new TotalSituacaoVm(ProjetoSituacaoType.rascunho, 0),
    ],
  totalProjetos: new ProjetoTotal(0, 0, 0),
  cadastroSucesso: false,
  editarSucesso: false,
  projetoCadastro: null,
  projetoEditar: null,
  informacoes: null,
  escopos: null,
  metas: null,
  cronogramaFinanceiro: null,
  cronogramaFisico: null,
  filtroAtual: new ProjetoFiltro(),
  programaAtual: null,
  usuarios: null,
  conviteIntencao: null,
  totaisInvestimento: new TotaisOrcamentoInvestimentoProjeto(),
  investimentosProjeto: [],
  vigencias: [],
  totalProjetosSituacao: new TotalProjetosSituacao(),
});

export function reducer(
  state = initialState,
  action: ProjetoActions
): State {
  switch (action.type) {

    case ProjetoActionTypes.IsResponsavelProjeto: {
      return { ...state, isResponsavel: action.isResponsavel }
    }

    case ProjetoActionTypes.SelectedMarco: {
      return { ...state, marcoAtual: action.marco }
    }
    case ProjetoActionTypes.LoadPassosFluxoProjetoSuccess: {
      return { ...state, passos: action.passosFluxos }
    }
    case ProjetoActionTypes.LoadPassosFluxoProjetoClean: {
      return { ...state, passos: [] }
    }
    case ProjetoActionTypes.LoadConviteIntencaoProjetoSuccess: {
      return { ...state, conviteIntencao: action.conviteIntencao };
    }

    case ProjetoActionTypes.LoadUsuariosProjetosSuccess: {
      return { ...state, usuarios: action.usuarios };
    }

    case ProjetoActionTypes.ClearProjetos: {
      // Se a action é editar mantém no store algumas informações, senão zera o store
      return action.editar ? { ...initialState, informacoes: state.informacoes, escopos: state.escopos, metas: state.metas } : { ...initialState };
    }
    case ProjetoActionTypes.LoadProjetos: {
      return { ...state, isLoadingProjetos: true }
    }
    case ProjetoActionTypes.LoadProjetosFail: {
      return { ...state, isLoadingProjetos: false }
    }
    case ProjetoActionTypes.LoadProjetosSuccess: {
      state.isLoadingProjetos = false;
      state.projetos = action.pagedData && action.pagedData.data.length > 0 ? state.projetos.concat(action.pagedData.data) : [];
      const filtro: ProjetoFiltro = { ...state.filtroAtual, paginacao: action.pagedData.page };
      return {
        ...state, filtroAtual: filtro
      };
    }
    case ProjetoActionTypes.DeleteProjetoSuccess: {
      state.projetos = state.projetos.filter(p => p.id !== action.projeto.id);
      state.filtroAtual = { ...state.filtroAtual, paginacao: { ...state.filtroAtual.paginacao, total: (state.filtroAtual.paginacao.total - 1) } };
      return {
        ...state
      };
    }
    case ProjetoActionTypes.TotalSituacaoProjetosSuccess: {
      return {
        ...state, totalSituacao: action.totalSituacao
      };
    }
    case ProjetoActionTypes.SelectedPrograma: {
      return {
        ...state, programaAtual: action.programa
      };
    }

    case ProjetoActionTypes.AddInformacoes: {
      return {
        ...state, informacoes: action.informacoesProjeto
      };
    }
    case ProjetoActionTypes.AddEscopos: {
      return {
        ...state, escopos: action.escopos
      };
    }
    case ProjetoActionTypes.AddCronogramaFinanceiro: {
      return {
        ...state, cronogramaFinanceiro: action.cronogramaFinanceiro
      };
    }
    case ProjetoActionTypes.ListarIndicadoresSuccess: {
      return {
        ...state, indicadores: action.indicadores
      };
    }
    case ProjetoActionTypes.AddCronogramaFisico: {
      const informacoes = state.informacoes;
      if (informacoes) { informacoes.dataFim = action.dataFimProjeto; }
      return {
        ...state, cronogramaFisico: action.cronogramaFisico, informacoes
      };
    }
    case ProjetoActionTypes.UpdateProjeto: {
      const projetoEditar = montarProjeto(state, action);
      return {
        ...state, projetoEditar
      };
    }
    case ProjetoActionTypes.UpdateProjetoSuccess: {
      return {
        ...state, projetoAtual: action.projeto, editarSucesso: true
      };
    }
    case ProjetoActionTypes.AddProjeto: {
      const projetoCadastro = montarProjeto(state, action);
      return {
        ...state, projetoCadastro
      };
    }
    case ProjetoActionTypes.AddProjetoSuccess: {
      return {
        ...state, projetoAtual: action.projeto, cadastroSucesso: true
      };
    }
    case ProjetoActionTypes.AddMetas: {
      return {
        ...state, metas: action.metas
      };
    }
    case ProjetoActionTypes.TotalProjetosSuccess: {
      return {
        ...state, totalProjetos: action.total
      };
    }
    case ProjetoActionTypes.LoadProjetoByIdSuccess: {
      return {
        ...state, projetoAtual: action.projeto
      };
    }

    case ProjetoActionTypes.LoadInvestimentoPorIdSuccess: {
      return {
        ...state, investimentoAtual: action.investimento
      };
    }
    case ProjetoActionTypes.CleanProjetoCadastro: {
      return {
        ...state, projetoCadastro: null, informacoes: null, escopos: null, metas: null, cronogramaFinanceiro: null, cronogramaFisico: null
      };
    }

    case ProjetoActionTypes.CleanProjetoAtual: {
      return {
        ...state, projetoAtual: null
      };
    }

    case ProjetoActionTypes.CleanUsuariosProjetos: {
      return {
        ...state, usuarios: null, conviteIntencao: null
      };
    }

    case ProjetoActionTypes.LoadTotaisInvestimentoSuccess: {
      return { ...state, totaisInvestimento: action.totais };
    }

    case ProjetoActionTypes.LoadInvestimentosProjetoSuccess: {
      return { ...state, investimentosProjeto: action.investimentosProjeto };
    }

    case ProjetoActionTypes.LoadVigenciasDisponiveisSuccess: {
      return { ...state, vigencias: action.vigencias };
    }

    case ProjetoActionTypes.LoadTotalProjetosSituacaoSuccess: {
      return { ...state, totalProjetosSituacao: action.totalProjetosSituacao };
    }

    case ProjetoActionTypes.LoadPassosConcluidosSuccess: {
      return {
        ...state, passosConcluidos: action.passosFluxos
      }
    }
    case ProjetoActionTypes.LoadPassosConcluidosClean: {
      return {
        ...state, passosConcluidos: []
      }
    }

    case ProjetoActionTypes.LoadPassosAguardandoAprovacaoSuccess: {
      return {
        ...state, passosAguardandoAprovacao: action.passosFluxos
      }
    }
    case ProjetoActionTypes.LoadPassosAguardandoAprovacaoClean: {
      return {
        ...state, passosAguardandoAprovacao: []
      }
    }

    default: {
      return state;
    }
  }
}

export function montarProjeto(state: any, action): Projeto {
  const projeto = new Projeto();
  if (action instanceof UpdateProjeto) {
    projeto.codigoProjeto = state.informacoes ? state.informacoes.codigoProjeto : null;
    projeto.id = state.informacoes ? state.informacoes.id : null;
    projeto.dataCriacao = state.informacoes ? state.informacoes.dataCriacao : null;
    projeto.dataModificacao = state.informacoes ? state.informacoes.dataModificacao : null;
  }
  projeto.criador = state.informacoes ? state.informacoes.criador : null;
  projeto.programa = state.informacoes ? state.informacoes.programa : null;
  projeto.nome = state.informacoes ? state.informacoes.nome : null;
  projeto.dataInicio = state.informacoes ? state.informacoes.dataInicio : null;
  projeto.responsavel = state.informacoes ? state.informacoes.responsavel : null;
  projeto.justificativa = state.informacoes ? state.informacoes.justificativa : null;
  projeto.orcamento = state.informacoes ? state.informacoes.orcamento : null;
  projeto.escopo = state.escopos ? state.escopos.escopo : null;
  projeto.naoEscopo = state.escopos ? state.escopos.naoEscopo : null;
  projeto.concluirCadastro = !action.rascunho;
  //projeto.cronogramaFinanceiro = state.cronogramaFinanceiro;
  projeto.cronogramaFisico = state.cronogramaFisico;
  projeto.dataFim = state.cronogramaFisico && state.cronogramaFisico.tarefas && state.cronogramaFisico.tarefas.length > 0 ? state.cronogramaFisico.tarefas[0].dataFim : null;
  projeto.numeroCooperativa = action.numeroCooperativa;
  projeto.metas = state.metas;
  return projeto;
}

export const getProjetoState = createFeatureSelector<State>('projeto');

export const getInvestimentoAtual = createSelector(
  getProjetoState,
  state => {
    return state.investimentoAtual
  }
)

export const getPassosAguardandoAprovacao = createSelector(
  getProjetoState,
  state => {
    return state.passosAguardandoAprovacao
  }
)

export const getPassosConcluidos = createSelector(
  getProjetoState,
  state => {
    return state.passosConcluidos
  }
)

export const isLoadingProjetos = createSelector(
  getProjetoState,
  state => {
    return state.isLoadingProjetos
  }
)

export const isResponsavel = createSelector(
  getProjetoState,
  state => {
    return state.isResponsavel;
  });

export const getPassos = createSelector(
  getProjetoState,
  state => {
    return state.passos;
  });

export const getMarcoAtual = createSelector(
  getProjetoState,
  state => {
    return state.marcoAtual;
  });

export const getConviteIntencao = createSelector(
  getProjetoState,
  state => {
    return state.conviteIntencao;
  });

export const getUsuarios = createSelector(
  getProjetoState,
  state => {
    return state.usuarios;
  });

export const getProjetos = createSelector(
  getProjetoState,
  state => {
    return state.projetos;
  });

export const getProjetoAtual = createSelector(
  getProjetoState,
  state => {
    return state.projetoAtual;
  });

export const getProjetoCadastro = createSelector(
  getProjetoState,
  state => {
    return state.projetoCadastro;
  });
export const getProjetoEditar = createSelector(
  getProjetoState,
  state => {
    return state.projetoEditar;
  });
export const getFiltroAtual = createSelector(
  getProjetoState,
  state => {
    return state.filtroAtual;
  });
export const getInformacoes = createSelector(
  getProjetoState,
  state => {
    return state.informacoes;
  });

export const getEscopos = createSelector(
  getProjetoState,
  state => {
    return state.escopos;
  });

export const getMetas = createSelector(
  getProjetoState,
  state => {
    return state.metas;
  });

export const getCronogramaFisico = createSelector(
  getProjetoState,
  state => {
    return state.cronogramaFisico;
  });

export const getCronogramaFinanceiro = createSelector(
  getProjetoState,
  state => {
    return state.cronogramaFinanceiro;
  });

export const getTotalProjetos = createSelector(
  getProjetoState,
  state => {
    return state.totalProjetos.todos;
  });

export const getTotalEstado = createSelector(
  getProjetoState,
  state => {
    return state.totalProjetos;
  });

export const getTotalSituacao = createSelector(
  getProjetoState,
  state => {
    return state.totalSituacao;
  });

export const getCadastroSucesso = createSelector(
  getProjetoState,
  state => {
    return state.cadastroSucesso;
  });
export const getEditarSucesso = createSelector(
  getProjetoState,
  state => {
    return state.editarSucesso;
  });



export const getIndicadores = createSelector(
  getProjetoState,
  state => {
    return state.indicadores;
  });

export const selectTotaisInvestimento = createSelector(
  getProjetoState,
  (state: State) => state.totaisInvestimento);

export const selectInvestimentosProjeto = createSelector(
  getProjetoState,
  (state: State) => state.investimentosProjeto);

export const selectVigencias = createSelector(
  getProjetoState,
  (state: State) => state.vigencias);

export const selectTotalProjetosSituacao = createSelector(
  getProjetoState,
  (state: State) => state.totalProjetosSituacao);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
