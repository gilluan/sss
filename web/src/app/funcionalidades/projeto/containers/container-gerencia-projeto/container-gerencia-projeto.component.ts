import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { ProgramasService } from '@app/funcionalidades/programas/programas.service';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { CleanProjetoAtual, LoadProjetoById, LoadUsuariosProjetos, SelectedMarco, IsResponsavelProjeto } from '../../actions/projeto.actions';
import { Projeto } from '../../models/projeto.model';
import { UsuariosProjeto } from '../../models/usuarios-projetos.model';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { ScAvatar } from '@app/shared/components/sins-avatar/sc-avatar.model';
import { PerfilType } from '@app/shared/types/perfil.type';
import * as fromApp from '@app/app.reduce'
import { DisplayVisibilityHeader } from '@app/app.actions';

@Component({
  selector: 'sc-container-gerencia-projeto',
  templateUrl: './container-gerencia-projeto.component.html',
  styleUrls: ['./container-gerencia-projeto.component.scss']
})
export class ContainerGerenciaProjetoComponent implements OnInit, OnDestroy {

  usuarioInstituto: any;
  subscription: Subscription = new Subscription();
  idProjeto: string;
  projeto$: Observable<Projeto>;
  programa$: Observable<Programa>;
  moreTitle: string = '';
  tabsModel: any[] = [];

  routerActive: string = '';
  avatars: ScAvatar[] = [];
  totalAvatar: number = 3;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public store$: Store<fromProjeto.State>,
    public appStore$: Store<fromApp.State>,
    private programaService: ProgramasService,
    private persistanceService: PersistanceService,
    public datePipe: DatePipe) { }

  ngOnInit() {
    this.usuarioInstituto = this.persistanceService.get('usuario_instituto');
    this.setTabs();
    this.route.paramMap.subscribe(params => {
      this.idProjeto = params.get("id");
      this.store$.dispatch(new LoadProjetoById(this.idProjeto));
      this.store$.dispatch(new LoadUsuariosProjetos(this.idProjeto))

    });
    this.subscription.add(this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      if (event.url === `/projetos/${this.idProjeto}`) {
        this.routerActive = '';
      } else {
        const params = event.url.split('/')[3].split('?');
        this.routerActive = params[0];
        if (params.length === 1) {
          this.store$.dispatch(new SelectedMarco(null))
        }
      }
    }));
    this.projeto$ = this.store$.pipe(select(fromProjeto.getProjetoAtual));
    this.subscription
      .add(this.projeto$.subscribe((projeto: Projeto) => {
        if (projeto) {
          this.programa$ = this.programaService.detalharPrograma(projeto.programa)
          this.store$.dispatch(new IsResponsavelProjeto(projeto.responsavel ? this.usuarioInstituto.id === (projeto.responsavel as any)._id : false))
        }
      }))
      .add(this.store$.pipe(select(fromProjeto.getUsuarios)).subscribe((usuarios: UsuariosProjeto) => this.populateAvatar(usuarios)))
  }


  setTabs() {
    this.tabsModel.push({ title: "Entregas", route: "" })
    this.tabsModel.push({ title: "Tarefas", route: "tarefas" })
    this.tabsModel.push({ title: "Respostas", route: "respostas" })
    this.tabsModel.push({ title: "Informações", route: "info" })
    this.tabsModel.push({ title: "Time", route: "time" });
    //this.tabsModel.push({ title: "Arquivos", route: "arquivos" });
    if (this.usuarioInstituto.perfil === PerfilType.voluntario) {
      this.tabsModel = this.tabsModel.filter(value => value.title === 'Tarefas' || value.title === 'Informações' || value.title === 'Time');
    }
  }

  private populateAvatar(usuarios: UsuariosProjeto) {
    if (usuarios) {
      this.avatars = [];
      let total = 0;
      if (usuarios.voluntarios) {
        usuarios.voluntarios.forEach(v => this.avatars.push({ id: v._id, image: '', name: v.nome }));
        total = usuarios.voluntarios.length;
      }
      if (usuarios.pdes) {
        for (let i = 0; i < usuarios.pdes.length; i++) {
          const pde = usuarios.pdes[i];
          if (this.avatars.length < this.totalAvatar) { this.avatars.push({ id: pde._id, image: '', name: pde.nome }) } else break;
        }
        total = total + usuarios.pdes.length;
      }
      if (usuarios.paes) {
        for (let i = 0; i < usuarios.paes.length; i++) {
          const pae = usuarios.paes[i];
          if (this.avatars.length < this.totalAvatar) { this.avatars.push({ id: pae._id, image: '', name: pae.nome }) } else break;
        }
        total = total + usuarios.paes.length;
      }
      if (usuarios.ppes) {
        for (let i = 0; i < usuarios.ppes.length; i++) {
          const ppe = usuarios.ppes[i];
          if (this.avatars.length < this.totalAvatar) { this.avatars.push({ id: ppe._id, image: '', name: ppe.nome }) } else break;
        }
        total = total + usuarios.ppes.length;
      }
      const pessoasAmais = total - this.totalAvatar;
      if (pessoasAmais > 0) {
        this.moreTitle = `+ ${total - this.totalAvatar} pessoas`;
      }
    }
  }

  isActive(tab) {
    return this.routerActive === tab.route
  }

  subtitle(projeto: Projeto) {
    const dataInicio = this.datePipe.transform(projeto.dataInicio, "dd 'de' MMMM");
    return `Começa ${dataInicio}`;
  }

  navegateTo(rota: string) {
    this.routerActive = rota;
    this.router.navigate([`./${rota}`], { relativeTo: this.route });
  }

  navegarParaPerfil() {
    if (this.usuarioInstituto.perfil === PerfilType.voluntario) {
      this.router.navigate(['/perfil-usuario', this.usuarioInstituto.id, 'projetos']);
      this.appStore$.dispatch(new DisplayVisibilityHeader(true))
    } else {
      this.router.navigate(['/projetos']);
    }
  }

  selecionarAvatar(scAvatar: ScAvatar) {
    this.convidar();
  }

  convidar() {
    this.routerActive = 'time';
    this.router.navigate([`./time`], { relativeTo: this.route });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.store$.dispatch(new CleanProjetoAtual())
    this.store$.dispatch(new SelectedMarco(null))
  }

}
