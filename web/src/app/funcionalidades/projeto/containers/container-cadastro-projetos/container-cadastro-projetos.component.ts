import { TemplatePortal } from '@angular/cdk/portal';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { Action, MemoizedSelector, select, Store } from '@ngrx/store';
import { HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { AddEscopos, AddInformacoes, AddMetas, AddProjeto, CleanProjetoCadastro, ListarIndicadores, UpdateProjeto, CleanProjetoAtual } from '../../actions/projeto.actions';
import { AcaoProjeto, DialogoConcluirCadastroProjeto } from '../../components/dialogo-concluir-cadastro-projeto/dialogo-concluir-cadastro-projeto.component';
import { Indicador } from '../../models/indicador.model';
import { Meta } from '../../models/meta.model';
import { EscoposProjeto } from '../../models/wizard/escopos-projeto.model';
import { InformacoesProjeto } from '../../models/wizard/informacoes-projeto.model';
import { ProjetoService } from '../../projeto.service';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { BreadcrumbService } from '@app/shared/components/breadcrumb/breadcrumb.service';
import { Projeto } from '../../models/projeto.model';

@Component({
  selector: 'sc-container-cadastro-projetos',
  templateUrl: './container-cadastro-projetos.component.html',
  styleUrls: ['./container-cadastro-projetos.component.scss']
})
export class ContainerCadastroProjetosComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    this.store$.dispatch(new CleanProjetoCadastro())
    this.subscription.unsubscribe();
    this.modalRef.forEach(s => s ? s.close() : null);
    this.headerActionsService.remove();
  }
  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;
  modalRef: ModalRef[] = []
  subscription: Subscription = new Subscription();

  constructor(
    private store$: Store<fromProjeto.State>,
    private activedRoute: ActivatedRoute,
    private modalService: ModalService,
    private router: Router,
    private headerActionsService: HeaderActionsContainerService,
    private projetoService: ProjetoService,
    private persistenceService: PersistanceService,
    private breadcrumbService: BreadcrumbService,
  ) { }

  completedInfo: boolean = false;
  completedEscopo: boolean = false;
  completedIndicadores: boolean = false;
  completedFinanceiro: boolean = false;
  completedCronograma: boolean = false;
  programaId: string;
  programa$: Observable<Programa>;
  informacoes$: Observable<InformacoesProjeto>;
  indicadores$: Observable<Indicador[]>;
  metas$: Observable<Meta[]>;
  informacoesProjeto: InformacoesProjeto;
  escoposProjeto: EscoposProjeto;
  metasProjeto: Meta[];
  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  ngOnInit() {
    this.informacoes$ = this.store$.pipe(select(fromProjeto.getInformacoes));
    this.metas$ = this.store$.pipe(select(fromProjeto.getMetas));
    this.indicadores$ = this.store$.pipe(select(fromProjeto.getIndicadores));
    this.subscription.add(this.activedRoute.paramMap.subscribe(params => {
      this.programaId = params.get("id");
      this.store$.dispatch(new ListarIndicadores(this.programaId));
      this.programa$ = this.projetoService.carregarProgramaPorId(this.programaId);
    }))
    this.subscription.add(this.informacoes$.subscribe((informacoes: InformacoesProjeto) => {
      if (informacoes) {
        this.breadcrumbService.changeBreadcrumb(this.activedRoute.snapshot, informacoes.nome);
      }
    }))
  }

  completeStep1($event) {
    this.completedInfo = $event.form.valid;
    this.informacoesProjeto = $event.form.value;
    this.informacoesProjeto.nome = this.informacoesProjeto.nome ? $event.nomePrograma.concat(' - ').concat($event.form.value.nome) : $event.nomePrograma;
    this.store$.dispatch(new AddInformacoes(this.informacoesProjeto));
  }

  completeStep2($event) {
    this.completedEscopo = $event.form.valid;
    this.escoposProjeto = $event.form.value;
  }

  completeStep3($event) {
    this.completedIndicadores = $event.form.valid;
    this.metasProjeto = $event.form.value.metas;
  }

  completeStep4($event) {
    this.completedFinanceiro = $event.form.valid;
    if (this.informacoesProjeto) {
      this.informacoesProjeto.orcamento = $event.form.value.orcamento;
    }
  }

  completeStep5($event) {
    this.completedCronograma = $event.form.valid;
  }

  salvarRascunho(informacoes: InformacoesProjeto) {
    informacoes && informacoes.id ? this.editar(false) : this.salvar(true)
  }

  salvar = (rascunho: boolean) =>
    this.openDialogo(new AddProjeto(rascunho, this.persistenceService.get("usuario_instituto").numeroCooperativa), fromProjeto.getCadastroSucesso);

  editar = (rascunho: boolean = false) =>
    this.openDialogo(new UpdateProjeto(rascunho, this.persistenceService.get("usuario_instituto").numeroCooperativa), fromProjeto.getEditarSucesso);

  private openDialogo(action: Action, selector: MemoizedSelector<object, boolean>) {
    let acao = action instanceof UpdateProjeto ? AcaoProjeto.editado : AcaoProjeto.criado;
    this.store$.dispatch(action);
    this.subscription.add(this.store$.pipe(select(fromProjeto.getProjetoAtual)).subscribe((projeto: Projeto) => {
      if (projeto) {
        let md = this.modalService.open(DialogoConcluirCadastroProjeto, { data: { acao }, panelClass: 'sins-modal-success-projeto' });
        this.modalRef.push(md);
        this.subscription.add(md.afterClosed().subscribe((success: boolean) => {
          if (success) {
            this.store$.dispatch(new CleanProjetoCadastro());
            this.store$.dispatch(new CleanProjetoAtual());
            this.router.navigate(["/projetos", projeto.id]);
          } else {
            this.store$.dispatch(new CleanProjetoCadastro()); this.router.navigate(["/projetos"]);
          }
        }))
      }
    }))
  }

  openModal() {
    let md = this.modalService.open(DialogoConcluirCadastroProjeto, { data: { acao: AcaoProjeto.criado }, panelClass: 'sins-modal-success-projeto' });
  }

  concluir($event) {
    let stepIndex = $event.step;
    if (stepIndex) {
      switch (stepIndex) {
        case 1:
          this.store$.dispatch(new AddInformacoes(this.informacoesProjeto));
          break;
        case 2:
          this.store$.dispatch(new AddEscopos(this.escoposProjeto));
          break;
        case 3:
          this.store$.dispatch(new AddMetas(this.metasProjeto));
          break;
        case 4:
          this.store$.dispatch(new AddInformacoes(this.informacoesProjeto));
        default:
          break;
      }
      if ($event.isLast) {
        $event.editar ? this.editar() : this.salvar(false);
      }
    }
  }
}
