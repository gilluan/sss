import { Component } from '@angular/core';

@Component({
  selector: 'sc-container-router-projeto',
  template: `<router-outlet></router-outlet>`
})
export class ContainerRouterProjetoComponent {

  constructor() { }


}

