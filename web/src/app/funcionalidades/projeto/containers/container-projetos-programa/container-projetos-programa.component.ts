import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';
import {Observable, Subscription} from 'rxjs';
import {ProjetoService} from '../../projeto.service';
import {ModalRef, ModalService} from '@sicoob/ui';
import {DialogoNovoProjetoComponent} from '../../components/dialogo-novo-projeto/dialogo-novo-projeto.component';
import {BreadcrumbService} from '@app/shared/components/breadcrumb/breadcrumb.service';

@Component({
  selector: 'sc-container-projetos-programa',
  templateUrl: './container-projetos-programa.component.html',
  styleUrls: ['./container-projetos-programa.component.scss']
})
export class ContainerProjetosProgramaComponent implements OnInit {

  subscription: Subscription = new Subscription();
  modalRefs: ModalRef[] = new Array<ModalRef>();

  constructor(
    public router: Router,
    private activedRoute: ActivatedRoute,
    private projetoService: ProjetoService,
    private readonly modalService: ModalService,
    private breadcrumbService: BreadcrumbService,
  ) { }

  programaId: string;
  programa$: Observable<Programa>;

  ngOnInit(): void {
    this.activedRoute.paramMap.subscribe(params => {
      this.programaId = params.get('id');
      this.programa$ = this.projetoService.carregarProgramaPorId(this.programaId);
    });
    this.subscription.add(this.programa$.subscribe((programa: Programa) => {
      if (programa) {
        this.breadcrumbService.changeBreadcrumb(this.activedRoute.snapshot, programa.nome);
      }
    }));
  }

  alterarPrograma() {
    const m = this.modalService.open(DialogoNovoProjetoComponent, { data: {}, panelClass: 'sins-modal-projetos' });
    this.subscription.add(m.afterClosed().subscribe((data: any) => {
      if (data.programa) {
        this.router.navigate([`/projetos/programa/${data.programa}`]);
      }
    }));
    this.modalRefs.push(m);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.modalRefs.forEach(modalRef => { if (modalRef) { modalRef.close(); } });
  }

  getImagem(programa: Programa) {
    let src = 'data:image/jpeg;base64,';
    if (programa.imagem) {
      src = `${src}${programa.imagem}`;
    }
    return src;
  }



}
