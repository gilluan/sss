import { TemplatePortal } from '@angular/cdk/portal';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CarregarCooperativas } from '@app/actions/usuario-instituto.actions';
import { ProgramaFiltro } from '@app/funcionalidades/programas/models/programa.filtro';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { TotalSituacaoVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import * as fromUsuarioInstituto from '@app/reducers/usuario-instituto.reducer';
import { DialogoConfirmacaoComponent } from '@app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { UsuarioInstitutoService } from '@app/shared/services/usuario-instituto.service';
import { PerfilType } from '@app/shared/types/perfil.type';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService, HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AddEscopos, AddInformacoes, AddMetas, ClearProjetos, DeleteProjeto, LoadProjetos } from '../../actions/projeto.actions';
import { ActionbarFiltroProjetosComponent } from '../../components/actionbar-filtro-projetos/actionbar-filtro-projetos.component';
import { DialogoConcluirPassoProjeto } from '../../components/dialogo-concluir-passo-projeto/dialogo-concluir-passo-projeto.component';
import { DialogoNovoProjetoComponent } from '../../components/dialogo-novo-projeto/dialogo-novo-projeto.component';
import { FiltroProjetosComponent } from '../../components/filtro-projetos/filtro-projetos.component';
import { ProjetoSituacaoType } from '../../models/projeto-situacao.type';
import { ProjetoFiltro } from '../../models/projeto.filtro';
import { Projeto } from '../../models/projeto.model';
import { EscoposProjeto } from '../../models/wizard/escopos-projeto.model';
import { InformacoesProjeto } from '../../models/wizard/informacoes-projeto.model';
import { ProjetoService } from '../../projeto.service';
import * as fromProjeto from '../../reducers/projeto.reducer';

@Component({
  selector: 'sc-container-projeto',
  templateUrl: './container-projeto.component.html',
  styleUrls: ['./container-projeto.component.css']
})
export class ContainerProjetoComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();

  @ViewChild('filtroProjetos') filtroProjetosComponent: FiltroProjetosComponent;
  @ViewChild('tpl') headerActionsTmpl: TemplateRef<any>;
  projetos$: Observable<Projeto[]>;
  filtro: ProjetoFiltro = new ProjetoFiltro();
  counters$: Observable<TotalSituacaoVm[]>;
  usuarioinstituto: any;
  modalRefs: ModalRef[] = new Array<ModalRef>();
  actionBarRef: ActionBarRef[] = new Array<ActionBarRef>();
  editar: boolean = false;
  isLoading$: Observable<boolean>;

  constructor(private store$: Store<fromProjeto.State>,
    private headerActionsService: HeaderActionsContainerService,
    private readonly storeUsuarioInstituto: Store<fromUsuarioInstituto.State>,
    private readonly actionBarService: ActionBarService,
    private readonly modalService: ModalService,
    private readonly router: Router,
    private readonly persistenceService: PersistanceService,
    private projetoService: ProjetoService,
    private usuarioInstitutoService: UsuarioInstitutoService,
  ) { }

  ngOnInit() {
    this.subscription.add(this.store$.pipe(select(fromProjeto.getFiltroAtual)).subscribe((filtro: ProjetoFiltro) => this.filtro = filtro))
    this.dispatchLoad();
    this.isLoading$ = this.store$.pipe(select(fromProjeto.isLoadingProjetos))
    this.projetos$ = this.store$.pipe(select(fromProjeto.getProjetos));
    this.counters$ = this.store$.pipe(select(fromProjeto.getTotalSituacao));
    this.usuarioinstituto = this.persistenceService.get('usuario_instituto');
    this.numeroCooperativaFixo(this.usuarioinstituto.numeroCooperativa)
  }

  editarProjeto(projeto: Projeto) {
    if (projeto && projeto.programa._id) {
      this.editar = true;
      this.store$.dispatch(new AddInformacoes(this.montarInformacoes(projeto)));
      this.store$.dispatch(new AddEscopos(this.montarEscopos(projeto)));
      this.store$.dispatch(new AddMetas(projeto.metas));
      this.router.navigate(['projetos/novo/' + projeto.programa._id])
    }
  }

  private montarInformacoes(projeto: Projeto): InformacoesProjeto {
    let info = new InformacoesProjeto();
    info.codigoProjeto = projeto.codigoProjeto;
    info.id = projeto.id;
    info.dataCriacao = projeto.dataCriacao;
    info.dataModificacao = projeto.dataModificacao;
    info.dataFim = projeto.dataFim;
    info.dataInicio = projeto.dataInicio;
    info.justificativa = projeto.justificativa;
    info.nome = projeto.nome;
    info.orcamento = projeto.orcamento;
    info.programa = projeto.programa;
    info.responsavel = projeto.responsavel;
    info.criador = projeto.criador;
    return info;
  }

  private montarEscopos(projeto: Projeto): EscoposProjeto {
    let escopos = new EscoposProjeto();
    escopos.escopo = projeto.escopo;
    escopos.naoEscopo = projeto.naoEscopo;
    return escopos;
  }

  excluirProjeto(projeto: Projeto) {
    const modalConfig = { data: { header: 'Excluir Projeto', pergunta: 'Deseja realmente excluir o projeto?' }, panelClass: 'sins-modal-confirm' };
    let modalRef = this.modalService.open(DialogoConfirmacaoComponent, modalConfig);
    this.subscription.add(modalRef.afterClosed().subscribe((data: any) =>
      data ? this.store$.dispatch(new DeleteProjeto(projeto.id, this.filtro)) : false
    ));
    this.modalRefs.push(modalRef);
  }

  irParaSuasTarefas(projeto: Projeto) {
    this.router.navigate(['/projetos', projeto.id])
  }

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  onScroll() {
    if (this.filtro.paginacao.pageNumber < this.filtro.paginacao.totalPages) {
      this.filtro.paginacao.pageNumber++;
      this.filtro.paginacao = this.filtro.paginacao;
      this.dispatchLoad();
    }
  }

  onChangeTab($event) {
    this.store$.dispatch(new ClearProjetos());
    this.filtro.estado = $event;
    this.filtro.paginacao = new PageModelVm(0, 8, 0);
    this.dispatchLoad();
  }

  onClear(numero) {
    this.store$.dispatch(new ClearProjetos());
    this.irParaTodas();
    this.filtro.estado = ProjetoSituacaoType.todos;
    this.numeroCooperativaFixo(numero);
    this.filtro.paginacao = new PageModelVm(0, 8, 0);
    this.dispatchLoad();
  }

  onClearResponsavel() {
    this.store$.dispatch(new ClearProjetos());
    this.irParaTodas();
    this.filtro.usuarioInstituto = null;
    this.filtro.estado = ProjetoSituacaoType.todos;
    this.filtro.paginacao = new PageModelVm(0, 8, 0);
    this.dispatchLoad();
  }

  onClearNomeFiltro() {
    this.store$.dispatch(new ClearProjetos());
    this.irParaTodas();
    this.filtro.nome = null;
    this.filtro.estado = ProjetoSituacaoType.todos;
    this.filtro.paginacao = new PageModelVm(0, 8, 0);
    this.dispatchLoad();
  }

  onClearProgramaFiltro() {
    this.store$.dispatch(new ClearProjetos());
    this.irParaTodas();
    this.filtro.programa = null;
    this.filtro.estado = ProjetoSituacaoType.todos;
    this.filtro.paginacao = new PageModelVm(0, 8, 0);
    this.dispatchLoad();
  }

  ngOnDestroy(): void {
    this.headerActionsService.remove();
    this.subscription.unsubscribe();
    this.modalRefs.forEach(modalRef => { if (modalRef) { modalRef.close(); } });
    this.actionBarRef.forEach(actionBarRef => { if (actionBarRef) { actionBarRef.close(); } });
    this.store$.dispatch(new ClearProjetos(this.editar));
  }

  private tratarResponsaveis = (responsaveis$) => {
    return responsaveis$.pipe(map((users: any) => {
      let usuarios: UsuarioInstituto[] = []
      users.forEach(u => {
        let us = new UsuarioInstituto();
        us.id = u._id
        us.nome = u.nome
        usuarios.push(us);
      });
      return usuarios;
    }))
  }

  abrirFiltroActionBar(): void {
    this.carregarCooperativas();
    let programa$ = this.projetoService.carregarProgramasInfos(new ProgramaFiltro(null, true, null));
    let responsaveis$, cooperativasObservable;
    if (this.isInstituto()) {
      cooperativasObservable = this.storeUsuarioInstituto.pipe(
        select(fromUsuarioInstituto.selectCooperativas),
        map((numerosCooperativas: string[]) => numerosCooperativas)
      );
      responsaveis$ = this.usuarioInstitutoService.findAll(null, null, null); // FIXME
      responsaveis$ = this.tratarResponsaveis(responsaveis$);
    }
    const config: ActionBarConfig = {
      data: {
        cooperativas: cooperativasObservable,
        programas: programa$,
        responsaveis: responsaveis$,
        filtros: this.filtro,
      },
      panelClass: 'action-bar-filtro-projetos'
    };
    let actionBarRef = this.actionBarService.open(ActionbarFiltroProjetosComponent, config);
    this.subscription.add(actionBarRef.afterClosed().subscribe(data => this.filtrarActionBarAfterClose(data)));
    this.actionBarRef.push(actionBarRef)
  }

  abrirCriar() {
    let m = this.modalService.open(DialogoNovoProjetoComponent, { data: {}, panelClass: 'sins-modal-projetos' });
    this.subscription.add(m.afterClosed().subscribe((data: any) => {
      if (data.programa) {
        this.router.navigate([`/projetos/programa/${data.programa}`]);
      }
    }));
    this.modalRefs.push(m);
  }

  private filtrarActionBarAfterClose = (data) => {
    if (data) {
      if (data.action === 'filtrar') {
        this.store$.dispatch(new ClearProjetos());
        this.filtro.numeroCooperativa = data.data;
        this.filtro.nome = data.filtroNome;
        this.filtro.programa = data.programa;
        this.filtro.usuarioInstituto = data.responsavel;
        this.dispatchLoad();
      }
    }
  }

  private carregarCooperativas = () => this.storeUsuarioInstituto.dispatch(new CarregarCooperativas());

  private irParaTodas = () => this.filtroProjetosComponent ? this.filtroProjetosComponent.mudarAba(ProjetoSituacaoType.todos) : null;

  private numeroCooperativaFixo = (numero) =>
    this.isInstituto() ? this.removerCooperativa(numero) : this.filtro.numeroCooperativa = [+(this.usuarioinstituto.numeroCooperativa as string)]

  private isInstituto = () => this.usuarioinstituto.perfil === PerfilType.ppe || this.usuarioinstituto.perfil === PerfilType.gestor;
  private removerCooperativa = (numero) => this.filtro.numeroCooperativa = this.filtro.numeroCooperativa.filter(n => n !== numero)

  private dispatchLoad = () => this.store$.dispatch(new LoadProjetos(this.filtro));


}
