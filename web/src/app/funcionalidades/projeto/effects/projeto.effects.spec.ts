import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ProjetoEffects } from './projeto.effects';

describe('ProjetoEffects', () => {
  let actions$: Observable<any>;
  let effects: ProjetoEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProjetoEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(ProjetoEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
