import { Injectable } from '@angular/core';
import { AddGameUser } from '@app/app.actions';
import * as fromApp from '@app/app.reduce';
import { PassoFluxoExecucaoFiltro } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.filtro';
import { PassoFluxoSituacao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-situacao.type';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { IndicadoresService } from '@app/funcionalidades/programas/indicadores.services';
import { Indicador } from '@app/funcionalidades/projeto/models/indicador.model';
import { OrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import { TotaisOrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import { TotalProjetosSituacao } from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';
import { TotalSituacaoVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { Game } from '@app/shared/models/game.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { PerfilType } from '@app/shared/types/perfil.type';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { Color, ModalService } from '@sicoob/ui';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { AddProjeto, AddProjetoFail, AddProjetoSuccess, AprovarPassoProjeto, AprovarPassoProjetoFail, AprovarPassoProjetoSuccess, ConvidarVoluntariosProjetos, ConvidarVoluntariosProjetosFail, ConvidarVoluntariosProjetosSuccess, DeleteProjeto, DeleteProjetoFail, DeleteProjetoSuccess, ExecutarPassoProjeto, ExecutarPassoProjetoFail, ExecutarPassoProjetoSuccess, ListarIndicadores, ListarIndicadoresFail, ListarIndicadoresSuccess, LoadConviteIntencaoProjeto, LoadConviteIntencaoProjetoFail, LoadConviteIntencaoProjetoSuccess, LoadInvestimentoPorId, LoadInvestimentoPorIdFail, LoadInvestimentoPorIdSuccess, LoadInvestimentosProjeto, LoadInvestimentosProjetoFail, LoadInvestimentosProjetoSuccess, LoadPassosAguardandoAprovacao, LoadPassosAguardandoAprovacaoFail, LoadPassosAguardandoAprovacaoSuccess, LoadPassosConcluidos, LoadPassosConcluidosFail, LoadPassosConcluidosSuccess, LoadPassosFluxoProjeto, LoadPassosFluxoProjetoFail, LoadPassosFluxoProjetoSuccess, LoadProjetoById, LoadProjetoByIdFail, LoadProjetoByIdSuccess, LoadProjetos, LoadProjetosFail, LoadProjetosSuccess, LoadTotaisInvestimento, LoadTotaisInvestimentoFail, LoadTotaisInvestimentoSuccess, LoadTotalProjetosSituacao, LoadTotalProjetosSituacaoFail, LoadTotalProjetosSituacaoSuccess, LoadUsuariosProjetos, LoadUsuariosProjetosFail, LoadUsuariosProjetosSuccess, LoadVigenciasDisponiveis, LoadVigenciasDisponiveisFail, LoadVigenciasDisponiveisSuccess, ProjetoActionTypes, TotalProjetos, TotalProjetosFail, TotalProjetosSuccess, TotalSituacaoProjetos, TotalSituacaoProjetosFail, TotalSituacaoProjetosSuccess, UpdateProjeto, UpdateProjetoFail, UpdateProjetoPontual, UpdateProjetoPontualFail, UpdateProjetoPontualSuccess, UpdateProjetoSuccess } from '../actions/projeto.actions';
import { DialogoConcluirPassoProjeto } from '../components/dialogo-concluir-passo-projeto/dialogo-concluir-passo-projeto.component';
import { ConviteIntencao } from '../models/convite-intencao.model';
import { Investimento } from '../models/investimento.model';
import { ProjetoTotal } from '../models/projeto-total.model';
import { Projeto } from '../models/projeto.model';
import { UsuariosProjeto } from '../models/usuarios-projetos.model';
import { ProjetoService } from '../projeto.service';
import * as fromProjeto from '../reducers/projeto.reducer';

const noDispatch = { dispatch: false };

@Injectable()
export class ProjetoEffects {

  constructor(
    private store$: Store<fromProjeto.State>,
    private appStore$: Store<fromApp.State>,
    private actions$: Actions,
    private _projetosService: ProjetoService,
    private _indicadoresService: IndicadoresService,
    private _modalService: ModalService,
    private alertService: CustomAlertService,
    private persistanceService: PersistanceService) { }

  @Effect()
  addProjeto$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.AddProjeto),
    map((action: AddProjeto) => action),
    withLatestFrom(this.store$.pipe(select(fromProjeto.getProjetoCadastro))),
    mergeMap(([_, projeto]: [AddProjeto, Projeto]) => {
      if (projeto) {
        return this._projetosService.salvar(projeto).pipe(
          map((projeto: Projeto) => new AddProjetoSuccess(projeto)),
          catchError(error => of(new AddProjetoFail(error))));
      } else {
        return of({ type: 'esperando projeto ficar pronto' });
      }
    }));

  @Effect({ dispatch: false })
  addProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.AddProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro cadastrando projeto')));

  @Effect()
  updateProjeto$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.UpdateProjeto),
    map((action: UpdateProjeto) => action),
    withLatestFrom(this.store$.pipe(select(fromProjeto.getProjetoEditar))),
    mergeMap(([_, projeto]: [UpdateProjeto, Projeto]) => {
      if (projeto) {
        return this._projetosService.editar(projeto).pipe(
          map((projeto: Projeto) => new UpdateProjetoSuccess(projeto)),
          catchError(error => of(new UpdateProjetoFail(error))));
      } else {
        return of({ type: 'esperando projeto ficar pronto' });
      }
    }));

  @Effect({ dispatch: false })
  updateProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.UpdateProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro editando projeto')));


  @Effect()
  updateProjetoPontual$ =
    this.actions$
      .ofType(ProjetoActionTypes.UpdateProjetoPontual).pipe(
        map((action: UpdateProjetoPontual) => action),
        switchMap((action: UpdateProjetoPontual) => this._projetosService.editar(action.projeto).pipe(
          map((projeto: Projeto) => new UpdateProjetoPontualSuccess(projeto, action.actions)),
          catchError(error => of(new UpdateProjetoPontualFail(error))))));

  @Effect()
  updateProjetoPontualSuccess$ = this.actions$.
    ofType(ProjetoActionTypes.UpdateProjetoPontualSuccess).pipe(
      switchMap((action: UpdateProjetoPontual) => [...action.actions]));

  @Effect()
  totalProjetos$ =
    this.actions$
      .ofType(ProjetoActionTypes.TotalProjetos).pipe(
        map((action: TotalProjetos) => action),
        switchMap((action: TotalProjetos) => this._projetosService.carregarTotais(action.filtro).pipe(
          map((total: ProjetoTotal) => new TotalProjetosSuccess(total)),
          catchError(error => of(new TotalProjetosFail(error))))));


  @Effect({ dispatch: false })
  totalProjetosFail$ = this.actions$.
    ofType(ProjetoActionTypes.TotalProjetosFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao contabilizar projetos')));

  @Effect()
  totalSituacaoProjetos$ =
    this.actions$
      .ofType(ProjetoActionTypes.TotalSituacaoProjetos).pipe(
        map((action: TotalSituacaoProjetos) => action),
        switchMap((action: TotalSituacaoProjetos) => this._projetosService.carregarTotalSituacao(action.filtro).pipe(
          map((total: TotalSituacaoVm[]) => new TotalSituacaoProjetosSuccess(total)),
          catchError(error => of(new TotalSituacaoProjetosFail(error))))));

  @Effect({ dispatch: false })
  totalSituacaoProjetosFail$ = this.actions$.
    ofType(ProjetoActionTypes.TotalSituacaoProjetosFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao contabilizar projetos por situação')));

  @Effect()
  loadProjetoById$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadProjetoById).pipe(
        map((action: LoadProjetoById) => action),
        switchMap((action: LoadProjetoById) => this._projetosService.carregarProjetoPorId(action.id).pipe(
          map((projeto: Projeto) => new LoadProjetoByIdSuccess(projeto)),
          catchError(error => of(new LoadProjetoByIdFail(error))))));

  @Effect({ dispatch: false })
  loadProjetoByIdFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadProjetoByIdFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar projeto')));

  @Effect()
  loadPassosFluxoProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadPassosFluxoProjeto).pipe(
        map((action: LoadPassosFluxoProjeto) => action),
        switchMap((action: LoadPassosFluxoProjeto) => this._projetosService.carregarPassosFluxoProjeto(action.filtro).pipe(
          map((passosFluxo: PassoFluxo[]) => new LoadPassosFluxoProjetoSuccess(passosFluxo)),
          catchError(error => of(new LoadPassosFluxoProjetoFail(error))))));

  @Effect({ dispatch: false })
  loadPassosFluxoProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadPassosFluxoProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar passos do marco')));


  @Effect()
  loadPassosAguardandoAprovacao$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadPassosAguardandoAprovacao).pipe(
        map((action: LoadPassosAguardandoAprovacao) => action),
        switchMap((action: LoadPassosAguardandoAprovacao) => this._projetosService.carregarPassosFluxoProjeto(new PassoFluxoExecucaoFiltro(action.idProjeto, null, null, null, null, PassoFluxoSituacao.aguardandoAprovacao, action.usuarioId)).pipe(
          map((passosFluxo: PassoFluxo[]) => new LoadPassosAguardandoAprovacaoSuccess(passosFluxo)),
          catchError(error => of(new LoadPassosAguardandoAprovacaoFail(error))))));

  @Effect({ dispatch: false })
  loadPassosAguardandoAprovacaoFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadPassosAguardandoAprovacaoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar passos do marco')));


  @Effect()
  loadInvestimentoPorId$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadInvestimentoPorId).pipe(
        map((action: LoadInvestimentoPorId) => action),
        switchMap((action: LoadInvestimentoPorId) => this._projetosService.carregarInvestimentoPorId(action.idProjeto).pipe(
          map((investimento: Investimento) => new LoadInvestimentoPorIdSuccess(investimento)),
          catchError(error => of(new LoadInvestimentoPorIdFail(error))))));

  @Effect({ dispatch: false })
  loadInvestimentoPorIdFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadInvestimentoPorIdFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar investimento do projeto')));


  @Effect()
  loadPassosConcluidos$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadPassosConcluidos).pipe(
        map((action: LoadPassosConcluidos) => action),
        switchMap((action: LoadPassosConcluidos) => this._projetosService.carregarPassosFluxoProjeto(new PassoFluxoExecucaoFiltro(action.idProjeto, null, null, null, null, PassoFluxoSituacao.concluida)).pipe(
          map((passosFluxo: PassoFluxo[]) => new LoadPassosConcluidosSuccess(passosFluxo)),
          catchError(error => of(new LoadPassosConcluidosFail(error))))));

  @Effect({ dispatch: false })
  loadPassosConcluidosFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadPassosConcluidosFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar passos concluídos')));

  @Effect()
  ExecutarPassoProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.ExecutarPassoProjeto).pipe(
        map((action: ExecutarPassoProjeto) => action),
        withLatestFrom(this.appStore$.pipe(select(fromApp.getGame))),
        mergeMap(([action, game]: [ExecutarPassoProjeto, Game[]]) => this._projetosService.executarPasso(action.idPasso, action.passoFluxoExecucao).pipe(
          map((passosFluxo: any) => {
            const isConcluida = action.passoFluxoExecucao.situacao === PassoFluxoSituacao.concluida;
            const temPontos = this.temPontos(game, action.idPasso, action.passoFluxoExecucao.projeto);
            const mostrarRecompensa = isConcluida && action.qtdPontos > 0 && !temPontos;
            const actions = action.filtro ? [new LoadPassosFluxoProjeto(action.filtro)] : []
            return new ExecutarPassoProjetoSuccess(passosFluxo, action.actionBarRef, { mostrarRecompensa, isConcluida }, action.qtdPontos, actions);
          }),
          catchError(error => of(new ExecutarPassoProjetoFail(error))))));

  @Effect()
  executarPassoProjetoSuccess$ =
    this.actions$
      .ofType(ProjetoActionTypes.ExecutarPassoProjetoSuccess).pipe(
        map((action: ExecutarPassoProjetoSuccess) => action),
        switchMap((action: ExecutarPassoProjetoSuccess) => {
          if (action.conditions.isConcluida && action.actionBarRef) {
            action.actionBarRef.close();
            this.alertService.abrirAlert(Color.SUCCESS, 'Passo executado com sucesso!')
          }
          if (action.conditions.mostrarRecompensa) {
            this.mostrarRecompensa(action.qtdPontos);
          }

          return [this.getAddGameUser(), ...action.actions]
        }));

  private getAddGameUser() {
    const usuario = this.persistanceService.get('usuario_instituto');
    const cpf = usuario.cpf;
    const isVoluntario = usuario.perfil === PerfilType.voluntario;
    return new AddGameUser(cpf, isVoluntario)
  }
  private mostrarRecompensa(qtdPontos: number) {
    this._modalService.open(DialogoConcluirPassoProjeto, { data: { qtdPontos } });
  }
  private temPontos(game: Game[], passoId: string, projeto: string) {
    if (game) {
      const array = game.filter((g: Game) => g.passoId === passoId && g.projeto === projeto);
      return array.length > 0;
    }
    return false;
  }

  @Effect({ dispatch: false })
  ExecutarPassoProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.ExecutarPassoProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Aconteceu um erro executando o passo do marco')));

  @Effect()
  AprovarPassoProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.AprovarPassoProjeto).pipe(
        map((action: AprovarPassoProjeto) => action),
        switchMap((action: AprovarPassoProjeto) => this._projetosService.executarPasso(action.idPasso, action.passoFluxoExecucao).pipe(
          switchMap((passosFluxo: any) => [new AprovarPassoProjetoSuccess(passosFluxo), new LoadPassosAguardandoAprovacao(action.passoFluxoExecucao.projeto, action.usuarioLogadoId), new LoadPassosConcluidos(action.passoFluxoExecucao.projeto)]),
          catchError(error => of(new AprovarPassoProjetoFail(error))))));

  @Effect({ dispatch: false })
  AprovarPassoProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.AprovarPassoProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Aconteceu um erro executando o passo do marco')));

  @Effect()
  loadUsuarios$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadUsuariosProjetos).pipe(
        map((action: LoadUsuariosProjetos) => action),
        switchMap((action: LoadUsuariosProjetos) =>
          this._projetosService.carregarUsuariosPorProjeto(action.id).pipe(
            map((usuarios: UsuariosProjeto) => new LoadUsuariosProjetosSuccess(usuarios)),
            catchError(error => of(new LoadUsuariosProjetosFail(error))))
        ));

  @Effect({ dispatch: false })
  loadUsuariosFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadUsuariosProjetosFail).pipe(
      map((action: LoadUsuariosProjetosFail) => this.alertService.abrirAlert(Color.DANGER, `Erro ao carregar usuários do projeto`)));

  @Effect()
  convidarVoluntarioProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.ConvidarVoluntariosProjetos).pipe(
        map((action: ConvidarVoluntariosProjetos) => action),
        switchMap((action: ConvidarVoluntariosProjetos) =>
          this._projetosService.convidarVoluntario(action.conviteIntencao).pipe(
            switchMap((conviteIntencao: ConviteIntencao) => [new LoadConviteIntencaoProjeto(conviteIntencao.projeto, conviteIntencao.convidador), new ConvidarVoluntariosProjetosSuccess(conviteIntencao)]),
            catchError(error => of(new ConvidarVoluntariosProjetosFail(error))))
        ));

  @Effect({ dispatch: false })
  convidarVoluntarioProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.ConvidarVoluntariosProjetosFail).pipe(
      map((action: ConvidarVoluntariosProjetosFail) => this.alertService.abrirAlert(Color.DANGER, action.error ? action.error : 'Não foi possível convidar os voluntários')));

  @Effect({ dispatch: false })
  convidarVoluntarioProjetoSuccess$ = this.actions$.
    ofType(ProjetoActionTypes.ConvidarVoluntariosProjetosSuccess).pipe(
      map((action: ConvidarVoluntariosProjetosSuccess) => this.alertService.abrirAlert(Color.SUCCESS, `Usuários convidados com sucesso`)));

  @Effect()
  loadConviteIntencaoProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadConviteIntencaoProjeto).pipe(
        map((action: LoadConviteIntencaoProjeto) => action),
        switchMap((action: LoadConviteIntencaoProjeto) =>
          this._projetosService.carregarConviteIntencao(action.projeto, action.convidador).pipe(
            map((conviteIntencao: ConviteIntencao) => new LoadConviteIntencaoProjetoSuccess(conviteIntencao)),
            catchError(error => of(new LoadConviteIntencaoProjetoFail(error))))
        ));

  @Effect({ dispatch: false })
  loadConviteIntencaoProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadConviteIntencaoProjetoFail).pipe(
      map((action: LoadConviteIntencaoProjetoFail) => this.alertService.abrirAlert(Color.DANGER, action.error ? action.error : 'Não foi possível verificar se existe convite pendente')));

  @Effect()
  getProjetos$ =
    this.actions$
      .ofType(ProjetoActionTypes.LoadProjetos).pipe(
        map((action: LoadProjetos) => action),
        switchMap((action: LoadProjetos) => this._projetosService.carregar(action.filtro).pipe(
          switchMap((pageData: PagedDataModelVm) => [new LoadProjetosSuccess(pageData), new TotalSituacaoProjetos({ ...action.filtro, estado: 'todos' })]),
          catchError(error => of(new LoadProjetosFail(error))))));


  @Effect({ dispatch: false })
  getProjetosFail$ = this.actions$.
    ofType(ProjetoActionTypes.LoadProjetosFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar projetos')));

  @Effect()
  deleteProjeto$ =
    this.actions$
      .ofType(ProjetoActionTypes.DeleteProjeto).pipe(
        map((action: DeleteProjeto) => action),
        switchMap((action: DeleteProjeto) => this._projetosService.remover(action.id).pipe(
          switchMap((projeto: Projeto) => [new DeleteProjetoSuccess(projeto), new TotalSituacaoProjetos({ ...action.filtro, estado: 'todos' })]),
          catchError(error => of(new DeleteProjetoFail(error))))));


  @Effect({ dispatch: false })
  deleteProjetoFail$ = this.actions$.
    ofType(ProjetoActionTypes.DeleteProjetoFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao excluir projetos')));

  @Effect({ dispatch: false })
  deleteProjetoSuccess$ = this.actions$.
    ofType(ProjetoActionTypes.DeleteProjetoSuccess).pipe(
      map((_) => this.alertService.abrirAlert(Color.SUCCESS, 'Projeto excluído com Sucesso')));

  @Effect()
  listarIndicadores$ =
    this.actions$
      .ofType(ProjetoActionTypes.ListarIndicadores).pipe(
        map((action: ListarIndicadores) => action),
        switchMap((action: ListarIndicadores) => this._indicadoresService.carregarIndicadores(action.idPrograma).pipe(
          switchMap((indicadores: Indicador[]) => [new ListarIndicadoresSuccess(indicadores)]),
          catchError(error => of(new ListarIndicadoresFail(error))))));

  @Effect({ dispatch: false })
  listarIndicadoresFail$ = this.actions$.
    ofType(ProjetoActionTypes.ListarIndicadoresFail).pipe(
      map((_) => this.alertService.abrirAlert(Color.DANGER, 'Erro ao carregar indicadores do programa')));

  @Effect()
  loadTotaisInvestimento$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadTotaisInvestimento),
    switchMap((action: LoadTotaisInvestimento) => this._projetosService.carregarTotalOrcamentoInvestimento(action.anoVigencia).pipe(
      map((totais: TotaisOrcamentoInvestimentoProjeto) => new LoadTotaisInvestimentoSuccess(totais)),
      catchError(error => of(new LoadTotaisInvestimentoFail(error))))));

  @Effect(noDispatch)
  loadTotaisInvestimentoFail$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadTotaisInvestimentoFail),
    map((action: LoadTotaisInvestimentoFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os totais de investimento dos projetos.')));

  @Effect()
  loadInvestimentosProjeto$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadInvestimentosProjeto),
    switchMap((action: LoadInvestimentosProjeto) => this._projetosService.carregarOrcamentoInvestimento(action.anoVigencia).pipe(
      map((investimentos: OrcamentoInvestimentoProjeto[]) => new LoadInvestimentosProjetoSuccess(investimentos)),
      catchError(error => of(new LoadInvestimentosProjetoFail(error))))));

  @Effect(noDispatch)
  loadInvestimentosProjeto$Fail$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadInvestimentosProjetoFail),
    map((action: LoadTotaisInvestimentoFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os investimentos dos projetos.')));


  @Effect()
  loadVigenciasDisponiveis$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadVigenciasDisponiveis),
    switchMap((action: LoadVigenciasDisponiveis) => this._projetosService.carregarVigenciasDisponiveis().pipe(
      map((vigencias: string[]) => new LoadVigenciasDisponiveisSuccess(vigencias)),
      catchError(error => of(new LoadVigenciasDisponiveisFail(error))))));

  @Effect(noDispatch)
  loadVigenciasDisponiveisFail$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadVigenciasDisponiveisFail),
    map((action: LoadVigenciasDisponiveisFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar as vigências disponíveis.')));

  @Effect()
  loadTotalProjetosSituacao$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadTotalProjetosSituacao),
    switchMap((action: LoadTotalProjetosSituacao) => this._projetosService.carregarTotalPorSituacao(action.anoVigencia).pipe(
      map((total: TotalProjetosSituacao) => new LoadTotalProjetosSituacaoSuccess(total)),
      catchError(error => of(new LoadTotalProjetosSituacaoFail(error))))));

  @Effect(noDispatch)
  loadTotalProjetosSituacaoFail$ = this.actions$.pipe(
    ofType(ProjetoActionTypes.LoadTotalProjetosSituacaoFail),
    map((action: LoadTotalProjetosSituacaoFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar o total de projetos por situação.')));
}
