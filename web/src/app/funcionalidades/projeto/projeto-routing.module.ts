import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerCadastroProjetosComponent } from './containers/container-cadastro-projetos/container-cadastro-projetos.component';
import { ContainerProjetoComponent } from './containers/container-projeto/container-projeto.component';
import { ContainerProjetosProgramaComponent } from './containers/container-projetos-programa/container-projetos-programa.component';
import { ContainerRouterProjetoComponent } from './containers/container-router-projeto.component';
import { ContainerGerenciaProjetoComponent } from './containers/container-gerencia-projeto/container-gerencia-projeto.component';
import { SobreProjetoComponent } from './components/tabs-projeto/sobre-projeto/sobre-projeto.component';
import { EntregasProjetoComponent } from './components/tabs-projeto/entregas-projeto/entregas-projeto.component';
import { TimeProjetoComponent } from './components/tabs-projeto/time-projeto/time-projeto.component';
import { ArquivosProjetoComponent } from './components/tabs-projeto/arquivos-projeto/arquivos-projeto.component';
import { SuasTarefasProjetoComponent } from './components/tabs-projeto/suas-tarefas/suas-tarefas.component';
import { RespostasProjetoComponent } from './components/tabs-projeto/respostas-projeto/respostas-projeto.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerProjetoComponent,
  },
  {
    path: '',
    component: ContainerRouterProjetoComponent,
    children: [
      {
        path: ':id',
        component: ContainerGerenciaProjetoComponent,
        data: {
          breadcrumb: 'Gerenciar'
        },
        children: [
          {
            path: 'info',
            component: SobreProjetoComponent,
            data: {
              breadcrumb: 'Informações'
            },
          },
          {
            path: '',
            component: EntregasProjetoComponent,
            data: {
              breadcrumb: 'Entregas'
            },
          },
          {
            path: 'tarefas',
            component: SuasTarefasProjetoComponent,
            data: {
              breadcrumb: 'Suas Tarefas'
            }
          },
          {
            path: 'respostas',
            component: RespostasProjetoComponent,
            data: {
              breadcrumb: 'Respostas'
            }
          },
          {
            path: 'time',
            component: TimeProjetoComponent,
            data: {
              breadcrumb: 'Time'
            },
          },
          {
            path: 'arquivos',
            component: ArquivosProjetoComponent,
            data: {
              breadcrumb: 'Aquivos'
            },
          }
        ]
      },
      {
        path: 'programa/:id',
        component: ContainerProjetosProgramaComponent,
        data: {
          breadcrumb: 'Programa'
        }
      },
      {
        path: 'novo/:id',
        component: ContainerCadastroProjetosComponent,
        data: {
          breadcrumb: 'Novo'
        },
      }
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjetoRoutingModule { }
