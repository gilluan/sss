import { Marco } from '@app/funcionalidades/fluxo-processo/models/marco.model';
import { PassoFluxoExecucaoFiltro } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.filtro';
import { PassoFluxoExecucao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.model';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { OrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import { TotaisOrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import { TotalProjetosSituacao } from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';
import { TotalSituacaoVm } from '@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { Action } from '@ngrx/store';
import { ActionBarRef } from '@sicoob/ui';
import { ConviteIntencao } from '../models/convite-intencao.model';
import { CronogramaFinanceiro } from '../models/cronogramas/cronograma-financeiro.model';
import { CronogramaFisico } from '../models/cronogramas/cronograma-fisico.model';
import { Indicador } from '../models/indicador.model';
import { Investimento } from '../models/investimento.model';
import { Meta } from '../models/meta.model';
import { ProjetoTotal } from '../models/projeto-total.model';
import { ProjetoFiltro } from '../models/projeto.filtro';
import { Projeto } from '../models/projeto.model';
import { UsuariosProjeto } from '../models/usuarios-projetos.model';
import { EscoposProjeto } from '../models/wizard/escopos-projeto.model';
import { InformacoesProjeto } from '../models/wizard/informacoes-projeto.model';

export enum ProjetoActionTypes {
  CleanProjetoCadastro = '[CleanProjetoCadastro] Clean Projeto Cadastro',
  LoadProjetos = '[Projeto] Load Projetos',
  LoadProjetosSuccess = '[Projeto] Load Projetos Success',
  LoadProjetosFail = '[Projeto] Load Projetos Fail',
  TotalProjetos = '[Projeto] Total Projeto',
  TotalProjetosSuccess = '[Projeto] Total Projeto Success',
  TotalProjetosFail = '[Projeto] Total Projeto Fail',
  TotalSituacaoProjetos = '[Projeto] Count Projetos',
  TotalSituacaoProjetosSuccess = '[Projeto] Count Projetos Success',
  TotalSituacaoProjetosFail = '[Projeto] Count Projetos Fail',
  SelectedPrograma = '[Projeto] Selected Programa',
  AddProjeto = '[Projeto] Add Projeto',
  AddProjetoSuccess = '[Projeto] Add Projeto Success',
  AddProjetoFail = '[Projeto] Add Projeto Fail',
  AddInformacoes = '[Projeto] Add AddInformacoes',
  AddEscopos = '[Projeto] Add AddEscopos',
  UpdateProjeto = '[Projeto] Update Projeto',
  UpdateProjetoSuccess = '[Projeto] Update Projeto Success',
  UpdateProjetoFail = '[Projeto] Update Projeto Fail',
  DeleteProjeto = '[Projeto] Delete Projeto',
  DeleteProjetoSuccess = '[Projeto] Delete Projeto Success',
  DeleteProjetoFail = '[Projeto] Delete Projeto Fail',
  ClearProjetos = '[Projeto] Clear Projetos',
  AddCronogramaFinanceiro = '[CronogramaFinanceiro] Add Cronograma Financeiro',
  AddCronogramaFisico = '[CronogramaFisico] Add Cronograma FIsico',
  AddMetas = '[AddMetas] Add Metas',
  ListarIndicadores = '[ListarIndicadores] Projeto',
  ListarIndicadoresSuccess = '[ListarIndicadoresSuccess] Projeto',
  ListarIndicadoresFail = '[ListarIndicadoresFail] Projeto',
  LoadProjetoById = '[LoadProjetoById] Projeto',
  LoadProjetoByIdSuccess = '[LoadProjetoByIdSuccess] Projeto',
  LoadProjetoByIdFail = '[LoadProjetoByIdFail] Projeto',
  LoadUsuariosProjetos = '[LoadUsuariosProjetos] Projeto',
  LoadUsuariosProjetosSuccess = '[LoadUsuariosProjetosSuccess] Projeto',
  LoadUsuariosProjetosFail = '[LoadUsuariosProjetosFail] Projeto',
  CleanProjetoAtual = '[CleanProjetoAtual] Projet',
  CleanUsuariosProjetos = '[CleanUsuariosProjetos] Projet',
  ConvidarVoluntariosProjetos = '[ConvidarVoluntariosProjetos] Projeto',
  ConvidarVoluntariosProjetosSuccess = '[ConvidarVoluntariosProjetosSuccess] Projeto',
  ConvidarVoluntariosProjetosFail = '[ConvidarVoluntariosProjetosFail] Projeto',
  LoadConviteIntencaoProjeto = '[LoadConviteIntencaoProjeto] Projeto',
  LoadConviteIntencaoProjetoSuccess = '[LoadConviteIntencaoProjetoSuccess] Projeto',
  LoadConviteIntencaoProjetoFail = '[LoadConviteIntencaoProjetoFail] Projeto',
  SelectedMarco = '[SelectedMarco] Projeto',
  LoadPassosFluxoProjeto = '[LoadPassosFluxoProjeto] Projeto',
  LoadPassosFluxoProjetoSuccess = '[LoadPassosFluxoProjetoSuccess] Projeto',
  LoadPassosFluxoProjetoFail = '[LoadPassosFluxoProjetoFail] Projeto',
  LoadPassosFluxoProjetoClean = '[LoadPassosFluxoProjetoClean] Projeto',
  ExecutarPassoProjeto = '[ExecutarPassoProjeto] Projeto',
  ExecutarPassoProjetoSuccess = '[ExecutarPassoProjetoSuccess] Projeto',
  ExecutarPassoProjetoFail = '[ExecutarPassoProjetoFail] Projeto',
  IsResponsavelProjeto = '[IsResponsavelProjeto] Projeto',
  LoadPassosConcluidos = '[LoadPassosConcluidos] Projeto',
  LoadPassosConcluidosSuccess = '[LoadPassosConcluidosSuccess] Projeto',
  LoadPassosConcluidosFail = '[LoadPassosConcluidosFail] Projeto',
  LoadPassosConcluidosClean = '[LoadPassosConcluidosClean] Projeto',
  LoadPassosAguardandoAprovacao = '[LoadPassosAguardandoAprovacao] Projeto',
  LoadPassosAguardandoAprovacaoSuccess = '[LoadPassosAguardandoAprovacaoSuccess] Projeto',
  LoadPassosAguardandoAprovacaoFail = '[LoadPassosAguardandoAprovacaoFail] Projeto',
  LoadPassosAguardandoAprovacaoClean = '[LoadPassosAguardandoAprovacaoClean] Projeto',
  AprovarPassoProjeto = '[AprovarPassoProjeto] Projeto',
  AprovarPassoProjetoSuccess = '[AprovarPassoProjetoSuccess] Projeto',
  AprovarPassoProjetoFail = '[AprovarPassoProjetoFail] Projeto',
  LoadTotaisInvestimento = '[LoadTotaisInvestimento]',
  LoadTotaisInvestimentoSuccess = '[LoadTotaisInvestimentoSuccess]',
  LoadTotaisInvestimentoFail = '[LoadTotaisInvestimentoFail]',
  LoadInvestimentosProjeto = '[LoadInvestimentosProjeto]',
  LoadInvestimentosProjetoSuccess = '[LoadInvestimentosProjetoSuccess]',
  LoadInvestimentosProjetoFail = '[LoadInvestimentosProjetoFail]',
  LoadInvestimentoPorId = '[LoadInvestimentoPorId] Projetos',
  LoadInvestimentoPorIdSuccess = '[LoadInvestimentoPorIdSuccess] Projetos',
  LoadInvestimentoPorIdFail = '[LoadInvestimentoPorIdFail] Projetos',
  UpdateProjetoPontual = '[UpdateProjetoPontual] Projetos',
  UpdateProjetoPontualSuccess = '[UpdateProjetoPontualSuccess] Projetos',
  UpdateProjetoPontualFail = '[UpdateProjetoPontualFail] Projetos',
  LoadVigenciasDisponiveis = '[LoadVigenciasDisponiveis]',
  LoadVigenciasDisponiveisSuccess = '[LoadVigenciasDisponiveisSuccess]',
  LoadVigenciasDisponiveisFail = '[LoadVigenciasDisponiveisFail]',
  LoadTotalProjetosSituacao = '[LoadTotalProjetosSituacao]',
  LoadTotalProjetosSituacaoSuccess = '[LoadTotalProjetosSituacaoSuccess]',
  LoadTotalProjetosSituacaoFail = '[LoadTotalProjetosSituacaoFail]',
}

export class SelectedMarco implements Action {
  readonly type = ProjetoActionTypes.SelectedMarco;
  constructor(public marco: Marco) { }
}

export class LoadConviteIntencaoProjeto implements Action {
  readonly type = ProjetoActionTypes.LoadConviteIntencaoProjeto;
  constructor(public projeto: string, public convidador: string) { }
}
export class LoadConviteIntencaoProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadConviteIntencaoProjetoSuccess;
  constructor(public conviteIntencao: ConviteIntencao) { }
}
export class LoadConviteIntencaoProjetoFail implements Action {
  readonly type = ProjetoActionTypes.LoadConviteIntencaoProjetoFail;
  constructor(public error: any) { }
}

export class ConvidarVoluntariosProjetos implements Action {
  readonly type = ProjetoActionTypes.ConvidarVoluntariosProjetos;
  constructor(public conviteIntencao: ConviteIntencao) { }
}
export class ConvidarVoluntariosProjetosSuccess implements Action {
  readonly type = ProjetoActionTypes.ConvidarVoluntariosProjetosSuccess;
  constructor(public conviteIntencao: ConviteIntencao) { }
}
export class ConvidarVoluntariosProjetosFail implements Action {
  readonly type = ProjetoActionTypes.ConvidarVoluntariosProjetosFail;
  constructor(public error: any) { }
}

export class ListarIndicadores implements Action {
  readonly type = ProjetoActionTypes.ListarIndicadores;
  constructor(public idPrograma: string) { }
}
export class ListarIndicadoresSuccess implements Action {
  readonly type = ProjetoActionTypes.ListarIndicadoresSuccess;
  constructor(public indicadores: Indicador[]) { }
}
export class ListarIndicadoresFail implements Action {
  readonly type = ProjetoActionTypes.ListarIndicadoresFail;
  constructor(public error: any) { }
}
export class CleanProjetoCadastro implements Action {
  readonly type = ProjetoActionTypes.CleanProjetoCadastro;
  constructor() { }
}

export class CleanProjetoAtual implements Action {
  readonly type = ProjetoActionTypes.CleanProjetoAtual;
  constructor() { }
}
export class CleanUsuariosProjetos implements Action {
  readonly type = ProjetoActionTypes.CleanUsuariosProjetos;
  constructor() { }
}

export class LoadProjetoById implements Action {
  readonly type = ProjetoActionTypes.LoadProjetoById;
  constructor(public id: string) { }
}
export class LoadProjetoByIdSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadProjetoByIdSuccess;
  constructor(public projeto: Projeto) { }
}
export class LoadProjetoByIdFail implements Action {
  readonly type = ProjetoActionTypes.LoadProjetoByIdFail;
  constructor(public error: any) { }
}

export class LoadProjetos implements Action {
  readonly type = ProjetoActionTypes.LoadProjetos;
  constructor(public filtro: ProjetoFiltro) { }
}
export class LoadProjetosSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadProjetosSuccess;
  constructor(public pagedData: PagedDataModelVm) { }
}
export class LoadProjetosFail implements Action {
  readonly type = ProjetoActionTypes.LoadProjetosFail;
  constructor(public error: any) { }
}

export class TotalProjetos implements Action {
  readonly type = ProjetoActionTypes.TotalProjetos;
  constructor(public filtro: ProjetoFiltro) { }
}
export class TotalProjetosSuccess implements Action {
  readonly type = ProjetoActionTypes.TotalProjetosSuccess;
  constructor(public total: ProjetoTotal) { }
}
export class TotalProjetosFail implements Action {
  readonly type = ProjetoActionTypes.TotalProjetosFail;
  constructor(public erro: any) { }
}

export class TotalSituacaoProjetos implements Action {
  readonly type = ProjetoActionTypes.TotalSituacaoProjetos;
  constructor(public filtro: ProjetoFiltro) { }
}
export class TotalSituacaoProjetosSuccess implements Action {
  readonly type = ProjetoActionTypes.TotalSituacaoProjetosSuccess;
  constructor(public totalSituacao: TotalSituacaoVm[]) { }
}
export class TotalSituacaoProjetosFail implements Action {
  readonly type = ProjetoActionTypes.TotalSituacaoProjetosFail;
  constructor(public error: any) { }
}

export class SelectedPrograma implements Action {
  readonly type = ProjetoActionTypes.SelectedPrograma;
  constructor(public programa: string) { }
}

export class AddProjeto implements Action {
  readonly type = ProjetoActionTypes.AddProjeto;
  constructor(public rascunho: boolean, public numeroCooperativa: string) { }
}
export class AddProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.AddProjetoSuccess;
  constructor(public projeto: Projeto) { }
}
export class AddProjetoFail implements Action {
  readonly type = ProjetoActionTypes.AddProjetoFail;
  constructor(public error: any) { }
}

export class AddInformacoes implements Action {
  readonly type = ProjetoActionTypes.AddInformacoes;
  constructor(public informacoesProjeto: InformacoesProjeto) { }
}
export class AddEscopos implements Action {
  readonly type = ProjetoActionTypes.AddEscopos;
  constructor(public escopos: EscoposProjeto) { }
}
export class AddMetas implements Action {
  readonly type = ProjetoActionTypes.AddMetas;
  constructor(public metas: Meta[]) { }
}

export class AddCronogramaFinanceiro implements Action {
  readonly type = ProjetoActionTypes.AddCronogramaFinanceiro;
  constructor(public cronogramaFinanceiro: CronogramaFinanceiro) { }
}

export class AddCronogramaFisico implements Action {
  readonly type = ProjetoActionTypes.AddCronogramaFisico;
  constructor(public cronogramaFisico: CronogramaFisico, public dataFimProjeto: Date) { }
}

export class UpdateProjetoPontual implements Action {
  readonly type = ProjetoActionTypes.UpdateProjetoPontual;
  constructor(public projeto: Projeto, public actions: Action[]) { }
}
export class UpdateProjetoPontualSuccess implements Action {
  readonly type = ProjetoActionTypes.UpdateProjetoPontualSuccess;
  constructor(public projeto: Projeto, public actions: Action[]) { }
}
export class UpdateProjetoPontualFail implements Action {
  readonly type = ProjetoActionTypes.UpdateProjetoPontualFail;
  constructor(public error: any) { }
}

export class UpdateProjeto implements Action {
  readonly type = ProjetoActionTypes.UpdateProjeto;
  constructor(public rascunho: boolean, public numeroCooperativa: string) { }
}
export class UpdateProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.UpdateProjetoSuccess;
  constructor(public projeto: Projeto) { }
}
export class UpdateProjetoFail implements Action {
  readonly type = ProjetoActionTypes.UpdateProjetoFail;
  constructor(public error: any) { }
}

export class DeleteProjeto implements Action {
  readonly type = ProjetoActionTypes.DeleteProjeto;
  constructor(public id: string, public filtro: ProjetoFiltro) { }
}
export class DeleteProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.DeleteProjetoSuccess;
  constructor(public projeto: Projeto) { }
}
export class DeleteProjetoFail implements Action {
  readonly type = ProjetoActionTypes.DeleteProjetoFail;
  constructor(public error: any) { }
}

export class ClearProjetos implements Action {
  readonly type = ProjetoActionTypes.ClearProjetos;
  constructor(public editar?: boolean) { }
}

/* COMEÇO DO TRABALHO COM USUARIOS */
export class LoadUsuariosProjetos implements Action {
  readonly type = ProjetoActionTypes.LoadUsuariosProjetos;
  constructor(public id: string) { }
}
export class LoadUsuariosProjetosSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadUsuariosProjetosSuccess;
  constructor(public usuarios: UsuariosProjeto) { }
}
export class LoadUsuariosProjetosFail implements Action {
  readonly type = ProjetoActionTypes.LoadUsuariosProjetosFail;
  constructor(public error: any) { }
}
/*FIM DO TRABALHO COM USUÁRIOS */


export class LoadPassosFluxoProjeto implements Action {
  readonly type = ProjetoActionTypes.LoadPassosFluxoProjeto;
  constructor(public filtro: PassoFluxoExecucaoFiltro) { }
}
export class LoadPassosFluxoProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadPassosFluxoProjetoSuccess;
  constructor(public passosFluxos: PassoFluxo[]) { }
}
export class LoadPassosFluxoProjetoFail implements Action {
  readonly type = ProjetoActionTypes.LoadPassosFluxoProjetoFail;
  constructor(public error: any) { }
}
export class LoadPassosFluxoProjetoClean implements Action {
  readonly type = ProjetoActionTypes.LoadPassosFluxoProjetoClean;
  constructor() { }
}


export class LoadPassosConcluidos implements Action {
  readonly type = ProjetoActionTypes.LoadPassosConcluidos;
  constructor(public idProjeto: string) { }
}
export class LoadPassosConcluidosSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadPassosConcluidosSuccess;
  constructor(public passosFluxos: PassoFluxo[]) { }
}
export class LoadPassosConcluidosFail implements Action {
  readonly type = ProjetoActionTypes.LoadPassosConcluidosFail;
  constructor(public error: any) { }
}
export class LoadPassosConcluidosClean implements Action {
  readonly type = ProjetoActionTypes.LoadPassosConcluidosClean;
  constructor() { }
}
export class LoadPassosAguardandoAprovacao implements Action {
  readonly type = ProjetoActionTypes.LoadPassosAguardandoAprovacao;
  constructor(public idProjeto: string, public usuarioId: string) { }
}
export class LoadPassosAguardandoAprovacaoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadPassosAguardandoAprovacaoSuccess;
  constructor(public passosFluxos: PassoFluxo[]) { }
}
export class LoadPassosAguardandoAprovacaoFail implements Action {
  readonly type = ProjetoActionTypes.LoadPassosAguardandoAprovacaoFail;
  constructor(public error: any) { }
}
export class LoadPassosAguardandoAprovacaoClean implements Action {
  readonly type = ProjetoActionTypes.LoadPassosAguardandoAprovacaoClean;
  constructor() { }
}

export class LoadInvestimentoPorId implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentoPorId;
  constructor(public idProjeto: string) { }
}
export class LoadInvestimentoPorIdSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentoPorIdSuccess;
  constructor(public investimento: Investimento) { }
}
export class LoadInvestimentoPorIdFail implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentoPorIdFail;
  constructor(public error: any) { }
}

export class ExecutarPassoProjeto implements Action {
  readonly type = ProjetoActionTypes.ExecutarPassoProjeto;
  constructor(public idPasso: string, public passoFluxoExecucao: PassoFluxoExecucao, public qtdPontos?: number, public filtro?: PassoFluxoExecucaoFiltro, public actionBarRef?: ActionBarRef) { }
}
export class ExecutarPassoProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.ExecutarPassoProjetoSuccess;
  constructor(public resposta: any, public actionBarRef: ActionBarRef, public conditions: { mostrarRecompensa: boolean, isConcluida: boolean } = { mostrarRecompensa: false, isConcluida: false }, public qtdPontos: number, public actions: Action[]) { }
}
export class ExecutarPassoProjetoFail implements Action {
  readonly type = ProjetoActionTypes.ExecutarPassoProjetoFail;
  constructor(public error: any) { }
}

export class AprovarPassoProjeto implements Action {
  readonly type = ProjetoActionTypes.AprovarPassoProjeto;
  constructor(public idPasso: string, public passoFluxoExecucao: PassoFluxoExecucao, public usuarioLogadoId: string) { }
}
export class AprovarPassoProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.AprovarPassoProjetoSuccess;
  constructor(public resposta: any) { }
}
export class AprovarPassoProjetoFail implements Action {
  readonly type = ProjetoActionTypes.AprovarPassoProjetoFail;
  constructor(public error: any) { }
}

export class IsResponsavelProjeto implements Action {
  readonly type = ProjetoActionTypes.IsResponsavelProjeto;
  constructor(public isResponsavel: boolean = false) { }
}

export class LoadTotaisInvestimento implements Action {
  readonly type = ProjetoActionTypes.LoadTotaisInvestimento;
  constructor(public anoVigencia: string) { }
}

export class LoadTotaisInvestimentoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadTotaisInvestimentoSuccess;
  constructor(public totais: TotaisOrcamentoInvestimentoProjeto) { }
}

export class LoadTotaisInvestimentoFail implements Action {
  readonly type = ProjetoActionTypes.LoadTotaisInvestimentoFail;
  constructor(public error: any) { }
}

export class LoadInvestimentosProjeto implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentosProjeto;
  constructor(public anoVigencia: string) { }
}

export class LoadInvestimentosProjetoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentosProjetoSuccess;
  constructor(public investimentosProjeto: OrcamentoInvestimentoProjeto[]) { }
}

export class LoadInvestimentosProjetoFail implements Action {
  readonly type = ProjetoActionTypes.LoadInvestimentosProjetoFail;
  constructor(public error: any) { }
}

export class LoadVigenciasDisponiveis implements Action {
  readonly type = ProjetoActionTypes.LoadVigenciasDisponiveis;
  constructor() { }
}

export class LoadVigenciasDisponiveisSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadVigenciasDisponiveisSuccess;
  constructor(public vigencias: string[]) { }
}

export class LoadVigenciasDisponiveisFail implements Action {
  readonly type = ProjetoActionTypes.LoadVigenciasDisponiveisFail;
  constructor(public error: any) { }
}

export class LoadTotalProjetosSituacao implements Action {
  readonly type = ProjetoActionTypes.LoadTotalProjetosSituacao;
  constructor(public anoVigencia: string) { }
}

export class LoadTotalProjetosSituacaoSuccess implements Action {
  readonly type = ProjetoActionTypes.LoadTotalProjetosSituacaoSuccess;
  constructor(public totalProjetosSituacao: TotalProjetosSituacao) { }
}

export class LoadTotalProjetosSituacaoFail implements Action {
  readonly type = ProjetoActionTypes.LoadTotalProjetosSituacaoFail;
  constructor(public error: any) { }
}

export type ProjetoActions =
  | UpdateProjetoPontual
  | UpdateProjetoPontualSuccess
  | UpdateProjetoPontualFail
  | LoadInvestimentoPorId
  | LoadInvestimentoPorIdSuccess
  | LoadInvestimentoPorIdFail
  | AprovarPassoProjeto
  | AprovarPassoProjetoSuccess
  | AprovarPassoProjetoFail
  | LoadPassosAguardandoAprovacao
  | LoadPassosAguardandoAprovacaoSuccess
  | LoadPassosAguardandoAprovacaoFail
  | LoadPassosAguardandoAprovacaoClean
  | LoadPassosConcluidos
  | LoadPassosConcluidosSuccess
  | LoadPassosConcluidosFail
  | LoadPassosConcluidosClean
  | ExecutarPassoProjeto
  | ExecutarPassoProjetoSuccess
  | ExecutarPassoProjetoFail
  | IsResponsavelProjeto
  | LoadPassosFluxoProjetoClean
  | LoadPassosFluxoProjeto
  | LoadPassosFluxoProjetoSuccess
  | LoadPassosFluxoProjetoFail
  | SelectedMarco
  | LoadConviteIntencaoProjeto
  | LoadConviteIntencaoProjetoSuccess
  | LoadConviteIntencaoProjetoFail
  | CleanProjetoAtual
  | CleanUsuariosProjetos
  | LoadUsuariosProjetos
  | LoadUsuariosProjetosSuccess
  | LoadUsuariosProjetosFail
  | LoadProjetoById
  | LoadProjetoByIdSuccess
  | LoadProjetoByIdFail
  | ListarIndicadores
  | ListarIndicadoresSuccess
  | ListarIndicadoresFail
  | CleanProjetoCadastro
  | LoadProjetos
  | LoadProjetosSuccess
  | LoadProjetosFail
  | TotalSituacaoProjetos
  | TotalSituacaoProjetosSuccess
  | TotalSituacaoProjetosFail
  | SelectedPrograma
  | AddProjeto
  | AddProjetoSuccess
  | AddProjetoFail
  | AddInformacoes
  | AddEscopos
  | UpdateProjeto
  | UpdateProjetoSuccess
  | UpdateProjetoFail
  | DeleteProjeto
  | DeleteProjetoSuccess
  | DeleteProjetoFail
  | ClearProjetos
  | TotalProjetos
  | TotalProjetosSuccess
  | TotalProjetosFail
  | AddCronogramaFinanceiro
  | AddCronogramaFisico
  | AddMetas
  | LoadTotaisInvestimento
  | LoadTotaisInvestimentoSuccess
  | LoadTotaisInvestimentoFail
  | LoadInvestimentosProjeto
  | LoadInvestimentosProjetoSuccess
  | LoadInvestimentosProjetoFail
  | LoadVigenciasDisponiveis
  | LoadVigenciasDisponiveisSuccess
  | LoadVigenciasDisponiveisFail
  | LoadTotalProjetosSituacao
  | LoadTotalProjetosSituacaoSuccess
  | LoadTotalProjetosSituacaoFail;
