import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as fromMarco from '@app/funcionalidades/fluxo-processo/reducers/marco.reducer';
import { FormioModule } from "@app/funcionalidades/formio/formio.module";
import { MenuDropDownModule } from '@app/shared/components/menu-dropdown/menu-dropdown.module';
import { ScChipModule } from '@app/shared/components/sc-chip/sc-chip.module';
import { ScListaRubricaModule } from '@app/shared/components/sc-lista-rubricas/sc-lista-rubricas.module';
import { ScListaUsuariosModule } from '@app/shared/components/sc-lista-usuarios/sc-lista-usuarios.module';
import { AvatarModule } from '@app/shared/components/sins-avatar/sc-avatar.module';
import { SinsDatepickerModule } from '@app/shared/components/sins-datepicker/sins-datepicker.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ScAccordionItemModule } from '@shared/components/sc-accordion-item/sc-accordion-item.module';
import { RadioModule } from '@shared/components/sc-radio/radio.module';
import { SinsBadgeModule } from '@shared/components/sins-badge/sins-badge.module';
import { SharedModule } from '@shared/shared.module';
import { ActionbarModule, FormModule, ModalModule, ProgressBarModule, TabsModule } from '@sicoob/ui';
import { BsDatepickerModule, defineLocale } from 'ngx-bootstrap';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgxCurrencyModule } from 'ngx-currency';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MarcoEffects } from '../fluxo-processo/effects/marco.effects';
import { ActionBarCadastroCronogramaFinanceiroComponent } from './components/actionbar-cadastro-cronograma-financeiro/actionbar-cadastro-cronograma-financeiro.component';
import { ActionBarCadastroCronogramaFisicoComponent } from './components/actionbar-cadastro-cronograma-fisico/actionbar-cadastro-cronograma-fisico.component';
import { ActionbarExecucaoTarefa } from './components/actionbar-execucao-tarefa/actionbar-execucao-tarefa.component';
import { ActionbarFiltroProjetosComponent } from './components/actionbar-filtro-projetos/actionbar-filtro-projetos.component';
import { AvatarPerfilProjetoComponent } from './components/avatar-prefil-projeto/avatar-perfil-projeto.component';
import { CadastroProjetoComponent } from './components/cadastro-projeto/cadastro-projeto.component';
import { CardProjetoComponent } from './components/card-projeto/card-projeto.component';
import { DataPlanejamentoComponent } from './components/data-planejamento/data-planejamento.component';
import { DialogoConcluirCadastroProjeto } from './components/dialogo-concluir-cadastro-projeto/dialogo-concluir-cadastro-projeto.component';
import { DialogoNovoProjetoComponent } from './components/dialogo-novo-projeto/dialogo-novo-projeto.component';
import { FiltroProjetosComponent } from './components/filtro-projetos/filtro-projetos.component';
import { ProjetoComponent } from './components/projeto/projeto.component';
import { StepCronogramasProjetoComponent } from './components/step-cronogramas-projeto/step-cronogramas-projeto.component';
import { StepEscopoProjetoComponent } from './components/step-escopo-projeto/step-escopo-projeto.component';
import { StepFinanceiroProjetoComponent } from './components/step-financeiro-projeto/step-financeiro-projeto.component';
import { StepIndicadoresProjetoComponent } from './components/step-indicadores-projeto/step-indicadores-projeto.component';
import { StepInformacoesProjetoComponent } from './components/step-informacoes-projeto/step-informacoes-projeto.component';
import { ArquivosProjetoComponent } from './components/tabs-projeto/arquivos-projeto/arquivos-projeto.component';
import { DialogoConviteProjeto } from './components/tabs-projeto/dialogo-convite-projeto/dialogo-convite-projeto.component';
import { EntregasProjetoComponent } from './components/tabs-projeto/entregas-projeto/entregas-projeto.component';
import { SobreProjetoComponent } from './components/tabs-projeto/sobre-projeto/sobre-projeto.component';
import { PassoProjetoComponent } from './components/tabs-projeto/suas-tarefas/passos-projeto/passo-projeto.component';
import { SuasTarefasProjetoComponent } from './components/tabs-projeto/suas-tarefas/suas-tarefas.component';
import { TimeProjetoComponent } from './components/tabs-projeto/time-projeto/time-projeto.component';
import { ContainerCadastroProjetosComponent } from './containers/container-cadastro-projetos/container-cadastro-projetos.component';
import { ContainerGerenciaProjetoComponent } from './containers/container-gerencia-projeto/container-gerencia-projeto.component';
import { ContainerProjetoComponent } from './containers/container-projeto/container-projeto.component';
import { ContainerProjetosProgramaComponent } from './containers/container-projetos-programa/container-projetos-programa.component';
import { ContainerRouterProjetoComponent } from './containers/container-router-projeto.component';
import { ProjetoEffects } from './effects/projeto.effects';
import { ProjetoRoutingModule } from './projeto-routing.module';
import * as fromProjeto from './reducers/projeto.reducer';
import { RespostasProjetoComponent } from './components/tabs-projeto/respostas-projeto/respostas-projeto.component';
import { ListaArquivosProjetoComponent } from './components/lista-arquivos-projeto/lista-arquivos-projeto.component';
import { DialogoConcluirPassoProjeto } from './components/dialogo-concluir-passo-projeto/dialogo-concluir-passo-projeto.component';

defineLocale('pt-br', ptBrLocale);


@NgModule({
  declarations: [
    DialogoConcluirPassoProjeto,
    ListaArquivosProjetoComponent,
    PassoProjetoComponent,
    DataPlanejamentoComponent,
    AvatarPerfilProjetoComponent,
    ContainerRouterProjetoComponent,
    ContainerProjetoComponent,
    ProjetoComponent,
    ContainerCadastroProjetosComponent,
    CadastroProjetoComponent,
    FiltroProjetosComponent,
    CardProjetoComponent,
    ActionbarFiltroProjetosComponent,
    DialogoNovoProjetoComponent,
    ContainerProjetosProgramaComponent,
    StepInformacoesProjetoComponent,
    StepIndicadoresProjetoComponent,
    StepEscopoProjetoComponent,
    StepFinanceiroProjetoComponent,
    StepCronogramasProjetoComponent,
    ActionBarCadastroCronogramaFinanceiroComponent,
    ActionBarCadastroCronogramaFisicoComponent,
    DialogoConcluirCadastroProjeto,
    ContainerGerenciaProjetoComponent,
    SobreProjetoComponent,
    EntregasProjetoComponent,
    TimeProjetoComponent,
    ArquivosProjetoComponent,
    SuasTarefasProjetoComponent,
    RespostasProjetoComponent,
    DialogoConviteProjeto,
    ActionbarExecucaoTarefa
  ],
  imports: [
    FormioModule,
    PopoverModule.forRoot(),
    ProgressBarModule,
    AvatarModule,
    SinsDatepickerModule,
    CommonModule,
    NgSelectModule,
    SharedModule,
    ProjetoRoutingModule,
    CdkStepperModule,
    FormModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPermissionsModule.forChild(),
    ScChipModule,
    InfiniteScrollModule,
    ModalModule,
    TabsModule,
    SinsBadgeModule,
    ScAccordionItemModule,
    RadioModule,
    ActionbarModule,
    MenuDropDownModule,
    ScListaUsuariosModule,
    BsDatepickerModule.forRoot(),
    NgxCurrencyModule,
    SinsBadgeModule,
    ScListaRubricaModule, NgxMaskModule,
    StoreModule.forFeature('projeto', fromProjeto.reducer),
    StoreModule.forFeature('marcoState', fromMarco.reducer),
    EffectsModule.forFeature([ProjetoEffects, MarcoEffects])
  ],
  entryComponents: [DialogoConcluirPassoProjeto, ActionbarExecucaoTarefa, DialogoConviteProjeto, ActionbarFiltroProjetosComponent, DialogoNovoProjetoComponent, ActionBarCadastroCronogramaFinanceiroComponent, ActionBarCadastroCronogramaFisicoComponent, DialogoConcluirCadastroProjeto]
})
export class ProjetoModule { }
