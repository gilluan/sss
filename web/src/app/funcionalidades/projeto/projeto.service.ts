import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/orcamento-investimento-projeto.model';
import { TotaisOrcamentoInvestimentoProjeto } from '@app/funcionalidades/projeto/models/totais-orcamento-investimento-projeto.model';
import { TotalProjetosSituacao } from '@app/funcionalidades/projeto/models/total-projetos-situacao.model';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { Resultado } from '@app/shared/models/resultado.model';
import { Service } from '@app/shared/services/service';
import { Validations } from '@app/shared/utils/validations';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Marco } from '../fluxo-processo/models/marco.model';
import { PassoFluxoExecucaoFiltro } from '../fluxo-processo/models/passo-fluxo-execucao.filtro';
import { PassoFluxoExecucao } from '../fluxo-processo/models/passo-fluxo-execucao.model';
import { PassoFluxo } from '../fluxo-processo/models/passo-fluxo.model';
import { ProgramaFiltro } from '../programas/models/programa.filtro';
import { Programa } from '../programas/models/programas.model';
import { TotalSituacaoVm } from '../sins-analise-cadastros/models/dto/total-situacao-vm.model';
import { ConviteIntencao } from './models/convite-intencao.model';
import { Investimento } from './models/investimento.model';
import { ProjetoTotal } from './models/projeto-total.model';
import { ProjetoFiltro } from './models/projeto.filtro';
import { Projeto } from './models/projeto.model';
import { UsuariosProjeto } from './models/usuarios-projetos.model';

const notNull = Validations.notNull;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProjetoService extends Service {

  constructor(private http: HttpClient) {
    super();
  }

  RESOURCE = 'projetos';
  RESOURCE_URL = `${environment.modulo_gestao_projeto}/${this.RESOURCE}`;
  RESOURCE_URL_TAREFAS = `${environment.modulo_gestao_tarefa}/passos-fluxo`;


  carregarMarcosPrograma(id: string): Observable<Marco[]> {
    return this.http.get<Marco[]>(`${environment.modulo_gestao_projeto}/programas/${id}/marcos`).pipe(
      catchError(this.handleError)
    );
  }

  carregarProgramasInfos(filtro?: ProgramaFiltro) {
    let params = this.buildParamsPrograma(filtro);
    params = params.append('info', 'true');
    const url = `${environment.modulo_gestao_projeto}/programas`;
    return this.http.get<{ resultado: PagedDataModelVm }>(url, { params }).pipe(
      map((r) => this.adicionarProgramaOutro(r)),
      catchError(this.handleError)
    );
  }

  private adicionarProgramaOutro(r: { resultado: PagedDataModelVm }) {
    const programaOutro = { nome: 'Outros', _id: null };
    let retorno = [];
    if (r.resultado) {
      retorno = r.resultado.data ? [...r.resultado.data, programaOutro] : [programaOutro];
    }
    return retorno;
  }


  // FIXME Essa chamada precisa ser feita na funcionalidade de Programa
  carregarProgramas(filtro?: ProgramaFiltro): Observable<PagedDataModelVm> {
    const params = this.buildParamsPrograma(filtro);
    const url = `${environment.modulo_gestao_projeto}/programas`;
    return this.http.get<{ resultado: PagedDataModelVm }>(url, { params }).pipe(
      map(r => {
        const array = r.resultado.data as any[];
        const data = array.map((p: any) => {
          const program = Object.assign(new Programa(), p);
          program.id = p.id;
          program.selecionado = false;
          return program;
        });
        const pagedDataModelVm = new PagedDataModelVm(r.resultado.page, data);
        return pagedDataModelVm;
      }),
      catchError(this.handleError)
    );
  }

  // FIXME Essa chamada precisa ser feita na funcionalidade de Programa
  carregarProgramaPorId(id: string): Observable<Programa> {
    return this.http.get<Programa>(`${environment.modulo_gestao_projeto}/programas/${id}/detalhado`).pipe(
      catchError(this.handleError)
    );
  }

  convidarVoluntario(conviteIntencao: ConviteIntencao): Observable<ConviteIntencao> {
    return this.http.post<ConviteIntencao>(`${environment.modulo_gestao_projeto}/convites`, conviteIntencao, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  executarPasso(idPasso: string, passoExecucao: PassoFluxoExecucao): Observable<ConviteIntencao> {
    return this.http.post<any>(`${this.RESOURCE_URL_TAREFAS}/executar/${passoExecucao.projeto}/tarefa/${idPasso}`, passoExecucao, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  carregarConviteIntencao(projeto: string, convidador: string) {
    let params = new HttpParams();
    params = params.append('projeto', projeto);
    params = params.append('convidador', convidador);
    return this.http.get<ConviteIntencao>(`${environment.modulo_gestao_projeto}/convites/intencao`, { params }).pipe(
      catchError(this.handleError)
    );
  }

  carregarUsuariosPorProjeto(id: string): Observable<UsuariosProjeto> {
    return this.http.get<{ resultado: UsuariosProjeto }>(`${this.RESOURCE_URL}/${id}/usuarios`).pipe(
      map((resultado: { resultado: UsuariosProjeto }) => resultado.resultado),
      catchError(this.handleError)
    );
  }

  carregarProjetoPorId(id: string): Observable<Projeto> {
    return this.http.get<Projeto>(`${this.RESOURCE_URL}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  carregarInvestimentoPorId(id: string): Observable<Investimento> {
    return this.http.get<Investimento>(`${this.RESOURCE_URL}/${id}/investimento`).pipe(
      catchError(this.handleError)
    );
  }

  carregarPassosFluxoProjeto(filtro: PassoFluxoExecucaoFiltro): Observable<PassoFluxo[]> {
    const params = this.buildFilter(filtro);
    return this.http.get<{ resultado: PassoFluxo[] }>(`${this.RESOURCE_URL_TAREFAS}/executar/${filtro.idProjeto}`, { params }).pipe(
      map(r => {
        r.resultado.map(pf => {
          if (pf && pf.execucao && pf.execucao.dataFimPlanejada) {
            pf.execucao.dataFimPlanejada = new Date(pf.execucao.dataFimPlanejada);
          }
        });
        return r.resultado;
      }
      ),
      catchError(this.handleError)
    );
  }

  salvar(projeto: Projeto): Observable<Projeto> {
    return this.http.post<Projeto>(this.RESOURCE_URL, projeto, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  editar(projeto: Projeto): Observable<Projeto> {
    return this.http.put<Projeto>(this.RESOURCE_URL, projeto, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  remover(id: string): Observable<Projeto> {
    return this.http.delete<Projeto>(`${this.RESOURCE_URL}/${id}`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  carregar(filtro: ProjetoFiltro): Observable<PagedDataModelVm> {
    const params = this.buildParams(filtro);
    return this.http.get<{ resultado: PagedDataModelVm }>(this.RESOURCE_URL, { params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  carregarTotais(filtro: ProjetoFiltro): Observable<ProjetoTotal> {
    const params = this.buildParams(filtro);
    return this.http.get<{ resultado: ProjetoTotal }>(`${this.RESOURCE_URL}/total-registros`, { params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  carregarTotalSituacao(filtro: ProjetoFiltro): Observable<TotalSituacaoVm[]> {
    const params = this.buildParams(filtro);
    return this.http.get<{ resultado: TotalSituacaoVm[] }>(`${this.RESOURCE_URL}/total-registros-agrupados`, { params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  carregarTotalOrcamentoInvestimento(anoVigencia: string): Observable<TotaisOrcamentoInvestimentoProjeto> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/total-orcamento-investimento/vigencia/${anoVigencia}`).pipe(
      map(r => r.resultado),
      catchError(this.handleError));
  }

  carregarOrcamentoInvestimento(anoVigencia: string): Observable<OrcamentoInvestimentoProjeto[]> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/orcamento-investimento/vigencia/${anoVigencia}`).pipe(
      map(r => r.resultado),
      catchError(this.handleError));
  }

  carregarTotalPorSituacao(anoVigencia: string): Observable<TotalProjetosSituacao> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/total-situacao/vigencia/${anoVigencia}`).pipe(
      map(r => r.resultado),
      catchError(this.handleError));
  }

  carregarVigenciasDisponiveis(): Observable<string[]> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/vigencias`).pipe(
      map(r => r.resultado),
      catchError(this.handleError));
  }

  private buildFilter(filtroExecucao: PassoFluxoExecucaoFiltro) {
    let params = new HttpParams();
    notNull(filtroExecucao.idPasso, (im) => params = params.append('idPasso', im));
    notNull(filtroExecucao.idMarco, (im) => params = params.append('idMarco', im));
    notNull(filtroExecucao.perfil, (im) => params = params.append('perfil', im));
    notNull(filtroExecucao.responsavel, (im) => params = params.append('responsavel', im));
    notNull(filtroExecucao.situacao, (im) => params = params.append('situacao', im));
    notNull(filtroExecucao.aprovador, (im) => params = params.append('aprovador', im));
    notNull(filtroExecucao.executor, (im) => params = params.append('executor', im));
    return params;
  }

  private buildParams(filtro: ProjetoFiltro): HttpParams {
    let params = new HttpParams();
    notNull(filtro.nome, n => params = params.append('nome', n));
    notNull(filtro.programa, n => params = typeof n === 'string' ? params.append('programa', n) : params.append('programa', n.id));
    notNull(filtro.usuarioInstituto, n => params = typeof n === 'string' ? params.append('responsavel', n) : params.append('responsavel', n.id));
    notNull(filtro.estado, n => params = params.append('estado', n));
    notNull(filtro.paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('offset', pn));
      notNull(p.pageSize, ps => params = params.append('limit', ps));
    });
    if (filtro.numeroCooperativa && filtro.numeroCooperativa.length > 0) {
      params = params.append('cooperativas', filtro.numeroCooperativa.join(','));
    }
    return params;
  }

  private buildParamsPrograma(filtro: ProgramaFiltro): HttpParams {
    let params = new HttpParams();
    notNull(filtro.nome, n => params = params.append('nome', n));
    notNull(filtro.vigencia, n => params = params.append('vigencia', n));
    notNull(filtro.ativo, n => params = params.append('ativo', n));
    notNull(filtro.portifolioId, n => params = params.append('identificadorPortifolio', n));
    notNull(filtro.paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('numeroPagina', pn));
      notNull(p.pageSize, ps => params = params.append('tamanhoPagina', ps));
    });
    return params;
  }


}
