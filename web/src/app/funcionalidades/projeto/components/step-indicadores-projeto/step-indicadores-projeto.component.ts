import { Component, DoCheck, EventEmitter, Input, OnDestroy, OnInit, Output, ContentChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Indicador } from '@app/funcionalidades/projeto/models/indicador.model';
import { Meta } from '../../models/meta.model';
import { InputDirective } from '@sicoob/ui';
import { nonZero, isInteger } from '@app/shared/utils/validations';

@Component({
  selector: 'step-indicadores-projeto',
  templateUrl: './step-indicadores-projeto.component.html',
  styleUrls: ['./step-indicadores-projeto.component.scss']
})
export class StepIndicadoresProjetoComponent implements OnInit, DoCheck, OnDestroy {

  constructor(
    private fb: FormBuilder) {
  }

  @Output() completed = new EventEmitter<any>();
  @Output() concluir: EventEmitter<any> = new EventEmitter<any>();
  @Input() indicadores: Indicador[];
  @Input() metasEditar: Meta[];
  metasForm: FormGroup;

  @ContentChild(InputDirective)
  inputDirective: InputDirective;

  onConcluir() {
    this.concluir.emit({ isLast: false, step: 3 });
  }

  ngOnInit() {
    this.carregarForm();
    this.populateMetas()
  }

  ngDoCheck(): void {
    this.completed.emit({ form: this.metasForm })
  }


  private populateMetas() {
    if (this.indicadores && this.indicadores.length > 0) {
      this.indicadores.forEach(f => this.metas.push(this.createTarefaForm(f.id)))
      this.patchForm(this.metasEditar);
    }
  }

  private patchForm(metas: Meta[]) {
    if (metas && metas.length > 0 && this.metas.length > 0) {
      for (let i = 0; i < metas.length; i++) {
        const meta = metas[i];
        this.metas.at(i).patchValue({
          indicador: meta.indicador,
          valor: meta.valor
        });
      }
    }
  }

  private createTarefaForm(id: string, value?: any): FormGroup {
    return this.fb.group({
      indicador: new FormControl(id, Validators.required),
      valor: new FormControl(value, Validators.compose([Validators.required, nonZero, isInteger])),
    });
  }

  private carregarForm() {
    this.metasForm = this.fb.group({
      metas: this.fb.array([], Validators.required)
    });
  }

  get metas() {
    return this.metasForm.get('metas') as FormArray;
  }

  hasSuccess(control: FormControl): boolean {
    return (
      control.valid &&
      this.isCampoFocus() &&
      !this.isWarning() &&
      (control.dirty || control.touched)
    );
  }

  isCampoFocus(): boolean {
    return this.inputDirective && this.inputDirective.focus;
  }

  isWarning(): boolean {
    return (this.inputDirective && this.inputDirective.warn);
  }

  hasError(control: FormControl): boolean {
    return (
      control.invalid &&
      !this.isWarning() &&
      (control.dirty || control.touched)
    );
  }

  hasWarning(): boolean {
    return this.isWarning();
  }

  ngOnDestroy(): void {
  }



}
