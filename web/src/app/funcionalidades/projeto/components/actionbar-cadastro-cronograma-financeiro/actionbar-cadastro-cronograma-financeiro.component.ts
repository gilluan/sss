import { ChangeDetectorRef, Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { AddCronogramaFinanceiro } from '../../actions/projeto.actions';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { FinanceiroTarefa } from '../../models/cronogramas/financeiro-tarefa.type';
import { Observable, Subscription } from 'rxjs';
import { CronogramaFinanceiro } from '../../models/cronogramas/cronograma-financeiro.model';

@Component({
  selector: 'app-actionbar-cadastro-cronograma-financeiro',
  templateUrl: './actionbar-cadastro-cronograma-financeiro.component.html',
  styleUrls: ['./actionbar-cadastro-cronograma-financeiro.component.scss']
})
export class ActionBarCadastroCronogramaFinanceiroComponent implements OnInit, AfterViewInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscription: Subscription = new Subscription();

  constructor(
    private store$: Store<fromProjeto.State>,
    private fb: FormBuilder,
    private changeDetector: ChangeDetectorRef,
    public actionBarRef: ActionBarRef,
    @Inject(ACTIONBAR_DATA) public data: any,
  ) { }

  cronogramaFinanceiro$: Observable<CronogramaFinanceiro>;
  cronogramaFinanceiroTarefas: FinanceiroTarefa[];

  ngAfterViewInit(): void {
    if( this.cronogramaFinanceiroTarefas && this.cronogramaFinanceiroTarefas.length > 0 ) {
      this.investimentoForm.patchValue({ tarefas: this.cronogramaFinanceiroTarefas });
    }
  }

  tarefas$: any =
    [
      { id: "5d55cd057d9ee1e2731c7c36", nome: "Solicitar a impressão da prova do material" },
      { id: "5d55cd224412bc65791ce2d1", nome: "Solicitar a da prova do material impressão" },
      { id: "5d55cd28548e810b55effa51", nome: "Solicitar prova a impressão da  do material" },
      { id: "5d55cd2dcdb69887f327d19a", nome: "A impressão da prova do Solicitar material" },
    ]


  ngOnInit() {
    this.carregarForm();
    this.cronogramaFinanceiro$ = this.store$.pipe(select(fromProjeto.getCronogramaFinanceiro))
    this.subscription.add( this.cronogramaFinanceiro$.subscribe((cronogramaFinanceiro: CronogramaFinanceiro) => {
      if(cronogramaFinanceiro)
        this.cronogramaFinanceiroTarefas = cronogramaFinanceiro.tarefas;
    }));
  }

  investimentoForm: FormGroup;

  createTarefaForm(id: string): FormGroup {
    return this.fb.group({
        id: new FormControl(id, Validators.required),
        rubrica: new FormControl('', Validators.required),
        valor: new FormControl('', Validators.required),
    });
  }

  carregarForm() {
    this.investimentoForm = this.fb.group({
      tarefas: this.fb.array([])
    });
    this.tarefas$.forEach(f => this.tarefas.push(this.createTarefaForm(f.id)));
  }

  get tarefas() {
    return this.investimentoForm.get('tarefas') as FormArray;
  }

  salvar = () => { this.store$.dispatch(new AddCronogramaFinanceiro(this.investimentoForm.value)); this.actionBarRef.close() };

  closeActionBar = () => this.actionBarRef.close({ action: 'fechar' });

}
