import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Observable, Subscription } from 'rxjs';
import { AddCronogramaFisico } from '../../actions/projeto.actions';
import { CronogramaFisico } from '../../models/cronogramas/cronograma-fisico.model';
import { FisicoTarefa } from '../../models/cronogramas/fisico-tarefa.type';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { ProjetoService } from '../../projeto.service';
import { Marco } from '@app/funcionalidades/fluxo-processo/models/marco.model';

@Component({
  selector: 'app-actionbar-cadastro-cronograma-fisico',
  templateUrl: './actionbar-cadastro-cronograma-fisico.component.html',
  styleUrls: ['./actionbar-cadastro-cronograma-fisico.component.scss']
})
export class ActionBarCadastroCronogramaFisicoComponent implements OnInit, AfterViewInit, OnDestroy {

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscription: Subscription = new Subscription();


  constructor(
    private store$: Store<fromProjeto.State>,
    private projetoService: ProjetoService,
    private fb: FormBuilder,
    public actionBarRef: ActionBarRef,
    private localeService: BsLocaleService,
    @Inject(ACTIONBAR_DATA) public data: any,
  ) { }


  cronogramaFisico$: Observable<CronogramaFisico>;
  cronogramaFisicoTarefas: FisicoTarefa[];
  dateInicioProjeto: Date;
  dateControl: Date;
  tarefas$: Observable<Marco[]>;

  ngOnInit() {
    this.localeService.use("pt-br");

    this.cronogramaFisico$ = this.store$.pipe(select(fromProjeto.getCronogramaFisico));

    if (this.data) {
      this.tarefas$ = this.projetoService.carregarMarcosPrograma(this.data.programa);
      this.dateInicioProjeto = this.data.dateInicioProjeto;
      this.dateControl = this.dateInicioProjeto;
    }
    this.subscription.add(this.cronogramaFisico$.subscribe((cronogramaFisico: CronogramaFisico) => {
      if (cronogramaFisico)
        this.cronogramaFisicoTarefas = cronogramaFisico.tarefas;
    }));
    this.carregarForm();
  }

  ngAfterViewInit(): void {
    if (this.cronogramaFisicoTarefas && this.cronogramaFisicoTarefas.length > 0) {
      this.fisicoForm.patchValue({ tarefas: this.cronogramaFisicoTarefas });
    }
  }

  fisicoForm: FormGroup;

  createTarefaForm(id: string, i: number): FormGroup {
    let f = this.fb.group({
      id: new FormControl(id, Validators.required),
      dataFim: new FormControl('', Validators.required),
    });
    i === 0 ? f.enable() : f.disable()
    f.validator = this.compareteDateValidator;
    return f;
  }

  carregarForm() {
    this.fisicoForm = this.fb.group({
      tarefas: this.fb.array([])
    });
    this.subscription
      .add(this.tarefas$.subscribe((marcosArray: Marco[]) => {
        for (let i = 0; i < marcosArray.length; i++) {
          const tarefa = marcosArray[i];
          this.tarefas.push(this.createTarefaForm(tarefa.id, i))
        }
      }));
  }

  get tarefas() {
    return this.fisicoForm.get('tarefas') as FormArray;
  }

  salvar = () => { this.store$.dispatch(new AddCronogramaFisico(this.createCronogramaFisico(), this.dateControl)); this.actionBarRef.close() };

  private createCronogramaFisico = () => {
    let cronogramaFisico = new CronogramaFisico();
    cronogramaFisico.tarefas = (this.fisicoForm.controls['tarefas'] as FormArray).getRawValue();
    return cronogramaFisico;
  }

  closeActionBar = () => { this.actionBarRef.close({ action: 'fechar' }) };

  replanejar = () => {
    this.fisicoForm.reset();
    this.carregarForm();
    this.dateControl = this.dateInicioProjeto;
  }

  private validateValue = () => {
    let index: number;
    for (let i = 0; i < this.tarefas.controls.length; i++) {
      let control: FormControl = this.tarefas.controls[i].get("dataFim") as FormControl;
      if (!control.errors && control.parent.enabled) {
        index = i;
        this.next(control, index);
      }
    }
    return index;
  }

  private next = (control, index) => {
    if (index !== undefined && (index + 1) !== this.tarefas.controls.length) {
      let controlNext: FormControl = this.tarefas.controls[index + 1].get("dataFim") as FormControl;
      if (controlNext && controlNext.disabled) {
        this.dateControl = control.value
        controlNext.parent.enable();
      }
    }
    //control.parent.disable();
  }

  compareteDateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const dataFimControl = control.get('dataFim');
    let dataFim;
    if (dataFimControl && dataFimControl.value) {
      dataFim = dataFimControl.value;
      if (!dataFim)
        return { "dataFimInvalida": true }
      else if (dataFim < this.dateInicioProjeto)
        return { 'dataFimMenor': true }
      else if (dataFim < this.dateControl)
        return { 'dataFimMenorControl': true }
      else if (dataFimControl.enabled) {
        this.validateValue();
      }
    }
    return null;
  };
}
