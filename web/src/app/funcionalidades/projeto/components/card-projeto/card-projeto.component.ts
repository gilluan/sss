import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Projeto } from '../../models/projeto.model';
import { ItemMenu } from '@app/shared/components/menu-dropdown/model/item-menu.model';
import { PositionDotsType } from '@app/shared/components/menu-dropdown/model/position-dots.type';
import { Router } from '@angular/router';
import { MenuDropDownComponent } from '@app/shared/components/menu-dropdown/menu-dropdown.component';
import { PassoFluxoSituacao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-situacao.type';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { ProgramasService } from '@app/funcionalidades/programas/programas.service';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'sc-card-projeto',
  templateUrl: './card-projeto.component.html',
  styleUrls: ['./card-projeto.component.scss']
})
export class CardProjetoComponent implements OnInit, OnDestroy {

  @Input() projeto: Projeto;
  @ViewChild('dropDown') dropDown: MenuDropDownComponent;
  @Output() editar: EventEmitter<Projeto> = new EventEmitter<Projeto>();
  @Output() excluir: EventEmitter<Projeto> = new EventEmitter<Projeto>();
  @Output() navegar: EventEmitter<Projeto> = new EventEmitter<Projeto>();
  imagem: string;

  position: PositionDotsType = PositionDotsType.vertical;
  menus: ItemMenu[] = []
  usuarioLogado: any;
  subscription: Subscription = new Subscription();

  constructor(
    public router: Router,
    private persistanceService: PersistanceService,
    private programaService: ProgramasService,
    private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.usuarioLogado = this.persistanceService.get("usuario_instituto");
    this.montarMenu();
    const imageObservable = this.programaService.getImagem(this.projeto.programa._id);
    this.subscription.add(imageObservable.subscribe((image: string) => this.imagem = image))
  }

  montarMenu = () => {
    if (this.projeto) {
      const responsavel: any = this.projeto.responsavel;
      const criador: any = this.projeto.criador;
      if (responsavel._id === this.usuarioLogado.id || criador === this.usuarioLogado.id) {
        this.menus.push(new ItemMenu(1, 'Editar Projeto'));
        if (!this.projeto.concluirCadastro) {
          this.menus.push(new ItemMenu(2, 'Excluir Projeto'))
        }
      }
      if (this.projeto.concluirCadastro) {
        this.menus.push(new ItemMenu(3, 'Suas tarefas'))
      }
    }
  }

  navegateTo() {
    if (this.projeto.concluirCadastro) {
      this.router.navigate(['projetos/' + this.projeto.id])
    }
  }

  getResponsavel = (projeto) => projeto.responsavel ? projeto.responsavel.nome : '-'

  onAction($event: ItemMenu) {
    this.dropDown.hide();
    switch ($event.id) {
      case 1:
        this.editar.emit(this.projeto);
        break;
      case 2:
        this.excluir.emit(this.projeto);
        break;
      case 3:
        this.navegar.emit(this.projeto);
        break;
      default:
        break;
    }
  }

  getSituacaoClass = (situacao: string) => PassoFluxoSituacao.classesMdi(situacao as PassoFluxoSituacao);

  getSituacaoLabel = (situacao: string) => PassoFluxoSituacao.label(situacao as PassoFluxoSituacao);

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
