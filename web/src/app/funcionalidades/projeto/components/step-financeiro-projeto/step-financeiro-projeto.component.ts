import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'step-financeiro-projeto',
  templateUrl: './step-financeiro-projeto.component.html',
  styleUrls: ['./step-financeiro-projeto.component.scss']
})
export class StepFinanceiroProjetoComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
  ) { }

  @Output() completed = new EventEmitter<any>();
  @Output() concluir: EventEmitter<any> = new EventEmitter<any>();
  editarCondition: boolean = false;
  @Input() orcamento: number;


  financeiroForm: FormGroup;
  informarDepois: boolean;

  ngOnInit() {
    this.carregarForm();
    this.editarCondition = this.orcamento != null && this.orcamento != undefined ? this.patchForm(this.orcamento) : false;
    this.financeiroForm.valueChanges.subscribe((form: FormGroup) => this.completed.emit({ form: this.financeiroForm }))
  }

  onConcluir() {
    this.concluir.emit({ isLast: true, step: 4, editar: this.editarCondition });
  }

  patchForm(orcamento: number) {
    this.financeiroForm.patchValue({
      orcamento
    });
    return true;
  }

  carregarForm() {
    this.financeiroForm = this.fb.group({
      orcamento: ['', Validators.required],
    });
  }

  changeInformarDepois() {
    this.informarDepois = !this.informarDepois;
    if (this.informarDepois) {
      this.financeiroForm.get('orcamento').setValue(0);
    }
  }
}
