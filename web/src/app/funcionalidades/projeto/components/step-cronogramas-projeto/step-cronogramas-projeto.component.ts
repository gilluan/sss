import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { CronogramaFinanceiro } from '../../models/cronogramas/cronograma-financeiro.model';
import { CronogramaFisico } from '../../models/cronogramas/cronograma-fisico.model';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { ActionBarCadastroCronogramaFinanceiroComponent } from '../actionbar-cadastro-cronograma-financeiro/actionbar-cadastro-cronograma-financeiro.component';
import { ActionBarCadastroCronogramaFisicoComponent } from '../actionbar-cadastro-cronograma-fisico/actionbar-cadastro-cronograma-fisico.component';

@Component({
  selector: 'step-cronogramas-projeto',
  templateUrl: './step-cronogramas-projeto.component.html',
  styleUrls: ['./step-cronogramas-projeto.component.scss']
})
export class StepCronogramasProjetoComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();

  cronogramaFisico: Observable<CronogramaFisico>;
  cronogramaFinanceiro: Observable<CronogramaFinanceiro>;

  constructor(
    private store$: Store<fromProjeto.State>,
    private fb: FormBuilder,
    private actionBarService: ActionBarService,
  ) { }

  @Output() completed = new EventEmitter<any>();
  actionBarRefs: ActionBarRef[] = [];
  cronogramaForm: FormGroup;

  cronogramaFisico$: Observable<CronogramaFisico>;
  cronogramaFinanceiro$: Observable<CronogramaFinanceiro>;

  ngOnInit() {
    this.carregarForm();
    this.cronogramaFisico$ = this.store$.pipe(select(fromProjeto.getCronogramaFisico));
    //this.cronogramaFinanceiro$ = this.store$.pipe(select(fromProjeto.getCronogramaFinanceiro));
    this.subscription.add(this.cronogramaFisico$.subscribe((cronogramaFisico: CronogramaFisico) =>  this.cronogramaForm.patchValue({cronogramaFisico})))
    //.add(this.cronogramaFinanceiro$.subscribe((cronogramaFinanceiro: CronogramaFinanceiro) =>  this.cronogramaForm.patchValue({cronogramaFinanceiro})))

  }

  ngDoCheck(): void {
    this.completed.emit({form: this.cronogramaForm});
  }

  carregarForm() {
    this.cronogramaForm = this.fb.group({
      cronogramaFisico: ['', Validators.required],
      //cronogramaFinanceiro: ['', Validators.required]
    });
  }

  addCronogramaFisico() {
    const config: ActionBarConfig = {};
    config.panelClass = 'action-bar-cadastro-cronograma-fisico'
    let actionBarFisico = this.actionBarService.open(ActionBarCadastroCronogramaFisicoComponent, config);
    this.actionBarRefs.push(actionBarFisico);
  }

  addCronogramaFinanceiro() {
    const config: ActionBarConfig = {};
    config.panelClass = 'action-bar-cadastro-cronograma-financeiro'
    let actionBarFinanceiro = this.actionBarService.open(ActionBarCadastroCronogramaFinanceiroComponent, config);
    this.actionBarRefs.push(actionBarFinanceiro);
   }

   ngOnDestroy(): void {
    this.actionBarRefs.forEach(element => {
      if(element) element.close();
    });
    this.subscription.unsubscribe();
  }

}
