import { Component, Inject, OnInit } from '@angular/core';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import { Validations } from '@app/shared/utils/validations';

const notNull = Validations.notNull;

@Component({
  selector: 'app-dialogo-concluir-passo-projeto',
  templateUrl: './dialogo-concluir-passo-projeto.component.html',
  styleUrls: ['./dialogo-concluir-passo-projeto.component.scss']
})
export class DialogoConcluirPassoProjeto implements OnInit {

  qtdPontos: string;

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit(): void {
    notNull(this.data.qtdPontos, (v) => this.qtdPontos = v);
  }

  closeModal = (success: boolean) => this.ref.close(success);
}
