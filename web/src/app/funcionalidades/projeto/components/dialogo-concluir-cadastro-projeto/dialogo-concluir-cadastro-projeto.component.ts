import { Component, Inject, OnInit } from '@angular/core';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';

export enum AcaoProjeto {
  criado = 'criado',
  editado = 'editado'
}

@Component({
  selector: 'app-dialogo-concluir-cadastro-projeto',
  templateUrl: './dialogo-concluir-cadastro-projeto.component.html',
  styleUrls: ['./dialogo-concluir-cadastro-projeto.component.scss']
})
export class DialogoConcluirCadastroProjeto implements OnInit {

  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit(): void {
    this.acao = this.data.acao ? this.data.acao : AcaoProjeto.criado;
  }

  acao: AcaoProjeto;
  closeModal = (success: boolean) => this.ref.close(success);
}
