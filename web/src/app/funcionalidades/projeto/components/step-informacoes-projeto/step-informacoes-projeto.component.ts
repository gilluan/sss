import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Observable, Subscription } from 'rxjs';
import { InformacoesProjeto } from '../../models/wizard/informacoes-projeto.model';

@Component({
  selector: 'step-informacoes-projeto',
  templateUrl: './step-informacoes-projeto.component.html',
  styleUrls: ['./step-informacoes-projeto.component.scss']
})
export class StepInformacoesProjetoComponent implements OnInit, OnDestroy {


  @Output() completed = new EventEmitter<any>();
  @Output() concluir: EventEmitter<any> = new EventEmitter<any>();
  @Input() programaId: string;
  @Input() programa: Observable<Programa>;
  @Input() informacoes: InformacoesProjeto;
  editar: boolean = false;
  infoForm: FormGroup;
  usuarioInstituto: any;
  minDate: Date = new Date((new Date()).getFullYear() + '-01-01T00:00:01');
  maxDate: Date = new Date((new Date()).getFullYear() + '-12-31T23:59:59');
  subscription: Subscription = new Subscription();
  nameTemplate: string = '';

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor(
    private fb: FormBuilder,
    private persistenceService: PersistanceService,
    private localeService: BsLocaleService,
  ) { }


  onConcluir(isLast: boolean) {
    this.concluir.emit({ isLast, step: 1 });
  }

  ngOnInit() {
    this.localeService.use("pt-br");
    this.usuarioInstituto = this.persistenceService.get("usuario_instituto");
    this.carregarForm();
    this.editar = this.informacoes ? this.patchForm(this.informacoes) : false;
    this.subscription
      .add(this.infoForm.valueChanges.subscribe(_ => this.completed.emit({ form: this.infoForm, nomePrograma: this.nameTemplate})))
      .add(this.programa.subscribe((programa: Programa) => programa ? this.formatNameTemplate(programa.nome.toString()) : false))
  }

  formatNameTemplate(nome: string) {
    this.nameTemplate = nome.concat(' - ').concat(new Date().getFullYear().toString());
    if(this.editar) {
      let nomeTemp = this.nome.value.replace(this.nameTemplate.concat(' - '), '');
      this.nome.setValue(nomeTemp);
    }
  }

  patchForm(info: InformacoesProjeto) {
    if(info) {
      this.infoForm.patchValue({
        id: info.id,
        dataCriacao: info.dataCriacao,
        dataModificacao: info.dataModificacao,
        nome: info.nome,
        programa: info.programa,
        dataInicio: info.dataInicio,
        responsavel: info.responsavel,
        justificativa: info.justificativa,
        criador: this.usuarioInstituto.id
      });
    }
    return true;
  }

  carregarForm() {
    this.infoForm = this.fb.group({
      id: [],
      dataCriacao: [],
      dataModificacao: [],
      nome: ['', Validators.required],
      programa: [this.programaId, Validators.required],
      dataInicio: ['', Validators.required],
      responsavel: ['', Validators.required],
      justificativa: ['', Validators.required],
      criador: this.usuarioInstituto.id
    });
  }



  get nome() {
    return this.infoForm.get('nome') as FormControl;
  }


}
