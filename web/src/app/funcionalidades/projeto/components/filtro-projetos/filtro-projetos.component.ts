import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { TabComponent, TabPanelComponent } from "@sicoob/ui";
import { Observable } from "rxjs";
import { TotalSituacaoVm } from "@app/funcionalidades/sins-analise-cadastros/models/dto/total-situacao-vm.model";
import { ProjetoSituacaoType } from "../../models/projeto-situacao.type";

@Component({
  selector: 'filtro-projetos',
  templateUrl: './filtro-projetos.component.html',
  styleUrls: ['./filtro-projetos.component.scss']
})
export class FiltroProjetosComponent implements AfterViewInit {

  @Input() counters$: Observable<TotalSituacaoVm[]>;
  @Output() changeTab: EventEmitter<ProjetoSituacaoType> = new EventEmitter<ProjetoSituacaoType>();

  @ViewChild('tabPanelDashboard') tabPanelDashboard: TabPanelComponent;

  abaSelecionada: TabComponent;

  onTabSelected(tabIndex) {
    this.changeTab.emit(this.getSituacao(tabIndex+1));
  }

  ngAfterViewInit() {
    this.abaSelecionada = this.getTab(ProjetoSituacaoType.todos);
  }

  active(situacao): boolean {
    return this.abaSelecionada && this.abaSelecionada.title == situacao
  }

  getTab = (situacao: ProjetoSituacaoType) => this.tabPanelDashboard.tabs.find(s => s.title == situacao);
  getSituacaoLabel = (situacao: ProjetoSituacaoType) => ProjetoSituacaoType.label(situacao);
  getSituacaoClasse = (situacao: ProjetoSituacaoType) => ProjetoSituacaoType.classesBadge(situacao);
  getIndexSituacao = (situacao: ProjetoSituacaoType) => ProjetoSituacaoType.values().indexOf(situacao) + 1

  mudarAba(situacao: ProjetoSituacaoType) {
    this.abaSelecionada = this.getTab(situacao);
    this.tabPanelDashboard.selectTab(this.abaSelecionada);
  }

  private getSituacao(tabIndex: number): ProjetoSituacaoType {
    if (tabIndex != 0) {
      let i = (tabIndex - 1);
      return ProjetoSituacaoType.values()[i];
    }
  }


}
