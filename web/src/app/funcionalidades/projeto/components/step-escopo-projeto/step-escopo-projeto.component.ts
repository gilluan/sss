import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { EscoposProjeto } from '../../models/wizard/escopos-projeto.model';
import * as fromProjeto from '../../reducers/projeto.reducer';

@Component({
  selector: 'step-escopo-projeto',
  templateUrl: './step-escopo-projeto.component.html',
  styleUrls: ['./step-escopo-projeto.component.scss']
})
export class StepEscopoProjetoComponent implements OnInit, OnDestroy {

  constructor(
    private store$: Store<fromProjeto.State>,
    private fb: FormBuilder) { }

  @Output() completed = new EventEmitter<any>();
  @Output() concluir: EventEmitter<any> = new EventEmitter<any>();
  @Input() isLast: boolean;
  subscription: Subscription = new Subscription();
  escopos$: Observable<EscoposProjeto>;
  escopoForm: FormGroup;
  editar: boolean = false;

  onConcluir() {
    this.concluir.emit({ isLast: this.isLast, step: 2 });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.carregarForm();
    this.escopos$ = this.store$.pipe(select(fromProjeto.getEscopos));
    this.subscription
      .add(this.escopoForm.valueChanges.subscribe(_ => this.completed.emit({ form: this.escopoForm })))
      .add(this.escopos$.subscribe((escopos: EscoposProjeto) => { this.editar = true; this.patchForm(escopos) }))
  }

  patchForm(escopos: EscoposProjeto) {
    if (escopos) {
      this.escopoForm.patchValue({
        escopo: escopos.escopo,
        naoEscopo: escopos.naoEscopo,
      });
    }
  }

  carregarForm() {
    this.escopoForm = this.fb.group({
      escopo: ['', Validators.required],
      naoEscopo: ['', Validators.required]
    });
  }


}
