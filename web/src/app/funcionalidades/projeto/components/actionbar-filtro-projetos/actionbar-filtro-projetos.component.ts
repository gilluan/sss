import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { Observable, Subscription } from "rxjs";
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { UsuarioInstituto } from '@app/shared/models/usuario-instituto.model';

@Component({
  selector: 'app-actionbar-filtro-projetos',
  templateUrl: './actionbar-filtro-projetos.component.html',
  styleUrls: ['./actionbar-filtro-projetos.component.scss']
})
export class ActionbarFiltroProjetosComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();

  cooperativasObservable: Observable<string[]>;
  programasObservable:  Observable<Programa[]>;
  responsavelObservable:  Observable<UsuarioInstituto[]>;

  filtroForm = this.fb.group({
    nomeFiltro: [''],
    cooperativas: [[]],
    programa: [''],
    responsavel: ['']
  });

  constructor(
    public actionBarRef: ActionBarRef,
    private fb: FormBuilder,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  ngOnInit(): void {
    this.filtroForm.setErrors({ required: true });
    this.cooperativasObservable = this.data['cooperativas'];
    this.programasObservable = this.data['programas'];
    this.responsavelObservable = this.data['responsaveis'];
    if (this.data.filtros) {
      this.filtroForm.controls['nomeFiltro'].setValue(this.data.filtros.nome)
      this.filtroForm.controls['cooperativas'].setValue(this.data.filtros.numeroCooperativa)
      this.filtroForm.controls['programa'].setValue(this.data.filtros.programa)
      this.filtroForm.controls['responsavel'].setValue(this.data.filtros.usuarioInstituto)
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  limparFiltros = () => this.filtroForm.reset();


  closeSidebar = () => this.actionBarRef.close({ action: 'fechar' });

  applyFilter = () => this.actionBarRef.close({
    action: 'filtrar',
    data: this.filtroForm.value.cooperativas,
    filtroNome: this.filtroForm.value.nomeFiltro,
    programa: this.filtroForm.value.programa,
    responsavel: this.filtroForm.value.responsavel,
  });

}
