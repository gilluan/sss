import { Component, Inject, OnInit } from "@angular/core";
import { ModalRef, MODAL_DATA } from "@sicoob/ui";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-dialogo-convite-projeto',
  templateUrl: './dialogo-convite-projeto.component.html',
  styleUrls: ['./dialogo-convite-projeto.component.scss']
})
export class DialogoConviteProjeto implements OnInit {

  conviteForm: FormGroup;

  constructor(
    public ref: ModalRef,
    private fb: FormBuilder,
    @Inject(MODAL_DATA) public data: any) { }

  ngOnInit() {
    this.carregarForm();
  }

  closeModal(data?: any) {
    this.ref.close(data);
  }

  onSubmit() {
    this.closeModal(this.conviteForm.value);
  }
  private carregarForm() {
    this.conviteForm = this.fb.group({
      convite: ['Olá estou iniciando um novo projeto, venha participar comigo!', Validators.required]
    });
  }




}
