import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Marco } from '@app/funcionalidades/fluxo-processo/models/marco.model';
import { PassoFluxoExecucaoFiltro } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.filtro';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { ExecutarPassoProjeto, LoadPassosFluxoProjeto, LoadPassosFluxoProjetoClean, LoadUsuariosProjetos, SelectedMarco } from '@app/funcionalidades/projeto/actions/projeto.actions';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarService } from '@sicoob/ui';
import { combineLatest, Observable, Subscription } from 'rxjs';
import * as fromProjeto from '../../../reducers/projeto.reducer';
import { ActionbarExecucaoTarefa } from '../../actionbar-execucao-tarefa/actionbar-execucao-tarefa.component';
import { PerfilType } from '@app/shared/types/perfil.type';
import { Projeto } from '@app/funcionalidades/projeto/models/projeto.model';

@Component({
  selector: 'suas-tarefas',
  templateUrl: './suas-tarefas.component.html',
  styleUrls: ['./suas-tarefas.component.scss'],
})
export class SuasTarefasProjetoComponent implements OnInit, OnDestroy {

  title: string = 'Suas tarefas';
  idProjeto: string;
  idMarco: string;
  subscription: Subscription = new Subscription();
  usuarioLogado: any;
  marco$: Observable<Marco>;
  passos$: Observable<PassoFluxo[]>;
  isResponsavel$: Observable<boolean>;
  projeto$:  Observable<Projeto>;


  constructor(
    private route: ActivatedRoute,
    private store$: Store<fromProjeto.State>,
    private actionBarService: ActionBarService,
    private persistanceService: PersistanceService) { }

  ngOnInit() {
    this.usuarioLogado = this.persistanceService.get('usuario_instituto');
    this.passos$ = this.store$.pipe(select(fromProjeto.getPassos));
    this.marco$ = this.store$.pipe(select(fromProjeto.getMarcoAtual));
    this.isResponsavel$ = this.store$.pipe(select(fromProjeto.isResponsavel));
    this.projeto$ = this.store$.pipe(select(fromProjeto.getProjetoAtual));
    this.subscription
      .add(combineLatest(this.route.parent.paramMap, this.route.parent.queryParamMap).subscribe((observer) => {
        this.idProjeto = observer[0].get('id');
        this.idMarco = observer[1].get('idMarco');
        if (this.idMarco) {
          this.store$.dispatch(new LoadPassosFluxoProjeto(new PassoFluxoExecucaoFiltro(this.idProjeto, null, this.idMarco)));
        } else {
          if(this.usuarioLogado.perfil === PerfilType.voluntario) {
            this.store$.dispatch(new LoadPassosFluxoProjeto(new PassoFluxoExecucaoFiltro(this.idProjeto, null, null, this.usuarioLogado.id)));
          } else {
            this.store$.dispatch(new LoadPassosFluxoProjeto(new PassoFluxoExecucaoFiltro(this.idProjeto, null, null, this.usuarioLogado.id, this.usuarioLogado.perfil)));
          }
        }
        this.store$.dispatch(new LoadUsuariosProjetos(this.idProjeto))
      }))
  }

  getAnoAtual(projeto: Projeto) {
    if(projeto) {
      const ano = typeof projeto.dataInicio === 'string' ? new Date(projeto.dataInicio).getFullYear() : projeto.dataInicio.getFullYear();
      return new Date(`${ano}-12-31T23:23:59`);
    }

  }

  setDataFimPlanejada($event) {
    if ($event) {
      this.store$.dispatch($event);
    }
  }

  setResponsavel($event: PassoFluxo) {
    this.store$.dispatch(new ExecutarPassoProjeto($event.id, $event.execucao));
  }

  setAprovador($event: PassoFluxo) {
    this.store$.dispatch(new ExecutarPassoProjeto($event.id, $event.execucao));
  }

  openPassoActionBar(passo: PassoFluxo, can: boolean, maxDate: Date, minDate: Date, isResponsavel: boolean) {
    if (can) {
      const config: ActionBarConfig = {
        data: {
          passoFluxo: passo,
          idMarco: this.idMarco,
          idProjeto: this.idProjeto,
          maxDate,
          minDate,
          isResponsavel,
        }, hasBackdrop: true
      };
      this.actionBarService.open(ActionbarExecucaoTarefa, config);
    }
  }

  ngOnDestroy(): void {
    this.store$.dispatch(new SelectedMarco(null));
    this.store$.dispatch(new LoadPassosFluxoProjetoClean());
    this.subscription.unsubscribe();
  }


}
