import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { PassoFluxoSituacao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-situacao.type';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { ExecutarPassoProjeto } from '@app/funcionalidades/projeto/actions/projeto.actions';
import { PerfilType } from '@app/shared/types/perfil.type';

@Component({
  selector: 'passo-projeto',
  templateUrl: './passo-projeto.component.html',
  styleUrls: ['./passo-projeto.component.scss'],
  animations: [
    trigger('hover', [
      state('inExec', style({ display: 'block', opacity: 1, height: '*' })),
      state('outExec', style({ display: 'none', opacity: 0, height: '0' })),
      transition('inExec <=> outExec', [animate('0.3s')]),
    ])
  ]
})
export class PassoProjetoComponent {

  @Input() passo: PassoFluxo;
  @Input() usuarioLogado: any;
  @Input() isResponsavel: boolean;
  @Input() dataEntrega: Date;
  @Input() minDate: Date;
  @Input() nomeMarco: string;
  @Output() onResponsavel: EventEmitter<PassoFluxo> = new EventEmitter<PassoFluxo>();
  @Output() onAprovador: EventEmitter<PassoFluxo> = new EventEmitter<PassoFluxo>();
  @Output() onExecutar: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onDataFimPlanejada: EventEmitter<ExecutarPassoProjeto> = new EventEmitter<ExecutarPassoProjeto>();
  stateExec = 'inExec';
  stateInfo = 'outExec';

  @HostListener('mouseenter', ['$event'])
  onHoverEnter(event: MouseEvent) {
    this.stateExec = 'outExec';
    this.stateInfo = 'inExec';
  }

  @HostListener('mouseleave', ['$event'])
  onHoverLeave(event: MouseEvent) {
    this.stateExec = 'inExec';
    this.stateInfo = 'outExec';
  }

  setResponsavel($event) {
    this.passo.execucao.responsavel = $event ? $event.id : null;
    this.onResponsavel.emit(this.passo);
  }

  setAprovador($event) {
    this.passo.execucao.aprovador = $event ? $event.id : null;
    this.onAprovador.emit(this.passo);
  }

  getSituacaoClass = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.classes(situacao);

  getSituacaoLabel = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.label(situacao);

  logicaParaExecutar(perfil: string) {
    switch (perfil) {
      case PerfilType.pae:
        return this.usuarioLogado.perfil === PerfilType.ppe || this.usuarioLogado.perfil === PerfilType.pde || this.usuarioLogado.perfil === PerfilType.pae;
      case PerfilType.pde:
        return this.usuarioLogado.perfil === PerfilType.ppe || this.usuarioLogado.perfil === PerfilType.pde;
      case PerfilType.ppe:
        return this.usuarioLogado.perfil === PerfilType.ppe;
      case PerfilType.voluntario:
        return true
    }
    return false;
  }

  getTextoNaoPodeExecutar(passo: PassoFluxo, isResponsavel: boolean) {
    if (this.depende(passo)) {
      const passoPai = passo.passoDependencia;
      const nomePassoPai = passoPai ? passoPai.titulo : '';
      return `Para executar este passo, primeiro execute o passo <b>${nomePassoPai}</b>`;
    } else if (!this.podeExecutar(passo, isResponsavel)) {
      return `Você não possui permissão para executar este passo`;
    }
  }

  depende(passo: PassoFluxo) {
    if (passo && passo.passoDependencia) {
      const passoPai = passo.passoDependencia;
      if (passoPai && passoPai.execucao) {
        const execArray: any = passoPai.execucao;
        const execucaoPai = execArray.filter(s => s.projeto === passo.execucao.projeto)[0];
        return execucaoPai && execucaoPai.situacao !== PassoFluxoSituacao.concluida;
      }
    }
  }

  openPassoActionBar(passo: PassoFluxo, isResponsavel: boolean) {
    this.onExecutar.emit(this.podeExecutar(passo, isResponsavel) && !this.depende(passo));
  }

  podeExecutar = (passo: PassoFluxo, isResponsavel: boolean) =>
    passo.execucao.responsavel ? this.isResponsavelPasso(passo.execucao.responsavel) : isResponsavel || this.logicaParaExecutar(passo.responsavel);

  isResponsavelPasso(resp: any) {
    return typeof resp === 'object' ? resp._id === this.usuarioLogado.id : resp === this.usuarioLogado.id // o usuario logado é igual que o responsavel
  }

  getPassoDependencia(passoFluxo?: PassoFluxo): string {
    return passoFluxo && passoFluxo.passoDependencia ? `Dependente de '${passoFluxo.passoDependencia.titulo.trim()}'` : null;
  }

  setDataFimPlanejada($event) {
    if ($event) {
      this.onDataFimPlanejada.emit($event)
    }
  }

}
