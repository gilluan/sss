import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Projeto } from "@app/funcionalidades/projeto/models/projeto.model";
import * as fromProjeto from '../../../reducers/projeto.reducer';
import { Store, select } from '@ngrx/store';
import { Programa } from "@app/funcionalidades/programas/models/programas.model";
import { ProjetoService } from "@app/funcionalidades/projeto/projeto.service";
import { Investimento } from "@app/funcionalidades/projeto/models/investimento.model";
import { LoadInvestimentoPorId, UpdateProjetoPontual } from "@app/funcionalidades/projeto/actions/projeto.actions";



@Component({
  selector: 'sobre-projeto',
  templateUrl: './sobre-projeto.component.html',
  styleUrls: ['./sobre-projeto.component.scss']
})
export class SobreProjetoComponent implements OnInit {

  projeto$: Observable<Projeto>;
  programa$: Observable<Programa>;
  isResponsavel$: Observable<boolean>;
  investimento$: Observable<Investimento>
  editMode = false;
  projetoId: string;

  constructor(public store$: Store<fromProjeto.State>, private projetoService: ProjetoService) { }

  ngOnInit(): void {
    this.projeto$ = this.store$.pipe(select(fromProjeto.getProjetoAtual));
    this.isResponsavel$ = this.store$.pipe(select(fromProjeto.isResponsavel));
    this.investimento$ = this.store$.pipe(select(fromProjeto.getInvestimentoAtual));
    this.projeto$.subscribe((projeto: Projeto) => {
      if (projeto) {
        this.projetoId = projeto.id;
        this.programa$ = this.projetoService.carregarProgramaPorId(projeto.programa);
        this.store$.dispatch(new LoadInvestimentoPorId(this.projetoId))
      }
    })
  }

  getImagem(programa: Programa) {
    let src = 'data:image/jpeg;base64,';
    if (programa.imagem) {
      src = `${src}${programa.imagem}`;
    }
    return src;
  }

  mudarOrcamento(projeto: Projeto) {
    this.editMode = false
    this.store$.dispatch(new UpdateProjetoPontual(projeto, [new LoadInvestimentoPorId(this.projetoId)]))
  }

}
