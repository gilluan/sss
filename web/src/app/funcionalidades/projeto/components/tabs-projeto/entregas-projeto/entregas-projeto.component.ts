import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MarcoFiltro } from "@app/funcionalidades/fluxo-processo/models/marco.filtro";
import { Marco } from "@app/funcionalidades/fluxo-processo/models/marco.model";
import { PassoFluxoSituacao } from "@app/funcionalidades/fluxo-processo/models/passo-fluxo-situacao.type";
import * as fromMarco from '@app/funcionalidades/fluxo-processo/reducers/marco.reducer';
import { SelectedMarco } from "@app/funcionalidades/projeto/actions/projeto.actions";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromProjeto from '../../../reducers/projeto.reducer';
import { LoadMarcos, ClearMarcos } from "@app/funcionalidades/fluxo-processo/actions/marco.actions";
import { PageModelVm } from "@app/shared/models/page.model";

@Component({
  selector: 'entregas-projeto',
  templateUrl: './entregas-projeto.component.html',
  styleUrls: ['./entregas-projeto.component.scss']
})
export class EntregasProjetoComponent implements OnInit, OnDestroy {

  marcos$: Observable<Marco[]>;
  idProjeto: string;
  paginacao: PageModelVm = new PageModelVm(0, 8, 0);

  constructor(private router: Router, public store$: Store<fromProjeto.State>, private storeMarco$: Store<fromMarco.State>, private route: ActivatedRoute) { }

  ngOnInit() {
    const marcoFiltroObservable = this.storeMarco$.pipe(select(fromMarco.getFiltroAtual));
    marcoFiltroObservable.subscribe((filtro: MarcoFiltro) => this.paginacao = { ...filtro.paginacao, pageSize: 8 })
    this.route.parent.paramMap.subscribe((params) => {
      this.idProjeto = params.get('id');
      this.loadMarcos();
    })
    this.marcos$ = this.storeMarco$.pipe(select(fromMarco.getMarcos));
  }

  detalharMarco(marco: Marco) {
    this.store$.dispatch(new SelectedMarco(marco));
    this.router.navigate([`./tarefas`], { queryParams: { idMarco: marco.id }, relativeTo: this.route });
  }

  onScroll() {
    if (this.paginacao.pageNumber < this.paginacao.totalPages) {
      this.paginacao.pageNumber++;
      this.loadMarcos();
    }
  }

  getSituacaoClass = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.classes(situacao);

  getSituacaoLabel = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.label(situacao);

  private loadMarcos = () => this.storeMarco$.dispatch(new LoadMarcos(new MarcoFiltro(null, null, null, this.idProjeto, null, this.paginacao)));

  getPercentualTarefasMarco(marco: Marco): {percentual: number, concluidas: number, total: number} {
    if (marco.passosFluxo) {
      const concluidas = marco.passosFluxo.filter((p) => p.execucao.situacao === PassoFluxoSituacao.concluida);
      return {percentual: Math.round((concluidas.length * 100) / marco.passosFluxo.length), concluidas: concluidas.length, total: marco.passosFluxo.length};
    }
    return {percentual: 0, concluidas: 0, total: 0};
  }

  ngOnDestroy(): void {
    this.storeMarco$.dispatch(new ClearMarcos());
  }

}
