import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { PassoFluxo } from "@app/funcionalidades/fluxo-processo/models/passo-fluxo.model";
import { LoadPassosAguardandoAprovacao, LoadPassosAguardandoAprovacaoClean, LoadPassosConcluidos, LoadPassosConcluidosClean } from "@app/funcionalidades/projeto/actions/projeto.actions";
import { PersistanceService } from "@app/shared/services/persistence.service";
import { select, Store } from "@ngrx/store";
import { ActionBarConfig, ActionBarService } from "@sicoob/ui";
import { Observable, Subscription, forkJoin } from "rxjs";
import * as fromProjeto from '../../../reducers/projeto.reducer';
import { ActionbarExecucaoTarefa } from "../../actionbar-execucao-tarefa/actionbar-execucao-tarefa.component";
import { PerfilType } from "@app/shared/types/perfil.type";
import { combineLatest } from "rxjs/operators";

@Component({
  selector: 'respostas-projeto',
  templateUrl: './respostas-projeto.component.html',
  styleUrls: ['./respostas-projeto.component.scss'],
})
export class RespostasProjetoComponent implements OnInit, OnDestroy {


  passos$: Observable<PassoFluxo[]>;
  passosAguardando$: Observable<PassoFluxo[]>;
  usuarioLogado: any;
  idProjeto: string;
  subscription: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private store$: Store<fromProjeto.State>,
    private actionBarService: ActionBarService,
    private persistanceService: PersistanceService) { }

  ngOnInit(): void {
    this.usuarioLogado = this.persistanceService.get('usuario_instituto');
    this.passos$ = this.store$.pipe(select(fromProjeto.getPassosConcluidos))
    this.passosAguardando$ = this.store$.pipe(select(fromProjeto.getPassosAguardandoAprovacao))
    this.route.parent.paramMap.subscribe((params: ParamMap) => {
      this.idProjeto = params.get('id');
      this.store$.pipe(select(fromProjeto.isResponsavel)).subscribe((isRes: boolean) => {
        this.store$.dispatch(new LoadPassosConcluidos(this.idProjeto))
        this.store$.dispatch(new LoadPassosAguardandoAprovacao(this.idProjeto, isRes ? null : this.usuarioLogado.id))
      })
    });
  }

  ngOnDestroy(): void {
    this.store$.dispatch(new LoadPassosAguardandoAprovacaoClean())
    this.store$.dispatch(new LoadPassosConcluidosClean())
  }

  getNomeExecutor(passo: PassoFluxo) {
    const execucao: any = passo.execucao;
    if (execucao && execucao.executor) {
      return execucao.executor.nome;
    }
    return null;
  }

  openPassoActionBar(passo: PassoFluxo, aprovar: boolean = false) {
    const config: ActionBarConfig = {
      data: {
        passoFluxo: passo,
        idMarco: passo.idMarco,
        idProjeto: this.idProjeto,
        isResponsavel: false,
        visualizar: true,
        aprovar,
      }, hasBackdrop: true
    };
    this.actionBarService.open(ActionbarExecucaoTarefa, config);
  }
}
