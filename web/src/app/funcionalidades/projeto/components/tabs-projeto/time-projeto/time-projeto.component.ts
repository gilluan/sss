import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { CleanUsuariosProjetos, ConvidarVoluntariosProjetos, LoadConviteIntencaoProjeto, LoadUsuariosProjetos } from "@app/funcionalidades/projeto/actions/projeto.actions";
import { ConviteIntencao } from "@app/funcionalidades/projeto/models/convite-intencao.model";
import { UsuariosProjeto } from "@app/funcionalidades/projeto/models/usuarios-projetos.model";
import { PersistanceService } from "@app/shared/services/persistence.service";
import { select, Store } from '@ngrx/store';
import { ModalRef, ModalService } from "@sicoob/ui";
import { Observable, Subscription } from "rxjs";
import * as fromProjeto from '../../../reducers/projeto.reducer';
import { DialogoConviteProjeto } from '../dialogo-convite-projeto/dialogo-convite-projeto.component';
import { PerfilType } from "@app/shared/types/perfil.type";

@Component({
  selector: 'time-projeto',
  templateUrl: './time-projeto.component.html',
  styleUrls: ['./time-projeto.component.scss']
})
export class TimeProjetoComponent implements OnInit, OnDestroy {

  idProjeto: string;
  usuarios$: Observable<UsuariosProjeto>;
  conviteIntencao$: Observable<ConviteIntencao>;
  usuarioInstituto: any;
  modalRefs: ModalRef[] = new Array<ModalRef>();
  subscription: Subscription = new Subscription();
  isResponsavel$: Observable<boolean>;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public store$: Store<fromProjeto.State>,
    private readonly modalService: ModalService,
    private persistanceService: PersistanceService) { }

  ngOnInit(): void {
    this.usuarioInstituto = this.persistanceService.get('usuario_instituto');
    this.isResponsavel$ = this.store$.pipe(select(fromProjeto.isResponsavel));
    this.route.parent.paramMap.subscribe(params => {
      this.idProjeto = params.get("id");
      this.store$.dispatch(new LoadUsuariosProjetos(this.idProjeto))
      this.store$.dispatch(new LoadConviteIntencaoProjeto(this.idProjeto, this.usuarioInstituto.id))
    })
    this.usuarios$ = this.store$.pipe(select(fromProjeto.getUsuarios));
    this.conviteIntencao$ = this.store$.pipe(select(fromProjeto.getConviteIntencao));
  }

  abrirDialogoConvite() {
    let modalRef = this.modalService.open(DialogoConviteProjeto, {});
    this.subscription.add(modalRef.afterClosed().subscribe((data: any) =>
      data ? this.convidar(data.convite) : false
    ));
  }

  irParaPerfil(idVoluntario: string, perfil: string) {
    if (this.usuarioInstituto.perfil !== PerfilType.voluntario) {
      this.router.navigate(['/perfil-usuario', idVoluntario], { queryParams: { perfil } });
    }
  }

  convidar(texto: string): void {
    let intencaoConvite = new ConviteIntencao();
    intencaoConvite.convidador = this.usuarioInstituto.id;
    intencaoConvite.enviado = false;
    intencaoConvite.projeto = this.idProjeto;
    intencaoConvite.texto = texto;
    this.store$.dispatch(new ConvidarVoluntariosProjetos(intencaoConvite));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.modalRefs.forEach(s => s.close());
    this.store$.dispatch(new CleanUsuariosProjetos());
  }



}
