import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ProgramaFiltro } from '@app/funcionalidades/programas/models/programa.filtro';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { ModalRef, MODAL_DATA } from '@sicoob/ui';
import { Subscription } from 'rxjs';
import { ProjetoService } from '../../projeto.service';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';


@Component({
  selector: 'app-dialogo-novo-projeto',
  templateUrl: './dialogo-novo-projeto.component.html',
  styleUrls: ['./dialogo-novo-projeto.component.scss']
})
export class DialogoNovoProjetoComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();
  programas: Programa[] = [];
  programaSelecionado: Programa;
  title: string = 'Selecione abaixo o programa que deseja vincular ao seu projeto';
  paginacao = new PageModelVm(0, 4, 0);
  numeroPagina: number = 0;
  constructor(
    public ref: ModalRef,
    @Inject(MODAL_DATA) public data: any,
    private projetoService: ProjetoService,
  ) {
  }

  ngOnInit() {
    this.carregarProgramas();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closeModal() {
    this.ref.close();
  }

  selecionarPrograma(programa: Programa) {
    programa.selecionado = !programa.selecionado;
    if (programa.selecionado) {
      this.programas.forEach(m => m.selecionado = m.id === programa.id);
      this.programaSelecionado = programa;
    } else {
      this.programaSelecionado = null;
    }
  }

  maisProgramas() {
    this.numeroPagina++;
    this.paginacao = new PageModelVm(this.numeroPagina, this.paginacao.pageSize, 0);
    this.carregarProgramas();
  }

  carregarProgramas() {
    let programas$ = this.projetoService.carregarProgramas(new ProgramaFiltro(this.paginacao));
    this.subscription.add(programas$.subscribe((pagedDataModelVm: PagedDataModelVm) => {
      this.paginacao = pagedDataModelVm.page;
      this.programas = [...this.programas, ...pagedDataModelVm.data]
    }));
  }

  onSubmit() {
    this.ref.close({ programa: this.programaSelecionado.id });
  }

  getImage(programa: Programa) {
    let src = 'data:image/jpeg;base64,';
    src = programa.imagem ? `${src}${programa.imagem}` : 'assets/images/empty-state-imagem-programas.svg';
    return src;
  }

}
