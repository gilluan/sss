import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { PassoFluxo } from "@app/funcionalidades/fluxo-processo/models/passo-fluxo.model";
import { PerfilType } from "@app/shared/types/perfil.type";
import { NgSelectComponent } from "@ng-select/ng-select";
import { select, Store } from "@ngrx/store";
import { UsuariosProjeto } from "../../models/usuarios-projetos.model";
import * as fromProjeto from '../../reducers/projeto.reducer';


type ListaUsuario = {
  perfilType: string,
  perfil: string,
  nome: string,
  id: string,
}

export enum TipoUsuario {
  responsavel = 'responsavel',
  aprovador = 'aprovador'
}

export namespace TipoUsuario {
  export function getLabel(value: TipoUsuario): string {
    switch (value) {
      case TipoUsuario.responsavel:
        return "Responsável";
      case TipoUsuario.aprovador:
        return "Aprovador";
      default:
        break;
    }
  }
}

@Component({
  selector: 'avatar-perfil-projeto',
  templateUrl: './avatar-perfil-projeto.component.html',
  styleUrls: ['./avatar-perfil-projeto.component.scss']
})
export class AvatarPerfilProjetoComponent implements OnInit {


  @ViewChild('selectResp') selectResp: NgSelectComponent;
  @Input() isResponsavel: boolean;
  @Input() passo: PassoFluxo;
  @Input() tipo: TipoUsuario;
  @Output() addResponsavel = new EventEmitter<any>();
  usuarios = [];
  usuariosTrabalho: ListaUsuario[] = [];
  responsavel: ListaUsuario;
  perfil: string;
  showComponent: boolean;
  constructor(private store$: Store<fromProjeto.State>, private changeDetection: ChangeDetectorRef) { }

  getLabel = (value: TipoUsuario) => TipoUsuario.getLabel(value);

  popoverResponsavel() {
    return this.isResponsavel ? `Selecione o ${this.tipo} (${PerfilType.obterPerfil(this.perfil)})` : PerfilType.obterPerfil(this.perfil);
  }

  ngOnInit(): void {
    this.perfil = this.tipo === TipoUsuario.responsavel ? this.passo.responsavel : this.passo.perfilAprovador;
    this.showComponent = this.tipo === TipoUsuario.responsavel && !!this.passo.responsavel || this.tipo === TipoUsuario.aprovador && !!this.passo.perfilAprovador;
    this.initResponsavel();
    this.store$.pipe(select(fromProjeto.getUsuarios)).subscribe((usuarariosProjeto: UsuariosProjeto) => {
      if (usuarariosProjeto) {
        this.usuarios = [];
        usuarariosProjeto.ppes ? usuarariosProjeto.ppes.forEach(ppe => this.usuarios.push({ perfilType: 'ppe', perfil: 'PPE\'s', nome: ppe.nome, id: ppe._id })) : false
        usuarariosProjeto.paes ? usuarariosProjeto.paes.forEach(paes => this.usuarios.push({ perfilType: 'pae', perfil: 'PAE\'s', nome: paes.nome, id: paes._id })) : false
        usuarariosProjeto.pdes ? usuarariosProjeto.pdes.forEach(pdes => this.usuarios.push({ perfilType: 'pde', perfil: 'PDE\'s', nome: pdes.nome, id: pdes._id })) : false
        usuarariosProjeto.voluntarios ? usuarariosProjeto.voluntarios.forEach(voluntarios => this.usuarios.push({ perfilType: 'voluntario', perfil: 'Voluntários', nome: voluntarios.nome, id: voluntarios._id })) : false
        this.inicializarUsuarios();
      }
    })
  }

  ngAfterContentChecked(): void {
    this.selectResp ? this.selectResp.open() : null;
  }

  initResponsavel() {
    if (this.tipo && this.passo) {
      let responsavel: any = this.tipo === TipoUsuario.responsavel ? this.passo.execucao.responsavel : this.passo.execucao.aprovador;
      this.responsavel = this.montarUsuario(responsavel);
    }
  }

  montarUsuario(responsavel) {
    if (responsavel) {
      return {
        perfilType: responsavel.perfil, perfil: PerfilType.obterPerfil(this.passo.responsavel), nome: responsavel.nome, id: responsavel._id
      }
    }
    return null;
  }

  inicializarUsuarios() {
    this.usuariosTrabalho = this.usuarios;
  }

  setResponsavel($event, component: any) {
    component.close();
    this.changeDetection.detectChanges();
    this.responsavel = $event;
    this.addResponsavel.emit($event);
  }

  logicaParaMostrarPerfil() {
    switch (this.perfil) {
      case PerfilType.pae:
        this.usuariosTrabalho = this.usuarios.filter(s => s.perfilType === PerfilType.ppe || s.perfilType === PerfilType.pde || s.perfilType === PerfilType.pae)
        break;
      case PerfilType.pde:
        this.usuariosTrabalho = this.usuarios.filter(s => s.perfilType === PerfilType.ppe || s.perfilType === PerfilType.pde)
        break;
      case PerfilType.ppe:
        this.usuariosTrabalho = this.usuarios.filter(s => s.perfilType === PerfilType.ppe)
        break;
    }
  }




}
