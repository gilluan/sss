import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { PassoFluxo } from "@app/funcionalidades/fluxo-processo/models/passo-fluxo.model";
import { Store } from "@ngrx/store";
import { ExecutarPassoProjeto } from "../../actions/projeto.actions";
import * as fromProjeto from '../../reducers/projeto.reducer';
import { BsLocaleService } from "ngx-bootstrap";


@Component({
  selector: 'data-planejamento',
  templateUrl: './data-planejamento.component.html',
  styleUrls: ['./data-planejamento.component.scss']
})
export class DataPlanejamentoComponent implements OnInit {

  @Input() isResponsavel: boolean;
  @Input() passo: PassoFluxo;
  @Input() maxDate;
  @Input() minDate;
  @Output() addDate = new EventEmitter<ExecutarPassoProjeto>();

  constructor(private localeService: BsLocaleService) { }

  ngOnInit(): void {
    this.localeService.use("pt-br");
    this.maxDate = this.tratarData(this.maxDate);
    this.minDate = this.tratarData(this.minDate);
  }

  setDataFimPlanejada($event, passoFluxo: PassoFluxo) {
    if ($event && passoFluxo && passoFluxo.id) {
      this.addDate.emit(new ExecutarPassoProjeto(passoFluxo.id, { ...passoFluxo.execucao, dataFimPlanejada: $event }))
    }
  }

  private tratarData(date: any) {
    if (date) {
      return typeof date === 'string' ? new Date(date) : date;
    }
    return null;
  }



}
