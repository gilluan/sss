import { ChangeDetectorRef, Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { PassoFluxoExecucao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.model';
import { PassoFluxoSituacao } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-situacao.type';
import { PassoFluxo } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { Store, select } from '@ngrx/store';
import { Validations } from '@shared/utils/validations';
import { ActionBarRef, ACTIONBAR_DATA } from '@sicoob/ui';
import { ExecutarPassoProjeto, AprovarPassoProjeto } from '../../actions/projeto.actions';
import * as fromProjeto from '../../reducers/projeto.reducer';
import { DocumentoEnvio } from '@app/shared/ged/models/envio/documentoEnvio.model';
import { FileUtil } from '@app/shared/utils/file.util';
import { GedService } from '@app/shared/ged/ged.service';
import { TipoDocumentoGed } from '@app/shared/ged/models/tipo-arquivo-ged.enum';
import * as fromGed from '@app/reducers/ged.reducer';
import { MultiUploadFile } from '@app/shared/models/multiupload-file.model';
import { SinsFileInProgressStates } from '@app/shared/components/sins-multiupload/sins-file-system-file-entry.model';
import { SalvarArquivosMultiUpload, ExecutarPassoArquivo, CarregarArquivo, LimparStoreGed } from '@app/actions/ged.actions';
import { PassoFluxoExecucaoFiltro } from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-execucao.filtro';
import { Observable } from 'rxjs';
import { DadosDocumento } from '@app/shared/ged/models/retorno/dadosDocumento.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { PerfilType } from '@app/shared/types/perfil.type';

const notNull = Validations.notNull;
export const TYPE_UPLOAD_FILE = 'uploadarquivo';


export type InputFile = {
  storage: string,
  name: string,
  size: number,
  type: string,
  originalName: string,
  url: string,
}

@Component({
  selector: 'sc-actionbar-execucao-tarefa',
  templateUrl: './actionbar-execucao-tarefa.component.html',
  styleUrls: ['./actionbar-execucao-tarefa.component.scss', '../../../../shared/components/sins-datepicker/sins-datepicker.component.scss']
})
export class ActionbarExecucaoTarefa implements OnInit, OnDestroy {


  passoFluxo: PassoFluxo;
  idMarco: string;
  idProjeto: string;
  passoForm: FormGroup;
  usuarioLogado: any;
  isResponsavel: boolean;
  visualizar: boolean = false;
  aprovar: boolean = false;
  maxDate: Date;
  minDate: Date;
  motivoRejeicao: string;
  filtro: PassoFluxoExecucaoFiltro = null;
  multiUploadFileProgress$: Observable<MultiUploadFile[]>;
  filesForm: MultiUploadFile[] = [];
  formulario: any;

  constructor(
    protected readonly changeDetector: ChangeDetectorRef,
    public actionBarRef: ActionBarRef,
    @Inject(ACTIONBAR_DATA) public data: any,
    private fb: FormBuilder,
    private store$: Store<fromProjeto.State>,
    private persistanceService: PersistanceService,
    private gedService: GedService,
    private gedStore$: Store<fromGed.State>,
    private localeService: BsLocaleService, ) { }


  ngOnInit(): void {
    this.localeService.use("pt-br");
    this.usuarioLogado = this.persistanceService.get('usuario_instituto');
    notNull(this.data['aprovar'], (pf) => this.aprovar = pf);
    notNull(this.data['isResponsavel'], (pf) => this.isResponsavel = pf);
    notNull(this.data['visualizar'], (pf) => this.visualizar = pf);
    notNull(this.data['passoFluxo'], (pf) => this.passoFluxo = pf);
    notNull(this.data['idMarco'], (m) => this.idMarco = m);
    notNull(this.data['idProjeto'], (p) => this.idProjeto = p);
    notNull(this.data['maxDate'], (p) => this.maxDate = this.tratarData(p));
    notNull(this.data['minDate'], (p) => this.minDate = this.tratarData(p));
    this.buildForm();
    if (this.idMarco) {
      this.filtro = new PassoFluxoExecucaoFiltro(this.idProjeto, null, this.idMarco)
    } else {
      this.filtro = this.usuarioLogado.perfil === PerfilType.voluntario ? new PassoFluxoExecucaoFiltro(this.idProjeto, null, null, this.usuarioLogado.id) : new PassoFluxoExecucaoFiltro(this.idProjeto, null, null, this.usuarioLogado.id, this.usuarioLogado.perfil);
    }
    this.passoForm.get('dataInicioExec').valueChanges.subscribe((value) => !value ? this.passoForm.get('dataFimExec').setValue(value) : null);
    this.gedStore$.pipe(select(fromGed.getFileUploadMultiple)).subscribe((multpleFiles: MultiUploadFile[]) => {
      if (multpleFiles && multpleFiles.length > 0) {
        this.filesForm = multpleFiles;
      }
    })
    this.multiUploadFileProgress$ = this.gedStore$.pipe(select(fromGed.getFileUploadMultipleProgresso));
    this.getFilesForm();
    this.setFormulario();
  }

  getInfofromForm() {
    if (this.passoForm.valid) {
      this.passoFluxo.execucao.dataInicioExec = this.passoForm.value.dataInicioExec
      this.passoFluxo.execucao.dataFimExec = this.passoForm.value.dataFimExec
      this.passoFluxo.execucao.horasVoluntario = this.passoForm.value.horasVoluntario
      this.passoFluxo.execucao.investimento = this.passoForm.value.investimento
    }
    return this.passoForm.valid;
  }

  enviarRespostas() {
    if (this.getInfofromForm()) {
      this.setSituacao();
      this.dispatchExecutar();
    }
  }

  setResponsavel($event: any) {
    if ($event) {
      if ($event.id != this.passoFluxo.execucao.responsavel) {
        this.passoFluxo.execucao.responsavel = $event.id;
      }
    } else {
      this.passoFluxo.execucao.responsavel = null;
    }
  }

  setAprovador($event) {
    if ($event) {
      if ($event.id != this.passoFluxo.execucao.aprovador) {
        this.passoFluxo.execucao.aprovador = $event.id;
      }
    } else {
      this.passoFluxo.execucao.aprovador = null;
    }
  }

  setDataFimPlanejada($event: ExecutarPassoProjeto) {
    if ($event) {
      this.passoFluxo.execucao.dataFimPlanejada = $event.passoFluxoExecucao.dataFimPlanejada;
    }
  }

  dispatchExecutar() {
    this.passoFluxo.execucao.executor = this.usuarioLogado.id;
    this.passoFluxo.execucao.responsavel = this.usuarioLogado.id;
    this.store$.dispatch(new ExecutarPassoProjeto(this.passoFluxo.id, this.passoFluxo.execucao, this.passoFluxo.pontuacao, this.filtro, this.actionBarRef))
  }

  setFormulario() {
    this.formulario = this.passoFluxo.formulario;
    const respostaForm = this.passoFluxo.execucao.respostaForm;
    if (this.formulario && respostaForm) {
      for (let i = 0; i < this.formulario.components.length; i++) {
        let component = this.formulario.components[i];
        const key = component.key;
        const type = component.type;
        Object.keys(respostaForm).forEach(keyReposta => {
          if (keyReposta === key) {
            if (type === TYPE_UPLOAD_FILE) {
              component['defaultValue'] = this.setFileInput(respostaForm[key].value);
            } else {
              component['defaultValue'] = respostaForm[key].value
            }
          }
        });
      }
    }
  }

  setFileInput(resposta: any[]) {
    let retorno: InputFile[] = [];
    if (resposta && resposta.length > 0) {
      for (let i = 0; i < resposta.length; i++) {
        const file = resposta[i];
        let inputFile: InputFile = {
          storage: 'base64',
          name: file.nome,
          size: file.tamanho,
          type: file.contentType,
          originalName: file.nome,
          url: '',
        }
        retorno.push(inputFile);
      }
    }
    return retorno;
  }

  getFilesForm() {
    const respostaForm = this.passoFluxo.execucao.respostaForm;
    if (respostaForm) {
      const keyFiles = Object.keys(respostaForm).filter((key) => respostaForm[key].type === TYPE_UPLOAD_FILE);
      let files = []
      for (let i = 0; i < keyFiles.length; i++) {
        const key = keyFiles[i];
        files.push([...respostaForm[key].value]);
      }
      for (let i = 0; i < files.length; i++) {
        const array = files[i];
        array.forEach(f => {
          this.filesForm.push(new MultiUploadFile(f.nome, f.tamanho, f.identificadorGed, 100, SinsFileInProgressStates.Finalizado));
        });
      }
    }
    this.changeDetector.detectChanges();
  }

  async submitFormulario($event) {
    const camposUpload: any[] = this.passoFluxo.formulario.components.filter(element => element.type === TYPE_UPLOAD_FILE);
    const envioArquivo = await this.gerarDocumentosEnvio(camposUpload, $event);
    if (!envioArquivo) {
      let resposta = {};
      if ($event && $event.data) {
        resposta = this.getRespostaForm($event);
        if (JSON.stringify(resposta) !== JSON.stringify({})) {
          this.passoFluxo.execucao.respostaForm = resposta;

        }
      }
      this.enviarRespostas();
    }
  }

  private getRespostaForm($event) {
    let resposta = {};
    const formulario = this.passoFluxo.formulario;
    const respostaForm = this.passoFluxo.execucao.respostaForm;
    if ($event && $event.data) {
      delete $event.data.submit;
      Object.keys($event.data).forEach(function (key) {
        if ($event.data[key]) {
          const component = formulario.components.filter(element => element.key === key);
          const type = component.length > 0 ? component[0].type : '';
          let value = null;
          if (TYPE_UPLOAD_FILE === type && respostaForm && respostaForm[key]) {
            value = respostaForm[key].value
          } else {
            value = $event.data[key]
          }
          resposta[key] = { value, type };
        }
      })
    }
    return resposta;
  }

  private async gerarDocumentosEnvio(camposUpload: any[], $event): Promise<boolean> {
    let retorno: boolean = false;
    if (camposUpload && camposUpload.length > 0) {
      let output: Array<MultiUploadFile> = [];
      let documentosEnvio: DocumentoEnvio[] = [];
      for (let i = 0; i < camposUpload.length; i++) {
        const campo = camposUpload[i];
        const data = $event.data[campo.key];
        for (let j = 0; j < data.length; j++) {
          const file = data[j];
          if (file.url) {
            const documentoEnvio: DocumentoEnvio = await this.gerarDocumentoEnvio(file.url, file.storage);
            output.push({
              nome: file.originalName,
              tamanho: file.size,
              identificadorGed: null,
              progress: 0,
              key: campo.key,
              contentType: file.type,
              estado: SinsFileInProgressStates.NaoIniciado,
              error: false,
            });
            documentosEnvio.push(documentoEnvio);
          }
        }
        if (documentosEnvio.length > 0) {
          this.passoFluxo.execucao.executor = this.usuarioLogado.id;
          this.passoFluxo.execucao.responsavel = this.usuarioLogado.id;
          this.passoFluxo.execucao.respostaForm = this.getRespostaForm($event);
          this.getInfofromForm();
          this.setSituacao();
          this.gedStore$.dispatch(new ExecutarPassoArquivo(documentosEnvio, output, this.passoFluxo, this.filtro, this.actionBarRef));
          retorno = true;
        }
      }
    }
    return retorno;
  }

  private async gerarDocumentoEnvio(data, storage): Promise<DocumentoEnvio> {
    try {
      let fileBinario: Int8Array = data;
      if (storage === 'base64') {
        fileBinario = FileUtil.convertDataURIToBinary(data);
      }
      return this.gedService.criarDocumentoEnvio(Array.from(fileBinario), TipoDocumentoGed.fluxoPassos, this.usuarioLogado);
    } catch (e) {
      console.error('erro', e);
    }
  }

  private buildForm() {
    if (this.passoFluxo && this.passoFluxo.execucao) {
      this.passoForm = this.fb.group({
        dataInicioExec: [this.tratarData(this.passoFluxo.execucao.dataInicioExec)],
        dataFimExec: [this.tratarData(this.passoFluxo.execucao.dataFimExec)],
        horasVoluntario: [this.passoFluxo.execucao.horasVoluntario],
        investimento: [this.passoFluxo.execucao.investimento, this.passoFluxo.codigoRubrica ? Validators.required : []],
      });
      this.passoForm.validator = this.compareteDateValidator;
      this.visualizar ? this.passoForm.disable() : null;
    }
  }

  private setSituacao() {
    if (this.checkValues()) {
      const concluido = this.checkConcluido();
      if (concluido) {
        if (this.passoFluxo.perfilAprovador) {
          this.passoFluxo.execucao.situacao = PassoFluxoSituacao.aguardandoAprovacao
        } else {
          this.passoFluxo.execucao.situacao = PassoFluxoSituacao.concluida
        }
      } else {
        this.passoFluxo.execucao.situacao = PassoFluxoSituacao.emExecucao
      }
    } else {
      this.passoFluxo.execucao.situacao = PassoFluxoSituacao.naoIniciada;
    }
  }

  private checkConcluido() {
    let haveInvestimento: boolean = true
    let haveResposta: boolean = true;
    if (this.passoFluxo.codigoRubrica) {
      haveInvestimento = this.passoFluxo.execucao.investimento ? true : false;
    }
    if (this.passoFluxo.formulario) {
      haveResposta = this.passoFluxo.execucao.respostaForm ? true : false;
    }
    return haveResposta && haveInvestimento && this.passoFluxo.execucao.dataInicioExec && this.passoFluxo.execucao.dataFimExec
  }

  private checkValues = () =>
    this.passoFluxo.execucao && (this.passoFluxo.execucao.dataFimExec || this.passoFluxo.execucao.dataInicioExec || this.passoFluxo.execucao.horasVoluntario || this.passoFluxo.execucao.respostaForm || this.passoFluxo.execucao.investimento) ? true : false;

  private tratarData(date: any) {
    if (date) {
      return typeof date === 'string' ? new Date(date) : date;
    }
    return null;
  }

  get dataFim() {
    return this.passoForm.get('dataFimExec') as FormGroup;
  }

  get dataInicio() {
    return this.passoForm.get('dataInicioExec') as FormGroup;
  }

  aprovarTarefa(aprovar: boolean) {
    this.passoFluxo.execucao.aprovado = aprovar;
    this.passoFluxo.execucao.situacao = aprovar ? PassoFluxoSituacao.concluida : PassoFluxoSituacao.ajusteSolicitado;
    this.passoFluxo.execucao.motivoRejeicao = this.motivoRejeicao;
    this.store$.dispatch(new AprovarPassoProjeto(this.passoFluxo.id, this.passoFluxo.execucao, this.usuarioLogado.id))
    this.actionBarRef.close();
  }

  ngOnDestroy(): void {
    this.gedStore$.dispatch(new LimparStoreGed());
  }

  compareteDateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const dataFimControl = control.get('dataFimExec');
    const dataInicioControl = control.get('dataInicioExec');
    if (dataInicioControl.value && dataFimControl.value) {
      if (dataFimControl.value < dataInicioControl.value) {
        this.dataFim.setErrors({ "dataFimMenorControl": true })
      } else {
        this.dataFim.setErrors(null)
      }
    }
    return null;
  };

  excluirArquivo($event: MultiUploadFile) {
    let respostaForm = this.passoFluxo.execucao.respostaForm;
    const fileUploadKeys = Object.keys(respostaForm).filter((key) => respostaForm[key].type === TYPE_UPLOAD_FILE);
    let keyChange: string;
    for (let i = 0; i < fileUploadKeys.length; i++) {
      const key = fileUploadKeys[i];
      let fileComponent = respostaForm[key];
      fileComponent.value = fileComponent.value.filter((file) => file.identificadorGed !== $event.identificadorGed);
      if (fileComponent.value.length === 0) {
        keyChange = key;
      }
    }
    if (keyChange) {
      this.filesForm = this.filesForm.filter((file: MultiUploadFile) => file.identificadorGed !== $event.identificadorGed);
      /*  let components: any[] = [];
       for (let i = 0; i < this.formulario.components.length; i++) {
         let component = this.formulario.components[i];
         if (component.key === keyChange) {
           component.defaultValue = null;
         }
         components.push(component)
       }
       this.formulario = {...this.formulario, components }; */
    }
  }

  getSituacaoClass = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.classes(situacao);

  getSituacaoLabel = (situacao: PassoFluxoSituacao) => PassoFluxoSituacao.label(situacao);


}
