import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { MultiUploadFile } from "@app/shared/models/multiupload-file.model";
import { Store, select } from "@ngrx/store";
import * as fromGed from '@app/reducers/ged.reducer';
import { CarregarArquivo, LimparStoreGed } from "@app/actions/ged.actions";
import { DadosDocumento } from "@app/shared/ged/models/retorno/dadosDocumento.model";

@Component({
  selector: 'lista-arquivos-projeto',
  templateUrl: './lista-arquivos-projeto.component.html',
  styleUrls: ['./lista-arquivos-projeto.component.scss']
})
export class ListaArquivosProjetoComponent implements OnInit {

  @Input() filesForm: MultiUploadFile[];
  @Output() scExcluir: EventEmitter<MultiUploadFile> = new EventEmitter<MultiUploadFile>();
  itemSelected: MultiUploadFile;

  constructor(private gedStore$: Store<fromGed.State>) { }

  ngOnInit(): void {
    this.gedStore$.pipe(select(fromGed.selectAll)).subscribe((arquivos: DadosDocumento[]) => {
      if (arquivos && arquivos.length > 0 && this.itemSelected) {
        this.downLoadFile(arquivos[0].listaSequenciaisDocumento[0].arquivoCodificadoBase64, this.itemSelected.nome, this.itemSelected.contentType)
      }
    })
  }

  downloadFile(item: MultiUploadFile) {
    this.itemSelected = item;
    this.gedStore$.dispatch(new CarregarArquivo(item.identificadorGed));
  }

  private downLoadFile(dataBase64: string, filename: string, type: string) {
    let downloadLink = document.createElement('a');
    downloadLink.href = `data:${type};base64,${dataBase64}`;
    downloadLink.setAttribute('download', filename)
    document.body.appendChild(downloadLink);
    downloadLink.click();
    downloadLink.remove();
    this.gedStore$.dispatch(new LimparStoreGed());
    this.itemSelected = null;
  }

  excluirArquivo(item: MultiUploadFile) {
    this.scExcluir.emit(item);
  }

}
