import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Marco} from '../../models/marco.model';
import {PositionDotsType} from '@app/shared/components/menu-dropdown/model/position-dots.type';
import {ItemMenu} from '@app/shared/components/menu-dropdown/model/item-menu.model';
import {MenuDropDownComponent} from '@app/shared/components/menu-dropdown/menu-dropdown.component';

@Component({
  selector: 'sc-marco',
  templateUrl: './marco.component.html',
  styleUrls: ['./marco.component.scss']
})
export class MarcoComponent {

  @Input() marco: Marco;
  @Input() scEditavel: boolean;

  @Output() scDetalhar: EventEmitter<Marco> = new EventEmitter<Marco>();
  @Output() scEditar: EventEmitter<Marco> = new EventEmitter<Marco>();
  @Output() scExcluir: EventEmitter<Marco> = new EventEmitter<Marco>();

  @ViewChild('dropDown') dropDown: MenuDropDownComponent;
  position: PositionDotsType = PositionDotsType.vertical;
  menus: ItemMenu[] = [new ItemMenu(1, 'Editar Marco'), new ItemMenu(2, 'Excluir Marco')];

  constructor() { }

  onAction($event: ItemMenu) {
    this.dropDown.hide();
    switch ($event.id) {
      case 1:
        this.scEditar.emit(this.marco);
        break;
      case 2:
        this.scExcluir.emit(this.marco);
        break;
      default:
        break;
    }
  }

  detalhar(): void {
    this.scDetalhar.emit(this.marco);
  }
}
