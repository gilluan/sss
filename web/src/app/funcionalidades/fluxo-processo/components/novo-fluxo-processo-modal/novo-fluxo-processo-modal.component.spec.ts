import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoFluxoProcessoModalComponent } from './novo-fluxo-processo-modal.component';

describe('NovoFluxoProcessoModalComponent', () => {
  let component: NovoFluxoProcessoModalComponent;
  let fixture: ComponentFixture<NovoFluxoProcessoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoFluxoProcessoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoFluxoProcessoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
