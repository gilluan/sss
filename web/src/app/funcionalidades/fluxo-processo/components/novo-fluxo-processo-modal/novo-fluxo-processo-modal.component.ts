import {Component, Inject, OnInit} from '@angular/core';
import {MODAL_DATA, ModalRef} from '@sicoob/ui';
import {FluxoProcesso} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo.model';

@Component({
  selector: 'app-novo-fluxo-processo-modal',
  templateUrl: './novo-fluxo-processo-modal.component.html',
  styleUrls: ['./novo-fluxo-processo-modal.component.scss']
})
export class NovoFluxoProcessoModalComponent implements OnInit {
  title: string;
  message: string;
  primaryAction: string;
  name: string;
  hasInput: boolean;

  constructor(
    private ref: ModalRef,
    @Inject(MODAL_DATA) public data: any) {}

  ngOnInit(): void {
    if (this.data && this.data.acao) {
      switch (this.data.acao) {
        case 'create':
          this.hasInput = true;
          this.title = 'Criando fluxo de processo';
          this.message = 'Nome do seu novo fluxo de processo';
          this.primaryAction = 'Criar';
          break;
        case 'clone':
          this.hasInput = true;
          this.title = 'Clonando fluxo de processo';
          this.message = 'Nome do seu novo fluxo de processo';
          this.name = `Cópia de ${this.data.fluxo.nome}`;
          this.primaryAction = 'Clonar';
          break;
        case 'delete':
          this.hasInput = false;
          this.title = 'Excluindo fluxo de processo';
          this.message = `Deseja excluir o fluxo <b>${this.data.fluxo.nome}</b>`;
          this.primaryAction = 'Excluir';
          break;
        default: {
          this.closeModal();
          break;
        }
      }
    }
  }

  submitForm() {
    if (this.data && this.data.acao) {
      const fluxo = this.data.fluxo;
      switch (this.data.acao) {
        case 'create':
          this.createFluxo();
          break;
        case 'clone':
          this.cloneFluxo(fluxo);
          break;
        case 'delete':
          this.deleteFluxo(fluxo);
          break;
        default: {
          this.closeModal();
          break;
        }
      }
    }
  }

  closeModal = (): void => this.ref.close({acao: 'cancelar'});

  private createFluxo = () => this.ref.close({acao: 'create', nome: this.name});

  private cloneFluxo = (fluxo: FluxoProcesso) => this.ref.close({acao: 'clone', nome: this.name, idFluxo: fluxo.id});

  private deleteFluxo = (fluxo: FluxoProcesso) => this.ref.close({acao: 'delete', fluxo: fluxo});

}
