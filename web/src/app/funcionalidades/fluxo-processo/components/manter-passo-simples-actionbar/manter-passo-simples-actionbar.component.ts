import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {ACTIONBAR_DATA, ActionBarRef} from '@sicoob/ui';
import {ActionBarComponentBase} from '@shared/models/actionbar-component-base.model';
import {PassoFluxo} from '../../models/passo-fluxo.model';
import {Validations} from '@shared/utils/validations';
import {PerfilType} from '@shared/types/perfil.type';
import {ManterPassoSimplesActionbarActions} from '../../models/types/manter-passo-simples-actionbar-actions.enum';
import {ScSelectItem} from '@shared/components/sc-select/sc-select-item';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Rubrica} from '@shared/components/sc-lista-rubricas/rubrica.model';

const notNull = Validations.notNull;
const isNull = Validations.isNull;

@Component({
  selector: 'sc-manter-passo-simples-actionbar',
  templateUrl: './manter-passo-simples-actionbar.component.html',
  styleUrls: ['./manter-passo-simples-actionbar.component.scss']
})
export class ManterPassoSimplesActionbarComponent extends ActionBarComponentBase implements OnInit {
  passoFluxo: PassoFluxo;
  formulario?: object;
  idPassoAnterior: string;
  passos: PassoFluxo[];
  perfis: ScSelectItem[] = PerfilType.valuesResponsavel().map(p => this.convertPerfilToSelectItem(p));
  perfisAprovacao: ScSelectItem[];
  pontuacoes: ScSelectItem[] = [0, 25, 50, 75, 100].map(v => new ScSelectItem(`${v} pontos`, v));
  programaEditavel = true;

  passoForm: FormGroup;

  constructor(
    protected readonly changeDetector: ChangeDetectorRef,
    public actionBarRef: ActionBarRef,
    @Inject(ACTIONBAR_DATA) public data: any,
    private fb: FormBuilder) {
    super(actionBarRef);
  }

  ngOnInit(): void {
    this.buildForm();
    this.setupConditionalValidation();

    notNull(this.data['passoFluxo'], (pf) => this.passoFluxo = pf);
    isNull(this.passoFluxo, (_) => this.passoFluxo = new PassoFluxo());
    notNull(this.passoFluxo.formulario, (f) => this.formulario = { ...f, readOnly: true });
    notNull(this.data['idMarco'], (im) => this.passoFluxo.idMarco = im);
    notNull(this.data['idPassoAnterior'], (pa) => this.idPassoAnterior = pa);
    notNull(this.data['passos'], (p) => this.passos = p);
    notNull(this.data['programaEditavel'], (pe) => this.programaEditavel = pe);
    this.patchForm();
  }

  patchForm() {
    if (this.passoFluxo.id) {
      this.passoForm.patchValue({
        titulo: this.passoFluxo.titulo,
        perfilResponsavel: this.getPerfilResponsavel(this.passoFluxo.responsavel),
        pontuacao: this.getPontuacao(this.passoFluxo.pontuacao),
        hasInvestment: this.passoFluxo.codigoRubrica != null,
        rubrica: this.getRubrica(this.passoFluxo.codigoRubrica),
        requiresApproval: this.passoFluxo.perfilAprovador != null,
        perfilAprovador: this.getPerfil(this.passoFluxo.perfilAprovador),
        dependencia: this.idPassoAnterior
      });
    } else if (this.idPassoAnterior) {
      this.passoForm.patchValue({
        dependencia: this.idPassoAnterior
      });
    }
  }

  getTitle(): string {
    return this.passoForm.value.titulo ? this.passoForm.value.titulo : this.passoFluxo.id ? 'Alterando Passo' : 'Criando Passo';
  }

  cancel(): void {
    this.actionBarRef.close({ action: ManterPassoSimplesActionbarActions.cancelar });
  }

  salvar(): void {
    const {titulo, perfilResponsavel, pontuacao, hasInvestment, rubrica, requiresApproval, perfilAprovador, dependencia}
      = this.passoForm.value;
    this.passoFluxo.titulo = titulo;
    this.passoFluxo.responsavel = perfilResponsavel.value;
    this.passoFluxo.pontuacao = pontuacao.value;
    this.passoFluxo.codigoRubrica = hasInvestment ? rubrica.codigo : null;
    this.passoFluxo.perfilAprovador = requiresApproval ? perfilAprovador.value : null;
    this.passoFluxo.idPassoDependencia = dependencia === null || dependencia === '' ? null : dependencia;
    this.actionBarRef.close({ passoFluxo: this.passoFluxo, action: ManterPassoSimplesActionbarActions.salvarPasso });
  }

  excluir(): void {
    this.actionBarRef.close({ passoFluxo: this.passoFluxo, action: ManterPassoSimplesActionbarActions.excluirPasso });
  }

  manterFormulario() {
    this.actionBarRef.close({ passoFluxo: this.passoFluxo, action: ManterPassoSimplesActionbarActions.manterFormulario });
  }

  private convertPerfilToSelectItem(perfil: PerfilType): ScSelectItem {
    const nomePerfil = PerfilType.obterPerfil(perfil);
    return new ScSelectItem(nomePerfil, perfil);
  }

  private getPerfilResponsavel(perfil?: string): ScSelectItem {
    if (perfil) {
      this.selecionarPerfilResponsavel(perfil);
    }
    return this.getPerfil(perfil);
  }

  private getPerfil(perfil?: string): ScSelectItem {
    return perfil ? this.perfis.filter((p: ScSelectItem) => p.value === perfil)[0] : null;
  }

  private getPontuacao(pontuacao?: number): ScSelectItem {
    return (pontuacao !== null && pontuacao !== undefined) ? this.pontuacoes.filter(p => p.value === pontuacao)[0] : null;
  }

  private getRubrica(codigoRubrica?: string): Rubrica {
    if (!codigoRubrica) {
      return null;
    }

    const rubrica = new Rubrica();
    rubrica.codigo = codigoRubrica;
    return rubrica;
  }

  private getDependencia(): PassoFluxo {
    return this.idPassoAnterior && this.passos ? this.passos.filter(p => p.id === this.idPassoAnterior)[0] : null;
  }

  private buildForm() {
    this.passoForm = this.fb.group({
      titulo: ['', Validators.required],
      perfilResponsavel: ['', Validators.required],
      pontuacao: ['', Validators.required],
      hasInvestment: [false],
      rubrica: [''],
      requiresApproval: [false],
      perfilAprovador: [''],
      dependencia: ['']
    });
  }

  private setupConditionalValidation() {
    const aprovadorCtrl = this.passoForm.get('perfilAprovador');
    const rubricaCtrl = this.passoForm.get('rubrica');

    this.passoForm.get('requiresApproval').valueChanges.subscribe(requiresApproval => {
      aprovadorCtrl.setValidators(requiresApproval ? [Validators.required] : null);
      aprovadorCtrl.updateValueAndValidity();
    });

    this.passoForm.get('hasInvestment').valueChanges.subscribe(hasInvestment => {
      rubricaCtrl.setValidators(hasInvestment ? [Validators.required] : null);
      rubricaCtrl.updateValueAndValidity();
    });
  }

  selecionarPerfilResponsavel($event: any | string): void {
    const perfil = typeof $event === 'string' ? $event : $event.item.value;
    const perfisParaAprovacao = PerfilType.getPerfisParaAprovacao(perfil);
    this.perfisAprovacao = perfisParaAprovacao.map(p => this.convertPerfilToSelectItem(p));
  }
}
