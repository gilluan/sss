import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: 'tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() scId: string;
  @Input() scSelected: boolean;
  @Input() scLabel: string;
  @Input() scCount: number;
  @Input() scCountColor: string;

  @Output() scTabSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void { }

  onTabSelected = () => this.scTabSelected.emit();
}
