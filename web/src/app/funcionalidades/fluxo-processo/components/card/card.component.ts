import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PositionDotsType} from '@shared/components/menu-dropdown/model/position-dots.type';
import {ItemMenu} from '@shared/components/menu-dropdown/model/item-menu.model';
import {MenuDropDownComponent} from '@shared/components/menu-dropdown/menu-dropdown.component';

@Component({
  selector: 'app-card',
  templateUrl: 'card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() scTitle: string;
  @Input() scEmptyCard: boolean;
  @Input() scItensMenu: Array<ItemMenu>;
  @Input() scMenuOrientation: 'vertical' | 'horizontal' = 'vertical';

  @Output() scClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() scMenuItemSelect: EventEmitter<ItemMenu> = new EventEmitter<ItemMenu>();

  posicao: PositionDotsType;
  menuPosition = '-20px';
  caretPosition = '28px';

  constructor() { }

  ngOnInit(): void {
    this.getMenuOrientation();
  }

  onClick = () => this.scClick.emit();

  onMenuItemSelect(menu: MenuDropDownComponent, item: ItemMenu) {
    menu.hide();
    this.scMenuItemSelect.emit(item);
  }

  private getMenuOrientation() {
    switch (this.scMenuOrientation) {
      case 'horizontal':
        this.posicao = PositionDotsType.horizontal;
        break;
      case 'vertical':
        this.posicao = PositionDotsType.vertical;
        break;
    }
  }
}
