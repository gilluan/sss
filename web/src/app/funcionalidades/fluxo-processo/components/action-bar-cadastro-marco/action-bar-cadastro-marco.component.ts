import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { Store } from '@ngrx/store';
import { ActionBarRef, ACTIONBAR_DATA } from '@sicoob/ui';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Subscription } from 'rxjs';
import { Marco } from '../../models/marco.model';
import * as fromMarco from '../../reducers/marco.reducer';


@Component({
  selector: 'sc-action-bar-cadastro-marco',
  templateUrl: './action-bar-cadastro-marco.component.html',
  styleUrls: ['./action-bar-cadastro-marco.component.scss']
})
export class ActionBarCadastroMarcoComponent implements OnInit, OnDestroy {

  minDate: Date;
  maxDate: Date;
  marcoForm: FormGroup;
  marcoEditar: Marco;
  processoId: string;
  subscription: Subscription = new Subscription();
  usuarioInstituto: any;
  possuiDataEntrega: boolean = false;
  vigencia: string;

  ngOnInit() {
    this.processoId = this.data.processoId ? this.data.processoId : null;
    this.maxDate = this.data.vigencia ? new Date(this.data.vigencia + '-12-31T23:59:59') : new Date((new Date()).getFullYear() + '-12-31T23:59:59');
    this.minDate = this.data.vigencia ? new Date(this.data.vigencia + '-01-01T00:00:01') : new Date((new Date()).getFullYear() + '-01-01T00:00:01');
    this.localeService.use('pt-br');
    this.usuarioInstituto = this.persistenceService.get('usuario_instituto');
    this.carregarForm();
    this.marcoEditar = this.data.marcoEditar;
    this.carregarFormEditar(this.marcoEditar);
  }

  constructor(
    public actionBarRef: ActionBarRef,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private persistenceService: PersistanceService,
    private store$: Store<fromMarco.State>,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  carregarForm() {
    this.marcoForm = this.fb.group({
      processo: this.processoId,
      dataEntrega: ['',  Validators.required],
      nome: ['', Validators.required],
    });
  }


  carregarFormEditar(marco: Marco) {
    if (marco) {
      this.possuiDataEntrega = !!marco.dataEntrega
      this.marcoForm.get('nome').patchValue(marco.nome);
      this.marcoForm.get('processo').patchValue(marco.processo);
      marco.dataEntrega ? this.marcoForm.get('dataEntrega').patchValue(new Date(marco.dataEntrega)) : false;
    }
  }

  get title() {
    return this.marcoEditar ? 'Editar Marco' : 'Criando Marco';
  }


  editarAcao() {
    const marco = new Marco();
    marco.id = this.marcoEditar.id;
    marco.processo = this.marcoEditar.processo;
    marco.dataCriacao = this.marcoEditar.dataCriacao;
    marco.dataModificacao = this.marcoEditar.dataModificacao;
    marco.dataEntrega = this.marcoForm.get('dataEntrega').value;
    marco.nome = this.marcoForm.get('nome').value;
    this.fechar({ marco, fechar: true, action: 'editar' });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  criar(fechar: boolean) {
    this.fechar({ marco: this.marcoForm.value, fechar, action: 'salvar' });
  }

  fechar = (data?: any) => this.actionBarRef.close(data);

}
