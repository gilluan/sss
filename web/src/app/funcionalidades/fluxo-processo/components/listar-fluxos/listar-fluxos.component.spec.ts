import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarFluxosComponent } from './listar-fluxos.component';

describe('ListarFluxosComponent', () => {
  let component: ListarFluxosComponent;
  let fixture: ComponentFixture<ListarFluxosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarFluxosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarFluxosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
