import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ItemMenu} from '@shared/components/menu-dropdown/model/item-menu.model';
import {FluxoProcesso} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo.model';
import {FluxoProcessoTotals} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo-totals.model';
import {Observable} from 'rxjs';
import {PagedData} from '@shared/models/paged-data.model';

@Component({
  selector: 'app-listar-fluxos',
  templateUrl: './listar-fluxos.component.html',
  styleUrls: ['./listar-fluxos.component.scss']
})
export class ListarFluxosComponent {

  @Input() scPagedData: Observable<PagedData<FluxoProcesso>>;
  @Input() scTotals: Observable<FluxoProcessoTotals>;

  @Output() scTabSelected: EventEmitter<number> = new EventEmitter<number>();
  @Output() scCreateFluxo: EventEmitter<any> = new EventEmitter<any>();
  @Output() scOpenFluxo: EventEmitter<FluxoProcesso> = new EventEmitter<FluxoProcesso>();
  @Output() scCloneFluxo: EventEmitter<FluxoProcesso> = new EventEmitter<FluxoProcesso>();
  @Output() scDeleteFluxo: EventEmitter<FluxoProcesso> = new EventEmitter<FluxoProcesso>();

  menuItems: Array<ItemMenu> = [
    new ItemMenu(0, 'Clonar', { acao: 'clonar' }),
    new ItemMenu(1, 'Excluir', { acao: 'excluir' })
  ];

  constructor() { }

  onTabSelected = (tabIndex: number) => this.scTabSelected.emit(tabIndex);

  emptyCardClick = () => this.scCreateFluxo.emit();

  openCardClick = (fluxo: FluxoProcesso) => this.scOpenFluxo.emit(fluxo);

  itemMenuSelected(fluxo: FluxoProcesso, item: ItemMenu) {
    switch (item.entity.acao) {
      case 'clonar':
        this.scCloneFluxo.emit(fluxo);
        break;
      case 'excluir':
        this.scDeleteFluxo.emit(fluxo);
        break;
    }
  }

}
