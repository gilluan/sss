import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {FluxoProcesso} from '../models/fluxo-processo.model';
import {FluxoProcessoActions, FluxoProcessoActionTypes} from '../actions/fluxo-processo.actions';
import {FluxoProcessoFilter} from '../models/fluxo-processo-filter.model';
import {FluxoProcessoTotals} from '../models/fluxo-processo-totals.model';
import {PageModelVm} from '@shared/models/page.model';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {PagedData} from '@shared/models/paged-data.model';

export interface State extends EntityState<FluxoProcesso> {
  fluxoProcesso: FluxoProcesso;
  pagination: PageModelVm;
  filter: FluxoProcessoFilter;
  totals: FluxoProcessoTotals;
  clearStore: boolean;
}

export const adapter: EntityAdapter<FluxoProcesso> = createEntityAdapter<FluxoProcesso>();

export const initialState: State = adapter.getInitialState({
  fluxoProcesso: new FluxoProcesso('', true, ''),
  pagination: new PageModelVm(0, 8, 0),
  filter: new FluxoProcessoFilter(),
  totals: new FluxoProcessoTotals(),
  clearStore: false
});

export function reducer(
  state = initialState,
  action: FluxoProcessoActions
): State {
  switch (action.type) {
    case FluxoProcessoActionTypes.LoadFluxoProcessoSuccess: {
      return { ...state, fluxoProcesso: action.fluxoProcesso };
    }

    case FluxoProcessoActionTypes.LoadScrollableFluxoProcessoList: {
      return { ...state, filter: action.filter, clearStore: action.clearStore };
    }

    case FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListSuccess: {
      if (state.clearStore) {
        return adapter.addAll(action.pagedData.data, { ...state, pagination: action.pagedData.page });
      } else {
        return adapter.addMany(action.pagedData.data, { ...state, pagination: action.pagedData.page });
      }
    }

    case FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListFail: {
      return {
        ...state,
        filter: initialState.filter,
        pagination: initialState.pagination,
        clearStore: initialState.clearStore
      };
    }

    case FluxoProcessoActionTypes.LoadFluxoProcessoList: {
      return { ...state, filter: action.filter };
    }

    case FluxoProcessoActionTypes.LoadFluxoProcessoListSuccess: {
      return adapter.addAll(action.pagedData.data, state);
    }

    case FluxoProcessoActionTypes.LoadFluxoProcessoListFail: {
      return { ...state, filter: initialState.filter, pagination: initialState.pagination };
    }

    case FluxoProcessoActionTypes.ClearFluxoProcesso: {
      return adapter.removeAll({ ...state, filter: initialState.filter, pagination: initialState.pagination, totals: initialState.totals });
    }

    case FluxoProcessoActionTypes.LoadTotalsFluxoProcessoListSuccess: {
      return { ...state, totals: action.totals };
    }

    case FluxoProcessoActionTypes.CreateFluxoProcessoSuccess: {
      return adapter.addOne(action.fluxoProcesso, state);
    }

    case FluxoProcessoActionTypes.CloneFluxoProcessoSuccess: {
      return adapter.addOne(action.fluxoProcesso, state);
    }

    case FluxoProcessoActionTypes.DeleteFluxoProcessoSuccess: {
      return adapter.removeOne(action.idFluxoProcesso, state);
    }

    default: {
      return state;
    }
  }
}

export const getFluxoProcessoState = createFeatureSelector<State>('fluxoProcessoState');

export const selectFluxoProcessoList = createSelector(
  getFluxoProcessoState,
  (state: State) => new PagedData(state.pagination, Object.values(state.entities)));

export const selectFluxoProcesso = createSelector(getFluxoProcessoState, (state: State) => state.fluxoProcesso);

export const selectFilter = createSelector(
  getFluxoProcessoState,
  (state: State) => state.filter);

export const selectTotals = createSelector(
  getFluxoProcessoState,
  (state: State) => state.totals);

export let selectTotalsAll = createSelector(
  getFluxoProcessoState,
  (state: State) => (state.totals || new FluxoProcessoTotals()).todos);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getFluxoProcessoState);
