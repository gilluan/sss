import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Marco } from '../models/marco.model';
import { MarcoActions, MarcoActionTypes } from '../actions/marco.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PageModelVm } from '@app/shared/models/page.model';
import { MarcoFiltro } from '../models/marco.filtro';

export interface State extends EntityState<Marco> {
  // additional entities state properties
  marcos: Marco[],
  marcoEditar: Marco,
  filtroAtual: MarcoFiltro
}

export const adapter: EntityAdapter<Marco> = createEntityAdapter<Marco>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  marcos: [],
  marcoEditar: null,
  filtroAtual: new MarcoFiltro(),
});

export function reducer(state = initialState, action: MarcoActions): State {
  switch (action.type) {
    case MarcoActionTypes.LoadMarcosSuccess: {
      state.marcos = action.pagedData && action.pagedData.data.length > 0 ? state.marcos.concat(action.pagedData.data) : [];
      let filtro = new MarcoFiltro(null, null, null, null, null, action.pagedData.page);
      return {
        ...state, filtroAtual: filtro
      }
    }
    case MarcoActionTypes.LoadMarcoByIdSuccess: {
      return {
        ...state, marcoEditar: action.marco
      }
    }
    case MarcoActionTypes.DeleteMarcoSuccess: {
      let marcos = state.marcos.filter(a => a.id !== action.id);
      let paginacaoAtual = new PageModelVm(state.filtroAtual.paginacao.pageNumber, state.filtroAtual.paginacao.pageSize, (state.filtroAtual.paginacao.total - 1))
      let filtro = new MarcoFiltro(null, null, null, null, null, paginacaoAtual);
      return {
        ...state, marcos, filtroAtual: filtro,
      }
    }
    case MarcoActionTypes.AddMarcoSuccess: {
      let marcos = state.marcos.concat([action.marco])
      let paginacaoAtual = new PageModelVm(state.filtroAtual.paginacao.pageNumber, state.filtroAtual.paginacao.pageSize, (state.filtroAtual.paginacao.total + 1))
      let filtro = new MarcoFiltro(null, null, null, null, null, paginacaoAtual);
      return {
        ...state, marcos, filtroAtual: filtro,
      }
    }
    case MarcoActionTypes.UpdateMarcoSuccess: {
      for (let i = 0; i < state.marcos.length; i++) {
        const acoes = state.marcos[i];
        if (acoes.id === action.marco.id)
          state.marcos[i] = action.marco
      }
      return {
        ...state
      }
    }
    case MarcoActionTypes.CleanUpdateMarco: {
      return {
        ...state, marcoEditar: null
      }
    }
    case MarcoActionTypes.ClearMarcos: {
      return {
        ...state, marcos: [], marcoEditar: null, filtroAtual: new MarcoFiltro()
      }
    }
    default: {
      return state;
    }
  }
}

export const getMarcosSate = createFeatureSelector<State>('marcoState');

export const getMarcos = createSelector(
  getMarcosSate,
  state => {
    return state.marcos;
  });

export const getMarcoEditar = createSelector(
  getMarcosSate,
  state => {
    return state.marcoEditar;
  });

  export const getFiltroAtual = createSelector(
    getMarcosSate,
    state => {
      return state.filtroAtual
    }
  )


