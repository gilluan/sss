import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {PassoFluxo} from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import {ActionBarRef} from '@sicoob/ui';
import {
  PassoFluxoActions,
  PassoFluxoActionTypes
} from '@app/funcionalidades/fluxo-processo/actions/passo-fluxo.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Validations} from '@shared/utils/validations';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';

const notNull = Validations.notNull;

export interface State extends EntityState<PassoFluxo> {
  passoFluxo: PassoFluxo;
  actionBarRef: ActionBarRef;
  totais: TotaisPassoSituacao;
}

export const adapter: EntityAdapter<PassoFluxo> = createEntityAdapter<PassoFluxo>();

export const initialState: State = adapter.getInitialState({
  passoFluxo: null,
  actionBarRef: null,
  totais: TotaisPassoSituacao.initialState(),
});

export function reducer(state = initialState, action: PassoFluxoActions): State {
  switch (action.type) {

    case PassoFluxoActionTypes.LoadPassoFluxoSuccess: {
      return adapter.addAll(action.passosFluxo, state);
    }

    case PassoFluxoActionTypes.LoadPassoFluxoByIdSuccess: {
      return { ...state, passoFluxo: action.passoFluxo };
    }

    case PassoFluxoActionTypes.CreatePassoFluxoSuccess: {
      return adapter.addOne(action.passoFluxo, state);
    }

    case PassoFluxoActionTypes.UpdatePassoFluxoSuccess: {
      return adapter.upsertOne(action.passoFluxo, state);
    }

    case PassoFluxoActionTypes.DeletePassoFluxoSuccess: {
      return adapter.removeOne(action.idPassoExcluido, adapter.upsertMany(action.passosDependentes, state));
    }

    case PassoFluxoActionTypes.OpenManterPassoSimplesActionBarSuccess: {
      return { ...state, actionBarRef: action.actionBarRef };
    }

    case PassoFluxoActionTypes.ClearPassoFluxo: {
      notNull(state.actionBarRef, (ref: ActionBarRef) => ref.close());
      return { ...state, passoFluxo: null, actionBarRef: null };
    }

    case PassoFluxoActionTypes.LoadTotaisPassosSituacaoSuccess: {
      return { ...state, totais: action.totais };
    }

    default: {
      return state;
    }
  }
}

export const getPassoFluxoState = createFeatureSelector<State>('passoFluxoState');

export const selectPassoFluxoList = createSelector(
  getPassoFluxoState,
  (state: State) => Object.values(state.entities));

export const selectPassoFluxo = createSelector(
  getPassoFluxoState,
  (state: State) => state.passoFluxo);

export const selectPassoById = (id: string) => createSelector(
  getPassoFluxoState,
  (state: State) => state.entities[id]);

export const selectTotais = createSelector(
  getPassoFluxoState,
  (state: State) => state.totais
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors(getPassoFluxoState);
