import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerListarFluxosComponent } from './container-listar-fluxos.component';

describe('ContainerListarFluxosComponent', () => {
  let component: ContainerListarFluxosComponent;
  let fixture: ComponentFixture<ContainerListarFluxosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerListarFluxosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerListarFluxosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
