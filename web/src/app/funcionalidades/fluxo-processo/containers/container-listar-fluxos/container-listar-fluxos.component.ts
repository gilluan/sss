import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FluxoProcesso} from '../../models/fluxo-processo.model';
import {FluxoProcessoFilter} from '../../models/fluxo-processo-filter.model';
import {HeaderActionsContainerService, ModalConfig, ModalRef, ModalService} from '@sicoob/ui';
import {NovoFluxoProcessoModalComponent} from '../../components/novo-fluxo-processo-modal/novo-fluxo-processo-modal.component';
import {Observable, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import * as fromFluxoProcesso from '../../reducers/fluxo-processo.reducer';
import {TemplatePortal} from '@angular/cdk/portal';
import {
  ClearFluxoProcesso,
  CloneFluxoProcesso,
  CreateFluxoProcesso,
  DeleteFluxoProcesso,
  LoadScrollableFluxoProcessoList
} from '../../actions/fluxo-processo.actions';
import {PagedData} from '@shared/models/paged-data.model';
import {FluxoProcessoTotals} from '../../models/fluxo-processo-totals.model';
import {PageModelVm} from '@shared/models/page.model';
import {Validations} from '@shared/utils/validations';
import {tap} from 'rxjs/operators';

const notNull = Validations.notNull;

@Component({
  selector: 'app-container-listar-fluxos',
  templateUrl: './container-listar-fluxos.component.html',
  styleUrls: ['./container-listar-fluxos.component.scss']
})
export class ContainerListarFluxosComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('headerTpl') headerTpl: TemplateRef<any>;

  fluxoProcessoList$: Observable<PagedData<FluxoProcesso>>;
  fluxoProcessoTotals$: Observable<FluxoProcessoTotals>;
  filter: FluxoProcessoFilter;

  showLoadMore: boolean;

  private subscription: Subscription = new Subscription();
  private modalRef: ModalRef;
  private idPrograma = null;

  constructor(
    private readonly headerActionsService: HeaderActionsContainerService,
    private readonly store: Store<fromFluxoProcesso.State>,
    private readonly modalService: ModalService,
    private readonly router: Router) { }

  ngOnInit(): void {
    this.filter = this.initFilter();

    this.fetchData(true);

    this.fluxoProcessoList$ = this.store.pipe(
      select(fromFluxoProcesso.selectFluxoProcessoList),
      tap((pagedData: PagedData<FluxoProcesso>) =>
        this.showLoadMore = pagedData !== undefined && (pagedData.data.length !== pagedData.page.total)));
    this.fluxoProcessoTotals$ = this.store.pipe(select(fromFluxoProcesso.selectTotals));
  }

  ngAfterViewInit(): void {
    this.headerActionsService.open(new TemplatePortal(this.headerTpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.headerActionsService.remove();
    this.subscription.unsubscribe();
    notNull(this.modalRef, (ref: ModalRef) => ref.close());
    this.store.dispatch(new ClearFluxoProcesso());
  }

  onTabSelect(tabIndex: number): void {
    this.filter = this.initFilter();
    switch (tabIndex) {
      case 0:
        this.filter.rascunho = null;
        break;
      case 1:
        this.filter.rascunho = true;
        break;
      case 2:
        this.filter.rascunho = false;
        break;
    }
    this.fetchData(true);
  }

  onScroll(): void {
    if (!this.filter.paginacao.isEOF()) {
      this.filter.paginacao.pageNumber++;
      this.fetchData();
    }
  }

  search(texto: string) {
    this.filter = this.initFilter();
    this.filter.nome = texto;
    this.fetchData(true);
  }

  clearSearch() {
    this.filter = this.initFilter();
    this.filter.nome = '';
    this.fetchData(true);
  }

  createFluxo() {
    const modalConfig = {data: {acao: 'create'}};
    this.openModalFluxo(modalConfig);
  }

  openFluxo(fluxo: FluxoProcesso) {
    this.router.navigate([`/fluxo-processo/${fluxo.id}`]);
  }

  cloneFluxo = (fluxo: FluxoProcesso) => this.openModalFluxo(this.buildModelConfig('clone', fluxo));

  deleteFluxo = (fluxo: FluxoProcesso) => this.openModalFluxo(this.buildModelConfig('delete', fluxo));

  private fetchData(clearState: boolean = false): void {
    this.store.dispatch(new LoadScrollableFluxoProcessoList(this.filter, clearState));
  }

  private buildModelConfig = (action: string, value: any): ModalConfig => ({data: {acao: action, fluxo: value}});

  private openModalFluxo(config: ModalConfig) {
    this.modalRef = this.modalService.open(NovoFluxoProcessoModalComponent, config);
    this.subscription.add(this.modalRef.afterClosed().subscribe((data?: any) => {
      if (data && data.acao) {
        switch (data.acao) {
          case 'create':
            this.store.dispatch(new CreateFluxoProcesso(data.nome, this.idPrograma));
            break;
          case 'clone':
            this.store.dispatch(new CloneFluxoProcesso(data.idFluxo, data.nome));
            break;
          case 'delete':
            this.store.dispatch(new DeleteFluxoProcesso(data.fluxo));
            break;
          case 'cancel':
            break;
        }
      }
    }));
  }

  private initFilter() {
    const filter = new FluxoProcessoFilter();
    filter.paginacao = new PageModelVm(0, 8, 0);
    return filter;
  }
}
