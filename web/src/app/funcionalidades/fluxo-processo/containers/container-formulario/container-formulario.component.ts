import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormioComponent} from '@app/funcionalidades/formio/components/formio/formio.component';
import {HeaderActionsContainerService} from '@sicoob/ui';
import {TemplatePortal} from '@angular/cdk/portal';
import {select, Store} from '@ngrx/store';
import * as fromPassoFluxo from '@app/funcionalidades/fluxo-processo/reducers/passo-fluxo.reducer';
import {Subscription} from 'rxjs';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Validations} from '@shared/utils/validations';
import {
  ClearPassoFluxo,
  LoadPassoFluxoById,
  UpdatePassoFluxo
} from '@app/funcionalidades/fluxo-processo/actions/passo-fluxo.actions';
import {PassoFluxo} from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';

const notNull = Validations.notNull;

@Component({
  selector: 'app-container-formulario',
  templateUrl: './container-formulario.component.html',
  styleUrls: ['./container-formulario.component.scss']
})
export class ContainerFormularioComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('formio') formio: FormioComponent;
  @ViewChild('headerTpl') headerTpl: TemplateRef<any>;

  formulario: any;
  passo: PassoFluxo;

  private subscription: Subscription = new Subscription();

  constructor(
    private readonly headerActionsService: HeaderActionsContainerService,
    private route: ActivatedRoute,
    private router: Router,
    private store$: Store<fromPassoFluxo.State>,
    private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.subscription
      .add(this.route.paramMap.subscribe((params: ParamMap) => this.paramsSubscriber(params)))
      .add(this.store$.pipe(select(fromPassoFluxo.selectPassoFluxo)).subscribe(passo => this.passoSubscriber(passo)));
  }

  ngAfterViewInit(): void {
    this.headerActionsService.open(new TemplatePortal(this.headerTpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.headerActionsService.remove();
    this.subscription.unsubscribe();
    this.store$.dispatch(new ClearPassoFluxo());
  }

  formularioAlterado($event): void {
    if ($event.components) {
      this.formulario = $event;
    }
  }

  salvarFormulario(): void {
    this.passo.formulario = this.formulario;
    this.store$.dispatch(new UpdatePassoFluxo(this.passo));
  }

  private passoSubscriber(passo: PassoFluxo) {
    if (passo) {
      this.passo = passo;
      this.formulario = passo.formulario ? passo.formulario : { components: [] };
      this.changeDetector.detectChanges();
    }
  }

  private paramsSubscriber(params: ParamMap) {
    if (params) {
      notNull(params.get('idPasso'), idPasso => this.loadPasso(idPasso));
    }
  }

  private loadPasso = (idPasso: string): void => this.store$.dispatch(new LoadPassoFluxoById(idPasso));
}
