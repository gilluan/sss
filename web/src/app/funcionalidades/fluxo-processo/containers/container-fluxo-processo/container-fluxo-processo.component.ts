import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import * as fromFluxoProcesso from '@app/funcionalidades/fluxo-processo/reducers/fluxo-processo.reducer';
import * as fromPrograma from '@app/funcionalidades/programas/reducers/programas.reducer';
import * as fromMarco from '../../reducers/marco.reducer';
import { ActivatedRoute, ParamMap, Router, RoutesRecognized } from '@angular/router';
import { LoadFluxoProcesso } from '@app/funcionalidades/fluxo-processo/actions/fluxo-processo.actions';
import { ScBannerComponent } from '@shared/components/sc-banner/sc-banner.component';
import { tap } from 'rxjs/operators';
import { ClearPrograma } from '@app/funcionalidades/programas/actions/programas.actions';
import { RouterService } from '../../../../shared/services/routerext.service';
import { Marco } from '../../models/marco.model';
import { MarcoService } from '../../marco.service';

@Component({
  selector: 'sc-container-fluxo-processo',
  templateUrl: './container-fluxo-processo.component.html',
  styleUrls: ['./container-fluxo-processo.component.scss']
})
export class ContainerFluxoProcessoComponent implements OnInit, OnDestroy {
  @ViewChild('banner') banner: ScBannerComponent;

  idFluxoProcesso: string;
  idPrograma: string;
  programa$: Observable<Programa>;
  marco: Marco;

  private subscription: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store$: Store<fromFluxoProcesso.State>,
    private storePrograma$: Store<fromPrograma.State>,
    private storeMarco$: Store<fromMarco.State>,
    private routeExtService: RouterService) { }

  ngOnInit(): void {
    this.programa$ = this.storePrograma$.pipe(select(fromPrograma.selectPrograma), tap(programa => this.programaSubscriber(programa)));
    this.subscription.add(this.route.paramMap.subscribe(params => this.paramsSubscriber(params)))
      .add(this.storeMarco$.pipe(select(fromMarco.getMarcoEditar)).subscribe((marco: Marco) => this.populateMarco(marco)));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.storePrograma$.dispatch(new ClearPrograma());
  }

  redirecionarParaPrograma(): void {
    if (this.marco) {
      const previousUrl = this.routeExtService.getPreviousUrl();
      if (previousUrl) {
        this.marco = null;
        this.router.navigate([previousUrl]);
      }
    } else {
      this.router.navigate(['programas', this.idPrograma]);
    }
  }

  private loadFluxoProcesso = (idFluxoProcesso: string) => this.store$.dispatch(new LoadFluxoProcesso(idFluxoProcesso));

  private populateMarco(marco: Marco) {
    if (marco) {
      this.marco = marco;
    }
  }

  private paramsSubscriber(params: ParamMap): void {
    if (params.get('id')) {
      this.idFluxoProcesso = params.get('id');
      this.loadFluxoProcesso(this.idFluxoProcesso);
    }
  }

  private programaSubscriber(programa: Programa): void {
    if (programa && programa.id) {
      this.idPrograma = programa.id;
    }
  }
}
