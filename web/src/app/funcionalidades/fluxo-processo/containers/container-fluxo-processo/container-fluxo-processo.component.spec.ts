import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerFluxoProcessoComponent } from './container-fluxo-processo.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerFluxoProcessoComponent', () => {
  let component: ContainerFluxoProcessoComponent;
  let fixture: ComponentFixture<ContainerFluxoProcessoComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerFluxoProcessoComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerFluxoProcessoComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
