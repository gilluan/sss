import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {from, Observable, Subscription} from 'rxjs';
import {Validations} from '@shared/utils/validations';
import {select, Store} from '@ngrx/store';
import * as fromMarco from '@app/funcionalidades/fluxo-processo/reducers/marco.reducer';
import * as fromPassoFluxo from '@app/funcionalidades/fluxo-processo/reducers/passo-fluxo.reducer';
import * as fromPrograma from '@app/funcionalidades/programas/reducers/programas.reducer.js';
import {Marco} from '@app/funcionalidades/fluxo-processo/models/marco.model';
import {LoadMarcoById, ClearMarcos} from '@app/funcionalidades/fluxo-processo/actions/marco.actions';
import {ClearPassoFluxo, LoadPassoFluxo, OpenManterPassoSimplesActionBar} from '../../actions/passo-fluxo.actions';
import {PassoFluxo} from '../../models/passo-fluxo.model';
import {PerfilType} from '@shared/types/perfil.type';

const notNull = Validations.notNull;

@Component({
  selector: 'app-container-passos',
  templateUrl: './container-passos.component.html',
  styleUrls: ['./container-passos.component.scss']
})
export class ContainerPassosComponent implements OnInit, OnDestroy {
  marco$: Observable<Marco>;
  passos$: Observable<PassoFluxo[]>;
  programaEditavel$: Observable<boolean>;

  private subscription: Subscription = new Subscription();
  private idFluxoProcesso: string;
  private idMarco: string;

  constructor(
    private store$: Store<fromPassoFluxo.State>,
    private storeMarco$: Store<fromMarco.State>,
    private storePrograma$: Store<fromPrograma.State>,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.subscription.add(this.route.paramMap.subscribe((params: ParamMap) => this.getParams(params)));
    this.marco$ = this.storeMarco$.pipe(select(fromMarco.getMarcoEditar));
    this.passos$ = this.store$.pipe(select(fromPassoFluxo.selectPassoFluxoList));
    this.programaEditavel$ = this.storePrograma$.pipe(select(fromPrograma.selectEditavel));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.store$.dispatch(new ClearPassoFluxo());
    this.storeMarco$.dispatch(new ClearMarcos());
  }

  async openPassoActionBar(passo?: PassoFluxo, passoAnterior?: PassoFluxo) {
    const idPasso = passo && passo.id ? passo.id : null;
    const idPassoAnterior = passo && passo.idPassoDependencia ? passo.idPassoDependencia : passoAnterior ? passoAnterior.id : null;
    this.store$.dispatch(new OpenManterPassoSimplesActionBar(this.idMarco, idPasso, idPassoAnterior));
  }

  finalizarMarco() {
    this.location.back();
  }

  getPassoDependencia(passos: PassoFluxo[], idPassoDependencia?: string): string {
    const passoFluxo = passos.filter(p => p.id === idPassoDependencia)[0];
    return passoFluxo ? passoFluxo.titulo : null;
  }

  /**
   * Retorna um passo como sugestão de dependência. O fluxo sequencial sugere que um passo é dependente do passo
   * anterior. Logo, o método retorna o passo anterior ao passo correspondente ao index informado. Caso o index seja
   * menor do que zero, significa que o usuário está criando um novo passo, portanto retorna o último passo criado, desde
   * que exista pelo menos dois passos na lista, caso contrário não sugere, pois no caso onde existe apenas um passo, o
   * método sugeriria ele mesmo como dependência. Se o index informado corresponder ao primeiro passo o método não
   * sugere um passo, pois não existe passo anterior à ele.
   * <p>
   * <code>getPassoAnterior([passo1, passo2], 1) // retorna passo1</code><p>
   * <code>getPassoAnterior([passo1, passo2], -1) // retorna passo2</code><p>
   * <code>getPassoAnterior([passo1, passo2], 0) // retorna null</code><p>
   * <code>getPassoAnterior([passo1], -1) // retorna null</code><p>
   *
   * @param passos lista de passos
   * @param index index do passo ao qual se deseja recuperar o anterior
   * @return passo anterior, caso exista
   */
  getPassoAnterior(passos: PassoFluxo[], index: number): PassoFluxo {
    return index > 0 ? passos[index - 1] : index < 0 && passos.length > 1 ? passos[passos.length - 1] : null;
  }

  getFormularioInfo(formulario: object) {
    const componentes: any[] = formulario['components'];
    const quantidadeCampos = componentes.filter(c => c.type !== 'indicador').length;
    const quantidadeIndicadores = componentes.filter(c => c.type === 'indicador').length;
    return `Possui formulário com ${quantidadeCampos} campo(s) e ${quantidadeIndicadores} indicador(es).`;
  }

  getDescricaoPerfil(perfil: PerfilType | string): string {
    return PerfilType.obterPerfil(perfil);
  }

  private getParams(params: ParamMap) {
    if (params) {
      notNull(params.get('id'), (idFluxoProcesso) => this.idFluxoProcesso = idFluxoProcesso);
      notNull(params.get('idMarco'), (idMarco) => {
        this.loadMarco(idMarco);
        this.loadPassos(idMarco);
        return this.idMarco = idMarco;
      });
    }
  }

  private loadMarco = (idMarco: string): void => this.storeMarco$.dispatch(new LoadMarcoById(idMarco));
  private loadPassos = (idMarco: string): void => this.store$.dispatch(new LoadPassoFluxo(idMarco));
}
