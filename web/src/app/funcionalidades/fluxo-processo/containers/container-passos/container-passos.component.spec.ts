import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPassosComponent } from './container-passos.component';

describe('ContainerPassosComponent', () => {
  let component: ContainerPassosComponent;
  let fixture: ComponentFixture<ContainerPassosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerPassosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPassosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
