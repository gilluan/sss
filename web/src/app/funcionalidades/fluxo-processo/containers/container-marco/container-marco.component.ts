import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DialogoConfirmacaoComponent } from '@app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { select, Store } from '@ngrx/store';
import { ActionBarConfig, ActionBarRef, ActionBarService, ModalRef, ModalService } from '@sicoob/ui';
import { Observable, Subscription } from 'rxjs';
import { AddMarco, ClearMarcos, DeleteMarco, LoadMarcos, UpdateMarco } from '../../actions/marco.actions';
import { ActionBarCadastroMarcoComponent } from '../../components/action-bar-cadastro-marco/action-bar-cadastro-marco.component';
import { MarcoFiltro } from '../../models/marco.filtro';
import { Marco } from '../../models/marco.model';
import * as fromMarco from '../../reducers/marco.reducer';
import * as fromPrograma from '../../../programas/reducers/programas.reducer';
import { tap, map } from 'rxjs/operators';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';

@Component({
  selector: 'sc-container-marco',
  templateUrl: './container-marco.component.html',
  styleUrls: ['./container-marco.component.scss']
})
export class ContainerMarcoComponent implements OnInit, OnDestroy {

  actionBarRefs: ActionBarRef[] = [];

  modalRefs: ModalRef[] = [];
  subscription: Subscription = new Subscription();
  marcos$: Observable<Marco[]>;
  filtro: MarcoFiltro = new MarcoFiltro();
  idFluxoProcesso: string;
  programaEditavel$: Observable<boolean>;
  programa$: Observable<Programa>;

  constructor(
    private store$: Store<fromMarco.State>,
    private storePrograma$: Store<fromPrograma.State>,
    private route: ActivatedRoute,
    public router: Router,
    private readonly actionBarService: ActionBarService,
    private readonly modalService: ModalService) { }

  ngOnInit() {
    const marcoFiltro$ = this.store$.pipe(select(fromMarco.getFiltroAtual));
    this.programaEditavel$ = this.storePrograma$.pipe(select(fromPrograma.selectEditavel));
    this.programa$ = this.storePrograma$.pipe(select(fromPrograma.selectPrograma));
    this.subscription
      .add(marcoFiltro$.subscribe((filtro: MarcoFiltro) => this.marcoFiltroSubscriber(filtro)))
      .add(this.route.paramMap.subscribe(params => this.paramsSubscriber(params)));

    this.marcos$ = this.store$.pipe(select(fromMarco.getMarcos));
  }

  ngOnDestroy(): void {
    this.actionBarRefs.forEach(a => a.close());
    this.modalRefs.forEach(a => a.close());
    this.store$.dispatch(new ClearMarcos());
    this.subscription.unsubscribe();
  }

  criarMarco = (programa: Programa) => this.openActionBar({ processoId: this.idFluxoProcesso, vigencia: programa ? programa.vigencia : null }, this.criarMarcoAction);

  editarMarco = (marcoEditar: Marco, programa: Programa) => this.openActionBar({ processoId: marcoEditar.processo, marcoEditar, vigencia: programa ? programa.vigencia : null }, this.editarMarcoAction);

  excluirMarco(marcoExcluir: Marco) {
    const modalConfig = {
      data: {
        header: 'Excluir Marco',
        pergunta: 'Excluíndo este marco todas as tarefas associadas também serão excluídas',
        adicional: 'Deseja excluir mesmo assim?'
      },
      panelClass: 'sins-modal-confirm'
    };

    const modalRef = this.modalService.open(DialogoConfirmacaoComponent, modalConfig);

    this.subscription.add(modalRef.afterClosed().subscribe((data: any) => {
      if (data) {
        this.store$.dispatch(new DeleteMarco(marcoExcluir.id));
      }
    }
    ));

    this.modalRefs.push(modalRef);
  }

  detalharMarco(marco: Marco) {
    this.router.navigate([`fluxo-processo/${this.idFluxoProcesso}/marco/${marco.id}`]);
  }

  onScroll() {
    if (this.filtro.paginacao.pageNumber < this.filtro.paginacao.totalPages) {
      this.filtro.paginacao.pageNumber++;
      this.loadMarcos();
    }
  }

  private paramsSubscriber(params: ParamMap): void {
    if (params.get('id')) {
      this.idFluxoProcesso = params.get('id');
      this.setIdFluxoProcesso();
      this.loadMarcos();
    }
  }

  private marcoFiltroSubscriber(filtro: MarcoFiltro): void {
    this.filtro = filtro;
    this.setIdFluxoProcesso();
  }

  private setIdFluxoProcesso = () => this.filtro.processoId = this.idFluxoProcesso;

  private editarMarcoAction(data, store$) {
    if (data && data.marco) {
      store$.dispatch(new UpdateMarco(data.marco));
    }
  }

  private loadMarcos = () => this.store$.dispatch(new LoadMarcos(this.filtro));

  private criarMarcoAction(data, store$, containerMarco: ContainerMarcoComponent) {
    if (data && data.marco) {
      store$.dispatch(new AddMarco(data.marco));
    }
    if (data && !data.fechar) {
      containerMarco.criarMarco(null);
    }
  }

  private openActionBar(data, action: Function) {
    const config: ActionBarConfig = { data: data, panelClass: 'action-bar-cadastro-marco' };
    const actionBarRef = this.actionBarService.open(ActionBarCadastroMarcoComponent, config);
    this.subscription.add(actionBarRef.afterClosed().subscribe((data: any) => action(data, this.store$, this)));
    this.actionBarRefs.push(actionBarRef);
  }
}
