import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContainerListarFluxosComponent} from './containers/container-listar-fluxos/container-listar-fluxos.component';
import {ContainerMarcoComponent} from './containers/container-marco/container-marco.component';
import {ContainerPassosComponent} from './containers/container-passos/container-passos.component';
import {ContainerFluxoProcessoComponent} from './containers/container-fluxo-processo/container-fluxo-processo.component';
import {ContainerFormularioComponent} from '@app/funcionalidades/fluxo-processo/containers/container-formulario/container-formulario.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerListarFluxosComponent
  }, {
    path: ':id',
    component: ContainerFluxoProcessoComponent,
    data: {
      breadcrumb: 'Fluxo de Processo'
    },
    children: [
      {
        path: '',
        component: ContainerMarcoComponent,
        data: {
          breadcrumb: 'Marcos'
        }
      }, {
        path: 'marco/:idMarco',
        component: ContainerPassosComponent,
        data: {
          breadcrumb: 'Passos'
        },
      }, {
        path: 'marco/:idMarco/passo/:idPasso',
        component: ContainerFormularioComponent,
        data: {
          breadcrumb: 'Formulário'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FluxoProcessoRoutingModule { }
