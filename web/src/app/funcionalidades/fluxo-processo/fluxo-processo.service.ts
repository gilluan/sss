import {Injectable} from '@angular/core';
import {FluxoProcessoFilter} from './models/fluxo-processo-filter.model';
import {Observable} from 'rxjs';
import {PagedData} from '@shared/models/paged-data.model';
import {FluxoProcesso} from './models/fluxo-processo.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Validations} from '@shared/utils/validations';
import {PageModelVm} from '@shared/models/page.model';
import {Service} from '@shared/services/service';
import {environment} from '../../../environments/environment';
import {catchError, map, switchMap} from 'rxjs/operators';
import {Resultado} from '@shared/models/resultado.model';
import {FluxoProcessoTotals} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo-totals.model';
import {ProgramasService} from '@app/funcionalidades/programas/programas.service';

const notNull = Validations.notNull;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class FluxoProcessoService extends Service {

  RESOURCE = 'fluxosprocesso';
  RESOURCE_URL = `${environment.modulo_gestao_tarefa}/${this.RESOURCE}`;

  constructor(private http: HttpClient, private programaService: ProgramasService) { super(); }

  findById(idFluxoProcesso: string): Observable<FluxoProcesso> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/${idFluxoProcesso}`).pipe(
      map(result => result.resultado),
      catchError(this.handleError)
    );
  }

  findAllByFilter(filter: FluxoProcessoFilter): Observable<PagedData<FluxoProcesso>> {
    const params = this.buildParams(filter);
    return this.http.get<Resultado>(this.RESOURCE_URL, { params }).pipe(
      map(result => result.resultado),
      catchError(this.handleError)
    );
  }

  findTotals(filter: FluxoProcessoFilter): Observable<FluxoProcessoTotals> {
    const params = this.buildParams(filter);
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/totais`, { params }).pipe(
      map(result => result.resultado),
      catchError(this.handleError)
    );
  }

  create(fluxoProcesso: FluxoProcesso): Observable<FluxoProcesso> {
    return this.http.post<Resultado>(this.RESOURCE_URL, fluxoProcesso, httpOptions).pipe(
      map(result => result.resultado),
      switchMap((fluxo: FluxoProcesso) => this.programaService.vincularFluxoProcesso(fluxo.idPrograma, fluxo.id).pipe(
        map(_ => fluxo),
        catchError(this.handleError))));
  }

  clone(idFluxo: string, nome: string): Observable<FluxoProcesso> {
    return this.http.patch<Resultado>(`${this.RESOURCE_URL}/${idFluxo}/clonar`, httpOptions).pipe(
      map(result => result.resultado),
      catchError(this.handleError));
  }

  delete(fluxoProcesso: FluxoProcesso) {
    return this.http.delete(`${this.RESOURCE_URL}/${fluxoProcesso.id}`, httpOptions).pipe(catchError(this.handleError));
  }

  private buildParams(filter: FluxoProcessoFilter): HttpParams {
    let params = new HttpParams();

    notNull(filter.nome, n => params = params.append('nome', n));
    notNull(filter.rascunho, r => params = params.append('rascunho', r));
    notNull(filter.paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('offset', pn));
      notNull(p.pageSize, ps => params = params.append('limit', ps));
    });

    return params;
  }
}
