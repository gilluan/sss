import { PassoFluxoSituacao } from "./passo-fluxo-situacao.type";

export class PassoFluxoExecucao {

    constructor(
        public projeto: string,
        public dataInicioPlanejada?: Date,
        public dataFimPlanejada?: Date,
        public dataInicioExec?: Date,
        public dataFimExec?: Date,
        public horasVoluntario?: any,
        public respostaForm?: any,
        public situacao?: PassoFluxoSituacao,
        public investimento?: number,
        public responsavel?: string,
        public aprovador?: string,
        public aprovado: boolean = false,
        public executor?: string,
        public motivoRejeicao?: string ) { }
}
