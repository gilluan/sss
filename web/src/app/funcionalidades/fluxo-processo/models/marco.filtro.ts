import { PageModelVm } from '@shared/models/page.model';


export class MarcoFiltro {

  constructor(
    public processoId?: string,
    public nome?: string,
    public dataEntrega?: any,
    public idProjeto?: string,
    public situacao?: string,
    public paginacao: PageModelVm = new PageModelVm(0, 7, 0)) { }
}
