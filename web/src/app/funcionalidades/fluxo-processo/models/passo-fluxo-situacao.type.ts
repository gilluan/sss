export enum PassoFluxoSituacao {
  naoIniciada = 'naoIniciada',
  emExecucao = 'emExecucao',
  aguardandoAprovacao = 'aguardandoAprovacao',
  ajusteSolicitado = 'ajusteSolicitado',
  concluida = 'concluida',
  cancelada = 'cancelada',
}

export namespace PassoFluxoSituacao {
  export function values(): PassoFluxoSituacao[] {
    return [
      PassoFluxoSituacao.naoIniciada,
      PassoFluxoSituacao.emExecucao,
      PassoFluxoSituacao.concluida,
      PassoFluxoSituacao.cancelada,
      PassoFluxoSituacao.aguardandoAprovacao,
      PassoFluxoSituacao.ajusteSolicitado,
    ]
  }

  export function label(situacao: PassoFluxoSituacao): string {
    switch (situacao) {
      case PassoFluxoSituacao.naoIniciada:
        return 'Não iniciada';
      case PassoFluxoSituacao.emExecucao:
        return 'Em execução';
      case PassoFluxoSituacao.concluida:
        return 'Concluída';
      case PassoFluxoSituacao.cancelada:
        return 'Cancelada';
      case PassoFluxoSituacao.aguardandoAprovacao:
        return 'Aguardando Aprovação';
      case PassoFluxoSituacao.ajusteSolicitado:
        return 'Ajuste Solicitado';
      default:
        return 'Não iniciada';
    }
  }

  export function classes(situacao: PassoFluxoSituacao): string {
    switch (situacao) {
      case PassoFluxoSituacao.naoIniciada:
        return 'ss-label-default';
      case PassoFluxoSituacao.emExecucao:
        return 'ss-label-active';
      case PassoFluxoSituacao.concluida:
        return 'ss-label-success';
      case PassoFluxoSituacao.cancelada:
        return 'ss-label-danger';
      case PassoFluxoSituacao.aguardandoAprovacao:
        return 'ss-label-primary';
      case PassoFluxoSituacao.ajusteSolicitado:
        return 'ss-label-danger';
      default:
        return 'ss-label-default';
    }
  }

  export function classesMdi(situacao: PassoFluxoSituacao): string {
    switch (situacao) {
      case PassoFluxoSituacao.naoIniciada:
        return 'mdi-timer-sand-empty';
      case PassoFluxoSituacao.concluida:
        return 'mdi-check-bold';
      case PassoFluxoSituacao.emExecucao:
        return 'mdi-cogs';
      default:
        return 'mdi-timer-sand-empty'
    }
  }
}
