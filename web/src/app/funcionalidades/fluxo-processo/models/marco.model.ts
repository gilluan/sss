import { BaseDto } from '@app/shared/models/base-dto.model';
import { MarcoExecucao } from './marco-execucao.model';
import { PassoFluxo } from './passo-fluxo.model';

export class Marco extends BaseDto {
  nome: string;
  dataEntrega: Date;
  processo: any;
  execucao?: MarcoExecucao;
  passosFluxo?: PassoFluxo[]
}
