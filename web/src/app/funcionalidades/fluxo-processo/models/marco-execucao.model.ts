import { PassoFluxoSituacao } from "./passo-fluxo-situacao.type";

export class MarcoExecucao {

    constructor(public idProjeto: string, public situacao?: PassoFluxoSituacao, public dataFimPlanejamento?: Date, public dataFimExecucao?: Date) { }

}
