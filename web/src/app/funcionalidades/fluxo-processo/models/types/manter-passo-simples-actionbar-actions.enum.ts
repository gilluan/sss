export enum ManterPassoSimplesActionbarActions {
  cancelar,
  salvarPasso,
  excluirPasso,
  manterFormulario,
}
