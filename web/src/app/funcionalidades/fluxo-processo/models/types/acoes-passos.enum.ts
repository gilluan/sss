export enum AcoesPassos {
  excluir = 'Excluir',
  finalizar = 'Finalizar'
}

export namespace AcoesPassos {
  export function values(): AcoesPassos[] {
    return [
      AcoesPassos.excluir,
      AcoesPassos.finalizar
    ];
  }
}
