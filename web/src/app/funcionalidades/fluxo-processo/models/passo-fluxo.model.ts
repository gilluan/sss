import {BaseDto} from '@shared/models/base-dto.model';
import { PassoFluxoExecucao } from './passo-fluxo-execucao.model';
import { Marco } from './marco.model';

export class PassoFluxo extends BaseDto {
  id: string;
  titulo: string;
  responsavel: string;
  pontuacao: number;
  perfilAprovador: string;
  codigoRubrica: string;
  formulario: any;
  idPassoDependencia?: string;
  idMarco: string;
  marco?: Marco; //FIXME
  passoDependencia?: PassoFluxo; //FIXME
  dataCriacao: Date;
  dataModificacao: Date;
  execucao: PassoFluxoExecucao;
}
