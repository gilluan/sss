import {PageModelVm} from '@shared/models/page.model';

export class FluxoProcessoFilter {

  constructor(
    public nome?: string,
    public rascunho?: boolean,
    public paginacao?: PageModelVm) { }
}
