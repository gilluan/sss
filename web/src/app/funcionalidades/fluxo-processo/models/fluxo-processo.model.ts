import {BaseDto} from '@shared/models/base-dto.model';

export class FluxoProcesso extends BaseDto {
  constructor(
    public nome: string,
    public rascunho: boolean,
    public idPrograma: string) {
    super();
  }
}
