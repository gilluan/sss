export class TotaisPassoSituacao {

  static initialState(): TotaisPassoSituacao {
    return new TotaisPassoSituacao(0, 0, 0, 0);
  }

  constructor(
    public aguardandoAprovacao: number,
    public concluida: number,
    public naoIniciada: number,
    public atrasadas: number) { }
}
