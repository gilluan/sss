import { PassoFluxoSituacao } from "./passo-fluxo-situacao.type";

export class PassoFluxoExecucaoFiltro {

  constructor(
    public idProjeto: string,
    public idPasso?: string,
    public idMarco?: string,
    public responsavel?: string,
    public perfil?: string,
    public situacao?: PassoFluxoSituacao,
    public aprovador?: string,
    public executor?: string) { }
}
