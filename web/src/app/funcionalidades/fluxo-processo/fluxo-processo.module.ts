import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {MenuDropDownModule} from '@shared/components/menu-dropdown/menu-dropdown.module';
import {SinsBadgeModule} from '@shared/components/sins-badge/sins-badge.module';
import {SharedModule} from '@shared/shared.module';
import {ActionbarModule, FormModule, ModalModule, TabsModule} from '@sicoob/ui';
import {BsDatepickerModule, PopoverModule} from 'ngx-bootstrap';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ActionBarCadastroMarcoComponent} from './components/action-bar-cadastro-marco/action-bar-cadastro-marco.component';
import {CardComponent} from './components/card/card.component';
import {FluxoProcessoComponent} from './components/fluxo-processo/fluxo-processo.component';
import {ListarFluxosComponent} from './components/listar-fluxos/listar-fluxos.component';
import {MarcoComponent} from './components/marco/marco.component';
import {NovoFluxoProcessoModalComponent} from './components/novo-fluxo-processo-modal/novo-fluxo-processo-modal.component';
import {TabComponent} from './components/tab/tab.component';
import {ContainerFluxoProcessoComponent} from './containers/container-fluxo-processo/container-fluxo-processo.component';
import {ContainerListarFluxosComponent} from './containers/container-listar-fluxos/container-listar-fluxos.component';
import {ContainerMarcoComponent} from './containers/container-marco/container-marco.component';
import {FluxoProcessoEffects} from './effects/fluxo-processo.effects';
import {MarcoEffects} from './effects/marco.effects';
import {FluxoProcessoRoutingModule} from './fluxo-processo-routing.module';
import * as fromFluxoProcesso from './reducers/fluxo-processo.reducer';
import * as fromMarco from './reducers/marco.reducer';
import * as passoFluxoState from './reducers/passo-fluxo.reducer';
import {ContainerPassosComponent} from './containers/container-passos/container-passos.component';
import {ManterPassoSimplesActionbarComponent} from './components/manter-passo-simples-actionbar/manter-passo-simples-actionbar.component';
import {PassoFluxoEffect} from '@app/funcionalidades/fluxo-processo/effects/passo-fluxo.effect';
import {ScAccordionItemModule} from '@shared/components/sc-accordion-item/sc-accordion-item.module';
import {ScListaRubricaModule} from '@shared/components/sc-lista-rubricas/sc-lista-rubricas.module';
import {NgSelectModule} from "@ng-select/ng-select";
import { ContainerFormularioComponent } from './containers/container-formulario/container-formulario.component';
import {FormioModule} from "@app/funcionalidades/formio/formio.module";

@NgModule({
  declarations: [
    ContainerFluxoProcessoComponent,
    FluxoProcessoComponent,
    ListarFluxosComponent,
    CardComponent,
    ContainerListarFluxosComponent,
    TabComponent,
    NovoFluxoProcessoModalComponent,
    ContainerMarcoComponent,
    MarcoComponent,
    ActionBarCadastroMarcoComponent,
    ContainerPassosComponent,
    ManterPassoSimplesActionbarComponent,
    ContainerFormularioComponent
  ],
    imports: [
        InfiniteScrollModule,
        PopoverModule.forRoot(),
        BsDatepickerModule.forRoot(),
        FormModule,
        ReactiveFormsModule,
        ActionbarModule,
        CommonModule,
        FluxoProcessoRoutingModule,
        StoreModule.forFeature('fluxoProcessoState', fromFluxoProcesso.reducer),
        StoreModule.forFeature('marcoState', fromMarco.reducer),
        StoreModule.forFeature('passoFluxoState', passoFluxoState.reducer),
        EffectsModule.forFeature([FluxoProcessoEffects, MarcoEffects, PassoFluxoEffect]),
        TabsModule,
        MenuDropDownModule,
        SinsBadgeModule,
        SharedModule,
        ModalModule,
        FormsModule,
        ScAccordionItemModule,
        ScListaRubricaModule,
        NgSelectModule,
        FormioModule
    ],
  entryComponents: [
    NovoFluxoProcessoModalComponent,
    ActionBarCadastroMarcoComponent,
    ManterPassoSimplesActionbarComponent
  ]
})
export class FluxoProcessoModule { }
