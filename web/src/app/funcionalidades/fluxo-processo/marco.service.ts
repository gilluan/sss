import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {PageModelVm} from '@app/shared/models/page.model';
import {PagedDataModelVm} from '@app/shared/models/paged-data.model';
import {Service} from '@app/shared/services/service';
import {Validations} from '@app/shared/utils/validations';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {MarcoFiltro} from './models/marco.filtro';
import {Marco} from './models/marco.model';

const notNull = Validations.notNull;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class MarcoService extends Service {


  constructor(private http: HttpClient) {
    super();
  }

  RESOURCE = 'marcos';
  RESOURCE_URL = `${environment.modulo_gestao_tarefa}/${this.RESOURCE}`;

  alterarMarco(marco: Marco): Observable<Marco> {
    return this.http.put<{ resultado: Marco }>(this.RESOURCE_URL, marco, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  salvarMarco(marco: Marco): Observable<Marco> {
    return this.http.post<{ resultado: Marco }>(this.RESOURCE_URL, marco, httpOptions).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  excluirMarco(id: string): Observable<string> {
    return this.http.delete<any>(`${this.RESOURCE_URL}/${id}`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }


  carregaMarcos(filtro: MarcoFiltro): Observable<PagedDataModelVm> {
    const params = this.buildParams(filtro);
    return this.http.get<{ resultado: PagedDataModelVm }>(`${this.RESOURCE_URL}`, { params }).pipe(
      map(r => r.resultado),
      catchError(this.handleError)
    );
  }

  carregaMarcoPorId(id: string): Observable<Marco> {
    return this.http.get<Marco>(`${this.RESOURCE_URL}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  private buildParams(filtro: MarcoFiltro): HttpParams {
    let params = new HttpParams();
    notNull(filtro.nome, n => params = params.append('nome', n));
    notNull(filtro.dataEntrega, n => params = params.append('dataEntrega', n));
    notNull(filtro.processoId, n => params = params.append('processo', n));
    notNull(filtro.idProjeto, n => params = params.append('idProjeto', n));
    notNull(filtro.situacao, n => params = params.append('situacao', n));
    notNull(filtro.paginacao, (p: PageModelVm) => {
      notNull(p.pageNumber, pn => params = params.append('offset', pn));
      notNull(p.pageSize, ps => params = params.append('limit', ps));
    });
    return params;
  }
}
