import { TestBed } from '@angular/core/testing';

import { FluxoProcessoService } from './fluxo-processo.service';

describe('FluxoProcessoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FluxoProcessoService = TestBed.get(FluxoProcessoService);
    expect(service).toBeTruthy();
  });
});
