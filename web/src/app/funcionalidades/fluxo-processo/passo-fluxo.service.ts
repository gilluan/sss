import {Service} from '@shared/services/service';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {PassoFluxo} from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Resultado} from '@shared/models/resultado.model';
import {PassoFluxoFilter} from '@app/funcionalidades/fluxo-processo/models/passo-fluxo-filter.model';
import {Validations} from '@shared/utils/validations';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';

const notNull = Validations.notNull;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class PassoFluxoService extends Service {
  RESOURCE = 'passos-fluxo';
  RESOURCE_URL = `${environment.modulo_gestao_tarefa}/${this.RESOURCE}`;

  constructor(private http: HttpClient) { super(); }

  create = (passoFluxo: PassoFluxo): Observable<PassoFluxo> =>
    this.http.post<Resultado>(this.RESOURCE_URL, passoFluxo, httpOptions).pipe(
      map((result: Resultado) => result.resultado),
      catchError(this.handleError))

  update(passoFluxo: PassoFluxo): Observable<PassoFluxo> {
    return this.http.put<Resultado>(`${this.RESOURCE_URL}/${passoFluxo.id}`, passoFluxo, httpOptions).pipe(
      map((result: Resultado) => result.resultado),
      catchError(this.handleError)
    );
  }

  delete(idPassoFluxo: string): Observable<PassoFluxo[]> {
    return this.http.delete<Resultado>(`${this.RESOURCE_URL}/${idPassoFluxo}`).pipe(
      map((result: Resultado) => result.resultado),
      catchError(this.handleError));
  }

  findById = (idPassoFluxo: string): Observable<PassoFluxo> =>
    this.http.get<Resultado>(`${this.RESOURCE_URL}/${idPassoFluxo}`).pipe(
      map((result: Resultado) => result.resultado),
      catchError(this.handleError))

  findAllByMarco(idMarco: string): Observable<PassoFluxo[]> {
    return this.findByFilter(new PassoFluxoFilter(idMarco));
  }

  carregarTotaisPassoSituacao(idResponsavel: string): Observable<TotaisPassoSituacao> {
    return this.http.get<Resultado>(`${this.RESOURCE_URL}/totais/responsavel/${idResponsavel}`).pipe(
      map((resultado: Resultado) => resultado.resultado),
      catchError(this.handleError));
  }

  private findByFilter(filter: PassoFluxoFilter): Observable<PassoFluxo[]> {
    return this.http.get<Resultado>(this.RESOURCE_URL, {params: this.buildParams(filter)}).pipe(
      map((result: Resultado) => result.resultado),
      catchError(this.handleError));
  }

  private buildParams(filter: PassoFluxoFilter): HttpParams {
    let params = new HttpParams();

    notNull(filter.idMarco, m => params = params.append('idMarco', m));

    return params;
  }
}
