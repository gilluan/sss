import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  CreatePassoFluxo,
  CreatePassoFluxoFail,
  CreatePassoFluxoSuccess,
  DeletePassoFluxo,
  DeletePassoFluxoFail,
  DeletePassoFluxoSuccess,
  LoadPassoFluxo,
  LoadPassoFluxoById,
  LoadPassoFluxoByIdFail,
  LoadPassoFluxoByIdSuccess,
  LoadPassoFluxoFail,
  LoadPassoFluxoSuccess,
  LoadTotaisPassosSituacao,
  LoadTotaisPassosSituacaoFail,
  LoadTotaisPassosSituacaoSuccess,
  NoActionPassoFluxo,
  OpenManterPassoSimplesActionBar,
  OpenManterPassoSimplesActionBarFail,
  OpenManterPassoSimplesActionBarSuccess,
  PassoFluxoActionTypes,
  SavePassoNavigateToForm,
  SavePassoNavigateToFormFail,
  SavePassoNavigateToFormSuccess,
  UpdatePassoFluxo,
  UpdatePassoFluxoFail,
  UpdatePassoFluxoSuccess
} from '../actions/passo-fluxo.actions';
import {catchError, map, mergeMap, switchMap, withLatestFrom} from 'rxjs/operators';
import {PassoFluxoService} from '../passo-fluxo.service';
import {ActionBarConfig, ActionBarService, Color} from '@sicoob/ui';
import {of} from 'rxjs';
import {ManterPassoSimplesActionbarComponent} from '../components/manter-passo-simples-actionbar/manter-passo-simples-actionbar.component';
import {ManterPassoSimplesActionbarActions} from '../models/types/manter-passo-simples-actionbar-actions.enum';
import {CustomAlertService} from '@shared/services/alert-service';
import {PassoFluxo} from '@app/funcionalidades/fluxo-processo/models/passo-fluxo.model';
import {select, Store} from '@ngrx/store';
import * as fromPassoFluxo from '@app/funcionalidades/fluxo-processo/reducers/passo-fluxo.reducer';
import * as fromPrograma from '@app/funcionalidades/programas/reducers/programas.reducer';
import {Validations} from '@shared/utils/validations';
import {Router} from '@angular/router';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';

const notNull = Validations.notNull;

const MARCO_URI = /\/fluxo-processo\/\w{24}\/marco\/\w{24}/g;
const PASSO_URI = /\/fluxo-processo\/\w{24}\/marco\/\w{24}\/passo\/\w{24}/g;

const noDispatch = { dispatch: false };

@Injectable()
export class PassoFluxoEffect {

  constructor(
    private actions$: Actions,
    private service: PassoFluxoService,
    private alertService: CustomAlertService,
    private actionBarService: ActionBarService,
    private store: Store<fromPassoFluxo.State>,
    private storePrograma: Store<fromPrograma.State>,
    private router: Router) { }

  @Effect()
  OpenSimpleStepActionBar$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.OpenManterPassoSimplesActionBar),
    withLatestFrom(this.store.pipe(select(fromPassoFluxo.selectPassoFluxoList))),
    mergeMap(([action, passos]: [OpenManterPassoSimplesActionBar, PassoFluxo[]]) => {
      const config: ActionBarConfig = {
        data: {
          idMarco: action.idMarco,
          passos: passos.filter(p => p.id !== action.idPasso)
        }, hasBackdrop: true
      };

      notNull(action.idPassoAnterior, (i) => config.data.idPassoAnterior = i);

      if (action.idPasso) {
        return this.service.findById(action.idPasso).pipe(map(passo => {
          config.data.passoFluxo = passo;
          return config;
        }));
      } else {
        return of(config);
      }
    }),
    withLatestFrom(this.storePrograma.pipe(select(fromPrograma.selectEditavel))),
    map(([config, programaEditavel]: [ActionBarConfig, boolean]) => {
      config.data.programaEditavel = programaEditavel;
      const actionBarRef = this.actionBarService.open(ManterPassoSimplesActionbarComponent, config);
      return new OpenManterPassoSimplesActionBarSuccess(actionBarRef);
    }),
    catchError(error => of(new OpenManterPassoSimplesActionBarFail(error))));

  @Effect()
  OpenSimpleStepActionBarSuccess$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.OpenManterPassoSimplesActionBarSuccess),
    switchMap((action: OpenManterPassoSimplesActionBarSuccess) => {
      return action.actionBarRef.afterClosed().pipe(
        map((data: any) => {
          if (data && data.action !== undefined) {
            switch (data.action) {
              case ManterPassoSimplesActionbarActions.salvarPasso:
                return data.passoFluxo.id ? new UpdatePassoFluxo(data.passoFluxo) : new CreatePassoFluxo(data.passoFluxo);
              case ManterPassoSimplesActionbarActions.excluirPasso:
                return new DeletePassoFluxo(data.passoFluxo.id);
              case ManterPassoSimplesActionbarActions.manterFormulario:
                return new SavePassoNavigateToForm(data.passoFluxo);
              default:
                return new NoActionPassoFluxo();
            }
          } else {
            return new NoActionPassoFluxo();
          }
        }));
    }));

  @Effect({ dispatch: false })
  OpenSimpleStepActionBarFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.OpenManterPassoSimplesActionBarFail),
    map((action: OpenManterPassoSimplesActionBarFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar passo do fluxo.')));

  @Effect()
  create$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.CreatePassoFluxo),
    map((action: CreatePassoFluxo) => action.passoFluxo),
    switchMap((passoFluxo: PassoFluxo) => this.service.create(passoFluxo).pipe(
      map((newPassoFluxo: PassoFluxo) => new CreatePassoFluxoSuccess(newPassoFluxo)),
      catchError(error => of(new CreatePassoFluxoFail(error))))));

  @Effect({ dispatch: false })
  createSuccess$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.CreatePassoFluxoSuccess),
    map((action: CreatePassoFluxoSuccess) => action.passoFluxo),
    map((passoFluxo: PassoFluxo) =>
      this.alertService.abrirAlert(Color.SUCCESS, `O passo ${passoFluxo.titulo} foi criado com sucesso.`)));

  @Effect({ dispatch: false })
  createFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.CreatePassoFluxoFail),
    map((action: CreatePassoFluxoFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar o passo do fluxo.')));

  @Effect()
  update$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.UpdatePassoFluxo),
    map((action: UpdatePassoFluxo) => action.passoFluxo),
    switchMap((passoFluxo: PassoFluxo) => this.service.update(passoFluxo).pipe(
      map((passoAlterado: PassoFluxo) => new UpdatePassoFluxoSuccess(passoAlterado)),
      catchError(error => of(new UpdatePassoFluxoFail(error))))));

  @Effect({ dispatch: false })
  updateSuccess$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.UpdatePassoFluxoSuccess),
    map((action: UpdatePassoFluxoSuccess) => action.passoFluxo),
    map((passo: PassoFluxo) => {
      if (this.router.url.match(PASSO_URI)) {
        const marcoURI = MARCO_URI.exec(this.router.url)[0];
        this.router.navigate([marcoURI]);
      } else if (this.router.url.match(MARCO_URI)) {
        this.alertService.abrirAlert(Color.SUCCESS, `O passo ${passo.titulo} foi alterado com sucesso.`);
      }
    }));

  @Effect({ dispatch: false })
  updateFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.UpdatePassoFluxoFail),
    map((action: UpdatePassoFluxoFail) => action),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível alterar o passo do fluxo.')));

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.DeletePassoFluxo),
    map((action: DeletePassoFluxo) => action.idPasso),
    switchMap((idPasso: string) => this.service.delete(idPasso).pipe(
      map((passosDependentes: PassoFluxo[]) => new DeletePassoFluxoSuccess(idPasso, passosDependentes)),
      catchError(error => of(new DeletePassoFluxoFail(error))))));

  @Effect({ dispatch: false })
  deleteSuccess$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.DeletePassoFluxoSuccess),
    map((action: DeletePassoFluxoSuccess) =>
      this.alertService.abrirAlert(Color.SUCCESS, `Passo foi excluído com sucesso.`)));

  @Effect({ dispatch: false })
  deleteFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.DeletePassoFluxoFail),
    map((action: DeletePassoFluxoFail) => action),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir o passo do fluxo.')));

  @Effect()
  load$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadPassoFluxo),
    map((action: LoadPassoFluxo) => action.idMarco),
    switchMap((idMarco: string) => this.service.findAllByMarco(idMarco).pipe(
      map((passoFluxoList: PassoFluxo[]) => new LoadPassoFluxoSuccess(passoFluxoList)),
      catchError(error => of(new LoadPassoFluxoFail(error))))));

  @Effect({ dispatch: false })
  loadFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadPassoFluxoFail),
    map((action: LoadPassoFluxoFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os passos do fluxo.')));

  @Effect() loadById$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadPassoFluxoById),
    map((action: LoadPassoFluxoById) => action.idPasso),
    switchMap((idPasso: string) => this.service.findById(idPasso).pipe(
      map((passoFound: PassoFluxo) => new LoadPassoFluxoByIdSuccess(passoFound)),
      catchError(error => of(new LoadPassoFluxoByIdFail(error))))));

  @Effect() loadByIdFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadPassoFluxoByIdFail),
    map((action: LoadPassoFluxoByIdFail) => action),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar o passo do fluxo.')));

  @Effect()
  saveAndNavigateToForm$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.SavePassoNavigateToForm),
    map((action: SavePassoNavigateToForm) => action.passoFluxo),
    switchMap((passo: PassoFluxo) =>
      (passo.id ? this.service.update(passo) : this.service.create(passo)).pipe(
        map((newOrUpdatedPasso: PassoFluxo) => new SavePassoNavigateToFormSuccess(newOrUpdatedPasso)),
        catchError(error => of(new SavePassoNavigateToFormFail(error))))));

  @Effect({ dispatch: false })
  saveAndNavigateToFormSuccess$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.SavePassoNavigateToFormSuccess),
    map((action: SavePassoNavigateToFormSuccess) => action.passoFluxo),
    map((passo: PassoFluxo) => this.router.navigate([ `${this.router.url}/passo/${passo.id}`])));

  @Effect({ dispatch: false })
  saveAndNavigateToFormFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.SavePassoNavigateToFormFail),
    map((action: SavePassoNavigateToFormFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível salvar os passos do fluxo para a inclusão do formulário.')));

  @Effect()
  loadTotaisPassoSituacao$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadTotaisPassosSituacao),
    switchMap((action: LoadTotaisPassosSituacao) => this.service.carregarTotaisPassoSituacao(action.idResponsavel).pipe(
      map((totais: TotaisPassoSituacao) => new LoadTotaisPassosSituacaoSuccess(totais)),
      catchError(error => of(new LoadTotaisPassosSituacaoFail(error))))));

  @Effect(noDispatch)
  loadTotaisPassoSituacaoFail$ = this.actions$.pipe(
    ofType(PassoFluxoActionTypes.LoadTotaisPassosSituacaoFail),
    map((action: LoadTotaisPassosSituacaoFail) =>
      this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os totais das suas tarefas')));
}
