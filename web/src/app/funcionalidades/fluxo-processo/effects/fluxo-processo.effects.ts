import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import * as fromFluxoProcesso from '../reducers/fluxo-processo.reducer';
import {
  CloneFluxoProcesso,
  CloneFluxoProcessoFail,
  CloneFluxoProcessoSuccess,
  CreateFluxoProcesso,
  CreateFluxoProcessoFail,
  CreateFluxoProcessoSuccess,
  DeleteFluxoProcesso,
  DeleteFluxoProcessoFail,
  DeleteFluxoProcessoSuccess,
  FluxoProcessoActionTypes,
  LoadFluxoProcesso,
  LoadFluxoProcessoFail,
  LoadFluxoProcessoList,
  LoadFluxoProcessoListFail,
  LoadFluxoProcessoListSuccess,
  LoadFluxoProcessoSuccess,
  LoadScrollableFluxoProcessoList,
  LoadScrollableFluxoProcessoListFail,
  LoadScrollableFluxoProcessoListSuccess,
  LoadTotalsFluxoProcessoList,
  LoadTotalsFluxoProcessoListFail,
  LoadTotalsFluxoProcessoListSuccess,
  NavigateFluxoProcesso,
  NavigateFluxoProcessoSuccess,
  NoActionFluxoProcesso
} from '../actions/fluxo-processo.actions';
import {catchError, map, mergeMap, switchMap, withLatestFrom} from 'rxjs/operators';
import {FluxoProcessoFilter} from '../models/fluxo-processo-filter.model';
import {FluxoProcessoService} from '../fluxo-processo.service';
import {PagedData} from '@shared/models/paged-data.model';
import {of} from 'rxjs';
import {FluxoProcesso} from '../models/fluxo-processo.model';
import {CustomAlertService} from '@shared/services/alert-service';
import {Color} from '@sicoob/ui';
import {FluxoProcessoTotals} from '../models/fluxo-processo-totals.model';
import {Router} from '@angular/router';
import {LoadPrograma} from '@app/funcionalidades/programas/actions/programas.actions';

@Injectable()
export class FluxoProcessoEffects {

  constructor(
    private actions$: Actions,
    private store$: Store<fromFluxoProcesso.State>,
    private service: FluxoProcessoService,
    private alertService: CustomAlertService,
    private router: Router) {}

  @Effect()
  load$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcesso),
    map((action: LoadFluxoProcesso) => action.idFluxoProcesso),
    switchMap((idFluxoProcesso: string) => this.service.findById(idFluxoProcesso).pipe(
      map((fluxoProcesso: FluxoProcesso) => new LoadFluxoProcessoSuccess(fluxoProcesso)),
      catchError(error => of(new LoadFluxoProcessoFail(error))))));

  @Effect()
  loadSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcessoSuccess),
    map((action: LoadFluxoProcessoSuccess) => action.fluxoProcesso),
    map((fluxoProcesso: FluxoProcesso) =>
      fluxoProcesso.idPrograma ? new LoadPrograma(fluxoProcesso.idPrograma, true) : new NoActionFluxoProcesso()));

  @Effect({ dispatch: false })
  loadFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcessoFail),
    map((action: LoadFluxoProcessoFail) => action.payload),
    map((error: string) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar o fluxo de processo.')));

  @Effect()
  loadScrollable$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadScrollableFluxoProcessoList),
    map((action: LoadScrollableFluxoProcessoList) => action.filter),
    switchMap((filter: FluxoProcessoFilter) => this.service.findAllByFilter(filter).pipe(
      map((pagedData: PagedData<FluxoProcesso>) => new LoadScrollableFluxoProcessoListSuccess(pagedData)),
      catchError(error => of(new LoadScrollableFluxoProcessoListFail(error))))));

  @Effect()
  loadScrollableSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListSuccess),
    withLatestFrom(this.store$.pipe(select(fromFluxoProcesso.selectFilter))),
    mergeMap(([_, filter]: [LoadFluxoProcessoListSuccess, FluxoProcessoFilter]) => of(new LoadTotalsFluxoProcessoList(filter))));

  @Effect({ dispatch: false })
  loadScrollableFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListFail),
    map((action: LoadFluxoProcessoListFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar a lista de fluxo de processo')));

  @Effect()
  loadList$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcessoList),
    map((action: LoadFluxoProcessoList) => action.filter),
    switchMap((filter: FluxoProcessoFilter) => this.service.findAllByFilter(filter).pipe(
      map((pagedData: PagedData<FluxoProcesso>) => new LoadFluxoProcessoListSuccess(pagedData)),
      catchError(error => of(new LoadFluxoProcessoListFail(error))))));

  @Effect()
  loadListSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcessoListSuccess),
    withLatestFrom(this.store$.pipe(select(fromFluxoProcesso.selectFilter))),
    mergeMap(([_, filter]: [LoadFluxoProcessoListSuccess, FluxoProcessoFilter]) => of(new LoadTotalsFluxoProcessoList(filter))));

  @Effect({ dispatch: false })
  loadListFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadFluxoProcessoListFail),
    map((action: LoadFluxoProcessoListFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar a lista de fluxo de processo')));

  @Effect()
  loadTotals$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadTotalsFluxoProcessoList),
    map((action: LoadTotalsFluxoProcessoList) => action.filter),
    switchMap((filter: FluxoProcessoFilter) => this.service.findTotals(filter).pipe(
      map((totals: FluxoProcessoTotals) => new LoadTotalsFluxoProcessoListSuccess(totals)),
      catchError(error => of(new LoadTotalsFluxoProcessoListFail(error))))));

  @Effect({ dispatch: false })
  loadTotalsFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.LoadTotalsFluxoProcessoListFail),
    map((action: LoadTotalsFluxoProcessoListFail) => action.payload),
    map(error => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os totais da lista de fluxo de processo')));

  @Effect()
  create$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CreateFluxoProcesso),
    switchMap((action: CreateFluxoProcesso) => this.service.create(new FluxoProcesso(action.nome, true, action.idPrograma)).pipe(
      map((fluxoProcesso: FluxoProcesso) => new CreateFluxoProcessoSuccess(fluxoProcesso)),
      catchError((error) => of(new CreateFluxoProcessoFail(error))))));

  @Effect({ dispatch: false })
  createSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CreateFluxoProcessoSuccess),
    map((action: CreateFluxoProcessoSuccess) => action.fluxoProcesso),
    map((fluxoProcesso: FluxoProcesso) => this.router.navigate([`fluxo-processo/${fluxoProcesso.id}`])));

  @Effect({ dispatch: false })
  createFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CreateFluxoProcessoFail),
    map((action: CreateFluxoProcessoFail) => action.payload),
    map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível criar o fluxo de processo.')));

  @Effect()
  clone$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CloneFluxoProcesso),
    switchMap((action: CloneFluxoProcesso) => this.service.clone(action.idFluxoProcesso, action.nome).pipe(
      map((fluxoProcesso: FluxoProcesso) => new CloneFluxoProcessoSuccess(fluxoProcesso)),
      catchError((error) => of(new CloneFluxoProcessoFail(error))))));

  @Effect({ dispatch: false })
  cloneSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CloneFluxoProcessoSuccess),
    map((action: CloneFluxoProcessoSuccess) => action.fluxoProcesso),
    map((fluxoProcesso: FluxoProcesso) => {
      this.alertService.abrirAlert(Color.SUCCESS, 'Fluxo de processo clonado com sucesso.');
      this.router.navigate([ `/fluxo-processo/${fluxoProcesso.id}` ]);
    }));

  @Effect({ dispatch: false })
  cloneFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.CloneFluxoProcessoFail),
    map((action: CloneFluxoProcessoFail) => action.payload),
    map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível clonar o fluxo de processo.')));

  @Effect()
  delete$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.DeleteFluxoProcesso),
    map((action: DeleteFluxoProcesso) => action.fluxoProcesso),
    switchMap((fluxoProcesso) => this.service.delete(fluxoProcesso).pipe(
      map(() => new DeleteFluxoProcessoSuccess(fluxoProcesso.id)),
      catchError((error) => of(new DeleteFluxoProcessoFail(error))))));

  @Effect()
  deleteSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.DeleteFluxoProcessoSuccess),
    withLatestFrom(this.store$.pipe(select(fromFluxoProcesso.selectFilter))),
    mergeMap(([_, filter]: [DeleteFluxoProcessoSuccess, FluxoProcessoFilter]) => {
      this.alertService.abrirAlert(Color.SUCCESS, 'Fluxo de processo excluído com sucesso.');
      // Caso o fluxo tenha sido excluído fora da tela de lista de fluxo de processo ele redireciona para ela.
      if (this.router.url !== '/fluxo-processo') {
        this.router.navigate(['/fluxo-processo']);
        return of(new NoActionFluxoProcesso());
      }
      // Caso contrário, apenas carrega os totais.
      return of(new LoadTotalsFluxoProcessoList(filter));
    }));

  @Effect({ dispatch: false })
  deleteFail$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.DeleteFluxoProcessoFail),
    map((action: DeleteFluxoProcessoFail) => action.payload),
    map((_) => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir o fluxo de processo.')));

  @Effect()
  navigateFluxoProcesso$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.NavigateFluxoProcesso),
    map((action: NavigateFluxoProcesso) =>
      action.idFluxoProcesso ?
        new NavigateFluxoProcessoSuccess(action.idFluxoProcesso) :
        new CreateFluxoProcesso(action.programa.nome, action.programa.id)));

  @Effect({ dispatch: false })
  navigateFluxoProcessoSuccess$ = this.actions$.pipe(
    ofType(FluxoProcessoActionTypes.NavigateFluxoProcessoSuccess),
    map((action: NavigateFluxoProcessoSuccess) => this.router.navigate([`/fluxo-processo/${action.idFluxoProcesso}`])));
}
