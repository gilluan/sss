import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { FluxoProcessoEffects } from './fluxo-processo.effects';

describe('FluxoProcessoEffects', () => {
  let actions$: Observable<any>;
  let effects: FluxoProcessoEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FluxoProcessoEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(FluxoProcessoEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
