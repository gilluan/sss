import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { MarcoActionTypes, LoadMarcos, LoadMarcosFail, LoadMarcosSuccess, AddMarco, AddMarcoSuccess, AddMarcoFail, UpdateMarco, UpdateMarcoSuccess, UpdateMarcoFail, LoadMarcoByIdFail, LoadMarcoByIdSuccess, LoadMarcoById, DeleteMarco, DeleteMarcoSuccess, DeleteMarcoFail } from '../actions/marco.actions';
import { map, catchError, switchMap } from 'rxjs/operators';
import { MarcoService } from '../marco.service';
import { of } from 'rxjs';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { CustomAlertService } from '@app/shared/services/alert-service';
import { Color } from '@sicoob/ui';
import { Marco } from '../models/marco.model';


@Injectable()
export class MarcoEffects {

  constructor(private actions$: Actions, private readonly marcoService: MarcoService, private alertService: CustomAlertService) { }

  @Effect()
  loadMarcoById$ = this.actions$.pipe(
    ofType(MarcoActionTypes.LoadMarcoById),
    switchMap((action: LoadMarcoById) => this.marcoService.carregaMarcoPorId(action.id).pipe(
      map((marco: Marco) => new LoadMarcoByIdSuccess(marco)),
      catchError((error: any) => of(new LoadMarcoByIdFail(error)))
    )));

  @Effect({ dispatch: false })
  loadMarcoByIdFail$ = this.actions$.pipe(
    ofType(MarcoActionTypes.LoadMarcoByIdFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível recuperar o marco')));

  @Effect()
  loadMarcos$ = this.actions$.pipe(
    ofType(MarcoActionTypes.LoadMarcos),
    switchMap((action: LoadMarcos) => this.marcoService.carregaMarcos(action.filtro).pipe(
      map((pagedData: PagedDataModelVm) => new LoadMarcosSuccess(pagedData)),
      catchError((error: any) => of(new LoadMarcosFail(error)))
    )));

  @Effect({ dispatch: false })
  loadMarcosFail$ = this.actions$.pipe(
    ofType(MarcoActionTypes.LoadMarcosFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível carregar os marcos do processo')));

  @Effect()
  addMarco$ = this.actions$.pipe(
    ofType(MarcoActionTypes.AddMarco),
    switchMap((action: AddMarco) => this.marcoService.salvarMarco(action.marco).pipe(
      switchMap((marco: Marco) => [new AddMarcoSuccess(marco)]),
      catchError((error: any) => of(new AddMarcoFail(error)))
    )));

  @Effect({ dispatch: false })
  addMarcoFail$ = this.actions$.pipe(
    ofType(MarcoActionTypes.AddMarcoFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível cadastrar o marco')));

  @Effect({ dispatch: false })
  addMarcoSuccess$ = this.actions$.pipe(
    ofType(MarcoActionTypes.AddMarcoSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Marco cadastrado com sucesso!')));

  @Effect()
  updateMarco$ = this.actions$.pipe(
    ofType(MarcoActionTypes.UpdateMarco),
    switchMap((action: UpdateMarco) => this.marcoService.alterarMarco(action.marco).pipe(
      map((marco: Marco)=> new UpdateMarcoSuccess(marco)),
      catchError((error: any) => of(new UpdateMarcoFail(error)))
    )));

  @Effect({ dispatch: false })
  updateMarcoFail$ = this.actions$.pipe(
    ofType(MarcoActionTypes.UpdateMarcoFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível alterar o marco')));

  @Effect({ dispatch: false })
  updateMarcoSuccess$ = this.actions$.pipe(
    ofType(MarcoActionTypes.UpdateMarcoSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Marco alterado com sucesso!')));



    @Effect()
  deleteMarco$ = this.actions$.pipe(
    ofType(MarcoActionTypes.DeleteMarco),
    switchMap((action: DeleteMarco) => this.marcoService.excluirMarco(action.id).pipe(
      map(_ => new DeleteMarcoSuccess(action.id)),
      catchError((error: any) => of(new DeleteMarcoFail(error)))
    )));

  @Effect({ dispatch: false })
  deleteMarcoFail$ = this.actions$.pipe(
    ofType(MarcoActionTypes.DeleteMarcoFail),
    map(_ => this.alertService.abrirAlert(Color.DANGER, 'Não foi possível excluir o marco')));

  @Effect({ dispatch: false })
  deleteMarcoSuccess$ = this.actions$.pipe(
    ofType(MarcoActionTypes.DeleteMarcoSuccess),
    map(_ => this.alertService.abrirAlert(Color.SUCCESS, 'Marco excluído com sucesso!')));

}
