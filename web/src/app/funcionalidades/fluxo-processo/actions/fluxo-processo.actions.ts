import {Action} from '@ngrx/store';
import {FluxoProcessoFilter} from '@app/funcionalidades/fluxo-processo/models/fluxo-processo-filter.model';
import {PagedData} from '@shared/models/paged-data.model';
import {FluxoProcesso} from '../models/fluxo-processo.model';
import {FluxoProcessoTotals} from '../models/fluxo-processo-totals.model';
import {Programa} from '@app/funcionalidades/programas/models/programas.model';

export enum FluxoProcessoActionTypes {
  LoadFluxoProcesso = '[LoadFluxoProcesso]',
  LoadFluxoProcessoSuccess = '[LoadFluxoProcessoSuccess]',
  LoadFluxoProcessoFail = '[LoadFluxoProcessoFail]',
  LoadScrollableFluxoProcessoList = '[LoadScrollableFluxoProcessoList]',
  LoadScrollableFluxoProcessoListSuccess = '[LoadScrollableFluxoProcessoListSuccess]',
  LoadScrollableFluxoProcessoListFail = '[LoadScrollableFluxoProcessoListFail]',
  LoadFluxoProcessoList = '[LoadFluxoProcessoList]',
  LoadFluxoProcessoListSuccess = '[LoadFluxoProcessoListSuccess]',
  LoadFluxoProcessoListFail = '[LoadFluxoProcessoListFail]',
  LoadTotalsFluxoProcessoList = '[LoadTotalsFluxoProcessoList]',
  LoadTotalsFluxoProcessoListSuccess = '[LoadTotalsFluxoProcessoListSuccess]',
  LoadTotalsFluxoProcessoListFail = '[LoadTotalsFluxoProcessoListFail]',
  CreateFluxoProcesso = '[CreateFluxoProcesso]',
  CreateFluxoProcessoSuccess = '[CreateFluxoProcessoSuccess]',
  CreateFluxoProcessoFail = '[CreateFluxoProcessoFail]',
  CloneFluxoProcesso = '[CloneFluxoProcesso]',
  CloneFluxoProcessoSuccess = '[CloneFluxoProcessoSuccess]',
  CloneFluxoProcessoFail = '[CloneFluxoProcessoFail]',
  DeleteFluxoProcesso = '[DeleteFluxoProcesso]',
  DeleteFluxoProcessoSuccess = '[DeleteFluxoProcessoSuccess]',
  DeleteFluxoProcessoFail = '[DeleteFluxoProcessoFail]',
  NavigateFluxoProcesso = '[NavigateFluxoProcesso]',
  NavigateFluxoProcessoSuccess = '[NavigateFluxoProcessoSuccess]',
  NavigateFluxoProcessoFail = '[NavigateFluxoProcessoFail]',
  ClearFluxoProcesso = '[ClearFluxoProcesso]',
  NoActionFluxoProcesso = '[NoActionFluxoProcesso]',
}

export class LoadFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcesso;
  constructor(public idFluxoProcesso: String) { }
}

export class LoadFluxoProcessoSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcessoSuccess;
  constructor(public fluxoProcesso: FluxoProcesso) { }
}

export class LoadFluxoProcessoFail implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcessoFail;
  constructor(public payload: any) { }
}

export class LoadScrollableFluxoProcessoList implements Action {
  readonly type = FluxoProcessoActionTypes.LoadScrollableFluxoProcessoList;
  constructor(public filter: FluxoProcessoFilter, public clearStore: boolean = false) { }
}

export class LoadScrollableFluxoProcessoListSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListSuccess;
  constructor(public pagedData: PagedData<FluxoProcesso>) { }
}

export class LoadScrollableFluxoProcessoListFail implements Action {
  readonly type = FluxoProcessoActionTypes.LoadScrollableFluxoProcessoListFail;
  constructor(public payload: any) { }
}

export class LoadFluxoProcessoList implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcessoList;
  constructor(public filter: FluxoProcessoFilter) { }
}

export class LoadFluxoProcessoListSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcessoListSuccess;
  constructor(public pagedData: PagedData<FluxoProcesso>) { }
}

export class LoadFluxoProcessoListFail implements Action {
  readonly type = FluxoProcessoActionTypes.LoadFluxoProcessoListFail;
  constructor(public payload: any) { }
}

export class LoadTotalsFluxoProcessoList implements Action {
  readonly type = FluxoProcessoActionTypes.LoadTotalsFluxoProcessoList;
  constructor(public filter: FluxoProcessoFilter) { }
}

export class LoadTotalsFluxoProcessoListSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.LoadTotalsFluxoProcessoListSuccess;
  constructor(public totals: FluxoProcessoTotals) { }
}

export class LoadTotalsFluxoProcessoListFail implements Action {
  readonly type = FluxoProcessoActionTypes.LoadTotalsFluxoProcessoListFail;
  constructor(public payload: any) { }
}

export class CreateFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.CreateFluxoProcesso;
  constructor(public nome: string, public idPrograma: string) { }
}

export class CreateFluxoProcessoSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.CreateFluxoProcessoSuccess;
  constructor(public fluxoProcesso: FluxoProcesso) { }
}

export class CreateFluxoProcessoFail implements Action {
  readonly type = FluxoProcessoActionTypes.CreateFluxoProcessoFail;
  constructor(public payload: any) { }
}

export class CloneFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.CloneFluxoProcesso;
  constructor(public idFluxoProcesso: string, public nome: string) { }
}

export class CloneFluxoProcessoSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.CloneFluxoProcessoSuccess;
  constructor(public fluxoProcesso: FluxoProcesso) { }
}

export class CloneFluxoProcessoFail implements Action {
  readonly type = FluxoProcessoActionTypes.CloneFluxoProcessoFail;
  constructor(public payload: any) { }
}

export class DeleteFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.DeleteFluxoProcesso;
  constructor(public fluxoProcesso: FluxoProcesso) { }
}

export class DeleteFluxoProcessoSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.DeleteFluxoProcessoSuccess;
  constructor(public idFluxoProcesso: string) { }
}

export class DeleteFluxoProcessoFail implements Action {
  readonly type = FluxoProcessoActionTypes.DeleteFluxoProcessoFail;
  constructor(public payload: any) { }
}

export class NavigateFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.NavigateFluxoProcesso;
  constructor(public programa: Programa, public idFluxoProcesso?: string) { }
}

export class NavigateFluxoProcessoSuccess implements Action {
  readonly type = FluxoProcessoActionTypes.NavigateFluxoProcessoSuccess;
  constructor(public idFluxoProcesso: string) { }
}

export class NavigateFluxoProcessoFail implements Action {
  readonly type = FluxoProcessoActionTypes.NavigateFluxoProcessoFail;
  constructor(public error: any) { }
}

export class ClearFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.ClearFluxoProcesso;
  constructor() { }
}

export class NoActionFluxoProcesso implements Action {
  readonly type = FluxoProcessoActionTypes.NoActionFluxoProcesso;
  constructor() { }
}

export type FluxoProcessoActions =
  LoadFluxoProcessoList
  | LoadFluxoProcessoListSuccess
  | LoadFluxoProcessoListFail
  | LoadTotalsFluxoProcessoList
  | LoadTotalsFluxoProcessoListSuccess
  | LoadTotalsFluxoProcessoListFail
  | ClearFluxoProcesso
  | CreateFluxoProcesso
  | CreateFluxoProcessoSuccess
  | CreateFluxoProcessoFail
  | CloneFluxoProcesso
  | CloneFluxoProcessoSuccess
  | CloneFluxoProcessoFail
  | DeleteFluxoProcesso
  | DeleteFluxoProcessoSuccess
  | DeleteFluxoProcessoFail
  | NoActionFluxoProcesso
  | LoadScrollableFluxoProcessoList
  | LoadScrollableFluxoProcessoListSuccess
  | LoadScrollableFluxoProcessoListFail
  | LoadFluxoProcesso
  | LoadFluxoProcessoSuccess
  | LoadFluxoProcessoFail
  | NavigateFluxoProcesso
  | NavigateFluxoProcessoSuccess
  | NavigateFluxoProcessoFail;


