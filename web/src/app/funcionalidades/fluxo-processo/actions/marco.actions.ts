import { Action } from '@ngrx/store';
import { Marco } from '../models/marco.model';
import { MarcoFiltro } from '../models/marco.filtro';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';

export enum MarcoActionTypes {
  LoadMarcoById = '[LoadMarcoById] Marco',
  LoadMarcoByIdSuccess = '[LoadMarcoByIdSuccess] Marco',
  LoadMarcoByIdFail = '[LoadMarcoByIdFail] Marco',
  LoadMarcos = '[LoadMarcos] Marcos',
  LoadMarcosSuccess = '[LoadMarcosSuccess] Marcos',
  LoadMarcosFail = '[LoadMarcosFail] Marcos',
  AddMarco = '[AddMarco] Marco',
  AddMarcoSuccess = '[AddMarcoSuccess] Marco',
  AddMarcoFail = '[AddMarcoFail] Marco',
  UpdateMarco = '[UpdateMarco] Marco',
  UpdateMarcoSuccess = '[UpdateMarcoSuccess] Marco',
  UpdateMarcoFail = '[UpdateMarcoFail] Marco',
  CleanUpdateMarco = '[CleanUpdateMarco] Marco',
  DeleteMarco = '[DeleteMarco] Marco',
  DeleteMarcoSuccess = '[DeleteMarcoSuccess] Marco',
  DeleteMarcoFail = '[DeleteMarcoFail] Marco',
  ClearMarcos = '[Marco] Clear Marcos'
}

export class LoadMarcoById implements Action {
  readonly type = MarcoActionTypes.LoadMarcoById;
  constructor(public id: string) { }
}
export class LoadMarcoByIdSuccess implements Action {
  readonly type = MarcoActionTypes.LoadMarcoByIdSuccess;
  constructor(public marco: Marco) { }
}
export class LoadMarcoByIdFail implements Action {
  readonly type = MarcoActionTypes.LoadMarcoByIdFail;
  constructor(public error: any) { }
}

export class LoadMarcos implements Action {
  readonly type = MarcoActionTypes.LoadMarcos;
  constructor(public filtro: MarcoFiltro) { }
}
export class LoadMarcosSuccess implements Action {
  readonly type = MarcoActionTypes.LoadMarcosSuccess;
  constructor(public pagedData: PagedDataModelVm) { }
}
export class LoadMarcosFail implements Action {
  readonly type = MarcoActionTypes.LoadMarcosFail;
  constructor(public error: any) { }
}

export class AddMarco implements Action {
  readonly type = MarcoActionTypes.AddMarco;
  constructor(public marco: Marco) { }
}
export class AddMarcoSuccess implements Action {
  readonly type = MarcoActionTypes.AddMarcoSuccess;
  constructor(public marco: Marco) { }
}
export class AddMarcoFail implements Action {
  readonly type = MarcoActionTypes.AddMarcoFail;
  constructor(public error: any) { }
}

export class UpdateMarco implements Action {
  readonly type = MarcoActionTypes.UpdateMarco;
  constructor(public marco: Marco) { }
}
export class UpdateMarcoSuccess implements Action {
  readonly type = MarcoActionTypes.UpdateMarcoSuccess;
  constructor(public marco: Marco) { }
}
export class UpdateMarcoFail implements Action {
  readonly type = MarcoActionTypes.UpdateMarcoFail;
  constructor(public error: any) { }
}
export class CleanUpdateMarco implements Action {
  readonly type = MarcoActionTypes.CleanUpdateMarco;
  constructor() { }
}

export class DeleteMarco implements Action {
  readonly type = MarcoActionTypes.DeleteMarco;
  constructor(public id: string) { }
}
export class DeleteMarcoSuccess implements Action {
  readonly type = MarcoActionTypes.DeleteMarcoSuccess;
  constructor(public id: string) { }
}
export class DeleteMarcoFail implements Action {
  readonly type = MarcoActionTypes.DeleteMarcoFail;
  constructor(public error: any) { }
}

export class ClearMarcos implements Action {
  readonly type = MarcoActionTypes.ClearMarcos;
}

export type MarcoActions =
  | LoadMarcoById
  | LoadMarcoByIdSuccess
  | LoadMarcoByIdFail
  | LoadMarcos
  | LoadMarcosSuccess
  | LoadMarcosFail
  | AddMarco
  | AddMarcoSuccess
  | AddMarcoFail
  | UpdateMarco
  | UpdateMarcoSuccess
  | UpdateMarcoFail
  | CleanUpdateMarco
  | DeleteMarco
  | DeleteMarcoSuccess
  | DeleteMarcoFail
  | ClearMarcos;
