import {Action} from '@ngrx/store';
import {ActionBarRef} from '@sicoob/ui';
import {PassoFluxo} from '../models/passo-fluxo.model';
import {TotaisPassoSituacao} from '@app/funcionalidades/fluxo-processo/models/totais-passo-situacao.model';

export enum PassoFluxoActionTypes {
  LoadPassoFluxo = '[LoadPassoFluxo]',
  LoadPassoFluxoSuccess = '[LoadPassoFluxoSuccess]',
  LoadPassoFluxoFail = '[LoadPassoFluxoFail]',
  LoadPassoFluxoById = '[LoadPassoFluxoById]',
  LoadPassoFluxoByIdSuccess = '[LoadPassoFluxoByIdSuccess]',
  LoadPassoFluxoByIdFail = '[LoadPassoFluxoByIdFail]',
  CreatePassoFluxo = '[CreatePassoFluxo]',
  CreatePassoFluxoSuccess = '[CreatePassoFluxoSuccess]',
  CreatePassoFluxoFail = '[CreatePassoFluxoFail]',
  UpdatePassoFluxo = '[UpdatePassoFluxo]',
  UpdatePassoFluxoSuccess = '[UpdatePassoFluxoSuccess]',
  UpdatePassoFluxoFail = '[UpdatePassoFluxoFail]',
  DeletePassoFluxo = '[DeletePassoFluxo]',
  DeletePassoFluxoSuccess = '[DeletePassoFluxoSuccess]',
  DeletePassoFluxoFail = '[DeletePassoFluxoFail]',
  OpenManterPassoSimplesActionBar = '[OpenManterPassoSimplesActionBar]',
  OpenManterPassoSimplesActionBarSuccess = '[OpenManterPassoSimplesActionBarSuccess]',
  OpenManterPassoSimplesActionBarFail = '[OpenManterPassoSimplesActionBarFail]',
  SavePassoNavigateToForm = '[SavePassoNavigateToForm]',
  SavePassoNavigateToFormSuccess = '[SavePassoNavigateToFormSuccess]',
  SavePassoNavigateToFormFail = '[SavePassoNavigateToFormFail]',
  ClearPassoFluxo = '[ClearPassoFluxo]',
  NoActionPassoFluxo = '[NoActionPassoFluxo]',
  LoadTotaisPassosSituacao = '[LoadTotaisPassosSituacao]',
  LoadTotaisPassosSituacaoSuccess = '[LoadTotaisPassosSituacaoSuccess]',
  LoadTotaisPassosSituacaoFail = '[LoadTotaisPassosSituacaoFail]',
}

export class LoadPassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxo;
  constructor(public idMarco: string) { }
}

export class LoadPassoFluxoSuccess implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxoSuccess;
  constructor(public passosFluxo: PassoFluxo[]) { }
}

export class LoadPassoFluxoFail implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxoFail;
  constructor(public payload: any) { }
}

export class LoadPassoFluxoById implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxoById;
  constructor(public idPasso: string) { }
}

export class LoadPassoFluxoByIdSuccess implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxoByIdSuccess;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class LoadPassoFluxoByIdFail implements Action {
  readonly type = PassoFluxoActionTypes.LoadPassoFluxoByIdFail;
  constructor(public payload: any) { }
}

export class CreatePassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.CreatePassoFluxo;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class CreatePassoFluxoSuccess implements Action {
  readonly type = PassoFluxoActionTypes.CreatePassoFluxoSuccess;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class CreatePassoFluxoFail implements Action {
  readonly type = PassoFluxoActionTypes.CreatePassoFluxoFail;
  constructor(public payload: any) { }
}

export class UpdatePassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.UpdatePassoFluxo;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class UpdatePassoFluxoSuccess implements Action {
  readonly type = PassoFluxoActionTypes.UpdatePassoFluxoSuccess;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class UpdatePassoFluxoFail implements Action {
  readonly type = PassoFluxoActionTypes.UpdatePassoFluxoFail;
  constructor(public payload: any) { }
}

export class DeletePassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.DeletePassoFluxo;
  constructor(public idPasso: string) { }
}

export class DeletePassoFluxoSuccess implements Action {
  readonly type = PassoFluxoActionTypes.DeletePassoFluxoSuccess;
  constructor(public idPassoExcluido: string, public passosDependentes: PassoFluxo[]) { }
}

export class DeletePassoFluxoFail implements Action {
  readonly type = PassoFluxoActionTypes.DeletePassoFluxoFail;
  constructor(public payload: any) { }
}

export class OpenManterPassoSimplesActionBar implements Action {
  readonly type = PassoFluxoActionTypes.OpenManterPassoSimplesActionBar;
  constructor(public idMarco: string, public idPasso?: string, public idPassoAnterior?: string) { }
}

export class OpenManterPassoSimplesActionBarSuccess implements Action {
  readonly type = PassoFluxoActionTypes.OpenManterPassoSimplesActionBarSuccess;
  constructor(public actionBarRef: ActionBarRef) { }
}

export class OpenManterPassoSimplesActionBarFail implements Action {
  readonly type = PassoFluxoActionTypes.OpenManterPassoSimplesActionBarFail;
  constructor(public payload: any) { }
}

export class SavePassoNavigateToForm implements Action {
  readonly type = PassoFluxoActionTypes.SavePassoNavigateToForm;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class SavePassoNavigateToFormSuccess implements Action {
  readonly type = PassoFluxoActionTypes.SavePassoNavigateToFormSuccess;
  constructor(public passoFluxo: PassoFluxo) { }
}

export class SavePassoNavigateToFormFail implements Action {
  readonly type = PassoFluxoActionTypes.SavePassoNavigateToFormFail;
  constructor(public payload: any) { }
}

export class ClearPassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.ClearPassoFluxo;
  constructor() { }
}

export class NoActionPassoFluxo implements Action {
  readonly type = PassoFluxoActionTypes.NoActionPassoFluxo;
  constructor() { }
}

export class LoadTotaisPassosSituacao implements Action {
  readonly type = PassoFluxoActionTypes.LoadTotaisPassosSituacao;
  constructor(public idResponsavel: string) { }
}

export class LoadTotaisPassosSituacaoSuccess implements Action {
  readonly type = PassoFluxoActionTypes.LoadTotaisPassosSituacaoSuccess;
  constructor(public totais: TotaisPassoSituacao) { }
}

export class LoadTotaisPassosSituacaoFail implements Action {
  readonly type = PassoFluxoActionTypes.LoadTotaisPassosSituacaoFail;
  constructor(public error: any) { }
}


export type PassoFluxoActions =
  OpenManterPassoSimplesActionBar
  | OpenManterPassoSimplesActionBarSuccess
  | OpenManterPassoSimplesActionBarFail
  | CreatePassoFluxo
  | CreatePassoFluxoSuccess
  | CreatePassoFluxoFail
  | NoActionPassoFluxo
  | ClearPassoFluxo
  | LoadPassoFluxo
  | LoadPassoFluxoSuccess
  | LoadPassoFluxoFail
  | UpdatePassoFluxo
  | UpdatePassoFluxoSuccess
  | UpdatePassoFluxoFail
  | DeletePassoFluxo
  | DeletePassoFluxoSuccess
  | DeletePassoFluxoFail
  | SavePassoNavigateToForm
  | SavePassoNavigateToFormSuccess
  | SavePassoNavigateToFormFail
  | LoadPassoFluxoById
  | LoadPassoFluxoByIdSuccess
  | LoadPassoFluxoByIdFail
  | LoadTotaisPassosSituacao
  | LoadTotaisPassosSituacaoSuccess
  | LoadTotaisPassosSituacaoFail;
'';
