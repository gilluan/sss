import { reducer, initialState } from '../reducers/sins-analise-cadastros.reducer';

describe('SinsAnaliseCadastros Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
