import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { VoluntarioDto } from '../../../shared/models/voluntario-dto.model';
import { AnaliseVoluntarioAction, AnaliseVoluntarioActionTypes } from '../actions/analise-voluntario.actions';
import { ModalRef } from '@sicoob/ui';

export interface State extends EntityState<VoluntarioDto> {
  modalSuccessRef: ModalRef
}

export const adapter: EntityAdapter<VoluntarioDto> = createEntityAdapter<VoluntarioDto>();

export const initialState: State = adapter.getInitialState({
  modalSuccessRef: null
});

//assinarVoluntario

export function reducer(
  state = initialState,
  action: AnaliseVoluntarioAction
): State {
  switch (action.type) {
    case AnaliseVoluntarioActionTypes.LoadAnaliseVoluntarioSuccess: {
      state.ids = [];
      state.entities = null;
      return adapter.addOne(action.payload, state);
    }
    case AnaliseVoluntarioActionTypes.AssinarVoluntarioSuccess: {
      state.ids = [];
      state.entities = null;
      return adapter.addOne(action.payload, state);
    }
    case AnaliseVoluntarioActionTypes.CleanAnaliseVoluntario: {
      return adapter.removeAll(state);
    }
    case AnaliseVoluntarioActionTypes.FecharModalSucess: {
      return {
        ...state,
        modalSuccessRef: null
      }
    }
    case AnaliseVoluntarioActionTypes.AddModal: {
      return {
        ...state,
        modalSuccessRef: action.modalRef
      }
    }
    default: {
      return state;
    }
  }
}

export const getAnaliseVoluntarioState = createFeatureSelector<State>('analiseVoluntario');


export const getModalState = createSelector(
  getAnaliseVoluntarioState,
  selectAll => selectAll.modalSuccessRef
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getAnaliseVoluntarioState);
