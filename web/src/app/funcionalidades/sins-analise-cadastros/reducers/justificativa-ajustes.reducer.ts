import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AnaliseVoluntarioAction, AnaliseVoluntarioActionTypes } from '../actions/analise-voluntario.actions';
import { JustificativaAjusteVm } from '../models/dto/justificativa-ajuste-vm.model';

export interface State extends EntityState<JustificativaAjusteVm> {
  modal: { message: string, color: string, title: string };
}

export const adapter: EntityAdapter<JustificativaAjusteVm> = createEntityAdapter<JustificativaAjusteVm>();

export const initialState: State = adapter.getInitialState({
  modal: {
    title: 'Agurdando resposta...',
    color: 'mdi-loading mdi-spin',
    message: 'Estamos analisando a solicitação, por favor aguarde'
  }

});

export function reducer(
  state = initialState,
  action: AnaliseVoluntarioAction
): State {
  switch (action.type) {

    case AnaliseVoluntarioActionTypes.LoadJustificativasAjustesSuccess: {
      return adapter.addAll(action.payload, state);
    }

    case AnaliseVoluntarioActionTypes.LoadJustificativasAjustesFail: {
      state.ids = [];
      state.entities = {};
      return {...state};
    }

    case AnaliseVoluntarioActionTypes.SalvarJustificativaFail: {
      return {
        ...state, modal: {
          title: 'Ocorreu um erro',
          color: 'mdi-close-circle-outline',
          message: 'Ocorreu um erro tentando salvar a justificativa'
        }
      };
    }

    case AnaliseVoluntarioActionTypes.SalvarJustificativaSuccess: {
      return {
        ...state, modal: {
          title: 'Pedido enviado',
          color: 'mdi-check-circle-outline',
          message: `A sua solicitação de ajustes foi encaminhada para o ${action.destinatario}.`
        }
      };
    }

    default: {
      return state;
    }
  }
}

export const getJustificativaAjustes = createFeatureSelector<State>('justificativaAjustes');

export const getModalState = createSelector(
  getJustificativaAjustes,
  selectAll => selectAll.modal
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getJustificativaAjustes);
