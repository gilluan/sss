import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { SinsAnaliseCadastros } from '../models/sins-analise-cadastros.model';
import { SinsAnaliseCadastrosActions, SinsAnaliseCadastrosActionTypes } from '../actions/sins-analise-cadastros.actions';
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface State extends EntityState<SinsAnaliseCadastros> {
  // additional entities state properties
  exibirFiltro: boolean;
}

export const adapter: EntityAdapter<SinsAnaliseCadastros> = createEntityAdapter<SinsAnaliseCadastros>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  exibirFiltro: true
});

export function reducer(
  state = initialState,
  action: SinsAnaliseCadastrosActions
): State {
  switch (action.type) {
    case SinsAnaliseCadastrosActionTypes.AddSinsAnaliseCadastros: {
      return adapter.addOne(action.payload.sinsAnaliseCadastros, state);
    }

    case SinsAnaliseCadastrosActionTypes.UpsertSinsAnaliseCadastros: {
      return adapter.upsertOne(action.payload.sinsAnaliseCadastros, state);
    }

    case SinsAnaliseCadastrosActionTypes.AddSinsAnaliseCadastross: {
      return adapter.addMany(action.payload.sinsAnaliseCadastross, state);
    }

    case SinsAnaliseCadastrosActionTypes.UpsertSinsAnaliseCadastross: {
      return adapter.upsertMany(action.payload.sinsAnaliseCadastross, state);
    }

    case SinsAnaliseCadastrosActionTypes.UpdateSinsAnaliseCadastros: {
      return adapter.updateOne(action.payload.sinsAnaliseCadastros, state);
    }

    case SinsAnaliseCadastrosActionTypes.UpdateSinsAnaliseCadastross: {
      return adapter.updateMany(action.payload.sinsAnaliseCadastross, state);
    }

    case SinsAnaliseCadastrosActionTypes.DeleteSinsAnaliseCadastros: {
      return adapter.removeOne(action.payload.id, state);
    }

    case SinsAnaliseCadastrosActionTypes.DeleteSinsAnaliseCadastross: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case SinsAnaliseCadastrosActionTypes.LoadSinsAnaliseCadastross: {
      return adapter.addAll(action.payload.sinsAnaliseCadastross, state);
    }

    case SinsAnaliseCadastrosActionTypes.ClearSinsAnaliseCadastross: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const selectAuthState = createFeatureSelector<State>('sinsAnaliseCadastros');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
