import {createEntityAdapter, EntityAdapter, EntityState} from "@ngrx/entity";
import {AnaliseVoluntarioAction, AnaliseVoluntarioActionTypes} from "../actions/analise-voluntario.actions";
import {PagedDataModelVm} from "../../../shared/models/paged-data.model";
import {createFeatureSelector} from "@ngrx/store";

export interface State extends EntityState<PagedDataModelVm> { }

export const adapter: EntityAdapter<PagedDataModelVm> = createEntityAdapter<PagedDataModelVm>();

export const initialState: State = adapter.getInitialState({ });

export function reducer(
  state = initialState,
  action: AnaliseVoluntarioAction
): State {
  switch (action.type) {
    case AnaliseVoluntarioActionTypes.LoadVoluntariosAnaliseSuccess: {
      state.ids = [0];
      state.entities = { 0: action.payload };
      return {...state};
    }

    case AnaliseVoluntarioActionTypes.LoadVoluntariosAnaliseFail: {
      state.ids = [];
      state.entities = {};
      return {...state};
    }

    default: {
      return state;
    }
  }
}

export const getVoluntariosAnalise = createFeatureSelector<State>('voluntariosAnalise');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors(getVoluntariosAnalise);
