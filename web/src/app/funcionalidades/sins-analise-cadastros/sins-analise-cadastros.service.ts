import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {PagedDataModelVm} from "../../shared/models/paged-data.model";
import {VoluntarioFiltroDto} from "../../shared/models/voluntario-filtro.model";
import {TotalSituacaoVm} from "./models/dto/total-situacao-vm.model";
import {VoluntarioDto} from "../../shared/models/voluntario-dto.model";

@Injectable({providedIn: 'root'})
export class SinsAnaliseCadastrosService {

  private pesquisarVoluntariosSource = new Subject<VoluntarioFiltroDto>();
  private retornarVoluntariosSource = new Subject<PagedDataModelVm>();

  private pesquisarTotaisSituacaoSource = new Subject();
  private retornarTotaisSituacaoSource = new Subject<TotalSituacaoVm[]>();

  private assinarContratoSource = new Subject<string>();
  private retornarAssinaturaContratoSource = new Subject<VoluntarioDto>();


  pesquisarVoluntarios$ = this.pesquisarVoluntariosSource.asObservable();
  retornarVoluntarios$ = this.retornarVoluntariosSource.asObservable();

  pesquisarTotais$ = this.pesquisarTotaisSituacaoSource.asObservable();
  retornarTotais$ = this.retornarTotaisSituacaoSource.asObservable();

  assinarContrato$ = this.assinarContratoSource.asObservable();
  retornarAssinaturaContrato$ = this.retornarAssinaturaContratoSource.asObservable();

  pesquisarVoluntarios = (filtro: VoluntarioFiltroDto) => this.pesquisarVoluntariosSource.next(filtro);
  retornarVoluntarios = (voluntarios: PagedDataModelVm) => this.retornarVoluntariosSource.next(voluntarios);

  pesquisarTotaisSituacao = () => this.pesquisarTotaisSituacaoSource.next();
  retornarTotaisSituacao = (totais: TotalSituacaoVm[]) => this.retornarTotaisSituacaoSource.next(totais);

  assinarContrato = (id: string) => this.assinarContratoSource.next(id);
  retornarAssinaturaContrato = (voluntario: VoluntarioDto) => this.retornarAssinaturaContratoSource.next(voluntario);
}
