import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import * as fromApp from '../../../../app.reduce';
import {select, Store} from '@ngrx/store';
import {DisplayToolbar} from 'src/app/app.actions';
import * as fromVoluntario from "../../../../reducers/voluntario.reducer";
import {PagedDataModelVm} from "../../../../shared/models/paged-data.model";
import {VoluntarioDto} from "../../../../shared/models/voluntario-dto.model";
import {TotalSituacaoVm} from "../../models/dto/total-situacao-vm.model";
import {VoluntarioFiltroDto} from "../../../../shared/models/voluntario-filtro.model";
import {CarregarVoluntariosPorFiltro} from "../../../../actions/voluntarioAction";
import {TemplatePortal} from "@angular/cdk/portal";
import {HeaderActionsContainerService} from "@sicoob/ui";
import {PageModelVm} from "../../../../shared/models/page.model";
import {SituacaoVoluntarioType} from "../../../../shared/types/situacao-voluntario.type";

@Component({
  selector: 'sc-container-sins-lista-cadastros',
  templateUrl: './container-sins-lista-cadastros.component.html',
  styleUrls: ['./container-sins-lista-cadastros.component.scss']
})
export class ContainerSinsListaCadastrosComponent implements OnInit, AfterViewInit, OnDestroy {
  subscription: Subscription = new Subscription();

  voluntarios$: VoluntarioDto[];
  totais$: TotalSituacaoVm[];
  filtro$: VoluntarioFiltroDto;

  @ViewChild("headerTpl") headerTpl: TemplateRef<any>;

  constructor(
    private readonly appStore$: Store<fromApp.State>,
    private readonly voluntarioStore$: Store<fromVoluntario.State>,
    private readonly headerActionsService: HeaderActionsContainerService,
    private readonly changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.appStore$.dispatch(new DisplayToolbar(true));

    this.initFilter();
    this.pesquisar();

    let pesquisarVoluntarios = this.voluntarioStore$.pipe(select(fromVoluntario.selectVolunatarioPaginadoComTotais));
    this.subscription
      .add(pesquisarVoluntarios.subscribe(state => this.onRetornarVoluntarios(state.dadosPaginados, state.totais)));
  }

  ngAfterViewInit(): void {
    this.headerActionsService.open(new TemplatePortal(this.headerTpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.headerActionsService.remove();
    this.subscription.unsubscribe();
  }

  pesquisar(): void {
    this.voluntarioStore$.dispatch(new CarregarVoluntariosPorFiltro(this.filtro$));
  }

  private onRetornarVoluntarios(dadosPaginados: PagedDataModelVm, totais: TotalSituacaoVm[]): void {
    this.voluntarios$ = dadosPaginados.data;
    this.filtro$.paginacao = dadosPaginados.page;
    this.totais$  = totais;
    this.changeDetectorRef.detectChanges();
  }

  private initFilter() {
    this.filtro$ = new VoluntarioFiltroDto();
    this.filtro$.situacao = SituacaoVoluntarioType.aguardandoAnalise;
    this.filtro$.paginacao = new PageModelVm(0, 6, 0);
  }
}
