import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { VoluntarioDto } from "../../../../shared/models/voluntario-dto.model";
import { select, Store } from "@ngrx/store";
import * as fromJustificativaAjustes from "../../reducers/justificativa-ajustes.reducer";
import { VoluntarioFiltroDto } from "../../../../shared/models/voluntario-filtro.model";
import { SituacaoVoluntarioType } from "../../models/types/situacao-voluntario.type";
import { PageModelVm } from "../../../../shared/models/page.model";
import * as fromVoluntario from "../../../../reducers/voluntario.reducer";
import { AssinarContratoVoluntario, CarregarVoluntariosPorFiltro } from "../../../../actions/voluntarioAction";
import { Subscription } from "rxjs";
import { PagedDataModelVm } from "../../../../shared/models/paged-data.model";
import { ModalService, ModalRef } from "@sicoob/ui";
import { DialogoJustificativaAjustesComponent } from "../../../../shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component";
import * as fromApp from '../../../../app.reduce';
import { DisplayToolbar } from 'src/app/app.actions';
import { FecharModal, SalvarJustificativa } from '../../actions/analise-voluntario.actions';
import * as fromAuth from '@sicoob/security';
import { withLatestFrom } from 'rxjs/operators';
import { UsuarioSicoob } from '@sicoob/security';
import { JustificativaAjusteVm } from '../../models/dto/justificativa-ajuste-vm.model';
import { PerfilType } from 'src/app/shared/types/perfil.type';
import { PersistanceService } from '@app/shared/services/persistence.service';


@Component({
  selector: 'app-container-assinatura-cadastro',
  templateUrl: './container-assinatura-cadastro.component.html',
  styleUrls: ['./container-assinatura-cadastro.component.scss']
})
export class ContainerAssinaturaCadastroComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription();

  voluntarios$: VoluntarioDto[];
  filtro$: VoluntarioFiltroDto;
  total$: number;
  pesquisaFiltrada$: boolean = false;
  modalRef: ModalRef;

  constructor(
    private readonly modalService: ModalService,
    private readonly voluntarioStore$: Store<fromVoluntario.State>,
    private readonly justificativaStore$: Store<fromJustificativaAjustes.State>,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly appStore$: Store<fromApp.State>,
    public authStore$: Store<fromAuth.State>,
    public persistanceService: PersistanceService
  ) { }

  ngOnInit(): void {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.initFilter();
    this.pesquisar();

    let pesquisarVoluntario$ = this.voluntarioStore$.pipe(select(fromVoluntario.selectVoluntarioPaginado));
    this.subscription
      .add(pesquisarVoluntario$.subscribe((dadosPaginados: PagedDataModelVm) => this.pesquisarVoluntarioSubscriber(dadosPaginados)));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    if (this.modalRef)
      this.modalRef.close();
    this.voluntarioStore$.dispatch(new FecharModal());

    if (this.selectSicoobUser$)
      this.selectSicoobUser$.unsubscribe();
  }

  pesquisar(): void {
    this.voluntarioStore$.dispatch(new CarregarVoluntariosPorFiltro(this.filtro$));
  }

  assinar(voluntario: VoluntarioDto): void {
    this.voluntarioStore$.dispatch(new AssinarContratoVoluntario(voluntario.id))
  }

  selectSicoobUser$: Subscription;

  solicitarAjuste(voluntario: VoluntarioDto): void {
    let config = {
      data: {},
      panelClass: 'sins-modal-justificativa'
    };
    // FIXME mover lógica de ajuste para o redux de voluntário.
    this.modalRef = this.modalService.open(DialogoJustificativaAjustesComponent, config);
    this.modalRef.afterClosed().subscribe((retorno: any) => {
      if (retorno.sucesso) {
        let justificativaAjuste = new JustificativaAjusteVm();
        justificativaAjuste.justificativa = retorno.justificativaAjusteText;
        justificativaAjuste.idVoluntario = voluntario.id;
        justificativaAjuste.idVoluntarioCriador = this.persistanceService.get("usuario_instituto").id;;
        this.justificativaStore$.dispatch(new SalvarJustificativa(justificativaAjuste, voluntario.cpf, PerfilType.gestor));
      }
    });

  }

  private initFilter(): void {
    this.filtro$ = new VoluntarioFiltroDto();
    this.filtro$.situacao = SituacaoVoluntarioType.aguardandoAssinatura;
    this.filtro$.paginacao = new PageModelVm(0, 6, 0);
  }

  private pesquisarVoluntarioSubscriber(dadosPaginados: PagedDataModelVm): void {
    this.total$ = dadosPaginados.page.total;
    this.voluntarios$ = dadosPaginados.data;
    this.filtro$.paginacao = dadosPaginados.page;
    this.pesquisaFiltrada$ = this.filtro$.nome != null;
    this.changeDetectorRef.detectChanges();
  }
}
