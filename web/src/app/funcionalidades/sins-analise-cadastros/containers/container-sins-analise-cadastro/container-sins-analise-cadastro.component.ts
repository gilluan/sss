import { TemplatePortal } from '@angular/cdk/portal';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Route } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { HeaderActionsContainerService, ModalRef, ModalService } from '@sicoob/ui';
import { Subscription } from 'rxjs';
import { DisplayToolbar } from 'src/app/app.actions';
import { DialogoSucessoComponent } from 'src/app/shared/components/dialogo-sucesso/dialogo-sucesso.component';
import { GedService } from 'src/app/shared/ged/ged.service';
import { VoluntarioDto } from 'src/app/shared/models/voluntario-dto.model';
import { PersistanceService } from 'src/app/shared/services/persistence.service';
import * as fromApp from '../../../../app.reduce';
import { DialogoJustificativaAjustesComponent } from '../../../../shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import { AssinarVoluntario, FecharModal, LoadAnaliseVoluntario, LoadJustificativasAjustes, SalvarJustificativa, CleanAnaliseVoluntario } from '../../actions/analise-voluntario.actions';
import { JustificativaAjusteVm } from '../../models/dto/justificativa-ajuste-vm.model';
import * as fromAnaliseVoluntario from '../../reducers/analise-volutario.reducer';
import * as fromJustificativaAjustes from '../../reducers/justificativa-ajustes.reducer';
import { JustificativaType } from '@app/shared/models/justificativa.type';
import { BreadcrumbService } from '@app/shared/components/breadcrumb/breadcrumb.service';



@Component({
  selector: 'sc-container-sins-analise-cadastro',
  templateUrl: './container-sins-analise-cadastro.component.html',
  styleUrls: ['./container-sins-analise-cadastro.component.scss']
})
export class ContainerSinsAnaliseCadastroComponent implements OnInit, OnDestroy {

  @ViewChild("tpl") headerActionsTmpl: TemplateRef<any>;

  assinatura: boolean = false;

  justificativasAjustes: JustificativaAjusteVm[];
  voluntario: VoluntarioDto;
  pdfSrc;

  msgDialog: string = "Seu pedido foi enviado para assinatura! Se o voluntário for aprovado ele será encaminhado para o banco de voluntário";

  isCollapsed = false;
  dadosClass: string = "close";
  justifClass: string = "closeJ";
  buttonClass: string;
  title: string;
  modalJustificativaRef: ModalRef;
  arrayJustificativa$: Subscription;
  selectVoluntario$: Subscription;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private headerActionsService: HeaderActionsContainerService,
    private store: Store<fromAnaliseVoluntario.State>,
    private storeJustificativas: Store<fromJustificativaAjustes.State>,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    private router: Router,
    private gedService: GedService,
    private persistenceService: PersistanceService,
    public appStore$: Store<fromApp.State>,
    private breadcrumbService: BreadcrumbService,
  ) { }

  ngAfterViewInit() {
    this.headerActionsService.open(new TemplatePortal(this.headerActionsTmpl, undefined, {}));
  }

  navigate() {
    this.router.navigate(['/analise-cadastros']);
  }

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar(true));
    this.activatedRoute.params.subscribe(params => {
      this.store.dispatch(new LoadAnaliseVoluntario(params['cpf']));
    });
    this.loadVoluntario();


  }

  loadVoluntario() {
    this.selectVoluntario$ = this.store.pipe(select(fromAnaliseVoluntario.selectAll)).subscribe((vol: VoluntarioDto[]) => {
      if (vol != null && vol != undefined && vol.length > 0) {
        this.store.dispatch(new LoadJustificativasAjustes(vol[0].id));
        this.voluntario = vol[0];
        this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, this.voluntario.nome);
        this.assinatura = (vol[0].situacao == "aguardandoAssinatura" || vol[0].situacao == "assinado");
        this.changeDetector.detectChanges();
        if (this.voluntario.idDocumentoCertificado != null && this.voluntario.idDocumentoCertificado != undefined) {
          this.gedService.pesquisarDocumento(this.voluntario.idDocumentoCertificado).subscribe(
            (r) => {
              this.pdfSrc = this.base64ToArrayBuffer(r.resultado.listaSequenciaisDocumento[0].arquivoCodificadoBase64);
              this.changeDetector.detectChanges();
            }
          ),
            (error) => console.error(error);
        }
      }
    },
      (erro) => console.log(erro)
    );

    this.arrayJustificativa$ = this.storeJustificativas.pipe(select(fromJustificativaAjustes.selectAll)).subscribe(
      arrayJustificativa => {
        this.justificativasAjustes = arrayJustificativa;
        this.changeDetector.detectChanges();
      }
    );
  }

  aprovarCadastro() {
    if (this.voluntario != null && this.voluntario != undefined) {
      let data = {
        header: "Agurdando resposta...",
        buttonClass: "mdi-loading mdi-spin",
        textButton: "Voltar para análise de voluntários",
        rota: "/analise-cadastros",
        message: "Estamos analisando a solicitação, por favor aguarde",
      }
      let modalSucessoref = this.modalService.open(DialogoSucessoComponent, { data: data, panelClass: 'sins-modal-success' });
      this.store.dispatch(new AssinarVoluntario(this.voluntario.id, modalSucessoref));
    }
  }

  solicitarAjustes() {
    this.modalJustificativaRef = this.modalService.open(DialogoJustificativaAjustesComponent, {
      data: {},
      panelClass: 'sins-modal-justificativa'
    });
    this.modalJustificativaRef.afterClosed().subscribe((retorno: any) => {
      if (retorno.sucesso) {
        let justificativaAjuste = new JustificativaAjusteVm();
        justificativaAjuste.justificativa = retorno.justificativaAjusteText;
        justificativaAjuste.idVoluntario = this.voluntario.id;
        justificativaAjuste.tipo = JustificativaType.analiseVoluntario;
        justificativaAjuste.idVoluntarioCriador = this.persistenceService.get("usuario_instituto").id;
        this.storeJustificativas.dispatch(new SalvarJustificativa(justificativaAjuste, this.voluntario.cpf, 'voluntário'));
      }
    });
  }

  private base64ToArrayBuffer(base64: string) {
    let binary_string = window.atob(base64);
    let len = binary_string.length;
    let bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  ngOnDestroy() {
    this.changeDetector.detach();
    this.headerActionsService.remove();
    if (this.modalJustificativaRef)
      this.modalJustificativaRef.close();
    if (this.arrayJustificativa$)
      this.arrayJustificativa$.unsubscribe();
    if (this.selectVoluntario$)
      this.selectVoluntario$.unsubscribe();
    this.store.dispatch(new CleanAnaliseVoluntario());
    this.store.dispatch(new FecharModal());
  }




}
