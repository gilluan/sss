import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {JustificativaAjusteVm} from "./models/dto/justificativa-ajuste-vm.model";
import {Service} from "src/app/shared/services/service";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class JustificativaAjustesService extends Service{

  RESOURCE_URL = `${environment.modulo_gestao_pessoa}/justificativas`;


  constructor(private http: HttpClient) {super() }

  findJustificativasByIdVoluntario(idVoluntario: string): Observable<JustificativaAjusteVm[]> {
    return this.http.get<JustificativaAjusteVm[]>(`${this.RESOURCE_URL}?idVoluntario=${idVoluntario}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  findJustificativasByIdAcao(idAcao: string): Observable<JustificativaAjusteVm> {
    return this.http.get<JustificativaAjusteVm[]>(`${this.RESOURCE_URL}?idAcao=${idAcao}`, httpOptions)
      .pipe(
        map( jus => jus[0]),
        catchError(this.handleError)
      );
  }

  salvarJustificativa(justificativaAjuste: JustificativaAjusteVm): Observable<JustificativaAjusteVm> {
    return this.http.post<JustificativaAjusteVm>(this.RESOURCE_URL, justificativaAjuste, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }





}
