import {Action} from '@ngrx/store';
import {VoluntarioDto} from '../../../shared/models/voluntario-dto.model';
import {JustificativaAjusteVm} from '../models/dto/justificativa-ajuste-vm.model';
import {PagedDataModelVm} from "../../../shared/models/paged-data.model";
import {VoluntarioFiltroDto} from "../../../shared/models/voluntario-filtro.model";
import {ModalRef} from '@sicoob/ui';

export enum AnaliseVoluntarioActionTypes {
  LoadAnaliseVoluntario = '[LoadAnaliseVoluntario] Load LoadAnaliseVoluntarios',
  LoadAnaliseVoluntarioSuccess = '[LoadAnaliseVoluntarioSuccess] Carregar AnaliseVoluntario Sucesso',
  LoadAnaliseVoluntarioFail = '[LoadAnaliseVoluntarioFail] Carregar AnaliseVoluntario Erro',
  UpdateAnaliseVoluntario = '[UpdateAnaliseVoluntario] Load UpdateAnaliseVoluntario',
  UpdateAnaliseVoluntarioSucess = '[UpdateAnaliseVoluntarioSucess] Load UpdateAnaliseVoluntarioSucess',
  UpdateAnaliseVoluntarioFail = '[UpdateAnaliseVoluntarioFail] Load UpdateAnaliseVoluntarioFail',
  OpenDialogo = "[OpenDialogo]",
  CleanButton = "[CleanButton]",
  LoadJustificativasAjustes = "[LoadJustificativasAjustes]",
  LoadJustificativasAjustesSuccess = "[LoadJustificativasAjustesSuccess]",
  LoadJustificativasAjustesFail = "[LoadJustificativasAjustesFail]",
  SalvarJustificativa = "[SalvarJustificativa]",
  SalvarJustificativaSuccess = "[SalvarJustificativaSuccess]",
  SalvarJustificativaClose = "[SalvarJustificativaClose]",
  SalvarJustificativaFail = "[SalvarJustificativaFail]",
  AssinarVoluntario = "[AssinarVoluntario]",
  AssinarVoluntarioSuccess = "[AssinarVoluntarioSuccess]",
  AssinarVoluntarioFail = "[AssinarVoluntarioFail]",
  AssinarVoluntarioClose  = "[AssinarVoluntarioClose]",
  LoadVoluntariosAnalise = "[LoadVoluntariosAnalise]",
  LoadVoluntariosAnaliseSuccess = "[LoadVoluntariosAnaliseSuccess]",
  LoadVoluntariosAnaliseFail = "[LoadVoluntariosAnaliseFail]",
  AbrirModal = "[AbrirModal]",
  FecharModal = "[FecharModal]",
  AddModal = '[AddModal]',
  FecharModalSucess = "[FecharModalSucess]",
  CleanAnaliseVoluntario = "[CleanAnaliseVoluntario]",
}

export class LoadAnaliseVoluntario implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadAnaliseVoluntario;
  constructor(public cpf: string ) {}
}

export class LoadAnaliseVoluntarioSuccess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadAnaliseVoluntarioSuccess;
  constructor(public payload: VoluntarioDto ) {}
}

export class LoadAnaliseVoluntarioFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadAnaliseVoluntarioFail;
  constructor(public payload: VoluntarioDto ) {}
}

export class UpdateAnaliseVoluntario implements Action {
  readonly type = AnaliseVoluntarioActionTypes.UpdateAnaliseVoluntario;
  constructor(public payload: VoluntarioDto ) {}
}

export class UpdateAnaliseVoluntarioSucess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.UpdateAnaliseVoluntarioSucess;
  constructor(public id: string, public changes: Partial<VoluntarioDto>) {}
}

export class UpdateAnaliseVoluntarioFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.UpdateAnaliseVoluntarioFail;
  constructor(public payload: VoluntarioDto ) {}
}

export class OpenDialogo implements Action {
  readonly type = AnaliseVoluntarioActionTypes.OpenDialogo;
}

export class CleanButton implements Action {
  readonly type = AnaliseVoluntarioActionTypes.CleanButton;
}

export class LoadJustificativasAjustes implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadJustificativasAjustes;
  constructor(public idVoluntario: string) {}
}

export class LoadJustificativasAjustesSuccess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadJustificativasAjustesSuccess;
  constructor(public payload: JustificativaAjusteVm[]) {}
}

export class LoadJustificativasAjustesFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadJustificativasAjustesFail;
  constructor(public erro: any) {}
}


export class SalvarJustificativa implements Action {
  readonly type = AnaliseVoluntarioActionTypes.SalvarJustificativa;
  constructor(public justificativa: JustificativaAjusteVm, public cpf: string, public destinatario: string) {}
}

export class SalvarJustificativaSuccess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.SalvarJustificativaSuccess;
  constructor(public justificativa:  JustificativaAjusteVm, public cpf: string, public destinatario: string) {}
}

export class SalvarJustificativaClose implements Action {
  readonly type = AnaliseVoluntarioActionTypes.SalvarJustificativaClose;
  constructor() {}
}


export class SalvarJustificativaFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.SalvarJustificativaFail;
  constructor(public payload: any) {}
}

export class AssinarVoluntario implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AssinarVoluntario;
  constructor(public id: string, public ref: ModalRef ) {}
}

export class AssinarVoluntarioSuccess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AssinarVoluntarioSuccess;
  constructor(public payload: VoluntarioDto ) {}
}

export class AssinarVoluntarioFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AssinarVoluntarioFail;
  constructor(public payload: VoluntarioDto ) {}
}

export class AssinarVoluntarioClose implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AssinarVoluntarioClose;
}

export class LoadVoluntario implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadVoluntariosAnalise;
  constructor(public filter: VoluntarioFiltroDto ) {}
}

export class LoadVoluntarioSuccess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadVoluntariosAnaliseSuccess;
  constructor(public payload: PagedDataModelVm) {}
}

export class LoadVoluntarioFail implements Action {
  readonly type = AnaliseVoluntarioActionTypes.LoadVoluntariosAnaliseFail;
  constructor(public payload: PagedDataModelVm) {}
}

export class AbrirModal implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AbrirModal;
  constructor(public data: any) {}
}
export class FecharModal implements Action {
  readonly type = AnaliseVoluntarioActionTypes.FecharModal;
  constructor() {}
}
export class AddModal implements Action {
  readonly type = AnaliseVoluntarioActionTypes.AddModal;
  constructor(public modalRef: ModalRef) {}
}

export class FecharModalSucess implements Action {
  readonly type = AnaliseVoluntarioActionTypes.FecharModalSucess;
  constructor() {}
}

export class CleanAnaliseVoluntario implements Action {
  readonly type = AnaliseVoluntarioActionTypes.CleanAnaliseVoluntario;
  constructor() {}
}

export type AnaliseVoluntarioAction =
CleanAnaliseVoluntario |
LoadAnaliseVoluntario
| LoadAnaliseVoluntarioFail
| LoadAnaliseVoluntarioSuccess
| UpdateAnaliseVoluntario
| UpdateAnaliseVoluntarioSucess
| UpdateAnaliseVoluntarioFail
| OpenDialogo
| CleanButton
| LoadJustificativasAjustes
| LoadJustificativasAjustesSuccess
| LoadJustificativasAjustesFail
| SalvarJustificativa
| SalvarJustificativaFail
| SalvarJustificativaSuccess
| AssinarVoluntario
| AssinarVoluntarioFail
| AssinarVoluntarioSuccess
| LoadVoluntario
| LoadVoluntarioSuccess
| LoadVoluntarioFail
| SalvarJustificativaClose
| AssinarVoluntarioClose
| FecharModal
| AbrirModal
| AddModal
| FecharModalSucess;
