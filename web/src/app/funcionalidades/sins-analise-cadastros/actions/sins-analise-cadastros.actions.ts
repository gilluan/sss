import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { SinsAnaliseCadastros } from '../models/sins-analise-cadastros.model';

export enum SinsAnaliseCadastrosActionTypes {
  LoadSinsAnaliseCadastross = '[SinsAnaliseCadastros] Load SinsAnaliseCadastross',
  AddSinsAnaliseCadastros = '[SinsAnaliseCadastros] Add SinsAnaliseCadastros',
  UpsertSinsAnaliseCadastros = '[SinsAnaliseCadastros] Upsert SinsAnaliseCadastros',
  AddSinsAnaliseCadastross = '[SinsAnaliseCadastros] Add SinsAnaliseCadastross',
  UpsertSinsAnaliseCadastross = '[SinsAnaliseCadastros] Upsert SinsAnaliseCadastross',
  UpdateSinsAnaliseCadastros = '[SinsAnaliseCadastros] Update SinsAnaliseCadastros',
  UpdateSinsAnaliseCadastross = '[SinsAnaliseCadastros] Update SinsAnaliseCadastross',
  DeleteSinsAnaliseCadastros = '[SinsAnaliseCadastros] Delete SinsAnaliseCadastros',
  DeleteSinsAnaliseCadastross = '[SinsAnaliseCadastros] Delete SinsAnaliseCadastross',
  ClearSinsAnaliseCadastross = '[SinsAnaliseCadastros] Clear SinsAnaliseCadastross'
}

export class LoadSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.LoadSinsAnaliseCadastross;

  constructor(public payload: { sinsAnaliseCadastross: SinsAnaliseCadastros[] }) {}
}

export class AddSinsAnaliseCadastros implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.AddSinsAnaliseCadastros;

  constructor(public payload: { sinsAnaliseCadastros: SinsAnaliseCadastros }) {}
}

export class UpsertSinsAnaliseCadastros implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.UpsertSinsAnaliseCadastros;

  constructor(public payload: { sinsAnaliseCadastros: SinsAnaliseCadastros }) {}
}

export class AddSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.AddSinsAnaliseCadastross;

  constructor(public payload: { sinsAnaliseCadastross: SinsAnaliseCadastros[] }) {}
}

export class UpsertSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.UpsertSinsAnaliseCadastross;

  constructor(public payload: { sinsAnaliseCadastross: SinsAnaliseCadastros[] }) {}
}

export class UpdateSinsAnaliseCadastros implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.UpdateSinsAnaliseCadastros;

  constructor(public payload: { sinsAnaliseCadastros: Update<SinsAnaliseCadastros> }) {}
}

export class UpdateSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.UpdateSinsAnaliseCadastross;

  constructor(public payload: { sinsAnaliseCadastross: Update<SinsAnaliseCadastros>[] }) {}
}

export class DeleteSinsAnaliseCadastros implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.DeleteSinsAnaliseCadastros;

  constructor(public payload: { id: string }) {}
}

export class DeleteSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.DeleteSinsAnaliseCadastross;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearSinsAnaliseCadastross implements Action {
  readonly type = SinsAnaliseCadastrosActionTypes.ClearSinsAnaliseCadastross;
}

export type SinsAnaliseCadastrosActions =
 LoadSinsAnaliseCadastross
 | AddSinsAnaliseCadastros
 | UpsertSinsAnaliseCadastros
 | AddSinsAnaliseCadastross
 | UpsertSinsAnaliseCadastross
 | UpdateSinsAnaliseCadastros
 | UpdateSinsAnaliseCadastross
 | DeleteSinsAnaliseCadastros
 | DeleteSinsAnaliseCadastross
 | ClearSinsAnaliseCadastross;
