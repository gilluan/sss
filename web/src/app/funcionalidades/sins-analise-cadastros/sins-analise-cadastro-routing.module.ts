import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContainerSinsListaCadastrosComponent } from "./containers/container-sins-lista-cadastros/container-sins-lista-cadastros.component";
import { ContainerSinsAnaliseCadastroComponent } from './containers/container-sins-analise-cadastro/container-sins-analise-cadastro.component';
import { ContainerAssinaturaCadastroComponent } from "./containers/container-assinatura-cadastro/container-assinatura-cadastro.component";
import { ContainerRouterAnaliseCadastroComponent } from './containers/container-router-analise-cadastro.component';

const routes: Routes = [
  {
    path: '',
    component: ContainerSinsListaCadastrosComponent
  },
  {
    path: 'assinatura',
    pathMatch: 'full',
    component: ContainerAssinaturaCadastroComponent,
    data: {
      breadcrumb: "Assinatura"
    }
  },
  {
    path: '',
    component: ContainerRouterAnaliseCadastroComponent,
    children: [
      {
        path: ':cpf',
        component: ContainerSinsAnaliseCadastroComponent,
        data: {
          breadcrumb: "Detalhamento de Voluntário"
        }
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinsAnaliseCadastrosRoutingModule { }
