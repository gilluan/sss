import { Injectable, OnDestroy } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { catchError, map, switchMap, withLatestFrom, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { VoluntariosService } from '../voluntarios.service';
import { JustificativaAjustesService } from '../justificativa-ajustes.service';
import { JustificativaAjusteVm } from '../models/dto/justificativa-ajuste-vm.model';
import { PagedDataModelVm } from "../../../shared/models/paged-data.model";
import { ModalService, ModalRef } from '@sicoob/ui';
import { DialogoSucessoComponent } from 'src/app/shared/components/dialogo-sucesso/dialogo-sucesso.component';
import { VoluntarioDto } from "../../../shared/models/voluntario-dto.model";
import * as fromAnaliseVoluntario from '../reducers/analise-volutario.reducer';
import * as fromVoluntario from "../../../reducers/voluntario.reducer";

import {
  AnaliseVoluntarioActionTypes,
  AssinarVoluntario,
  AssinarVoluntarioClose,
  AssinarVoluntarioFail,
  AssinarVoluntarioSuccess,
  LoadAnaliseVoluntario,
  LoadAnaliseVoluntarioFail,
  LoadAnaliseVoluntarioSuccess,
  LoadJustificativasAjustes,
  LoadJustificativasAjustesFail,
  LoadJustificativasAjustesSuccess,
  LoadVoluntario,
  LoadVoluntarioFail,
  LoadVoluntarioSuccess,
  SalvarJustificativa,
  SalvarJustificativaClose,
  SalvarJustificativaFail,
  SalvarJustificativaSuccess,
  AbrirModal,
  FecharModal,
  AddModal,
  FecharModalSucess
} from "../actions/analise-voluntario.actions";
import { Store, select, Action } from '@ngrx/store';
import { VoluntarioFiltroDto } from 'src/app/shared/models/voluntario-filtro.model';
import { CarregarVoluntariosPorFiltro } from 'src/app/actions/voluntarioAction';
import { PageModelVm } from 'src/app/shared/models/page.model';
import { SituacaoVoluntarioType } from '../models/types/situacao-voluntario.type';
import { PerfilType } from 'src/app/shared/types/perfil.type';


@Injectable()
export class AnaliseVoluntarioEffects implements OnDestroy {

  ngOnDestroy() {
    console.log("Destriu o effects");
  }

  constructor(
    private actions$: Actions,
    private service: VoluntariosService,
    private justificativaService: JustificativaAjustesService,
    private modalService: ModalService,
    private store$: Store<fromAnaliseVoluntario.State>,
    private readonly voluntarioStore$: Store<fromVoluntario.State>,

  ) { }

  @Effect()
  load$ =
    this.actions$
      .ofType(AnaliseVoluntarioActionTypes.LoadAnaliseVoluntario).pipe(
        map((action: LoadAnaliseVoluntario) => action),
        switchMap((action) => this.service.findVoluntarioByCpf(action.cpf).pipe(
          map((voluntario: VoluntarioDto) => new LoadAnaliseVoluntarioSuccess(voluntario)),
          catchError(error => of(new LoadAnaliseVoluntarioFail(error))))));

  @Effect()
  assinar$ =
    this.actions$
      .ofType(AnaliseVoluntarioActionTypes.AssinarVoluntario).pipe(
        map((action: AssinarVoluntario) => action),
        switchMap((action) =>
          this.service.mudarStatus(action.id, "aguardandoAssinatura").pipe(
            map((voluntario: VoluntarioDto) => {
              action.ref.close();
              return new AssinarVoluntarioSuccess(voluntario)
            }),
            catchError(error => {
              action.ref.close();
              return of(new AssinarVoluntarioFail(error))
            })
          )));


  @Effect()
  loadJustificativasAjustes$ =
    this.actions$
      .ofType(AnaliseVoluntarioActionTypes.LoadJustificativasAjustes).pipe(
        map((action: LoadJustificativasAjustes) => action),
        switchMap((action) => this.justificativaService.findJustificativasByIdVoluntario(action.idVoluntario).pipe(
          map((justificativas: JustificativaAjusteVm[]) => new LoadJustificativasAjustesSuccess(justificativas)),
          catchError(error => of(new LoadJustificativasAjustesFail(error))))));

  @Effect()
  createJustificativa$ =
    this.actions$
      .ofType(AnaliseVoluntarioActionTypes.SalvarJustificativa).pipe(
        switchMap((actionX: SalvarJustificativa) => this.justificativaService.salvarJustificativa(actionX.justificativa).pipe(
          map((justificativaResultado: JustificativaAjusteVm) => new SalvarJustificativaSuccess(justificativaResultado, actionX.cpf, actionX.destinatario)),
          catchError(error => of(new SalvarJustificativaFail(error)))
        )
        ));


  @Effect()
  createJustificativaSucess$ = this.actions$.ofType(AnaliseVoluntarioActionTypes.SalvarJustificativaSuccess).pipe(
    map((action: SalvarJustificativaSuccess) => action),
    switchMap((datos: SalvarJustificativaSuccess) => {
      let rota = "/analise-cadastros";
      if (datos.destinatario == PerfilType.gestor) {
        rota = rota + "/assinatura";
      }
      let data: any = {
        header: "Pedido enviado",
        buttonClass: "mdi-check-circle-outline",
        textButton: "Voltar para análise de voluntários",
        rota: rota,
        cpf: datos.cpf,
        message: `A sua solicitação de ajustes foi encaminhada para o ${datos.destinatario}`,
      }
      this.voluntarioStore$.dispatch(new CarregarVoluntariosPorFiltro(new VoluntarioFiltroDto(null, SituacaoVoluntarioType.aguardandoAssinatura, null, null, new PageModelVm(0, 6, 0))))
      return of(data)
    }),
    switchMap((data: any) => [
      new AbrirModal(data),
      new LoadAnaliseVoluntario(data.cpf),
    ])
  );

  @Effect()
  createJustificativaFail$ = this.actions$.ofType(AnaliseVoluntarioActionTypes.SalvarJustificativaFail).pipe(
    map((action: SalvarJustificativaFail) => action.payload),
    switchMap((datos: any) => {
      let rota = "/analise-cadastros";
      if (datos.destinatario == PerfilType.gestor) {
        rota = rota + "/assinatura";
      }
      let data: any = {
        header: "Ocorreu um erro",
        buttonClass: "mdi-close-circle-outline",
        textButton: "Voltar para análise de voluntários",
        rota: rota,
        message: "Ocorreu um erro tentando salvar a justificativa",
      }
      return of(new AbrirModal(data));
    })
  );

  @Effect()
  assinarSucess$ = this.actions$.ofType(AnaliseVoluntarioActionTypes.AssinarVoluntarioSuccess).pipe(
    map((action: AssinarVoluntarioSuccess) => action),
    switchMap((action: AssinarVoluntarioSuccess) => {
      let data = {
        header: "Pedido enviado",
        buttonClass: "mdi-check-circle-outline",
        textButton: "Voltar para análise de voluntários",
        rota: "/analise-cadastros",
        cpf: action.payload.cpf,
        message: "Seu pedido foi enviado para assinatura! Se o voluntário for aprovado ele será encaminhado para o banco de voluntário"
      }
      return of(data)
    }),
    switchMap((datos: any) => [
      new AbrirModal(datos),
      new LoadAnaliseVoluntario(datos.cpf)
    ])
  );

  @Effect()
  assinarErro$ = this.actions$.ofType(AnaliseVoluntarioActionTypes.AssinarVoluntarioFail).pipe(
    map((action: AssinarVoluntarioFail) => action.payload),
    switchMap((datos: any) => {
      let data: any = {
        header: "Ocorreu um erro",
        buttonClass: "mdi-close-circle-outline",
        textButton: "Voltar para análise de voluntários",
        rota: "/analise-cadastros",
        message: "Ocorreu um erro tentando aprovar o usuário. Por favor tente mais tarde.",
      }
      return of(new AbrirModal(data));
    })
  );

  @Effect()
  abrirModal$ =
    this.actions$.ofType(AnaliseVoluntarioActionTypes.AbrirModal).pipe(
      switchMap((action: AbrirModal) => {
        let modalRef = this.modalService.open(DialogoSucessoComponent, { data: action.data, panelClass: 'sins-modal-success' });
        return of(new AddModal(modalRef));
      })
    );

  @Effect()
  fecharModal$ =
    this.actions$.ofType(AnaliseVoluntarioActionTypes.FecharModal).pipe(
      withLatestFrom(this.store$.pipe(select(fromAnaliseVoluntario.getModalState))),
      mergeMap(([_, modalRef]: [Action, ModalRef]) => {
        if (modalRef)
          modalRef.close();
        return of(new FecharModalSucess());
      })
    );

  @Effect()
  loadVoluntariosAnalise$ =
    this.actions$.ofType(AnaliseVoluntarioActionTypes.LoadVoluntariosAnalise).pipe(
      map((action: LoadVoluntario) => action),
      switchMap((action) => this.service.pesquisarPorFiltro(action.filter).pipe(
        map((pagedData: PagedDataModelVm) => new LoadVoluntarioSuccess(pagedData)),
        catchError(error => of(new LoadVoluntarioFail(error)))
      ))
    );
}
