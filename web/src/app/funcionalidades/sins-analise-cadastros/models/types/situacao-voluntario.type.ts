export enum SituacaoVoluntarioType {
  aguardandoAnalise = 'aguardandoAnalise',
  devolvidoAjuste = 'devolvidoAjuste',
  aguardandoAssinatura = 'aguardandoAssinatura',
  ajusteVoluntario = 'ajusteVoluntario',
  assinado = 'assinado',
  atualizacaoTermo = 'atualizacaoTermo',
  aguardandoAssinaturaTermo = 'aguardandoAssinaturaTermo'
}

export namespace SituacaoVoluntarioType {
  export function values(): SituacaoVoluntarioType[] {
    return [
      SituacaoVoluntarioType.aguardandoAnalise,
      SituacaoVoluntarioType.aguardandoAssinatura,
      SituacaoVoluntarioType.devolvidoAjuste,
      SituacaoVoluntarioType.ajusteVoluntario,
      SituacaoVoluntarioType.assinado,
      SituacaoVoluntarioType.atualizacaoTermo,
      SituacaoVoluntarioType.aguardandoAssinaturaTermo
    ]
  }

  export function label(situacao: SituacaoVoluntarioType): string {
    switch (situacao) {
      case SituacaoVoluntarioType.aguardandoAnalise:
        return 'Aguardando análise';
      case SituacaoVoluntarioType.devolvidoAjuste:
        return 'Devolvido para ajuste';
      case SituacaoVoluntarioType.aguardandoAssinatura:
        return 'Aguardando assinatura';
      case SituacaoVoluntarioType.ajusteVoluntario:
        return 'Ajuste do voluntário';
      case SituacaoVoluntarioType.assinado:
        return 'Assinado';
      case SituacaoVoluntarioType.atualizacaoTermo:
        return 'Atualização de Termo';
      case SituacaoVoluntarioType.aguardandoAssinaturaTermo:
        return 'Aguardando Assinatura do Termo de Compromisso'
    }
  }

  export function classes(situacao: SituacaoVoluntarioType): string {
    switch (situacao) {
      case SituacaoVoluntarioType.aguardandoAnalise:
        return 'ss-label-primary';
      case SituacaoVoluntarioType.devolvidoAjuste:
        return 'ss-label-danger';
      case SituacaoVoluntarioType.aguardandoAssinatura:
        return 'ss-label-active';
      case SituacaoVoluntarioType.ajusteVoluntario:
        return 'ss-label-danger';
      case SituacaoVoluntarioType.assinado:
        return 'ss-label-success';
      case SituacaoVoluntarioType.aguardandoAssinaturaTermo:
        return 'ss-label-active';
      default:
        return 'ss-label-default'
    }
  }

}
