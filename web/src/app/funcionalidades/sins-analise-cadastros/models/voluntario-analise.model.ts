import {SituacaoVoluntarioType} from "./types/situacao-voluntario.type";

export class VoluntarioAnaliseModel {
  nome: string;
  dataRegistro: Date;
  situacao: SituacaoVoluntarioType;
  cpf: string;

  constructor(nome: string, dataRegistro: Date, situacao: SituacaoVoluntarioType, cpf: string) {
    this.nome = nome;
    this.dataRegistro = dataRegistro;
    this.situacao = situacao;
    this.cpf = cpf;
  }
}
