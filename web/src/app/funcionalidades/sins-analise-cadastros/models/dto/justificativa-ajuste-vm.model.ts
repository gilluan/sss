import { BaseDto } from "src/app/shared/models/base-dto.model";

export class JustificativaAjusteVm extends BaseDto   {
    justificativa: string;
    idVoluntario: string;
    idAcao: string;
    idVoluntarioCriador: string;
    nomeVoluntario: string;
    nomeVoluntarioCriador: string;
    tipo: string;
}
