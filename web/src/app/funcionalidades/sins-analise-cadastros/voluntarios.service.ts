import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { UsuarioService } from '@app/shared/services/usuario.service';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { VoluntarioFiltroDto } from 'src/app/shared/models/voluntario-filtro.model';
import { environment } from '../../../environments/environment';
import { PageModelVm } from '../../shared/models/page.model';
import { PagedDataModelVm } from '../../shared/models/paged-data.model';
import { VoluntarioDto } from '../../shared/models/voluntario-dto.model';
import { Voluntario } from '../termo-voluntario/models/voluntario.model';
import { TotalSituacaoVm } from './models/dto/total-situacao-vm.model';
import { SituacaoVoluntarioType } from './models/types/situacao-voluntario.type';
import { VoluntarioAnaliseModel } from './models/voluntario-analise.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class VoluntariosService extends UsuarioService {

  constructor(public http: HttpClient, private persistanceService: PersistanceService) {
    super(http, `${environment.modulo_gestao_pessoa}/voluntarios`);
  }

  concordarTermo(voluntario: VoluntarioDto): Observable<VoluntarioDto> {
    const ACCEPT_TERMOS_URL = this.RESOURCE_URL + '/concordar-termo';
    return this.http.patch<Voluntario>(ACCEPT_TERMOS_URL, voluntario)
      .pipe(
        catchError(this.handleError)
      );
  }

  criarVoluntario(voluntario: Voluntario): Observable<any> {
    return this.http.post<any>(this.RESOURCE_URL, voluntario, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  atualizarVoluntario(voluntario: Voluntario): Observable<Voluntario> {
    return this.http.put<Voluntario>(this.RESOURCE_URL, voluntario, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  pesquisarPorFiltro(filtro: VoluntarioFiltroDto): Observable<PagedDataModelVm> {
    const params = this.buildParams(filtro);
    return this.http.get<PagedDataModelVm>(this.RESOURCE_URL, { params: params }).pipe(map(pagedData => {
      const data = pagedData.data.map(v => v as VoluntarioDto);
      return new PagedDataModelVm(pagedData.page, data);
    }));
  }

  pesquisarSituacaoTotal(filtro: VoluntarioFiltroDto): Observable<TotalSituacaoVm[]> {
    const params = this.buildParams(filtro);
    return this.http.get<TotalSituacaoVm[]>(`${this.RESOURCE_URL}/situacao/total`, { params: params }).pipe(
      catchError(this.handleError)
    );
  }

  findVoluntarioByCpf(cpf: string): Observable<VoluntarioDto> {
    return this.http.get<any>(`${this.RESOURCE_URL}?cpf=${cpf}&offset=0&limit=1`, httpOptions)
      .pipe(
        map(a => a && a.data ? a.data[0] : null),
        catchError(this.handleError)
      );
  }

  findVoluntarioById(id: string): Observable<Voluntario> {
    return this.http.get<any>(`${this.RESOURCE_URL}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  assinarVoluntario(id: string): Observable<VoluntarioDto> {
    let idGestor = this.persistanceService.get("usuario_instituto").id;
    return this.http.patch<VoluntarioDto>(`${this.RESOURCE_URL}/${id}/assinar?identificadorGestorInstituto=${idGestor}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  mudarStatus(id: string, situacao: string): Observable<VoluntarioDto> {
    return this.http.patch<VoluntarioDto>(`${this.RESOURCE_URL}/${id}/situacao/${situacao}`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  desativarVoluntario(id: string): Observable<VoluntarioDto> {
    return this.http.patch<VoluntarioDto>(`${this.RESOURCE_URL}/${id}/desativar`, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  ativarVoluntario(id: string): Observable<VoluntarioDto> {
    return this.http.patch<VoluntarioDto>(`${this.RESOURCE_URL}/${id}/ativar`, httpOptions).pipe(catchError(this.handleError));
  }

  recuperarCooperativasDeVoluntarios(): Observable<string[]> {
    const filtro = new VoluntarioFiltroDto();
    filtro.ativo = true;
    filtro.situacao = SituacaoVoluntarioType.assinado;
    filtro.paginacao = new PageModelVm(0, 500, 0);
    let params = this.buildParams(filtro);
    params = params.append('campo', 'numeroCooperativa');
    return this.http.get<string[]>(`${this.RESOURCE_URL}`, { params: params }).pipe(
      catchError(this.handleError)
    );
  }

  pesquisarTotalPorFiltro(filtro: VoluntarioFiltroDto): Observable<number> {
    const params = this.buildParams(filtro);
    return this.http.get<number>(`${this.RESOURCE_URL}/total`, { params: params }).pipe(
      catchError(this.handleError)
    );
  }

  private buildParams(filtro: VoluntarioFiltroDto) {
    let params = new HttpParams();

    if (filtro.paginacao) {
      if (filtro.paginacao.pageNumber != null) {
        params = params.append('offset', `${filtro.paginacao.pageNumber}`);
      }

      if (filtro.paginacao.pageSize != null) {
        params = params.append('limit', `${filtro.paginacao.pageSize}`);
      }
    }

    if (filtro.nome != null) {
      params = params.append('nome', filtro.nome);
    }

    if (filtro.situacao != null) {
      params = params.append('situacao', filtro.situacao);
    }

    if (filtro.cooperativas != null && filtro.cooperativas.length > 0) {
      params = params.append('cooperativas', filtro.cooperativas.join(','));
    }

    if (filtro.ativo != null) {
      params = params.append('ativo', `${filtro.ativo}`);
    }

    return params;
  }

  private toModel(dto: VoluntarioDto): VoluntarioAnaliseModel {
    return new VoluntarioAnaliseModel(dto.nome, dto.dataCriacao, SituacaoVoluntarioType[dto.situacao], dto.cpf);
  }

}
