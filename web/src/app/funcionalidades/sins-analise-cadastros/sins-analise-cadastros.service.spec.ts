import { TestBed } from '@angular/core/testing';

import { SinsAnaliseCadastrosService } from './sins-analise-cadastros.service';

describe('SinsAnaliseCadastrosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SinsAnaliseCadastrosService = TestBed.get(SinsAnaliseCadastrosService);
    expect(service).toBeTruthy();
  });
});
