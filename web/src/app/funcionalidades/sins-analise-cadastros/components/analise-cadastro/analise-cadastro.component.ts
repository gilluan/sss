import {Component, Input, OnInit} from '@angular/core';
import {SituacaoVoluntarioType} from '../../models/types/situacao-voluntario.type';
import {JustificativaAjusteVm} from '../../models/dto/justificativa-ajuste-vm.model';
import {VoluntarioDto} from "../../../../shared/models/voluntario-dto.model";

@Component({
  selector: 'app-analise-cadastro',
  templateUrl: './analise-cadastro.component.html',
  styleUrls: ['./analise-cadastro.component.scss']
})
export class AnaliseCadastroComponent implements OnInit {

  @Input() voluntario: VoluntarioDto;
  @Input() pdfSrc: any;
  @Input() justificativasAjustes: JustificativaAjusteVm[];

  open: boolean = false;

  ngOnInit(){
  }

  toggle() {
    this.open = !this.open;;
  }

  getSituacaoClass(situacao: string): string {
    return SituacaoVoluntarioType.classes(SituacaoVoluntarioType[situacao]);
  }

  getSituacaoLabel(situacao: string): string {
    return SituacaoVoluntarioType.label(SituacaoVoluntarioType[situacao]);
  }

  constructor() { }

}
