import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {VoluntarioFiltroDto} from "../../../../shared/models/voluntario-filtro.model";
import {SituacaoVoluntarioType} from "../../models/types/situacao-voluntario.type";
import {TotalSituacaoVm} from "../../models/dto/total-situacao-vm.model";
import {VoluntarioDto} from "../../../../shared/models/voluntario-dto.model";

@Component({
  selector: 'app-lista-analise-cadastro',
  templateUrl: './lista-analise-cadastro.component.html',
  styleUrls: ['./lista-analise-cadastro.component.scss']
})
export class ListaAnaliseCadastroComponent implements OnInit {
  columns = [];

  @ViewChild('dataRegistroTpl') dataRegistroTpl: TemplateRef<any>;
  @ViewChild('situacaoTpl') situacaoTpl: TemplateRef<any>;
  @ViewChild('acoesTpl') acoesTpl: TemplateRef<any>;

  @Input() voluntarios$: VoluntarioDto[];
  @Input() totais$: TotalSituacaoVm[];
  @Input() filtro$: VoluntarioFiltroDto;

  @Output() pesquisar: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
    this.initColumns();
  }

  onSearch($event: string) {
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = $event;
    this.pesquisarVoluntarios();
  }

  onClear(_: string) {
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = null;
    this.pesquisarVoluntarios();
  }

  onChangeTab($event) {
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.situacao = $event;
    this.pesquisarVoluntarios();
  }

  onChangePage(offset) {
    this.filtro$.paginacao.pageNumber = offset;
    this.pesquisarVoluntarios();
  }

  getSituacaoClass = (situacao: SituacaoVoluntarioType) => SituacaoVoluntarioType.classes(situacao);

  getSituacaoLabel = (situacao: SituacaoVoluntarioType) => SituacaoVoluntarioType.label(situacao);

  private initColumns() {
    this.columns = [
      { name: "Nome do Voluntário", prop: "nome", flexGrow: 3, minWidth: 250 },
      { name: "Data de Registro", prop: "dataCriacao", cellTemplate: this.dataRegistroTpl, flexGrow: 1, minWidth: 120 },
      { name: "Situação", prop: "situacao", cellTemplate: this.situacaoTpl, flexGrow: 1, minWidth: 180 },
      { prop: "cpf", cellTemplate: this.acoesTpl, headerTemplate: null, flexGrow: 1, sortable: false, minWidth: 150 }
    ];
  }

  private pesquisarVoluntarios() {
    this.pesquisar.emit();
  }

}
