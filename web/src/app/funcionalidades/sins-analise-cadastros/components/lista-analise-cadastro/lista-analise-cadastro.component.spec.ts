import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAnaliseCadastroComponent } from './lista-analise-cadastro.component';

describe('ListaAnaliseCadastroComponent', () => {
  let component: ListaAnaliseCadastroComponent;
  let fixture: ComponentFixture<ListaAnaliseCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAnaliseCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAnaliseCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
