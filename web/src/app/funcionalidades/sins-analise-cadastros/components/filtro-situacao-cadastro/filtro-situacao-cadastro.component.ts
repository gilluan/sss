import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SituacaoVoluntarioType} from "../../models/types/situacao-voluntario.type";
import {TotalSituacaoVm} from "../../models/dto/total-situacao-vm.model";

@Component({
  selector: 'sc-sins-analise-cadastros',
  templateUrl: './filtro-situacao-cadastro.component.html',
  styleUrls: [ './filtro-situacao-cadastro.component.scss' ]
})
export class
FiltroSituacaoCadastroComponent implements OnInit {

  @Input() counters$: TotalSituacaoVm[];
  @Output() changeTab: EventEmitter<SituacaoVoluntarioType> = new EventEmitter<SituacaoVoluntarioType>();

  constructor() { }

  ngOnInit(): void { }

  onTabSelected(tabIndex) {
    this.changeTab.emit(this.getSituacao(tabIndex));
  }

  getCounter(situacao: string): number {
    if (this.counters$ != null) {
      const totalSituacao = this.counters$.find(value => value.situacao == situacao);
      return ((totalSituacao || ({} as TotalSituacaoVm)).total || 0);
    }
    return 0;
  }

  private getSituacao(tabIndex: number): SituacaoVoluntarioType {
    return SituacaoVoluntarioType.values()[tabIndex];
  }

}
