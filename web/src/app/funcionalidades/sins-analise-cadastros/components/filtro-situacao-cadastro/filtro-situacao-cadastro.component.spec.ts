import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroSituacaoCadastroComponent } from './filtro-situacao-cadastro.component';

describe('FiltroSituacaoCadastroComponent', () => {
  let component: FiltroSituacaoCadastroComponent;
  let fixture: ComponentFixture<FiltroSituacaoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroSituacaoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroSituacaoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
