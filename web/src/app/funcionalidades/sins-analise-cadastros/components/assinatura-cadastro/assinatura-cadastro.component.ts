import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, ChangeDetectorRef} from '@angular/core';
import {VoluntarioFiltroDto} from "../../../../shared/models/voluntario-filtro.model";
import {VoluntarioDto} from 'src/app/shared/models/voluntario-dto.model';

@Component({
  selector: 'app-assinatura-cadastro',
  templateUrl: './assinatura-cadastro.component.html',
  styleUrls: ['./assinatura-cadastro.component.scss']
})
export class AssinaturaCadastroComponent implements OnInit {
  columns = [];
  isSearching: boolean = false;

  @ViewChild('nomeTpl') nomeTpl: TemplateRef<any>;
  @ViewChild('dataRegistroTpl') dataRegistroTpl: TemplateRef<any>;
  @ViewChild('acoesTpl') acoesTpl: TemplateRef<any>;

  @Input() voluntarios$: VoluntarioDto[];
  @Input() filtro$: VoluntarioFiltroDto;
  @Input() total$: number;
  @Input() pesquisaFiltrada$: boolean;

  @Output() pesquisar: EventEmitter<any> = new EventEmitter<any>();
  @Output() assinar: EventEmitter<VoluntarioDto> = new EventEmitter<VoluntarioDto>();
  @Output() solicitarAjuste: EventEmitter<VoluntarioDto> = new EventEmitter<VoluntarioDto>();

  constructor() { }

  ngOnInit(): void {
    this.initColumns();
  }

  onSearch($event: string) {
    this.isSearching = true;
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = $event;
    this.callPesquisarVolutarios();
  }

  onClear(_: string) {
    this.isSearching = false;
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = null;
    this.callPesquisarVolutarios();
  }

  onChangePage(offset) {
    this.filtro$.paginacao.pageNumber = offset;
    this.callPesquisarVolutarios();
  }

  assinarContrato(item: VoluntarioDto): void {
    this.assinar.emit(item);
    this.callPesquisarVolutarios();
  }

  solicitarAjustes(item: VoluntarioDto) {
    this.solicitarAjuste.emit(item);
  }

  renderMensagemTotal(): string {
    return this.pesquisaFiltrada$ ? this.generateFilteredMessage(this.total$) : this.generateTotalMessage(this.total$);
  }

  private generateTotalMessage(total: number): string {
    let label = this.getPlural(total, "voluntário", null);
    return `${label} aguardando assinatura`;
  }

  private generateFilteredMessage(total: number): string {
    let label = this.getPlural(total, "voluntário encontrado", "voluntários encontrados", "Nenhum voluntário encontrado");
    return `${label} na busca`;
  }

  private getPlural(amount: number, singularLabel: string, pluralLabel?: string, noneLabel?: string) {
    noneLabel = noneLabel ? noneLabel : 'Nenhum';
    pluralLabel = pluralLabel ? pluralLabel : `${singularLabel}s`;
    return amount == 0 ? noneLabel : amount > 1 ? `${amount} ${pluralLabel}` : `${amount} ${singularLabel}`;
  }

  private initColumns() {
    this.columns = [
      { name: "Nome do Voluntário", prop: "nome", flexGrow: 3, cellClass: 'cell-align-center', minWidth: 150 },
      { name: "Data de Registro", prop: "dataCriacao", cellTemplate: this.dataRegistroTpl, flexGrow: 1, cellClass: 'cell-align-center', minWidth: 120 },
      { prop: "cpf", cellTemplate: this.acoesTpl, headerTemplate: null,  flexGrow: 2,  minWidth: 305 }
    ];
  }

  private callPesquisarVolutarios() {
    this.pesquisar.emit();
  }

}
