import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromAnaliseVoluntario from './reducers/analise-volutario.reducer';
import * as fromJustificativaAjustes from './reducers/justificativa-ajustes.reducer';
import * as fromVoluntariosAnalise from './reducers/voluntarios-analise.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ContainerSinsListaCadastrosComponent } from './containers/container-sins-lista-cadastros/container-sins-lista-cadastros.component';
import { FiltroSituacaoCadastroComponent } from './components/filtro-situacao-cadastro/filtro-situacao-cadastro.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AlertModule, FormModule, ModalModule, ModalService, TabsModule } from '@sicoob/ui';
import { PdfViewerModule } from 'ng2-pdf-viewer'; // <- import OrderModule
import { AnaliseVoluntarioEffects } from './effects/analise-voluntario.effects';
import { NgxBrModule } from '@nbfontana/ngx-br';
import { AvatarModule } from '@app/shared/components/sins-avatar/sc-avatar.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnaliseCadastroComponent } from './components/analise-cadastro/analise-cadastro.component';
import { ScDatatableModule } from '../../shared/components/sc-datatable/sc-datatable.module';
import { ListaAnaliseCadastroComponent } from './components/lista-analise-cadastro/lista-analise-cadastro.component';
import { AssinaturaCadastroComponent } from './components/assinatura-cadastro/assinatura-cadastro.component';
import { ScCheckbuttonModule } from '../../shared/components/sc-checkbutton/sc-checkbutton.module';
import { ContainerSinsAnaliseCadastroComponent } from './containers/container-sins-analise-cadastro/container-sins-analise-cadastro.component';
import { DialogoConfirmacaoComponent } from 'src/app/shared/components/dialogo-confirmacao/dialogo-confirmacao.component';
import { NgxMaskModule } from 'ngx-mask';
import { ContainerAssinaturaCadastroComponent } from './containers/container-assinatura-cadastro/container-assinatura-cadastro.component';
import { SinsAnaliseCadastrosRoutingModule } from './sins-analise-cadastro-routing.module';
import { DialogoJustificativaAjustesComponent } from 'src/app/shared/components/dialogo-justificativa-ajustes/dialogo-justificativa-ajustes.component';
import { SharedModule } from '../../shared/shared.module';
import { SinsBadgeModule } from '@app/shared/components/sins-badge/sins-badge.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ContainerRouterAnaliseCadastroComponent } from './containers/container-router-analise-cadastro.component';
@NgModule({
  declarations: [
    ContainerRouterAnaliseCadastroComponent,
    ContainerSinsListaCadastrosComponent,
    ContainerSinsAnaliseCadastroComponent,
    FiltroSituacaoCadastroComponent,
    AnaliseCadastroComponent,
    ListaAnaliseCadastroComponent,
    AssinaturaCadastroComponent,
    ContainerAssinaturaCadastroComponent
  ],
  imports: [
    NgxPermissionsModule.forChild(),
    NgxMaskModule,
    SharedModule,
    FormModule,
    AlertModule,
    TabsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CommonModule,
    PdfViewerModule,
    NgxBrModule.forRoot(),
    AvatarModule,
    ModalModule,
    SinsAnaliseCadastrosRoutingModule,
    ScDatatableModule,
    ScCheckbuttonModule,
    StoreModule.forFeature('voluntariosAnalise', fromVoluntariosAnalise.reducer),
    StoreModule.forFeature('analiseVoluntario', fromAnaliseVoluntario.reducer),
    StoreModule.forFeature('justificativaAjustes', fromJustificativaAjustes.reducer),
    EffectsModule.forFeature([AnaliseVoluntarioEffects]),
    SinsBadgeModule,
  ],
  providers: [ModalService],
  entryComponents: [DialogoConfirmacaoComponent, DialogoJustificativaAjustesComponent]

})
export class SinsAnaliseCadastrosModule {
}
