import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ACTIONBAR_DATA, ActionBarRef} from "@sicoob/ui";
import {CooperativaFiltro} from "../../models/cooperativa-filtro.model";
import {Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-actionbar-filtro-voluntarios',
  templateUrl: './actionbar-filtro-voluntarios.component.html',
  styleUrls: ['./actionbar-filtro-voluntarios.component.scss']
})
export class ActionbarFiltroVoluntariosComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();

  cooperativasObservable: Observable<CooperativaFiltro[]>;
  cooperativas$: CooperativaFiltro[] = [];
  cooperativas: CooperativaFiltro[] = [];
  filtrosAnteriores: string[];

  constructor(
    private changeDetector: ChangeDetectorRef,
    public actionBarRef: ActionBarRef,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  ngOnInit(): void {
    this.cooperativasObservable = this.data['cooperativas'];
    this.filtrosAnteriores = this.data['filtros'];
    this.subscription.add(this.cooperativasObservable.subscribe(cooperativas => this.onCooperativasRetornadas(cooperativas)));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  adicionarFiltro(numeroCooperativa: string, state: boolean) {
    if (state) {
      this.updateSelectionState(numeroCooperativa, true);
      this.changeDetector.detectChanges();
    } else {
      this.removerFiltro(numeroCooperativa);
    }
  }

  removerFiltro(numeroCooperativa: string) {
    this.updateSelectionState(numeroCooperativa, false);
    this.changeDetector.detectChanges();
  }

  cooperativasFiltradas() {
    return this.cooperativas.filter(c => c.selecionado == true);
  }

  onSearch($event: string) {
    this.cooperativas$ = this.cooperativas.filter(c => c.numero.indexOf($event) != -1)
  }

  onClear($event: string) {
    this.cooperativas$ = [...this.cooperativas];
  }

  closeSidebar = () => this.actionBarRef.close({ action: 'fechar' });

  applyFilter = () => this.actionBarRef.close({
    action: 'filtrar',
    data: this.cooperativasFiltradas().map(c => c.numero)
  });

  disableApplyFilterButton = (): boolean => this.cooperativasFiltradas().length == 0;

  private updateSelectionState(numeroCooperativa: string, selected: boolean) {
    this.cooperativas$.find(c => c.numero === numeroCooperativa).selecionado = selected;
    this.cooperativas.find(c => c.numero === numeroCooperativa).selecionado = selected;
  }

  private onCooperativasRetornadas(cooperativas: CooperativaFiltro[]) {
    this.cooperativas = [...cooperativas];
    this.cooperativas$ = [...cooperativas];
    this.aplicarFiltrosAnteriores();
    this.changeDetector.detectChanges();
  }

  private aplicarFiltrosAnteriores() {
    if(this.filtrosAnteriores != null) {
      this.filtrosAnteriores.forEach(filtro => this.adicionarFiltro(filtro, true));
    }
  }
}
