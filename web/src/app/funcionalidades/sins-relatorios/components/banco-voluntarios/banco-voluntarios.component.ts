import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { VoluntarioFiltroDto } from "../../../../shared/models/voluntario-filtro.model";
import { CpfCnpjPipe, TelefonePipe } from "@nbfontana/ngx-br";

import { VoluntarioDto } from "../../../../shared/models/voluntario-dto.model";
import { TableColumn } from "@swimlane/ngx-datatable";
import { UsuarioInstituto } from "../../../../shared/models/usuario-instituto.model";
import { SituacaoVoluntarioType } from '@app/funcionalidades/sins-analise-cadastros/models/types/situacao-voluntario.type';

@Component({
  selector: 'app-banco-voluntarios',
  templateUrl: './banco-voluntarios.component.html',
  styleUrls: ['./banco-voluntarios.component.scss']
})
export class BancoVoluntariosComponent implements OnInit {
  columns: TableColumn[];
  voluntario: VoluntarioDto;

  @ViewChild('acoesTpl') acoesTpl: TemplateRef<any>;
  @ViewChild('statusTpl') statusTpl: TemplateRef<any>;
  @ViewChild("headerTpl") headerTpl: TemplateRef<any>;

  @Input() voluntarios$: VoluntarioDto[];
  @Input() filtro$: VoluntarioFiltroDto;
  @Input() total$: number;
  @Input() filtrosCooperativa: string[];

  @Output() pesquisar: EventEmitter<any> = new EventEmitter();
  @Output() desativar: EventEmitter<VoluntarioDto> = new EventEmitter();
  @Output() detalhar: EventEmitter<string> = new EventEmitter();
  @Output() manterHoras: EventEmitter<string> = new EventEmitter();
  @Output() filtrar: EventEmitter<any> = new EventEmitter();

  private usuarioInstituto: UsuarioInstituto;

  constructor() { }

  ngOnInit(): void {
    this.initColumns();
  }

  pesquisarVoluntario = (cpf) => this.voluntarios$.filter(v => v.cpf === cpf)[0];

  onChangePage(offset) {
    this.filtro$.paginacao.pageNumber = offset;
    this.pesquisarVoluntarios();
  }

  onSearch(text: string): void {
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = text;
    this.pesquisarVoluntarios();
  }

  onClear(_: string) {
    this.filtro$.paginacao.pageNumber = 0;
    this.filtro$.nome = null;
    this.pesquisarVoluntarios();
  }

  filtrarVoluntarios(filtros: string[]) {
    this.filtro$.nome = null;
    this.filtro$.cooperativas = filtros;
    this.pesquisarVoluntarios();
  }

  abrirDetalheActionBar(cpf: string) {
    this.detalhar.emit(cpf);
  }

  abrirHorasActionBar(cpf: string) {
    this.manterHoras.emit(cpf);
  }

  abrirFiltroActionBar() {
    this.filtrar.emit();
  }

  removerFiltroCooperativa(cooperativa: string) {
    const indexParaExclusao = this.filtrosCooperativa.indexOf(cooperativa);
    this.filtrosCooperativa.splice(indexParaExclusao, 1);
    this.filtrarVoluntarios(this.filtrosCooperativa);
  }

  mostrarFiltro(): boolean {
    return !this.isPaeOuPde();
  }

  private isPaeOuPde(): boolean {
    return this.usuarioInstituto && ['pae', 'pde'].indexOf(this.usuarioInstituto.perfil) != -1
  }

  private pesquisarVoluntarios() {
    this.pesquisar.emit();
  }

  private initColumns() {
    this.columns = [
      { name: 'Nome', prop: 'nome', flexGrow: 3, minWidth: 200 },
      { name: 'CPF', prop: 'cpf', pipe: new CpfCnpjPipe(), sortable: false, flexGrow: 2, minWidth: 100 },
      { name: 'Telefone', prop: 'telefone', pipe: new TelefonePipe(), sortable: false, flexGrow: 2, minWidth: 150 },
      { name: 'Cooperativa', prop: 'numeroCooperativa', flexGrow: 1, minWidth: 65 },
      { name: 'Status', cellTemplate: this.statusTpl, prop: 'ativo', flexGrow: 1, sortable: false, minWidth: 100 },
      { name: 'Horas', prop: 'horas', flexGrow: 1, sortable: false, minWidth: 50 },
      { prop: 'cpf', cellTemplate: this.acoesTpl, headerTemplate: null, sortable: false, flexGrow: 2, cellClass: 'cellClassAction', minWidth: 100 },
    ]
  }

  getSituacaoClass = (ativo: boolean) => ativo ? 'ss-label-success' : 'ss-label-danger';
  getSituacaoLabel = (ativo: boolean) => ativo ? 'Ativo' : 'Inativo';

}
