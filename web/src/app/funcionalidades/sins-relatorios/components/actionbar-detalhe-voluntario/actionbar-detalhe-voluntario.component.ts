import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { Observable, Subscription } from "rxjs";
import { VoluntarioDto } from "../../../../shared/models/voluntario-dto.model";
import { Curso } from '@app/funcionalidades/perfil-usuario/models/curso.model';
import { Store, select } from '@ngrx/store';
import * as fromGed from '../../../../reducers/ged.reducer';
import { CarregarArquivo, LimparStoreGed } from '@app/actions/ged.actions';
import { DadosDocumento } from '@app/shared/ged/models/retorno/dadosDocumento.model';

@Component({
  selector: 'app-actionbar-detalhe-voluntario',
  templateUrl: './actionbar-detalhe-voluntario.component.html',
  styleUrls: ['./actionbar-detalhe-voluntario.component.scss']
})
export class ActionbarDetalheVoluntarioComponent implements OnInit {

  voluntario: VoluntarioDto;
  solicitacaoInativacao: boolean = false;
  solicitacaoAtivacao: boolean = false;
  capacitacoes: Curso[] = [];

  constructor(
    private sanitizer:DomSanitizer,
    public actionBarRef: ActionBarRef,
    private changeDetectorRef: ChangeDetectorRef,
    private gedStore$: Store<fromGed.State>,
    @Inject(ACTIONBAR_DATA) public data: any) { }

  ngOnInit() {
    this.voluntario = this.data['voluntario'];
    (this.data['capacitacoes'] as Observable<Curso[]>).subscribe(documento => {
      this.capacitacoes = documento;
      this.changeDetectorRef.detectChanges();
    });
  }

  isBday(): boolean {
    if (this.voluntario.dataNascimento) {
      const today = new Date();
      const bday = new Date(this.voluntario.dataNascimento);
      return bday.getDay() == today.getDay() && bday.getDate() == today.getDate();
    }
    return false;
  }

  baSubscription: Subscription;

  baixarCertificado(curso: Curso) {
    this.gedStore$.dispatch(new CarregarArquivo(curso.identificadorCertificadoGED));
    this.baSubscription = this.gedStore$.pipe(select(fromGed.selectAll)).subscribe((arquivos: DadosDocumento[]) => {
      if (arquivos && arquivos.length > 0 && curso) {
        this.downLoadFile(arquivos[0].listaSequenciaisDocumento[0].arquivoCodificadoBase64, `${curso.nome}.pdf`)
        curso = null;
      }
    })
  }

  private downLoadFile(dataBase64: string, filename: string) {
    let downloadLink = document.createElement('a');
    downloadLink.href = `data:application/pdf;base64,${dataBase64}` //window.URL.createObjectURL(new Blob([dataBase64], {type: 'application/pdf;base64'}));;
    downloadLink.setAttribute('download', filename)
    document.body.appendChild(downloadLink);
    downloadLink.click();
    downloadLink.remove();
    this.gedStore$.dispatch(new LimparStoreGed())
  }

  handleSolicitarDesativacao = (state: boolean) => this.solicitacaoInativacao = state;

  ativarVoluntario = () => this.actionBarRef.close({ action: 'ativar', data: this.voluntario });

  desativarVoluntario = () => this.actionBarRef.close({ action: 'desativar', data: this.voluntario });

  closeActionbar = () => this.actionBarRef.close();
}
