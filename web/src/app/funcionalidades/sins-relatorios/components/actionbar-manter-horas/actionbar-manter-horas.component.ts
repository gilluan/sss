import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { CarregarHorasVoluntario, CleanHorasVoluntario, EditarHorasVoluntario, RemoverHorasVoluntario, SalvarHorasVoluntario } from '@app/actions/horas-voluntario.actions';
import { PageModelVm } from '@app/shared/models/page.model';
import { select, Store } from '@ngrx/store';
import { HorasVoluntario } from '@shared/models/horas-voluntario.model';
import { ActionBarRef, ACTIONBAR_DATA } from "@sicoob/ui";
import { Observable, Subscription } from 'rxjs';
import * as fromHorasVoluntario from '../../../../reducers/horas-voluntario.reducers';
import { VoluntarioDto } from "../../../../shared/models/voluntario-dto.model";

enum AcaoHorasVoluntario {
  form = 'form',
  listar = 'listar'
}

@Component({
  selector: 'app-actionbar-manter-horas',
  templateUrl: './actionbar-manter-horas.component.html',
  styleUrls: ['./actionbar-manter-horas.component.scss']
})
export class ActionbarManterHorasComponent implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();
  voluntario: VoluntarioDto;
  horasVoluntario$: Observable<HorasVoluntario[]>;
  paginacaoAtual: PageModelVm;
  paginas: number[];
  acao: AcaoHorasVoluntario = AcaoHorasVoluntario.listar;
  horasVoluntarioEditar: HorasVoluntario;
  horasDeletar: HorasVoluntario;
  totalHoras$: Observable<number>;
  isLoading$: Observable<boolean>;
  anoAtual = new Date().getFullYear().toString();

  constructor(
    public actionBarRef: ActionBarRef,
    @Inject(ACTIONBAR_DATA) public data: any,
    private readonly store$: Store<fromHorasVoluntario.State>) { }

  ngOnInit() {
    this.subscription.add(this.store$.pipe(select(fromHorasVoluntario.getPaginacaoAtual)).subscribe((paginacao: PageModelVm) => this.montarPaginacao(paginacao)))
    this.horasVoluntario$ = this.store$.pipe(select(fromHorasVoluntario.getHorasVoluntarios));
    this.totalHoras$ = this.store$.pipe(select(fromHorasVoluntario.getTotalHorasVoluntario));
    this.isLoading$ = this.store$.pipe(select(fromHorasVoluntario.isLoading));
    this.voluntario = this.data['voluntario'];
    if (this.voluntario) {
      this.carregarHorasVoluntario();
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.cleanHorasVoluntario(true);
  }

  private carregarHorasVoluntario = () => {
    this.store$.dispatch(new CarregarHorasVoluntario(this.voluntario.id, this.paginacaoAtual))
  };

  private cleanHorasVoluntario = (all: boolean) => this.store$.dispatch(new CleanHorasVoluntario(all));

  private recarregarHoras() {
    this.cleanHorasVoluntario(true);
    this.acao = AcaoHorasVoluntario.listar;
  }

  closeActionbar = () => {
    if (this.acao === AcaoHorasVoluntario.listar) {
      this.actionBarRef.close();
    } else {
      this.acao = AcaoHorasVoluntario.listar;
    }
  }

  criar($event) {
    this.store$.dispatch(new SalvarHorasVoluntario($event));
    this.recarregarHoras();
  }

  excluir(horasVoluntario?: HorasVoluntario) {
    if (horasVoluntario) {
      this.horasDeletar = horasVoluntario
    } else {
      this.cleanHorasVoluntario(true);
      this.store$.dispatch(new RemoverHorasVoluntario(this.horasDeletar));
      this.horasDeletar = null;
    }
  }

  editar($event) {
    this.store$.dispatch(new EditarHorasVoluntario($event));
    this.recarregarHoras();
  }

  habilitarFormEditar(horasVoluntario: HorasVoluntario) {
    this.acao = AcaoHorasVoluntario.form;
    this.horasVoluntarioEditar = horasVoluntario
  }

  habilitarForm = () => {
    this.acao = AcaoHorasVoluntario.form;
    this.horasVoluntarioEditar = null;
  };


  // --- Começo da Paginacação --- //
  irPara($event) {
    let page = +$event.srcElement.innerHTML - 1;
    if (page !== this.paginacaoAtual.pageNumber) {
      this.cleanHorasVoluntario(false);
      this.store$.dispatch(new CarregarHorasVoluntario(this.voluntario.id, { ...this.paginacaoAtual, pageNumber: page }))
    }
  }
  anterior() {
    this.cleanHorasVoluntario(false);
    this.store$.dispatch(new CarregarHorasVoluntario(this.voluntario.id, { ...this.paginacaoAtual, pageNumber: (this.paginacaoAtual.pageNumber - 1) }))
  }
  proximo() {
    this.cleanHorasVoluntario(false);
    this.store$.dispatch(new CarregarHorasVoluntario(this.voluntario.id, { ...this.paginacaoAtual, pageNumber: (this.paginacaoAtual.pageNumber + 1) }))
  }
  private montarPaginacao(paginacao) {
    this.paginacaoAtual = paginacao;
    let totalPages = this.paginacaoAtual.totalPages;
    let index = 0;
    this.paginas = [];
    while (totalPages >= 0) {
      this.paginas.push(++index);
      totalPages--;
    }
  }
  // --- Fim da Paginacação --- //




}
