import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-data',
  template: `
    <div class="user-data">
      <span>{{label}}</span>
      <small>{{value}}</small>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit {

  @Input() label: string;
  @Input() value: string;

  constructor() { }

  ngOnInit() { }

}
