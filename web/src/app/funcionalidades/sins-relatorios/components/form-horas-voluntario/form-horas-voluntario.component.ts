import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';
import { ProgramaFiltro } from '@app/funcionalidades/programas/models/programa.filtro';
import { Programa } from '@app/funcionalidades/programas/models/programas.model';
import { ProjetoService } from '@app/funcionalidades/projeto/projeto.service';
import { PageModelVm } from '@app/shared/models/page.model';
import { PagedDataModelVm } from '@app/shared/models/paged-data.model';
import { PersistanceService } from '@app/shared/services/persistence.service';
import { HorasVoluntario } from '@shared/models/horas-voluntario.model';
import { VoluntarioDto } from "../../../../shared/models/voluntario-dto.model";
import { nonZero, isInteger } from '@app/shared/utils/validations';
import { Observable } from 'rxjs';


@Component({
  selector: 'form-horas-voluntario',
  templateUrl: './form-horas-voluntario.component.html',
  styleUrls: ['./form-horas-voluntario.component.scss']
})
export class FormHorasVoluntarioComponent implements OnInit {

  @Input() voluntario: VoluntarioDto;
  @Input() horasVoluntarioEditar: HorasVoluntario;
  @Output() voltar: EventEmitter<any> = new EventEmitter();
  @Output() editar: EventEmitter<any> = new EventEmitter();
  @Output() criar: EventEmitter<any> = new EventEmitter();
  horasForm: FormGroup;
  programas$: Observable<Programa[]>;
  loading = false;
  numberOfItemsFromEndBeforeFetchingMore = 1;
  programaOutro = { nome: 'Outros', _id: null }

  constructor(
    private fb: FormBuilder,
    private persistenceService: PersistanceService,
    private readonly projetoService: ProjetoService
  ) { }

  ngOnInit() {
    this.populatePrograma();
    if (this.voluntario) {
      this.carregarForm();
      if (this.horasVoluntarioEditar) {
        this.popularForm();
      }
    }
  }

  _voltar() {
    this.voltar.emit();
  }

  _criar() {
    this.criar.emit(this.prepararRetorno(false));
  }

  _editar() {
    this.editar.emit(this.prepararRetorno(true));
  }

  prepararRetorno(editar: boolean) {
    let horasVoluntario = new HorasVoluntario();
    horasVoluntario.id = editar ? this.horasForm.value.id : null;
    horasVoluntario.horas = this.horasForm.value.horas;
    horasVoluntario.programa = this.horasForm.value.programa._id;
    horasVoluntario.outros = this.horasForm.value.programa._id ? false : true;
    horasVoluntario.responsavel = this.horasForm.value.responsavel;
    horasVoluntario.voluntario = this.horasForm.value.voluntario;
    return horasVoluntario;
  }

  private popularForm() {
    const programa = this.horasVoluntarioEditar.programa ? this.horasVoluntarioEditar.programa : this.programaOutro;
    this.horasForm.get('programa').patchValue(programa);
    this.horasForm.get('horas').patchValue(this.horasVoluntarioEditar.horas);
    this.horasForm.get('id').patchValue(this.horasVoluntarioEditar.id);
  }

  private carregarForm() {
    this.horasForm = this.fb.group({
      id: [],
      programa: [null, Validators.required],
      horas: ['', Validators.compose([Validators.required, nonZero, isInteger])],
      responsavel: [this.persistenceService.get('usuario_instituto').id],
      voluntario: [this.voluntario.id]
    });
  }


  private populatePrograma() {
    this.programas$ = this.projetoService.carregarProgramasInfos(new ProgramaFiltro(null, true, null));
  }

}
