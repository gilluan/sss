import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActionBarConfig, ActionBarRef, ActionBarService, HeaderActionsContainerService } from '@sicoob/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { Observable, Subscription } from 'rxjs';
import { PersistanceService } from '../../../../shared/services/persistence.service';
import { UsuarioInstituto } from '../../../../shared/models/usuario-instituto.model';
import { select, Store } from '@ngrx/store';
import * as fromVoluntario from '../../../../reducers/voluntario.reducer';
import * as fromHorasVoluntario from '../../../../reducers/horas-voluntario.reducers';
import { PagedDataModelVm } from '../../../../shared/models/paged-data.model';
import { VoluntarioDto } from '../../../../shared/models/voluntario-dto.model';
import { VoluntarioFiltroDto } from '../../../../shared/models/voluntario-filtro.model';
import { CooperativaFiltro } from '../../models/cooperativa-filtro.model';
import { SituacaoVoluntarioType } from '../../../sins-analise-cadastros/models/types/situacao-voluntario.type';
import { PageModelVm } from '../../../../shared/models/page.model';
import {
  CarregarCooperativas,
  CarregarVoluntariosPorFiltro,
  DesativarVoluntario,
  AtivarVoluntario
} from '../../../../actions/voluntarioAction';
import { ActionbarDetalheVoluntarioComponent } from '../../components/actionbar-detalhe-voluntario/actionbar-detalhe-voluntario.component';
import { ActionbarFiltroVoluntariosComponent } from '../../components/actionbar-filtro-voluntarios/actionbar-filtro-voluntarios.component';
import { map } from 'rxjs/operators';
import { GedService } from '../../../../shared/ged/ged.service';
import { PerfilType } from '@app/shared/types/perfil.type';
import * as fromPerfilUsuario from '@app/funcionalidades/perfil-usuario/reducers/perfil-usuario.reducer'
import { ListarCursos } from '@app/funcionalidades/perfil-usuario/actions/perfil-usuario.actions';
import { CursoFiltro } from '@app/funcionalidades/perfil-usuario/models/curso.filtro'
import { ActionbarManterHorasComponent } from '../../components/actionbar-manter-horas/actionbar-manter-horas.component';
import { CarregarHorasVoluntario, CleanHorasVoluntario } from '@app/actions/horas-voluntario.actions';

const USUARIO_INSTITUTO = 'usuario_instituto';

@Component({
  selector: 'app-container-banco-voluntarios',
  templateUrl: './container-banco-voluntarios.component.html',
  styleUrls: ['./container-banco-voluntarios.component.scss']
})
export class ContainerBancoVoluntariosComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('headerTpl') headerTpl: TemplateRef<any>;

  subscription: Subscription = new Subscription();
  voluntarios$: VoluntarioDto[];
  cooperativas$: CooperativaFiltro[];
  filtro$: VoluntarioFiltroDto;
  filtrosCooperativa$: string[];
  total$: number;

  private usuarioInstituto: UsuarioInstituto;
  private detalheActionBarRef?: ActionBarRef;
  private filtroActionBarRef?: ActionBarRef;
  private cooperativasObservable: Observable<CooperativaFiltro[]>;

  constructor(
    private readonly storeVoluntario: Store<fromVoluntario.State>,
    private readonly actionBarService: ActionBarService,
    private readonly headerActionService: HeaderActionsContainerService,
    private readonly persistanceService: PersistanceService,
    private readonly gedService: GedService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly cursosStore$: Store<fromPerfilUsuario.State>,
    private readonly horasVoluntarioStore$: Store<fromHorasVoluntario.State>) { }

  ngOnInit(): void {
    this.usuarioInstituto = this.persistanceService.get(USUARIO_INSTITUTO) as UsuarioInstituto;
    this.initFiltro();
    this.pesquisar();
    this.registrarSubscricoes();
  }

  ngAfterViewInit(): void {
    this.headerActionService.open(new TemplatePortal(this.headerTpl, undefined, {}));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();

    /* Por ser um componente global, as actionbars precisam ser fechadas ao término de um página */
    this.notNull(this.detalheActionBarRef, (ref: ActionBarRef) => ref.close());
    this.notNull(this.filtroActionBarRef, (ref: ActionBarRef) => ref.close());

    this.headerActionService.remove();
  }

  pesquisar(): void {
    this.storeVoluntario.dispatch(new CarregarVoluntariosPorFiltro(this.filtro$));
  }

  desativarVoluntario(voluntario: VoluntarioDto): void {
    this.storeVoluntario.dispatch(new DesativarVoluntario(voluntario, this.filtro$));
  }

  ativarVoluntario(voluntario: VoluntarioDto): void {
    this.storeVoluntario.dispatch(new AtivarVoluntario(voluntario, this.filtro$));
  }

  carregarCooperativas(): void {
    this.storeVoluntario.dispatch(new CarregarCooperativas());
  }

  abrirActionBarDetalharVoluntario(cpfVoluntario: string): void {
    const voluntarioSelecionado: VoluntarioDto = this.voluntarios$.filter(i => i.cpf === cpfVoluntario)[0];
    this.cursosStore$.dispatch(new ListarCursos(new CursoFiltro(voluntarioSelecionado.id)))
    const config: ActionBarConfig = {
      data: {
        voluntario: voluntarioSelecionado,
        capacitacoes: this.cursosStore$.pipe(select(fromPerfilUsuario.getCursos))
      }
    };
    this.detalheActionBarRef = this.actionBarService.open(ActionbarDetalheVoluntarioComponent, config);
    const subscription: Subscription = this.detalheActionBarRef.afterClosed()
      .subscribe(data => this.detalharActionBarAfterClose(data, subscription));
  }

  abrirActionBarManterHoras(cpfVoluntario: string): void {
    const voluntarioSelecionado: VoluntarioDto = this.voluntarios$.filter(i => i.cpf === cpfVoluntario)[0];
    const config: ActionBarConfig = {
      data: {
        voluntario: voluntarioSelecionado,
      }
    };
    this.actionBarService.open(ActionbarManterHorasComponent, config).afterClosed().subscribe(_ => {
      this.horasVoluntarioStore$.dispatch(new CleanHorasVoluntario(true))
      this.pesquisar();
    });
  }

  abrirActionBarFiltrarVoluntarios(): void {
    this.carregarCooperativas();
    const config: ActionBarConfig = {
      data: {
        cooperativas: this.cooperativasObservable,
        filtros: this.filtrosCooperativa$
      }
    };
    this.filtroActionBarRef = this.actionBarService.open(ActionbarFiltroVoluntariosComponent, config);
    const subscription: Subscription = this.filtroActionBarRef.afterClosed()
      .subscribe(data => this.filtrarActionBarAfterClose(data, subscription));
  }

  private registrarSubscricoes() {
    const voluntariosObservable = this.storeVoluntario.pipe(select(fromVoluntario.selectVoluntarioPaginado));
    this.subscription
      .add(voluntariosObservable.subscribe(pagedData => this.onVoluntariosCarregados(pagedData)));

    this.cooperativasObservable = this.storeVoluntario.pipe(
      select(fromVoluntario.selectVoluntariosCooperativas),
      map(numerosCooperativas => this.onCooperativasCarregadas(numerosCooperativas))
    );
  }

  private onVoluntariosCarregados(dadosPaginados: PagedDataModelVm): void {
    this.total$ = dadosPaginados.page.total;
    this.voluntarios$ = dadosPaginados.data;
    this.filtro$.paginacao = dadosPaginados.page;
    this.changeDetectorRef.detectChanges();
  }

  private onCooperativasCarregadas(numerosCooperativas: string[]): CooperativaFiltro[] {
    return numerosCooperativas.map(numero => new CooperativaFiltro(numero));
  }

  private initFiltro() {
    this.filtro$ = new VoluntarioFiltroDto();
    this.filtro$.situacao = SituacaoVoluntarioType.assinado;
    // this.filtro$.ativo = true; No banco de voluntário será listado voluntários ativos e desativados
    this.filtro$.paginacao = new PageModelVm(0, 6, 0);
    this.filtro$.cooperativas = [];
   /*  if (this.isPaeOuPde()) {
      this.filtro$.cooperativas = [this.usuarioInstituto.numeroCooperativa];
    } else {
      this.filtro$.cooperativas = [];
    } */
  }

  private isPaeOuPde(): boolean {
    return this.usuarioInstituto && ['pae', 'pde'].indexOf(this.usuarioInstituto.perfil) != -1;
  }

  private detalharActionBarAfterClose(data: any, subscription: Subscription): void {
    if (data !== undefined) {
      const action = data!.action;
      switch (action) {
        case 'desativar': {
          this.desativarVoluntario(data!.data);
          subscription.unsubscribe();
          break;
        }
        case 'ativar': {
          this.ativarVoluntario(data!.data);
          subscription.unsubscribe();
          break;
        }
      }
    }
  }

  private filtrarActionBarAfterClose(data: any, subscription: Subscription): void {
    if (data !== undefined) {
      const action = data!.action;
      switch (action) {
        case 'filtrar': {
          this.filtrosCooperativa$ = data!.data;
          this.filtro$.cooperativas = data!.data;
          this.pesquisar();
          subscription.unsubscribe();
        }
      }
    }
  }

  private notNull(value: any, predicate: (value: any) => void) {
    if (value != null) {
      predicate(value);
    }
  }
}
