import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from "../../../../app.reduce";
import {DisplayToolbar} from "../../../../app.actions";

@Component({
  selector: 'sc-container-sins-relatorios',
  templateUrl: './container-sins-relatorios.component.html',
  styleUrls: ['./container-sins-relatorios.component.css']
})
export class ContainerSinsRelatoriosComponent implements OnInit {

  constructor(private appStore$: Store<fromApp.State>) { }

  ngOnInit() {
    this.appStore$.dispatch(new DisplayToolbar (true));
  }

}
