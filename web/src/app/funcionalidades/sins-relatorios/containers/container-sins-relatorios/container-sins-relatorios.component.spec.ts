import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerSinsRelatoriosComponent } from './container-sins-relatorios.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ContainerSinsRelatoriosComponent', () => {
  let component: ContainerSinsRelatoriosComponent;
  let fixture: ComponentFixture<ContainerSinsRelatoriosComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ContainerSinsRelatoriosComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerSinsRelatoriosComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
