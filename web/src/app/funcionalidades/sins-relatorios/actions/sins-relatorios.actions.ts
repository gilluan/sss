import {Action} from '@ngrx/store';
import {VoluntarioFiltroDto} from "../../../shared/models/voluntario-filtro.model";
import {PagedDataModelVm} from "../../../shared/models/paged-data.model";

export enum SinsRelatoriosActionTypes {
  LoadBancoVoluntarios = '[LoadBancoVoluntarios]',
  LoadBancoVoluntariosSuccess = '[LoadBancoVoluntariosSuccess]',
  LoadBancoVoluntariosFail = '[LoadBancoVoluntariosFail]',
}

export class LoadBancoVoluntarios implements Action {
  readonly type = SinsRelatoriosActionTypes.LoadBancoVoluntarios;
  constructor(public filter: VoluntarioFiltroDto) { }
}

export class LoadBancoVoluntariosSuccess implements Action {
  readonly type = SinsRelatoriosActionTypes.LoadBancoVoluntariosSuccess;
  constructor(public payload: PagedDataModelVm) { }
}

export class LoadBancoVoluntariosFail implements Action {
  readonly type = SinsRelatoriosActionTypes.LoadBancoVoluntariosFail;
  constructor(public payload: PagedDataModelVm) { }
}

export type SinsRelatoriosActions =
  LoadBancoVoluntarios
  | LoadBancoVoluntariosSuccess
  | LoadBancoVoluntariosFail;
