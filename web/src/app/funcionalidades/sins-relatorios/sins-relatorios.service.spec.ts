import { TestBed } from '@angular/core/testing';

import { SinsRelatoriosService } from './sins-relatorios.service';

describe('SinsRelatoriosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SinsRelatoriosService = TestBed.get(SinsRelatoriosService);
    expect(service).toBeTruthy();
  });
});
