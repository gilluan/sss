import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SinsRelatoriosComponent} from "./components/sins-relatorios/sins-relatorios.component";
import {ContainerBancoVoluntariosComponent} from "./containers/container-banco-voluntarios/container-banco-voluntarios.component";
import { ContainerSinsRelatoriosComponent } from './containers/container-sins-relatorios/container-sins-relatorios.component';

const routes: Routes = [
  {
    path: '',
    component: SinsRelatoriosComponent
  },
  {
    path: '',
    component: ContainerSinsRelatoriosComponent,
    children: [
      {
        path: 'banco-voluntarios',
        component: ContainerBancoVoluntariosComponent,
        data: {
          breadcrumb: 'Banco de voluntários'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinsRelatoriosRoutingModule { }
