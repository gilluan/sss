import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HorasVoluntarioEffects } from '@app/effects/horas-voluntario.effects';
import { VoluntarioEffects } from '@app/effects/voluntario.effects';
import { NgxBrModule } from '@nbfontana/ngx-br';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ScAccordionItemModule } from '@shared/components/sc-accordion-item/sc-accordion-item.module';
import { ScChipModule } from '@shared/components/sc-chip/sc-chip.module';
import { ScDatatableModule } from '@shared/components/sc-datatable/sc-datatable.module';
import { ScSectionModule } from '@shared/components/sc-section/sc-section.module';
import { ScSelectionListModule } from '@shared/components/sc-selection-list/sc-selection-list.module';
import { AvatarModule } from '@app/shared/components/sins-avatar/sc-avatar.module';
import { CodigoVoluntarioModule } from '@shared/pipe/codigoVoluntario.module';
import { SharedModule } from '@shared/shared.module';
import { ActionbarModule, FormModule, SidebarContainerModule, TabsModule } from '@sicoob/ui';
import { PopoverModule } from 'ngx-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';
import * as fromHorasVoluntario from '../../reducers/horas-voluntario.reducers';
import * as fromVoluntario from '../../reducers/voluntario.reducer';
import * as fromPrograma from '@app/funcionalidades/programas/reducers/programas.reducer';

import { ActionbarDetalheVoluntarioComponent } from './components/actionbar-detalhe-voluntario/actionbar-detalhe-voluntario.component';
import { ActionbarFiltroVoluntariosComponent } from './components/actionbar-filtro-voluntarios/actionbar-filtro-voluntarios.component';
import { ActionbarManterHorasComponent } from './components/actionbar-manter-horas/actionbar-manter-horas.component';
import { BancoVoluntariosComponent } from './components/banco-voluntarios/banco-voluntarios.component';
import { SinsRelatoriosComponent } from './components/sins-relatorios/sins-relatorios.component';
import { UserDataComponent } from './components/user-data/user-data.component';
import { ContainerBancoVoluntariosComponent } from './containers/container-banco-voluntarios/container-banco-voluntarios.component';
import { ContainerSinsRelatoriosComponent } from './containers/container-sins-relatorios/container-sins-relatorios.component';
import { SinsRelatoriosEffects } from './effects/sins-relatorios.effects';
import * as fromSinsRelatorios from './reducers/sins-relatorios.reducer';
import { SinsRelatoriosRoutingModule } from './sins-relatorios-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ProgramasEffects } from '../programas/effects/programas.effects';
import { FormHorasVoluntarioComponent } from './components/form-horas-voluntario/form-horas-voluntario.component';

@NgModule({
  declarations: [
    ContainerSinsRelatoriosComponent,
    SinsRelatoriosComponent,
    BancoVoluntariosComponent,
    ActionbarManterHorasComponent,
    ActionbarDetalheVoluntarioComponent,
    ActionbarFiltroVoluntariosComponent,
    UserDataComponent,
    ContainerBancoVoluntariosComponent,
    FormHorasVoluntarioComponent
  ],
  imports: [
    NgSelectModule,
    FormsModule,
    FormModule,
    ReactiveFormsModule,
    PopoverModule.forRoot(),
    CommonModule,
    SharedModule,
    ScDatatableModule,
    SidebarContainerModule,
    SinsRelatoriosRoutingModule,
    TabsModule,
    CodigoVoluntarioModule,
    ScAccordionItemModule,
    ScSectionModule,
    ScChipModule,
    FormsModule,
    ActionbarModule,
    ScSelectionListModule,
    NgxPermissionsModule,
    AvatarModule.forRoot(),
    NgxBrModule.forRoot(),
    StoreModule.forFeature('sinsRelatorios', fromSinsRelatorios.reducer),
    StoreModule.forFeature('voluntarioSelector', fromVoluntario.reducer),
    StoreModule.forFeature('horasVoluntarioState', fromHorasVoluntario.reducer),
    StoreModule.forFeature('programaSelector', fromPrograma.reducer),
    EffectsModule.forFeature([SinsRelatoriosEffects, VoluntarioEffects, HorasVoluntarioEffects, ProgramasEffects])
  ],
  entryComponents: [
    ActionbarManterHorasComponent,
    ActionbarDetalheVoluntarioComponent,
    ActionbarFiltroVoluntariosComponent
  ]
})
export class SinsRelatoriosModule { }
