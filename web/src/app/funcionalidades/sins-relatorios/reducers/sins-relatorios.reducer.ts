import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { SinsRelatorios } from '../models/sins-relatorios.model';
import { SinsRelatoriosActions, SinsRelatoriosActionTypes } from '../actions/sins-relatorios.actions';

export interface State extends EntityState<SinsRelatorios> {
  // additional entities state properties
}

export const adapter: EntityAdapter<SinsRelatorios> = createEntityAdapter<SinsRelatorios>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: SinsRelatoriosActions
): State {
  switch (action.type) {
    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
