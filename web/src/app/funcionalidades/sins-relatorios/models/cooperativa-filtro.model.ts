export class CooperativaFiltro {
  numero: string;
  selecionado: boolean;

  constructor(numero: string, selecionado: boolean = false) {
    this.numero = numero;
    this.selecionado = selecionado;
  }
}
