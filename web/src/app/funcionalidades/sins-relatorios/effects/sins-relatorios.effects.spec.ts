import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SinsRelatoriosEffects } from './sins-relatorios.effects';

describe('SinsRelatoriosEffects', () => {
  let actions$: Observable<any>;
  let effects: SinsRelatoriosEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SinsRelatoriosEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(SinsRelatoriosEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
