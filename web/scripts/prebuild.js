#!/usr/bin/env node
const ngenv = require('@sicoob/ngenv');

ngenv
    .generateEnvFromEnvSys()
    .then(result => {
        process.exit(0);
});
