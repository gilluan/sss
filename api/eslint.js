module.exports = {
  "env": {
    "commonjs": true,
    "es6": true,
    "node": true,
    "mocha": true
  },
  "extends": "airbnb-base",
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly",
    "pathRootApp":"readonly"
  },
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    "class-methods-use-this": "off",
    "linebreak-style":"off",
    "no-tabs":"off",
    "eol-last":"off",
    "semi":"off",
    "space-before-function-paren":"off",
    "comma-dangle":"off",
    "padded-blocks":"off",
    "no-underscore-dangle":"off",
    "keyword-spacing":"off",
    "no-use-before-define":"off",
    "import/no-dynamic-require":"off",
    "import/newline-after-import":"off",
    "indent":"off",
    //"indent": ["warn", "tab"],
    "object-shorthand": ["warn", "always"],
    "arrow-body-style":["warn", "as-needed"],
    "prefer-template":["warn"],
    "comma-spacing":["warn"],
    "prefer-destructuring": ["warn", {
      "array": true,
      "object": true
    }, {
      "enforceForRenamedProperties": false
    }],
    "key-spacing":["warn"],
    "no-underscore-dangle":["warn"],
    "object-curly-spacing":["warn"],
    "space-before-blocks":["warn"],
    "prefer-arrow-callback":["warn"],
    "newline-per-chained-call":["warn"],
    "spaced-comment":["warn"],
    "space-infix-ops":["warn"],
    "max-len": ["warn"],
    "vars-on-top":["warn"],
    "no-mixed-spaces-and-tabs":["warn"],
    "no-multi-spaces":["warn"]
  }
};
