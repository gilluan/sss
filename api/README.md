# MODULO GESTAO PESSOA

Sistema de Gestão do Instituto sicoob

Execute `npm install` para baixar as dependencias

## Inicie o projeto

Execute `npm start` .
Navigate to `http://localhost:3000/api/`. 

## Comandos de desenvolvimento
 ng build 
 node app.js
 
## Configuracao arquivo .npmrc

registry=http://registry.npmjs.org/

@sicoob:registry=http://nexus.sicoob.com.br/repository/npm-sicoob/

proxy=http://user_name:password@pac.sicoob.com.br/sicoob.pac

https_proxy=https://192.168.100.178:80

strict-ssl=false

## Atualizando a versão do @sicoob/ui

npm install @sicoob/ui@latest --save