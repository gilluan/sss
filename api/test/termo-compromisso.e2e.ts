import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import bodyParser = require('body-parser');
import { async } from 'rxjs/internal/scheduler/async';

describe('Termo de Compromisso Controller (e2e)', () => {
  let app: INestApplication;
  let mongoose = require('mongoose');
  let perfil = {nome:null}

  beforeAll(async (done) => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.init();

    mongoose.connect('mongodb://localhost:27017/sins');
    let db = mongoose.connection.on('connected', function () {  
      console.log('Mongoose default connection open');
    }); 

    let chat = db.collection('projetos');
    //chat.insertOne({author:'Jefferson', message: 'mensagem', date:''}).catch(err =>{
      //console.log(err)
    //})

    done();
    
  });

  afterAll( (done) => {
    mongoose.close();
    done();
  })

  it('/ (POST) Termo', async (done) => {
    perfil.nome = `Perfil Teste E2E ${Math.random().toFixed(10)}`;
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('')
    .send(perfil).then(
      resp => {
        console.log(resp.body);
        expect(resp.body);
        done();
      }
    )  
  });

});
