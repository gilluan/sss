import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import bodyParser = require('body-parser');
import * as mongoose from 'mongoose';

describe('Termo de Compromisso Controller (e2e)', () => {
  let app: INestApplication;
  let perfilsDB;
  let voluntariosDB;
  let voluntarios = [];

  beforeAll(async (done) => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.init();

    done();

    await  mongoose.connect('mongodb://localhost:27017/sins');
    let db = mongoose.connection.on('connected', function () {  
      console.log('Mongoose default connection open');
    }); 

    perfilsDB = await db.collection('perfils');
    voluntariosDB = await db.collection('voluntarios');
    
    await voluntariosDB.deleteMany({ profissao: voluntario.profissao }, function (err) { });

  })

  let perfil = {
    _id: mongoose.Types.ObjectId('5c4a1f111368fd5e65b00fa9'),
     nome:'Jefferson'
  }

  enum voluntarioTipo {
    Sicoob = 'Sicoob',
    Externo = 'Externo'
  }

  let voluntario = {
    voluntarioTipo: voluntarioTipo.Sicoob,
    idPerfil: perfil._id,
    loginVoluntario: 'teste.e2e',
    nome: 'End to End',
    rg: '26529910',
    cpf: '012345678909',
    dataNascimento: new Date(),
    nacionalidade: 'Pernambucano',
    naturalidade: 'Brasileiro',
    profissao: 'E2E',
    telefone: '+556191295524',
    email: 'testee2e@gmail.com',
    endCep: '72630300',
    endLogradouro: 'Qd 400, CJ 200 CS, 50',
    endNumero: '20',
    endBairro: 'Auracarias',
    endCidade: 'Recanto das Emas',
    endUf: 'DF',
    idDocumentoCertificado: 'xpto'
  }

  afterAll( async (done)  => {
    await voluntariosDB.deleteMany({ profissao: voluntario.profissao }, function (err) {
      //console.log(err)
    });

    await perfilsDB.deleteMany({ nome: perfil.nome }, function (err) {
      //console.log(err)
    });
    mongoose.connection.close();
    done();
  })
  
  it('Inluir Voluntario - Perfil Inexistente', async (done) => {
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/voluntario/register')
    .send(voluntario)
    .then(
      resp => {
        expect(resp.status).toEqual(500);
        expect(resp.body.message).toEqual('Não existe um Perfil com o idPerfil informado.');
      }
      )  
      done();
  });
  
  it('Inluir Voluntario', async (done) => {
    await perfilsDB.insertOne(perfil);
    voluntario.nome = voluntario.nome + Math.random().toFixed(10);
    voluntario.loginVoluntario = voluntario.loginVoluntario + Math.random().toFixed(10);
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/voluntario/register')
    .send(voluntario)
    .then(
      resp => {
        expect(resp.status).toEqual(201);
        expect(resp.body.cpf).toEqual(voluntario.cpf);
        expect(new Date(resp.body.dataNascimento)).toEqual(voluntario.dataNascimento)
        expect(resp.body.email).toEqual(voluntario.email);
        expect(resp.body.endBairro).toEqual(voluntario.endBairro);
        expect(resp.body.endCep).toEqual(voluntario.endCep);
        expect(resp.body.endCidade).toEqual(voluntario.endCidade);
        expect(resp.body.endLogradouro).toEqual(voluntario.endLogradouro);
        expect(resp.body.endNumero).toEqual(voluntario.endNumero);
        expect(resp.body.endUf).toEqual(voluntario.endUf);
        expect(resp.body.idDocumentoCertificado).toEqual(voluntario.idDocumentoCertificado);
        expect(mongoose.Types.ObjectId(resp.body.idPerfil)).toEqual(voluntario.idPerfil);
        expect(resp.body.loginVoluntario).toEqual(voluntario.loginVoluntario);
        expect(resp.body.nacionalidade).toEqual(voluntario.nacionalidade);
        expect(resp.body.naturalidade).toEqual(voluntario.naturalidade);
        expect(resp.body.nome).toEqual(voluntario.nome);
        expect(resp.body.profissao).toEqual(voluntario.profissao);
        expect(resp.body.rg).toEqual(voluntario.rg);
        expect(resp.body.telefone).toEqual(voluntario.telefone);
        expect(resp.body.voluntarioTipo).toEqual(voluntario.voluntarioTipo);
        expect(resp.body.createdAt).not.toBeNull();
        expect(resp.body.createdAt).not.toBeNull();
        voluntarios.push(resp.body);
      }
      )  
      done();
  });
  
  it('Inluir Voluntario - Login Duplicado', async (done) => {
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/voluntario/register')
    .send(voluntarios[0])
    .then(
      resp => {
        expect(resp.status).toEqual(500);
        expect(resp.body.message).toEqual(`Já existe um voluntário com o Login \'${voluntario.loginVoluntario}\'`)
      })  
      done();
  });

  it('Atualizar Voluntario', async (done) => {
      voluntarios[0].voluntarioTipo = voluntarioTipo.Externo;
      voluntarios[0].codigoVoluntario = 'NOVO login e2e - Teste Atualizar';
      voluntarios[0].nome = 'NOVO End to End';
      voluntarios[0].rg = '99999999';
      voluntarios[0].cpf = '37373214690';
      voluntarios[0].dataNascimento = new Date();
      voluntarios[0].nacionalidade = 'Brasileira';
      voluntarios[0].naturalidade = 'Havana';
      voluntarios[0].telefone = '+556191553355';
      voluntarios[0].email = 'novosicoob@gmail.com';
      voluntarios[0].endCep = '72630300';
      voluntarios[0].endLogradouro = 'NOVO Qd 400, CJ 200 CS, 50';
      voluntarios[0].endNumero = 'NOVO 20';
      voluntarios[0].endBairro = 'NOVO Auracarias';
      voluntarios[0].endCidade = 'NOVO Recanto das Emas';
      voluntarios[0].endUf = 'MA';
      voluntarios[0].idDocumentoCertificado = ' NOVO xpto documento';

      await request(app.use(bodyParser.json())
      .getHttpServer())
      .put('/voluntario/update')
      .send(voluntarios[0])
      .then(resp =>{
        expect(resp.status).toEqual(200);
        expect(resp.body._doc.nome).toEqual(voluntarios[0].nome);
        expect(resp.body._doc.loginVoluntario).toEqual(voluntarios[0].loginVoluntario);
        expect(resp.body._doc.cpf).toEqual(voluntarios[0].cpf);
        expect(resp.body._doc.email).toEqual(voluntarios[0].email);
        expect(resp.body._doc.endBairro).toEqual(voluntarios[0].endBairro);
        expect(resp.body._doc.endCep).toEqual(voluntarios[0].endCep);
        expect(resp.body._doc.endCidade).toEqual(voluntarios[0].endCidade);
        expect(resp.body._doc.endLogradouro).toEqual(voluntarios[0].endLogradouro);
        expect(resp.body._doc.endNumero).toEqual(voluntarios[0].endNumero);
        expect(resp.body._doc.endUf).toEqual(voluntarios[0].endUf);
        expect(resp.body._doc.idDocumentoCertificado).toEqual(voluntarios[0].idDocumentoCertificado);
        expect(resp.body._doc.loginVoluntario).toEqual(voluntarios[0].loginVoluntario);
        expect(resp.body._doc.nacionalidade).toEqual(voluntarios[0].nacionalidade);
        expect(resp.body._doc.naturalidade).toEqual(voluntarios[0].naturalidade);
        expect(resp.body._doc.nome).toEqual(voluntarios[0].nome);
        expect(resp.body._doc.profissao).toEqual(voluntarios[0].profissao);
        expect(resp.body._doc.rg).toEqual(voluntarios[0].rg);
        expect(resp.body._doc.telefone).toEqual(voluntarios[0].telefone);
        expect(resp.body._doc.voluntarioTipo).toEqual(voluntarios[0].voluntarioTipo);
        expect(resp.body._doc.createdAt).not.toBeNull();
        expect(resp.body._doc.createdAt).not.toBeNull();
        done();
      })
  });

  it('Atualizar Voluntario - Sem Perfil', async (done) => {
    await perfilsDB.deleteMany({ nome: perfil.nome }, function (err) { });
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .put('/voluntario/update')
    .send(voluntarios[0])
    .then(resp =>{
      console.log(resp.body)
      expect(resp.status).toEqual(500);
      expect(resp.body.message).toEqual('Não existe um Perfil com o idPerfil informado.')
      done();
    })

});

});
