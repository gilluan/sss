import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import bodyParser = require('body-parser');
import { async } from 'rxjs/internal/scheduler/async';

describe('Perfil Controller (e2e)', () => {
  let app: INestApplication;
  let mongoose = require('mongoose');
  let perfil = {nome:null}

  beforeAll(async (done) => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.init();

    done();
    
  });
  
  afterAll( (done) => {
    mongoose.close();
    done();
  })

  it('/ (POST) Incluir Novo Perfil', async (done) => {
    perfil.nome = `Perfil Teste E2E ${Math.random().toFixed(10)}`;
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/perfil/register')
    .send(perfil).then(
      resp => {
        console.log(resp.body);
        expect(resp.body);
        expect(resp.body).toHaveProperty('nome');
        expect(resp.body).toHaveProperty('id');
        done();
      }
    )  
  });

  it('/ (POST) Incluir Novo Perfil Duplicado', async (done) => {
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/perfil/register')
    .send(perfil).then(
      resp => {
        //console.log(resp);
        expect(resp.body);
        expect(resp.body).toHaveProperty('nome');
        expect(resp.body).toHaveProperty('id');
        done();
      }
    )  
  });

  it('/ (POST)', async (done) => {
    await request(app.use(bodyParser.json())
    .getHttpServer())
    .post('/perfil/register')
    .then(
      resp => {
        expect(resp.status).toEqual(400);
        expect(resp.body.message).toEqual('Nome do perfil é obrigatório')
        done();
      })  
  });

});
