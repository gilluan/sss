import { CategoriaCursoController } from "./categoria-curso.controller";
import { CategoriaCursoService } from "./categoria-curso.service";
import { MongooseModule } from "@nestjs/mongoose";
import { CategoriaCurso } from "./models/categoria-curso.model";
import { Module } from "@nestjs/common";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: CategoriaCurso.modelName, schema: CategoriaCurso.model.schema }])
    ],
    providers: [CategoriaCursoService],
    controllers: [CategoriaCursoController],
    exports: [CategoriaCursoService],
})
export class CategoriaCursoModule {
}
