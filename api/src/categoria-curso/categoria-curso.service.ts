import { Injectable } from "@nestjs/common";
import { ComumService } from "@sins/comum";
import { CategoriaCurso, CategoriaCursoModel } from "./models/categoria-curso.model";
import { CategoriaCursoVm } from "./view-models/categoria-curso-vm";
import { InjectModel } from "@nestjs/mongoose";
import { InstanceType, ModelType } from 'typegoose';

@Injectable()
export class CategoriaCursoService extends ComumService<CategoriaCurso>{

    constructor(@InjectModel(CategoriaCurso.modelName) public readonly _categoriaCursoModel: ModelType<CategoriaCurso>) {
        super();
        this._model = _categoriaCursoModel;
    }

    public async inserir(eixoVm: CategoriaCursoVm) {
        const categoriaCurso = new CategoriaCursoModel(eixoVm);
        await this._model.create(categoriaCurso);
    }

    public async list(params: any) {
        let filtro = {};
        let categoriasCurso: Array<CategoriaCursoVm> = new Array<CategoriaCursoVm>();
        if (params && params.nome)
            filtro = { nome: new RegExp(params.nome, 'i') };
        const result = await this._model.find(filtro);
        if (result && result.length > 0) {
            result.forEach(r => {
                categoriasCurso.push(this.getCategoriaCursoVm(r));
            });
        } else if(JSON.stringify(filtro) == JSON.stringify({})) {
            await this._model.insertMany(this.popularCategoriasCurso());
            return await this.list(null);
        }
        return { resultado: categoriasCurso };
    }

    private getCategoriaCursoVm(categoriaCurso: InstanceType<CategoriaCurso>) {
        let categoriaCursoVm = new CategoriaCursoVm();
        categoriaCursoVm.nome = categoriaCurso.nome;
        categoriaCursoVm.descricao = categoriaCurso.descricao;
        categoriaCursoVm.icone = categoriaCurso.icone;
        categoriaCursoVm.id = categoriaCurso.id;
        return categoriaCursoVm;
    }

    private popularCategoriasCurso() {
        return [
            {
                descricao: 'Destina-se a cursos que abrangem a área de conhecimento financeiro.',
                icone: 'assets/images/categoria-cursos/educacao_financeira.png',
                nome: 'Educação Financeira',
                dataCriacao: new Date(),
                dataModificacao: new Date(),
            },
            {
                descricao: 'Destina-se a cursos que abrangem a área de conhecimento cooperativista.',
                icone: 'assets/images/categoria-cursos/cooperativismo.png',
                nome: 'Cooperativismo',
                dataCriacao: new Date(),
                dataModificacao: new Date(),
            },
            {
                descricao: 'Destina-se a cursos que abrangem a área de conhecimento em Gestão e Liderança.',
                icone: 'assets/images/categoria-cursos/empreendedorimo.png',
                nome: 'Empreendedorismo',
                dataCriacao: new Date(),
                dataModificacao: new Date(),
            },
            {
                descricao: 'Destina-se a cursos que abrangem a área de conhecimento Social e de Voluntário.',
                icone: 'assets/images/categoria-cursos/consciencia_social.png',
                nome: 'Consciência Social',
                dataCriacao: new Date(),
                dataModificacao: new Date(),
            },
            {
                descricao: 'Destina-se a cursos que abrangem a área de conhecimento ambiental.',
                icone: 'assets/images/categoria-cursos/educacao_ambiental.png',
                nome: 'Educação Ambiental',
                dataCriacao: new Date(),
                dataModificacao: new Date(),
            }
        ]

    }

}