import { Body, Controller, Delete, Get, Param, Post, Put, Query } from "@nestjs/common";
import { CategoriaCursoService } from "./categoria-curso.service";
import { CategoriaCursoVm } from "./view-models/categoria-curso-vm";

@Controller('categorias-curso')
export class CategoriaCursoController {

    constructor(private service: CategoriaCursoService) {

    }

    @Post()
    inserir(@Body() eixoVm: CategoriaCursoVm) {
        this.service.inserir(eixoVm);
    }

    @Get()
    listar(@Query() param: any) {
        return this.service.list(param);
    }

    @Put()
    atualizar(@Body()  item: CategoriaCursoVm) {
        return this.service.updateById(item.id, item);
    }

    @Delete(':id')
    deletar(@Param() id: string) {
        return this.service.delete(id);
    }
}