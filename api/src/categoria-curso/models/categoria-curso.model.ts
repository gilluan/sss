import { Typegoose, prop, ModelType, pre } from "typegoose";
import { SchemaOptions } from "mongoose";

@pre<CategoriaCurso>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class CategoriaCurso extends Typegoose {

    @prop({required: [true, 'Nome da Categoria é obrigatório']})
    nome: string;

    @prop({required: [true, 'Descrição da categoria é obrigatório']})
    descricao: string;

    @prop({required: [true, 'Ícone da categoria é obrigatório']})
    icone: string;

    @prop({ default: Date.now, index: true })
    dataCriacao: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao: Date;

    static get model(): ModelType<CategoriaCurso> {
        return new CategoriaCurso().getModelForClass(CategoriaCurso, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
    
}

const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    collection: 'categoria-curso'
};

export const CategoriaCursoModel = new CategoriaCurso().getModelForClass(CategoriaCurso, { schemaOptions });