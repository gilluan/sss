import { ComumModelVm } from "@sins/comum";

export class CategoriaCursoVm extends ComumModelVm {
    nome: string;
    descricao: string;
    icone: string;
}