import { ModelType, pre, prop, Typegoose } from 'typegoose';
import { schemaOptions } from '@sins/comum';
import { Types } from 'mongoose';
import { JustificativaType } from './justificativa.type';

@pre<JustificativaAjuste>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class JustificativaAjuste extends Typegoose {

    @prop({ default: Date.now })
    dataCriacao?: Date;

    @prop({ default: Date.now })
    dataModificacao?: Date;

    @prop({
        required: [true, 'Justificativa é obrigatória']
    }) justificativa: string;

    @prop({}) idVoluntario: string;

    @prop({}) idAcao: Types.ObjectId;

    @prop({}) tipo: JustificativaType;

    @prop({
        required: [true, 'o voluntário requisitante dos ajustes é obrigatório']
    }) idVoluntarioCriador: string;

    static get model(): ModelType<JustificativaAjuste> {
        return new JustificativaAjuste().getModelForClass(JustificativaAjuste, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const JustificativaAjusteModel = new JustificativaAjuste().getModelForClass(JustificativaAjuste, { schemaOptions });
