
import {ComumModelVm} from '@sins/comum';
import { JustificativaType } from '../justificativa.type';

export class JustificativaAjusteVm extends ComumModelVm {
    justificativa: string;
    idAcao: string;
    idVoluntario: string;
    idVoluntarioCriador: string;
    nomeVoluntario: string;
    nomeVoluntarioCriador: string;
    emailVoluntarioCriador: string;
    tipo: JustificativaType;
}
