export enum JustificativaType {
    analiseVoluntario = 'analiseVoluntario',
    impedimentoAcao = 'impedimentoAcao',
    investimentoMaior = 'investimentoMaior',
    alteracaoPlano = 'alteracaoPlano',
    alteracaoInvestimento = 'alteracaoInvestimento'
}

export namespace JustificativaType {
    export function values(): JustificativaType[] {
        return [
            JustificativaType.analiseVoluntario,
            JustificativaType.impedimentoAcao,
            JustificativaType.investimentoMaior,
            JustificativaType.alteracaoPlano,
            JustificativaType.alteracaoInvestimento,
        ]
    }
}
