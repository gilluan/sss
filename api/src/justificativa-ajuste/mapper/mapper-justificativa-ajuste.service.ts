import { Injectable } from '@nestjs/common';
import 'automapper-ts/dist/automapper';

@Injectable()
export class MapperJustificativaAjusteService {
    mapper: AutoMapperJs.AutoMapper;

    constructor() {
        this.mapper = automapper;
        this.initializeMapper();
    }

    private initializeMapper(): void {
        this.mapper.initialize(MapperJustificativaAjusteService.configure);
    }

    private static configure(config: AutoMapperJs.IConfiguration): void {
        config
            .createMap('JustificativaAjuste', 'JustificativaAjusteVm')
            .forSourceMember('_id', opts => opts.ignored());

    }
}