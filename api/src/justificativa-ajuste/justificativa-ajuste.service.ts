import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService } from '@sins/comum';
import { ModelType } from 'typegoose';
import { PerfilType } from '../perfil/models/perfils.enum';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';
import { SituacaoVoluntario } from '../voluntario/models/types/situacao-voluntario.type';
import { VoluntarioService } from '../voluntario/voluntario.service';
import { MapperJustificativaAjusteService } from './mapper/mapper-justificativa-ajuste.service';
import { JustificativaAjuste, JustificativaAjusteModel } from './models/justificativa-ajuste.model';
import { JustificativaType } from './models/justificativa.type';
import { JustificativaAjusteVm } from './models/view-models/justificativa-ajuste-vm.model';
import { VoluntarioVm } from '../voluntario/models/view-models/voluntario-vm.model';

@Injectable()
export class JustificativaAjusteService extends ComumService<JustificativaAjuste>{


    constructor(
        @InjectModel(JustificativaAjuste.modelName) private readonly _justificativaAjusteModel: ModelType<JustificativaAjuste>,
        @Inject(forwardRef(() => VoluntarioService))
        private _voluntarioService: VoluntarioService,
        private _usuarioSicoobService: UsuarioInstitutoService,
        private readonly _mapperService: MapperJustificativaAjusteService,
    ) {
        super();
        this._model = _justificativaAjusteModel;
        this._mapper = _mapperService.mapper;
    }

    private validarCamposObrigatorios(justificativaAjuste: JustificativaAjusteVm) {
        if (!justificativaAjuste.justificativa) {
            throw new HttpException('A justificativa é um campo obrigatório', HttpStatus.BAD_REQUEST);
        }
    }

    private getNewJustificativa(vm: JustificativaAjusteVm, idVoluntarioCriador: string): any {
        let newJustificativa = new JustificativaAjusteModel();
        newJustificativa.justificativa = vm.justificativa;
        newJustificativa.idVoluntario = vm.idVoluntario;
        newJustificativa.idVoluntarioCriador = idVoluntarioCriador;
        newJustificativa.idAcao = vm.idAcao ? this.toObjectId(vm.idAcao) : null;
        newJustificativa.tipo = vm.tipo;
        return newJustificativa;
    }

    private getNewJustificativaVm(just: JustificativaAjuste, nomeVoluntario: string, nomeVoluntarioCriador: string, id: string) {
        let newJustificativaVm = new JustificativaAjusteVm();
        newJustificativaVm.id = id;
        newJustificativaVm.dataCriacao = just.dataCriacao;
        newJustificativaVm.dataModificacao = just.dataModificacao;

        newJustificativaVm.justificativa = just.justificativa;
        newJustificativaVm.idVoluntario = just.idVoluntario;
        newJustificativaVm.idAcao = just.idAcao ? new String(just.idAcao).toString() : null;
        newJustificativaVm.idVoluntarioCriador = just.idVoluntarioCriador;
        newJustificativaVm.tipo = just.tipo;

        newJustificativaVm.nomeVoluntario = nomeVoluntario;
        newJustificativaVm.nomeVoluntarioCriador = nomeVoluntarioCriador;

        return newJustificativaVm;
    }

    /**
     * Quando se cadastra uma nova justificativa automaticamente
     * o usuário irá atualizar a situação do voluntário
     */
    async register(vm: JustificativaAjusteVm) {
        this.validarCamposObrigatorios(vm);
        let voluntarioCriador = await this._usuarioSicoobService.findById(vm.idVoluntarioCriador);
        if (vm.idVoluntario) {
            if (voluntarioCriador) {
                let perfilCriador = voluntarioCriador.perfil;
                let updateVoluntario = await this._voluntarioService.findById(vm.idVoluntario);
                vm.idVoluntarioCriador = voluntarioCriador._id;
                const newJustificativa = this.getNewJustificativa(vm, voluntarioCriador.id);
                if (perfilCriador == PerfilType.superintendente) {
                    let vmV = new VoluntarioVm();
                    vmV.situacao = SituacaoVoluntario.devolvidoAjuste
                    await this._voluntarioService.enviarNotificacao( updateVoluntario, vmV.situacao, vm.justificativa)
                    updateVoluntario.situacao = vmV.situacao as SituacaoVoluntario;
                } else if (perfilCriador == PerfilType.analistaProjeto) {
                    let vmV = new VoluntarioVm();
                    vmV.situacao = SituacaoVoluntario.ajusteVoluntario
                    await this._voluntarioService.enviarNotificacao( updateVoluntario, vmV.situacao, vm.justificativa)
                    updateVoluntario.situacao = vmV.situacao as SituacaoVoluntario;
                }
                return this._voluntarioService.updateById(updateVoluntario.id, updateVoluntario)
                    .then(() => this.create(newJustificativa)
                        .then(result => (result.toJSON() as JustificativaAjuste))
                        .catch(error => { throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR) }))
                    .catch(error => { throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR) })
            } else {
                throw new HttpException(`O usuário criador não encontrado ${vm.idVoluntarioCriador}`, HttpStatus.NOT_FOUND);
            }
        } else if (vm.idAcao) {
            if (voluntarioCriador) {
                return this.create(this.getNewJustificativa(vm, voluntarioCriador.id))
                    .then(result => (result.toJSON() as JustificativaAjuste))
                    .catch(error => { throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR) });
            } else {
                throw new HttpException(`O usuário criador não encontrado ${vm.idVoluntarioCriador}`, HttpStatus.NOT_FOUND);
            }
        }
    }

    async findJustificativaByIdVoluntario(params: any) {
        let filter = this.montarParametros(params);
        let result = await this._model.find({ $and: filter }).sort({ "dataCriacao": -1 }).exec();
        if (!params.tipo) {
            if(result && result.length > 0) {
                let nomeVoluntario = await this._voluntarioService.recuperarNomeVoluntarioById(result[0].idVoluntario);
                let nomeVoluntarioCriador = await this._usuarioSicoobService.recuperarNomeById(result[0].idVoluntarioCriador);
                let array: Array<JustificativaAjusteVm> = [];
                result.forEach(element => {
                    array.push(this.getNewJustificativaVm(element, nomeVoluntario.nome, nomeVoluntarioCriador.nome, element.id));
                });
                return array;
            }
        }
        return result;
    }

    private montarParametros(params) {
        let filter = [];
        if (params) {
            let idVoluntarioCriador = params.idVoluntarioCriador;
            let idVoluntario = params.idVoluntario;
            let tipo = params.tipo;
            let idAcao = params.idAcao;
            if (idVoluntarioCriador) {
                filter.push({ idVoluntarioCriador });
            }
            if (idVoluntario) {
                filter.push({ idVoluntario });
            }
            if (idAcao) {
                filter.push({ idAcao });
            }
            if (tipo) {
                filter.push({ tipo });
            }
        }
        return filter;
    }
}