import { Module, forwardRef } from '@nestjs/common';
import { JustificativaAjusteService } from './justificativa-ajuste.service';
import { JustificativaAjusteController } from './justificativa-ajuste.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { JustificativaAjuste } from './models/justificativa-ajuste.model';
import { MapperJustificativaAjusteService } from './mapper/mapper-justificativa-ajuste.service';
import { VoluntarioModule } from '../voluntario/voluntario.module';
import { UsuarioInstitutoModule } from '../usuario-instituto/usuario-instituto.module';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';

@Module({
  imports: [
    forwardRef(() => VoluntarioModule),
    UsuarioInstitutoModule, MongooseModule.forFeature([{ name: JustificativaAjuste.modelName, schema: JustificativaAjuste.model.schema }])],
  providers: [MapperJustificativaAjusteService, JustificativaAjusteService, UsuarioInstitutoService ],
  controllers: [JustificativaAjusteController],
  exports: [JustificativaAjusteService, MapperJustificativaAjusteService]
})
export class JustificativaAjusteModule {}
