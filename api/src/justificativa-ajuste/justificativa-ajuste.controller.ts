import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { InstanceType } from 'typegoose';
import { JustificativaAjusteService } from './justificativa-ajuste.service';
import { JustificativaAjuste } from './models/justificativa-ajuste.model';
import { JustificativaAjusteVm } from './models/view-models/justificativa-ajuste-vm.model';



@Controller('justificativas')
export class JustificativaAjusteController {

    constructor(private readonly service: JustificativaAjusteService) { }

    private mapTo<T>(object: any): Promise<T> {
        return this.service.map<T>(object);
    }

    @Post("/")
    async register(@Body() vm: JustificativaAjusteVm): Promise<JustificativaAjusteVm> {
        return this.service.register(vm).then(justificativaAjuste => this.mapTo<JustificativaAjusteVm>(justificativaAjuste));
    }

    @Get('/')
    findById(@Query() params): Promise<JustificativaAjusteVm[] | InstanceType<JustificativaAjuste>[]> {
        return this.service.findJustificativaByIdVoluntario(params);
    }

}
