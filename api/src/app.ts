import { NestFactory } from '@nestjs/core';
import { HttpExceptionFilter, AppLogger } from '@sins/comum';
import { AppModule } from './app.module';

declare const module: any;

async function bootstrap() {

  const app = await NestFactory.create(AppModule, {
    logger: new AppLogger()
  });
  
  //const hostDomain = AppModule.isDev ? `${AppModule.host}:${AppModule.port}` : AppModule.host;

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    next();
  });

  app.enableCors();
  app.setGlobalPrefix('api');
  app.useGlobalFilters(new HttpExceptionFilter());
  //app.useGlobalGuards(new JwtAuthGuard());

  await app.listen(AppModule.port);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }

}
bootstrap();