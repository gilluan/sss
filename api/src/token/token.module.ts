import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Token } from './models/token.model';
import { TokenService } from './token.service';
import { TokenController } from './token.controller';
import { MapperService } from './mapper/mapper.service';
import { MailSinsService, TemplateWork } from '@sins/comum';

@Module({
    imports: [MongooseModule.forFeature([{ name: Token.modelName, schema: Token.model.schema }])],
    providers: [MapperService, TokenService, MailSinsService, TemplateWork],
    controllers: [TokenController],
    exports: [TokenService, MapperService],
})
export class TokenModule {
}