import { Body, Controller, HttpException, HttpStatus, Post, Get, Query, Param, Patch } from '@nestjs/common';
import { Token } from './models/token.model';
import { TokenService } from './token.service';
import { TokenVm } from './models/view-models/token-vm.model';

import { ApiException } from '@sins/comum';
import { GetOperationId } from '@sins/comum';
import { TokenResultVm } from './models/view-models/token-result-vm.model';

@Controller('tokens')
export class TokenController {

    constructor(private readonly service: TokenService) { }

    private mapTo<T>(object: any): Promise<T> {
        return this.service.map<T>(object);
    }

    @Post()
    async register(@Body() vm: TokenVm): Promise<TokenResultVm> {
        return this.service.registerToken(vm).then(result => result);
    }

    @Patch(':token/validar/:cpf')
    async validateToken(@Param('token') token: string, @Param('cpf') cpf: string ): Promise<TokenResultVm> {
        return this.service.validateToken(token, cpf).then(result => result);
    }    
}
