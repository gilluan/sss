import { ModelType, pre, prop, Typegoose, plugin } from 'typegoose';
import { schemaOptions } from '@sins/comum';

@pre<Token>('findOneAndUpdate', function(next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class Token extends Typegoose {
    
    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao: Date;
    
    @prop({})
    token: string;

    @prop({
        required: [true, 'CPF é obrigatório']
    })
    cpf: string;

    @prop({
        required: [true, 'Email é obrigatório']
    })
    email: string;

    
    @prop({
        required: [true, 'Nome é obrigatório']
    })
    nome: string;
    

    static get model(): ModelType<Token> {
        return new Token().getModelForClass(Token, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const TokenModel = new Token().getModelForClass(Token, { schemaOptions });
