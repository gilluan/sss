import { ComumModelVm } from '@sins/comum';

export class TokenResultVm extends ComumModelVm {
    isValid: boolean;
    reason: string;
}
