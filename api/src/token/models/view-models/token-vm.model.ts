import { ComumModelVm } from '@sins/comum';

export class TokenVm extends ComumModelVm {
    token: string;
    cpf: string;
    email: string;
    nome: string;
}
