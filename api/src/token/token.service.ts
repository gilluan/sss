import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService, MailSinsService, TemplateWork } from '@sins/comum';
import { ModelType } from 'typegoose';
import { MapperService } from './mapper/mapper.service';
import { Token, TokenModel } from './models/token.model';
import { TokenResultVm } from './models/view-models/token-result-vm.model';
import { TokenVm } from './models/view-models/token-vm.model';

@Injectable()
export class TokenService extends ComumService<Token> {

    private fs = require('fs');

    private tokenResultFactory(): TokenResultVm {
        return new TokenResultVm();
    }

    private validateCPF(token: TokenVm): any {
        let tokenResultVm: TokenResultVm = this.tokenResultFactory();
        tokenResultVm.isValid = true;
        tokenResultVm.reason = "";
        if (!token.cpf) {
            tokenResultVm.isValid = false;
            tokenResultVm.reason = "CPF é obrigatório";
            return tokenResultVm;
        }
        return tokenResultVm;
    }

    private validateFieldsToken(token: TokenVm): any {
        let tokenResultVm: TokenResultVm = this.tokenResultFactory();
        tokenResultVm.isValid = true;
        tokenResultVm.reason = "";
        if (!token.token) {
            tokenResultVm.isValid = false;
            tokenResultVm.reason = "Token não preenchido";
            return tokenResultVm;
        }
        return tokenResultVm;
    }

    private getNewToken(vm: TokenVm): any {
        let newToken = new TokenModel();
        newToken.token = vm.token;
        newToken.cpf = vm.cpf;
        newToken.nome = vm.nome;
        newToken.email = vm.email;
        return newToken;
    }

    private getTokenResult(message: string, isValid: boolean): TokenResultVm {
        let tokenResultVm: TokenResultVm = this.tokenResultFactory();
        tokenResultVm.isValid = isValid;
        tokenResultVm.reason = message;
        return tokenResultVm;
    }

    static email = `<!DOCTYPEhtml><htmllang="pt-br"><head><metacharset="UTF-8"><metaname="viewport"content="width=device-width,initial-scale=1.0"></head><body><tableclass="container"align="center"width="100%"cellpadding="0"cellspacing="0"style="max-width:800px;margin:0auto;border-radius:10px"><tr><tdcolspan="3"></td><tdcolspan="3">Prezado(a)Voluntário<b>/%NOME%/</b><br><pstyle="text-align:justify;padding-left:5em">Utilize o Token /%TOKEN%/ para confirmação do seuc adastro.<br></p></td></tr></table></body><style>@mediascreenand(min-width:641px){.container{width:640px!important;}}</style></html>`;
    static path = './src/resources/index-token.html';

    async registerToken(vm: TokenVm) {
        let tokenResultVm: TokenResultVm = this.validateCPF(vm);
        if (!tokenResultVm.isValid) {
            return tokenResultVm;
        }
        vm.token = this.generateToken();
        return this.findOne({ cpf: vm.cpf })
            .then(resultFound => {
                if (resultFound != null && resultFound != undefined) {
                    resultFound.token = vm.token;
                    return this.updateById(resultFound.id, resultFound)
                        .then(resultUpdated => {
                            let params = [{ searchValue: 'TOKEN', replaceValue: resultUpdated.token }, { searchValue: 'NOME', replaceValue: vm.nome, }];
                            this.templateWork.sendMail(vm.email, TokenService.path, params, null, TokenService.email);
                            return this.getTokenResult("Sucesso.", true);
                        })
                        .catch(error => {
                            return this.getTokenResult("Ocorreu um erro: " + error, false);
                        });
                } else {
                    return this.create(this.getNewToken(vm))
                        .then(resultCreated => {
                            let params = [{ searchValue: 'TOKEN', replaceValue: resultCreated.token }, { searchValue: 'NOME', replaceValue: resultCreated.nome, }];
                            this.templateWork.sendMail(resultCreated.email, TokenService.path, params, null, TokenService.email);
                            return this.getTokenResult("Sucesso.", true);
                        })
                        .catch(error => {
                            return this.getTokenResult("Ocorreu um erro: " + error, false);
                        });
                }

            })
            .catch(error => {
                return this.getTokenResult("Ocorreu um erro: " + error, false);
            });
    }

    private isTokenExpired(creationDate: Date): boolean {
        let dateNow = new Date();
        let diffMinutes: number = Math.ceil(((dateNow.getTime() - creationDate.getTime()) / 1000) / 60);
        return diffMinutes > 5;
    }

    async validateToken(tokenPass: string, cpf: string) {
        return this.findOne({ cpf })
            .then(result => {
                if (!result) {
                    return this.getTokenResult("CPF não encontrado.", false);
                }
                let token: Token = result.toJSON() as Token;
                if (token.token === tokenPass) {
                    if (this.isTokenExpired(token.dataModificacao)) {
                        return this.getTokenResult("Token expirado. Reenvie outro token e " +
                            "repita o processo em, no máximo, 5 minutos.", false);
                    }
                    return this.getTokenResult("Sucesso.", true);
                } else {
                    return this.getTokenResult("Token inválido.", false);
                }
            })
            .catch(error => {
                return this.getTokenResult("Ocorreu um erro: " + error, false);
            });
    }

    private generateToken(): string {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    constructor(
        @InjectModel(Token.modelName) private readonly _tokenModel: ModelType<Token>,
        private readonly _mapperService: MapperService,
        private readonly templateWork: TemplateWork,
    ) {
        super();
        this._model = _tokenModel;
        this._mapper = _mapperService.mapper;
    }


}
