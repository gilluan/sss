import { Injectable } from '@nestjs/common';
import 'automapper-ts/dist/automapper';

@Injectable()
export class MapperService {
    mapper: AutoMapperJs.AutoMapper;

    constructor() {
        this.mapper = automapper;
        this.initializeMapper();
    }

    private initializeMapper(): void {
        this.mapper.initialize(MapperService.configure);
    }

    private static configure(config: AutoMapperJs.IConfiguration): void {
        config
            .createMap('Voluntario', 'VoluntarioVm')
            .forSourceMember('_id', opts => opts.ignored())
            .forSourceMember('__v', opts => opts.ignored())
            .forMember('horas', 0)
            .forMember('projetos', 0);
    }
}
