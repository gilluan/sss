import { HttpException, HttpStatus, Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService, MailSinsService, PagedDataModelVm, PageModelVm, TemplateWork, SequenceService } from '@sins/comum';
import { MongoError } from 'mongodb';
import { ModelType, InstanceType } from 'typegoose';
import { JustificativaAjusteService } from '../justificativa-ajuste/justificativa-ajuste.service';
import { SituacaoVoluntario } from './models/types/situacao-voluntario.type';
import { TotalSituacaoVm } from './models/view-models/total-situacao-vm.model';
import { VoluntarioVm } from './models/view-models/voluntario-vm.model';
import { Voluntario, VoluntarioModel } from './models/voluntario.model';
import { MapperService } from './mapper/mapper.service';
import { DadosTermo } from './models/dados-termo';

@Injectable()
export class VoluntarioService extends ComumService<Voluntario> {

    constructor(
        @InjectModel(Voluntario.modelName) private readonly _voluntarioModel: ModelType<Voluntario>,
        private readonly templateWork: TemplateWork,
        private readonly sequenceService: SequenceService,
        @Inject(forwardRef(() => JustificativaAjusteService))
        private readonly _justificativaService: JustificativaAjusteService,
        private readonly _mapperService: MapperService,
    ) {
        super();
        this._model = _voluntarioModel;
        this._mapper = _mapperService.mapper;
    }

    async updateMany(conditions, doc) {
        return await this._model.update(conditions, { $set: doc }).exec();
    }

    private validarCamposObrigatoriosRegister(voluntario: VoluntarioVm) {
        console.debug('Validar Campos', voluntario.voluntarioTipo);
        if (!voluntario.voluntarioTipo) {
            throw new HttpException('Tipo de Voluntário é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.nome) {
            throw new HttpException('Nome do Voluntario é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.rg) {
            throw new HttpException('RG é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.cpf) {
            throw new HttpException('CPF é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.dataNascimento) {
            throw new HttpException('Data de Nascimento é obrigatória', HttpStatus.CONFLICT);
        }
        if (!voluntario.nacionalidade) {
            throw new HttpException('Nacionalidade é obrigatória', HttpStatus.CONFLICT);
        }
        if (!voluntario.naturalidade) {
            throw new HttpException('Naturalidade é obrigatória', HttpStatus.CONFLICT);
        }
        if (!voluntario.profissao) {
            throw new HttpException('Profissão é obrigatória', HttpStatus.CONFLICT);
        }
        if (!voluntario.telefone) {
            throw new HttpException('Telefone é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.email) {
            throw new HttpException('E-mail é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.endCep) {
            throw new HttpException('CEP é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.endLogradouro) {
            throw new HttpException('Logradouro é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.endNumero) {
            throw new HttpException('Número é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.endBairro) {
            throw new HttpException('Bairro é obrigatório', HttpStatus.CONFLICT);
        }
        if (!voluntario.endCidade) {
            throw new HttpException('Cidade é obrigatória', HttpStatus.CONFLICT);
        }
        if (!voluntario.endUf) {
            throw new HttpException('UF é obrigatória', HttpStatus.CONFLICT);
        }
    }

    /* async  validarIdPerfil(idPerfil: string): Promise<boolean> {
        if (idPerfil) {
            return this._perfilService.findById(idPerfil)
                .then(result => {
                    return result != null;
                })
                .catch(error => {
                    throw new HttpException(error.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
                });
        } else {
            return true;
        }
    } */

    private getNewVoluntario(vm: VoluntarioVm): any {
        let newVoluntario = new VoluntarioModel();
        newVoluntario.id = vm.id;
        newVoluntario.voluntarioTipo = vm.voluntarioTipo;
        newVoluntario.idPerfil = vm.idPerfil;
        newVoluntario.codigoVoluntario = vm.codigoVoluntario;
        newVoluntario.nome = vm.nome;
        newVoluntario.rg = vm.rg;
        newVoluntario.cpf = vm.cpf;
        newVoluntario.genero = vm.genero;
        newVoluntario.dataNascimento = vm.dataNascimento;
        newVoluntario.nacionalidade = vm.nacionalidade;
        newVoluntario.naturalidade = vm.naturalidade;
        newVoluntario.profissao = vm.profissao;
        newVoluntario.telefone = vm.telefone;
        newVoluntario.email = vm.email;
        newVoluntario.endCep = vm.endCep;
        newVoluntario.endLogradouro = vm.endLogradouro;
        newVoluntario.endNumero = vm.endNumero;
        newVoluntario.endBairro = vm.endBairro;
        newVoluntario.endCidade = vm.endCidade;
        newVoluntario.endUf = vm.endUf;
        newVoluntario.idDocumentoCertificado = vm.idDocumentoCertificado;
        newVoluntario.situacao = SituacaoVoluntario[vm.situacao];
        newVoluntario.numeroCooperativa = vm.numeroCooperativa;
        newVoluntario.dadosTermo = this.criarDadosTermoNovoVoluntario();
        return newVoluntario;
    }

    async findBySituacaoAndUpdate(situacao: SituacaoVoluntario) {
        let voluntariosAssinados = await this._voluntarioModel.find({ situacao: SituacaoVoluntario.assinado });
        voluntariosAssinados.forEach((v: InstanceType<Voluntario>) => this.enviarNotificacao(v, situacao));
        return await this._model.updateMany({ situacao: SituacaoVoluntario.assinado }, { $set: { situacao } }, { multi: true });
    }

    private criarDadosTermoNovoVoluntario(): DadosTermo {
        let dadosTermo = new DadosTermo();
        dadosTermo.dataAssinaturaVoluntario = new Date();

        return dadosTermo;
    }

    async registerVoluntario(vm: VoluntarioVm) {
        //console.log(vm);
        this.validarCamposObrigatoriosRegister(vm);
        const newVoluntario = this.getNewVoluntario(vm);
        return this.create(newVoluntario)
            .then(result => { return (result.toJSON() as Voluntario) })
            .catch(error => {
                if (error instanceof MongoError && error.code === 11000) /*FIXME*/ {
                    console.info(error.message);
                    throw new HttpException('Voluntário já existente.', HttpStatus.INTERNAL_SERVER_ERROR);
                } else {
                    //console.log('error: ' + error);
                    throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            });
    }

    async updateVoluntario(vm: VoluntarioVm) {
        this.validarCamposObrigatoriosRegister(vm);
        let voluntario = await this._voluntarioModel.findById(vm.id);
        await this.enviarNotificacao(voluntario, vm.situacao);
        return this.updateById(vm.id, vm)
            .then(result => (result as Voluntario))
            .catch(error => {
                throw new HttpException('Erro:' + error, HttpStatus.INTERNAL_SERVER_ERROR);

            });
    }
    async enviarNotificacao(voluntario: InstanceType<Voluntario>, situacaoAtual: string, justificativa?: string) {
        if (voluntario.situacao != situacaoAtual) { // Mudança de situacao
            let assunto: string = null;
            let path: string = '';
            let params;
            let defaultMail: string;
            switch (situacaoAtual) {
                case SituacaoVoluntario.devolvidoAjuste:
                    break;
                case SituacaoVoluntario.aguardandoAnalise:
                    break;
                case SituacaoVoluntario.aguardandoAssinatura:
                    assunto = 'Enviado para autorização!';
                    path = './src/resources/enviado-assinatura.html';
                    break;
                case SituacaoVoluntario.ajusteVoluntario:
                    assunto = 'Análise dos dados e Certificado de Voluntário.';
                    if (!justificativa) justificativa = await this._justificativaService.findJustificativaByIdVoluntario({ idVoluntario: voluntario.id })[0].justificativa;
                    path = './src/resources/revise-dados.html';
                    params = [{ searchValue: 'JUSTIFICATIVA', replaceValue: justificativa }]
                    break;
                case SituacaoVoluntario.assinado:
                    assunto = "Parabéns! Seja bem-vindo ao time de voluntários do Instituto Sicoob.";
                    path = './src/resources/bem-vindo.html';
                    params = [{ searchValue: 'VOLUNTARIO', replaceValue: voluntario.nome, }]
                    break;
                case SituacaoVoluntario.aguardandoAssinaturaTermo:
                    assunto = "Mudanças no termo de compromisso";
                    params = [{ searchValue: 'VOLUNTARIO', replaceValue: voluntario.nome, }]
                    path = './src/resources/alteracao-termo.html';
                    break;
            }
            if (assunto && voluntario.email) {
                this.logger.error(`Notificando o usuario ${voluntario.email}`, assunto);
                this.templateWork.sendMail(voluntario.email, path, params, assunto, defaultMail);
            } else {
                this.logger.log(`Usuário não notificado: situacao "${situacaoAtual}"`);
            }
        }
    }

    async concordarTermo(voluntario: VoluntarioVm): Promise<Voluntario> {
        let voluntarioModel = null;
        if (voluntario.id) {
            voluntarioModel = await this._voluntarioModel.findById(voluntario.id).exec();
        }
        if ((voluntarioModel && voluntarioModel.situacao != SituacaoVoluntario.aguardandoAssinaturaTermo) || !voluntarioModel) {
            voluntario['situacao'] = SituacaoVoluntario.aguardandoAnalise;
            voluntario['dadosTermo'] = {
                dataAssinaturaVoluntario: new Date()
            };
        } else {
            voluntario['situacao'] = SituacaoVoluntario.assinado;
            if (voluntarioModel.dadosTermo) {
                voluntario['dadosTermo'] = new DadosTermo(voluntarioModel.dadosTermo.gestorInstituto, voluntarioModel.dadosTermo.dataAssinaturaGestorInstituto, new Date());
            }
        }
        return this._voluntarioModel.findByIdAndUpdate(voluntario.id, voluntario, { new: false })
            .then((vol) => (vol.toJSON() as Voluntario));
    }

    async findVoluntario(id: string) {
        return super.findById(id)
            .then(result => (result.toJSON() as Voluntario))
            .catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            });

    }

    async findVoluntarioByCpf(cpf: string) {
        return super.findOne({ cpf: cpf })
            .then(result => {
                if (result != null && result != undefined) {
                    return (result.toJSON() as Voluntario);
                }
            })
            .catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    async desativarVoluntario(id: string): Promise<Voluntario> {
        return this.alterarStatusVoluntario(id, false);
    }

    async ativarVoluntario(id: string): Promise<Voluntario> {
        return this.alterarStatusVoluntario(id, true);
    }

    private alterarStatusVoluntario(id: string, status: boolean): Promise<Voluntario> {
        let update = { ativo: status, dataModificacao: new Date() };
        return this._voluntarioModel.findByIdAndUpdate(id, update, { new: true })
            .then((vol) => (vol.toJSON() as Voluntario));
    }

    async assinarContrato(id: string, idGestorInstituto: string): Promise<Voluntario> {
        this.enviarNotificacao(await this._voluntarioModel.findOne({ "_id": id }), SituacaoVoluntario.assinado);
        let dadosTermo = await this._voluntarioModel.findOne({ "_id": id }).select({ "dadosTermo": 1, "_id": 0 });
        let update = {
            situacao: SituacaoVoluntario.assinado,
            dataModificacao: new Date(),
            codigoVoluntario: await this.sequenceService.nextSequence(Voluntario.sequenceName),
            dadosTermo: {
                dataAssinaturaVoluntario: dadosTermo.dadosTermo.dataAssinaturaVoluntario,
                dataAssinaturaGestorInstituto: new Date(),
                gestorInstituto: this.toObjectId(idGestorInstituto)
            }
        };
        return this._voluntarioModel.findByIdAndUpdate(id, update, { new: true })
            .then((vol) => (vol.toJSON() as Voluntario));
    }

    async mudarSituacao(id: string, situacao: string): Promise<Voluntario> {
        let voluntario = await this._voluntarioModel.findById(id).exec();
        if (voluntario) {
            let voluntarioVm = new VoluntarioVm();
            voluntarioVm.situacao = situacao as SituacaoVoluntario;
            await this.enviarNotificacao(voluntario, voluntarioVm.situacao)
        }
        return this._voluntarioModel.findOneAndUpdate({ _id: id }, { $set: { situacao } }, { new: true, upsert: false })
            .lean()
            .then((result) => {
                result.id = result._id;
                return Object.assign(new Voluntario(), result)
            });
    }

    async pesquisarPorFiltro(params): Promise<PagedDataModelVm | any> {
        let filter = this.buildFilter(params);
        if (params.campos || params.campo) {
            return this.recuperarPorCampos(filter, params.campos, params.campo);
        }
        return this.recuperarPorFiltroPaginado({ $and: filter }, parseInt(params['offset']), parseInt(params['limit']));
    }

    private async recuperarPorCampos(filtro, campos, campo): Promise<any> {
        let pipelineOperators: any[] = [{ $match: { $and: filtro } }];
        if (campo) {
            pipelineOperators.push({ $group: this.buildGroup(campo, campo) });
            pipelineOperators.push({ $project: this.buildFields(campo) });
            return this._voluntarioModel.aggregate(pipelineOperators)
                .then(projection => projection.map(i => i[campo]));
        } else {
            pipelineOperators.push({ $project: this.buildFields(campos) });
            return this._voluntarioModel.aggregate(pipelineOperators);
        }
    }

    async recuperarTotalSituacao(params): Promise<TotalSituacaoVm[]> {
        let filter = this.buildFilter(params, true);
        return this._voluntarioModel.aggregate([
            { $match: { $and: filter } },
            { $group: { _id: '$situacao', situacao: { $first: '$situacao' }, total: { $sum: 1 } } },
            { $project: { _id: 0, situacao: 1, total: 1 } }
        ]).exec().catch(error => {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        });
    }

    async recuperarNomeVoluntarioById(id: string) {
        return this._model.findOne({ '_id': id }).select({ 'nome': 1, '_id': 0 }).exec().then(
            (vol) => vol)
            .catch((erro: any) => { throw new HttpException(`Aconteceu um erro tentando buscar o usuário ${id}`, HttpStatus.BAD_REQUEST) }
            )
    }

    async recuperarCampo(campo: string, query: any): Promise<any> {
        if (campo == null) {
            throw new HttpException('Recuperação de campo deve conter o nome do campo', HttpStatus.BAD_REQUEST);
        }
        let filter = this.buildFilter(query);
        return this._voluntarioModel.distinct(campo, { $and: filter }).exec()
            .then((cooperativas) => cooperativas)
            .catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            })
    }

    async totalPorFiltro(params: any): Promise<number> {
        let filter = this.buildFilter(params);
        return this.recuperarTotalPorFiltro({ $and: filter })
            .then(total => total)
            .catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    async removerDataAssinaturaTermoVoluntarios() {
        let voluntarios = await this.findAll();
        voluntarios.forEach(async voluntario => {
            let update = {
                situacao: SituacaoVoluntario.aguardandoAssinaturaTermo,
                dataModificacao: new Date(),
                dadosTermo: {
                    dataAssinaturaGestorInstituto: voluntario.dadosTermo.gestorInstituto,
                    gestorInstituto: voluntario.dadosTermo.gestorInstituto,
                }
            };
            this.updateById(voluntario.id, update);
        });
    }

    private async recuperarPorFiltroPaginado(filtro, offset: number, limit: number): Promise<PagedDataModelVm> {
        if (isNaN(offset) || isNaN(limit)) {
            throw new HttpException(`Dados de paginação 'offset' e 'limit' são obrigatórios`, HttpStatus.BAD_REQUEST);
        }

        return this.recuperarTotalPorFiltro(filtro).then(total => {
            if (total > 0) {
                return this.recuperarPorFiltro(filtro, offset, limit)
                    .then(result => result.map(voluntario => voluntario))
                    .then(data => new PagedDataModelVm(new PageModelVm(offset, limit, total), data));
            } else {
                return new PagedDataModelVm(new PageModelVm(offset, limit, 0), [])
            }
        }).catch(error => {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        });
    }

    private async recuperarTotalPorFiltro(filter): Promise<number> {
        return this._voluntarioModel.countDocuments(filter);
    }

    private async recuperarPorFiltro(filter, offset: number, limit: number): Promise<Voluntario[]> {
        return await this._model.aggregate([
            { $match: filter },
            { $lookup: { from: 'horas-voluntario', localField: '_id', foreignField: 'voluntario', as: 'horas' } },
            { $unwind: { path: "$horas", preserveNullAndEmptyArrays: true } },
            {
                $group: {
                    _id: '$_id',
                    id: { $first: "$_id" },
                    voluntarioTipo: { $first: "$voluntarioTipo" },
                    dadosTermo: { $first: "$dadosTermo" },
                    idPerfil: { $first: "$idPerfil" },
                    codigoVoluntario: { $first: "$codigoVoluntario" },
                    ativo: { $first: "$ativo" },
                    dataCriacao: { $first: "$dataCriacao" },
                    dataModificacao: { $first: "$dataModificacao" },
                    nome: { $first: "$nome" },
                    rg: { $first: "$rg" },
                    cpf: { $first: "$cpf" },
                    genero: { $first: "$genero" },
                    dataNascimento: { $first: "$dataNascimento" },
                    nacionalidade: { $first: "$nacionalidade" },
                    naturalidade: { $first: "$naturalidade" },
                    profissao: { $first: "$profissao" },
                    telefone: { $first: "$telefone" },
                    email: { $first: "$email" },
                    endCep: { $first: "$endCep" },
                    game: { $first: "$game" },
                    endLogradouro: { $first: "$endLogradouro" },
                    endNumero: { $first: "$endNumero" },
                    endBairro: { $first: "$endBairro" },
                    endCidade: { $first: "$endCidade" },
                    endUf: { $first: "$endUf" },
                    idDocumentoCertificado: { $first: "$idDocumentoCertificado" },
                    situacao: { $first: "$situacao" },
                    numeroCooperativa: { $first: "$numeroCooperativa" },
                    horas: { $sum: '$horas.horas' }
                }
            }]).skip(offset * limit).limit(limit).sort({ _id: -1 }).exec();
    }



    private mapTo(voluntario: Voluntario): VoluntarioVm {
        return this._mapper.map('Voluntario', 'VoluntarioVm', voluntario);
    }

    private buildFilter(params, totais: boolean = false): any[] {
        let filter = [];
        filter.push({ situacao: { $exists: true, $ne: null } })
        if (params.nome) {
            filter.push({ nome: new RegExp(params['nome'], 'i') });
        }
        if (!totais && params.situacao) {
            filter.push({ situacao: params['situacao'] });
        }
        if (params.cooperativas) {
            filter.push({ numeroCooperativa: { $in: params.cooperativas.split(',') } });
        }
        if (params.ativo) {
            filter.push({ ativo: params.ativo == 'true' });
        }
        if (params.profissao) {
            filter.push({ profissao: params['profissao'] });
        }
        if (params.cpf) {
            filter.push({ cpf: params['cpf'] });
        }
        return filter;
    }

    private buildFields(fields: string): any {
        const possuiId: boolean = fields.indexOf("_id") > -1;
        let campos: string = fields.split(',').map(campo => `\"${campo}\": 1`).join(',');
        campos = !possuiId ? "\"_id\": 0," + campos : campos;
        return JSON.parse(`{${campos}}`);
    }

    private buildGroup(id, fields): any {
        let campos: string = fields.split(',').map(campo => `\"${campo}\": { \"$first\": \"$${campo}\" }`).join(',');
        campos = `\"_id\": \"$${id}\", ${campos}`;
        return JSON.parse(`{${campos}}`);
    }

}
