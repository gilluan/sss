import { Body, Controller, Get, Param, Patch, Post, Put, Query, Req } from '@nestjs/common';
import { PagedDataModelVm } from '@sins/comum';
import { TotalSituacaoVm } from './models/view-models/total-situacao-vm.model';
import { VoluntarioVm } from './models/view-models/voluntario-vm.model';
import { VoluntarioService } from './voluntario.service';
import { Voluntario } from './models/voluntario.model';
import { Request } from 'express';

@Controller('voluntarios')
export class VoluntarioController {

    constructor(private readonly service: VoluntarioService) { }

    private mapTo<T>(object: any): Promise<T> {
        return this.service.map<T>(object);
    }

    @Post()
    async register(@Body() vm: VoluntarioVm): Promise<VoluntarioVm> {
        return this.service.registerVoluntario(vm).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Put()
    async update(@Body() vm: VoluntarioVm): Promise<VoluntarioVm> {
        return this.service.updateVoluntario(vm).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Get()
    async recuperarPorFiltro(@Query() params): Promise<PagedDataModelVm | any> {
        return this.service.pesquisarPorFiltro(params);
    }
    
    @Patch(':identificador/assinar')
    async assinarContrato(@Req() request: Request): Promise<VoluntarioVm> {
        return this.service.assinarContrato(request.params["identificador"], request.query["identificadorGestorInstituto"] ).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Patch('/concordar-termo')
    async concordarTermo(@Body() voluntario): Promise<Voluntario> {
        return this.service.concordarTermo(voluntario).then(voluntario => this.mapTo<Voluntario>(voluntario));
    }

    @Patch(':identificador/situacao/:situacao')
    async mudarStatus(@Param('identificador') id, @Param('situacao') situacao): Promise<VoluntarioVm> {
        return this.service.mudarSituacao(id, situacao).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Get('cpf/:cpf')
    async findVoluntarioByCpf(@Param('cpf') cpf): Promise<VoluntarioVm> {
        return this.service.findVoluntarioByCpf(cpf).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Get('situacao/total')
    async recuperarTotalPorSituacao(@Query() params): Promise<TotalSituacaoVm[]> {
        return this.service.recuperarTotalSituacao(params);
    }

    @Patch(':identificador/desativar')
    async desativar(@Param('identificador') id): Promise<VoluntarioVm> {
        return this.service.desativarVoluntario(id).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Patch(':identificador/ativar')
    async ativar(@Param('identificador') id): Promise<VoluntarioVm> {
        return this.service.ativarVoluntario(id).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }

    @Get('campo/:campo')
    async recuperarCampo(@Param('campo') campo, @Query() params): Promise<any> {
        return this.service.recuperarCampo(campo, params);
    }

    @Get('total')
    async total(@Query() params): Promise<number> {
        return this.service.totalPorFiltro(params);
    }

    @Get(':identificador')
    async findById(@Param('identificador') id): Promise<VoluntarioVm> {
        return this.service.findVoluntario(id).then(voluntario => this.mapTo<VoluntarioVm>(voluntario));
    }
}
