import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Voluntario } from './models/voluntario.model';
import { VoluntarioService } from './voluntario.service';
import { VoluntarioController } from './voluntario.controller';
import { MapperService } from './mapper/mapper.service';
import { MailSinsService, TemplateWork, SequenceModule } from '@sins/comum';
import { JustificativaAjusteModule } from '../justificativa-ajuste/justificativa-ajuste.module';
import { JustificativaAjusteService } from '../justificativa-ajuste/justificativa-ajuste.service';
import { UsuarioInstitutoModule } from '../usuario-instituto/usuario-instituto.module';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';

@Module({
    imports: [
        SequenceModule,
        forwardRef(() => JustificativaAjusteModule),
        UsuarioInstitutoModule,
        MongooseModule.forFeature([{ name: Voluntario.modelName, schema: Voluntario.model.schema }])
    ],
    providers: [MapperService, VoluntarioService, MailSinsService, JustificativaAjusteService, UsuarioInstitutoService, TemplateWork],
    controllers: [VoluntarioController],
    exports: [VoluntarioService, MapperService],
})
export class VoluntarioModule {
}