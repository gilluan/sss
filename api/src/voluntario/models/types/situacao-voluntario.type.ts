export enum SituacaoVoluntario {
    aguardandoAnalise = 'aguardandoAnalise',
    devolvidoAjuste = 'devolvidoAjuste',
    aguardandoAssinatura = 'aguardandoAssinatura',
    ajusteVoluntario = 'ajusteVoluntario',
    assinado = 'assinado',
    aguardandoAssinaturaTermo = 'aguardandoAssinaturaTermo'
}