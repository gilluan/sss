import {ModelType, pre, prop, Typegoose} from 'typegoose';
import {schemaOptions} from '@sins/comum';
import {VoluntarioTipo} from "./voluntario-tipo.enum";
import {SituacaoVoluntario} from "./types/situacao-voluntario.type";
import { DadosTermo } from './dados-termo';
import { Types } from 'mongoose';

@pre<Voluntario>('findOneAndUpdate', function(next) {
    if(this._update['situacao'] == SituacaoVoluntario.assinado) {
        next();
    } else {
        this._update.dataModificacao = new Date(Date.now());
        next();
    }
})
export class Voluntario extends Typegoose {
    
    //@prop({}) id?: Types.ObjectId;

    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao?: Date;

    @prop({
        required: [true, 'Tipo de Voluntário é obrigatório'], 
        enum: VoluntarioTipo, 
        default: VoluntarioTipo.Sicoob 
    })
    voluntarioTipo: VoluntarioTipo;

    @prop({ default: null })
    dadosTermo: DadosTermo;

    @prop({ default: null })
    idPerfil: string;

    @prop({ default: null, unique: true })
    codigoVoluntario: number;

    @prop({ required: [ true, 'Nome do Voluntario é obrigatório' ] })
    nome: string;

    @prop({ enum: SituacaoVoluntario })
    situacao: SituacaoVoluntario;

    @prop({ required: [ true, 'RG é obrigatório' ] })
    rg: string;

    @prop({ required: [ true, 'CPF é obrigatório' ] })
    cpf: string;

    @prop({ required: [true, 'Genero é obrigatório']})
    genero: string;

    @prop({ required: [ true, 'Data de Nascimento é obrigatória' ] })
    dataNascimento: Date;

    @prop({ required: [ true, 'Nacionalidade é obrigatória' ] })
    nacionalidade: string;

    @prop({ required: [ true, 'Naturalidade é obrigatória' ] })
    naturalidade: string;

    @prop({ required: [ true, 'Profissão é obrigatória' ] })
    profissao: string;

    @prop({ required: [ true, 'Telefone é obrigatório' ] })
    telefone: string;

    @prop({ required: [ true, 'E-mail é obrigatório' ] })
    email: string;

    @prop({ required: [ true, 'CEP é obrigatório' ] })
    endCep: string;

    @prop({ required: [ true, 'Logradouro é obrigatório' ] })
    endLogradouro: string;

    @prop({ required: [ true, 'Número é obrigatório' ] })
    endNumero: string;

    @prop({ required: [ true, 'Bairro é obrigatório' ] })
    endBairro: string;

    @prop({ required: [ true, 'Cidade é obrigatória' ] })
    endCidade: string;

    @prop({ required: [ true, 'UF é obrigatória' ] })
    endUf: string;

    @prop()
    idDocumentoCertificado: string;

    @prop({ required: [ true, 'Número da cooperativa é obrigatório' ] })
    numeroCooperativa: string;

    @prop({ default: true })
    ativo: boolean;

    static get model(): ModelType<Voluntario> {
        return new Voluntario().getModelForClass(Voluntario, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }

    static get sequenceName(): string {
        return 'voluntario_seq'
    }
}

export const VoluntarioModel = new Voluntario().getModelForClass(Voluntario, { schemaOptions });
