import { Types } from "mongoose";

export class DadosTermo {

    constructor(public gestorInstituto?: Types.ObjectId,
        public dataAssinaturaGestorInstituto?: Date,
        public dataAssinaturaVoluntario?: Date) { }

}