import {ComumModelVm} from '@sins/comum';
import {VoluntarioTipo} from "../voluntario-tipo.enum";

export class VoluntarioVm extends ComumModelVm {
    voluntarioTipo: VoluntarioTipo;
    idPerfil: string;
    codigoVoluntario: number;
    nome: string;
    rg: string;
    cpf: string;
    genero: string;
    dataNascimento: Date;
    nacionalidade: string;
    naturalidade: string;
    profissao: string;
    telefone: string;
    email: string;
    endCep: string;
    endLogradouro: string;
    endNumero: string;
    endBairro: string;
    endCidade: string;
    endUf: string;
    idDocumentoCertificado: string;
    situacao: string;
    horas: number = 0;
    projetos: number = 0;
    numeroCooperativa: string;
    ativo: boolean = true;
}
