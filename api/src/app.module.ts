import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigurationService, Configuration } from '@sins/comum';
import { ComumModule } from '@sins/comum';
import { MongooseModule } from '@nestjs/mongoose';
import { VoluntarioModule } from './voluntario/voluntario.module';
import { TermoCompromissoModule } from './termo-compromisso/termo-compromisso.module';
import { JustificativaAjusteModule } from './justificativa-ajuste/justificativa-ajuste.module';

import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import { MailSinsService } from '@sins/comum';
import { TokenModule } from './token/token.module';
import { UsuarioInstitutoModule } from './usuario-instituto/usuario-instituto.module';
import { PlanoAcaoModule } from './plano-acao/plano-acao.module';
import { APP_GUARD, APP_FILTER } from '@nestjs/core';
import { AuthModule } from '@sins/comum';
import { HttpExceptionFilter } from '@sins/comum';
import { CategoriaCursoModule } from './categoria-curso/categoria-curso.module';
import { CursoModule } from './curso/curso.module';
import { HorasVoluntarioModule } from './horas-voluntario/horas-voluntario.module';



@Module({
  imports: [
    ComumModule,
    MongooseModule.forRoot(ConfigurationService.connectionString, {
      retryDelay: 500,
      retryAttempts: 3,
      useNewUrlParser: true,
    }),
    HorasVoluntarioModule,
    VoluntarioModule,
    TermoCompromissoModule,
    JustificativaAjusteModule,
    UsuarioInstitutoModule,
    PlanoAcaoModule,
    TokenModule,
    AuthModule,
    MailerModule.forRoot({
      transport: {
        host: ConfigurationService.smtpServidor,
        port: ConfigurationService.smtpPorta,
        ignoreTLS: true
      },
      defaults: {
        from: '"SINS" <dev@sicoob.com.br>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new HandlebarsAdapter(),
        options: {
          debug: "true",
          doctype: 'html',
        },
      },
    }), 
    CategoriaCursoModule,
    CursoModule
  ],
  controllers: [AppController],
  providers: [AppService, MailSinsService,{provide: APP_FILTER, useClass: HttpExceptionFilter}],
})
export class AppModule {
  static host: string;
  static port: number | string;
  static isDev: boolean;

  constructor(private readonly _configurationService: ConfigurationService) {
    AppModule.port = AppModule.normalizePort("3000");
    AppModule.host = _configurationService.get(Configuration.HOSTNAME);
    AppModule.isDev = _configurationService.isDevelopment;
  }

  private static normalizePort(param: number | string): number | string {
    const portNumber: number = typeof param === 'string' ? parseInt(param, 10) : param;
    if (isNaN(portNumber)) return param;
    else if (portNumber >= 0) return portNumber;
  }

}
