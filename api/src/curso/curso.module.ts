import { MongooseModule } from "@nestjs/mongoose";
import { Curso } from "./models/curso.model";
import { CursoService } from "./curso.service";
import { CursoController } from "./curso.controller";
import { Module } from "@nestjs/common";
import { VoluntarioModule } from "../voluntario/voluntario.module";

@Module({
    imports: [
        VoluntarioModule,
        MongooseModule.forFeature([{ name: Curso.modelName, schema: Curso.model.schema }])
    ],
    providers: [CursoService],
    controllers: [CursoController],
    exports: [CursoService],
})
export class CursoModule{}
