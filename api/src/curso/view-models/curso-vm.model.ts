import { ComumModelVm } from "@sins/comum";

export class CursoVm extends ComumModelVm {
    nome: string;
    dataConclusao: Date;
    descricao?: string;
    quantidadeHoras: number;
    identificadorCertificadoGED: string;
    categoriaCurso: any;
    voluntario: any;
    usuarioInstituto: any;
}