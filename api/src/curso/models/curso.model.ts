import { Typegoose, prop, ModelType, pre } from "typegoose";
import { SchemaOptions, Types } from "mongoose";

@pre<Curso>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class Curso extends Typegoose {
    
    @prop({ required: [ true, 'Nome do Curso é obrigatório' ] })
    nome: string;

    @prop()
    dataConclusao: Date;

    @prop()
    descricao?: string;

    @prop({required: [true, 'Id do Documento do Certificado no GED obrigatório']})
    identificadorCertificadoGED: string;

    @prop()
    categoriaCurso: Types.ObjectId;

    @prop()
    voluntario: Types.ObjectId;

    @prop()
    usuarioInstituto: Types.ObjectId;

    @prop({required: [true, 'Quantidade de Horas é obrigatório']})
    quantidadeHoras: number;

    @prop({ default: Date.now, index: true })
    dataCriacao: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao: Date;
    
    static get model(): ModelType<Curso> {
        return new Curso().getModelForClass(Curso, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    collection: 'curso'
};

export const CursoModel = new Curso().getModelForClass(Curso, { schemaOptions });