import { Controller, Param, Get, Post, Body, Put, Delete, Query, HttpException, HttpStatus } from "@nestjs/common";
import { CursoService } from "./curso.service";
import { CursoVm } from "./view-models/curso-vm.model";

@Controller('cursos')
export class CursoController {
    
    constructor(private _service: CursoService) {}

    @Post()
    cadastrarCursoVoluntario(@Body() cursoVm: CursoVm) {
        return this._service.cadastrar(cursoVm).catch(error => {throw new HttpException(error.message, HttpStatus.BAD_GATEWAY)});
    }

    @Get()
    listarPorVoluntario(@Query() params)  {
        return this._service.listarPorVoluntario(params);
    }

    @Get('total')
    totalCursosPorVoluntario(@Query() params) {
        return this._service.totalCursosPorVoluntario(params);
    }

    @Put()
    atualizarCurso(@Body() curso: CursoVm) {
        return this._service.atualizarCurso(curso);
    }

    @Get(':id')
    detalhaCurso(@Param() id: string) {
        return this._service.findById(id);
    }

    @Delete(':identificador')
    deletarCurso(@Param('identificador') identificador: string) {
        return this._service.delete(identificador);
    }

}