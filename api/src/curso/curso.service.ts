import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { ComumService, PagedDataModelVm, PageModelVm, AppLogger } from "@sins/comum";
import { InstanceType, ModelType } from "typegoose";
import { Curso, CursoModel } from './models/curso.model';
import { CursoVm } from './view-models/curso-vm.model';
import { ObjectId } from "bson";

@Injectable()
export class CursoService extends ComumService<Curso>{

    constructor(@InjectModel(Curso.modelName) public readonly _cursoModel: ModelType<Curso>) {
        super();
        this._model = _cursoModel;
    }

    logger: AppLogger = new AppLogger();

    async atualizarCurso(curso: CursoVm) {
        return await this._model.updateOne({_id: new ObjectId(curso.id)}, this.getCursoModel(curso)); 
    }

    public async cadastrar(cursoVm: CursoVm) {
        let cursoNew: InstanceType<Curso>;
        try {
            cursoNew = await this._model.create(this.getCursoModel(cursoVm));
        } catch(error) {
            this.logger.error("Erro cadastrando curso: ", error);
            throw new HttpException(error, HttpStatus.BAD_REQUEST);
        }
        return {resultado: cursoNew};
    }

    public async listarPorVoluntario(params) {
        let filter = this.buildFilter(params);
        if (isNaN(params['offset']) || isNaN(params['limit'])) {
            throw new HttpException(`Dados de paginação 'offset' e 'limit' são obrigatórios`, HttpStatus.BAD_REQUEST);
        }
        const offset = parseInt(params['offset']);
        const limit = parseInt(params['limit']);
        const total = await this.totalCursosPorVoluntario(params);
        let result = await this._model.aggregate([
            {
                $match: {
                    $and: filter
                }
            },{
                $lookup: {
                    from: 'categoria-curso',
                    localField: 'categoriaCurso',
                    foreignField: '_id',
                    as: 'categoriaCurso'
                }
            },
            {
                $unwind: {
                    path: "$categoriaCurso",
                    preserveNullAndEmptyArrays: false
                }
            }, { $addFields: { id: "$_id" } }
        ]).skip(offset * limit).limit(limit).exec();
        return {resultado: new PagedDataModelVm(new PageModelVm(offset, limit, total), result)};
    }

    public async totalCursosPorVoluntario(params: any) {
        let filter = this.buildFilter(params);
        if(filter.length > 0)
            return await this._model.countDocuments({$and: filter}).exec();
        else
            return await this._model.countDocuments({}).exec();
    }

    private buildFilter(params) {
        let filter = [];
        if(params.identificadorVoluntario) {
            filter.push({voluntario: this.toObjectId(params.identificadorVoluntario)});
        }
        if(params.identificadorUsuarioInstituto) {
            filter.push({usuarioInstituto: this.toObjectId(params.identificadorUsuarioInstituto)});
        }
        if(params.nome) {
            filter.push({ nome: new RegExp(params['nome'], 'i') });
        }
        if(params.categoriaCurso) {
            filter.push({ categoriaCurso: new RegExp(params['categoriaCurso'], 'i')});
        }
        return filter;
    }


    private getCursoModel(cursoVm: CursoVm) {
        let cursoModel = new CursoModel();
        cursoModel._id = cursoVm.id;
        cursoModel.dataConclusao = cursoVm.dataConclusao;
        cursoModel.nome = cursoVm.nome;
        cursoModel.identificadorCertificadoGED = cursoVm.identificadorCertificadoGED;
        cursoModel.descricao = cursoVm.descricao;
        if( cursoVm.usuarioInstituto ) {
            if( typeof cursoVm.usuarioInstituto === 'string') {
                cursoModel.usuarioInstituto = this.toObjectId(cursoVm.usuarioInstituto);
            } else {
                cursoModel.usuarioInstituto = this.toObjectId(cursoVm.usuarioInstituto._id);
            }
        }
        if(cursoVm.categoriaCurso) {
            if( typeof cursoVm.categoriaCurso === 'string') {
                cursoModel.categoriaCurso = this.toObjectId(cursoVm.categoriaCurso);
            }else {
                cursoModel.categoriaCurso = this.toObjectId(cursoVm.categoriaCurso._id);
            }
        }
        if(cursoVm.voluntario) {
            if( typeof cursoVm.voluntario === 'string') {
                cursoModel.voluntario = this.toObjectId(cursoVm.voluntario);
            }else {
                cursoModel.voluntario = this.toObjectId(cursoVm.voluntario._id);
            }
        }
        cursoModel.quantidadeHoras = cursoVm.quantidadeHoras
        return cursoModel;
    }
    
    
}