import { Module } from '@nestjs/common';
import { PlanoAcaoService } from './plano-acao.service'
import { PlanoAcaoController } from './plano-acao.controller'
import { MongooseModule } from '@nestjs/mongoose';
import { PlanoAcao } from './models/plano-acao.model';
import { Acao } from './models/acao.model';
import { UsuarioInstitutoModule } from '../usuario-instituto/usuario-instituto.module';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';
import { AuthModule } from '@sins/comum';

@Module({
  imports: [ AuthModule, MongooseModule.forFeature([{ name: PlanoAcao.modelName, schema: PlanoAcao.model.schema }, { name: Acao.modelName, schema: Acao.model.schema }]), UsuarioInstitutoModule],
  providers: [PlanoAcaoService, UsuarioInstitutoService],
  controllers: [PlanoAcaoController]
})
export class PlanoAcaoModule {}
