import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService, PagedDataModelVm, PageModelVm } from '@sins/comum';
import { ObjectId } from 'bson';
import { Types } from 'mongoose';
import { InstanceType, ModelType } from 'typegoose';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';
import { TotalSituacaoVm } from '../voluntario/models/view-models/total-situacao-vm.model';
import { Acao, AcaoModel } from './models/acao.model';
import { PlanoAcao, PlanoAcaoModel } from './models/plano-acao.model';
import { SituacaoAcaoType } from './models/situacao-acao.type';
import { AcaoVm } from './models/view-models/acao-vm.model';
import { PlanoAcaoVm } from './models/view-models/plano-acao-vm.model';


@Injectable()
export class PlanoAcaoService extends ComumService<PlanoAcao> {

    constructor(@InjectModel(PlanoAcao.modelName) public readonly _planoAcaoModel: ModelType<PlanoAcao>, @InjectModel(Acao.modelName) public readonly _acaoModel: ModelType<Acao>, private readonly _usuarioInstitutoService: UsuarioInstitutoService) {
        super();
        this._model = _planoAcaoModel;
    }

    // Funções do Plano de Ação
    async createPlanoAcao(vm: PlanoAcaoVm) {
        await this.verificarPlanoAcao(vm.vigencia, vm.numeroCooperativa)
        vm.situacao = SituacaoAcaoType.planejando;
        return await this._planoAcaoModel.create(this.getPlanoAcao(vm));
    }

    async updatePlanoAcao(vm: PlanoAcaoVm) {
        await this.verificarPlanoAcao(vm.vigencia, vm.numeroCooperativa, vm.id);
        let planoAcao = await this._planoAcaoModel.findById(vm.id);
        if (planoAcao) {
            let modified = await this._planoAcaoModel.updateOne({ _id: vm.id }, this.getPlanoAcao(vm));
            if (modified.nModified > 0) {
                return vm;
            } else {
                throw new HttpException("Não foi possível atualizar o plano de ação", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    private async verificarPlanoAcao(vigencia: string, numeroCooperativa: string, id?: string) {
        const filterBase = { vigencia: vigencia, numeroCooperativa: numeroCooperativa };
        const filter = id ? { ...filterBase, _id: { $ne: this.toObjectId(id) } } : filterBase;
        const exists = await this.findAll(filter);
        if (exists && exists.length > 0) {
            throw new HttpException(`Existe outro plano de ação de vigência ${vigencia} na cooperativa ${numeroCooperativa}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async excluirPlanoAcao(id: string) {
        if (id) {
            let planoAcao = await this.findPlanoAcao(id, null, null, null, true);
            if (planoAcao && planoAcao.resultado.data && planoAcao.resultado.data[0] && planoAcao.resultado.data[0].acoes) {
                let acoes: AcaoVm[] = <AcaoVm[]>planoAcao.resultado.data[0].acoes.resultado.data;
                let acoesEx = acoes.filter(a => a.situacao != SituacaoAcaoType.naoIniciada);
                if (acoesEx.length == 0) {
                    for (let i = 0; i < acoes.length; i++) {
                        let acao = acoes[i];
                        await this.excluirAcao(acao.id);
                    }
                    return await this._planoAcaoModel.deleteOne({ _id: id }).exec();
                } else throw new HttpException("O plano de ação não pode ser excluído, porque existem tarefa(s) iniciada(s).", HttpStatus.BAD_REQUEST);
            } else throw new HttpException("Plano de ação não encontrado", HttpStatus.BAD_REQUEST);
        } else throw new HttpException("O id é um campo obrigatório", HttpStatus.BAD_REQUEST);
    }

    async iniciarPlanoAcao(id: string) {
        let planoAcao = await this._planoAcaoModel.findById(id);
        if (planoAcao) {
            let tarefasPais = await this._acaoModel.find({ planoAcaoPai: new ObjectId(id) });
            if (!tarefasPais || !tarefasPais[0])
                throw new HttpException("O planejamento do plano de ação não pode ser finalizado sem tarefas cadastradas", HttpStatus.BAD_REQUEST);
            for (let i = 0; i < tarefasPais.length; i++) {
                const tarefa = tarefasPais[i];
                /* if (!tarefa.investimento || tarefa.investimento <= 0)
                    throw new HttpException("O planejamento do plano de ação não pode ser finalizado com tarefas sem investimento", HttpStatus.BAD_REQUEST); */
                if (!tarefa.dataFim)
                    throw new HttpException("O planejamento do plano de ação não pode ser finalizado com tarefas sem data de fim", HttpStatus.BAD_REQUEST);
                if (!tarefa.dataInicio)
                    throw new HttpException("O planejamento do plano de ação não pode ser finalizado com tarefas sem data de início", HttpStatus.BAD_REQUEST);
            }
            planoAcao.iniciado = true;
            planoAcao.situacao = await this.calcularSituacaoPlanoAcao(this.toObjectId(id));
            try {
                await this.updateById(id, planoAcao);
                return await this.findPlanoAcao(id);
            } catch (error) {
                console.log(error);
                throw new HttpException("Não foi possível atualizar o plano de ação", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }

    async findAllPlanoAcao(request: any) {
        let planoAcaoArray = await this.findPlanoAcaoAggregate(null, request)
        if (planoAcaoArray && planoAcaoArray.resultado.data.length > 0) {
            let planoAcaoArrayVm = new Array<AcaoVm>();
            for (let index = 0; index < planoAcaoArray.resultado.data.length; index++) {
                const planoAcao = planoAcaoArray.resultado.data[index];
                let situacaoAcoes = await this.countAcoesExecutar(planoAcao.id, null, null);
                let totalSituacaoArray = situacaoAcoes.resultado.totalSituacao;
                situacaoAcoes.resultado.totalSituacao = this.organizarRetornoArraySituacao(totalSituacaoArray);
                let vm = this.getPlanoAcaoVm(planoAcao, null, situacaoAcoes.resultado)
                planoAcaoArrayVm.push(vm);
            }
            planoAcaoArray.resultado.data = planoAcaoArrayVm;
            return planoAcaoArray;
        } else {
            return planoAcaoArray;
        }
    }

    async findPlanoAcao(id: string, situacao?: SituacaoAcaoType, filtroNome?: string, responsavelX?: string, retornarAcoes?: boolean) {
        let planoAcao: { resultado: PagedDataModelVm } = await this.findPlanoAcaoAggregate(id, null)
        if (planoAcao && planoAcao.resultado.data) {
            let acoesArray = null;
            if (retornarAcoes) {
                let params = { situacao: situacao, nome: filtroNome, responsavel: responsavelX }
                acoesArray = await this.findAcoes(planoAcao.resultado.data[0]._id, params);
            }
            if (planoAcao.resultado.data[0]) {
                planoAcao.resultado.data[0].acoes = acoesArray;
                return planoAcao
            }
        }
        return planoAcao
    }

    private async findPlanoAcaoAggregate(id, req: any) {
        const offset = req && req['offset'] ? parseInt(req['offset']) : 0;
        const limit = req && req['limit'] ? parseInt(req['limit']) : 10;
        let filter = this.montarParametros(req);
        filter.push({ _id: { $exists: true } })

        let total = await this.countPlanoAcao(req);
        if (id)
            filter.push({ _id: new ObjectId(id) })

        let aggregate = [
            {
                $match: {
                    $and: filter
                }
            },
            {
                $lookup: {
                    from: 'usuarioinstitutos',
                    localField: 'responsavel',
                    foreignField: '_id',
                    as: 'responsavel'
                }
            },
            {
                $unwind: {
                    path: "$responsavel",
                    preserveNullAndEmptyArrays: false
                }
            }, { $addFields: { id: "$_id", acoes: [] } }];
        let resultado = await this._planoAcaoModel.aggregate(aggregate).skip(offset * limit).limit(limit).exec();




        return { resultado: new PagedDataModelVm(new PageModelVm(offset, limit, total.resultado.total), resultado) };
    }

    private async atualizarSituacaoPlanoAcao(id: Types.ObjectId) {
        let planoAcaoPai = await this._planoAcaoModel.findById(id);
        if (planoAcaoPai.situacao != SituacaoAcaoType.planejando) {
            let situacaoPlanoAcao = await this.calcularSituacaoPlanoAcao(planoAcaoPai._id);
            if (situacaoPlanoAcao != planoAcaoPai.situacao) { // Tirei situacaoAcao != planoAcaoPai.situacao porque não lembro porque preciso comprar com a situacao da ação
                planoAcaoPai.situacao = situacaoPlanoAcao;
                let modifiedPlanoAcao = await this._planoAcaoModel.updateOne({ _id: planoAcaoPai._id }, planoAcaoPai);
                if (modifiedPlanoAcao.nModified <= 0)
                    throw new HttpException("Não foi possível atualizar o plano de ação", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    private async calcularSituacaoPlanoAcao(planoAcaoPai: Types.ObjectId): Promise<SituacaoAcaoType> {
        //let naoIniciada: number = await this._acaoModel.countDocuments({ planoAcaoPai, situacao: SituacaoAcaoType.naoIniciada });
        let iniciada: number = await this._acaoModel.countDocuments({ planoAcaoPai, situacao: SituacaoAcaoType.iniciada, acaoPai: null });
        let concluida: number = await this._acaoModel.countDocuments({ planoAcaoPai, situacao: SituacaoAcaoType.concluida, acaoPai: null });
        let impedida: number = await this._acaoModel.countDocuments({ planoAcaoPai, situacao: SituacaoAcaoType.impedida, acaoPai: null });
        let total: number = await this._acaoModel.countDocuments({ planoAcaoPai, acaoPai: null });
        return this.logicaSituacao(iniciada, concluida, impedida, total);
    }

    async countPlanoAcaoSituacao(request: any) {
        let filter = this.montarParametros(request);
        let totalSituacao = new Array<TotalSituacaoVm>();
        let totalSituacaoVm = new TotalSituacaoVm();
        totalSituacaoVm.situacao = "todas";
        if (filter.length > 0)
            totalSituacaoVm.total = await this._planoAcaoModel.countDocuments({ $and: filter });
        else
            totalSituacaoVm.total = await this._planoAcaoModel.countDocuments({});
        totalSituacao.push(totalSituacaoVm);
        for (let index = 0; index < SituacaoAcaoType.values().length; index++) {
            const situacao = SituacaoAcaoType.values()[index];
            let count = 0;
            if (filter.length > 0)
                count = await this._planoAcaoModel.countDocuments({ situacao, $and: filter });
            else
                count = await this._planoAcaoModel.countDocuments({ situacao: situacao });
            let totalSituacaoVm = new TotalSituacaoVm();
            totalSituacaoVm.situacao = situacao;
            totalSituacaoVm.total = count;
            totalSituacao.push(totalSituacaoVm);
        }
        return { resultado: { totalSituacao } };
    }


    async countPlanoAcao(params) {
        let filter = this.montarParametros(params);
        let total = 0;
        if (filter.length > 0)
            total = await this._planoAcaoModel.countDocuments({ $and: filter });
        else
            total = await this._planoAcaoModel.countDocuments({});
        return { resultado: { total } };
    }

    // Funções das Ações
    async createAcao(vm: AcaoVm) {
        if (vm.acaoPai) {
            let acaoPai = await this._acaoModel.findById(vm.acaoPai);
            if (acaoPai) {
                let irmaos = await this._acaoModel.find({ acaoPai: new ObjectId(new String(vm.acaoPai).toString()) });
                irmaos.push(this.getAcao(vm));
                let investimentoPai = 0.0;
                let dataInicioPai = null;
                let dataFimPai = null;
                for (let index = 0; index < irmaos.length; index++) {
                    const subTarefa = irmaos[index];
                    //Somando os investimento do Pai com os investimentos da Filha
                    investimentoPai = investimentoPai + subTarefa.investimento;
                    // Setando as datas por default sendo as da Filha
                    if (dataInicioPai == null || dataInicioPai == undefined)
                        dataInicioPai = subTarefa.dataInicio
                    if (dataFimPai == null || dataFimPai == undefined)
                        dataFimPai = subTarefa.dataFim
                    // Comparando as datas para pegar a menor (Data Inicial) e a maior (Data Final)
                    if (subTarefa.dataInicio < dataInicioPai)
                        dataInicioPai = subTarefa.dataInicio
                    if (subTarefa.dataFim > dataFimPai)
                        dataFimPai = subTarefa.dataFim
                }
                acaoPai.investimento = investimentoPai;
                acaoPai.dataInicio = dataInicioPai;
                acaoPai.dataFim = dataFimPai;
                await this._acaoModel.updateOne({ _id: acaoPai._id }, acaoPai);
            } else throw new HttpException("Ação Pai não encontrada", HttpStatus.BAD_REQUEST);
        }
        await this.validarVigencia(vm.planoAcaoPai, vm.dataInicio, vm.dataFim);
        let acaoCriada = await this._acaoModel.create(this.getAcao(vm));
        return await this.findAcaoById(acaoCriada.id);
    }

    async excluirAcao(id: string) {
        if (id) {
            let acao = await this.findAcaoById(id);
            if (acao) {
                if (acao.acaoPai) {
                    let acaoPai = await this._acaoModel.findById(acao.acaoPai);
                    let irmaos = await this._acaoModel.find({ acaoPai: new ObjectId(acaoPai.id) });
                    let investimentoPai = acaoPai.investimento;
                    let dataInicioPai = null; //acaoPai.dataInicio;
                    let dataFimPai = null; //acaoPai.dataFim;
                    if (irmaos) {
                        for (let i = 0; i < irmaos.length; i++) {
                            let subTarefa = irmaos[i];
                            if (new String(subTarefa._id).toString() == acao.id) {
                                investimentoPai = investimentoPai - subTarefa.investimento;
                                irmaos = irmaos.filter(i => i._id !== subTarefa._id);
                                break;
                            }
                        }
                        for (let i = 0; i < irmaos.length; i++) {
                            let subTarefa = irmaos[i];
                            // Comparando as datas para pegar a menor (Data Inicial) e a maior (Data Final)
                            if (!dataInicioPai)
                                dataInicioPai = subTarefa.dataInicio
                            if (!dataFimPai)
                                dataFimPai = subTarefa.dataFim
                            if (subTarefa.dataInicio < dataInicioPai)
                                dataInicioPai = subTarefa.dataInicio
                            if (subTarefa.dataFim > dataFimPai)
                                dataFimPai = subTarefa.dataFim
                        }
                    }
                    acaoPai.investimento = investimentoPai;
                    acaoPai.dataInicio = dataInicioPai;
                    acaoPai.dataFim = dataFimPai;
                    await this._acaoModel.updateOne({ _id: acaoPai._id }, acaoPai);
                    await this._acaoModel.deleteOne({ _id: acao.id });
                    return await this.findAcaoById(acaoPai._id);
                } else {
                    await this._acaoModel.deleteMany({ acaoPai: acao.id });
                    await this._acaoModel.deleteOne({ _id: acao.id });
                }
            }
        }
    }

    async updateAcao(vm: AcaoVm) {
        await this.validarVigencia(vm.planoAcaoPai, vm.dataInicio, vm.dataFim);
        // É uma subtarefa
        if (vm.acaoPai) {
            let acaoPaiObjectId = null;
            if (typeof vm.acaoPai === "string")
                acaoPaiObjectId = new ObjectId(vm.acaoPai);
            else
                acaoPaiObjectId = new ObjectId(vm.acaoPai._id);
            let acaoPai = await this._acaoModel.findById(acaoPaiObjectId);
            if (acaoPai) {
                let irmaos = await this._acaoModel.find({ acaoPai: acaoPaiObjectId });
                let investimentoPai = 0.0;
                let dataInicioPai = null;
                let dataFimPai = null;
                let situacaoPai = null;
                let dataRealFimPai = acaoPai.dataRealFim;
                let dataRealInicioPai = acaoPai.dataRealInicio;
                let investimentoRealPai = 0.0;
                for (let i = 0; i < irmaos.length; i++) {
                    let subTarefa = irmaos[i];
                    //É a tarefa que atualizou
                    if (subTarefa.id == vm.id)
                        subTarefa = this.getAcao(vm);
                    //Somando os investimento do Pai com os investimentos da Filha
                    investimentoPai = investimentoPai + subTarefa.investimento;
                    if (subTarefa.investimentoReal)
                        investimentoRealPai = investimentoRealPai + subTarefa.investimentoReal;
                    // Setando as datas por default sendo as da Filha
                    if (subTarefa.dataRealInicio && !dataRealInicioPai)
                        dataRealInicioPai = subTarefa.dataRealInicio;
                    if (subTarefa.dataRealFim && !dataRealFimPai)
                        dataRealFimPai = subTarefa.dataRealFim;
                    if (dataInicioPai == null || dataInicioPai == undefined)
                        dataInicioPai = subTarefa.dataInicio
                    if (dataFimPai == null || dataFimPai == undefined)
                        dataFimPai = subTarefa.dataFim
                    // Comparando as datas para pegar a menor (Data Inicial) e a maior (Data Final)
                    if (subTarefa.dataInicio < dataInicioPai)
                        dataInicioPai = subTarefa.dataInicio
                    if (subTarefa.dataFim > dataFimPai)
                        dataFimPai = subTarefa.dataFim
                    if (subTarefa.dataRealInicio < dataRealInicioPai)
                        dataRealInicioPai = subTarefa.dataRealInicio;
                    if (subTarefa.dataRealFim > dataRealFimPai)
                        dataRealFimPai = subTarefa.dataRealFim;
                }
                // Atualizar dados Tarefa Editada
                let modified = await this._acaoModel.updateOne({ _id: vm.id }, this.getAcao(vm));
                if (modified.nModified > 0) {
                    situacaoPai = await this.calcularSituacaoAcaoPai(acaoPai._id);
                    let mudouSituacao = false;
                    // Atualizar dados acaoPai
                    acaoPai.investimento = investimentoPai;
                    acaoPai.investimentoReal = investimentoRealPai;
                    acaoPai.dataInicio = dataInicioPai;
                    acaoPai.dataFim = dataFimPai;
                    acaoPai.dataRealInicio = dataRealInicioPai;
                    acaoPai.dataRealFim = dataRealFimPai;
                    if (acaoPai.situacao != situacaoPai)
                        mudouSituacao = true;
                    acaoPai.situacao = situacaoPai;
                    let modifiedPai = await this._acaoModel.updateOne({ _id: acaoPai._id }, acaoPai);
                    if (modifiedPai.ok > 0) {
                        // Atualizar o estado plano ação 
                        if (mudouSituacao) {
                            await this.atualizarSituacaoPlanoAcao(acaoPai.planoAcaoPai)
                        }

                    } else
                        throw new HttpException("Não foi possível atualizar a ação pai", HttpStatus.INTERNAL_SERVER_ERROR);
                    return this.findAcaoById(vm.id);
                } else {
                    throw new HttpException("Não foi possível atualizar a ação", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }
        // É uma tarefa pai 
        else {
            let acaoVm = await this.findAcaoById(vm.id);
            if (acaoVm) {
                let modified = await this._acaoModel.updateOne({ _id: vm.id }, this.getAcao(vm));
                if (modified.nModified > 0) {
                    await this.atualizarSituacaoPlanoAcao(this.toObjectId(acaoVm.planoAcaoPai));
                    return this.findAcaoById(vm.id);
                } else {
                    throw new HttpException("Não foi possível atualizar a ação", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }
    }

    private async validarVigencia(idPlano, dataInicio: Date, dataFim: Date) {
        let planoAcaoPai = await this._planoAcaoModel.findById(idPlano).exec();
        if(planoAcaoPai && planoAcaoPai.vigencia ){
            const minDate = new Date(`${planoAcaoPai.vigencia}-01-01T00:00:01`);
            const maxDate = new Date(`${planoAcaoPai.vigencia}-12-31T23:59:59`);
            if(dataInicio && new Date(dataInicio) < minDate) throw new HttpException(`A data de início da ação não pode ser anterior à ${minDate.getFullYear()}`, HttpStatus.BAD_REQUEST);    
            if(dataFim && new Date(dataFim) > maxDate) throw new HttpException(`A data fim da ação não pode ser superior à ${maxDate.getFullYear()}`, HttpStatus.BAD_REQUEST);    
        }
         else throw new HttpException("Plano de ação não encontrado", HttpStatus.BAD_REQUEST);
    }

    private async calcularSituacaoAcaoPai(acaoPai: string): Promise<SituacaoAcaoType> {
        //let naoIniciada: number = await this._acaoModel.countDocuments({ planoAcaoPai, situacao: SituacaoAcaoType.naoIniciada });
        let iniciada: number = await this._acaoModel.countDocuments({ acaoPai, situacao: SituacaoAcaoType.iniciada });
        let concluida: number = await this._acaoModel.countDocuments({ acaoPai, situacao: SituacaoAcaoType.concluida });
        let impedida: number = await this._acaoModel.countDocuments({ acaoPai, situacao: SituacaoAcaoType.impedida });
        let total: number = await this._acaoModel.countDocuments({ acaoPai });
        return this.logicaSituacao(iniciada, concluida, impedida, total);
    }

    async totalAcoes(id: string, request: any) {
        let filter = this.montarParametros(request);
        let total = 0;
        if (filter.length > 0)
            total = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id), $and: filter });
        else
            total = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id) });
        return { resultado: { total } };
    }

    async countAcoes(id: string, request: any) {
        let filter = this.montarParametros(request);
        let totalSituacao = new Array<TotalSituacaoVm>();
        let totalSituacaoVm = new TotalSituacaoVm();
        totalSituacaoVm.situacao = "todas";
        if (filter.length > 0)
            totalSituacaoVm.total = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id), $and: filter, acaoPai: null });
        else
            totalSituacaoVm.total = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id), acaoPai: null });
        totalSituacao.push(totalSituacaoVm);
        let situacaoArray = SituacaoAcaoType.values().filter(d => d != SituacaoAcaoType.planejando);
        for (let index = 0; index < situacaoArray.length; index++) {
            const situacao = situacaoArray[index];
            let count = 0;
            if (filter.length > 0)
                count = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id), situacao, $and: filter, acaoPai: null });
            else
                count = await this._acaoModel.countDocuments({ planoAcaoPai: new ObjectId(id), situacao: situacao, acaoPai: null });
            let totalSituacaoVm = new TotalSituacaoVm();
            totalSituacaoVm.situacao = situacao;
            totalSituacaoVm.total = count;
            totalSituacao.push(totalSituacaoVm);
        }
        let totalImpedida = totalSituacao.filter(s => s.situacao == SituacaoAcaoType.impedida)[0].total;
        let totalConcluida = totalSituacao.filter(s => s.situacao == SituacaoAcaoType.concluida)[0].total;
        let percentual = 0;
        if (totalSituacaoVm.total > 0)
            percentual = (totalConcluida * 100) / totalSituacaoVm.total;
        return { resultado: { totalSituacao, percentual, impedida: totalImpedida > 0 } };
    }

    async countAcoesExecutar(id: string, idVoluntario: string, request: any) {
        let totalSituacao = new Array<TotalSituacaoVm>();
        let totalSituacaoVm = new TotalSituacaoVm();
        totalSituacaoVm.situacao = "todas";
        totalSituacaoVm.total = await this.countTotalAcoesExecutar(id, idVoluntario, request);
        totalSituacao.push(totalSituacaoVm);
        let situacaoArray = SituacaoAcaoType.values().filter(d => d != SituacaoAcaoType.planejando);
        for (let index = 0; index < situacaoArray.length; index++) {
            const situacao = situacaoArray[index];
            let totalSituacaoVm = new TotalSituacaoVm();
            totalSituacaoVm.situacao = situacao;
            request = request ? request : {}
            request.situacao = situacao;
            totalSituacaoVm.total = await this.countTotalAcoesExecutar(id, idVoluntario, request);
            totalSituacao.push(totalSituacaoVm);
        }
        let totalImpedida = totalSituacao.filter(s => s.situacao == SituacaoAcaoType.impedida)[0].total;
        let totalConcluida = totalSituacao.filter(s => s.situacao == SituacaoAcaoType.concluida)[0].total;
        let percentual = 0;
        if (totalSituacaoVm.total > 0)
            percentual = (totalConcluida * 100) / totalSituacaoVm.total;
        return { resultado: { totalSituacao, percentual, impedida: totalImpedida > 0 } };
    }

    async findAcaoById(id: string): Promise<AcaoVm> {
        let a = await this.findAcoes(null, null, id);
        return a.resultado.data[0];
    }

    async findAcoesExecutadas(idPlanoAcao: string, idVoluntario: string, request?: any) {
        const offset = request && request['offset'] ? parseInt(request['offset']) : 0;
        const limit = request && request['limit'] ? parseInt(request['limit']) : 10;
        let filter = this.montarParametros(request);
        filter.push({ responsavel: new ObjectId(idVoluntario) });
        filter.push({ planoAcaoPai: new ObjectId(idPlanoAcao) })
        let total = await this.countTotalAcoesExecutar(idPlanoAcao, idVoluntario, request);
        let resultado = await this._acaoModel.aggregate(
            [
                {
                    $match: { $and: filter }
                },
                {
                    $lookup: {
                        from: 'acao',
                        localField: 'acaoPai',
                        foreignField: '_id',
                        as: 'acaoPaiObj'
                    }
                },
                {
                    $unwind: {
                        path: "$acaoPaiObj",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $lookup: {
                        from: 'acao',
                        localField: '_id',
                        foreignField: 'acaoPai',
                        as: 'subTarefas'
                    }
                },
                {
                    $unwind: {
                        path: "$subTarefas",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        "subTarefas": null
                    }
                },
                {
                    $lookup: {
                        from: 'usuarioinstitutos',
                        localField: 'responsavel',
                        foreignField: '_id',
                        as: 'responsavel'
                    }
                },
                {
                    $unwind: {
                        path: "$responsavel",
                        preserveNullAndEmptyArrays: false
                    }
                },
                {
                    $lookup: {
                        from: 'justificativaajustes',
                        localField: '_id',
                        foreignField: 'idAcao',
                        as: 'justificativa'
                    }
                },
                {
                    $unwind: {
                        path: "$justificativa",
                        preserveNullAndEmptyArrays: true
                    }
                },
                { $sort: { "justificativa.dataCriacao": 1 } },
                {
                    $group: {
                        _id: "$_id",
                        id: { $first: "$_id" },
                        dataCriacao: { $first: "$dataCriacao" },
                        dataModificacao: { $first: "$dataModificacao" },
                        nome: { $first: "$nome" },
                        acaoPai: { $first: "$acaoPai" },
                        dataFim: { $first: "$dataFim" },
                        dataInicio: { $first: "$dataInicio" },
                        dataRealFim: { $first: "$dataRealFim" },
                        dataRealInicio: { $first: "$dataRealInicio" },
                        descricao: { $first: "$descricao" },
                        investimento: { $first: "$investimento" },
                        investimentoReal: { $first: "$investimentoReal" },
                        lugar: { $first: "$lugar" },
                        motivo: { $first: "$motivo" },
                        planoAcaoPai: { $first: "$planoAcaoPai" },
                        situacao: { $first: "$situacao" },
                        responsavel: { $first: "$responsavel" },
                        acaoPaiObj: { $first: "$acaoPaiObj" },
                        justificativaImpedimento: {
                            $push: {
                                $cond: {
                                    if: { $eq: ["$justificativa.tipo", "impedimentoAcao"] },
                                    then: {
                                        _id: "$justificativa._id",
                                        justificativa: "$justificativa.justificativa",
                                        idAcao: "$justificativa.idAcao",
                                        idVoluntario: "$justificativa.idVoluntario",
                                        idVoluntarioCriador: "$justificativa.idVoluntarioCriador",
                                        nomeVoluntario: "$justificativa.nomeVoluntario",
                                        nomeVoluntarioCriador: "$justificativa.nomeVoluntarioCriador",
                                        emailVoluntarioCriador: "$justificativa.emailVoluntarioCriador",
                                        tipo: "$justificativa.tipo"
                                    },
                                    else: null
                                }
                            }
                        },
                        justificativaInvestimento: {
                            $push: {
                                $cond: {
                                    if: { $eq: ["$justificativa.tipo", "investimentoMaior"] },
                                    then: {
                                        _id: "$justificativa._id",
                                        justificativa: "$justificativa.justificativa",
                                        idAcao: "$justificativa.idAcao",
                                        idVoluntario: "$justificativa.idVoluntario",
                                        idVoluntarioCriador: "$justificativa.idVoluntarioCriador",
                                        nomeVoluntario: "$justificativa.nomeVoluntario",
                                        nomeVoluntarioCriador: "$justificativa.nomeVoluntarioCriador",
                                        emailVoluntarioCriador: "$justificativa.emailVoluntarioCriador",
                                        tipo: "$justificativa.tipo"
                                    },
                                    else: null
                                }
                            }
                        }
                    }
                },
                { $sort: { dataInicio: 1 } }
            ]
        ).skip(offset * limit).limit(limit).exec();
        let array = [];
        resultado.forEach(element => {
            let justificativaImpedimento = element.justificativaImpedimento.filter(s => s);
            let justificativaInvestimento = element.justificativaInvestimento.filter(s => s);
            element.justificativaImpedimento = justificativaImpedimento ? justificativaImpedimento[0] : null
            element.justificativaInvestimento = justificativaInvestimento ? justificativaInvestimento[0] : null
            array.push(element);
        });
        resultado = array;
        return { resultado: new PagedDataModelVm(new PageModelVm(offset, limit, total), resultado) };
    }


    async findAcoes(idPlanoAcao: string, request?: any, idAcao?: string) {
        const offset = request && request['offset'] ? parseInt(request['offset']) : 0;
        const limit = request && request['limit'] ? parseInt(request['limit']) : 10;

        let filter = this.montarParametros(request);
        if (idPlanoAcao)
            filter.push({ acaoPai: null }, { planoAcaoPai: new ObjectId(idPlanoAcao) })
        else if (idAcao)
            filter.push({ _id: new ObjectId(idAcao) })

        let total: number = await this._acaoModel.countDocuments({ $and: filter });
        let sort = { $sort: { dataCriacao: -1 } }
        let resultado = await this._acaoModel.aggregate(
            [
                {
                    $match: {
                        $and: filter
                    }
                },
                {
                    $lookup: {
                        from: 'acao',
                        localField: '_id',
                        foreignField: 'acaoPai',
                        as: 'subTarefas'
                    }
                },
                {
                    $unwind: {
                        path: "$subTarefas",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'usuarioinstitutos',
                        localField: 'responsavel',
                        foreignField: '_id',
                        as: 'responsavel'
                    }
                },
                {
                    $unwind: {
                        path: "$responsavel",
                        preserveNullAndEmptyArrays: false
                    }
                },
                {
                    $lookup: {
                        from: 'usuarioinstitutos',
                        localField: 'subTarefas.responsavel',
                        foreignField: '_id',
                        as: 'responsavel2'
                    }
                },
                {
                    $unwind: {
                        path: "$responsavel2",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'acao',
                        localField: 'acaoPai',
                        foreignField: '_id',
                        as: 'acaoPai'
                    }
                },
                {
                    $unwind: {
                        path: "$acaoPai",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'justificativaajustes',
                        localField: '_id',
                        foreignField: 'idAcao',
                        as: 'justificativa'
                    }
                },
                {
                    $unwind: {
                        path: "$justificativa",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'justificativaajustes',
                        localField: 'subTarefas._id',
                        foreignField: 'idAcao',
                        as: 'justificativa2'
                    }
                },
                { $sort: { "justificativa.dataCriacao": 1 } },
                {
                    $group: {
                        _id: "$_id",
                        id: { $first: "$_id" },
                        dataCriacao: { $first: "$dataCriacao" },
                        dataModificacao: { $first: "$dataModificacao" },
                        nome: { $first: "$nome" },
                        acaoPai: { $first: "$acaoPai" },
                        dataFim: { $first: "$dataFim" },
                        dataRealFim: { $first: "$dataRealFim" },
                        dataRealInicio: { $first: "$dataRealInicio" },
                        dataInicio: { $first: "$dataInicio" },
                        descricao: { $first: "$descricao" },
                        investimento: { $first: "$investimento" },
                        investimentoReal: { $first: "$investimentoReal" },
                        lugar: { $first: "$lugar" },
                        motivo: { $first: "$motivo" },
                        planoAcaoPai: { $first: "$planoAcaoPai" },
                        situacao: { $first: "$situacao" },
                        responsavel: { $first: "$responsavel" },
                        subTarefas: {
                            $addToSet: {
                                $cond: {
                                    if: { $gt: ["$subTarefas", null] },
                                    then: {
                                        _id: "$subTarefas._id",
                                        id: "$subTarefas._id",
                                        dataCriacao: "$subTarefas.dataCriacao",
                                        dataModificacao: "$subTarefas.dataModificacao",
                                        nome: "$subTarefas.nome",
                                        acaoPai: "$subTarefas.acaoPai",
                                        dataFim: "$subTarefas.dataFim",
                                        dataInicio: "$subTarefas.dataInicio",
                                        dataRealFim: "$subTarefas.dataRealFim",
                                        dataRealInicio: "$subTarefas.dataRealInicio",
                                        descricao: "$subTarefas.descricao",
                                        investimento: "$subTarefas.investimento",
                                        investimentoReal: "$subTarefas.investimentoReal",
                                        lugar: "$subTarefas.lugar",
                                        motivo: "$subTarefas.motivo",
                                        situacao: "$subTarefas.situacao",
                                        planoAcaoPai: "$subTarefas.planoAcaoPai",
                                        responsavel: "$responsavel2",
                                        justificativaImpedimento: {
                                            $filter: {
                                                input: "$justificativa2",
                                                as: "justificativa2",
                                                cond: { $eq: ["$$justificativa2.tipo", "impedimentoAcao"] }
                                            }
                                        },
                                        justificativaInvestimento: {
                                            $filter: {
                                                input: "$justificativa2",
                                                as: "justificativa2",
                                                cond: { $eq: ["$$justificativa2.tipo", "investimentoMaior"] }
                                            }
                                        }
                                    },
                                    else: null
                                }
                            }
                        },
                        justificativaImpedimento: {
                            $addToSet: {
                                $cond: {
                                    if: { $eq: ["$justificativa.tipo", "impedimentoAcao"] },
                                    then: {
                                        _id: "$justificativa._id",
                                        justificativa: "$justificativa.justificativa",
                                        idAcao: "$justificativa.idAcao",
                                        idVoluntario: "$justificativa.idVoluntario",
                                        idVoluntarioCriador: "$justificativa.idVoluntarioCriador",
                                        nomeVoluntario: "$justificativa.nomeVoluntario",
                                        nomeVoluntarioCriador: "$justificativa.nomeVoluntarioCriador",
                                        emailVoluntarioCriador: "$justificativa.emailVoluntarioCriador",
                                        tipo: "$justificativa.tipo"
                                    },
                                    else: null
                                }
                            }
                        },
                        justificativaInvestimento: {
                            $addToSet: {
                                $cond: {
                                    if: { $eq: ["$justificativa.tipo", "investimentoMaior"] },
                                    then: {
                                        _id: "$justificativa._id",
                                        justificativa: "$justificativa.justificativa",
                                        idAcao: "$justificativa.idAcao",
                                        idVoluntario: "$justificativa.idVoluntario",
                                        idVoluntarioCriador: "$justificativa.idVoluntarioCriador",
                                        nomeVoluntario: "$justificativa.nomeVoluntario",
                                        nomeVoluntarioCriador: "$justificativa.nomeVoluntarioCriador",
                                        emailVoluntarioCriador: "$justificativa.emailVoluntarioCriador",
                                        tipo: "$justificativa.tipo"
                                    },
                                    else: null
                                }
                            }
                        },

                    }
                },
                sort
            ]
        ).skip(offset * limit).limit(limit).exec();
        let array = [];
        resultado.forEach(element => {
            let justificativaImpedimento = element.justificativaImpedimento.filter(s => s);
            let justificativaInvestimento = element.justificativaInvestimento.filter(s => s);
            element.justificativaImpedimento = justificativaImpedimento ? justificativaImpedimento[0] : null
            element.justificativaInvestimento = justificativaInvestimento ? justificativaInvestimento[0] : null
            array.push(element);
        });
        resultado = array;
        return { resultado: new PagedDataModelVm(new PageModelVm(offset, limit, total), resultado) };

    }

    // Métodos privados de apoio
    private async countTotalAcoesExecutar(idPlanoAcao: string, idVoluntario: string, request: any) {
        let filter = this.montarParametros(request);
        filter.push({ planoAcaoPai: new ObjectId(idPlanoAcao) })
        if (idVoluntario)
            filter.push({ responsavel: new ObjectId(idVoluntario) })
        let total = await this._acaoModel.aggregate(
            [
                {
                    $match: {
                        $and: filter
                    }
                },
                {
                    $lookup: {
                        from: 'acao',
                        localField: 'acaoPai',
                        foreignField: '_id',
                        as: 'acaoPaiObj'
                    }
                },
                {
                    $unwind: {
                        path: "$acaoPaiObj",
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $lookup: {
                        from: 'acao',
                        localField: '_id',
                        foreignField: 'acaoPai',
                        as: 'subTarefas'
                    }
                },
                {
                    $unwind: {
                        path: "$subTarefas",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        "subTarefas": null
                    }
                },
                {
                    $count: "total"
                }
            ]
        ).exec();
        return total.length > 0 ? total[0].total : 0;
    }

    private getPlanoAcao(vm: PlanoAcaoVm): any {
        var planoAcao = new PlanoAcaoModel();
        planoAcao._id = vm.id;
        planoAcao.dataProximaRevisao = vm.dataProximaRevisao
        planoAcao.dataCriacao = vm.dataCriacao;
        planoAcao.vigencia = vm.vigencia;
        planoAcao.nome = vm.nome
        if (vm.responsavel) {
            if(typeof vm.responsavel === "string") {
                planoAcao.responsavel = this.toObjectId(vm.responsavel)
            } else if(vm.responsavel._id) {
                planoAcao.responsavel = this.toObjectId(vm.responsavel._id);
            } else if(vm.responsavel.id) {
                planoAcao.responsavel = this.toObjectId(vm.responsavel.id);
            }
        }
        planoAcao.situacao = vm.situacao
        planoAcao.iniciado = vm.iniciado;
        planoAcao.numeroCooperativa = vm.numeroCooperativa;
        return planoAcao;
    }

    private getPlanoAcaoVm(planoAcao: InstanceType<PlanoAcao>, acoes: AcaoVm[], situacaoAcoes?: any): any {
        var vm = new PlanoAcaoVm();
        vm.id = planoAcao._id;
        vm.dataCriacao = planoAcao.dataCriacao;
        vm.dataModificacao = planoAcao.dataModificacao;
        vm.dataProximaRevisao = planoAcao.dataProximaRevisao;
        vm.nome = planoAcao.nome;
        vm.vigencia = planoAcao.vigencia;
        vm.responsavel = typeof planoAcao.responsavel === "string" ? new String(planoAcao.responsavel).toString() : planoAcao.responsavel;
        vm.situacao = planoAcao.situacao;
        vm.acoes = acoes;
        vm.iniciado = planoAcao.iniciado;
        vm.numeroCooperativa = planoAcao.numeroCooperativa;
        vm.situacaoAcoes = situacaoAcoes;
        return vm;
    }

    private getAcao(vm: AcaoVm): any {
        var acao = new AcaoModel();
        acao._id = vm.id;
        acao.dataCriacao = vm.dataCriacao;
        acao.nome = vm.nome;
        if (vm.acaoPai && vm.acaoPai._id)
            acao.acaoPai = this.toObjectId(vm.acaoPai._id);
        else if (typeof vm.acaoPai === "string")
            acao.acaoPai = this.toObjectId(vm.acaoPai);
        acao.dataFim = vm.dataFim;
        acao.dataInicio = vm.dataInicio;
        acao.descricao = vm.descricao;
        acao.investimento = vm.investimento;
        acao.investimentoReal = vm.investimentoReal;
        acao.lugar = vm.lugar;
        acao.motivo = vm.motivo;
        acao.planoAcaoPai = vm.planoAcaoPai ? this.toObjectId(vm.planoAcaoPai) : null;
        if (vm.responsavel && vm.responsavel.id)
            acao.responsavel = this.toObjectId(vm.responsavel.id);
        else if (typeof vm.responsavel === "string")
            acao.responsavel = this.toObjectId(vm.responsavel);
        acao.situacao = vm.situacao;
        acao.dataRealFim = vm.dataRealFim
        acao.dataRealInicio = vm.dataRealInicio
        return acao;
    }

    private getAcaoVm(acao: InstanceType<Acao>, subTarefas?: AcaoVm[]): any {
        var vm = new AcaoVm();
        vm.id = acao._id;
        vm.dataCriacao = acao.dataCriacao;
        vm.dataModificacao = acao.dataModificacao;
        vm.nome = acao.nome;
        vm.acaoPai = acao.acaoPai ? new String(acao.acaoPai).toString() : null;
        vm.dataFim = acao.dataFim;
        vm.dataInicio = acao.dataInicio;
        vm.descricao = acao.descricao;
        vm.investimento = acao.investimento;
        vm.investimentoReal = acao.investimentoReal;
        vm.lugar = acao.lugar;
        vm.motivo = acao.motivo;
        vm.planoAcaoPai = acao.planoAcaoPai ? new String(acao.planoAcaoPai).toString() : null;
        //vm.responsavel = acao.responsavel ? new String(acao.responsavel).toString() : null;
        vm.situacao = acao.situacao;
        vm.subTarefas = subTarefas;
        vm.dataRealFim = acao.dataRealFim;
        vm.dataRealInicio = acao.dataRealInicio;
        return vm;
    }

    private montarParametros(params) {
        let filter = [];
        if (params) {
            let responsavel = params.responsavel;
            let situacao = params.situacao;
            let numeroCooperativa = params.numeroCooperativa;
            let ano = params.ano;
            let nome = params.nome;
            let vigencia = params.vigencia;
            if (vigencia) {
                filter.push({ vigencia });
            }
            if (responsavel) {
                filter.push({ responsavel: new ObjectId(responsavel) });
            }
            if (situacao) {
                filter.push({ situacao });
            }
            if (numeroCooperativa) {
                filter.push({ numeroCooperativa: { $in: numeroCooperativa.split(',') } });
            }
            if (nome) {
                filter.push({ nome: new RegExp(nome, 'i') })
            }
            if (ano) {
                filter.push({
                    created_at: {
                        $gte: new Date(ano + "-01-01T00:00:00.000Z"),
                        $lt: new Date(ano + "-12-31T23:59:59.000Z")
                    }
                });
            }
        }
        return filter;
    }

    private organizarRetornoArraySituacao(totalSituacaoArray: TotalSituacaoVm[]): TotalSituacaoVm[] {
        totalSituacaoArray = totalSituacaoArray.filter(d => (d.situacao == SituacaoAcaoType.naoIniciada || d.situacao == SituacaoAcaoType.concluida || d.situacao == SituacaoAcaoType.impedida))
        let arraySitu = new Array<TotalSituacaoVm>(3);
        totalSituacaoArray.forEach(d => {
            if (d.situacao == SituacaoAcaoType.naoIniciada)
                arraySitu[0] = d;
            else if (d.situacao == SituacaoAcaoType.impedida)
                arraySitu[1] = d;
            else if (d.situacao == SituacaoAcaoType.concluida)
                arraySitu[2] = d;
        });
        return arraySitu;
    }

    // Este método tem um escopo público, porque precisei realizar testes unitários nele
    public logicaSituacao(iniciada: number, concluida: number, impedida: number, total: number) {
        let situacao: SituacaoAcaoType = null;
        if (iniciada > 0)
            situacao = SituacaoAcaoType.iniciada;
        else if (concluida > 0 && concluida == total)
            situacao = SituacaoAcaoType.concluida;
        else if (impedida > 0)
            situacao = SituacaoAcaoType.impedida;
        else if (concluida > 0)
            situacao = SituacaoAcaoType.iniciada;
        else
            situacao = SituacaoAcaoType.naoIniciada;
        return situacao;
    }



}
