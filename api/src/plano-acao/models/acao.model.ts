import { SchemaOptions, Types } from 'mongoose';
import { ModelType, pre, prop, Typegoose } from 'typegoose';
import { SituacaoAcaoType } from './situacao-acao.type';

@pre<Acao>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class Acao extends Typegoose {

    
    @prop({
        required: [true, 'Responsável pela Ação é obrigatório']
    })
    responsavel: Types.ObjectId;

    
    @prop({})
    dataInicio: Date;

    
    @prop({})
    dataFim: Date;

    
    @prop({
        required: [true, 'O nome da Ação é obrigatório']
    })
    nome: string;

    
    @prop({
        required: [true, 'O lugar da Ação é obrigatório']
    })
    lugar: string;

    
    @prop({})
    investimento: number;

    
    @prop({
        required: [true, 'A descrição da Ação é obrigatória']
    })
    descricao: string;

    
    @prop({
        required: [true, 'O motivo da Ação é obrigatório']
    })
    motivo: string;
    
     
    @prop({}) 
    acaoPai: Types.ObjectId;

     
    @prop({}) 
    planoAcaoPai: Types.ObjectId;

     
    @prop({}) 
    situacao: SituacaoAcaoType

     
    @prop({})
    dataRealInicio: Date
    
    
    @prop({})
    dataRealFim: Date

    @prop({})
    investimentoReal: number;

    
    @prop({ default: Date.now })
    dataCriacao?: Date;

    
    @prop({ default: Date.now })
    dataModificacao?: Date;

 

    static get model(): ModelType<Acao> {
        return new Acao().getModelForClass(Acao, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    collection: 'acao'
};

export const AcaoModel = new Acao().getModelForClass(Acao, { schemaOptions });
