export enum SituacaoAcaoType {
    naoIniciada = 'naoIniciada',
    iniciada = 'iniciada',
    concluida = 'concluida',
    impedida = 'impedida',
    planejando = 'planejando'
  }
  
  export namespace SituacaoAcaoType {
    export function values(): SituacaoAcaoType[] {
      return [
        SituacaoAcaoType.naoIniciada,
        SituacaoAcaoType.iniciada,
        SituacaoAcaoType.concluida,
        SituacaoAcaoType.impedida,
        SituacaoAcaoType.planejando,
      ]
    }
  
  }
  