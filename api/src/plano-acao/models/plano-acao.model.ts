import { ModelType, pre, prop, Typegoose, plugin } from 'typegoose';
import { SituacaoAcaoType } from './situacao-acao.type';
import { SchemaOptions, Types } from 'mongoose';

@pre<PlanoAcao>('findOneAndUpdate', function(next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})

export class PlanoAcao extends Typegoose {
    
   
    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

   
    @prop({ default: Date.now, index: true })
    dataModificacao?: Date;

   
    @prop({
        required: [true, 'Nome do Plano de Ação é obrigatório']
    })
    nome: string;

    @prop({}) 
    vigencia: string;

    @prop({
        required: [true, 'Data de Próxima Revisão é obrigatória']
    })
    dataProximaRevisao: Date;

   
    @prop({}) 
    responsavel: Types.ObjectId;

    
    @prop({default: SituacaoAcaoType.planejando}) 
    situacao: SituacaoAcaoType;

    
    @prop({default: false}) 
    iniciado: boolean;

    
    @prop({}) 
    numeroCooperativa: string

    static get model(): ModelType<PlanoAcao> {
        return new PlanoAcao().getModelForClass(PlanoAcao, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
    
}


const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    collection: 'plano-acao'
};

export const PlanoAcaoModel = new PlanoAcao().getModelForClass(PlanoAcao, { schemaOptions });





