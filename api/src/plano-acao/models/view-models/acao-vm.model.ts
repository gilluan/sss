import { SituacaoAcaoType } from "../situacao-acao.type";
import { ComumModelVm } from '@sins/comum';
import { UsuarioInstitutoVm } from "../../../usuario-instituto/models/view-models/usuario-instituto-vm.model";
import { VoluntarioVm } from "../../../voluntario/models/view-models/voluntario-vm.model";
import { Acao } from "../acao.model";
import { InstanceType } from 'typegoose';

export class AcaoVm extends ComumModelVm {

  id: string;
  responsavel: UsuarioInstitutoVm | VoluntarioVm;
  dataInicio: Date;
  dataFim: Date;
  nome: string;
  lugar: string;
  investimento: number;
  investimentoReal: number;
  descricao: string;
  motivo: string;
  acaoPai: any;
  planoAcaoPai: string;
  situacao: SituacaoAcaoType;
  subTarefas: AcaoVm[];
  dataRealInicio: Date;
  dataRealFim: Date;
  acaoPaiObj: InstanceType<Acao>


}
