import { ComumModelVm } from '@sins/comum';
import { SituacaoAcaoType } from "../situacao-acao.type";
import { AcaoVm } from './acao-vm.model';

export class PlanoAcaoVm extends ComumModelVm {
  id: string;
  nome: string;
  situacao: SituacaoAcaoType;
  dataProximaRevisao: Date;
  responsavel: any;
  iniciado: boolean;
  acoes: AcaoVm[];
  numeroCooperativa: string
  situacaoAcoes: any;
  vigencia: string;
}
