import { SituacaoAcaoType } from './models/situacao-acao.type';
import { PlanoAcaoService } from './plano-acao.service';

describe('PlanoAcaoController', () => {
    let planoAcaoService: PlanoAcaoService;

    beforeEach(() => {
        planoAcaoService = new PlanoAcaoService(null, null, null);
/*     catsController = new PlanoAcaoController(planoAcaoService);
 */  });

    describe('logicaSituacao', () => {

        it('Tarefas zeradas', async () => {
            let iniciada: number = 0;
            let concluida: number = 0;
            let impedida: number = 0;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.naoIniciada;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Somente tarefas naoIniciadas', async () => {
            let iniciada: number = 0;
            let concluida: number = 0;
            let impedida: number = 0;
            let naoIniciada: number = 4;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.naoIniciada;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Somente tarefas iniciadas', async () => {
            let iniciada: number = 3;
            let concluida: number = 0;
            let impedida: number = 0;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.iniciada;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Somente tarefas concluidas', async () => {
            let iniciada: number = 0;
            let concluida: number = 3;
            let impedida: number = 0;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.concluida;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Somente tarefas impedidas', async () => {
            let iniciada: number = 0;
            let concluida: number = 0;
            let impedida: number = 3;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.impedida;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Tarefas iniciadas e impedidas', async () => {
            let iniciada: number = 3;
            let concluida: number = 0;
            let impedida: number = 3;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.iniciada;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Tarefas iniciadas e concluidas', async () => {
            let iniciada: number = 3;
            let concluida: number = 3;
            let impedida: number = 0;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.iniciada;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Tarefas impedidas e concluidas', async () => {
            let iniciada: number = 0;
            let concluida: number = 3;
            let impedida: number = 1;
            let naoIniciada: number = 0;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.impedida;
            expect(await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)).toBe(result);
        });
        it('Tarefas impedidas e naoIniciada', async () => {
            let iniciada: number = 0;
            let concluida: number = 0;
            let impedida: number = 1;
            let naoIniciada: number = 1;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.impedida;
            let situacao = await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)
            expect(situacao).toBe(result);
        });
        it('Tarefas concluidas e naoIniciada', async () => {
            let iniciada: number = 0;
            let concluida: number = 3;
            let impedida: number = 0;
            let naoIniciada: number = 1;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.iniciada;
            let situacao = await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)
            expect(situacao).toBe(result);
        });
        it('Todas as situações preenchidas', async () => {
            let iniciada: number = 1;
            let concluida: number = 1;
            let impedida: number = 1;
            let naoIniciada: number = 1;
            const total: number = iniciada + concluida + impedida + naoIniciada;
            const result = SituacaoAcaoType.iniciada;
            let situacao = await planoAcaoService.logicaSituacao(iniciada, concluida, impedida, total)
            expect(situacao).toBe(result);
        });
    });
});
