import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, Req } from '@nestjs/common';
import { Request } from 'express';
import { AcaoVm } from './models/view-models/acao-vm.model';
import { PlanoAcaoVm } from './models/view-models/plano-acao-vm.model';
import { PlanoAcaoService } from './plano-acao.service';
import { PagedDataModelVm } from '@sins/comum';

@Controller('planos-acao')
export class PlanoAcaoController {

    constructor(private readonly _planoAcaoService: PlanoAcaoService) { }

    @Get("/total-registros")
    async countPlanoAcao(@Query() params) {
        return this._planoAcaoService.countPlanoAcao(params).then(total => total);
    }


    @Post()
    async register(@Body() vm: PlanoAcaoVm): Promise<PlanoAcaoVm> {
        return this._planoAcaoService.createPlanoAcao(vm).then(planoAcao => planoAcao.toJSON() as PlanoAcaoVm);
    }

    @Post('/acao')
    async registerAcao(@Body() vm: AcaoVm): Promise<any> {
        return this._planoAcaoService.createAcao(vm).then(acao => acao);
    }

    @Delete('/acao/:id')
    async excluirAcao(@Param("id") id: string) {
        let acoes = await this._planoAcaoService.excluirAcao(id).catch( error =>{ throw error });
        return acoes;
    }

    @Delete('/:id')
    async excluirPlanoAcao(@Param("id") id: string) {
        let planosAcao =  await this._planoAcaoService.excluirPlanoAcao(id).catch( error =>{ throw error });
        return planosAcao;
    }

    @Put()
    async update(@Body() vm: PlanoAcaoVm): Promise<PlanoAcaoVm> {
        return this._planoAcaoService.updatePlanoAcao(vm).then(planoAcao => planoAcao);
    }

    @Patch(':id/iniciar')
    async iniciarPlanoAcao(@Param('id') id: string): Promise<{resultado: PagedDataModelVm}> {
        return this._planoAcaoService.iniciarPlanoAcao(id).then(planoAcao => planoAcao);
    }

    @Put('/acao')
    async updateAcao(@Body() vm: AcaoVm): Promise<AcaoVm> {
        return this._planoAcaoService.updateAcao(vm).then(acao => {
            return acao;
        });
    }

    @Get()
    async findAllPlanoAcao(@Query() params) {
        return this._planoAcaoService.findAllPlanoAcao(params).then(planoAcao => planoAcao);
    }

    @Get("/:id")
    async findPlanoAcao(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.findPlanoAcao(request.params["id"], request.query["situacao"], request.query["nome"], request.query["responsavel"] ).then(planoAcao => planoAcao);
    }

    @Get("/:id/acoes-executar")
    async findAcoesExecutar(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.findAcoesExecutadas(request.query.planoAcao, request.params["id"], request.query).then(acoes => acoes);
    }

    @Get("/:id/acoes-executar/situacao/total-registros-agrupados")
    async countAcoesExecutar(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.countAcoesExecutar(request.query.planoAcao, request.params["id"], request.query).then(count => count);
    }

    @Get("/:id/acoes")
    async findAcoes(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.findAcoes(request.params["id"], request.query).then(acoes => acoes);
    }

    @Get("/situacao/total-registros-agrupados")
    async countPlanoAcaoSituacao(@Query() params) {
        return this._planoAcaoService.countPlanoAcaoSituacao(params).then(total => total);
    }

    @Get("/:id/acoes/total-registros")
    async totalAcoes(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.totalAcoes(request.params["id"], request.query).then(acoes => acoes);
    }

    @Get("/:id/acoes/situacao/total-registros-agrupados")
    async countAcoes(@Req() request: Request): Promise<any> {
        return this._planoAcaoService.countAcoes(request.params["id"], request.query).then(count => count);
    }

    @Get("/acao/:id")
    async findAcaoById(@Param('id') id: string): Promise<any> {
        return this._planoAcaoService.findAcaoById(id).then(acao => acao);
    }

 
}
