import { Injectable } from '@nestjs/common';
import 'automapper-ts/dist/automapper';

@Injectable()
export class MapperTermoCompromissoService {
    mapper: AutoMapperJs.AutoMapper;

    constructor() {
        this.mapper = automapper;
        this.initializeMapper();
    }

    private initializeMapper(): void {
        this.mapper.initialize(MapperTermoCompromissoService.configure);
    }

    private static configure(config: AutoMapperJs.IConfiguration): void {
        config
            .createMap('TermoCompromisso', 'TermoCompromissoVm')
            .forSourceMember('_id', opts => opts.ignored());

    }
}