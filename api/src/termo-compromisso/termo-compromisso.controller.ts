import {TermoCompromissoService} from './termo-compromisso.service';
import {Body, Controller, Get, Put, Query, UploadedFile, UseInterceptors, Post, FileFieldsInterceptor, Request, FileInterceptor} from '@nestjs/common';
import {TermoCompromissoVm} from './models/view-models/termo-compromisso-vm.model';
import { Request as RequestExpress } from 'express';


@Controller('termos-compromisso')
export class TermoCompromissoController {

    constructor(private readonly _termoCompromissoService: TermoCompromissoService) { }

    private mapTo<T>(object: any): Promise<T> {
        return this._termoCompromissoService.map<T>(object);
    }

    @Put()
    @UseInterceptors(FileInterceptor('thumbnail'))
    async update(@UploadedFile() file, @Body() vm: any) {
        vm.thumbnail = file;
        return this._termoCompromissoService.updateTermoCompromisso(vm).then(termoCompromisso => this.mapTo<TermoCompromissoVm>(termoCompromisso));
    }

    @Get()
    async findAllTermoCompromisso(@Query() params): Promise<TermoCompromissoVm | TermoCompromissoVm[]> {
        if (params.tipo) {
            return this._termoCompromissoService.findTermoCompromisso(params.tipo).then(termoCompromisso => this.mapTo<TermoCompromissoVm[]>(termoCompromisso));
        } else {
            return this._termoCompromissoService.findAllTermoCompromisso().then(termoCompromisso => this.mapTo<TermoCompromissoVm[]>(termoCompromisso));
        }
    }

    @Get('/baixar-termo')
    async baixarTermo(@Query() params): Promise<{resultado: string;} | {resultado: String;}> {
        return this._termoCompromissoService.baixarTermo(params.tipo, params.id).then(termoCompromisso => termoCompromisso);
    }

}
