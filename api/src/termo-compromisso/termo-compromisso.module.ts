import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsuarioInstitutoModule } from '../usuario-instituto/usuario-instituto.module';
import { VoluntarioModule } from '../voluntario/voluntario.module';
import { MapperTermoCompromissoService } from './mapper/mapper-termo-compromisso.service';
import { TermoCompromisso } from './models/termo-compromisso.model';
import { TermoCompromissoController } from './termo-compromisso.controller';
import { TermoCompromissoService } from './termo-compromisso.service';

@Module({
  imports: [VoluntarioModule, UsuarioInstitutoModule, MongooseModule.forFeature([{ name: TermoCompromisso.modelName, schema: TermoCompromisso.model.schema }])],
  providers: [MapperTermoCompromissoService, TermoCompromissoService],
  controllers: [TermoCompromissoController]
})
export class TermoCompromissoModule {}
