import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService } from '@sins/comum';
import { InstanceType, ModelType } from 'typegoose';
import { PerfilType } from '../perfil/models/perfils.enum';
import { UsuarioInstituto } from '../usuario-instituto/models/usuario-instituto.model';
import { UsuarioInstitutoService } from '../usuario-instituto/usuario-instituto.service';
import { Voluntario } from '../voluntario/models/voluntario.model';
import { VoluntarioService } from '../voluntario/voluntario.service';
import { MapperTermoCompromissoService } from './mapper/mapper-termo-compromisso.service';
import { TermoCompromisso, TermoCompromissoModel } from './models/termo-compromisso.model';
import { TipoTermo } from './models/tipo-termo.enum';
import { TermoCompromissoVm } from './models/view-models/termo-compromisso-vm.model';
import { SituacaoVoluntario } from '../voluntario/models/types/situacao-voluntario.type';

@Injectable()
export class TermoCompromissoService extends ComumService<TermoCompromisso> {

    private fs = require('fs');


    constructor(
        @InjectModel(TermoCompromisso.modelName) private readonly _termoCompromissoModel: ModelType<TermoCompromisso>,
        private readonly _voluntarioService: VoluntarioService,
        private readonly _usuarioInstitutoService: UsuarioInstitutoService,
        private readonly _mapperService: MapperTermoCompromissoService,

    ) {
        super();
        this._model = _termoCompromissoModel;
        this._mapper = _mapperService.mapper;
    }

    async baixarTermo(perfil: string, id: string) {
        let usuario: any;
        this._model.updateMany
        let termoCompromisso = await this._model.findOne({ tipo: perfil });
        if (perfil == PerfilType.voluntario) {

            usuario = await this._voluntarioService.findById(id);
            if (usuario && usuario.dadosTermo && usuario.dadosTermo.dataAssinaturaGestorInstituto && usuario.dadosTermo.dataAssinaturaVoluntario) {
                //console.log(usuario.dadosTermo.gestorInstituto)
                let usuarioInstituto = await this._usuarioInstitutoService.findOne({ _id: usuario.dadosTermo.gestorInstituto });
                if (usuarioInstituto) {
                    return { resultado: this.getTermoHTML(termoCompromisso, "VOLUNTÁRIO", usuario, usuarioInstituto) };
                } else {
                    throw new HttpException("Gestor não encontrado", HttpStatus.BAD_REQUEST);
                }

            }
        } else
            return { resultado: this.getTermoHTML(termoCompromisso, perfil) };
    }

    private getTermoHTML(termoCompromisso: TermoCompromisso, perfil: string, usuario?: Voluntario, usuarioInstituto?: UsuarioInstituto): string {
        let header = `<div style="width: 700px"><h1>Termo de Compromisso ${perfil.toUpperCase()}</h1><br/>${termoCompromisso.textoTermo}<br/><br/><br/>`;
        if (usuario) {
            return header.concat(`<p><strong>Voluntário</strong>: ${usuario.nome}. <strong>Data</strong>: ${this.formatSignatureDate(usuario.dadosTermo.dataAssinaturaVoluntario)}</p><br/>`)
                .concat(`<p><strong>Instituto Sicoob</strong>: ${usuarioInstituto.nome.toUpperCase()}. <strong>Data</strong>: ${this.formatSignatureDate(usuario.dadosTermo.dataAssinaturaGestorInstituto)}</p><br/>`)
        }
        return header.concat('</div>');
    }

    private formatSignatureDate(date: Date) {
        let data = date,
            dia = data.getDate().toString(),
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data.getFullYear();
        return diaF + "/" + mesF + "/" + anoF;
    }

    /**
     * Por enquanto este servicio somente irá possuir um método para atualizar 
     * pois não será possível adicionar mais de um texto de compromisso, nem deletar este.
     */
    async updateTermoCompromisso(termoCompromisso: TermoCompromissoVm): Promise<TermoCompromissoVm> {
        let update = termoCompromisso.thumbnail ? { textoTermo: termoCompromisso.textoTermo, thumbnail: termoCompromisso.thumbnail } : { textoTermo: termoCompromisso.textoTermo };
        let result = await this.updateById(termoCompromisso.id, update).catch(error => {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        });
        if (termoCompromisso.tipo == PerfilType.pde) {
            await this._usuarioInstitutoService.updateMany({ perfil: PerfilType.pde }, { isSigned: false }, true)
        } else if (termoCompromisso.tipo == PerfilType.pae) {
            await this._usuarioInstitutoService.updateMany({ perfil: PerfilType.pae }, { isSigned: false }, true);
        } else if (termoCompromisso.tipo == PerfilType.voluntario) {
            await this._voluntarioService.findBySituacaoAndUpdate(SituacaoVoluntario.aguardandoAssinaturaTermo);
        }
        return this.getTermoCompromissoVm(result)
    }

    async createTermoCompromisso(data: any, tipo: TipoTermo): Promise<TermoCompromisso> {
        var termoCompromisso = new TermoCompromissoVm();
        termoCompromisso.textoTermo = data.toString();
        termoCompromisso.tipo = tipo;
        this.validarCamposObrigatorios(termoCompromisso);
        let createTermoCompromisso = this.getTermoCompromisso(termoCompromisso);
        return this.create(createTermoCompromisso)
            .then((result: any) => JSON.parse(JSON.stringify(result)) as TermoCompromisso)
            .catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    /**
     * Convertendo o readFile que precisa de um callback numa Promise
     */
    promiseRead = (path, options) => {
        return new Promise((resolve, reject) => {
            this.fs.readFile(path, options, (error, data) => {
                error ? reject(error) : resolve(data);
            });
        });
    }

    async createTermoCompromissoDefault(tipo: TipoTermo): Promise<TermoCompromisso> {
        return this.promiseRead(`./src/resources/texto-compromisso-${tipo}.txt`, 'utf-8')
            .then(data => this.createTermoCompromisso(data, tipo))
            .catch(error => { throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR) });
    }


    async findTermoCompromisso(tipo: TipoTermo): Promise<TermoCompromisso> {
        if (tipo != null && tipo != undefined) {
            return this._model.findOne({ "tipo": tipo }).then((termoCompromisso: any) => {
                if (termoCompromisso != null && termoCompromisso != undefined) {
                    return (JSON.parse(JSON.stringify(termoCompromisso)) as TermoCompromisso);
                } else {
                    return this.createTermoCompromissoDefault(tipo);
                }
            }).catch(error => {
                throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
            });
        }
    }

    async findAllTermoCompromisso(): Promise<TermoCompromisso[]> {
        return this._model.find().then(async (termoCompromisso) => {
            if (termoCompromisso != null && termoCompromisso != undefined && termoCompromisso.length > 0) {
                return (JSON.parse(JSON.stringify(termoCompromisso)) as TermoCompromisso[]);
            } else {
                let termoVoluntario = await this.createTermoCompromissoDefault(TipoTermo.voluntario);
                let termoPde = await this.createTermoCompromissoDefault(TipoTermo.pde);
                let termoPae = await this.createTermoCompromissoDefault(TipoTermo.pae);
                let array = [
                    termoVoluntario,
                    termoPde,
                    termoPae
                ];
                return array;
            }
        }).catch(error => {
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
        });
    }

    getTermoCompromissoVm(termoCompromisso: InstanceType<TermoCompromisso>): TermoCompromissoVm {
        let vm = new TermoCompromissoVm();
        vm.id = termoCompromisso._id;
        vm.textoTermo = termoCompromisso.textoTermo;
        vm.tipo = termoCompromisso.tipo;
        vm.dataCriacao = termoCompromisso.dataCriacao;
        vm.dataModificacao = termoCompromisso.dataModificacao;
        return vm;
    }

    private validarCamposObrigatorios(termoCompromisso: TermoCompromissoVm) {
        if (!termoCompromisso.textoTermo) {
            throw new HttpException('O termo de compromisso deverá possuir um texto', HttpStatus.BAD_REQUEST);
        }
    }
    private getTermoCompromisso(vm: TermoCompromissoVm): any {
        var termoCompromisso = new TermoCompromissoModel();
        termoCompromisso.id = vm.id;
        termoCompromisso.textoTermo = vm.textoTermo;
        termoCompromisso.tipo = vm.tipo;
        return termoCompromisso;
    }

}
