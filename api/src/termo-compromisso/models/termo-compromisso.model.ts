import { ModelType, pre, prop, Typegoose, plugin } from 'typegoose';
import { schemaOptions } from '@sins/comum';

@pre<TermoCompromisso>('findOneAndUpdate', function(next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})

export class TermoCompromisso extends Typegoose {
    
    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao?: Date;

    @prop({
        required: [true, 'Nome do TermoCompromisso é obrigatório']
    }) textoTermo: string;

    @prop({ default: "voluntario", index: true })
    tipo: string;

    @prop({ })
    thumbnail: any;

    static get model(): ModelType<TermoCompromisso> {
        return new TermoCompromisso().getModelForClass(TermoCompromisso, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const TermoCompromissoModel = new TermoCompromisso().getModelForClass(TermoCompromisso, { schemaOptions });
