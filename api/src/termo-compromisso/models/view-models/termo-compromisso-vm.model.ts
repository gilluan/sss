import { ComumModelVm } from '@sins/comum';

export class TermoCompromissoVm extends ComumModelVm {
    textoTermo: string;
    tipo: string;
    thumbnail: any;
}
