import { Body, Controller, Get, Post, Put, Query, Param, Delete } from '@nestjs/common';
import { PagedDataModelVm, RestController } from '@sins/comum';
import { HorasVoluntarioService } from './horas-voluntario.service';
import { HorasVoluntario } from './models/horas-voluntario.model';
import { HorasVoluntarioVm } from './models/view-models/horas-voluntario-vm.model';



@Controller('horas-voluntario')
export class HorasVoluntarioController  {

    constructor(private readonly _horasVoluntarioService: HorasVoluntarioService) {
    }

    @Post("/")
    async salvarHorasVoluntario(@Body() horasVoluntario: HorasVoluntarioVm): Promise<HorasVoluntario> {
        return this._horasVoluntarioService.salvarHorasVoluntario(horasVoluntario).then(horasVoluntario => horasVoluntario);
    }

    @Put("/")
    async editarHorasVoluntario(@Body() horasVoluntario: HorasVoluntarioVm): Promise<HorasVoluntario> {
        return this._horasVoluntarioService.editarHorasVoluntario(horasVoluntario).then(horasVoluntario => horasVoluntario);
    }

    @Delete("/:id")
    async removerHorasVoluntario(@Param('id') id: string): Promise<HorasVoluntario> {
        return this._horasVoluntarioService.delete(id).then(horasVoluntario => horasVoluntario);
    }

    @Get("/:id/total")
    async carregarTotalHorasVoluntario(@Param('id') id: string): Promise<{ resultado: PagedDataModelVm }> {
        return this._horasVoluntarioService.carregarTotalHorasVoluntario(id).then(total => total);
    }

    @Get("/:id")
    async carregarHorasVoluntarios(@Param('id') id: string, @Query() params): Promise<{ resultado: PagedDataModelVm }> {
        return this._horasVoluntarioService.carregarHorasVoluntarios(id, params).then(total => total);
    }

    @Get("/total")
    async totalHorasVoluntarios(@Query() params): Promise<{resultado: number}> {
        return this._horasVoluntarioService.totalHorasVoluntarios(params).then((total: number) => {return {resultado: total}});
    }


}
