import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService, PagedDataModelVm, PageModelVm, SequenceService, Validations } from '@sins/comum';
import { ObjectId } from 'bson';
import { ModelType, InstanceType } from 'typegoose';
import { MapperService } from './mapper/mapper.service';
import { HorasVoluntario, HorasVoluntarioModel } from './models/horas-voluntario.model';
import { HorasVoluntarioVm } from './models/view-models/horas-voluntario-vm.model';

@Injectable()
export class HorasVoluntarioService extends ComumService<HorasVoluntario> {

    constructor(
        @InjectModel(HorasVoluntario.modelName) private readonly _horasVoluntarioModel: ModelType<HorasVoluntario>,
        private readonly _mapperService: MapperService,
        private readonly _sequenceService: SequenceService
    ) {
        super();
        this._model = _horasVoluntarioModel;
        this._mapper = _mapperService.mapper;
    }

    async salvarHorasVoluntario(vm: HorasVoluntarioVm) {
        try {
            const programa = vm.programa ? { programa: this.toObjectId(vm.programa) } : { outros: true };
            const horasVoluntarios = await this._model.find({ $and: [programa, { voluntario: this.toObjectId(vm.voluntario) }] }).exec();
            if (horasVoluntarios && horasVoluntarios.length > 0) {
                const h: InstanceType<HorasVoluntario> = horasVoluntarios[0];
                vm.horas = vm.horas + h.horas;
                vm.id = h.id;
                return await this.editarHorasVoluntario(vm);
            } else {
                await this._model.create(this.getHorasVoluntarioModel(vm));
                return await this._model.findOne();
            }
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async editarHorasVoluntario(vm: HorasVoluntarioVm) {
        try {
            const horasVoluntario = await this.getHorasVoluntarioModel(vm);
            return await this.updateById(vm.id, horasVoluntario);
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async carregarTotalHorasVoluntario(voluntario: string) {
        let resultado = await this._model.aggregate([
            { $match: { $and: [{ voluntario: this.toObjectId(voluntario) }] } },
            { $group: { _id: 0, total: { $sum: "$horas" } } },
            { $project: { _id: 0, total: 1 } }
        ]).exec();
        let total = resultado && resultado.length > 0 ? resultado[0].total : 0;
        return { resultado: total };
    }

    async carregarHorasVoluntarios(voluntario: string, params: any) {
        const { offset, limit } = this.tratarPaginacao(params.numeroPagina, params.tamanhoPagina);
        params.voluntario = voluntario;
        let filter = this.montarParametros(params);
        const total = await this.totalHorasVoluntarios(params);
        let resultado = await this._model.aggregate([
            { $match: { $and: filter } },
            { $lookup: { from: 'programa', localField: 'programa', foreignField: '_id', as: 'programa' } },
            { $unwind: { path: "$programa", preserveNullAndEmptyArrays: true } },
            { $lookup: { from: 'usuarioinstitutos', localField: 'responsavel', foreignField: '_id', as: 'responsavel' } },
            { $unwind: { path: "$responsavel", preserveNullAndEmptyArrays: false } },
            { $lookup: { from: 'voluntarios', localField: 'voluntario', foreignField: '_id', as: 'voluntario' } },
            { $unwind: { path: "$voluntario", preserveNullAndEmptyArrays: false } },
            { $addFields: { id: "$_id" } }]).skip(offset * limit).limit(limit).exec();
        return { resultado: new PagedDataModelVm(new PageModelVm(offset, limit, total), resultado) };
    }

    private tratarPaginacao = (offset, limit) => {
        offset = parseInt(offset, 10);
        limit = parseInt(limit, 10);
        if (isNaN(offset) || isNaN(limit)) {
            throw new HttpException('Dados de paginação \'numeroPagina\' e \'tamanhoPagina\' são obrigatórios', HttpStatus.BAD_REQUEST);
        }
        return { offset, limit }
    }

    async totalHorasVoluntarios(params: any) {
        let filter = this.montarParametros(params);
        let total = await this._model.countDocuments({ $and: filter });
        return total;
    }


    private montarParametros(params) {
        let filter = [];
        filter.push({ _id: { $exists: true } })
        if (params) {
            Validations.notNull(params.responsavel, (n) => filter.push({ responsavel: new ObjectId(n) }));
            Validations.notNull(params.programa, (n) => filter.push({ programa: new ObjectId(n) }));
            Validations.notNull(params.voluntario, (n) => filter.push({ voluntario: new ObjectId(n) }));
        }
        return filter;
    }

    private getHorasVoluntarioModel(vm: HorasVoluntarioVm) {
        const newHorasVoluntario = new HorasVoluntarioModel();
        newHorasVoluntario._id = vm.id ? vm.id : null;
        newHorasVoluntario.horas = vm.horas;
        newHorasVoluntario.programa = this.tratarObjectId(vm.programa);
        newHorasVoluntario.responsavel = this.tratarObjectId(vm.responsavel);
        newHorasVoluntario.voluntario = this.tratarObjectId(vm.voluntario);
        newHorasVoluntario.outros = vm.outros;
        return newHorasVoluntario;
    }

    private tratarObjectId = (value: any) => value ? typeof value === 'string' ? this.toObjectId(value) : this.toObjectId(value._id) : null;


}
