import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HorasVoluntario } from './models/horas-voluntario.model';
import { HorasVoluntarioService } from './horas-voluntario.service';
import { HorasVoluntarioController } from './horas-voluntario.controller';
import { MapperService } from './mapper/mapper.service';
import { SequenceModule, SequenceService } from '@sins/comum';

@Module({
    imports: [SequenceModule, MongooseModule.forFeature([{ name: HorasVoluntario.modelName, schema: HorasVoluntario.model.schema }])],
    providers: [MapperService, HorasVoluntarioService, SequenceService],
    controllers: [HorasVoluntarioController],
    exports: [HorasVoluntarioService, MapperService],
})
export class HorasVoluntarioModule {
}