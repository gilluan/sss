import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigurationService } from '@sins/comum';
import { InstanceType } from 'typegoose';
import { HorasVoluntario, HorasVoluntarioModel } from './models/horas-voluntario.model';
import { HorasVoluntarioVm } from './models/view-models/horas-voluntario-vm.model';
import { HorasVoluntarioModule } from './horas-voluntario.module';
import { HorasVoluntarioService } from './horas-voluntario.service';


function getHorasVoluntarioModel(vm: HorasVoluntarioVm) {
  const newHorasVoluntario = new HorasVoluntarioModel();
  newHorasVoluntario.id = vm.id ? vm.id : null;
  newHorasVoluntario.horas = vm.horas;
  newHorasVoluntario.programa = this.tratarObjectId(vm.programa);
  newHorasVoluntario.responsavel =  this.tratarObjectId(vm.responsavel);
  newHorasVoluntario.voluntario = this.tratarObjectId(vm.voluntario);
  return newHorasVoluntario;
}


describe('HorasVoluntarioService', () => {
  let service: HorasVoluntarioService;
  let model: InstanceType<HorasVoluntario>;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(ConfigurationService.connectionString, {
          retryDelay: 500,
          retryAttempts: 3,
          useNewUrlParser: true,
        }),
        MongooseModule.forFeature([{ name: HorasVoluntario.modelName, schema: HorasVoluntario.model.schema }]),
        HorasVoluntarioModule,
      ]
    }).compile();
    service = module.get<HorasVoluntarioService>(HorasVoluntarioService);
    model = null;
  });

 it('Criando horas voluntario', async () => {
    let vm = new HorasVoluntarioVm();
    vm.programa = "5d9d0250d3feae42c03f3925";
    vm.voluntario = "5d35f761e68e5000185095d0";
    vm.responsavel = "5cb794068d676d81f46c5e87";
    vm.horas = 40;
    model = await service.salvarHorasVoluntario(vm);
    expect(model.id).not.toBeNull()
  }); 

  it('Listando o horas voluntario cadastrado', async () => {
    let horas = await service.carregarHorasVoluntarios("5d35f761e68e5000185095d0", {numeroPagina: 0, tamanhoPagina: 10});
    expect(horas.resultado.data.length).toBe(1)
  });

  it('Editando um horas voluntario', async () => {
    let vm = new HorasVoluntarioVm();
    vm.programa = "5d9d0250d3feae42c03f3925";
    vm.voluntario = "5d35f761e68e5000185095d0";
    vm.responsavel = "5cb794068d676d81f46c5e87";
    vm.horas = 50;
    vm.id = model.id;
    try {
      model = await service.editarHorasVoluntario(vm);
    } catch (error) {
      model = new HorasVoluntarioModel();
      model.id = vm.id;      
      model.horas = 40;
    }
    expect(model.horas).toBe(50)
  });

  it('Deletando horas voluntario', async () => {
     await service.delete(model.id);
     model = await service.findById(model.id);
    expect(model).toBeNull()
  });

  
});


