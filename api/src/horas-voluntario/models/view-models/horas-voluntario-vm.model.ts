
import { ComumModelVm } from '@sins/comum';

export class HorasVoluntarioVm extends ComumModelVm {
  horas: number;
  programa: any;
  responsavel: any;
  voluntario: any;
  outros: boolean;
}
