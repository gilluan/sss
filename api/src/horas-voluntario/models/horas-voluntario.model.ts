import { Types, SchemaOptions } from 'mongoose';
import { ModelType, pre, prop, Typegoose } from 'typegoose';

@pre<HorasVoluntario>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class HorasVoluntario extends Typegoose {
    @prop({ required: [true, 'Quantidade de horas é obrigatório'] }) horas: number;
    @prop({ required: [true, `Um projeto precisa estar associado a um voluntario`] }) voluntario: Types.ObjectId;
    @prop({}) programa: Types.ObjectId;
    @prop({default: false}) outros: boolean;
    @prop() responsavel: Types.ObjectId;
    @prop({ default: Date.now, index: true }) dataCriacao?: Date;
    @prop({ default: Date.now, index: true }) dataModificacao?: Date;


    static get model(): ModelType<HorasVoluntario> {
        return new HorasVoluntario().getModelForClass(HorasVoluntario, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }

}
 
const schemaOptions: SchemaOptions = {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    collection: 'horas-voluntario'
};

export const HorasVoluntarioModel = new HorasVoluntario().getModelForClass(HorasVoluntario, { schemaOptions });

