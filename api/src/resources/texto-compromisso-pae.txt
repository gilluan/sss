<p>Já devidamente habilitado no Sistema do Instituto Sicoob - SINS, comprometo-me com a adequada utilização dos acessos a mim disponibilizados, sob pena de sujeitar-me às sanções previstas no caderno normativo, quanto ao não fiel cumprimento das normas aplicáveis.</p>
<p>Declaro estar ciente de que o acesso ao SINS pode ser realizado por meio de usuário e senha do SISBR, padrão, emitido pelo CTA de modo nominal e individualizado, reconhecendo desde já que os acessos efetuados por citado dispositivo, registrados em log de transações mantido pelo SINS, constitui prova irretratável quanto à autoria das movimentações efetuadas no sistema.</p>
<p>Assumo, ainda, os seguintes compromissos:</p>

<ul>I - Utilizar os dados dos sistemas informatizados com cautela na exibição de dados em tela e/ou impressora e/ou gravação em meios eletrônicos;</ul>
<ul>II - Manter a confidencialidade das senhas, não as compartilhando com terceiros, mesmo que servidores lotados em minha unidade de trabalho, sob qualquer protesto;</ul>
<ul>III - Evitar registrar as senhas em papel;</ul>
<ul>IV - Alterar a senha sempre que existir qualquer dúvida quanto a sua confidencialidade;</ul>
<ul>V - Não revelar, fora do âmbito profissional, fato ou informação de qualquer natureza de que tenha conhecimento por força dos acessos a sistemas e informações a mim conferidos;</ul>
<ul>VI - Não me ausentar da estação de trabalho sem encerrar a sessão de uso do sistema, garantindo assim a impossibilidade de acesso indevido por terceiros;</ul>
<ul>VII - Não acessar e/ou divulgar informações não motivadas por necessidade de serviço;</ul>
<ul>VIII - Realizar o acompanhamento do cadastro dos Voluntários no SINS;</ul>
<ul>IX – Realizar correto registro no SINS da execução dos programas realizados na sua Central;</ul>
<ul>X – Realizar a guarda da documentação referente a execução dos programas realizados na sua Central;</ul>
<ul>XI - Alimentar o Sistema do Instituto Sicoob (SINS) que compõe: cadastro, acompanhamento e prestação de contas dos resultados;</ul>
<ul>XII - Executar o plano de ação pactuado;</ul>
<ul>XIII - Executar as ações e atividades regionais em conformidade com as diretrizes estratégicas e a expertise do INSTITUTO SICOOB;</ul>
<ul>XIV - Prestar contas rotineiramente ao INSTITUTO SICOOB sobre suas ações e atividades regionais;</ul>
<ul>XV - Executar suas atribuições em conformidade com o caderno normativo do Instituto Sicoob vigente</ul>

<p>A inobservância ao disposto acima poderá ensejar na execução por parte do INSTITUTO SICOOB das penalidades previstas na clausula nona do <b>CONTRATO DE RELACIONAMENTO</b>, assinado entre as partes.</p>