import { Module } from '@nestjs/common';
import { UsuarioInstitutoService } from './usuario-instituto.service'
import { UsuarioInstitutoController } from './usuario-instituto.controller'
import { MongooseModule } from '@nestjs/mongoose';
import { UsuarioInstituto } from './models/usuario-instituto.model';

@Module({
  imports: [MongooseModule.forFeature([{ name: UsuarioInstituto.modelName, schema: UsuarioInstituto.model.schema }])],
  providers: [UsuarioInstitutoService],
  controllers: [UsuarioInstitutoController],
  exports: [UsuarioInstitutoService]
})
export class UsuarioInstitutoModule {}
