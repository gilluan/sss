import { Body, Delete, Get, Param, Post, Put, Query, Controller } from '@nestjs/common';
import { UsuarioInstituto } from './models/usuario-instituto.model';
import { UsuarioInstitutoVm } from './models/view-models/usuario-instituto-vm.model';
import { UsuarioInstitutoService } from './usuario-instituto.service';

@Controller('usuarios-instituto')
export class UsuarioInstitutoController {

    constructor(private readonly _usuarioSicoobService: UsuarioInstitutoService) { }
 
    @Post()
    async register(@Body() vm: UsuarioInstitutoVm): Promise<UsuarioInstituto> {
        return this._usuarioSicoobService.createUsuarioInstituto(vm).then(usuarioSicoob => usuarioSicoob);
    }

    @Put()
    async update(@Body() vm: UsuarioInstitutoVm): Promise<UsuarioInstituto> {
        return this._usuarioSicoobService.updateUsuarioInstituto(vm).then(usuarioSicoob => usuarioSicoob);
    }

    @Delete('/:id')
    async delete(@Param('id') id: string): Promise<any> {
        return this._usuarioSicoobService.deletarUsuario(id).then(usuarioSicoob => usuarioSicoob);
    }

    @Post("/:cpf/assinar")
    async usuarioAssinou(@Param('cpf') cpf: string): Promise<UsuarioInstituto> {
        return this._usuarioSicoobService.usuarioInstitutoAssinou(cpf).then(usuarioSicoob => usuarioSicoob);
    }
 
    @Get()
    async find(@Query() req): Promise<UsuarioInstituto[]> {
        return this._usuarioSicoobService.recuperar(req).then(usuarioSicoob => usuarioSicoob);
    }

}
