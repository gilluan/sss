import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ComumService } from '@sins/comum';
import { InstanceType, ModelType } from 'typegoose';
import { UsuarioInstituto } from './models/usuario-instituto.model';
import { UsuarioInstitutoVm } from './models/view-models/usuario-instituto-vm.model';


@Injectable()
export class UsuarioInstitutoService extends ComumService<UsuarioInstituto> {

    constructor(@InjectModel(UsuarioInstituto.modelName) private readonly _usuarioSicoobModel: ModelType<UsuarioInstituto>) {
        super();
        this._model = _usuarioSicoobModel;
    }

    async updateMany(conditions, doc, multi = false) {
        return await this._model.update(conditions, {$set: doc}, {multi}).exec();
    }

    getNewUsuarioInstituto(vm: UsuarioInstitutoVm): UsuarioInstituto {
        let obj = new UsuarioInstituto();
        //obj.id = vm.id
        obj.dataCriacao = vm.dataCriacao
        obj.dataModificacao = vm.dataModificacao
        obj.cpf = vm.cpf
        obj.email = vm.email
        obj.idInstituicaoOrigem = vm.idInstituicaoOrigem
        obj.idUnidadeInstOrigem = vm.idUnidadeInstOrigem
        obj.login = vm.login
        obj.nome = vm.nome
        obj.numeroCooperativa = vm.numeroCooperativa
        obj.foto = vm.foto
        obj.perfil = vm.perfil
        obj.isSigned = vm.isSigned
        return obj;
    }

    getNewUsuarioInstitutoVm(obj: InstanceType<UsuarioInstituto>): UsuarioInstitutoVm {
        let vm = new UsuarioInstitutoVm();
        vm.id = obj._id
        vm.dataCriacao = obj.dataCriacao
        vm.dataModificacao = obj.dataModificacao
        vm.cpf = obj.cpf
        vm.email = obj.email
        vm.idInstituicaoOrigem = obj.idInstituicaoOrigem
        vm.idUnidadeInstOrigem = obj.idUnidadeInstOrigem
        vm.login = obj.login
        vm.nome = obj.nome
        vm.numeroCooperativa = obj.numeroCooperativa
        vm.foto = obj.foto
        vm.perfil = obj.perfil
        vm.isSigned = obj.isSigned
        return vm;
    }

    async createUsuarioInstituto(vm: UsuarioInstitutoVm): Promise<UsuarioInstituto> {
        return this._usuarioSicoobModel.create(this.getNewUsuarioInstituto(vm)).catch(
            erro => { this.logger.error(`POST [id: ${vm.email}]`, erro); throw new HttpException(`Aconteceu um erro tentando criar o usuário ${vm.email}`, HttpStatus.INTERNAL_SERVER_ERROR) }
        )
    }

    async updateUsuarioInstituto(vm: UsuarioInstitutoVm): Promise<UsuarioInstituto> {
        return this.updateById(vm.id, this.getNewUsuarioInstituto(vm)).catch(
            erro => { this.logger.error(`PUT [id: ${vm.id}]`, erro); throw new HttpException(`Aconteceu um erro tentando atualizar o usuário ${vm.id}`, HttpStatus.INTERNAL_SERVER_ERROR) }
        )
    }

    async usuarioInstitutoAssinou(cpf: string): Promise<UsuarioInstituto> {
        let usuario = await this.findOne({cpf});
        if(usuario) {
            usuario.isSigned = true;
            try {
                return await this.updateById(usuario.id, usuario);
            } catch (error) {
                this.logger.error(`PUT assinar [cpf: ${cpf}]`, error);
            }
        }
        throw new HttpException(`Aconteceu um erro tentando buscar o usuário ${cpf}`, HttpStatus.INTERNAL_SERVER_ERROR) 
    }

    async findUsuarioInstituto(email: string): Promise<InstanceType<UsuarioInstituto>> {
        return this.findOne({ email }).catch(
            erro => { this.logger.error(`GET [email: ${email}]`, erro); throw new HttpException(`Aconteceu um erro tentando buscar o usuário ${email}`, HttpStatus.INTERNAL_SERVER_ERROR) }
        )
    }

    async recuperarNomeById(id: string) {
        return this._model.findOne({ "_id": id }).select({ "nome": 1, "_id": 0 }).exec().then(
            (vol) => vol).catch(
                erro => { this.logger.error(`GET [id: ${id}]`, erro); throw new HttpException(`Aconteceu um erro tentando buscar o usuário ${id}`, HttpStatus.BAD_REQUEST) }
            )
    }

    async deletarUsuario(id: string) {
        return this._usuarioSicoobModel.deleteOne({ _id: id }).exec().catch(
            erro => { this.logger.error(`DELETE /usuarios-instituto/${id}`, erro); throw new HttpException("Aconteceu um erro tentando excluir o usuário", HttpStatus.INTERNAL_SERVER_ERROR); }
        )
    }

    async recuperar(req: any) {
        let filter = this.montarParametros(req)
        if (req.campo) {
            return await this.recuperarPorCampos(filter, req.campo);
        } else {
            if (filter.length > 0)
                return await this.findAll({ $and: filter });
            else
                return await this.findAll();
        }

    }

    private async recuperarPorCampos(filtro, campo): Promise<any> {
        let pipelineOperators: any[] = [{ $match: { $and: filtro } }];
        pipelineOperators.push({ $group: this.buildGroup(campo, campo) });
        pipelineOperators.push({ $project: this.buildFields(campo) });
        return this._usuarioSicoobModel.aggregate(pipelineOperators)
            .then(projection => projection.map(i => i[campo]));
    }

    private buildFields(fields: string): any {
        const possuiId: boolean = fields.indexOf("_id") > -1;
        let campos: string = fields.split(',').map(campo => `\"${campo}\": 1`).join(',');
        campos = !possuiId ? "\"_id\": 0," + campos : campos;
        return JSON.parse(`{${campos}}`);
    }

    private buildGroup(id, fields): any {
        let campos: string = fields.split(',').map(campo => `\"${campo}\": { \"$first\": \"$${campo}\" }`).join(',');
        campos = `\"_id\": \"$${id}\", ${campos}`;
        return JSON.parse(`{${campos}}`);
    }

    private montarParametros(params) {
        let filter = [];
        filter.push({ _id: { $exists: true } })
        if (params) {
            let nome = params.nome;
            let identificador = params.identificador;
            let email = params.email;
            let perfil = params.perfil;
            let numeroCooperativa = params.numeroCooperativa;
            let cpf = params.cpf;
            if (identificador) {
                filter.push({ _id: this.toObjectId(identificador) });
            }
            if (email) {
                filter.push({ email });
            }
            if (numeroCooperativa) {
                filter.push({ numeroCooperativa });
            }
            if (nome) {
                filter.push({ nome: new RegExp(nome, 'i') })
            }
            if (perfil) {
                filter.push({ perfil });
            }
            if (cpf) {
                filter.push({ cpf })
            }
        }
        return filter;
    }
}
