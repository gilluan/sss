import { ModelType, pre, prop, Typegoose, plugin } from 'typegoose';
import { schemaOptions } from '@sins/comum';

@pre<UsuarioInstituto>('findOneAndUpdate', function (next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class UsuarioInstituto extends Typegoose {

    @prop({required: [true, 'O login é um campo obrigatório do Usuário Sicoob']})
    login: string;

    @prop({required: [true, 'O nome é um campo obrigatório do Usuário Sicoob']})
    nome: string;

    @prop({required: [true, 'O cpf é um campo obrigatório do Usuário Sicoob']})
    cpf: string;

    @prop({required: [true, 'O email é um campo obrigatório do Usuário Sicoob']})
    email: string;

    @prop({required: [true, 'O numero da cooperativa é um campo obrigatório do Usuário Sicoob']})
    numeroCooperativa: string;

    @prop({required: [true, 'O id da instituição de origem é um campo obrigatório do Usuário Sicoob']})
    idInstituicaoOrigem: number;

    @prop({required: [true, 'O id da unidade de origem é um campo obrigatório do Usuário Sicoob']})
    idUnidadeInstOrigem: number;

    @prop({})
    foto: string;

    @prop({required: [true, 'O perfil é um campo obrigatório do Usuário Sicoob']})
    perfil: string;

    @prop({ default: false })
    isSigned: boolean;

    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao?: Date;

    static get model(): ModelType<UsuarioInstituto> {
        return new UsuarioInstituto().getModelForClass(UsuarioInstituto, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const UsuarioInstitutoModel = new UsuarioInstituto().getModelForClass(UsuarioInstituto, { schemaOptions });
