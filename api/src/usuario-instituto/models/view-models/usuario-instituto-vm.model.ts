import { ComumModelVm } from '@sins/comum';

export class UsuarioInstitutoVm extends ComumModelVm {
    cpf: string;
    email: string;
    idInstituicaoOrigem: number;
    idUnidadeInstOrigem: number;
    login: string;
    nome: string;
    numeroCooperativa: string;
    foto: string;
    perfil: string;
    isSigned: boolean;
}