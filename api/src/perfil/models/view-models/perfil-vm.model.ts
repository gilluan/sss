import { ComumModelVm } from '@sins/comum';

export class PerfilVm extends ComumModelVm {
    nome: string;
    codigo: number;
    label: string;
}
