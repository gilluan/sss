export enum PerfilType {
    superintendente = "gestor",
    analistaProjeto = "ppe",
    pae = "pae",
    pde = "pde",
    voluntario = "voluntario",
}
