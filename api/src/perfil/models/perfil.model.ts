import { ModelType, pre, prop, Typegoose, plugin } from 'typegoose';
import { schemaOptions } from '@sins/comum';


@pre<Perfil>('findOneAndUpdate', function(next) {
    this._update.dataModificacao = new Date(Date.now());
    next();
})
export class Perfil extends Typegoose {
    
    @prop({
        required: [true, 'Nome do Perfil é obrigatório'],
        unique: true,
        minlength: [10, 'O nome do Perfil deve conter mais de 10 caracteres'],
    })
    nome: string;

    @prop({ default: Date.now, index: true })
    dataCriacao?: Date;

    @prop({ default: Date.now, index: true })
    dataModificacao?: Date;

    @prop({})codigo: number;

    @prop({})label: string;



    static get model(): ModelType<Perfil> {
        return new Perfil().getModelForClass(Perfil, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const PerfilModel = new Perfil().getModelForClass(Perfil, { schemaOptions });
