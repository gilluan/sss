import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Perfil, PerfilModel } from './models/perfil.model';
import { ComumService } from '@sins/comum';
import { InjectModel } from '@nestjs/mongoose';
import { MapperService } from './mapper/mapper.service';
import { ModelType } from 'typegoose';
import { PerfilVm } from './models/view-models/perfil-vm.model';

@Injectable()
export class PerfilService extends ComumService<Perfil> {

    constructor(
        @InjectModel(Perfil.modelName) private readonly _perfilModel: ModelType<Perfil>,
        private readonly _mapperService: MapperService,
    ) {
        super();
        this._model = _perfilModel;
        this._mapper = _mapperService.mapper;
    }

    async register(vm: PerfilVm) {
        const { nome, codigo, label } = vm;

        const newPerfil = new PerfilModel();
        newPerfil.nome = nome.trim().toLowerCase();
        newPerfil.codigo = codigo;
        newPerfil.label = label;

        try {
            const result = await this.create(newPerfil);
            return result.toJSON() as Perfil;
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
