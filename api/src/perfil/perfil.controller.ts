import { Body, Controller, HttpException, HttpStatus, Post, Get, Query } from '@nestjs/common';

import { Perfil } from './models/perfil.model';
import { PerfilService } from './perfil.service';
import { PerfilVm } from './models/view-models/perfil-vm.model';
import { ApiException } from '@sins/comum';

import { GetOperationId } from '@sins/comum';

@Controller('perfil')
export class PerfilController {

    constructor(private readonly _perfilService: PerfilService) { }

    @Post('register')
    async register(@Body() vm: PerfilVm): Promise<PerfilVm> {
        const { nome } = vm;

        if (!nome) {
            throw new HttpException('Nome do perfil é obrigatório', HttpStatus.CONFLICT);
        }

        let exist;
        try {
            exist = await this._perfilService.findOne({ nome });
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (exist) {
            throw new HttpException(`Um Perfil com o nome ${nome} já existe`, HttpStatus.CONFLICT);
        }

        const newPerfil = await this._perfilService.register(vm);
        return this._perfilService.map<PerfilVm>(newPerfil);
    }
}
