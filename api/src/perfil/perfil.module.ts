import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Perfil } from './models/perfil.model';
import { PerfilService } from './perfil.service';
import { PerfilController } from './perfil.controller';
import { MapperService } from './mapper/mapper.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: Perfil.modelName, schema: Perfil.model.schema }])],
    providers: [MapperService, PerfilService],
    controllers: [PerfilController],
    exports: [PerfilService, MapperService],
})
export class PerfilModule {
}